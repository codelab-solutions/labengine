<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   CMS: The CMS superobject
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class CMS_CMSSUPEROBJECT
	{


		/**
		 *   Page structure
		 *   @var array
		 *   @access public
		 */

		public $cmsStruct = array();


		/**
		 *   Page structure map by URL
		 *   @var array
		 *   @access public
		 */

		public $cmsStructByURL = array();


		/**
		 *   Languages
		 *   @var array
		 *   @access public
		 */

		public $cmsLang = array();


		/**
		 *   Modules
		 *   @var array
		 *   @access public
		 */

		public $cmsModule = array();


		/**
		 *   Init CMS environment
		 *   @access public
		 *   @throws Exception
		 *   @return void
		 */

		public function initCMS()
		{
			global $LAB;

			// Load structure
			$sql="
				SELECT
					*
				FROM
					cms_struct
				ORDER BY
					ord
			";
			$this->cmsStruct=$LAB->DB->querySelectSQL($sql,'struct_oid');
			if (!sizeof($this->cmsStruct)) throw new Exception('Structure empty. Default database not loaded?');

			// Create URL map
			foreach ($this->cmsStruct as $row)
			{
				$this->cmsStructByURL[mb_strtolower($row['struct_url_full'])]=$row['struct_oid'];
			}

			// Load languages
			$sql="
				SELECT
					*
				FROM
					cms_lang
				WHERE
					lang_status=0
				ORDER BY
					ord
			";
			$dataset=$LAB->DB->querySelectSQL($sql);
			if (!sizeof($dataset)) throw new Exception('No languages. Default database not loaded?');
			foreach ($dataset as $row)
			{
				$this->cmsLang[$row['lang_tag']]=$row['lang_tag'];
			}

			// Load modules
			$sql="
				SELECT
					*
				FROM
					cms_module
				WHERE
					module_status=0
				ORDER BY
					module_tag
			";
			$dataset=$LAB->DB->querySelectSQL($sql);
			foreach ($dataset as $row)
			{
				$this->cmsModule[$row['module_tag']]=$row;
			}
		}


		/**
		 *   Display sidebar
		 *   @access public
		 *   @param int $struct_oid Struct OID (if in page sidebar mode)
		 *   @param string $lang Request language (if in page sidebar mode)
		 *   @throws Exception
		 *   @return string
		 */

		public function displaySidebar( $struct_oid=null, $lang=null )
		{
			global $LAB;

			// Navi
			$SIDEBAR=array(

				'struct' => array(
					'url' => '/admin/struct',
					'icon' => 'icon-struct'
				),

				'media' => array(
					'url' => '/admin/media',
					'icon' => 'icon-folder'
				),

				'settings' => array(
					'url' => '/admin/settings',
					'icon' => 'icon-settings'
				),

			);

			$c='<div id="admin_sidebar">';
			$c.='<a target="_top" href="/admin/dashboard"><div class="block"><img src="/cms/static/gfx/lab-logo.svg" style="width: 80px; height: 80px" onerror="this.src=\'/cms/static/gfx/lab-logo.png\'"></div></a>';

			if (!empty($struct_oid))
			{
				$c.='<div class="admin_sidebar_split">';
				$c.='<div class="admin_sidebar_col">';
				$LAB->RESPONSE->responseParam['section']='struct';
			}

			foreach ($SIDEBAR as $sidebarID => $sidebarSettings)
			{
				$c.='<a target="_top" href="'.$sidebarSettings['url'].'"><div class="block selectable'.($LAB->RESPONSE->responseParam['section']==$sidebarID?' active':'').'"><span class="'.$sidebarSettings['icon'].'"></span></div></a>';
			}
			$c.='<a target="_top" href="/admin/logout"><div class="block selectable gray"><span class="icon-logout"></span></div></a>';

			if (!empty($struct_oid))
			{
				$c.='</div>';
				$c.='<div class="admin_sidebar_col admin_sidebar_col2">';

					// Page navi
					$PAGESIDEBAR=array(

						'page' => array(
							'url' => '/admin/page/'.$struct_oid.'/'.$lang,
							'icon' => 'icon-page'
						),

						'settings' => array(
							'url' => '/admin/page/settings/'.$struct_oid.'/'.$lang,
							'icon' => 'icon-sliders'
						),

						'permissions' => array(
							'url' => '/admin/page/permissions/'.$struct_oid.'/'.$lang,
							'icon' => 'icon-lock'
						),

					);

					$LAB->RESPONSE->responseParam['subsection']=oneof($LAB->RESPONSE->responseParam['subsection'],'page');
					foreach ($PAGESIDEBAR as $sidebarID => $sidebarSettings)
					{
						$c.='<a target="_top" href="'.$sidebarSettings['url'].'"><div class="block selectable'.($LAB->RESPONSE->responseParam['subsection']==$sidebarID?' active':'').'"><span class="'.$sidebarSettings['icon'].'"></span></div></a>';
					}

				$c.='</div>';
				$c.='</div>';
			}

			$c.='</div>';
			return $c;
		}


		/**
		 *   Get CMS roles
		 *   @access public
		 *   @return array
		 */

		public function getRoleList()
		{
			$roleList=array();
			$roleList['cms']='CMS: CMS access';
			$roleList['cms_supervisor']='CMS: supervisor';
			return $roleList;
		}


	}


?>