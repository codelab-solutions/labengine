<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   CMS: The request handler class
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class CMS_REQUEST extends BASE_REQUEST
	{


		/**
		 *   Request type (cms, admin, site)
		 *   @var string
		 *   @access public
		 */

		public $requestType = false;


		/**
		 *   Request page
		 *   @var string
		 *   @access public
		 */

		public $requestPage = '';


		/**
		 *   Request struct OID
		 *   @var integer|boolean
		 *   @access public
		 */

		public $requestStructOID = FALSE;


		/**
		 *   Handle request
		 *   @access public
		 *   @throws Exception
		 *   @return void
		 */

		public function handleRequest()
		{
			global $LAB;

			// Init CMS
			$LAB->CMS=new CMS_CMSSUPEROBJECT();
			$LAB->CMS->initCMS();

			// Init
			if (strlen($_SERVER['SCRIPT_URL'])>1)
			{
				$uriArray=$originalUriArray=preg_split('/\//', substr($_SERVER['SCRIPT_URL'], 1));
			}
			else
			{
				$uriArray=array();
				$rootElementOID=CMS_STRUCT::getRootElementOID();
				if (sizeof($LAB->CMS->cmsLang)>1) $uriArray[]=key($LAB->CMS->cmsLang);
				$uriArray[]=$LAB->CMS->cmsStruct[$rootElementOID]['struct_url'];
				$LAB->RESPONSE->setRedirect('/'.join('/',$uriArray));
				return;
			}
			$uriMode = 0;
			$uriPos = 0;
			$uriVarCount = 0;
			$structURL = '';
			$haveLang = false;
			$haveModule = false;
			$haveController = false;
			$haveURL = false;
			$resource = false;

			// Parse URI
			while (true)
			{
				// Check
				if (!sizeof($uriArray)) break;
				$uriElement=array_shift($uriArray);
				$uriPos++;

				// Language?
				if ($uriPos==1 && mb_strlen($uriElement)==2)
				{
					$uriElement=mb_strtolower($uriElement);
					if (!empty($LAB->CMS->cmsLang[$uriElement]))
					{
						$this->requestLang=$uriElement;
						$haveLang=true;
						$this->requestURI.='/'.$uriElement;
						continue;
					}
				}

				// Resource?
				if ($uriPos==1)
				{
					if (mb_strtolower($uriElement)=='robots.txt')
					{
						$this->requestType='resource';
						$resource='robots';
						break;
					}
					if (mb_strtolower($uriElement)=='sitemap.xml')
					{
						$this->requestType='resource';
						$resource='sitemap';
						break;
					}
				}

				// Admin?
				if (empty($this->requestType) && mb_strtolower($uriElement)=='admin')
				{
					$this->requestURI.='/'.$uriElement;
					$this->requestType='admin';
					continue;
				}

				// Base?
				if (empty($this->requestType) && mb_strtolower($uriElement)=='base')
				{
					$this->requestURI.='/'.$uriElement;
					$this->requestType='base';
					continue;
				}

				// Module?
				if (empty($this->requestType) && mb_strtolower($uriElement)=='module')
				{
					$this->requestURI.='/'.$uriElement;
					$this->requestType='module';
					continue;
				}

				// Got this far and no request type? Means: CMS
				if (empty($this->requestType))
				{
					$this->requestType='cms';
				}

				// Check
				if (!preg_match("/^[A-Za-z0-9\.\_\-]+$/",$uriElement))
				{
					$uriMode=1;
				}

				// Variable?
				if (!empty($uriMode))
				{
					// Split
					$varName=$varValue='';
					if (strpos($uriElement,'=')!==FALSE)
					{
						list($varName,$varValue)=preg_split('/=/',$uriElement,2);
					}
					else
					{
						$varName=$uriElement;
						$varValue=TRUE;
					}

					// Check name
					if ($varValue!==TRUE && !preg_match("/^[A-Za-z0-9\.\_\-]+$/",$varName))
					{
						$LAB->RESPONSE->setRedirect($this->requestURI);
						return;
					}

					// Add to URI
					$this->requestURI.='/'.$uriElement;

					// Set variable
					$uriVarCount++;
					if (is_string($varValue) && preg_match("/^[0-9]+$/",$varValue)) $varValue=intval($varValue);
					if (is_string($varValue) && preg_match("/^[0-9\.]+$/",$varValue)) $varValue=floatval($varValue);
					$this->requestVar[$varName]=$varValue;
					$this->requestVar['var_'.$uriVarCount]=array('name'=>$varName,'value'=>$varValue);

					continue;
				}

				// Do stuff depending on request type
				switch ($this->requestType)
				{

					// Admin
					case 'admin':

						// Check if controller exists
						$controllerPath='cms/admin/controller/';
						if (!stream_resolve_include_path($controllerPath.$uriElement.'.php'))
						{
							header("Status: 404 Not Found");
							throw New Exception('HTTP 404: The specified resource '.$this->requestURI.'/'.$uriElement.' was not found.');
						}

						// Init controller
						$this->requestURI.='/'.$uriElement;
						$this->requestController=$uriElement;
						$this->initController($controllerPath.$uriElement.'.php');
						$uriMode=1;

						// Check login
						if ($this->controllerObject->controllerLoginRequired && $this->requestController!='login' && !$LAB->USER->isLoggedIn())
						{
							$LAB->SESSION->set('login.redirect',$_SERVER['SCRIPT_URL']);
							$LAB->RESPONSE->setRedirect('/admin/login');
							return;
						}

						$haveController=1;
						break;

					// Base
					case 'base':

						// Check if controller exists
						$controllerPath='base/controller/';
						if (!stream_resolve_include_path($controllerPath.$uriElement.'.php'))
						{
							header("Status: 404 Not Found");
							throw New Exception('HTTP 404: The specified resource '.$this->requestURI.'/'.$uriElement.' was not found.');
						}

						// Init controller
						$this->requestURI.='/'.$uriElement;
						$this->requestController=$uriElement;
						$this->initController($controllerPath.$uriElement.'.php');
						$uriMode=1;

						// Check login
						if ($this->controllerObject->controllerLoginRequired && $this->requestController!='login' && !$LAB->USER->isLoggedIn())
						{
							$LAB->SESSION->set('login.redirect',$_SERVER['SCRIPT_URL']);
							$LAB->RESPONSE->setRedirect('/admin/login');
							return;
						}

						$haveController=1;
						break;

					// Admin
					case 'module':

						// Check module
						if (empty($haveModule))
						{
							$uriElement=mb_strtolower($uriElement);
							if (empty($LAB->CMS->cmsModule[$uriElement]))
							{
								header("Status: 404 Not Found");
								throw New Exception('HTTP 404: The specified resource '.$this->requestURI.'/'.$uriElement.' was not found.');
							}
							$haveModule=$LAB->CMS->cmsModule[$uriElement];
							$this->requestURI.='/'.$uriElement;
							continue;
						}

						// Check if controller exists
						if (!empty($haveModule['module_type']))
						{
							$controllerPath='site/module/'.$haveModule['module_tag'].'/controller/';
						}
						else
						{
							$controllerPath='cms/module/'.$haveModule['module_tag'].'/controller/';
						}
						if (!stream_resolve_include_path($controllerPath.$uriElement.'.php'))
						{
							header("Status: 404 Not Found");
							throw New Exception('HTTP 404: The specified resource '.$this->requestURI.'/'.$uriElement.' was not found.');
						}

						// Init controller
						$this->requestURI.='/'.$uriElement;
						$this->requestController=$uriElement;
						$this->initController($controllerPath.$uriElement.'.php');
						$uriMode=1;

						// Check login
						if ($this->controllerObject->controllerLoginRequired && $this->requestController!='login' && !$LAB->USER->isLoggedIn())
						{
							$LAB->SESSION->set('login.redirect',$_SERVER['SCRIPT_URL']);
							$LAB->RESPONSE->setRedirect('/admin/login');
							return;
						}

						$haveController=1;
						break;

					// CMS
					default:

						// Lang?
						if (empty($this->requestLang))
						{
							if (sizeof($LAB->CMS->cmsLang)>1)
							{
								$LAB->RESPONSE->setRedirect('/'.key($LAB->CMS->cmsLang).'/'.join('/',$originalUriArray));
								return;
							}
							else
							{
								// TODO: Always have lang in URL
								if (1==2)
								{
									$LAB->RESPONSE->setRedirect('/'.key($LAB->CMS->cmsLang).'/'.join('/',$originalUriArray));
									return;
								}
								else
								{
									$this->requestLang=key($LAB->CMS->cmsLang);
								}
							}
						}

						$checkURL=mb_strtolower($structURL.'/'.$uriElement);
						$haveURL=true;
						if (!empty($LAB->CMS->cmsStructByURL[$checkURL]))
						{
							$this->requestStructOID=$LAB->CMS->cmsStructByURL[$checkURL];
						}
						else
						{
							$uriMode=1;
							array_unshift($uriArray,$uriElement);
							continue;
						}

						break;
				}

			}

			// Display resource
			if ($this->requestType=='resource')
			{
				$this->displayResource($resource);
				return;
			}

			// Run controller
			if (in_array($this->requestType,array('base','module','admin')))
			{
				// Check
				if (!$haveController)
				{
					if ($this->requestType=='admin')
					{
						// Init controller
						$this->initController('cms/admin/controller/index.php');
						if ($this->controllerObject->controllerLoginRequired && $this->requestController!='login' && !$LAB->USER->isLoggedIn())
						{
							$LAB->SESSION->set('login.redirect',$_SERVER['SCRIPT_URL']);
							$LAB->RESPONSE->setRedirect('/admin/login');
							return;
						}
					}
					else
					{
						header("Status: 404 Not Found");
						throw new Exception('HTTP 404: Page Not Found.');
					}
				}

				// Set default admin template
				if ($this->requestType=='admin')
				{
					$LAB->RESPONSE->setTemplate('cms/admin/response.cms.admin.default');
				}

				$this->runController();
				return;
			}

			// Display page
			if (!empty($this->requestStructOID))
			{
				$this->displayPage();
			}
			else
			{
				if (!$haveURL)
				{
					$uriArray=array();
					$rootElementOID=CMS_STRUCT::getRootElementOID();
					if (sizeof($LAB->CMS->cmsLang)>1) $uriArray[]=key($LAB->CMS->cmsLang);
					$uriArray[]=$LAB->CMS->cmsStruct[$rootElementOID]['struct_url'];
					$LAB->RESPONSE->setRedirect('/'.join('/',$uriArray));
					return;
				}
				else
				{
					header("Status: 404 Not Found");
					throw new Exception('HTTP 404: Page Not Found.');
				}
			}
		}


		/**
		 *   Display page
		 *   @access public
		 *   @throws Exception
		 *   @return string
		 */

		public function displayPage()
		{
			global $LAB;

			// Init
			$c='';

			// Admin wrapper
			if ($LAB->USER->isLoggedIn() && $LAB->USER->checkRole('cms'))
			{
				$LAB->RESPONSE->addCSS('base/static/css/iconation.css','iconation','9');
				$LAB->RESPONSE->addCSS('cms/static/css/sidebar.css','sidebar','5');
				$c.='<iframe src="/admin/pagesidebar/'.$this->requestStructOID.'/'.$this->requestLang.'" style="display: inline-block; position: absolute; top: 0; left: 0; width: 80px; height: 100%; outline: none; border: none; z-index: 1000000"></iframe>';
				$c.='<div id="admin_wrapper">';
			}

			// Page content
			$c.='<div id="page_content">';

				$c.='foo!';

			// Page content ends
			$c.='</div>';

			// Admin wrapper ends
			if ($LAB->USER->isLoggedIn() && $LAB->USER->checkRole('cms'))
			{
				$c.='</div>';
			}

			// Return
			$LAB->RESPONSE->responseContent=$c;
		}


		/**
		 *   Display resource
		 *   @access public
		 *   @throws Exception
		 *   @return string
		 */

		public function displayResource( $resource )
		{
			global $LAB;
			$LAB->RESPONSE->setType('raw','text/plain');
			$LAB->RESPONSE->responseContent=$resource;
		}

	}


?>