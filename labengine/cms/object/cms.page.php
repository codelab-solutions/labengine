<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   The CMS page data object class
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class CMS_PAGE extends BASE_DATAOBJECT
	{


		/**
		 *   Init data object
		 *   @access public
		 *   @throws Exception
		 *   @return void
		 */

		public function initDataObject()
		{
			// Main fields
			$this->_param['table']            = 'cms_page';
			$this->_param['objectname']       = 'CMS page';
			$this->_param['idfield']          = 'page_oid';
			$this->_param['descriptionfield'] = '';
			$this->_param['setord']           = FALSE;
			$this->_param['prop']             = TRUE;
			$this->_param['modfields']        = TRUE;

			// Relatsioonid
			$this->addRelation(
				'STRUCT',
				'struct_oid',
				'CMS_STRUCT'
			);
		}


		/**
		 *   Define fields and relations for data object
		 *   @access public
		 *   @throws Exception
		 *   @return void
		 */

		public function defineFields()
		{
			global $LAB;
		}


		/**
		 *   Get page by struct
		 *   @access public
		 *   @static
		 *   @param int $struct_oid Struct OID
		 *   @param string $lang Language
		 *   @param bool $throwExceptionOnError Throw exception on error
		 *   @throws Exception
		 *   @return bool|CMS_PAGE
		 */

		public static function getbyStruct( $struct_oid, $lang, $throwExceptionOnError=true )
		{
			global $LAB;

			$page_oid=$LAB->DB->selectFieldSQL("page_oid","SELECT page_oid FROM cms_page WHERE struct_oid=".intval($struct_oid)." and page_lang='".addslashes($lang)."'");
			if (empty($page_oid))
			{
				if ($throwExceptionOnError) throw new Exception('Page not found.');
				return false;
			}
			return CMS_PAGE::getObject($page_oid);
		}


	}


?>