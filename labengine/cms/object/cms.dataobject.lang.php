<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   The CMS language data object class
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class CMS_DATAOBJECT_LANG extends BASE_DATAOBJECT
	{


		/**
		 *   Init data object
		 *   @access public
		 *   @throws Exception
		 *   @return void
		 */

		public function initDataObject()
		{
			// Main fields
			$this->_param['table']            = 'cms_lang';
			$this->_param['objectname']       = 'Language';
			$this->_param['idfield']          = 'lang_oid';
			$this->_param['descriptionfield'] = 'lang_tag';
			$this->_param['setord']           = TRUE;
			$this->_param['prop']             = FALSE;
			$this->_param['modfields']        = TRUE;
		}


		/**
		 *   Define fields and relations for data object
		 *   @access public
		 *   @throws Exception
		 *   @return void
		 */

		public function defineFields()
		{
			global $LAB;

			// Language tag
			$f=$this->addField('lang_tag',new BASE_FIELD_DROPDOWN());
			$f->setTitle('[BASE.COMMON.Email]');
			$f->setMaximumLength(255);
			$f->setUnique(TRUE);
			$f->setRequired('always');
				$languageSet=array();
				foreach (BASE_LOCALE::$localeInfo as $localeTag => $localeInfo)
				{
					$languageSet[$localeTag]=$localeInfo['tag'].' -- '.$localeInfo['name_eng'];
				}
			$f->setSourceList($languageSet);

			// Status
			$f=$this->addField('lang_status',new BASE_FIELD_DROPDOWN());
			$f->setTitle('[BASE.COMMON.Status]');
			$f->setSourceList(array(
				'0' => '[BASE.COMMON.Enabled|tolower]',
				'1' => '[BASE.COMMON.Disabled|tolower]'
			));
		}


	}


?>