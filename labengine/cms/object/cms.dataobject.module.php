<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   The CMS module data object class
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class CMS_DATAOBJECT_MODULE extends BASE_DATAOBJECT
	{


		/**
		 *   Init data object
		 *   @access public
		 *   @throws Exception
		 *   @return void
		 */

		public function initDataObject()
		{
			// Main fields
			$this->_param['table']            = 'cms_module';
			$this->_param['objectname']       = 'Module';
			$this->_param['idfield']          = 'module_oid';
			$this->_param['descriptionfield'] = 'module_tag';
			$this->_param['setord']           = FALSE;
			$this->_param['prop']             = FALSE;
			$this->_param['modfields']        = TRUE;
		}


		/**
		 *   Define fields and relations for data object
		 *   @access public
		 *   @throws Exception
		 *   @return void
		 */

		public function defineFields()
		{
			global $LAB;

			// Language tag
			$f=$this->addField('module_tag',new BASE_FIELD_TEXT());
			$f->setTitle('[BASE.COMMON.Email]');

			// Language tag
			$f=$this->addField('module_name',new BASE_FIELD_TEXT());
			$f->setTitle('[BASE.COMMON.Email]');

			// Language tag
			$f=$this->addField('module_type',new BASE_FIELD_DROPDOWN());
			$f->setTitle('[BASE.COMMON.Email]');
			$f->setSourceList(array(
				'0' => 'system',
				'1' => 'site'
			));

			// Status
			$f=$this->addField('module_status',new BASE_FIELD_DROPDOWN());
			$f->setTitle('[BASE.COMMON.Status]');
			$f->setSourceList(array(
				'0' => '[BASE.COMMON.Enabled|tolower]',
				'1' => '[BASE.COMMON.Disabled|tolower]'
			));
		}


	}


?>