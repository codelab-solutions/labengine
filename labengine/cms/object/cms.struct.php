<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   The CMS structure class
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class CMS_STRUCT extends BASE_DATAOBJECT
	{


		/**
		 *   Init data object
		 *   @access public
		 *   @throws Exception
		 *   @return void
		 */

		public function initDataObject()
		{
			// Main fields
			$this->_param['table']            = 'cms_struct';
			$this->_param['objectname']       = 'CMS page';
			$this->_param['idfield']          = 'struct_oid';
			$this->_param['descriptionfield'] = 'lang_tag';
			$this->_param['setord']           = TRUE;
			$this->_param['prop']             = FALSE;
			$this->_param['modfields']        = TRUE;

			// Relatsioonid
			$this->addRelation(
				'PARENT',
				'parent_oid',
				'CMS_STRUCT',
				'struct_oid',
				TRUE
			);
		}


		/**
		 *   Delete data object
		 *   @param string|boolean $logMessage Log message (NULL for default, FALSE for no log, string for message)
		 *   @param string $logData Log data
		 *   @param int $refOID Log reference OID
		 *   @param string $logOp Log operation
		 *   @return void
		 *   @throws Exception
		 */

		public function delete ( $logMessage=NULL, $logData=NULL, $refOID=NULL, $logOp=NULL )
		{
			global $LAB;

			// TODO: delete substuff
			parent::delete($logMessage,$logData,$refOID,$logOp);
		}


		/**
		 *   Define fields and relations for data object
		 *   @access public
		 *   @throws Exception
		 *   @return void
		 */

		public function defineFields()
		{
			global $LAB;

			// URL tag
			$f=$this->addField('struct_url',new BASE_FIELD_TEXT());
			$f->setTitle('URL tag');
			$f->setMaximumLength(32);
			$f->setUnique(TRUE,'parent_oid=$parent_oid');
			$f->setRequired('always');
			$f->setFilter("/^[A-Za-z0-9\.\_\-]+$/","URL tag can contain characters, letters, or ._-");

			// Description
			$f=$this->addField('struct_description',new BASE_FIELD_TEXT());
			$f->setTitle('Page description');
			$f->setRequired('always');
		}


		/**
		 *   Recalculate full URLs
		 *   @access public
		 *   @static
		 *   @throws Exception
		 *   @return void
		 */

		public static function recalculateURLs( $parent_oid=0, $prefix='' )
		{
			global $LAB;
			try
			{
				// Init
				$rootOID=CMS_STRUCT::getRootElementOID();
				if (empty($parent_oid))
				{
					$LAB->DB->startTransaction();
					$sql="
						SELECT
							*
						FROM
							cms_struct
						ORDER BY
							parent_oid,ord
					";
					$LAB->CMS->cmsStruct=$LAB->DB->querySelectSQL($sql,'struct_oid');
				}

				// Traverse
				foreach ($LAB->CMS->cmsStruct as $row)
				{
					if (intval($row['parent_oid'])!=$parent_oid) continue;
					if (empty($parent_oid) || $parent_oid==$rootOID)
					{
						$struct_url_full='/'.$row['struct_url'];
					}
					else
					{
						$struct_url_full=$prefix.'/'.$row['struct_url'];
					}
					if ($struct_url_full!=$row['$struct_url_full'])
					{
						$LAB->DB->queryUpdateSQL("
							UPDATE
								cms_struct
							SET
								struct_url_full='".addslashes($struct_url_full)."'
							WHERE
								struct_oid=".intval($row['struct_oid'])."
						");
					}
					CMS_STRUCT::recalculateURLs($row['struct_oid'],$struct_url_full);
				}

				// Commit if outermost
				if (empty($parent_oid))
				{
					$LAB->DB->doCommit();
				}
			}
			catch (Exception $e)
			{
				if (empty($parent_oid)) $LAB->DB->doRollback();
				throw $e;
			}
		}


		/**
		 *   Get root element OID
		 *   @access public
		 *   @static
		 *   @throws Exception
		 *   @return void
		 */

		public static function getRootElementOID()
		{
			global $LAB;
			foreach ($LAB->CMS->cmsStruct as $row)
			{
				if (empty($row['parent_oid'])) return $row['struct_oid'];
			}
		}


	}


?>