<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   CMS: Admin: Templates: Page sidebar
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
   *
   */


	// CSS
	$LAB->RESPONSE->addCSS('base/static/bootstrap/css/bootstrap.min.css','bootstrap_ui',4);
	$LAB->RESPONSE->addCSS('cms/static/css/admin.css','admin','5');
	$LAB->RESPONSE->addCSS('base/static/css/iconation.css','iconation','8');


?>
<div id="wrapper">
	<div id="sidebar">
		{content}
	</div>
</div>
