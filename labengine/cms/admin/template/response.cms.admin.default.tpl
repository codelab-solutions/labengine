<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   CMS: Admin: Templates: Admin interface
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
 	 *
	 */


	// CSS
	$LAB->RESPONSE->addCSS('base/static/bootstrap/css/bootstrap.min.css','bootstrap_ui',4);
	$LAB->RESPONSE->addCSS('cms/static/css/admin.css','admin','5');
	$LAB->RESPONSE->addCSS('base/static/css/iconation.css','iconation','8');

	// JavaScript
	$LAB->RESPONSE->JS->addJS('base/static/js/base.js','labengine_base',5);
	$LAB->RESPONSE->JS->addJS('cms/static/js/admin.js','admin','6');
	$LAB->RESPONSE->JS->addLocale($LAB->REQUEST->requestLang);


?>
<div id="admin_wrapper">
	<?

		echo $LAB->CMS->displaySidebar((!empty($LAB->RESPONSE->responseParam['sidebar_struct_oid'])?$LAB->RESPONSE->responseParam['sidebar_struct_oid']:null),(!empty($LAB->RESPONSE->responseParam['sidebar_struct_lang'])?$LAB->RESPONSE->responseParam['sidebar_struct_lang']:null));

	?>
	<div id="content">
		{content}
	</div>
</div>
</div>
