<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   CMS: Admin: Templates: Login
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
   *
   */


	// CSS
	$LAB->RESPONSE->addCSS('base/static/bootstrap/css/bootstrap.min.css','bootstrap_ui',4);
	$LAB->RESPONSE->addCSS('cms/static/css/login.css','login','5');


?>
<div id="wrapper">
	<div id="sidebar">
		<div class="block">
			<img src="/cms/static/gfx/lab-logo.png">
		</div>
		<div class="block active">
			<span class="icon-login"></span>
		</div>
	</div>
	<div id="content">
		<div class="loginwrapper">
			{content}
		</div>
	</div>
</div>
