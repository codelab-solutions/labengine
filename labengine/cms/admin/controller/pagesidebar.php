<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   CMS: Admin Interface: Controllers: Page sidebar
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	// Init controller
	$CMS_ADMIN_PAGESIDEBAR=new BASE_CONTROLLEROBJECT();

	// Parameters
	$CMS_ADMIN_PAGESIDEBAR->setLoginRequired(FALSE);


	//
	//  Actions
	//

	$a=$CMS_ADMIN_PAGESIDEBAR->addAction('pagesidebar');
	$a->setInclude('cms/admin/action/pagesidebar.php');
	$a->setHandler('CMS_ADMIN_PAGESIDEBAR');


?>