<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   CMS: Admin Interface: Controllers: Page
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	// Init controller
	$CMS_ADMIN_PAGE=new BASE_CONTROLLEROBJECT();

	// Parameters
	$CMS_ADMIN_PAGE->setLoginRequired(TRUE);
	$LAB->RESPONSE->responseParam['section']='page';


	//
	//  Page
	//

	$a=$CMS_ADMIN_PAGE->addAction('view');
	$a->setInclude('cms/admin/action/page.view.php');
	$a->setHandler('CMS_ADMIN_PAGE_VIEW');


	//
	//  Settings
	//

	$a=$CMS_ADMIN_PAGE->addAction('settings');
	$a->setInclude('cms/admin/action/page.settings.php');
	$a->setHandler('CMS_ADMIN_PAGE_SETTINGS');


	//
	//  Permissions
	//

	$a=$CMS_ADMIN_PAGE->addAction('permissions');
	$a->setInclude('cms/admin/action/page.permissions.php');
	$a->setHandler('CMS_ADMIN_PAGE_PERMISSIONS');


?>