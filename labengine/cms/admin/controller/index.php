<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   CMS: Admin Interface: Controllers: Index
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	// Init controller
	$CMS_ADMIN_INDEX=new BASE_CONTROLLEROBJECT();


	// Login is required
	$CMS_ADMIN_INDEX->setLoginRequired(TRUE);


	// Default action
	$a=$CMS_ADMIN_INDEX->addAction('index');
	$a->setInclude('cms/admin/action/index.php');
	$a->setHandler('CMS_ADMIN_INDEX');


?>