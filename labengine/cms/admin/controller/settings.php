<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   CMS: Admin Interface: Controllers: Settings
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	// Init controller
	$CMS_ADMIN_SETTINGS=new BASE_CONTROLLEROBJECT();

	// Parameters
	$CMS_ADMIN_SETTINGS->setLoginRequired(TRUE);
	$LAB->RESPONSE->responseParam['section']='settings';


	//
	//  List
	//

	$a=$CMS_ADMIN_SETTINGS->addAction('settings');
	$a->setInclude('cms/admin/action/settings.php');
	$a->setHandler('CMS_ADMIN_SETTINGS');


	//
	//  Site settings
	//

	$a=$CMS_ADMIN_SETTINGS->addAction('sitesettings');
	$a->setInclude('cms/admin/action/settings.sitesettings.php');
	$a->setHandler('CMS_ADMIN_SETTINGS_SITESETTINGS');


	//
	//  Robots.txt
	//

	$a=$CMS_ADMIN_SETTINGS->addAction('robots');
	$a->setInclude('cms/admin/action/settings.robots.php');
	$a->setHandler('CMS_ADMIN_SETTINGS_ROBOTS');


	//
	//  Languages
	//

	$a=$CMS_ADMIN_SETTINGS->addAction('languages');
	$a->setInclude('cms/admin/action/settings.languages.php');
	$a->setHandler('CMS_ADMIN_SETTINGS_LANGUAGES');

	$a=$CMS_ADMIN_SETTINGS->addAction('addlanguage');
	$a->setInclude('cms/admin/action/settings.addlanguage.php');
	$a->setHandler('CMS_ADMIN_SETTINGS_ADDLANGUAGE');

	$a=$CMS_ADMIN_SETTINGS->addAction('togglelanguage');
	$a->setInclude('cms/admin/action/settings.togglelanguage.php');
	$a->setHandler('CMS_ADMIN_SETTINGS_TOGGLELANGUAGE');

	$a=$CMS_ADMIN_SETTINGS->addAction('deletelanguage');
	$a->setInclude('cms/admin/action/settings.deletelanguage.php');
	$a->setHandler('CMS_ADMIN_SETTINGS_DELETELANGUAGE');

	$a=$CMS_ADMIN_SETTINGS->addAction('swaplanguage');
	$a->setInclude('cms/admin/action/settings.swaplanguage.php');
	$a->setHandler('CMS_ADMIN_SETTINGS_SWAPLANGUAGE');


	//
	//  Users
	//

	$a=$CMS_ADMIN_SETTINGS->addAction('modules');
	$a->setInclude('cms/admin/action/settings.modules.php');
	$a->setHandler('CMS_ADMIN_SETTINGS_MODULES');

	$a=$CMS_ADMIN_SETTINGS->addAction('enablemodule');
	$a->setInclude('cms/admin/action/settings.enablemodule.php');
	$a->setHandler('CMS_ADMIN_SETTINGS_ENABLEMODULE');

	$a=$CMS_ADMIN_SETTINGS->addAction('disablemodule');
	$a->setInclude('cms/admin/action/settings.disablemodule.php');
	$a->setHandler('CMS_ADMIN_SETTINGS_DISABLEMODULE');

	$a=$CMS_ADMIN_SETTINGS->addAction('modulesettings');
	$a->setInclude('cms/admin/action/settings.modulesettings.php');
	$a->setHandler('CMS_ADMIN_SETTINGS_MODULESETTINGS');


	//
	//  Users
	//

	$a=$CMS_ADMIN_SETTINGS->addAction('users');
	$a->setInclude('cms/admin/action/settings.users.php');
	$a->setHandler('CMS_ADMIN_SETTINGS_USERS');

	$a=$CMS_ADMIN_SETTINGS->addAction('edituser');
	$a->setInclude('cms/admin/action/settings.edituser.php');
	$a->setHandler('CMS_ADMIN_SETTINGS_EDITUSER');

	$a=$CMS_ADMIN_SETTINGS->addAction('deleteuser');
	$a->setInclude('cms/admin/action/settings.deleteuser.php');
	$a->setHandler('CMS_ADMIN_SETTINGS_DELETEUSER');


?>