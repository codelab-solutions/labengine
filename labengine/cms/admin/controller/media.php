<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   CMS: Admin Interface: Controllers: Media
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	// Init controller
	$CMS_ADMIN_MEDIA=new BASE_CONTROLLEROBJECT();


	// Parameters
	$CMS_ADMIN_MEDIA->setLoginRequired(TRUE);
	$LAB->RESPONSE->responseParam['section']='media';


	// Default action
	$a=$CMS_ADMIN_MEDIA->addAction('media');
	$a->setInclude('cms/admin/action/media.php');
	$a->setHandler('CMS_ADMIN_MEDIA');


?>