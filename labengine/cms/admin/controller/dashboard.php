<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   CMS: Admin Interface: Controllers: Dashboard
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	// Init controller
	$CMS_ADMIN_DASHBOARD=new BASE_CONTROLLEROBJECT();


	// Login is required
	$CMS_ADMIN_DASHBOARD->setLoginRequired(TRUE);


	// Default action
	$a=$CMS_ADMIN_DASHBOARD->addAction('dashboard');
	$a->setInclude('cms/admin/action/dashboard.php');
	$a->setHandler('CMS_ADMIN_DASHBOARD');


?>