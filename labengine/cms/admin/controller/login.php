<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   CMS: Admin Interface: Controllers: Login
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	// Init controller
	$CMS_ADMIN_LOGIN=new BASE_CONTROLLEROBJECT();


	// Login is required
	$CMS_ADMIN_LOGIN->setLoginRequired(FALSE);


	// Default action
	$a=$CMS_ADMIN_LOGIN->addAction('login');
	$a->setInclude('cms/admin/action/login.php');
	$a->setHandler('CMS_ADMIN_LOGIN');


?>