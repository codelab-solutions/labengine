<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   CMS: Admin Interface: Controllers: Structure
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	// Init controller
	$CMS_ADMIN_STRUCT=new BASE_CONTROLLEROBJECT();


	// Parameters
	$CMS_ADMIN_STRUCT->setLoginRequired(TRUE);
	$LAB->RESPONSE->responseParam['section']='struct';


	// Default action
	$a=$CMS_ADMIN_STRUCT->addAction('list');
	$a->setInclude('cms/admin/action/struct.list.php');
	$a->setHandler('CMS_ADMIN_STRUCT');

	// Add sub-page
	$a=$CMS_ADMIN_STRUCT->addAction('addsubpage');
	$a->setInclude('cms/admin/action/struct.addsubpage.php');
	$a->setHandler('CMS_ADMIN_STRUCT_ADDSUBPAGE');

	// Edit page info
	$a=$CMS_ADMIN_STRUCT->addAction('editpage');
	$a->setInclude('cms/admin/action/struct.editpage.php');
	$a->setHandler('CMS_ADMIN_STRUCT_EDITPAGE');

	// Delete page
	$a=$CMS_ADMIN_STRUCT->addAction('deletepage');
	$a->setInclude('cms/admin/action/struct.deletepage.php');
	$a->setHandler('CMS_ADMIN_STRUCT_DELETEPAGE');

	// Swap page order
	$a=$CMS_ADMIN_STRUCT->addAction('swappage');
	$a->setInclude('cms/admin/action/struct.swappage.php');
	$a->setHandler('CMS_ADMIN_STRUCT_SWAPPAGE');


?>