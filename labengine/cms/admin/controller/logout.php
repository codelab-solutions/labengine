<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   CMS: Admin Interface: Controllers: Logout
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	// Init controller
	$CMS_ADMIN_LOGOUT=new BASE_CONTROLLEROBJECT();


	// Login is required
	$CMS_ADMIN_LOGOUT->setLoginRequired(FALSE);


	// Default action
	$a=$CMS_ADMIN_LOGOUT->addAction('logout');
	$a->setInclude('cms/admin/action/logout.php');
	$a->setHandler('CMS_ADMIN_LOGOUT');


?>