<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   CMS: Admin Interface: Actions: Users: List
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class CMS_ADMIN_SETTINGS_USERS extends BASE_LIST
	{


		/**
		 *   Init list
		 *   @return void
		 */

		public function initList()
		{
			global $LAB;

			// Data
			$this->data=new BASE_USER();

			// Params
			$this->setTemplate('cms');
			$this->setListClass('table');
			$this->setTitle('Users');
			$this->setBaseURL('/admin/settings/users');
			$this->setReturnLink('/admin/settings');
		}


		/**
		 *   Define columns
		 *   @return void
		 */

		public function initColumns()
		{
			$f=$this->addField('user_email');
			$f->setTitle('E-mail');
			$f->setListFieldWidth('30%');

			$f=$this->addField('user_name');
			$f->setTitle('Name');
			$f->setListFieldWidth('50%');

			$f=$this->addField('user_status');
			$f->setTitle('Status');
			$f->setListFieldWidth('20%');
			$f->setListFieldAlign('center');
		}


		/**
		 *   Define actions
		 *   @return void
		 */

		public function initActions()
		{
			$a=$this->addGlobalAction('add');
			$a->setTitle('<span class="icon-add">Add a new user</span>');
			$a->setJSAction("BASE.openEditDialog('/admin/settings/edituser')");

			$a=$this->addAction('edit');
			$a->setTitle('<span class="icon-edit" data-toggle="tooltip" data-placement="top" title="Edit user"></span>');
			$a->setJSAction("BASE.openEditDialog('/admin/settings/edituser',{user_oid:\$user_oid})");

			$a=$this->addAction('delete');
			$a->setTitle('<span class="icon-delete" data-toggle="tooltip" data-placement="top" title="Delete user"></span>');
			$a->setURL('/admin/settings/deleteuser');
		}

	}


	$CMS_ADMIN_SETTINGS_USERS=new CMS_ADMIN_SETTINGS_USERS();


?>