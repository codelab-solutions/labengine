<?


	/**
	 *
	 *   LabEngine™ 7
	 *   CMS: Admin Interface: Actions: Structure: Add a subpage
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class CMS_ADMIN_STRUCT_ADDSUBPAGE  extends BASE_DIALOGFORM
	{


		public function initForm()
		{
			$this->data=new CMS_STRUCT();
			parent::initForm();

			$this->setURL('/admin/struct/addsubpage');
			$this->setReload(true);

			$this->setTitle('Add a subpage');
		}


		public function initFields()
		{
			global $LAB;

			// Parent
			$f=$this->addField('parent_oid',new BASE_FIELD_HIDDEN());
			$f->setDefault($_POST['parent_oid']);

			// Põhiväljad
			$this->addField('struct_url');
			$this->addField('struct_description');
		}


		public function dataSave()
		{
			global $LAB;
			$e=parent::dataSave();
			if (!empty($e)) return $e;
			CMS_STRUCT::recalculateURLs();
		}


	}


	$CMS_ADMIN_STRUCT_ADDSUBPAGE =new CMS_ADMIN_STRUCT_ADDSUBPAGE();


?>