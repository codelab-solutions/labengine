<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   CMS: Admin Interface: Actions: Modules: List
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class CMS_ADMIN_SETTINGS_MODULES extends BASE_LIST
	{


		/**
		 *   Init list
		 *   @return void
		 */

		public function initList()
		{
			global $LAB;

			// Data
			$this->data=new CMS_MODULE();
			$loadParam=new BASE_DATAOBJECT_PARAM();
			$loadParam->addColumns('module_tag');
			$this->setLoadParam($loadParam);

			// Params
			$this->setTemplate('cms');
			$this->setListClass('table');
			$this->setShowResultCount(false);
			$this->setTitle('[CMS.ADMIN.MODULES.Title]');
			$this->setBaseURL('/admin/settings/modules');
			$this->setReturnLink('/admin/settings');
		}


		/**
		 *   Define columns
		 *   @return void
		 */

		public function initColumns()
		{
			$f=$this->addField('module_tag');
			$f->setTitle('ID');
			$f->setListFieldWidth('20%');

			$f=$this->addField('module_name');
			$f->setTitle('Name');
			$f->setListFieldWidth('40%');

			$f=$this->addField('module_type');
			$f->setTitle('Type');
			$f->setListFieldWidth('20%');
			$f->setListFieldAlign('center');

			$f=$this->addField('module_status');
			$f->setListFieldWidth('20%');
			$f->setListFieldAlign('center');
		}


		/**
		 *   Define actions
		 *   @return void
		 */

		public function initActions()
		{
			$a=$this->addAction('toggle', new CMS_ADMIN_SETTINGS_MODULES_ACTION_TOGGLE());
			$a->setJSAction("BASE.openEditDialog('/admin/settings/togglemodule',{module_tag:'\$module_tag'})");

			$a=$this->addAction('settings');
			$a->setTitle('<span class="icon-settings" data-toggle="tooltip" data-placement="top" title="Settings"></span>');
			$a->setJSAction("BASE.openEditDialog('/admin/settings/modulesettings',{module_tag:'\$module_tag'})");
		}

	}


	class CMS_ADMIN_SETTINGS_MODULES_ACTION_TOGGLE extends BASE_LIST_ACTION
	{

		/**
		 *   Return title
		 *   @return string
		 */

		public function getTitle( &$row=NULL )
		{
			if (!empty($row['module_status']))
			{
				return '<span class="icon-switch-0" data-toggle="tooltip" data-placement="top" title="Enable module"></span>';
			}
			else
			{
				return '<span class="icon-switch-1" data-toggle="tooltip" data-placement="top" title="Disable module"></span>';
			}
		}

	}


	$CMS_ADMIN_SETTINGS_MODULES=new CMS_ADMIN_SETTINGS_MODULES();


?>