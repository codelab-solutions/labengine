<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   CMS: Admin Interface: Actions: Index
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class CMS_ADMIN_INDEX extends BASE_DISPLAYOBJECT
	{
		function display()
		{
			global $LAB;
			$LAB->RESPONSE->setRedirect('/admin/dashboard');
		}
	}


	$CMS_ADMIN_INDEX=new CMS_ADMIN_INDEX();


?>