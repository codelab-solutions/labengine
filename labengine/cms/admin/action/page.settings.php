<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   CMS: Admin Interface: Actions: Page: Settings
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class CMS_ADMIN_PAGE_SETTINGS extends BASE_DISPLAYOBJECT
	{


		function display()
		{
			global $LAB;

			// Check params
			try
			{
				// Validate
				$struct_oid=$LAB->REQUEST->requestVar['var_2']['name'];
				if (mb_strlen($struct_oid)<8 || !preg_match("/^[0-9]+$/",$struct_oid)) throw new Exception('Invalid parameters: struct_oid');
				$struct_lang=$LAB->REQUEST->requestVar['var_3']['name'];
				if (mb_strlen($struct_lang)<2 || !preg_match("/^[A-Za-z\_]+$/",$struct_lang)) throw new Exception('Invalid parameters: struct_lang');

				// Load
				$PAGE=CMS_PAGE::getbyStruct($struct_oid,$struct_lang,false);
				if (!is_object($PAGE))
				{
					$PAGE=new CMS_PAGE();
					$PAGE->struct_oid=$struct_oid;
					$PAGE->page_lang=$struct_lang;
				}

				// Set
				$LAB->RESPONSE->responseParam['sidebar_struct_oid']=$struct_oid;
				$LAB->RESPONSE->responseParam['sidebar_struct_lang']=$struct_lang;
				$LAB->RESPONSE->responseParam['subsection']='settings';
			}
			catch (Exception $e)
			{
				$LAB->RESPONSE->setRedirect('/admin');
				return;
			}

			$c='<h1>Page settings</h1>';
			$c.='<div class="contentwrapper">';

			$c.='</div>';
			return $c;
		}


	}


	$CMS_ADMIN_PAGE_SETTINGS=new CMS_ADMIN_PAGE_SETTINGS();


?>