<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   CMS: Admin Interface: Actions: Languages: Swap language order
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	$CMS_ADMIN_SETTINGS_SWAPLANGUAGE=new BASE_ACTION_SWAP();
	$CMS_ADMIN_SETTINGS_SWAPLANGUAGE->data=new CMS_DATAOBJECT_LANG();


?>