<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   CMS: Admin Interface: Actions: Login
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class CMS_ADMIN_LOGIN extends BASE_DISPLAYOBJECT
	{


		/**
		 *   Run the displayobject and return contents
		 *   @throws Exception
		 *   @return string
		 */

		function display()
		{
			global $LAB;

			// Already logged in?
			if ($LAB->USER->isLoggedIn() && $LAB->USER->checkRole('cms_admin'))
			{
				$LAB->RESPONSE->setRedirect('/');
				return;
			}

			// Set template
			$LAB->RESPONSE->setTemplate('cms/admin/response.cms.admin.login');

			// Do login?
			BASE::sanitizeInput();
			if (!empty($_POST['user_username']) || !empty($_POST['user_password']))
			{
				try
				{
					$loginSuccess=$LAB->USER->doLogin($_POST['user_username'],$_POST['user_password']);
					$LAB->RESPONSE->setRedirect('/');
				}
				catch (Exception $e)
				{
					$ERROR=$e->getMessage();
				}
			}

			// Form
			$c='
				<div class="login">
					<form role="form" method="post" action="/admin/login">
						<div class="form-group">
							<label for="user_username">E-mail:</label>
							<input type="email" class="form-control" id="user_username" name="user_username">
						</div>
						<div class="form-group">
							<label for="exampleInputPassword1">Password:</label>
							<input type="password" class="form-control" id="user_password" name="user_password">
						</div>
						<button type="submit" class="btn btn-default">Login</button>
					</form>
				</div>
				<script language="JavaScript">
					$(function(){
						$("#user_username").focus();
					});
				</script>
			';

			// Return contents
			return $c;
		}


	}


	$CMS_ADMIN_LOGIN=new CMS_ADMIN_LOGIN();


?>