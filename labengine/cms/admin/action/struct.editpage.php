<?


	/**
	 *
	 *   LabEngine™ 7
	 *   CMS: Admin Interface: Actions: Structure: Edit page
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class CMS_ADMIN_STRUCT_EDITPAGE  extends BASE_DIALOGFORM
	{


		public function initForm()
		{
			$this->data=new CMS_STRUCT();
			parent::initForm();

			$this->setURL('/admin/struct/editpage');
			$this->setReload(true);

			$this->setTitle('Edit structure item');
		}


		public function initFields()
		{
			global $LAB;

			// Parent
			$f=$this->addField('parent_oid',new BASE_FIELD_HIDDEN());

			// Põhiväljad
			$this->addField('struct_url');
			$this->addField('struct_description');
		}


		public function dataSave()
		{
			global $LAB;
			$e=parent::dataSave();
			if (!empty($e)) return $e;
			CMS_STRUCT::recalculateURLs();
		}


	}


	$CMS_ADMIN_STRUCT_EDITPAGE =new CMS_ADMIN_STRUCT_EDITPAGE();


?>