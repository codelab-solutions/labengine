<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   CMS: Admin Interface: Actions: Page sidebar
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class CMS_ADMIN_PAGESIDEBAR extends BASE_DISPLAYOBJECT
	{
		function display()
		{
			global $LAB;

			// Check
			if (!$LAB->USER->isLoggedIn())
			{
				$LAB->RESPONSE->setType('raw','text/html');
				return '';
			}

			// Data
			$struct_oid=(!empty($LAB->REQUEST->requestVar['var_1']['name'])?$LAB->REQUEST->requestVar['var_1']['name']:null);
			$lang=(!empty($LAB->REQUEST->requestVar['var_2']['name'])?$LAB->REQUEST->requestVar['var_2']['name']:null);

			$LAB->RESPONSE->setTemplate('cms/admin/response.cms.admin.pagesidebar');
			return $LAB->CMS->displaySidebar($struct_oid,$lang);
		}
	}


	$CMS_ADMIN_PAGESIDEBAR=new CMS_ADMIN_PAGESIDEBAR();


?>