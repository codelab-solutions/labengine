<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   CMS: Admin Interface: Actions: Dashboard
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class CMS_ADMIN_SETTINGS extends BASE_DISPLAYOBJECT
	{
		function display()
		{
			global $LAB;

			$c='<h1>Settings</h1>';
			$c.='<div class="contentwrapper settings-list">';

				$c.='<div class="row">';
				$c.='<div class="col-md-6">';

					$c.='<p style="margin-top: 5px"><a href="/admin/settings/sitesettings"><span class="icon-settings">Main website settings</span></a>';
					$c.='<p style="margin-top: 15px"><a href="/admin/settings/languages"><span class="icon-language">Languages</span></a>';
					$c.='<p style="margin-top: 15px"><a href="/admin/settings/robots"><span class="icon-robots">Edit robots.txt</span></a>';

				$c.='</div>';
				$c.='<div class="col-md-6">';

					$c.='<p style="margin-top: 5px"><a href="/admin/settings/modules"><span class="icon-module">Modules</span></a>';
					$c.='<p style="margin-top: 15px"><a href="/admin/settings/users"><span class="icon-user">Users</span></a>';

				$c.='</div>';
				$c.='</div>';

			return $c;
		}
	}


	$CMS_ADMIN_SETTINGS=new CMS_ADMIN_SETTINGS();


?>