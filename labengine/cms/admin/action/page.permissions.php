<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   CMS: Admin Interface: Actions: Page: Permissions
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class CMS_ADMIN_PAGE_PERMISSIONS extends BASE_DISPLAYOBJECT
	{


		function display()
		{
			global $LAB;

			// Check params
			try
			{
				// Validate
				$struct_oid=$LAB->REQUEST->requestVar['var_2']['name'];
				if (mb_strlen($struct_oid)<8 || !preg_match("/^[0-9]+$/",$struct_oid)) throw new Exception('Invalid parameters: struct_oid');
				$struct_lang=$LAB->REQUEST->requestVar['var_3']['name'];
				if (mb_strlen($struct_lang)<2 || !preg_match("/^[A-Za-z\_]+$/",$struct_lang)) throw new Exception('Invalid parameters: struct_lang');

				// Load

				// Set
				$LAB->RESPONSE->responseParam['sidebar_struct_oid']=$struct_oid;
				$LAB->RESPONSE->responseParam['sidebar_struct_lang']=$struct_lang;
				$LAB->RESPONSE->responseParam['subsection']='permissions';
			}
			catch (Exception $e)
			{
				$LAB->RESPONSE->setRedirect('/admin');
				return;
			}

			$c='<h1>Page permissions</h1>';
			$c.='<div class="contentwrapper">';

			$c.='</div>';
			return $c;
		}


	}


	$CMS_ADMIN_PAGE_PERMISSIONS=new CMS_ADMIN_PAGE_PERMISSIONS();


?>