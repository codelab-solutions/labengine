<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   CMS: Admin Interface: Actions: Languages: List
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class CMS_ADMIN_SETTINGS_TOGGLELANGUAGE extends BASE_AJAXACTION
	{


		public function handleAction()
		{
			global $LAB;
			try
			{
				// Check
				BASE::sanitizeInput();
				if (mb_strlen($_POST['lang'])<2) throw new Exception('[BASE.COMMON.ErrorCheckingData]');

				// Load
				$lang_oid=$LAB->DB->selectFieldSQL("lang_oid","SELECT * FROM cms_lang WHERE lang_tag='".addslashes($_POST['lang'])."'");
				if (empty($lang_oid)) throw new Exception('[BASE.COMMON.ErrorCheckingData]');
				$LANG=CMS_DATAOBJECT_LANG::getObject($lang_oid);

				// Check
				if (empty($LANG->lang_status))
				{
					$dataset=$LAB->DB->querySelectSQL("SELECT lang_oid FROM cms_lang WHERE lang_status=0");
					if (sizeof($dataset)<2) throw new Exception('[CMS.ADMIN.LANGUAGES.Error.AtLeastOneActive]');
				}

				// Toggle
				$LANG->lang_status=(!empty($LANG->lang_status)?'0':'1');
				$LANG->save(BASE_DATAOBJECT_PARAM::columns('lang_status'));

				// Success
				$response=array();
				$response['status']='1';
				$response['reload']='1';
				return $response;
			}
			catch (Exception $e)
			{
				$response=array();
				$response['status']='2';
				$response['error']=$e->getMessage();
				return $response;
			}
		}

		
	}


	$CMS_ADMIN_SETTINGS_TOGGLELANGUAGE=new CMS_ADMIN_SETTINGS_TOGGLELANGUAGE();


?>