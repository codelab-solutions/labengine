<?


	class CMS_ADMIN_SETTINGS_EDITUSER  extends BASE_DIALOGFORM
	{


		public function initForm()
		{
			$this->data=new BASE_USER();
			parent::initForm();

			$this->setURL('/admin/settings/edituser');
			$this->setReload(true);

			$this->setAddTitle('Add a new user');
			$this->setEditTitle('Edit user');
		}


		public function initFields()
		{
			global $LAB;

			// Põhiväljad
			$this->addField('user_email');
			$this->addField('user_name');
			$this->addField('user_password');
			$this->addField('user_status');

			if ($LAB->USER->checkRole('cms_supervisor'))
			{
				$f=$this->addField('roleset',new BASE_FIELD_CHECKLIST());
				$f->setTitle('Roles');
				$f->setItemsPerRow(2);
				$f->setSourceList(BASE_USER::getRoleList());
				$f->setNoSave(true);
			}
		}


		public function dataLoad()
		{
			parent::dataLoad();
			$this->data->roleset=$this->data->_in['roleset']=$this->data->getRoles();
		}


		public function dataSave()
		{
			global $LAB;
			try
			{
				$LAB->DB->startTransaction();
				parent::dataSave();
				$this->data->setRoles($_POST['roleset']);
				$LAB->DB->doCommit();
			}
			catch (Exception $e)
			{
				$LAB->DB->doRollback();
				throw $e;
			}
		}


	}


	$CMS_ADMIN_SETTINGS_EDITUSER =new CMS_ADMIN_SETTINGS_EDITUSER ();


?>