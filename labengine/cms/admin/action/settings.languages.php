<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   CMS: Admin Interface: Actions: Languages: List
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class CMS_ADMIN_SETTINGS_LANGUAGES extends BASE_LIST
	{


		/**
		 *   Init list
		 *   @return void
		 */

		public function initList()
		{
			global $LAB;

			// Data
			$this->data=new CMS_DATAOBJECT_LANG();

			// Params
			$this->setTemplate('cms');
			$this->setListClass('table');
			$this->setShowResultCount(false);
			$this->setTitle('[CMS.ADMIN.LANGUAGES.Title]');
			$this->setBaseURL('/admin/settings/languages');
			$this->setReturnLink('/admin/settings');
		}


		/**
		 *   Define columns
		 *   @return void
		 */

		public function initColumns()
		{
			$f=$this->addField('lang_tag',new CMS_ADMIN_SETTINGS_LANGUAGES_FIELD_TAG());
			$f->setTitle('[CMS.ADMIN.LANGUAGES.Fld.Language]');
			$f->setListFieldWidth('70%');

			$f=$this->addField('lang_status');
			$f->setTitle('[BASE.COMMON.Status]');
			$f->setListFieldWidth('30%');
			$f->setListFieldAlign('center');
		}


		/**
		 *   Define actions
		 *   @return void
		 */

		public function initActions()
		{
			$a=$this->addGlobalAction('add');
			$a->setTitle('<span class="icon-add">[CMS.ADMIN.LANGUAGES.Action.AddLanguage]</span>');
			$a->setJSAction("BASE.openEditDialog('/admin/settings/addlanguage')");

			$a=$this->addAction('toggle', new CMS_ADMIN_SETTINGS_LANGUAGES_ACTION_TOGGLE());

			$a=$this->addAction('delete');
			$a->setTitle('<span class="icon-delete" data-toggle="tooltip" data-placement="top" title="[CMS.ADMIN.LANGUAGES.Action.DeleteLanguage]"></span>');
			$a->setURL('/admin/settings/deletelanguage');

			$a=$this->addAction('swap');
			$a->setURL('/admin/settings/swaplanguage');
		}

	}


	class CMS_ADMIN_SETTINGS_LANGUAGES_FIELD_TAG extends BASE_FIELD
	{

		/**
		 *   Return value
		 *   @return string
		 */

		public function listValue( $value, &$row )
		{
			return '<div class="lang-tag '.(!empty($row['lang_status'])?'disabled':'active').'">'.mb_strtoupper($value).'</div> '.BASE_LOCALE::$localeInfo[$value]['name_eng'];
		}

	}


	class CMS_ADMIN_SETTINGS_LANGUAGES_ACTION_TOGGLE extends BASE_LIST_ACTION
	{

		/**
		 *   Return title
		 *   @return string
		 */

		public function getTitle( &$row=NULL )
		{
			if (!empty($row['lang_status']))
			{
				return '<span class="icon-switch-0" data-toggle="tooltip" data-placement="top" title="[CMS.ADMIN.LANGUAGES.Action.EnableLanguage]"></span>';
			}
			else
			{
				return '<span class="icon-switch-1" data-toggle="tooltip" data-placement="top" title="[CMS.ADMIN.LANGUAGES.Action.DisableLanguage]"></span>';
			}
		}

		/**
		 *   Return action
		 *   @return string
		 */

		public function getJSAction( &$row=NULL )
		{
			if (!empty($row['lang_status']))
			{
				return "BASE.doAjaxAction('/admin/settings/togglelanguage',{lang:'".htmlspecialchars($row["lang_tag"])."'})";
			}
			else
			{
				return "BASE.doAjaxAction('/admin/settings/togglelanguage',{lang:'".htmlspecialchars($row["lang_tag"])."'},'Disable language?')";
			}
		}

	}


	$CMS_ADMIN_SETTINGS_LANGUAGES=new CMS_ADMIN_SETTINGS_LANGUAGES();


?>