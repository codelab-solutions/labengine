<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   CMS: Admin Interface: Actions: Structure: List
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class CMS_ADMIN_STRUCT_X extends BASE_DISPLAYOBJECT
	{


		/**
		 *   Display content
		 *   @return string
		 */

		public function display()
		{
			global $LAB;

			$c='<h1>[CMS.ADMIN.STRUCT.Title]</h1>';
			$c.='<div class="contentwrapper struct-list">';

				// Table begins
				$c.='<div class="list"><table class="table">';

					// Header
					$c.='<thead>';
					$c.='<th style="width: 80%">[CMS.COMMON.Page]</th>';
					$c.='<th style="width: 19%" class="languages">[CMS.ADMIN.LANGUAGES.Title]</th>';
					$c.='<th style="width: 1%" class="rowactions"></th>';
					$c.='</thead>';

					// Content
					$c.='<tbody>';
					$c.=$this->displayStruct(0,0);
					$c.='</tbody>';

				// Table ends
				$c.='</table></div>';

			$c.='</div>';

			return $c;
		}


		/**
		 *   Recursive function to display
		 *   @return string
		 */

		public function displayStruct( $parent_oid, $depth )
		{
			global $LAB;

			// Traverse
			foreach ($LAB->CMS->cmsStruct as $struct)
			{
				// Skip those not in this tree
				if ($struct['parent_oid']!=$parent_oid) continue;

				// Show self
				$c='<tr>';
				$c.='<td>';
					$c.=$struct['struct_url'];
					if (!empty($struct['struct_description'])) $c.=' <span style="color: #b0b0b0"> &raquo; '.htmlspecialchars($struct['struct_description']).'</span>';
				$c.='</td>';
				$c.='<td class="languages">';
					foreach ($LAB->CMS->cmsLang as $lang)
					{
						$c.='<a href="/admin/page/'.$struct['struct_oid'].'/'.mb_strtolower($lang).'"><div class="lang active" data-toggle="tooltip" data-placement="top" title="[CMS.ADMIN.STRUCT.Action.Content]: '.BASE_LOCALE::$localeInfo[$lang]['name_eng'].'">'.mb_strtoupper($lang).'</div></a>';
					}
				$c.='</td>';
				$c.='<td class="rowactions">';
					$c.='<ul class="rowactions">';
					$c.='<li class="rowaction"><a onclick="BASE.openEditDialog(\'/admin/struct/addsubpage\',{parent_oid:'.$struct['struct_oid'].'})"><span class="icon-addsub"></span></a></li>';
					$c.='<li class="rowaction"><a onclick="BASE.openEditDialog(\'/admin/struct/editpage\',{struct_oid:'.$struct['struct_oid'].'})"><span class="icon-edit" data-toggle="tooltip" data-placement="top" title="[CMS.ADMIN.STRUCT.Action.Edit]"></span></a></li>';
					$c.='<li class="rowaction"><a onclick="BASE.doAjaxAction(\'/admin/struct/deletepage\',{struct_oid:'.$struct['struct_oid'].'},\'[CMS.ADMIN.STRUCT.Action.Delete.Confirm]\')"><span class="icon-delete"></span></a></li>';
					$c.='<li class="rowaction disabled"><span class="icon-up" data-toggle="tooltip" data-placement="top" title="[CMS.ADMIN.STRUCT.Action.MoveUp]"></span></li>';
					$c.='<li class="rowaction disabled"><span class="icon-dn" data-toggle="tooltip" data-placement="top" title="[CMS.ADMIN.STRUCT.Action.MoveDn]"></span></li>';
					$c.='</ul>';
				$c.='</td>';
				$c.='</tr>';

				// Show children
				$c.=$this->displayStruct($struct['struct_oid'],($depth+1));

				// Return
				return $c;
			}

		}

	}


	class CMS_ADMIN_STRUCT extends BASE_LIST
	{


		/**
		 *   Init list
		 *   @return void
		 */

		public function initList()
		{
			global $LAB;

			// Data
			$this->data=new CMS_STRUCT();
			$loadParam=new BASE_DATAOBJECT_PARAM();
			$loadParam->addColumns('struct_description');
			$this->setLoadParam($loadParam);

			// Params
			$this->setTemplate('cms');
			$this->setListClass('table');
			$this->setShowResultCount(false);
			$this->setTitle('[CMS.ADMIN.STRUCT.Title]');
			$this->setBaseURL('/admin/struct');
			$this->setNested(true);
		}


		/**
		 *   Define columns
		 *   @return void
		 */

		public function initColumns()
		{
			$f=$this->addField('struct_url', new CMS_ADMIN_STRUCT_FIELD_URL());
			$f->setTitle('[CMS.COMMON.Page]');
			$f->setListFieldWidth('70%');

			$f=$this->addField('struct_oid', new CMS_ADMIN_STRUCT_FIELD_LANGUAGES());
			$f->setTitle('[CMS.ADMIN.LANGUAGES.Title]');
			$f->setListFieldClass('languages');
			$f->setListFieldWidth('30%');
		}


		/**
		 *   Define actions
		 *   @return void
		 */

		public function initActions()
		{
			$a=$this->addAction('addsubpage');
			$a->setTitle('<span class="icon-addsub" data-toggle="tooltip" data-placement="top" title="[CMS.ADMIN.STRUCT.Action.AddSubpage]"></span>');
			$a->setJSAction("BASE.openEditDialog('/admin/struct/addsubpage',{parent_oid:\$struct_oid})");

			$a=$this->addAction('edit');
			$a->setTitle('<span class="icon-edit" data-toggle="tooltip" data-placement="top" title="[CMS.ADMIN.STRUCT.Action.Edit]"></span>');
			$a->setJSAction("BASE.openEditDialog('/admin/struct/editpage',{struct_oid:\$struct_oid})");

			$a=$this->addAction('delete');
			$a->setTitle('<span class="icon-delete" data-toggle="tooltip" data-placement="top" title="[CMS.ADMIN.STRUCT.Action.Delete]"></span>');
			$a->setDisabledTitle('<span class="icon-delete disabled"></span>');
			$a->setURL('/admin/struct/deletepage');
			$a->setConfirm('[CMS.ADMIN.STRUCT.Action.Delete.Confirm]');

			$a=$this->addAction('swap');
			$a->setURL('/admin/struct/swappage');
		}

	}


	class CMS_ADMIN_STRUCT_FIELD_URL extends BASE_FIELD
	{

		public function listValue( $value, &$row )
		{
			$c=$row['struct_url'];
			if (!empty($row['struct_description'])) $c.=' <span style="color: #b0b0b0"> &raquo; '.htmlspecialchars($row['struct_description']).'</span>';
			return $c;
		}

	}


	class CMS_ADMIN_STRUCT_FIELD_LANGUAGES extends BASE_FIELD
	{

		public function listValue( $value, &$row )
		{
			global $LAB;
			$c='';
			foreach ($LAB->CMS->cmsLang as $lang)
			{
				$c.='<a href="/admin/page/'.$row['struct_oid'].'/'.mb_strtolower($lang).'"><div class="lang-tag active" data-toggle="tooltip" data-placement="top" title="[CMS.ADMIN.STRUCT.Action.Content]: '.BASE_LOCALE::$localeInfo[$lang]['name_eng'].'">'.mb_strtoupper($lang).'</div></a>';
			}
			return $c;
		}

	}


	class CMS_ADMIN_SETTINGS_LANGUAGES_ACTION_TOGGLE extends BASE_LIST_ACTION
	{

		/**
		 *   Return title
		 *   @return string
		 */

		public function getTitle( &$row=NULL )
		{
			if (!empty($row['lang_status']))
			{
				return '<span class="icon-switch-0" data-toggle="tooltip" data-placement="top" title="Enable language"></span>';
			}
			else
			{
				return '<span class="icon-switch-1" data-toggle="tooltip" data-placement="top" title="Disable language"></span>';
			}
		}

	}



	$CMS_ADMIN_STRUCT=new CMS_ADMIN_STRUCT();


?>