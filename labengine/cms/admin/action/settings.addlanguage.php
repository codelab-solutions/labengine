<?


	/**
	 *
	 *   LabEngine™ 7
	 *   CMS: Admin Interface: Actions: Languages: Add a language
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class CMS_ADMIN_SETTINGS_ADDLANGUAGE  extends BASE_DIALOGFORM
	{


		public function initForm()
		{
			$this->data=new CMS_DATAOBJECT_LANG();
			parent::initForm();

			$this->setURL('/admin/settings/addlanguage');
			$this->setReload(true);

			$this->setTitle('[CMS.ADMIN.LANGUAGES.Action.AddLanguage]');
		}


		public function initFields()
		{
			global $LAB;

			// Language
			$languageSet=array();
			foreach (BASE_LOCALE::$localeInfo as $localeTag => $localeInfo)
			{
				if (!empty($LAB->CMS->cmsLang[$localeTag])) continue;
				$languageSet[$localeTag]=$localeInfo['tag'].' -- '.$localeInfo['name_eng'];
			}
			if (!sizeof($languageSet)) throw new Exception('[CMS.ADMIN.LANGUAGES.Error.NoLanguages]');

			$f=$this->addField('lang_tag',new BASE_FIELD_DROPDOWN());
			$f->setTitle('[CMS.ADMIN.LANGUAGES.Fld.Language]');
			$f->setSourceList($languageSet);
			$f->setRequired('always');
			$f->setSelect('[BASE.FORM.Select]');
		}


	}


	$CMS_ADMIN_SETTINGS_ADDLANGUAGE =new CMS_ADMIN_SETTINGS_ADDLANGUAGE();


?>