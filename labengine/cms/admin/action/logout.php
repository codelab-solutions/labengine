<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   CMS: Admin Interface: Actions: Logout
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class CMS_ADMIN_LOGOUT extends BASE_DISPLAYOBJECT
	{
		function display()
		{
			global $LAB;
			$LAB->USER->doLogout();
			$LAB->RESPONSE->setRedirect('/');
		}
	}


	$CMS_ADMIN_LOGOUT=new CMS_ADMIN_LOGOUT();


?>