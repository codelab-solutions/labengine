<?


	/**
	 *
	 *   LabEngine™ 7
	 *   Standard actions: chooser search
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_ACTION_CHOOSERSEARCH extends BASE_AJAXACTION
	{


		/**
		 *  Parameters
		 *  @var string
		 *  @access public
		 */
		public $param = array();


		/**
		 *  Set minimum search term length
		 *  @access public
		 *  @param int $minimumLength Minimum length
		 *  @return void
		 */

		public function setMinimumLength( $minimumLength )
		{
			$this->param['search_minimumlength']=$minimumLength;
		}


		/**
		 *  Set maximum number of results
		 *  @access public
		 *  @param int $maxResults Max results
		 *  @return void
		 */

		public function setMaxResults( $maxResults )
		{
			$this->param['search_maxresults']=$maxResults;
		}


		/**
		 *   Load data
		 *   @access public
		 *   @param string $search Search term
		 *   @param array $data Additional data
		 *   @return array
		 *   @throws Exception
		 */

		public function dataLoad( $search, $data=array() )
		{
			// This should be implemented in the subclass
			throw new Exception('Function dataLoad() should be implemented in the subclass.');
		}


		/**
		 *   Display results
		 *   @access public
		 *   @param array $dataset Results
		 *   @return string
		 *   @throws Exception
		 */

		public function displaySearchResults( $dataset )
		{
			// This should be implemented in the subclass
			throw new Exception('Function displaySearchResults() should be implemented in the subclass.');
		}


		/**
		 *  Swap
		 *  @access public
		 *  @return array
		 */

		public function handleAction()
		{
			global $LAB;

			// Init
			BASE::sanitizeInput();
			$response=array();
			$response['status']='1';

			try
			{
				// Check
				$searchMinimumLength=oneof($this->param['search_minimumlength'],3);
				$search=trim($_POST['search']);
				if (!mb_strlen($search)) throw new Exception('[BASE.CHOOSER.Error.Empty]');
				if (mb_strlen($search)<$searchMinimumLength) throw new Exception(sprintf($LAB->LOCALE->get('BASE.CHOOSER.Error.MinimumLength'),$searchMinimumLength));

				// Search
				$dataset=$this->dataLoad($search,$_POST);

				// Display results
				if (sizeof($dataset))
				{
					$c=$this->displaySearchResults($dataset);

					$maxResults=oneof($this->param['search_maxresults'],10);
					if (sizeof($dataset)==($maxResults+1))
					{
						$c.='<p>'.sprintf($LAB->LOCALE->get('BASE.CHOOSER.Results.TooManyResults'),$maxResults).'</p>';
					}
					else
					{
						$c.='<p>'.sprintf($LAB->LOCALE->get('BASE.CHOOSER.Results.ResultCount'),'<b>'.sizeof($dataset).'</b>').'</p>';
					}
					$response['content']=$c;
				}
				else
				{
					$response['content']='No results found.';
				}

				return $response;
			}
			catch (Exception $e)
			{
				$response['content']='<p style="color: #920000">'.htmlspecialchars($e->getMessage()).'</p>';
			}

			return $response;
		}


	}


?>