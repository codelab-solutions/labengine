<?


	/**
	 *
	 *   LabEngine™ 7
	 *   Standard actions: swap items
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_ACTION_SWAP extends BASE_AJAXACTION
	{


		/**
		 *   Init delete action
		 *   @access public
		 *   @return void
		 *   @throws Exception
		 */

		public function initSwap()
		{
			// This can be implemented in the subclass.
		}


		/**
		 *  Swap
		 *  @access public
		 *  @return array
		 */

		public function handleAction()
		{
			$response=array('status'=>'2');
			try
			{
				// Init self
				$this->initSwap();

				// Check
				if (!intval($_POST['swap1']) || !intval($_POST['swap2'])) throw new Exception('[BASE.COMMON.ErrorCheckingData]');

				// Do swap
				$this->data->swap(intval($_POST['swap1']),intval($_POST['swap2']));

				// Success
				$response['status']='1';
				if (!empty($this->param['url_redirect']))
				{
					$response['redirect']=$this->param['url_redirect'];
				}
				else
				{
					$response['reload']='1';
				}
				return $response;
			}
			catch (Exception $e)
			{
				$response['error']=$e->getMessage();
				return $response;
			}
		}


	}


?>