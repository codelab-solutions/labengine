<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   The FOP wrapper class
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_FOP
	{


		/**
		 *   Generate PDF from XML+XSL
		 *   @access public
		 *   @static
		 *   @var string $XML XML (as string)
		 *   @var string $XSL Path to XSL file
		 *   @var bool|string Base path (path or FALSE = autodetect from XSL)
		 *   @var bool|string $outputFile Output PDF as file (FALSE = no, filename = yes)
		 *   @return string
		 *   @throws Exception
		 */

		public static function generatePDF( $XML, $XSL, $basePath=FALSE, $outputFile=FALSE, $returnPDF=TRUE )
		{
			global $LAB;

			// Check FOP path
			$fopBasePath=$LAB->CONFIG->get('fop.path');
			if (!mb_strlen($fopBasePath)) throw new Exception('FOP path not set in the configuration.');

			// Check XSL
			if (!stream_resolve_include_path($XSL)) throw new Exception('XSL file '.($LAB->DEBUG->devEnvironment?'('.$XSL.')':'').' not readable');
			if ($basePath===false)
			{
				$pathInfo=pathinfo(stream_resolve_include_path($XSL));
				$basePath=$pathInfo['dirname'];
			}

			// Lokaliseeritud stringid XML-is
			$XML=BASE_TEMPLATE::parseContent($XML,TRUE,FALSE);

			// Base filename
			$fnBase=BASE::getTmpDir().'/'.md5(uniqid()).'.'.time();

			// Write XML file
			file_put_contents($fnBase.'.xml',$XML);

			// Configuration
			$xconf = '<?xml version="1.0"?>
				<fop version="1.0">
					<base>.</base>
					<source-resolution>72</source-resolution>
					<target-resolution>72</target-resolution>
					<default-page-settings height="29.7cm" width="21cm"/>
					<renderers>
						<renderer mime="application/pdf">
							<filterList>
								<value>flate</value>
							</filterList>
							<fonts>
								<directory recursive="true">'.$fopBasePath.'</directory>
								<directory recursive="true">'.$basePath.'</directory>
							</fonts>
						</renderer>
						<renderer mime="application/vnd.hp-PCL">
						</renderer>
						<renderer mime="image/svg+xml">
							<format type="paginated"/>
							<link value="true"/>
							<strokeText value="false"/>
						</renderer>
						<renderer mime="application/awt">
						</renderer>
						<renderer mime="image/png">
						</renderer>
						<renderer mime="image/tiff">
						</renderer>
						<renderer mime="text/xml">
						</renderer>
						<renderer mime="text/plain">
							<pageSize columns="80"/>
						</renderer>
					</renderers>
				</fop>
			';
			file_put_contents($fnBase.'.xconf',$xconf);

			// Generate FOP command-line
			$fopCWD=$basePath;
			$fopCmd=$fopBasePath.'/fop'
				.' -xml '.$fnBase.'.xml'
				.' -xsl '.$XSL
				.' -pdf '.(!empty($outputFile)?$outputFile:$fnBase.'.pdf')
				.' -c '.$fnBase.'.xconf';

			// Run FOP
			exec_with_cwd($fopCmd,$fopCWD,$errors);

			//  Check output
			if (!file_exists(!empty($outputFile)?$outputFile:$fnBase.'.pdf'))
			{
				unlink($fnBase.'.xml');
				unlink($fnBase.'.xconf');
				if ($LAB->CONFIG->get('DEBUG.dev'))
				{
					$debugFile=$fnBase.'.foperror';
					$debugContent="Path: ".$fopCWD."\n";
					$debugContent.="Cmd: ".$fopCmd."\n\n";
					$debugContent.="--- 8< ---\n\n";
					$debugContent.="Output:\n\n";
					$debugContent.=$errors."\n\n";
					$debugContent.="--- 8< ---\n\n";
					$debugContent.="XML:\n\n";
					$debugContent.=$XML."\n\n";
					$debugContent.="--- 8< ---\n\n";
					$debugContent.="XSL:\n\n";
					$debugContent.=file_get_contents($XSL)."\n";
					file_put_contents($debugFile,$debugContent);
					throw new Exception('Error generating PDF. Details in: '.$debugFile);
				}
				else
				{
					throw new Exception('Error generating PDF.');
				}
			}
			if ($returnPDF) $pdf=file_get_contents(!empty($outputFile)?$outputFile:$fnBase.'.pdf');

			// Clean up
			unlink($fnBase.'.xml');
			unlink($fnBase.'.xconf');
			if (empty($outputFile)) unlink($fnBase.'.pdf');

			// iHaz Success
			if ($returnPDF) return $pdf;
			return '';
		}


		/**
		 *   Generate RTF from XML+XSL
		 *   @access public
		 *   @static
		 *   @var string $XML XML (as string)
		 *   @var string $XSL Path to XSL file
		 *   @var bool|string Base path (path or FALSE = autodetect from XSL)
		 *   @var bool|string $outputFile Output PDF as file (FALSE = no, filename = yes)
		 *   @return string
		 *   @throws Exception
		 */

		public static function generateRTF( $XML, $XSL, $basePath=FALSE, $outputFile=FALSE )
		{
			global $LAB;

			// Check FOP path
			$fopBasePath=$LAB->CONFIG->get('fop.path');
			if (!mb_strlen($fopBasePath)) throw new Exception('FOP path not set in the configuration.');

			// Check XSL
			if (!stream_resolve_include_path($XSL)) throw new Exception('XSL file not readable');
			if ($basePath===false)
			{
				$pathInfo=pathinfo(stream_resolve_include_path($XSL));
				$basePath=$pathInfo['dirname'];
			}

			// Lokaliseeritud stringid XML-is
			$XML=BASE_TEMPLATE::parseContent($XML,TRUE,FALSE);

			// Base filename
			$fnBase=BASE::getTmpDir().'/'.md5(uniqid()).'.'.time();

			// Write XML file
			file_put_contents($fnBase.'.xml',$XML);

			// Configuration
			$xconf = '<?xml version="1.0"?>
				<fop version="1.0">
					<base>.</base>
					<source-resolution>72</source-resolution>
					<target-resolution>72</target-resolution>
					<default-page-settings height="29.7cm" width="21cm"/>
					<renderers>
						<renderer mime="application/x-rtf">
							<filterList>
								<value>flate</value>
							</filterList>
							<fonts>
								<directory recursive="true">'.$fopBasePath.'</directory>
								<directory recursive="true">'.$basePath.'</directory>
							</fonts>
						</renderer>
						<renderer mime="application/vnd.hp-PCL">
						</renderer>
						<renderer mime="image/svg+xml">
							<format type="paginated"/>
							<link value="true"/>
							<strokeText value="false"/>
						</renderer>
						<renderer mime="application/awt">
						</renderer>
						<renderer mime="image/png">
						</renderer>
						<renderer mime="image/tiff">
						</renderer>
						<renderer mime="text/xml">
						</renderer>
						<renderer mime="text/plain">
							<pageSize columns="80"/>
						</renderer>
					</renderers>
				</fop>
			';
			file_put_contents($fnBase.'.xconf',$xconf);

			// Generate FOP command-line
			$fopCWD=$basePath;
			$fopCmd=$fopBasePath.'/fop'
				.' -xml '.$fnBase.'.xml'
				.' -xsl '.$XSL
				.' -rtf '.(!empty($outputFile)?$outputFile:$fnBase.'.rtf')
				.' -c '.$fnBase.'.xconf';

			// Run FOP
			exec_with_cwd($fopCmd,$fopCWD,$errors);

			//  Check output
			if (!file_exists(!empty($outputFile)?$outputFile:$fnBase.'.rtf'))
			{
				unlink($fnBase.'.xml');
				unlink($fnBase.'.xconf');
				if ($LAB->CONFIG->get('DEBUG.dev'))
				{
					$debugFile=$fnBase.'.foperror';
					$debugContent="Path: ".$fopCWD."\n";
					$debugContent.="Cmd: ".$fopCmd."\n\n";
					$debugContent.="--- 8< ---\n\n";
					$debugContent.="Output:\n\n";
					$debugContent.=$errors."\n\n";
					$debugContent.="--- 8< ---\n\n";
					$debugContent.="XML:\n\n";
					$debugContent.=$XML."\n\n";
					$debugContent.="--- 8< ---\n\n";
					$debugContent.="XSL:\n\n";
					$debugContent.=file_get_contents($XSL)."\n";
					file_put_contents($debugFile,$debugContent);
					throw new Exception('Error generating RTF. Details in: '.$debugFile);
				}
				else
				{
					throw new Exception('Error generating RTF.');
				}
			}
			$rtf=file_get_contents(!empty($outputFile)?$outputFile:$fnBase.'.rtf');

			// Clean up
			unlink($fnBase.'.xml');
			unlink($fnBase.'.xconf');
			if (empty($outputFile)) unlink($fnBase.'.rtf');

			// iHaz Success
			return $rtf;
		}

	}


?>