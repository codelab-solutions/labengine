<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   The PHPMailer wrapper class
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	// Include PHPMailer
	include_once 'base/contrib/PHPMailer/class.phpmailer.php';
	include_once 'base/contrib/PHPMailer/class.smtp.php';


	class BASE_PHPMAILER extends PHPMailer
	{


		/**
		 *   Constructor wrapper
		 *   @param bool $exceptions Throw exceptions (in LabEngine it's always true)
		 *   @return BASE_PHPMAILER
		 */

		public function __construct($exceptions = false)
		{
			global $LAB;

			// We throw exceptions in LabEngine
			$this->exceptions = true;

			// UTF8
			$this->CharSet  = "UTF-8";

			// SMTP
			if ($LAB->CONFIG->get('smtp.host'))
			{
				ini_set('openssl.cafile',stream_resolve_include_path('base/data/cacert/cacert.pem'));
				$this->IsSMTP();
				$this->Host = $LAB->CONFIG->get('smtp.host');
				if ($LAB->CONFIG->get('smtp.auth'))
				{
					$this->SMTPAuth = true;
					$this->Username = $LAB->CONFIG->get('smtp.username');
					$this->Password = $LAB->CONFIG->get('smtp.password');
				}
			}
			if ($LAB->CONFIG->get('smtp.port'))
			{
				$this->Port = $LAB->CONFIG->get('smtp.port');
			}
		}


		/**
		 *   Add recipient
		 *   @param string $address E-mail address
		 *   @param string $name Name (optional)
		 *   @return void
		 */

		public function AddAddress ( $address, $name='' )
		{
			global $LAB;
			if ($LAB->CONFIG->get('debug.dev'))
			{
				parent::AddAddress(oneof($LAB->CONFIG->get('debug.email'),'indrek.siitan@codelab.ee'),$name);
			}
			else
			{
				$recipients=str_array($address,',;');
				foreach ($recipients as $email) parent::AddAddress($email,$name);
			}
		}


		/**
		 *   Add BCC recipient
		 *   @param string $address E-mail address
		 *   @param string $name Name (optional)
		 *   @return void
		 */

		public function AddBCC ( $address, $name='' )
		{
			global $LAB;
			if ($LAB->CONFIG->get('debug.dev'))
			{
				parent::AddAddress(oneof($LAB->CONFIG->get('debug.email'),'indrek.siitan@codelab.ee'),$name);
			}
			else
			{
				$recipients=str_array($address,',;');
				foreach ($recipients as $email) parent::AddBCC($email,$name);
			}
		}


		/**
		 *   Set message subject
		 *   @param string $subject Subject
		 *   @return void
		 */

		public function setSubject( $subject )
		{
			$this->Subject=$subject;
		}


	}


?>