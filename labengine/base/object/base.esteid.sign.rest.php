<?php


	/**
	 *
	 *   FlaskPHP-Identity-EstEID
	 *   ------------------------
	 *   Digital signing provider for ID card
	 *
	 *   @author   Codelab Solutions OÜ <codelab@codelab.ee>
	 *   @license  https://www.flaskphp.com/LICENSE MIT
	 *
	 */


	class BASE_ESTEID_SIGN_REST
	{

		/**
		 *   Dev mode?
		 *   @var bool
		 *   @access public
		 */

		public $devMode=false;


		/**
		 *   Service language
		 *   @var string
		 *   @access public
		 */

		public $serviceLanguage='EST';


		/**
		 *   JAVA class path
		 *   @var string
		 *   @access public
		 */

		private $javaClassPath='.:./specs-validation-report-5.5.d4j.1.jar:./bcpkix-jdk15on-1.62.jar:./bcprov-jdk15on-1.62.jar:./commons-cli-1.4.jar:./commons-codec-1.12.jar:./commons-collections4-4.3.jar:./commons-io-2.6.jar:./commons-lang3-3.9.jar:./commons-logging-1.2.jar:./contiperf-2.4.3.jar:./ddoc4j-4.0.3.jar:./digidoc4j-4.0.3.jar:./dss-asic-cades-5.5.d4j.1.jar:./dss-asic-common-5.5.d4j.1.jar:./dss-asic-xades-5.5.d4j.1.jar:./dss-cades-5.5.d4j.1.jar:./dss-crl-parser-5.5.d4j.1.jar:./dss-crl-parser-stream-5.5.d4j.1.jar:./dss-crl-parser-x509crl-5.5.d4j.1.jar:./dss-detailed-report-jaxb-5.5.d4j.1.jar:./dss-diagnostic-jaxb-5.5.d4j.1.jar:./dss-document-5.5.d4j.1.jar:./dss-enumerations-5.5.d4j.1.jar:./dss-jaxb-parsers-5.5.d4j.1.jar:./dss-model-5.5.d4j.1.jar:./dss-pades-5.5.d4j.1.jar:./dss-pades-pdfbox-5.5.d4j.1.jar:./dss-policy-jaxb-5.5.d4j.1.jar:./dss-service-5.5.d4j.1.jar:./dss-simple-report-jaxb-5.5.d4j.1.jar:./dss-spi-5.5.d4j.1.jar:./dss-token-5.5.d4j.1.jar:./dss-tsl-validation-5.5.d4j.1.jar:./dss-utils-5.5.d4j.1.jar:./dss-utils-apache-commons-5.5.d4j.1.jar:./dss-xades-5.5.d4j.1.jar:./hamcrest-core-2.1.jar:./hamcrest-library-2.1.jar:./httpclient-4.5.8.jar:./httpcore-4.4.11.jar:./httpcore-4.4.13.jar:./jackson-annotations-2.10.1.jar:./jackson-core-2.10.1.jar:./jackson-databind-2.10.1.jar:./javax.activation-1.2.0.jar:./jaxb-api-2.3.0.jar:./jaxb-core-2.3.0.1.jar:./jaxb-core-2.3.0.jar:./jaxb-impl-2.3.0.jar:./jaxb-impl-2.3.2.jar:./jaxb-runtime-2.3.2.jar:./jcabi-matchers-1.4.jar:./json-20180813.jar:./jsonassert-1.5.0.jar:./junit-4.12.jar:./junit-toolbox-1.11.jar:./logback-classic-1.2.3.jar:./logback-core-1.2.3.jar:./mockito-core-2.28.2.jar:./slf4j-api-1.7.25.jar:./slf4j-api-1.7.26.jar:./slf4j-api-1.7.28.jar:./snakeyaml-1.24.jar:./specs-trusted-list-5.5.d4j.1.jar:./specs-xades-5.5.d4j.1.jar:./specs-xmldsig-5.5.d4j.1.jar:./spring-boot-1.5.8.RELEASE.jar:./spring-context-4.3.12.RELEASE.jar:./system-rules-1.19.0.jar:./validation-policy-5.5.d4j.1.jar:./wiremock-2.25.1.jar:./xmlsec-2.1.4.jar:./xmlunit-1.6.jar';


		/**
		 *
		 *   Constructor
		 *   -----------
		 *   @access public
		 *   @param bool $devMode
		 *   @throws Exception
		 *   @return Sign
		 *
		 */

		public function __construct( bool $devMode=null )
		{
			global $LAB;

			// Add locale
//			$LAB->LOCALE->addLocalePath(__DIR__ . '/../locale');

			if ($devMode !== null)
			{
				$this->devMode=$devMode;
			}
			else
			{
				$this->devMode=$LAB->CONFIG->get('debug.dev');
			}

			// Language
			if ($LAB->CONFIG->get('identity.esteid.language'))
			{
				$this->serviceLanguage=$LAB->CONFIG->get('identity.esteid.language');
			}
		}


		/**
		 *
		 *   Start signing session
		 *   ---------------------
		 *   @access public
		 *   @param string $container
		 *   @throws Exception
		 *   @return SessionStartResponse
		 *
		 */

		public function startSession( string $container ): BASE_ESTEID_SIGN_RESTRESPONSE
		{
			global $LAB;

			$uniqueName=uniqid();
			$filename=$LAB->CONFIG->get('site.tmppath') . '/' . $uniqueName . '.txt';
			$digestHashFile=$LAB->CONFIG->get('site.tmppath') . '/' . $uniqueName . '.digest.txt';
			$dataToSignFile=$LAB->CONFIG->get('site.tmppath') . '/' . $uniqueName . '.datatosign.bin';
			$signerCertificateFile=$LAB->CONFIG->get('site.tmppath') . '/' . $uniqueName . '.signerCert.txt';
			$BDOCFile=$LAB->CONFIG->get('site.tmppath') . '/' . $uniqueName . '.bdoc';

			try
			{
				file_put_contents($BDOCFile, $container);
				file_put_contents($filename, '');
				file_put_contents($dataToSignFile, '');
				file_put_contents($digestHashFile, '');
				file_put_contents($signerCertificateFile, '');

				$LAB->SESSION->set('eid.temp.output.file', $filename);
				$LAB->SESSION->set('eid.temp.output.name', $uniqueName);
				$LAB->SESSION->set('eid.temp.output.data', $dataToSignFile);
				$LAB->SESSION->set('eid.temp.output.digest', $digestHashFile);
				$LAB->SESSION->set('eid.temp.output.signerCert', $signerCertificateFile);

			}
			catch (Exception $e)
			{
				throw new Exception('Session start failed');
			}

			$response=new BASE_ESTEID_SIGN_RESTRESPONSE();
			$response
				->setFilename($filename)
				->setDataToSignFile($dataToSignFile)
				->setDigestHashFile($digestHashFile)
				->setSignerCertificateFile($signerCertificateFile)
				->setBdocFileName($uniqueName);

			return $response;
		}


		/**
		 *
		 *   Prepare signature
		 *   -----------------
		 *   @access public
		 *   @param string $signerCertificate Signer's certificate
		 *   @throws Exception|Exception
		 *   @return PrepareResponse
		 *
		 */

		public function prepareSignature( string $signerCertificate )
		{
			global $LAB;

			$filename=$LAB->SESSION->get('eid.temp.output.file');
			$uniqueName=$LAB->SESSION->get('eid.temp.output.name');
			$dataToSignFile=$LAB->SESSION->get('eid.temp.output.data');
			$digestHashFile=$LAB->SESSION->get('eid.temp.output.digest');
			$signerCertificateFile=$LAB->SESSION->get('eid.temp.output.signerCert');

			file_put_contents($filename, '');
			file_put_contents($dataToSignFile, '');
			file_put_contents($digestHashFile, '');
			file_put_contents($signerCertificateFile, '');

			if ($LAB->CONFIG->get('debug.dev'))
			{
				$environment='TEST';
			}
			else
			{
				$environment='PROD';
			}

			file_put_contents($signerCertificateFile, $signerCertificate);

			$BDOCFile=$LAB->CONFIG->get('site.tmppath') . '/' . $uniqueName . '.bdoc';

			$cmd='java -classpath ' . $this->javaClassPath . ' ' .
				'GenerateHash '
				. $signerCertificate . ' '
				. $BDOCFile . ' '
				. $filename . ' '
				. $dataToSignFile . ' '
				. $digestHashFile . ' '
				. $environment . ' '
				. '>/dev/null 2>&1 &';
			$errors='';

			exec_with_cwd($cmd, realpath(__DIR__.'/digisign-java/'), $errors);

			for ($t=0;$t<=10;$t++)
			{
				sleep(1);
				if ($file=fopen($filename, "r"))
				{
					while (!feof($file))
					{
						$line=fgets($file);
						if (strpos($line, '[error]') !== false || strpos($line, 'Exception in thread "main"') !== false)
						{
							$exception=trim(str_replace('[error] ', '', $line));

							// Notify
							$this->notifyDeveloper(
								'Hash generation failed: '.$exception,
								'Java cmd: '.$cmd."\n\nOutput:\n".$errors,
								[
									$filename,
									$BDOCFile,
									$dataToSignFile,
									$digestHashFile,
									$signerCertificateFile
								]
							);

							// Delete temp files
							/*
							file_exists($filename) && unlink($filename);
							file_exists($BDOCFile) && unlink($BDOCFile);
							file_exists($dataToSignFile) && unlink($dataToSignFile);
							file_exists($digestHashFile) && unlink($digestHashFile);
							file_exists($signerCertificateFile) && unlink($signerCertificateFile);
							*/

							// Throw exception
							throw new Exception($exception);
						}
						elseif (strpos($line, '[dataToSignInHex]') !== false)
						{
							$dataToSignInHex=trim(str_replace('[dataToSignInHex] ', '', $line));
							if ($dataToSignInHex)
							{
								$digest=new BASE_ESTEID_SIGN_REST_PREPARERESPONSE;
								$digest->setHex(trim($dataToSignInHex));
								return $digest;
							}
						}
					}
				}
			}

			// Notify
			$this->notifyDeveloper(
				'Hash generation failed',
				'Java cmd: '.$cmd."\n\nOutput:\n".$errors,
				[
					$filename,
					$BDOCFile,
					$dataToSignFile,
					$digestHashFile,
					$signerCertificateFile
				]
			);

			// Delete temp files
			/*
			file_exists($filename) && unlink($filename);
			file_exists($BDOCFile) && unlink($BDOCFile);
			file_exists($dataToSignFile) && unlink($dataToSignFile);
			file_exists($digestHashFile) && unlink($digestHashFile);
			file_exists($signerCertificateFile) && unlink($signerCertificateFile);
			*/

			// Throw exception
			throw new Exception('Hash generation failed!');
		}


		/**
		 *
		 *   Finalize signature
		 *   ------------------
		 *   @access public
		 *   @param string $signatureHex Signer's signature
		 *   @throws Exception
		 *   @return SignResponse
		 *
		 */

		public function finalizeSignature( string $signatureHex )
		{
			global $LAB;

			$filename=$LAB->SESSION->get('eid.temp.output.file');
			$uniqueName=$LAB->SESSION->get('eid.temp.output.name');
			$dataToSignFile=$LAB->SESSION->get('eid.temp.output.data');
			$digestHashFile=$LAB->SESSION->get('eid.temp.output.digest');
			$signerCertificateFile=$LAB->SESSION->get('eid.temp.output.signerCert');

			if ($LAB->CONFIG->get('debug.dev'))
			{
				$environment='TEST';
			}
			else
			{
				$environment='PROD';
			}

			$signerCertificate=file_get_contents($signerCertificateFile);

			$BDOCFile=$LAB->CONFIG->get('site.tmppath') . '/' . $uniqueName . '.bdoc';

			$cmd='java -classpath ' . $this->javaClassPath . ' ' .
				'Sign '
				. $signerCertificate . ' '
				. $signatureHex . ' '
				. $BDOCFile . ' '
				. $filename . ' '
				. $dataToSignFile . ' '
				. $digestHashFile . ' '
				. $environment . ' '
				. '>/dev/null 2>&1 &';

			$errors='';

			exec_with_cwd($cmd, realpath(__DIR__.'/digisign-java/'), $errors);

			for ($t=0; $t <= 10; $t++)
			{
				sleep(1);

				if ($file=fopen($filename, "r"))
				{
					while (!feof($file))
					{
						$line=fgets($file);
						if (strpos($line, '[error]') !== false || strpos($line, 'Exception in thread "main"') !== false)
						{
							$exception=trim(str_replace('[error] ', '', $line));

							// Notify
							$this->notifyDeveloper(
								'Signing failed: '.$exception,
								'Java cmd: '.$cmd."\n\nOutput:\n".$errors,
								[
									$filename,
									$BDOCFile,
									$dataToSignFile,
									$digestHashFile,
									$signerCertificateFile
								]
							);

							// Delete temp file
							file_exists($filename) && unlink($filename);
							file_exists($dataToSignFile) && unlink($dataToSignFile);
							file_exists($digestHashFile) && unlink($digestHashFile);
							file_exists($signerCertificateFile) && unlink($signerCertificateFile);
							file_exists($BDOCFile) && unlink($BDOCFile);

							// Clear session
							$LAB->SESSION->set('eid.temp.output.file', '');
							$LAB->SESSION->set('eid.temp.output.name', '');
							$LAB->SESSION->set('eid.temp.output.data', '');
							$LAB->SESSION->set('eid.temp.output.digest', '');
							$LAB->SESSION->set('eid.temp.output.signerCert', '');

							// Throw exception
							throw new Exception($exception);
						}
						elseif (strpos($line, '[containerFile]') !== false)
						{
							$bDocFile=trim(str_replace('[containerFile] ', '', $line));
							if ($bDocFile)
							{

								// Delete temp file
								file_exists($filename) && unlink($filename);
								file_exists($dataToSignFile) && unlink($dataToSignFile);
								file_exists($digestHashFile) && unlink($digestHashFile);
								file_exists($signerCertificateFile) && unlink($signerCertificateFile);

								$response=new BASE_ESTEID_SIGNRESPONSE();
								$response->status='success';
								$response->signedDocInfo=file_get_contents($bDocFile);

								// Cleanup
								file_exists($bDocFile) && unlink($bDocFile);
								file_exists($BDOCFile) && unlink($BDOCFile);
								$LAB->SESSION->set('eid.temp.output.file', '');
								$LAB->SESSION->set('eid.temp.output.name', '');
								$LAB->SESSION->set('eid.temp.output.data', '');
								$LAB->SESSION->set('eid.temp.output.digest', '');
								$LAB->SESSION->set('eid.temp.output.signerCert', '');

								return $response;
							}
						}
					}
				}
			}

			// Notify
			$this->notifyDeveloper(
				'Signing failed',
				'Java cmd: '.$cmd."\n\nOutput:\n".$errors,
				[
					$filename,
					$BDOCFile,
					$dataToSignFile,
					$digestHashFile,
					$signerCertificateFile
				]
			);

			// Delete temp file
			file_exists($filename) && unlink($filename);
			file_exists($BDOCFile) && unlink($BDOCFile);
			file_exists($dataToSignFile) && unlink($dataToSignFile);
			file_exists($digestHashFile) && unlink($digestHashFile);
			file_exists($signerCertificateFile) && unlink($signerCertificateFile);

			throw new Exception('Signing failed!');
		}


		/**
		 *
		 *   Get signed document
		 *   -------------------
		 *   @access public
		 *   @throws Exception
		 *   @return string
		 *
		 */

		public function getSignedDoc()
		{
			global $LAB;

			$uniqueName=$LAB->SESSION->get('eid.temp.output.name');
			$BDOCFile=$LAB->CONFIG->get('site.tmppath') . '/' . $uniqueName . '.bdoc';

			try
			{
				return file_get_contents($BDOCFile);
			}
			catch (\SoapFault $soapFault)
			{
				throw new Exception('Error getting signed doc.');
			}
		}


		/**
		 *
		 *   Notify developer
		 *   ----------------
		 *   @access public
		 *   @param string $error
		 *   @param array $attachments
		 *   @return void
		 *
		 */

		public function notifyDeveloper( string $error, string $info='', array $attachments=[] )
		{
			global $LAB;

			try
			{
				// Check
				if (!mb_strlen($LAB->CONFIG->get('debug.email'))) return;

				// Send
				$Mailer=new KRM_MAILER();
				$Mailer->setSubject("Signing error using EstEID: ".$error);
				$Mailer->setContent("Signing error using EstEID: ".$error."\n\nAdditional info:\n\n".$info);
				$Mailer->addAddress($LAB->CONFIG->get('debug.email'));
				foreach ($attachments as $a)
				{
					if (!file_exists($a)) continue;
					$p=pathinfo($a);
					$Mailer->addAttachment(file_get_contents($a),$p['filename']);
				}
				$Mailer->send();
			}
			catch (\Exception $e)
			{
			}
		}


	}

?>
