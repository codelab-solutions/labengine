<?php


	/**
	 *
	 *   FlaskPHP-Identity-EstEID
	 *   ------------------------
	 *   EstEID authentication response
	 *
	 *   @author   Codelab Solutions OÜ <codelab@codelab.ee>
	 *   @license  https://www.flaskphp.com/LICENSE MIT
	 *
	 */


	class BASE_ESTEID_AUTHENTICATERESPONSE
	{


		/**
		 *   Full CN
		 *   @var string
		 *   @access public
		 */

		public $CN = null;


		/**
		 *   First name
		 *   @var string
		 *   @access public
		 */

		public $firstName = null;


		/**
		 *   Last name
		 *   @var string
		 *   @access public
		 */

		public $lastName = null;


		/**
		 *   ID code
		 *   @var string
		 *   @access public
		 */

		public $idCode = null;


		/**
		 *
		 *   Get CN
		 *   ------
		 *   @access public
		 *   return string
		 *
		 */

		public function getCN()
		{
			return $this->CN;
		}


		/**
		 *
		 *   Get first name
		 *   --------------
		 *   @access public
		 *   return string
		 *
		 */

		public function getFirstName()
		{
			return $this->firstName;
		}


		/**
		 *
		 *   Get last name
		 *   -------------
		 *   @access public
		 *   return string
		 *
		 */

		public function getLastName()
		{
			return $this->lastName;
		}


		/**
		 *
		 *   Get ID code
		 *   -----------
		 *   @access public
		 *   return string
		 *
		 */

		public function getIDCode()
		{
			return $this->idCode;
		}


	}


?>