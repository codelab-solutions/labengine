<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   The MS SQL db class
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_DB_MSSQL extends BASE_DB
	{


		/**
		 *   Database handle
		 *   @var object
		 *   @access public
		 */

		public $DBH = FALSE;


		/**
		 *   Database connection properties
		 *   @var array
		 *   @access public
		 */

		public $dbConnection = array();


		/**
		 *   Last DB error occurred
		 *   @var string
		 *   @access public
		 */

		public $dbError = '';


		/**
		 *   Transaction level
		 *   @var int
		 *   @access public
		 */

		public $transactionLevel = 0;


		/**
		 *   Found rows during last query
		 *   @var int
		 *   @access public
		 */

		public $foundRows = 0;



		/**
		 *   Init datasource connection. Does not do a real connect
		 *   @access public
		 *   @param array connection parameters
		 *   @return boolean TRUE if succeeded, FALSE if failed
		 */

		public function connect( $param=array() )
		{
			global $LAB;

			// Get connection parameters
			$this->dbConnection['hostname']=oneof($param['hostname'],$LAB->CONFIG->get('db.hostname'));
			$this->dbConnection['database']=oneof($param['database'],$LAB->CONFIG->get('db.database'));
			$this->dbConnection['username']=oneof($param['username'],$LAB->CONFIG->get('db.username'));
			$this->dbConnection['password']=oneof($param['password'],$LAB->CONFIG->get('db.password'));
			$this->dbConnection['port']=oneof($param['port'],$LAB->CONFIG->get('db.port'),'1433');

			// Return
			return TRUE;
		}


		/**
		 *   Connect to the datasource
		 *   @access public
		 *   @throws Exception
		 *   @return void
		 */

		public function realConnect()
		{
			global $LAB;

			// Return if already connected
			if ($this->DBH) return;

			// Connect
			try
			{
				$this->DBH=new PDO("dblib:host=".$this->dbConnection['hostname'].":".$this->dbConnection['port'].";dbname=".$this->dbConnection['database'],$this->dbConnection['username'],$this->dbConnection['password']);
			}
			catch (PDOException $e)
			{
				throw new Exception('Could not connect to database: '.$e->getMessage());
				return FALSE;
			}

			// Set to throw exceptions
			$this->DBH->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

			// Return successfully
			return TRUE;
		}


		/**
		 *   Disconnect from the datasource
		 *   @access public
		 *   @return void
		 */

		public function disconnect()
		{
			// PDO has no disconnect() method at this point.
		}



		/**
		 *  Add profiler info
		 *  @access public
		 *  @param string $sql SQL
		 *  @param int $queryTime Query time
		 *  @param int $rows Rows affected
		 *  @param boolean $explain Run EXPLAIN?
		 *  @return void
		 */

		function addProfilerInfo ( $sql, $queryTime, $rows=0, $explain=FALSE )
		{
		}


		/**
		 *  Column value
		 *  @access public
		 *  @param string Input value
		 *  @return string Output value
		 */

		function colValue ( $value )
		{
			if (!strcasecmp($value,'NULL'))
				return $value;
			else if (mb_strtolower($value)=="now()")
				return $value;
			else
				return "'".addslashes($value)."'";
		}


		/**
		 *   Build SQL
		 *   @access public
		 *   @param BASE_QUERY $queryObject Query object
		 *   @throws Exception
		 *   @return string SQL statement
		 */

		function buildSQL ( $queryObject )
		{
			// Build query
			$sql=$queryObject->queryType." ";

			// Found rows
			if ($queryObject->calcFoundRows)
			{
				$sql.="SQL_CALC_FOUND_ROWS ";
			}

			// Columns
			if (!empty($queryObject->queryColumns))
			{
				$sql.=implode(", ",$queryObject->queryColumns);
			}
			elseif ($queryObject->queryType=='SELECT')
			{
				$sql.='*';
			}

			// Tables
			if (!empty($queryObject->queryTable))
			{
				$c=0;
				foreach ($queryObject->queryTable as $table)
				{
					if (!$c)
					{
						$sql.=" FROM ".$table;
					}
					else
					{
						$sql.=" ".$table;
					}
					$c++;
				}
			}
			else
			{
				throw new Exception('No table specified.');
				return;
			}

			// WHERE
			if (!empty($queryObject->queryWhere))
			{
				$sql.= " WHERE ".join(' '.$queryObject->queryWhereType.' ',$queryObject->queryWhere);
			}

			// GROUP BY
			if (!empty($queryObject->queryGroupBy))
			{
				$sql.= " GROUP BY ".join(', ',$queryObject->queryGroupBy);
			}

			// HAVING
			if (!empty($queryObject->queryHaving))
			{
				$sql.= " HAVING ".join(' '.$queryObject->queryWhereType.' ',$queryObject->queryHaving);
			}

			// ORDER BY
			if (!empty($queryObject->queryOrderBy))
			{
				$sql.= " ORDER BY ".join(', ',$queryObject->queryOrderBy);
			}

			// LIMIT
			if (!empty($queryObject->queryLimit))
			{
				$sql.= " LIMIT ";
				if ($queryObject->queryLimitOffset!=NULL) $sql.=$queryObject->queryLimitOffset.',';
				$sql.=$queryObject->queryLimit;
			}

			// Return
			return $sql;
		}


		/**
		 *   Execute a query
		 *   @access public
		 *   @param string $sql SQL statement
		 *   @throws Exception
		 *   @return object query handle or FALSE in case of a failure
		 */

		function query ( $sql )
		{
			global $LAB;

			// Connect if not yet
			$this->realConnect();

			$query=$this->DBH->query($sql,PDO::FETCH_ASSOC);
			return $query;
		}


		/**
		 *  End query
		 *  @access public
		 *  @param object $query query handle
		 *  @return void
		 */

		function endQuery ( $query )
		{
			unset($query);
		}


		/**
		 *  Fetch next row from the query handle
		 *  @access public
		 *  @param object $query query handle
		 *  @return array|boolean row or FALSE on error
		 */

		function fetchRow ( $query )
		{
			throw new Exception('Not implemented.');
		}


		/**
		 *   Select database
		 *   @access public
		 *   @param string $database database
		 *   @throws Exception
		 *   @return void
		 */

		function selectDatabase ( $database )
		{
			// Reconnect
			try
			{
				$this->DBH=new PDO("dblib:host=".$this->dbConnection['hostname'].":".$this->dbConnection['port'].";dbname=".$database,$this->dbConnection['username'],$this->dbConnection['password']);
			}
			catch (PDOException $e)
			{
				throw new Exception('Could not connect to database: '.$e->getMessage());
				return FALSE;
			}

			// Set to throw exceptions
			$this->DBH->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

			// Return successfully
			return TRUE;
		}


		/**
		 *  Perform a query and return results as an array
		 *  @access public
		 *  @param BASE_QUERY $queryObject query object
		 *  @param string|boolean $key field to use as the array key
		 *  @return array resultset
		 */

		function querySelect ( $queryObject, $key=FALSE )
		{
			global $LAB;

			// Connect if not yet
			$this->realConnect();

			// Reset dataset
			$dataset=array();

			// Build query
			$sql=$this->buildSQL($queryObject);

			// Execute query and get results
			$timeStart=microtime_float();
			$res=$this->query($sql);
			$timeEnd=microtime_float();
			$queryTime=$timeEnd-$timeStart;
			foreach ($res as $row)
			{
				if ($key)
				{
					$dataset[$row[$key]]=$row;
				}
				else
				{
					array_push($dataset,$row);
				}
			}
			$this->endquery($res);

			// Update stats
			$this->cntQuery++;
			$this->cntQuerySelect++;

			// Found rows
			/*
			if ($queryObject->calcFoundRows)
			{
				$res=$this->query("SELECT found_rows() as found_rows");
				if ($res!==FALSE) while($row=$this->fetchrow($res)) $this->_foundRows=intval($row['found_rows']);
				$this->endquery($res);
			}
			*/

			// Profiler info
			/*
			$this->addProfilerInfo($sql,$queryTime,sizeof($dataset),true);
			*/

			// Return dataset
			return $dataset;
		}


		/**
		 *   Execute a raw SQL query and return results as an array
		 *   @access public
		 *   @param string $sql SQL statement
		 *   @param string|boolean $key field to use as the array key
		 *   @return array resultset
		 */

		function querySelectSQL ( $sql, $key=FALSE )
		{
			global $LAB;

			// Connect if not yet
			$this->realConnect();

			// Reset dataset
			if (isset($dataset)) unset($dataset);
			$dataset=array();

			// Execute query and get results
			$timeStart=microtime_float();
			$res=$this->query($sql);
			$timeEnd=microtime_float();
			$queryTime=$timeEnd-$timeStart;
			foreach ($res as $row)
			{
				if ($key)
				{
					$dataset[$row[$key]]=$row;
				}
				else
				{
					array_push($dataset,$row);
				}
			}
			$this->endquery($res);

			// Found rows
			/*
			if (mb_stripos($sql,'SQL_CALC_FOUND_ROWS')!==false)
			{
				$res=$this->query("SELECT found_rows() as found_rows");

				if ($res!==FALSE) while($row=$this->fetchrow($res)) $this->_foundRows=intval($row['found_rows']);
				$this->endquery($res);
			}
			*/

			// Profiler info
			/*
			$explain=(!strncasecmp(trim($sql),"SELECT",6))?TRUE:FALSE;
			$this->addProfilerInfo($sql,$queryTime,sizeof($dataset),$explain);
			*/

			// Update stats
			$this->cntQuery++;
			if (!strncasecmp($sql,"UPDATE",6) || !strncasecmp($sql,"REPLACE",6) || !strncasecmp($sql,"DELETE",6))
			{
				$this->cntQueryUpdate++;
			}
			else
			{
				$this->cntQuerySelect++;
			}

			// Return dataset
			return $dataset;
		}


		/**
		 *   Perform a query and return one row
		 *   @access public
		 *   @param BASE_QUERY $queryObject query object
		 *   @return array resultset
		 */

		function selectOne ( $queryObject )
		{
			// Get dataset
			$queryObject->addLimit(1,NULL);
			$dataset=$this->querySelect($queryObject);

			// Return
			if (!sizeof($dataset)) return FALSE;
			return $dataset[0];
		}


		/**
		 *   Execute a raw SQL query and return one row
		 *   @access public
		 *   @param string $sql SQL
		 *   @return array resultset
		 */

		function selectOneSQL ( $sql )
		{
			// Get dataset
			if (stripos($sql,"limit")===FALSE) $sql.=" LIMIT 1";
			$dataset=$this->querySelectSQL($sql, '');

			// Return
			if (!sizeof($dataset)) return FALSE;
			return $dataset[0];
		}


		/**
		 *  Perform a query and return one field
		 *  @access public
		 *  @param string Field name
		 *  @param BASE_QUERY query object
		 *  @return mixed value or FALSE in case of error
		 */

		function selectField ( $field, $queryObject )
		{
			// Get dataset
			$queryObject->addLimit(1,NULL);
			$dataset=$this->querySelect($queryObject);

			// Return
			if (!sizeof($dataset)) return FALSE;
			return $dataset[0][$field];
		}


		/**
		 *   Execute a raw SQL query and return one field
		 *   @access public
		 *   @param string $field Field name
		 *   @param string $sql SQL
		 *   @return mixed value
		 */

		function selectFieldSQL ( $field, $sql )
		{
			// Get dataset
			if (stripos($sql,"limit")===FALSE) $sql.=" LIMIT 1";
			$dataset=$this->querySelectSQL($sql, '');

			// Return
			if (!sizeof($dataset)) return FALSE;
			return $dataset[0][$field];
		}


		/**
		 *   Insert a row into the table
		 *   @access public
		 *   @param string $table Table name
		 *   @param array $columns columns
		 *   @return int Generated auto_increment value
		 */

		function queryInsert ( $table, $columns )
		{
			// Connect if not yet
			$this->realConnect();

			// Build query
			$sql="INSERT INTO ".addslashes($table)." SET ";
			$fcnt=0;
			foreach ($columns as $col => $value)
			{
				$sql.=$fcnt?',':'';
				$sql.=addslashes($col)."=".$this->colvalue($value);
				$fcnt++;
			}

			// Execute query
			$timeStart=microtime_float();
			$res=$this->query($sql);
			$timeEnd=microtime_float();
			$queryTime=$timeEnd-$timeStart;

			// Return on error
			if ($res===FALSE) return FALSE;

			// Update stats
			$this->cntQuery++;
			$this->cntQueryUpdate++;

			// Profiler info
			$this->addProfilerInfo($sql,$queryTime);

			// Return insert ID
			$id=mysqli_insert_id($this->DBH);
			return $id;
		}


		/**
		 *   Insert a row into the table by SQL
		 *   @access public
		 *   @param string $sql SQL statement
		 *   @return int Generated auto_increment value
		 */

		function queryInsertSQL ( $sql )
		{
			// Connect if not yet
			$this->realConnect();

			// Execute query
			$timeStart=microtime_float();
			$res=$this->query($sql);
			$timeEnd=microtime_float();
			$queryTime=$timeEnd-$timeStart;

			// Return on error
			if ($res===FALSE) return FALSE;

			// Update stats
			$this->cntQuery++;
			$this->cntQueryUpdate++;

			// Profiler info
			$this->addProfilerInfo($sql,$queryTime);

			// Return ID
			$id=mysqli_insert_id($this->DBH);
			return $id;
		}


		/**
		 *   Perform a REPLACE query
		 *   @access public
		 *   @param string $table Table name
		 *   @param array $columns columns
		 *   @return int Generated auto_increment value
		 */

		function queryReplace ( $table, $columns )
		{
			// Connect if not yet
			$this->realConnect();

			// Build query
			if (is_array($columns[0]))
			{
				$sql="REPLACE INTO ".addslashes($table)." (";
				$fcnt=0;
				foreach ($columns[0] as $col => $value)
				{
					$sql.=($fcnt?',':'').$col;
					$fcnt++;
				}
				$sql.=") VALUES";
				$rcnt=0;
				foreach ($columns as $c)
				{
					$sql.=($rcnt?',':'').'(';
					$fcnt=0;
					foreach ($c as $col => $value)
					{
						$sql.=($fcnt?',':'').$this->colvalue($value);
						$fcnt++;
					}
					$sql.=')';
					$rcnt++;
				}
			}
			else
			{
				$sql="REPLACE INTO ".addslashes($table)." SET ";
				$fcnt=0;
				foreach ($columns as $col => $value)
				{
					$sql.=$fcnt?',':'';
					$sql.=addslashes($col)."=".$this->colvalue($value);
					$fcnt++;
				}
			}

			// Execute query
			$timeStart=microtime_float();
			$res=$this->query($sql);
			$timeEnd=microtime_float();
			$queryTime=$timeEnd-$timeStart;

			// Return on error
			if ($res===FALSE) return FALSE;

			// Update stats
			$this->cntQuery++;
			$this->cntQueryUpdate++;

			// Debug info
			$this->addProfilerInfo($sql,$queryTime);

			// Return ID
			$id=mysqli_insert_id($this->DBH);
			return $id;
		}


		/**
		 *   Perform an UPDATE query
		 *   @access public
		 *   @param string $table Table name
		 *   @param array $columns columns
		 *   @param string $where WHERE clause
		 *   @return int Number of affected rows
		 */

		function queryUpdate ( $table, $columns, $where )
		{
			// Connect if not yet
			$this->realConnect();

			// Build query
			$sql="UPDATE ".addslashes($table)." SET ";
			$fcnt=0;
			foreach ($columns as $col => $value)
			{
				$sql.=$fcnt?',':'';
				$sql.=addslashes($col)."=".$this->colvalue($value);
				$fcnt++;
			}
			$sql.=" WHERE ".(is_array($where)?join(' and ',$where):$where);

			// Execute query
			$timeStart=microtime_float();
			$res=$this->query($sql);
			$timeEnd=microtime_float();
			$queryTime=$timeEnd-$timeStart;
			$affectedRows=intval(mysqli_affected_rows($this->DBH));

			// Return on error
			if ($res===FALSE) return FALSE;

			// Update stats
			$this->cntQuery++;
			$this->cntQueryUpdate++;

			// Debug info
			$this->addProfilerInfo($sql,$queryTime,$affectedRows);

			// Return affected rows
			return $affectedRows;
		}


		/**
		 *   Perform a raw UPDATE query
		 *   @access public
		 *   @param string $sql SQL statement
		 *   @return int Number of affected rows
		 */

		function queryUpdateSQL ( $sql )
		{
			// Connect if not yet
			$this->realConnect();

			// Execute query
			$timeStart=microtime_float();
			$res=$this->query($sql);
			$timeEnd=microtime_float();
			$queryTime=$timeEnd-$timeStart;
			$affectedRows=intval(mysqli_affected_rows($this->DBH));

			// Return on error
			if ($res===FALSE) return FALSE;

			// Update stats
			$this->cntQuery++;
			$this->cntQueryUpdate++;

			// Debug info
			$this->addProfilerInfo($sql,$queryTime,$affectedRows);

			// Return affected rows
			return $affectedRows;
		}


		/**
		 *   Delete rows from the table
		 *   @access public
		 *   @param array Parameters
		 *   @return int Number of affected rows
		 */

		function queryDelete ( $param )
		{
			// Connect if not yet
			$this->realConnect();

			// Build query
			$sql=$this->buildSQL($param);

			// Execute query
			$timeStart=microtime_float();
			$res=$this->query($sql);
			$timeEnd=microtime_float();
			$queryTime=$timeEnd-$timeStart;
			$affectedRows=intval(mysqli_affected_rows($this->DBH));

			// Update stats
			$this->cntQuery++;
			$this->cntQueryUpdate++;

			// Debug info
			$this->addProfilerInfo($sql,$queryTime,$affectedRows);

			// Return affected rows
			return $affectedRows;
		}


		/**
		 *  Start transaction
		 *  @access public
		 *  @return void
		 */

		function startTransaction()
		{
			// Connect if not yet
			$this->realConnect();

			// Already in a transaction: create a savepoint
			if ($this->transactionLevel>0)
			{
				$this->transactionLevel++;
				$sql='SAVEPOINT trans'.intval($this->transactionLevel);
				$this->query($sql);
				$this->addProfilerInfo($sql,0,0);
			}

			// Start transaction
			else
			{
				$sql='BEGIN';
				$this->query($sql);
				$this->addProfilerInfo($sql,0,0);
				$this->transactionLevel++;
			}
		}


		/**
		 *  Commit
		 *  @access public
		 *  @return void
		 */

		function doCommit()
		{
			// Connect if not yet
			$this->realConnect();

			// In a nested transaction: release savepoint
			if ($this->transactionLevel>1)
			{
				$sql='RELEASE SAVEPOINT trans'.intval($this->transactionLevel);
				$this->query($sql);
				$this->addProfilerInfo($sql,0,0);
				$this->transactionLevel--;
			}

			// Commit
			else
			{
				$sql='COMMIT';
				$this->query($sql);
				$this->addProfilerInfo($sql,0,0);
				$this->transactionLevel--;
			}
		}


		/**
		 *  Rollback
		 *  @access public
		 *  @return void
		 */

		function doRollback()
		{
			// Connect if not yet
			$this->realConnect();

			// Nested transaction: rollback to savepoint
			if ($this->transactionLevel>1)
			{
				$sql='ROLLBACK TO trans'.intval($this->transactionLevel);
				$this->query($sql);
				$this->addProfilerInfo($sql,0,0);
				$this->transactionLevel--;
			}

			// Otherwise: full rollback
			else
			{
				$sql='ROLLBACK';
				$this->query($sql);
				$this->addProfilerInfo($sql,0,0);
				$this->transactionLevel--;
			}
		}


		/**
		 *   Lock tables
		 *   @access public
		 *   @param string|array $tableList List of tables
		 *   @param string $lockType lock type
		 *   @return void
		 */

		function lockTables( $tableList, $lockType='WRITE' )
		{
			// Connect if not yet
			$this->realConnect();

			$lockList=array();
			foreach (str_array($tableList) as $table) $lockList[]=$table.' '.$lockType;
			$this->query("LOCK TABLES ".join(', ',$lockList));
		}


		/**
		 *   Unlock tables
		 *   @access public
		 *   @return void
		 */

		function unlockTables()
		{
			// Connect if not yet
			$this->realConnect();

			$this->query('UNLOCK TABLES');
		}


		/**
		 *   Returns number of found rows in last query
		 *   @access public
		 *   @return int Number of found rows
		 */

		function foundRows()
		{
			return intval($this->_foundRows);
		}


		/**
		 *   Returns last error message
		 *   @access public
		 *   @return string error message
		 */

		function getError()
		{
			return mysqli_error($this->DBH);
		}


	}


	?>