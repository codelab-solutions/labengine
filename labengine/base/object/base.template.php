<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   The template class
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_TEMPLATE
	{


		/**
		 *   Template name
		 *   @var string
		 *   @access public
		 */

		public $templateName = '';


		/**
		 *   Template variables
		 *   @var string
		 *   @access public
		 */

		public $templateVar = array();


		/**
		 *   Template contents
		 *   @var string
		 *   @access public
		 */

		public $templateContents = '';


		/**
		 *   Parse template vars
		 *   @var boolean
		 *   @access public
		 */

		public $templateParseVars  = TRUE;


		/**
		 *   The constructor
		 *   @access public
		 *   @param bool $templateName
		 *   @throws Exception
		 */

		public function __construct( $templateName=FALSE )
		{
			// Set template name
			$this->templateName=$templateName;

			// Load template if specified
			if (!empty($templateName))
			{
				$this->loadTemplate($templateName);
			}
		}


		/**
		 *   Load the template
		 *   @access public
		 *   @throws Exception
		 *   @return void
		 */

		public function loadTemplate()
		{
			// Template location
			if ($this->templateName[0]=='/')
			{
				$templateFilename=$this->templateName;
			}
			elseif (strpos($this->templateName,'/')!==FALSE)
			{
				$tnArray=preg_split("/\//",$this->templateName);
				if ($tnArray[sizeof($tnArray)-2]!='template')
				{
					$fn=$tnArray[sizeof($tnArray)-1];
					$tnArray[sizeof($tnArray)-1]='template';
					$tnArray[]=$fn;
				}
				$templateFilename=join('/',$tnArray);
			}
			elseif (stream_resolve_include_path('site/template/'.$this->templateName.'.tpl'))
			{
				$templateFilename='site/template/'.$this->templateName.'.tpl';
			}
			else
			{
				$templateFilename='base/template/'.$this->templateName.'.tpl';
			}
			if (!preg_match("/\.tpl$/",$templateFilename))
			{
				$templateFilename.='.tpl';
			}

			// Load template contents
			$this->templateContents=file_get_contents($templateFilename, FILE_USE_INCLUDE_PATH);
			if ($this->templateContents===FALSE)
			{
				throw new Exception('Unable to load template: '.$this->templateName);
			}
		}



		/**
		 *   Set a template var
		 *   @access public
		 *   @param string $varName variable name
		 *   @param mixed $varValue value
		 *   @return void
		 */

		public function set( $varName, $varValue )
		{
			traverse_set($varName,$this->templateVar,$varValue);
		}



		/**
		 *   Get a template var
		 *   @access public
		 *   @param string $varName variable name
		 *   @return mixed Value
		 */

		public function get( $varName )
		{
			return traverse_get($varName,$this->templateVar);
		}


		/**
		 *   Parse template contents
		 *   @access public
		 *   @param string|bool $src Source
		 *   @param bool $parseLocale Parse locale tags
		 *   @param bool $parseVariables Parse variable tags
		 *   @return string parsed template
		 */

		public function parse( $src=FALSE, $parseLocale=TRUE, $parseVariables=TRUE )
		{
			global $LAB;

			// Get source
			$x=$src;
			if ($src===FALSE) $src=$this->templateContents;

			// Replace this
			$savedSource=array();
			if (preg_match_all('/\[\*\*\*SRC\*\*\*(.*?)\*\*\*SRC\*\*\*\]/ms',$src,$match))
			{
				foreach ($match[1] as $m => $m_res)
				{
					$savedSource[$m]=$m_res;
					$src=str_replace($match[0][$m],'[***SRC*'.intval($m).'***]',$src);
				}
			}

			// Parse variables
			if ($src===FALSE || $parseVariables)
			{
				if ($this->templateParseVars)
				{
					$src=preg_replace_callback("/\{([A-Za-z0-9\_\=\.\/]+?)\}/",array($this,'_parse_variable'),$src);
				}

				// Import template vars into local context
				extract($this->templateVar,EXTR_SKIP);

				// Parse template
				ob_start();
				eval('?>'.$src.'<?php ');
				$t=ob_get_contents();
				ob_end_clean();
			}
			else
			{
				$t=$src;
			}

			// Parse localized strings
			if (($src===FALSE && $this->templateParseVars) || ($src!==FALSE && $parseLocale))
			{
				$t=preg_replace_callback("/\[([A-Za-z0-9\.\,\|]+?)\]/",array($this,'_parse_getlocale'),$t);
			}

			// Put saved source back in
			if (sizeof($savedSource))
			{
				foreach ($savedSource as $c => $content)
				{
					$t=str_replace('[***SRC*'.intval($c).'***]',$content,$t);
				}
			}

			// Return the contents
			return $t;
		}



		/**
		 *   Get a localized string
		 *   @access private
		 *   @param string $match match
		 *   @return string localized string
		 */

		function _parse_getlocale( $match )
		{
			global $LAB;
			if (!strlen($match[1])) return '[]';
			return strlen($LAB->LOCALE->get($match[1]))?$LAB->LOCALE->get($match[1]):'['.$match[1].']';
		}



		/**
		 *   Get a variable
		 *   @access private
		 *   @param string $match Match
		 *   @param array $var Variables as array
		 *   @return string parsed variable
		 */

		private function _parse_variable( $match, $var=FALSE )
		{
			$var=($var===FALSE)?$this->templateVar:$var;
			$aTmp=preg_split('/\./',$match[1]);

			if ($aTmp[0]=='contents')
			{
				return $this->parse($var['contents']);
			}
			return traverse_get($match[1],$var);
		}


		/**
		 *   Parse the static content for locale variables
		 *   @access public
		 *   @static
		 *   @param string|array|object $content Content
		 *   @param bool $parseLocale Parse locale tags
		 *   @param bool $parseVariables Parse variables
		 *   @param array $templateVar Template variables
		 *   @return string parsed template
		 */

		public static function parseContent( $content, $parseLocale=TRUE, $parseVariables=TRUE, $templateVar=array() )
		{
			if (is_array($content))
			{
				foreach ($content as $k => $v)
				{
					$content[$k]=static::parseContent($v,$parseLocale,$parseVariables,$templateVar);
				}
				return $content;
			}
			elseif (is_object($content))
			{
				foreach ($content as $k => $v)
				{
					$content->{$k}=static::parseContent($v,$parseLocale,$parseVariables,$templateVar);
				}
				return $content;
			}
			else
			{
				$TEMPLATE=new BASE_TEMPLATE();
				$TEMPLATE->templateVar=$templateVar;
				return ($TEMPLATE->parse($content,$parseLocale,$parseVariables));
			}
		}


		/**
		 *   Parse the template and return the contents
		 *   @access public
		 *   @return string parsed template
		 */

		public function display()
		{
			return $this->parse();
		}


	}


?>