<?


	/**
	 *
	 *   LabEngine™ 7
	 *   Standard actions: AJAX action
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */

	class BASE_AJAXACTION extends BASE_DISPLAYOBJECT
	{


		/**
		 *   Execute the AJAX action
		 *   @access public
		 *   @return string output
		 *   @throws Exception
		 */

		function handleAction()
		{
			throw new Exception('Method <b>handleAction()</b> not implemented in this class.');
		}


		/**
		 *  Run the display object
		 *  @access public
		 *  @return string output
		 */

		function display()
		{
			global $LAB;

			// Set response type
			$LAB->RESPONSE->setType('json');

			// Run
			try
			{
				$response=$this->handleAction();
				if (!is_array($response)) $response=array('status'=>'1');
				return $response;
			}

			// Catch exception
			catch (Exception $e)
			{
				$response=array(
					'status' => '2',
					'error'  => $e->getMessage()
				);
				return $response;
			}
		}


	}


?>