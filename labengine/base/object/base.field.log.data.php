<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   Field class: detailed log data
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_FIELD_LOG_DATA extends BASE_FIELD_TEXT
	{


		/**
		 *   List value
		 *   @access public
		 *   @return mixed value
		 */

		public function listValue( $value, &$row )
		{
			if (mb_strlen($value))
			{
				return '<a onclick="BASE.alert(Base64.decode(\''.base64_encode(BASE_LOG::getLogDataHtml($row['log_data'])).'\'),\'[BASE.LOG.Fld.Data]\')"><span class="icon-log"></span></a>';
			}
			else
			{
				return '';
			}
		}


	}


?>