<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   Voog API
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_VOOG
	{


		/**
		 *   Site URL
		 *   @var string
		 */

		public $VOOG_url = null;


		/**
		 *   API token
		 *   @var string
		 */

		public $VOOG_token = null;


		/**
		 *   Log?
		 *   @var bool
		 */

		public $VOOG_log = false;


		/**
		 *   Init API, return instance
		 *   @access public
		 *   @static
		 *   @param string $url URL
		 *   @param string $token API token
		 *   @throws Exception
		 *   @return BASE_VOOG
		 *
		 */

		public static function init( $url=null, $token=null )
		{
			global $LAB;

			// Create instance
			$VOOG=new BASE_VOOG();

			// Set up parameters
			$VOOG->VOOG_url=oneof($url,$LAB->CONFIG->get('voog.url'));
			$VOOG->VOOG_token=oneof($token,$LAB->CONFIG->get('voog.token'));
			$VOOG->VOOG_log=($LAB->CONFIG->get('voog.log')?true:false);

			// Return
			return $VOOG;
		}


		/**
		 *   Make API request
		 *   @access public
		 *   @param string $url URL
		 *   @param string $method Request method (GET/POST/PUT/PATCH/DELETE)
		 *   @param string|array|object $data Request data
		 *   @param string $contentType Content-type
		 *   @throws Exception
		 *   @return BASE_VOOG
		 *
		 */

		public function makeAPIrequest( $url, $method='GET', $data=null, $contentType=false )
		{
			global $LAB;
			try
			{
				// Set up request
				$HTTP=new BASE_HTTPREQUEST();
				$HTTP->setURL($this->VOOG_url.'/admin/api'.(strncmp($url,'/',1)?'/':'').$url);
				$HTTP->setRequestMethod(mb_strtoupper($method));
				$HTTP->setRequestHeader('X-API-TOKEN',$this->VOOG_token);
				if (!empty($data))
				{
					if (in_array(mb_strtoupper($method),array('GET')))
					{
						if (is_array($data))
						{
							$HTTP->setURL($HTTP->URL.'?'.BASE_HTTPREQUEST::buildQueryString($data));
						}
						else
						{
							$HTTP->setURL($HTTP->URL.'?'.$data);
						}
					}
					else
					{
						$HTTP->setRequestHeader('Content-Type',oneof($contentType,'application/json'));
						if (is_array($data) || is_object($data))
						{
							$HTTP->setRawPostData(json_encode($data,JSON_HEX_QUOT));
						}
						else
						{
							$HTTP->setRawPostData($data);
						}
					}
				}

				// Make request
				$voogResponse=$HTTP->send();

				// Log
				if ($this->VOOG_log)
				{
					$cols=array();
					$cols['user_oid']=$LAB->USER->getUserOID();
					$cols['vooglog_tstamp']=date('Y-m-d H:i:s');
					$cols['vooglog_method']=$HTTP->requestMethod;
					$cols['vooglog_url']=$HTTP->URL;
					$cols['vooglog_request']=$HTTP->rawPostData;
					$cols['vooglog_response']=$voogResponse;
					$cols['vooglog_status']=$HTTP->getResponseCode();
					$cols['vooglog_requesttime']='-1';
					$LAB->DB->queryInsert('base_vooglog',$cols);
				}
			}
			catch (Exception $e)
			{
				// Log
				if ($this->VOOG_log)
				{
					$cols=array();
					$cols['user_oid']=$LAB->USER->getUserOID();
					$cols['vooglog_tstamp']=date('Y-m-d H:i:s');
					$cols['vooglog_method']=$HTTP->requestMethod;
					$cols['vooglog_url']=$HTTP->URL;
					$cols['vooglog_request']=$HTTP->rawPostData;
					$cols['vooglog_response']='ERROR: '.$e->getMessage();
					$cols['vooglog_status']='-1';
					$cols['vooglog_requesttime']='-1';
					$LAB->DB->queryInsert('base_vooglog',$cols);
				}

				// Rethrow
				throw $e;
			}

			// Check response
			if ($HTTP->getResponseCode()<200 || $HTTP->getResponseCode()>299)
			{
				if (mb_strlen($voogResponse))
				{
					$voogResponseObj=json_decode($voogResponse);
					if ($voogResponseObj===false)
					{
						throw new Exception('Non-200 response from VOOG API: '.$HTTP->getResponseCode().' - '.$voogResponse);
					}
					else
					{
						throw new Exception('Error from VOOG API: '.$HTTP->getResponseCode().' - '.$voogResponseObj->message);
					}
				}
				else
				{
					throw new Exception('Non-200 response from VOOG API: '.$HTTP->getResponseCode());
				}
			}

			// Parse response
			if (mb_strlen($voogResponse))
			{
				$voogResponse=json_decode($voogResponse);
				if ($voogResponse===false) throw new Exception('Could not parse response JSON.');
			}

			// Return
			return $voogResponse;
		}


		/**
		 *   Get site data
		 *   @access public
		 *   @return object
		 *
		 */

		public function getSiteData()
		{
			return $this->makeAPIrequest('/site');
		}


		/**
		 *   Update site data
		 *   @access public
		 *   @throws Exception
		 *   @return object
		 *
		 */

		public function updateSiteData( $data )
		{
			return $this->makeAPIrequest('/site','PATCH',$data);
		}


		/**
		 *   Set site data parameter
		 *   @access public
		 *   @throws Exception
		 *   @return object
		 *
		 */

		public function setSiteCustomData( $paramName, $paramValue )
		{
			$data=array(
				"value" => $paramValue
			);
			return $this->makeAPIrequest('/site/data/'.$paramName,'PUT',$data);
		}


		/**
		 *   Remove site data parameter
		 *   @access public
		 *   @throws Exception
		 *   @return object
		 *
		 */

		public function deleteSiteCustomData( $paramName )
		{
			return $this->makeAPIrequest('/site/data/'.$paramName,'DELETE');
		}


		/**
		 *   Get languages
		 *   @access public
		 *   @return object
		 *
		 */

		public function getLanguages()
		{
			// Make response
			$voogResponse=$this->makeAPIrequest('/languages');

			// Build response array
			$languageArray=array();
			foreach ($voogResponse as $lang)
			{
				$languageArray[strval($lang->code)]=$lang;
			}

			// Return
			return $languageArray;
		}


		/**
		 *   Get users
		 *   @access public
		 *   @return object
		 *
		 */

		public function getUsers()
		{
			return $this->makeAPIrequest('/site_users');
		}


		/**
		 *   Get admin users
		 *   @access public
		 *   @return object
		 *
		 */

		public function getAdminUsers()
		{
			return $this->makeAPIrequest('/people');
		}


		/**
		 *   Get element definitions
		 *   @access public
		 *   @param array $data Request data
		 *   @param bool $listOnly Return displayable list only
		 *   @throws Exception
		 *   @return array
		 */

		public function getElementDefinitions( $data=null, $listOnly=false )
		{
			// Request with parameters
			if (!empty($data))
			{
				$requestData=array();
				foreach ($data as $k => $v)
				{
					$requestData[$k]=$v;
				}
				$voogResponse=$this->makeAPIrequest('/element_definitions?'.BASE_HTTPREQUEST::buildQueryString($data));
			}

			// Full list
			else
			{
				$voogResponse=$this->makeAPIrequest('/element_definitions');
			}

			// Build response array
			$elementDefArray=array();
			foreach ($voogResponse as $elementDef)
			{
				$elementDefArray[strval($elementDef->id)]=($listOnly?strval($elementDef->title):$elementDef);
			}

			// Return
			return $elementDefArray;
		}


		/**
		 *   Get element definition
		 *   @access public
		 *   @param int $id Element definition ID
		 *   @throws Exception
		 *   @return array
		 */

		public function getElementDefinition( $id )
		{
			// Check
			if (empty($id)) throw new Exception('Missing input data: id.');

			// Make response
			return $this->makeAPIrequest('/element_definitions/'.strval($id));
		}


		/**
		 *   Get element definition by name
		 *   @access public
		 *   @param string $name Element definition name
		 *   @throws Exception
		 *   @return array
		 */

		public function getElementDefinitionByName( $name )
		{
			// Check
			if (empty($name)) throw new Exception('Missing input data: name.');

			// Get list
			$elementDefinitionList=$this->getElementDefinitions(array('q.element_definition.title.$eq'=>$name),false);
			if (!sizeof($elementDefinitionList)) throw new Exception('Element definition '.$name.' not found.');
			if (sizeof($elementDefinitionList)>1) throw new Exception('Multiple matches for '.$name.' found.');

			// Return
			foreach ($elementDefinitionList as $elementDefinition)
			{
				return $this->getElementDefinition(strval($elementDefinition->id));
			}
		}


		/**
		 *   Get element list
		 *   @access public
		 *   @param int $elementDefinitionID Element definition ID
		 *   @param int|string $lang Language (ID or string)
		 *   @param int|page $page Page (ID or path)
		 *   @param array $params Additional parameters
		 *   @throws Exception
		 *   @return array
		 */

		public function getElements( $elementDefinitionID, $lang=null, $page=null, $params=null )
		{
			// Check
			if (empty($elementDefinitionID)) throw new Exception('Missing input data: elementDefinitionID.');

			// Build query string
			$queryData=array();
			$queryData['element_definition_id']=$elementDefinitionID;
			$queryData['include_values']='true';
			if (!empty($lang))
			{
				if (is_numeric($lang))
				{
					$queryData['language_id']=$lang;
				}
				else
				{
					$queryData['langauge_code']=$lang;
				}
			}
			if (!empty($page))
			{
				if (is_numeric($page))
				{
					$queryData['page_id']=$page;
				}
				else
				{
					$queryData['page_path']=$page;
				}
			}
			if (!empty($params) && is_array($params))
			{
				foreach ($params as $k => $v)
				{
					$queryData[$k]=$v;
				}
			}

			// Make response
			$voogResponse=$this->makeAPIrequest('/elements?'.BASE_HTTPREQUEST::buildQueryString($queryData));

			// Build response array
			$elementArray=array();
			foreach ($voogResponse as $element)
			{
				$elementArray[strval($element->id)]=$element;
			}

			// Return
			return $elementArray;
		}


		/**
		 *   Get element
		 *   @access public
		 *   @param int $id Element ID
		 *   @throws Exception
		 *   @return array
		 */

		public function getElement( $id )
		{
			// Check
			if (empty($id)) throw new Exception('Missing input data: id.');

			// Make response
			return $this->makeAPIrequest('/elements/'.strval($id));
		}


		/**
		 *   Create element
		 *   @access public
		 *   @param int $elementDefinitionID Element definition ID
		 *   @param int $pageID Page ID
		 *   @param string $title
		 *   @param array|object $data Element values/data
		 *   @throws Exception
		 *   @return array
		 */

		public function addElement( $elementDefinitionID, $pageID, $title, $data=null )
		{
			// Check
			if (empty($elementDefinitionID)) throw new Exception('Missing input data: elementDefinitionID.');
			if (empty($pageID)) throw new Exception('Missing input data: pageID.');
			if (empty($title)) throw new Exception('Missing input data: title.');

			// Create data
			$requestData=new stdClass();
			$requestData->element_definition_id=intval($elementDefinitionID);
			$requestData->page_id=intval($pageID);
			$requestData->title=strval($title);
			$requestData->values=new stdClass();
			if (!empty($data) && (is_array($data) || is_object($data)))
			{
				foreach ($data as $k => $v)
				{
					$requestData->values->{$k}=$v;
				}
			}

			// Make response
			return $this->makeAPIrequest('/elements','POST',$requestData);
		}


		/**
		 *   Create element
		 *   @access public
		 *   @param int $id Element ID
		 *   @param array|object $data Element values/data
		 *   @param string $title Title (if updated)
		 *   @param string $path Path (if updated)
		 *   @throws Exception
		 *   @return array
		 */

		public function updateElement( $id, $data, $title=null, $path=null )
		{
			// Check
			if (empty($id)) throw new Exception('Missing input data: id.');

			// Create data
			$requestData=new stdClass();
			if (!empty($title))
			{
				$requestData->title=$title;
			}
			if (!empty($path))
			{
				$requestData->path=$path;
			}

			// Values
			$requestData->values=new stdClass();
			foreach ($data as $k => $v)
			{
				$requestData->values->{$k}=$v;
			}

			// Make response
			return $this->makeAPIrequest('/elements/'.strval($id),'PUT',$requestData);
		}


		/**
		 *   Delete element
		 *   @access public
		 *   @param int $id Element ID
		 *   @throws Exception
		 *   @return object
		 *
		 */

		public function deleteElement( $id )
		{
			return $this->makeAPIrequest('/elements/'.strval($id),'DELETE');
		}


		/**
		 *   Get element contents
		 *   @access public
		 *   @param int $id Element ID
		 *   @throws Exception
		 *   @return array
		 */

		public function getElementContents( $element_id, $data=null )
		{
			// Check
			if (empty($element_id)) throw new Exception('Missing input data: element_id.');

			// Request with parameters
			if (!empty($data))
			{
				$requestData=array();
				foreach ($data as $k => $v)
				{
					$requestData[$k]=$v;
				}
				$voogResponse=$this->makeAPIrequest('/elements/'.strval($element_id).'/contents?'.BASE_HTTPREQUEST::buildQueryString($data),'GET');
			}

			// Full list
			else
			{
				$voogResponse=$this->makeAPIrequest('/elements/'.strval($element_id).'/contents');
			}

			// Build response array
			$contentsArray=array();
			foreach ($voogResponse as $content)
			{
				$contentsArray[strval($content->id)]=$content;
			}

			// Return
			return $contentsArray;
		}


		/**
		 *   Get element contents
		 *   @access public
		 *   @param int $element_id Element ID
		 *   @param int $content_id Content ID
		 *   @throws Exception
		 *   @return array
		 */

		public function getElementContent( $element_id, $content_id )
		{
			// Check
			if (empty($element_id)) throw new Exception('Missing input data: element_id.');
			if (empty($content_id)) throw new Exception('Missing input data: content_id.');

			// Make response
			return $this->makeAPIrequest('/elements/'.strval($element_id).'/contents/'.strval($content_id));
		}


		/**
		 *   Create a element
		 *   @access public
		 *   @param int $elementID Element ID
		 *   @param array $data Content data
		 *   @throws Exception
		 *   @return array
		 */

		public function addElementContent( $elementID, $data=null )
		{
			// Check
			if (empty($elementID)) throw new Exception('Missing input data: elementID.');
			if (empty($data)) throw new Exception('Missing input data: data.');

			// Make response
			return $this->makeAPIrequest('/elements/'.strval($elementID).'/contents','POST',$data);
		}


		/**
		 *   Delete element content
		 *   @access public
		 *   @param int $id Element ID
		 *   @throws Exception
		 *   @return object
		 *
		 */

		public function deleteElementContent( $element_id, $content_id )
		{
			// Check
			if (empty($element_id)) throw new Exception('Missing input data: element_id.');
			if (empty($content_id)) throw new Exception('Missing input data: content_id.');

			// Delete
			return $this->makeAPIrequest('/elements/'.strval($element_id).'/contents/'.strval($content_id),'DELETE');
		}


		/**
		 *   Get pages
		 *   @access public
		 *   @param array|object $data Filter data
		 *   @throws Exception
		 *   @return object
		 */

		public function getPages( $data=null )
		{
			// Request with parameters
			if (!empty($data))
			{
				$requestData=array();
				foreach ($data as $k => $v)
				{
					$requestData[$k]=$v;
				}
				$voogResponse=$this->makeAPIrequest('/pages?'.BASE_HTTPREQUEST::buildQueryString($data));
			}

			// Full list
			else
			{
				$voogResponse=$this->makeAPIrequest('/pages');
			}

			// Build response array
			$pageArray=array();
			foreach ($voogResponse as $page)
			{
				$pageArray[strval($page->id)]=$page;
			}

			// Return
			return $pageArray;
		}


		/**
		 *   Get page
		 *   @access public
		 *   @param int $id Page ID
		 *   @throws Exception
		 *   @return array
		 */

		public function getPage( $id )
		{
			// Check
			if (empty($id)) throw new Exception('Missing input data: id.');

			// Make response
			return $this->makeAPIrequest('/pages/'.strval($id));
		}


		/**
		 *   Get page by URL
		 *   @access public
		 *   @param int $url Page URL
		 *   @throws Exception
		 *   @return array
		 */

		public function getPageByURL( $url )
		{
			// Check
			if (empty($url)) throw new Exception('Missing input data: url.');

			// Remove leading slash
			if (!strncmp($url,'/',1))
			{
				$url=mb_substr($url,1);
			}

			// Get list
			$pageList=$this->getPages(array('path'=>$url));
			if (!sizeof($pageList)) throw new Exception('Page /'.$url.' not found.');
			if (sizeof($pageList)>1) throw new Exception('Multiple matches for /'.$url.' found.');

			// Return
			foreach ($pageList as $page)
			{
				return $this->getPage(strval($page->id));
			}
		}


		/**
		 *   Create a page
		 *   @access public
		 *   @param string $title Page title
		 *   @param string $slug URL slug
		 *   @param int $parentID Parent ID
		 *   @param int $layoutID Layout ID
		 *   @param array $data Additional page data
		 *   @param array $pageData Additional page custom data
		 *   @throws Exception
		 *   @return array
		 */

		public function addPage( $title, $slug, $parentID, $layoutID, $data=null, $pageData=null )
		{
			// Check
			if (empty($title)) throw new Exception('Missing input data: title.');
			if (empty($slug)) throw new Exception('Missing input data: slug.');
			if (empty($parentID)) throw new Exception('Missing input data: parentID.');
			if (empty($layoutID)) throw new Exception('Missing input data: layoutID.');

			// Create data
			$requestData=new stdClass();
			$requestData->title=$title;
			$requestData->slug=$slug;
			$requestData->layout_id=$layoutID;
			$requestData->parent_id=$parentID;
			if (!empty($data))
			{
				foreach ($data as $k => $v)
				{
					$requestData->{$k}=$v;
				}
			}
			if (!empty($pageData))
			{
				$requestData->data=new stdClass();
				foreach ($pageData as $k => $v)
				{
					$requestData->data->{$k}=$v;
				}
			}

			// Make response
			return $this->makeAPIrequest('/pages','POST',$requestData);
		}


		/**
		 *   Update a page
		 *   @access public
		 *   @param int $id Page ID
		 *   @param string $slug URL slug
		 *   @param int $parentID Parent ID
		 *   @param int $layoutID Layout ID
		 *   @param array $data Additional page data
		 *   @param array $pageData Additional page custom data
		 *   @throws Exception
		 *   @return array
		 */

		public function updatePage( $id, $data=null, $pageData=null )
		{
			// Check
			if (empty($id)) throw new Exception('Missing input data: id.');

			// Create data
			$requestData=new stdClass();
			if (!empty($data))
			{
				foreach ($data as $k => $v)
				{
					$requestData->{$k}=$v;
				}
			}
			if (!empty($pageData))
			{
				$requestData->data=new stdClass();
				foreach ($pageData as $k => $v)
				{
					$requestData->data->{$k}=$v;
				}
			}

			// Make response
			return $this->makeAPIrequest('/pages/'.strval($id),'PATCH',$requestData);
		}


		/**
		 *   Delete page
		 *   @access public
		 *   @param int $id Page ID
		 *   @throws Exception
		 *   @return object
		 *
		 */

		public function deletePage( $id )
		{
			return $this->makeAPIrequest('/pages/'.strval($id),'DELETE');
		}


		/**
		 *   Get page contents
		 *   @access public
		 *   @param int $id Page ID
		 *   @throws Exception
		 *   @return array
		 */

		public function getPageContents( $page_id, $data=null )
		{
			// Check
			if (empty($page_id)) throw new Exception('Missing input data: page_id.');

			// Request with parameters
			if (!empty($data))
			{
				$requestData=array();
				foreach ($data as $k => $v)
				{
					$requestData[$k]=$v;
				}
				$voogResponse=$this->makeAPIrequest('/pages/'.strval($page_id).'/contents?'.BASE_HTTPREQUEST::buildQueryString($data),'GET');
			}

			// Full list
			else
			{
				$voogResponse=$this->makeAPIrequest('/pages/'.strval($page_id).'/contents');
			}

			// Build response array
			$contentsArray=array();
			foreach ($voogResponse as $content)
			{
				$contentsArray[strval($content->id)]=$content;
			}

			// Return
			return $contentsArray;
		}


		/**
		 *   Get page contents
		 *   @access public
		 *   @param int $page_id Page ID
		 *   @param int $content_id Content ID
		 *   @throws Exception
		 *   @return array
		 */

		public function getPageContent( $page_id, $content_id )
		{
			// Check
			if (empty($page_id)) throw new Exception('Missing input data: page_id.');
			if (empty($content_id)) throw new Exception('Missing input data: content_id.');

			// Make response
			return $this->makeAPIrequest('/pages/'.strval($page_id).'/contents/'.strval($content_id));
		}


		/**
		 *   Create a page
		 *   @access public
		 *   @param int $pageID Page ID
		 *   @param array $data Content data
		 *   @throws Exception
		 *   @return array
		 */

		public function addPageContent( $pageID, $data=null )
		{
			// Check
			if (empty($pageID)) throw new Exception('Missing input data: pageID.');
			if (empty($data)) throw new Exception('Missing input data: data.');

			// Make response
			return $this->makeAPIrequest('/pages/'.strval($pageID).'/contents','POST',$data);
		}


		/**
		 *   Get text content
		 *   @access public
		 *   @param int $id Content ID
		 *   @throws Exception
		 *   @return array
		 */

		public function getContentText( $id )
		{
			// Check
			if (empty($id)) throw new Exception('Missing input data: id.');

			// Make response
			return $this->makeAPIrequest('/texts/'.strval($id));
		}


		/**
		 *   Update text content
		 *   @access public
		 *   @param int $id Text ID
		 *   @param string $body Text body
		 *   @param array $data Additional text element data
		 *   @throws Exception
		 *   @return array
		 */

		public function updateContentText( $id, $body, $data=null )
		{
			// Check
			if (empty($id)) throw new Exception('Missing input data: id.');

			// Create data
			$requestData=new stdClass();
			$requestData->body=strval($body);
			if (!empty($data))
			{
				foreach ($data as $k => $v)
				{
					$requestData->{$k}=$v;
				}
			}

			// Make response
			return $this->makeAPIrequest('/texts/'.strval($id),'PUT',$requestData);
		}


		/**
		 *   Delete page
		 *   @access public
		 *   @param int $id Page ID
		 *   @throws Exception
		 *   @return object
		 *
		 */

		public function deletePageContent( $page_id, $content_id )
		{
			// Check
			if (empty($page_id)) throw new Exception('Missing input data: page_id.');
			if (empty($content_id)) throw new Exception('Missing input data: content_id.');

			// Delete
			return $this->makeAPIrequest('/pages/'.strval($page_id).'/contents/'.strval($content_id),'DELETE');
		}


		/**
		 *   Add an article (nog post)
		 *   @access public
		 *   @param int $pageID Page ID
		 *   @param string $title
		 *   @param string $excerpt
		 *   @param string $body
		 *   @param array|object $data Article parameters
		 *   @param array|object $articleData Additional key/value data
		 *   @param bool $publish Publish article
		 *   @throws Exception
		 *   @return array
		 */

		public function addArticle( $pageID, $title, $excerpt='', $body='', $data=null, $articleData=null, $publish=false )
		{
			// Check
			if (empty($pageID)) throw new Exception('Missing input data: pageID.');
			if (empty($title)) throw new Exception('Missing input data: title.');

			// Create data
			$requestData=new stdClass();
			$requestData->page_id=intval($pageID);
			$requestData->autosaved_title=strval($title);
			if (!empty($excerpt)) $requestData->autosaved_excerpt=strval($excerpt);
			if (!empty($body)) $requestData->autosaved_body=strval($body);
			if (!empty($data) && (is_array($data) || is_object($data)))
			{
				foreach ($data as $k => $v)
				{
					$requestData->{$k}=$v;
				}
			}
			if (!empty($articleData) && (is_array($articleData) || is_object($articleData)))
			{
				$requestData->data=new stdClass();
				foreach ($articleData as $k => $v)
				{
					$requestData->data->{$k}=$v;
				}
			}
			if ($publish)
			{
				$requestData->publishing=true;
				$requestData->published=true;
			}

			// Make response
			return $this->makeAPIrequest('/articles','POST',$requestData);
		}


		/**
		 *   Add an article (nog post)
		 *   @access public
		 *   @param int $id Article ID
		 *   @param array|object $data Article parameters
		 *   @param array|object $articleData Additional key/value data
		 *   @param bool $publish Publish article
		 *   @throws Exception
		 *   @return array
		 */

		public function updateArticle( $id, $data=null, $articleData=null )
		{
			// Check
			if (empty($id)) throw new Exception('Missing input data: id.');
			if (empty($title)) throw new Exception('Missing input data: title.');

			// Create data
			$requestData=new stdClass();
			if (!empty($data) && (is_array($data) || is_object($data)))
			{
				foreach ($data as $k => $v)
				{
					$requestData->{$k}=$v;
				}
			}
			if (!empty($articleData) && (is_array($articleData) || is_object($articleData)))
			{
				$requestData->data=new stdClass();
				foreach ($articleData as $k => $v)
				{
					$requestData->data->{$k}=$v;
				}
			}

			// Make response
			return $this->makeAPIrequest('/articles/'.strval($id),'PATCH',$requestData);
		}


		/**
		 *   Delete artcle (blog post)
		 *   @access public
		 *   @param int $id Article ID
		 *   @throws Exception
		 *   @return object
		 *
		 */

		public function deleteArticle( $id )
		{
			return $this->makeAPIrequest('/articles/'.strval($id),'DELETE');
		}


		/**
		 *   Get article contents
		 *   @access public
		 *   @param int $id Article ID
		 *   @throws Exception
		 *   @return array
		 */

		public function getArticleContents( $article_id, $data=null )
		{
			// Check
			if (empty($article_id)) throw new Exception('Missing input data: article_id.');

			// Request with parameters
			if (!empty($data))
			{
				$requestData=array();
				foreach ($data as $k => $v)
				{
					$requestData[$k]=$v;
				}
				$voogResponse=$this->makeAPIrequest('/articles/'.strval($article_id).'/contents?'.BASE_HTTPREQUEST::buildQueryString($data),'GET');
			}

			// Full list
			else
			{
				$voogResponse=$this->makeAPIrequest('/articles/'.strval($article_id).'/contents');
			}

			// Build response array
			$contentsArray=array();
			foreach ($voogResponse as $content)
			{
				$contentsArray[strval($content->id)]=$content;
			}

			// Return
			return $contentsArray;
		}


		/**
		 *   Get article contents
		 *   @access public
		 *   @param int $article_id Article ID
		 *   @param int $content_id Content ID
		 *   @throws Exception
		 *   @return array
		 */

		public function getArticleContent( $article_id, $content_id )
		{
			// Check
			if (empty($article_id)) throw new Exception('Missing input data: article_id.');
			if (empty($content_id)) throw new Exception('Missing input data: content_id.');

			// Make response
			return $this->makeAPIrequest('/articles/'.strval($article_id).'/contents/'.strval($content_id));
		}


		/**
		 *   Create a article
		 *   @access public
		 *   @param int $articleID Article ID
		 *   @param array $data Content data
		 *   @throws Exception
		 *   @return array
		 */

		public function addArticleContent( $articleID, $data=null )
		{
			// Check
			if (empty($articleID)) throw new Exception('Missing input data: articleID.');
			if (empty($data)) throw new Exception('Missing input data: data.');

			// Make response
			return $this->makeAPIrequest('/articles/'.strval($articleID).'/contents','POST',$data);
		}


		/**
		 *   Delete article content
		 *   @access public
		 *   @param int $id Article ID
		 *   @throws Exception
		 *   @return object
		 *
		 */

		public function deleteArticleContent( $article_id, $content_id )
		{
			// Check
			if (empty($article_id)) throw new Exception('Missing input data: article_id.');
			if (empty($content_id)) throw new Exception('Missing input data: content_id.');

			// Delete
			return $this->makeAPIrequest('/articles/'.strval($article_id).'/contents/'.strval($content_id),'DELETE');
		}


		/**
		 *   Get media sets
		 *   @access public
		 *   @param array|object $data Filter data
		 *   @throws Exception
		 *   @return object
		 */

		public function getMediaSets( $data=null )
		{
			// Request with parameters
			if (!empty($data))
			{
				$requestData=array();
				foreach ($data as $k => $v)
				{
					$requestData[$k]=$v;
				}
				$voogResponse=$this->makeAPIrequest('/media_sets?'.BASE_HTTPREQUEST::buildQueryString($data));
			}

			// Full list
			else
			{
				$voogResponse=$this->makeAPIrequest('/media_sets');
			}

			// Build response array
			$mediasetArray=array();
			foreach ($voogResponse as $mediaset)
			{
				$mediasetArray[strval($mediaset->id)]=$mediaset;
			}

			// Return
			return $mediasetArray;
		}


		/**
		 *   Get media set
		 *   @access public
		 *   @param int $id Media set ID
		 *   @throws Exception
		 *   @return array
		 */

		public function getMediaSet( $id )
		{
			// Check
			if (empty($id)) throw new Exception('Missing input data: id.');

			// Make response
			return $this->makeAPIrequest('/media_sets/'.strval($id));
		}


		/**
		 *   Get media set by name
		 *   @access public
		 *   @param string $name Media set name
		 *   @throws Exception
		 *   @return array
		 */

		public function getMediaSetByName( $name )
		{
			// Check
			if (empty($name)) throw new Exception('Missing input data: name.');

			// Get list
			$mediaSetList=$this->getMediaSets(array('q.media_set.title.$eq'=>$name));
			if (!sizeof($mediaSetList)) throw new Exception('Media set '.$name.' not found.');
			if (sizeof($mediaSetList)>1) throw new Exception('Multiple matches for '.$name.' found.');

			// Return
			foreach ($mediaSetList as $mediaset)
			{
				return $this->getMediaSet(strval($mediaset->id));
			}
		}


		/**
		 *   Add a new media set
		 *   @access public
		 *   @param string $name Media set name
		 *   @param array $data Additional data
		 *   @throws Exception
		 *   @return array
		 */

		public function addMediaSet( $title, $data=null )
		{
			// Check
			if (empty($title)) throw new Exception('Missing input data: title.');

			// Create data
			$requestData=new stdClass();
			$requestData->title=$title;
			if (!empty($data))
			{
				foreach ($data as $k => $v)
				{
					$requestData->{$k}=$v;
				}
			}

			// Make response
			return $this->makeAPIrequest('/media_sets','POST',$requestData);
		}



		/**
		 *   Get media set
		 *   @access public
		 *   @param int $id Media set ID
		 *   @throws Exception
		 *   @return array
		 */

		public function addAssetsToMediaset( $id, $assetList )
		{
			// Check
			if (empty($id)) throw new Exception('Missing input data: id.');
			if (!is_array($assetList)) throw new Exception('Missing/invalid input data: assetList.');
			if (empty($assetList)) throw new Exception('Asset list empty.');

			// Object
			$requestData=new stdClass();
			$requestData->asset_ids=$assetList;

			// Make response
			return $this->makeAPIrequest('/media_sets/'.strval($id).'/add_assets','POST',$requestData);
		}



		/**
		 *   Get assets
		 *   @access public
		 *   @param array|object $data Filter data
		 *   @throws Exception
		 *   @return object
		 */

		public function getAssets( $data=null )
		{
			// Request with parameters
			if (!empty($data))
			{
				$requestData=array();
				foreach ($data as $k => $v)
				{
					$requestData[$k]=$v;
				}
				$voogResponse=$this->makeAPIrequest('/assets?'.BASE_HTTPREQUEST::buildQueryString($data));
			}

			// Full list
			else
			{
				$voogResponse=$this->makeAPIrequest('/assets');
			}

			// Build response array
			$assetArray=array();
			foreach ($voogResponse as $asset)
			{
				$assetArray[strval($asset->id)]=$asset;
			}

			// Return
			return $assetArray;
		}


		/**
		 *   Get asset info
		 *   @access public
		 *   @param int $id Asset ID
		 *   @throws Exception
		 *   @return array
		 */

		public function getAsset( $id )
		{
			// Check
			if (empty($id)) throw new Exception('Missing input data: id.');

			// Make response
			return $this->makeAPIrequest('/assets/'.strval($id));
		}


		/**
		 *   Get asset by filename
		 *   @access public
		 *   @param string $name Media set name
		 *   @throws Exception
		 *   @return array
		 */

		public function getAssetByFilename( $filename )
		{
			// Check
			if (empty($filename)) throw new Exception('Missing input data: filename.');

			// Get list
			$assetList=$this->getAssets(array('name'=>$filename));
			if (!sizeof($assetList)) throw new Exception('Asset '.$filename.' not found.');
			if (sizeof($assetList)>1) throw new Exception('Multiple matches for '.$filename.' found.');

			// Return
			foreach ($assetList as $asset)
			{
				return $this->getAsset(strval($asset->id));
			}
		}


		/**
		 *   Upload asset
		 *   @access public
		 *   @param string $sourceFilename Source file name
		 *   @param string $filename File name at Voog
		 *   @param string $contentType MIME content type
		 *   @param int $mediaSetID Media set ID (optional)
		 *   @throws Exception
		 *   @return array
		 */

		public function uploadAsset( $sourceFilename, $filename=null, $contentType=null, $mediaSetID=null )
		{
			// Check
			if (empty($sourceFilename)) throw new Exception('Missing input data: sourceFilename.');
			if (!is_readable($sourceFilename)) throw new Exception('Source file not readable.');

			// Check/get filename
			if (empty($filename))
			{
				$filename=pathinfo($sourceFilename,PATHINFO_BASENAME);
			}

			// Check/get content-type
			if (empty($contentType))
			{
				$contentType=BASE::getMimeType($filename);
			}

			// Initialize file upload
			$uploadInitData=array();
			$uploadInitData['filename']=$filename;
			$uploadInitData['content_type']=$contentType;
			$uploadInitData['size']=filesize($sourceFilename);
			$voogUploadInitResponse=$this->makeAPIrequest('/assets','POST',$uploadInitData);

			// Check
			if (empty($voogUploadInitResponse->id)) throw new Exception('Upload init failed: no id in response.');
			if (empty($voogUploadInitResponse->upload_url)) throw new Exception('Upload init failed: no upload URL in response.');
			if (empty($voogUploadInitResponse->confirm_url)) throw new Exception('Upload init failed: no confirm URL in response.');

			// If this fails, we need to delete the original file
			try
			{
				// Upload file
				try
				{
					$HTTP=new BASE_HTTPREQUEST();
					$HTTP->setURL($voogUploadInitResponse->upload_url);
					$HTTP->setRequestMethod('PUT');
					$HTTP->setRawPostData(file_get_contents($sourceFilename));
					$HTTP->setRequestHeader('x-amz-acl','public-read');
					$HTTP->setRequestHeader('Content-Type',$contentType);
					$amazonUploadResponse=$HTTP->send();
				}
				catch (Exception $e)
				{
					throw new Exception('HTTP error talking to Amazon: '.$e->getMessage());
				}

				// Check response
				if ($HTTP->getResponseCode()<200 || $HTTP->getResponseCode()>299)
				{
					// Try to see if we have an error
					if (mb_strlen($amazonUploadResponse))
					{
						try
						{
							$amazonUploadResponse=new SimpleXMLElement($amazonUploadResponse);
						}
						catch (Exception $e)
						{
							throw new Exception('Error uploading file: got HTTP '.$HTTP->getResponseCode().' error from Amazon.');
						}
						$errorMessage=(!empty(strval($amazonUploadResponse->Code))?strval($amazonUploadResponse->Code).' - ':'').strval($amazonUploadResponse->Message);
						throw new Exception('Error from Amazon: '.$errorMessage);
					}
					else
					{
						throw new Exception('Error uploading file: got HTTP '.$HTTP->getResponseCode().' error from Amazon.');
					}
				}

				// Confirm
				$voogConfirmResponse=$this->makeAPIrequest('/assets/'.strval($voogUploadInitResponse->id).'/confirm','PUT');

				// Move to media set?
				if (!empty($voogConfirmResponse->id) && !empty($mediaSetID))
				{
					$moveResponse=$this->addAssetsToMediaset($mediaSetID,array(strval($voogConfirmResponse->id)));
				}

				return $voogConfirmResponse;
			}
			catch (Exception $e)
			{
				$error=$e->getMessage();
				try
				{
					$this->makeAPIrequest('/assets/'.strval($voogUploadInitResponse->id),'DELETE');
				}
				catch (Exception $e)
				{
					throw new Exception($error.' // Additionally, trying to delete the pending asset failed.');
				}
				throw new Exception($error);
			}
		}


		/**
		 *   Delete asset
		 *   @access public
		 *   @param int $id Page ID
		 *   @throws Exception
		 *   @return object
		 *
		 */

		public function deleteAsset( $id )
		{
			return $this->makeAPIrequest('/assets/'.strval($id),'DELETE');
		}


		/**
		 *   Get asset public URL for given size
		 *   @access public
		 *   @param object $assetObject Asset info object
		 *   @param array $sizeArray Size preference array
		 *   @param bool $protocolLess Return protocol-less link
		 *   @throws Exception
		 *   @return array
		 */

		public function getAssetUrlForSize( $assetObject, $sizeArray=array(), $protocolLess=true )
		{
			// Check
			if (!is_object($assetObject)) throw new Exception('Input parameter assetObject not an object.');
			if (empty($assetObject->id)) throw new Exception('Input parameter assetObject does not look like an asset object.');
			if (empty($assetObject->size)) throw new Exception('Input parameter assetObject does not look like an asset object.');

			// Check for specific size
			$url='';
			if (!empty($assetObject->sizes))
			{
				foreach ($sizeArray as $size)
				{
					foreach ($assetObject->sizes as $assetSize)
					{
						if (strval($assetSize->thumbnail)==$size)
						{
							$url=strval($assetSize->public_url);
							break 2;
						}
					}
				}
			}

			// If nothing found, then use full size url
			if (empty($url))
			{
				$url=strval($assetObject->public_url);
			}

			// Remove protocol
			if ($protocolLess)
			{
				$url=preg_replace("/^([A-Za-z]+):/","",$url);
			}

			// Return
			return $url;
		}


		/**
		 *   Get layouts
		 *   @access public
		 *   @param array $data Request data
		 *   @param bool $listOnly Return displayable list only
		 *   @throws Exception
		 *   @return array
		 */

		public function getLayouts( $data=null, $listOnly=false )
		{
			// Request with parameters
			if (!empty($data))
			{
				$requestData=array();
				foreach ($data as $k => $v)
				{
					$requestData[$k]=$v;
				}
				$voogResponse=$this->makeAPIrequest('/layouts?'.BASE_HTTPREQUEST::buildQueryString($data));
			}

			// Full list
			else
			{
				$voogResponse=$this->makeAPIrequest('/layouts');
			}

			// Build response array
			$layoutArray=array();
			foreach ($voogResponse as $layout)
			{
				$layoutArray[strval($layout->id)]=($listOnly?strval($layout->title):$layout);
			}

			// Return
			return $layoutArray;
		}


		/**
		 *   Get element definition by name
		 *   @access public
		 *   @param string $name Element definition name
		 *   @throws Exception
		 *   @return array
		 */

		public function getLayoutByName( $name )
		{
			// Check
			if (empty($name)) throw new Exception('Missing input data: name.');

			// Get list
			$layoutList=$this->getLayouts(array('q.layout.title.$eq'=>$name),false);
			if (!sizeof($layoutList)) throw new Exception('Layout '.$name.' not found.');
			if (sizeof($layoutList)>1) throw new Exception('Multiple matches for '.$name.' found.');

			// Return
			foreach ($layoutList as $layout)
			{
				return $this->getLayout(strval($layout->id));
			}
		}


		/**
		 *   Get layout
		 *   @access public
		 *   @param int $id Layout ID
		 *   @throws Exception
		 *   @return array
		 */

		public function getLayout( $id )
		{
			// Check
			if (empty($id)) throw new Exception('Missing input data: id.');

			// Make response
			return $this->makeAPIrequest('/layouts/'.strval($id));
		}


	}


?>