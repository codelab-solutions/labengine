<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   Field class: e-mail field
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_FIELD_PASSWORD extends BASE_FIELD
	{


		/**
		 *   Is a multi-row input
		 *   @var array
		 *   @access public
		 */

		public $multirow = 2;


		/**
		 *   Set enable suggest
		 *   @access public
		 *   @param bool $enableSuggest Enable password suggestion
		 *   @return void
		 */

		public function setSuggest ( $enableSuggest )
		{
			$this->param['suggest']=$enableSuggest;
		}


		/**
		 *   Set repeat title
		 *   @access public
		 *   @param string $titleRepeat Repeat title
		 *   @return void
		 */

		public function setTitleRepeat ( $titleRepeat )
		{
			$this->param['title_repeat']=$titleRepeat;
		}


		/**
		 *   Set password strength
		 *   @access public
		 *   @param int $strength Password strength
		 *   @return void
		 */

		public function setPasswordStrength ( $strength )
		{
			$this->param['strength']=$strength;
		}


		/**
		 *   Validate contents
		 *   @access public
		 *   @param string $value Value to validate
		 *   @return void
		 */

		function validate( $value )
		{
			// Required, but not set
			if ($this->required() && !mb_strlen($value))
			{
				return '[BASE.FORM.Error.RequiredFieldEmpty]';
			}
			if ($this->required() && !mb_strlen($_POST[$this->tag.'_repeat']))
			{
				return '[BASE.FORM.Error.PasswordRepeatEmpty]';
			}

			// No point in checking further if empty
			if (!mb_strlen($value)) return;

			// Minimum length not met
			if (intval($this->param['minlength']) && strlen($value)<intval($this->param['minlength']))
			{
				return '[BASE.FORM.Error.MinLength,'.intval($this->param['minlength']).']';
			}

			// Over maximum length
			if (intval($this->param['maxlength']) && strlen($value)>intval($this->param['maxlength']))
			{
				return '[BASE.FORM.Error.MaxLength,'.intval($this->param['maxlength']).']';
			}

			// Check strength
			if (isset($this->param['strength']))
			{
				switch(intval($this->param['strength']))
				{
					case 1:
						if (!preg_match('/[A-Za-z]/',$value)) return '[BASE.FORM.ERROR.PasswordWeak1]';
						if (!preg_match('/[0-9]/',$value)) return '[BASE.FORM.ERROR.PasswordWeak1]';
						break;
					case 2:
						if (!preg_match('/[A-Z]/',$value)) return '[BASE.FORM.ERROR.PasswordWeak2]';
						if (!preg_match('/[a-z]/',$value)) return '[BASE.FORM.ERROR.PasswordWeak2]';
						if (!preg_match('/[0-9]/',$value)) return '[BASE.FORM.ERROR.PasswordWeak2]';
						if (!preg_match('/[^A-Za-z0-9]/',$value)) return '[BASE.FORM.ERROR.PasswordWeak2]';
						break;
					default:
						break;
				}
			}

			// Repeat doesn't match
			if ($value!=$_POST[$this->tag.'_repeat'])
			{
				return '[BASE.FORM.Error.PasswordRepeatMismatch]';
			}

			// Otherwise validate
			return '';
		}


		/**
		 *   Save value
		 *   @access public
		 *   @return mixed value
		 *   @throws Exception
		 */

		public function saveValue()
		{
			global $LAB;

			// Get password
			$val=$this->getValue();
			if (empty($val)) return '';

			// Return hash
			return $LAB->USER->getPasswordHash($val);
		}


		/**
		 *   No save?
		 *   @access public
		 *   @return boolean
		 */

		public function noSave()
		{
			// Nosave parameter set
			if (!empty($this->param['nosave'])) return TRUE;

			// Don't save if editing and no password set
			if (is_object($this->formObject) && !$this->required() && !mb_strlen($_POST[$this->tag])) return TRUE;

			// Have gun, will save
			return FALSE;
		}


		/**
		 *   Form triggers: pre-display
		 *   @access public
		 *   @return void
		 */

		public function triggerFormPreDisplay()
		{
			if (!empty($this->param['suggest']))
			{
				$this->param['rowclass']=(!empty($this->param['rowclass'])?' ':'').'has-feedback';
			}
			if (empty($this->param['required']) || ($this->param['required']=='add' && $this->formObject->operation=='edit'))
			{
				$this->formObject->notes[$this->tag]='<div class="'.$this->param['rowclass'].'"><span class="icon-asterisk"></span> [BASE.FORM.Notes.Password]</div>';
			}
		}


		/**
		 *  Log data
		 *  @access public
		 *  @return boolean
		 */

		public function getLogData( $dataObject, $operation, &$logValuesArray )
		{
			unset($logValuesArray[$this->tag]);

			// Pwd changed?
			if ($dataObject->_in[$this->tag]!=$dataObject->{$this->tag})
			{
				$oldVal='*** [BASE.FORM.Log.Password.Old] ***';
				$newVal='*** [BASE.FORM.Log.Password.New] ***';
			}
			elseif ($operation!='add') return;

			// Log
			$XML='<fld id="'.htmlspecialchars($this->tag).'" name="'.htmlspecialchars($this->param['title']).'">';
			if ($operation!='add' && !$this->param['forcevalue']) $XML.='<old>'.htmlspecialchars($oldVal).'</old>';
			$XML.='<new>'.htmlspecialchars($newVal).'</new>';
			$XML.='</fld>';
			return $XML;
		}


		/**
		 *   Display the label
		 *   @access public
		 *   @return string contents
		 */

		public function displayLabel( $value, $row=NULL )
		{
			$c='<label for="'.($row>1?$this->tag.'_repeat':$this->tag).'" class="'.$this->getLabelWidthClass().' control-label'.(!empty($this->param['titleclass'])?' '.$this->param['titleclass']:'').'">';
			if ($this->required()) $c.='<span class="text-danger icon-asterisk"></span> ';
			switch ($row)
			{
				case 2:
					$c.=$this->param['title_repeat'].':';
					break;
				default:
					$c.=$this->param['title'].':';
					break;
			}
			$c.='</label>';
			return $c;
		}


		/**
		 *  Display the field
		 *  @access public
		 *  @return string contents
		 */

		public function displayField( $value, $row=NULL )
		{
			// Style
			$style=array();
			if (!empty($this->param['fieldstyle'])) $style[]=$this->param['fieldstyle'];
			if (!empty($this->param['comment'])) $style[]='width: 70% !important';

			// Class
			$class=array();
			$class[]='form-control';
			if (!empty($this->param['fieldclass'])) $class[]=$this->param['fieldclass'];

			// Render
			$c='<div class="'.$this->getContentWidthClass().'">';

			$c.='<input type="password"';
			$c.=' id="'.$this->tag.($row>1?'_repeat':'').'"';
			$c.=' name="'.$this->tag.($row>1?'_repeat':'').'"';
			$c.=' autocomplete="off"';
			$c.=' class="'.join(' ',$class).'"';
			if (!empty($style)) $c.=' style="'.join('; ',$style).'"';
			if (!empty($this->param['maxlength'])) $c.=' maxlength="'.intval($this->param['maxlength']).'"';
			if (!empty($this->param['readonly'])) $c.=' readonly="readonly"';
			if (!empty($this->param['disabled'])) $c.=' disabled="disabled"';
			if (!empty($this->param['keephiddenvalue'])) $c.=' data-keephiddenvalue="1"';
			if (isset($this->param['event']) && is_array($this->param['event']))
			{
				foreach ($this->param['event'] as $eventType => $eventContent) $c.=' '.$eventType.'="'.$eventContent.'"';
			}
			if (isset($this->param['data']) && is_array($this->param['data']))
			{
				foreach ($this->param['data'] as $dataKey => $dataValue) $c.=' data-'.$dataKey.'="'.htmlspecialchars($dataValue).'"';
			}
			$c.='>';

			// Suggest
			if (!empty($this->param['suggest']) && $row==1)
			{
				$c.='<div class="form-control-feedback" style="pointer-events: auto"><a style="cursor: pointer" onclick="PASSWORD.suggestPassword(\''.htmlspecialchars($this->tag).'\')" data-toggle="tooltip" title="Suggest a secure password"><span class="icon-bulb"></span></a></div>';
			}

			// Comment
			$c.=$this->displayComment();

			$c.='</div>';

			// Return
			return $c;
		}


	}


?>