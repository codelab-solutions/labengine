<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   DigiDoc helper functions: BDOC
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_DIGIDOC_BDOC extends BASE_DIGIDOC
	{


		/**
		 *   Tmp path
		 *   @var string
		 */

		public $path = null;


		/**
		 *   Parse file
		 *   @access public
		 *   @var string $filename
         *   @var string $parseType array or object
		 *   @throws Exception
		 *   @return void
		 */

		public function parse( $filename, $parseType='array' )
		{
			global $LAB;

			// Generate tmp path
			$this->path=oneof($LAB->CONFIG->get('site.tmppath'),'/tmp');
			$this->path.='/labengine-bdoc-'.uniqid();

			// Create tmp path
			mkdir($this->path);
			if (!is_readable($this->path))
			{
				if ($LAB->DEBUG->devEnvironment) throw new Exception('Could not generate temporary directory: '.$this->path);
				throw new Exception('Could not generate temporary directory');
			}

			// Try to unpack
			$ZIP=new ZipArchive();
			if ($ZIP->open($filename)===true)
			{
				$ZIP->extractTo($this->path);
				$ZIP->close();
			}
			else
			{
				if ($LAB->DEBUG->devEnvironment) throw new Exception('Error reading .bdoc format: error unpacking ZIP');
				throw new Exception('Error reading .bdoc format');
			}

			// Check for manifest
			if (!is_readable($this->path.'/META-INF/manifest.xml'))
			{
				if ($LAB->DEBUG->devEnvironment) throw new Exception('Error reading .bdoc format: could not find META-INF/manifest.xml in '.$this->path);
				throw new Exception('Error reading .bdoc format');
			}

			// Parse manifest
			try
			{
				$MANIFEST=new DOMDocument();
				$MANIFEST->load($this->path.'/META-INF/manifest.xml');
				$ns='urn:oasis:names:tc:opendocument:xmlns:manifest:1.0';
				$fileList=$MANIFEST->documentElement->getElementsByTagNameNS($ns,'file-entry');
				$fileIdx=0;
				foreach ($fileList as $file)
				{
					$fileFullPath=$file->getAttributeNS($ns,'full-path');
					$fileContentType=$file->getAttributeNS($ns,'media-type');
					if ($fileFullPath!='/')
					{

                        $fileData = new BASE_DIGIDOC_FILE();
                        $fileData->id = intval($fileIdx);
                        $fileData->filename = $fileFullPath;
                        if (in_array($fileContentType,['','application/octet-stream']))
                        {
                            $fileData->contenttype = BASE::getMimeType($fileFullPath);
                        }
                        else
                        {
                            $fileData->contenttype = $fileContentType;
                        }
                        $fileData->size = filesize($this->path . '/' . $fileFullPath);
                        $fileData->readable = true;
                        $fileData->contentFilename = $this->path . '/' . $fileFullPath;
                        $fileData->content = file_get_contents($this->path . '/' . $fileFullPath);
                        $this->files[] = $fileData;
					}
					$fileIdx++;
				}

			}
			catch (Exception $e)
			{
				if ($LAB->DEBUG->devEnvironment) throw new Exception('Error parsing manifest.xml: '.$e->getMessage());
				throw new Exception('Error reading .bdoc format');
			}

			// Parse signatures
			try
			{
				$sigList=glob($this->path.'/META-INF/signatures*.xml');
                $sigNum = 0;
				foreach ($sigList as $sigFile)
				{
					// See if signature is readable
					if (!file_exists($sigFile) || !is_readable($sigFile)) continue;

					// Try to parse
					$SIGNATURE=new DOMDocument();
					$SIGNATURE->load($sigFile);
					$XPATH=new DOMXpath($SIGNATURE);
					$XPATH->registerNamespace('asic','http://uri.etsi.org/02918/v1.2.1#');
					$XPATH->registerNamespace('ds','http://www.w3.org/2000/09/xmldsig#');
					$XPATH->registerNamespace('xades','http://uri.etsi.org/01903/v1.3.2#');


                        $Signature=new BASE_BDOC_SIGNATURE();
                        $Signature->XML=file_get_contents($sigFile);


                        // Get X.509 signature
                        $x509signatureList=$XPATH->query('/asic:XAdESSignatures/ds:Signature/ds:KeyInfo/ds:X509Data/ds:X509Certificate',$SIGNATURE);
                        foreach ($x509signatureList as $x509signature)
                        {
                            $certificateData=openssl_x509_parse('-----BEGIN CERTIFICATE-----'."\n".trim(strval($x509signature->nodeValue))."\n".'-----END CERTIFICATE-----');
                            if ($certificateData!==false && is_array($certificateData))
                            {
                                $Signature->certificate=$certificateData['name'];
                                $Signature->country=$certificateData['subject']['C'];
                                $Signature->name_last=$certificateData['subject']['SN'];
                                $Signature->name_first=$certificateData['subject']['GN'];

                                $Signature->idcode=(mb_strpos($certificateData['subject']['serialNumber'],'-')!==false?str_array($certificateData['subject']['serialNumber'],'-',2)[1]:$certificateData['subject']['serialNumber']);


                            }
                        }

                        // See if there's signed time
                        $signedTimeList=$XPATH->query('/asic:XAdESSignatures/ds:Signature/ds:Object/xades:QualifyingProperties/xades:SignedProperties/xades:SignedSignatureProperties/xades:SigningTime',$SIGNATURE);
                        foreach ($signedTimeList as $signedTime)
                        {
                            $Signature->signed_time=strtotime(strval($signedTime->nodeValue));
                        }

                        // See if there's a role
                        $roleList=$XPATH->query('/asic:XAdESSignatures/ds:Signature/ds:Object/xades:QualifyingProperties/xades:SignedProperties/xades:SignedSignatureProperties/xades:SignerRole/xades:ClaimedRoles/xades:ClaimedRole',$SIGNATURE);
                        foreach ($roleList as $role)
                        {
                            $Signature->role=strval($role->nodeValue);
                        }

                        // Address components
                        $addressCityList=$XPATH->query('/asic:XAdESSignatures/ds:Signature/ds:Object/xades:QualifyingProperties/xades:SignedProperties/xades:SignedSignatureProperties/xades:SignatureProductionPlace/xades:City',$SIGNATURE);
                        foreach ($addressCityList as $addressCity)
                        {
                            $Signature->addressCity=strval($addressCity->nodeValue);
                        }
                        $addressStateList=$XPATH->query('/asic:XAdESSignatures/ds:Signature/ds:Object/xades:QualifyingProperties/xades:SignedProperties/xades:SignedSignatureProperties/xades:SignatureProductionPlace/xades:StateOrProvince',$SIGNATURE);
                        foreach ($addressStateList as $addressState)
                        {
                            $Signature->addressState=strval($addressState->nodeValue);
                        }
                        $addressCountryList=$XPATH->query('/asic:XAdESSignatures/ds:Signature/ds:Object/xades:QualifyingProperties/xades:SignedProperties/xades:SignedSignatureProperties/xades:SignatureProductionPlace/xades:CountryName',$SIGNATURE);
                        foreach ($addressCountryList as $addressCountry)
                        {
                            $Signature->addressCountry=strval($addressCountry->nodeValue);
                        }
                        $addressZIPList=$XPATH->query('/asic:XAdESSignatures/ds:Signature/ds:Object/xades:QualifyingProperties/xades:SignedProperties/xades:SignedSignatureProperties/xades:SignatureProductionPlace/xades:PostalCode',$SIGNATURE);
                        foreach ($addressZIPList as $addressZIP)
                        {
                            $Signature->addressZIP=strval($addressZIP->nodeValue);
                        }


					// Add
					if (!empty($Signature))
					{
						$this->signatures[]=$Signature;
					}

					// Continue
					$sigNum++;
				}
			}
			catch (Exception $e)
			{
				if ($LAB->DEBUG->devEnvironment) throw new Exception('Error parsing signature #'.intval($sigNum).': '.$e->getMessage());
				throw new Exception('Error reading .bdoc format');
			}
		}


        /**
         *   Create a new container
         *   @access public
         *   @throws Exception
         *   @static
         *   @return BASE_DIGIDOC_BDOC
         */

        public static function createContainer()
        {
            global $LAB;

            // Check for necessary functionality
            if (!class_exists('\\ZipArchive')) throw new Exception('Zip extension not found.');
            if (!class_exists('\\DomDocument')) throw new Exception('DOM extension not found.');
            if (!function_exists('openssl_x509_parse')) throw new Exception('OpenSSL extension not found.');

            // Create
            $BDOC=new BASE_DIGIDOC_BDOC();

            // Check and create temp path
            $BDOC->path=$LAB->CONFIG->get('site.tmppath').'/labengine-digidoc-'.uniqid();
            mkdir($BDOC->path);
            if (!is_writable($BDOC->path))
            {
                if ($LAB->DEBUG->devEnvironment) throw new Exception('Failed to create temp directory: '.$BDOC->path);
                throw new Exception('Failed to create temp directory.');
            }

            // Write mimetype file
            file_put_contents($BDOC->path.'/mimetype','application/vnd.etsi.asic-e+zip');

            // Create META-INF folder
            mkdir($BDOC->path.'/META-INF');

            // Write manifest
            $BDOC->writeManifest();

            // Return container
            return $BDOC;
        }


        /**
         *
         *   Add a file to container
         *   -----------------------
         *   @access public
         *   @param string $sourceFile Source file
         *   @param string $fileName File name (if not the same as source file)
         *   @throws Exception
         *   @return void
         *
         */

        public function addFile( $sourceFile, $fileName=null )
        {
            global $LAB;

            // Initial checks
            if (empty($this->path)) throw new Exception('Temp directory not found.');
            if (!is_writable($this->path))
            {
                if ($LAB->DEBUG->devEnvironment) throw new Exception('Temp directory not writable: '.$this->path);
                throw new Exception('Temp directory not writable.');
            }

            // Can't add more files if already signed
            if (sizeof($this->signatures))
            {
                throw new Exception("Cannot add files to a container that's already signed.");
            }

            // Check source file
            if (!file_exists($sourceFile))
            {
                throw new Exception('Source file does not exist.');
            }
            if (!is_readable($sourceFile))
            {
                throw new Exception('Source file not readable.');
            }

            // Detect filename if not passed
            if ($fileName===null)
            {
                $fileName=pathinfo($sourceFile,PATHINFO_FILENAME);
            }

            // Check for duplicate
            if (file_exists($this->path.'/'.$fileName))
            {
                throw new Exception('File already exists in the container.');
            }

            // Write file
            file_put_contents($this->path.'/'.$fileName,file_get_contents($sourceFile));

            // Add file to structure
            $FILE=new BASE_DIGIDOC_FILE();
            $FILE->id=intval(sizeof($this->files));
            $FILE->filename=$fileName;
            $FILE->contenttype=BASE_FILE::getMimeType($this->path.'/'.$fileName);
            $FILE->size=filesize($this->path.'/'.$fileName);
            $FILE->readable=true;
            $FILE->contentFilename=$this->path.'/'.$fileName;
            $this->files[]=$FILE;

            // Write manifest
            $this->writeManifest();
        }


        /**
         *
         *   Add a file to container from string
         *   -----------------------------------
         *   @access public
         *   @param string $fileContent File content
         *   @param string $fileName File name
         *   @throws Exception
         *   @return void
         *
         */

        public function addFileFromString( $fileContent, $fileName )
        {
            global $LAB;

            // Initial checks
            if (empty($this->path)) throw new Exception('Temp directory not found.');
            if (!is_writable($this->path))
            {
                if ($LAB->DEBUG->devEnvironment) throw new Exception('Temp directory not writable: '.$this->path);
                throw new Exception('Temp directory not writable.');
            }

            // Can't add more files if already signed
            if (sizeof($this->signatures))
            {
                throw new Exception("Cannot add files to a container that's already signed.");
            }

            // Check file
            if (file_exists($this->path.'/'.$fileName))
            {
                throw new Exception('File already exists in the container.');
            }

            // Write file
            file_put_contents($this->path.'/'.$fileName,$fileContent);

            // Add file to structure
            $FILE=new BASE_DIGIDOC_FILE();
            $FILE->id=intval(sizeof($this->files));
            $FILE->filename=$fileName;
            $FILE->contenttype=BASE_FILE::getMimeType($this->path.'/'.$fileName);
            $FILE->size=filesize($this->path.'/'.$fileName);
            $FILE->readable=true;
            $FILE->contentFilename=$this->path.'/'.$fileName;
            $this->files[]=$FILE;

            // Write manifest
            $this->writeManifest();
        }


        /**
         *
         *   Get DigiDoc container file
         *   --------------------------
         *   @access public
         *   @param bool $getHashcoded Get hashcoded version?
         *   @throws Exception
         *   @return string
         *
         */

        public function getDigiDoc( $getHashcoded=false )
        {
            global $LAB;

            // Initial checks
            if (empty($this->path)) throw new Exception('Temp directory not found.');
            if (!sizeof($this->files)) throw new Exception('No files in the container.');

            // Check outfile
            $zipFileName=$this->path.'.bdoc';
            if (file_exists($zipFileName)) throw new Exception('A container already exists in the temp location. Previous getDigiDoc() failed?');

            // Create ZIP container
            try
            {
                // Create file
                $ZIP=new \ZipArchive();
                $ZIP->open($zipFileName,\ZipArchive::CREATE);

                // Add mimetype
                $ZIP->addFromString('mimetype','application/vnd.etsi.asic-e+zip');
                $ZIP->setCompressionIndex(0, \ZipArchive::CM_STORE);

                // Add manifest
                $ZIP->addFromString('META-INF/manifest.xml',$this->getManifest());

                // Add signatures
                foreach ($this->signatures as $sNum => $Signature)
                {
                    $ZIP->addFromString('META-INF/signatures'.intval($sNum).'.xml',$Signature->getXML());
                }

                // Add files
                if ($getHashcoded)
                {
                    $ZIP->addFromString('META-INF/hashcodes-sha256.xml',$this->getHashcodes('sha256'));
                    $ZIP->addFromString('META-INF/hashcodes-sha512.xml',$this->getHashcodes('sha512'));
                }
                else
                {

                    foreach ($this->files as $FILE)
                    {
                        $ZIP->addFile($this->path.'/'.$FILE->filename,$FILE->filename);
                    }
                }

                // Close & write
                $ZIP->close();

                // Delete temp file and return
                $ZIP=file_get_contents($zipFileName);
                unlink($zipFileName);
                return $ZIP;
            }
            catch (\Exception $e)
            {
                if ($LAB->DEBUG->devEnvironment) throw new Exception('Failed to create BDOC container: '.$e->getMessage());
                throw new Exception('Failed to create Bdoc container.');
            }
        }


		/**
		 *   Close/cleanup
		 *   @access public
		 *   @var string $filename
		 *   @throws Exception
		 *   @return void
		 */

		public function close()
		{
			global $LAB;

			// Delete temporary content
			if (!empty($this->path))
			{
				exec_with_cwd('/bin/rm -rf '.$this->path,oneof($LAB->CONFIG->get('site.tmppath'),'/tmp'), $errors);
			}
		}



        /**
         *
         *   Get manifest
         *   ------------
         *   @access private
         *   @throws Exception
         *   @return void
         *
         */

        private function getManifest()
        {
            global $LAB;

            // Create manifest
            $manifest='<?xml version="1.0" encoding="UTF-8" standalone="no" ?>'."\n";
            $manifest.='<manifest:manifest xmlns:manifest="urn:oasis:names:tc:opendocument:xmlns:manifest:1.0">'."\n";

            // Root entry
            $manifest.='  <manifest:file-entry manifest:full-path="/" manifest:media-type="application/vnd.etsi.asic-e+zip"/>'."\n";

            // Write files
            foreach ($this->files as $FILE)
            {
                $manifest.='  <manifest:file-entry manifest:full-path="'.htmlspecialchars($FILE->filename).'" manifest:media-type="'.htmlspecialchars($FILE->contenttype).'"/>'."\n";
            }

            // Close and return
            $manifest.='</manifest:manifest>'."\n";
            return $manifest;
        }


        /**
         *
         *   Write manifest
         *   --------------
         *   @access private
         *   @throws Exception
         *   @return void
         *
         */

        private function writeManifest()
        {
            global $LAB;

            // Checks
            if (empty($this->path)) throw new Exception('Temp directory not found.');
            if (!is_writable($this->path))
            {
                if ($LAB->DEBUG->devEnvironment) throw new Exception('Failed to create temp directory: '.$this->path);
                throw new Exception('Failed to create temp directory.');
            }
            if (!is_writable($this->path.'/META-INF'))
            {
                if ($LAB->DEBUG->devEnvironment) throw new Exception('Temp directory not writable: '.$this->path.'/META-INF');
                throw new Exception('Temp directory not writable.');
            }

            // Create manifest
            file_put_contents($this->path.'/META-INF/manifest.xml',$this->getManifest());
        }


        /**
         *
         *   Get hashcodes XML
         *   -----------------
         *   @access private
         *   @param string $hashMethod Hash method
         *   @throws Exception
         *   @return string
         *
         */

        private function getHashcodes( $hashMethod )
        {
            global $LAB;

            // File starts
            $hashCodes='<?xml version="1.0" encoding="utf-8"?>'."\n";
            $hashCodes.='<hashcodes>'."\n";

            // Iterate through files
            foreach ($this->files as $FILE)
            {
                switch ($hashMethod)
                {
                    case 'sha256':
                    case 'sha512':
                        $hash=hash($hashMethod,file_get_contents($this->path.'/'.$FILE->filename),true);
                        break;
                    default:
                        throw new Exception('Unknown hashMethod: '.$hashMethod);
                }
                $hashCodes.='  <file-entry full-path="'.htmlspecialchars($FILE->filename).'" hash="'.htmlspecialchars(base64_encode($hash)).'" size="'.intval($FILE->size).'" />'."\n";
            }

            // End and return
            $hashCodes.='</hashcodes>'."\n";
            return $hashCodes;
        }



    }


?>