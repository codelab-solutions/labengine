<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   Field class: inline chooser field
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_FIELD_INLINECHOOSER extends BASE_FIELD
	{


		/**
		 *   Allow multiple items to be selected
		 *   @param bool $allowMultiple Allow multiple
		 */

		public function setAllowMultiple( $allowMultiple )
		{
			$this->param["multiple"]=$allowMultiple;
		}


		/**
		 *   Set chooser URL
		 *   @param string $chooser_url Chooser URL
		 */

		public function setChooserURL( $chooser_url )
		{
			$this->param["chooser_url"]=$chooser_url;
		}


		/**
		 *   Set chooser placeholder
		 *   @param string $chooser_placeholder Chooser placeholder
		 */

		public function setChooserPlaceholder( $chooser_placeholder )
		{
			$this->param["chooser_placeholder"]=$chooser_placeholder;
		}


		/**
		 *   Set empty value
		 *   @param string $chooser_emptyvalue Empty value title
		 */

		public function setChooserEmptyValue( $chooser_emptyvalue )
		{
			$this->param["chooser_emptyvalue"]=$chooser_emptyvalue;
		}


		/**
		 *   Set allow clearing of value
		 *   @param string $chooser_clear Allow clear value
		 */

		public function setChooserClear( $chooser_clear )
		{
			$this->param["chooser_clear"]=$chooser_clear;
		}


		/**
		 *   Set minimum search length
		 *   @param int $minimum_search_length Minimum search term length
		 */

		public function setMinimumSearchLength( $minimum_search_length )
		{
			$this->param["minimum_search_length"]=$minimum_search_length;
		}


		/**
		 *   Display form value
		 *   @param string $value Value
		 *   @return string
		 */

		public function displayFormValue( $value )
		{
			global $LAB;

			switch ($this->fieldSource)
			{
				case 'list':
					return $this->param['source_list'][$value];
				case 'query':
					$dataset=$LAB->DB->querySelectSQL($this->param['source_query'],'value');
					return $dataset[$value]['description'];
				case 'dao':
				case 'dataobject':
					if ($this->fieldSourceDAO instanceof BASE_DATAOBJECT)
					{
						$DAO=$this->fieldSourceDAO;
					}
					else
					{
						$cName=$this->fieldSourceDAO;
						$DAO=new $cName();
					}
					$DAO->load($value);
					return $DAO->{$this->fieldSourceDAOvalueField};
				default:
					return $value;
			}
		}


		/**
		 *  Display the field
		 *  @access public
		 *  @return string contents
		 */

		public function displayField( $value, $row=NULL )
		{
			global $LAB;

			// Style
			$style=array();
			if (!empty($this->param['fieldstyle'])) $style[]=$this->param['fieldstyle'];
			if (!empty($this->param['comment'])) $style[]='width: 70%; display: inline-block';

			// Class
			$class=array();
			$class[]='form-control';
			if (!empty($this->param['fieldclass'])) $class[]=$this->param['fieldclass'];

			// Render
			$c='<div class="field-chooser '.$this->getContentWidthClass().'" style="overflow: hidden">';

				// Current value(s)
				if (!empty($this->param['multiple']))
				{
					$value=str_array($value,"\t");
					$c.='<input type="hidden"';
						$c.=' id="'.$this->tag.'_trigger"';
						$c.='name="'.$this->tag.'_trigger"';
						$c.=' value=""';
						if (isset($this->param['event']) && is_array($this->param['event']))
						{
							foreach ($this->param['event'] as $eventType => $eventContent) $c.=' '.$eventType.'="'.$eventContent.'"';
						}
					$c.='>';
					$c.='<div id="'.$this->tag.'_value">';
					$c.='<p id="'.$this->tag.'_none" class="form-control-static field-inlinechooser-value-none" style="width: 100%'.(empty($value)?'':'; display: none').'"><span class="lightgray">'.oneof($this->param["chooser_emptyvalue"],'[BASE.COMMON.None]').'</span></p>';
					foreach ($value as $v)
					{
						$c.='<p id="'.$this->tag.'_val_'.htmlspecialchars($v).'" class="form-control-static field-inlinechooser-value" style="width: 100%">';
						$c.='<input type="hidden" id="'.$this->tag.'_'.htmlspecialchars($v).'" name="'.$this->tag.'[]" value="'.htmlspecialchars($v).'">';
						$c.='<span class="field-chooser-display">'.$this->displayFormValue($v).'</span>';
						$c.='<span class="field-chooser-choose"><a onclick="INLINECHOOSER.removeItem(\''.$this->tag.'\',\''.htmlspecialchars($v).'\')"><span class="icon-cancel"></span></a></span>';
						$c.='</p>';
					}
					$c.='</div>';
				}
				else
				{
					$c.='<p class="form-control-static" style="width: 100%">';
						$c.='<input type="hidden"';
							$c.=' id="'.$this->tag.'"';
							$c.='name="'.$this->tag.'"';
							$c.=' value="'.htmlspecialchars($value).'"';
							if (isset($this->param['event']) && is_array($this->param['event']))
							{
								foreach ($this->param['event'] as $eventType => $eventContent) $c.=' '.$eventType.'="'.$eventContent.'"';
							}
						$c.='>';
						$c.='<span class="field-chooser-display" id="'.$this->tag.'_display">';
							if (!empty($value))
							{
								$c.=$this->displayFormValue($value);
							}
							else
							{
								$c.='<span class="lightgray">'.oneof($this->param["chooser_emptyvalue"],'[BASE.COMMON.NotSet]').'</span>';
							}
						$c.='</span>';
						$c.='<span class="field-chooser-choose">';
						if (!empty($this->param['chooser_clear']))
						{
							$c.='<a onclick="INLINECHOOSER.select(\''.$this->tag.'\',\'\',\''.jsencode('<span class=lightgray>'.oneof($this->param["chooser_emptyvalue"],'[BASE.COMMON.NotSet]').'</span>').'\');$(\'#'.$this->tag.'\').trigger(\'change\');"><span class="icon-cancel"></span></a>';
						}
						$c.='</span>';
					$c.='</p>';
				}

				// Chooser
				$c.='<p class="form-control-static" style="width: 100%; border-top: 1px #e8e8e8 solid; margin-top: 10px; padding-top: 10px; position: relative">';
				$c.='<input';
					$c.=' type="text"';
					$c.=' class="form-control noautosubmit"';
					$c.=' autocomplete="off"';
					$c.=' id="'.$this->tag.'_search"';
					$c.=' name="'.$this->tag.'_search"';
					$c.=' onkeyup="INLINECHOOSER.keyPress(\''.$this->tag.'\','.(intval($this->param["minimum_search_length"]?intval($this->param["minimum_search_length"]):'3')).',\''.jsencode($this->param['chooser_url']).'\','.($this->param['multiple']?'true':'false').',event)"';
					if (!empty($this->param["chooser_placeholder"])) $c.='placeholder="'.htmlspecialchars($this->param["chooser_placeholder"]).'"';
				$c.='>';
				$c.='<div id="'.$this->tag.'_searchresults" class="field-inlinechooser-search-results" style="display: none"></div>';
				$c.='</p>';

			$c.='</div>';

			// Return
			return $c;
		}


	}


?>