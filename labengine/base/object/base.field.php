<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   The field class
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_FIELD
	{


		/**
		 *   Field tag
		 *   @var string
		 *   @access public
		 */

		public $tag = NULL;


		/**
		 *   Parameters
		 *   @var array
		 *   @access public
		 */

		public $param = NULL;


		/**
		 *   Is a multi-row input?
		 *   @var array
		 *   @access public
		 */

		public $multirow = 1;


		/**
		 *   Back-reference to data object
		 *   @var BASE_DATAOBJECT
		 *   @access public
		 */

		public $dataObject = NULL;


		/**
		 *   Back-reference to form object
		 *   @var BASE_FORM
		 *   @access public
		 */

		public $formObject = NULL;


		/**
		 *   Back-reference to list object
		 *   @var BASE_LIST
		 *   @access public
		 */

		public $listObject = NULL;


		/**
		 *   Source parameters: type
		 *   @var string
		 *     list - fixed list
		 *     dao - data object
		 *   @access public
		 */

		public $fieldSource='list';


		/**
		 *   Source parameters: data object
		 *   @var BASE_DATAOBJECT
		 *   @access public
		 */

		public $fieldSourceDAO='';


		/**
		 *   Source parameters: data object: key field
		 *   @var string
		 *   @access public
		 */

		public $fieldSourceDAOkeyField='';


		/**
		 *   Source parameters: data object: value field
		 *   @var string
		 *   @access public
		 */

		public $fieldSourceDAOvalueField='';


		/**
		 *   Source parameters: data object: load parameters
		 *   @var bool|BASE_DATAOBJECT_PARAM
		 *   @access public
		 */

		public $fieldSourceDAOloadParam=FALSE;


		/**
		 *   Allow HTML
		 *   @var bool
		 *   @access public
		 */

		public $allowHTML=FALSE;


		/**
		 *   The constructor
		 *   @access public
		 *   @param string tag
		 *   @return BASE_FIELD
		 */

		public function __construct ( $tag=NULL )
		{
			if (!empty($tag)) $this->tag=$tag;
		}


		/**
		 *   Set title
		 *   @access public
		 *   @param string title
		 *   @return void
		 */

		public function setTitle ( $title )
		{
			$this->param['title']=$title;
		}


		/**
		 *   Get title
		 *   @access public
		 *   @param string title
		 *   @return void
		 */

		public function getTitle()
		{
			return $this->param['title'];
		}


		/**
		 *   Set group
		 *   @access public
		 *   @param string group
		 *   @return void
		 */

		public function setGroup ( $group )
		{
			$this->param['fieldgroup']=$group;
		}


		/**
		 *  Set required
		 *  @access public
		 *  @param string title
		 *  @return void
		 */

		public function setRequired ( $required, $requiredCondition=NULL )
		{
			switch($required)
			{
				case 'no':
					unset($this->param['required']);
					unset($this->param['required_cond']);
					break;
				case 'always':
				case 'add':
				case 'edit':
					$this->param['required']=$required;
					unset($this->param['required_if']);
					break;
				case 'if':
					if (empty($requiredCondition))
					{
						throw new Exception('Condition required if type is "if"');
						return;
					}
					$this->param['required']='if';
					$this->param['required_cond']=$requiredCondition;
					break;
				default:
					throw new Exception('Unknown required value: '.$required);
					return;
			}
		}


		/**
		 *   Set minimum length
		 *   @access public
		 *   @param int minimum length
		 *   @return void
		 */

		public function setMinimumLength ( $length )
		{
			$this->param['minlength']=$length;
		}


		/**
		 *   Set maximum length
		 *   @access public
		 *   @param int minimum length
		 *   @return void
		 */

		public function setMaximumLength ( $length )
		{
			$this->param['maxlength']=$length;
		}


		/**
		 *   Set unique requirement
		 *   @access public
		 *   @param string title
		 *   @return void
		 */

		public function setUnique ( $unique, $uniqueCondition=NULL, $uniqueMessage=NULL )
		{
			if ($unique)
			{
				$this->param['unique']=TRUE;
				if (!empty($uniqueCondition)) $this->param['unique_cond']=$uniqueCondition;
				if (!empty($uniqueMessage)) $this->param['unique_message']=$uniqueMessage;
			}
			else
			{
				unset($this->param['unique']);
				unset($this->param['unique_cond']);
				unset($this->param['unique_message']);
			}
		}


		/**
		 *   Set row class
		 *   @access public
		 *   @param string row class
		 *   @return void
		 */

		public function setRowClass ( $rowClass )
		{
			$this->param['rowclass']=$rowClass;
		}


		/**
		 *   Set row style
		 *   @access public
		 *   @param string row class
		 *   @return void
		 */

		public function setRowStyle ( $rowStyle )
		{
			$this->param['rowstyle']=$rowStyle;
		}


		/**
		 *   Set title class
		 *   @access public
		 *   @param string title class
		 *   @return void
		 */

		public function setTitleClass ( $titleClass )
		{
			$this->param['titleclass']=$titleClass;
		}


		/**
		 *  Set field class
		 *  @access public
		 *  @param string field class
		 *  @return void
		 */

		public function setFieldClass ( $fieldClass )
		{
			$this->param['fieldclass']=$fieldClass;
		}


		/**
		 *  Set field style
		 *  @access public
		 *  @param string field style
		 *  @return void
		 */

		public function setFieldStyle ( $fieldStyle )
		{
			$this->param['fieldstyle']=$fieldStyle;
		}


		/**
		 *   Set field additional data
		 *   @access public
		 *   @param string $key Key
		 *   @param string $value Value
		 *   @return void
		 */

		public function setData ( $key, $value )
		{
			$this->param['data'][$key]=$value;
		}


		/**
		 *   Set source
		 *   @access public
		 *   @param string source
		 *   @return void
		 *   @throws Exception
		 */

		public function setSource ( $source )
		{
			switch($source)
			{
				case 'list':
				case 'query':
				case 'dao':
				case 'dataobject':
				case 'func':
					$this->fieldSource=$source;
					break;
				default:
					throw new Exception('Unknown source: '.$source);
					return;
			}
		}


		/**
		 *   Set source DAO
		 *   @access public
		 *   @param string $sourceDAO source data object
		 *   @param string $keyField key field
		 *   @param string $valueField value field
		 *   @param BASE_DATAOBJECT_PARAM $loadParam Load parameters/criteria
		 *   @return void
		 */

		public function setSourceDAO ( $sourceDAO, $keyField, $valueField, $loadParam=FALSE )
		{
			$this->fieldSource='dao';
			$this->fieldSourceDAO=$sourceDAO;
			$this->fieldSourceDAOkeyField=$keyField;
			$this->fieldSourceDAOvalueField=$valueField;
			$this->fieldSourceDAOloadParam=$loadParam;
		}


		/**
		 *   Set source list
		 *   @access public
		 *   @param array $sourceList source list
		 *   @return void
		 */

		public function setSourceList ( $sourceList )
		{
			$this->param['source_list']=$sourceList;
		}


		/**
		 *   Set source query
		 *   @access public
		 *   @param string $sourceQuery source query
		 *   @return void
		 */

		public function setSourceQuery( $sourceQuery )
		{
			$this->param['source_query']=$sourceQuery;
		}


		/**
		 *   Set default value
		 *   @access public
		 *   @param string default value
		 *   @return void
		 */

		public function setDefault ( $default )
		{
			$this->param['default']=$default;
		}


		/**
		 *   Set value
		 *   @access public
		 *   @param string value
		 *   @return void
		 */

		public function setValue ( $value )
		{
			$this->param['value']=$value;
		}


		/**
		 *   Set no save
		 *   @access public
		 *   @param boolean no save
		 *   @return void
		 */

		public function setNoSave ( $nosave )
		{
			$this->param['nosave']=$nosave;
		}


		/**
		 *   Set no log
		 *   @access public
		 *   @param boolean no log
		 *   @return void
		 */

		public function setNoLog ( $noLog )
		{
			$this->param['nolog']=$noLog;
		}


		/**
		 *   Set disabled
		 *   @access public
		 *   @param boolean is disabled
		 *   @return void
		 */

		public function setDisabled ( $disabled )
		{
			$this->param['disabled']=$disabled;
		}


		/**
		 *   Set comment
		 *   @access public
		 *   @param string comment
		 *   @return void
		 */

		public function setComment ( $comment )
		{
			$this->param['comment']=$comment;
		}


		/**
		 *   Add an event
		 *   @access public
		 *   @param string $eventType event type
		 *   @param string $eventAction event action
		 *   @param bool $runOnStartup Run this on startup (as JS/displaytrigger)
		 *   @return void
		 */

		public function addEvent ( $eventType, $eventAction, $runOnStartup=false )
		{
			if (!empty($this->param['event'][$eventType]))
			{
				$this->param['event'][$eventType].=$eventAction;
			}
			else
			{
				$this->param['event'][$eventType]=$eventAction;
			}
			if (!empty($runOnStartup) && !empty($this->formObject))
			{
				if ($this->formObject instanceof BASE_DIALOGFORM)
				{
					$this->formObject->setDisplayTrigger($eventAction);
				}
				elseif ($this->formObject instanceof BASE_FORM)
				{
					$this->formObject->addJS($eventAction);
				}
			}
		}


		/**
		 *   Set filter
		 *   @access public
		 *   @param string $filter Filter (regexp)
		 *   @param bool|string $filter_description Filter description
		 *   @return void
		 */

		public function setFilter ( $filter, $filter_description=FALSE )
		{
			$this->param['filter']=$filter;
			if (!empty($filter_description))
			{
				$this->param['filter_description']=$filter_description;
			}
		}


		/**
		 *   Set read-only
		 *   @access public
		 *   @param bool $readOnly Is read-only?
		 *   @return void
		 */

		public function setReadOnly ( $readOnly )
		{
			$this->param['readonly']=$readOnly;
		}


		/**
		 *  Set empty format
		 *  @access public
		 *  @param string $emptyFormat empty value format
		 *  @return void
		 */

		public function setEmptyFormat ( $emptyFormat )
		{
			$this->param['emptyformat']=$emptyFormat;
		}


		/**
		 *  Set keep hidden value
		 *  @access public
		 *  @param bool $keepHiddenValue Keep hidden value?
		 *  @return void
		 */

		public function setKeepHiddenValue ( $keepHiddenValue )
		{
			$this->param['keephiddenvalue']=$keepHiddenValue;
		}


		/**
		 *  Set placeholder
		 *  @access public
		 *  @param string placeholder
		 *  @return void
		 */

		public function setPlaceholder ( $placeholder )
		{
			$this->param['placeholder']=$placeholder;
		}


		/**
		 *  List parameters: column width
		 *  @access public
		 *  @param string $listFieldWidth Column width (px or %)
		 *  @return void
		 */

		public function setListFieldWidth ( $listFieldWidth )
		{
			$this->param['list_fieldwidth']=$listFieldWidth;
		}


		/**
		 *  List parameters: column alignment
		 *  @access public
		 *  @param string $listFieldAlign Column alignment (left, right, center, justify)
		 *  @return void
		 */

		public function setListFieldAlign ( $listFieldAlign )
		{
			$this->param['list_fieldalign']=$listFieldAlign;
		}


		/**
		 *  List parameters: column no-wrap
		 *  @access public
		 *  @param boolean $listNoWrap No-wrap
		 *  @return void
		 */

		public function setListNoWrap ( $listNoWrap )
		{
			$this->param['list_nowrap']=$listNoWrap;
		}


		/**
		 *  List parameters: field style
		 *  @access public
		 *  @param string $listFieldStyle Column style
		 *  @return void
		 */

		public function setListFieldStyle ( $listFieldStyle )
		{
			$this->param['list_fieldstyle']=$listFieldStyle;
		}


		/**
		 *  List parameters: field style
		 *  @access public
		 *  @param string $listFieldClass Column field class
		 *  @return void
		 */

		public function setListFieldClass ( $listFieldClass )
		{
			$this->param['list_fieldclass']=$listFieldClass;
		}


		/**
		 *  List parameters: is sortable
		 *  @access public
		 *  @param boolean $sortable Is sortable
		 *  @return void
		 */

		public function setListSortable ( $sortable )
		{
			$this->param['list_sortable']=$sortable;
		}


		/**
		 *  List parameters: sort default direction
		 *  @access public
		 *  @param boolean $sortDefault Is default sort field?
		 *  @return void
		 */

		public function setListSortDefault ( $sortDefault )
		{
			$this->param['list_sortdefault']=$sortDefault;
		}


		/**
		 *  List parameters: sort default direction
		 *  @access public
		 *  @param string $defaultSortOrder Default sort direction
		 *  @return void
		 */

		public function setListDefaultSortOrder ( $defaultSortOrder )
		{
			$this->param['list_defaultsortorder']=mb_strtolower($defaultSortOrder);
		}


		/**
		 *  List parameters: empty values to end
		 *  @access public
		 *  @param boolean $sortEmptyValuesLast Sort empty values last
		 *  @return void
		 */

		public function setListSortEmptyValuesLast ( $sortEmptyValuesLast )
		{
			$this->param['list_sortemptyvalueslast']=$sortEmptyValuesLast;
		}


		/**
		 *   List functions: get sort criteria
		 *   @access public
		 *   @param string $direction Direction (asc/desc)
		 *   @param BASE_DATAOBEJCT_PARAM $paramObject Parameters object reference
		 *   @return void
		 */

		public function getListSortCriteria ( $direction, &$paramObject )
		{
			if (strpos($this->fieldColumn,'->')!==FALSE)
			{
				$relArr=preg_split('/->/',$this->fieldColumn);
				$sortFld=$relArr[sizeof($relArr)-2].'_'.$relArr[sizeof($relArr)-1];
			}
			else
			{
				$sortFld=$this->fieldColumn;
			}
			if (!empty($this->param['list_sortemptyvalueslast'])) $paramObject->addOrderBy("if((length(".$sortFld.")=0 or ".$sortFld." is null),1,0)");
			$paramObject->addOrderBy($sortFld.' '.$direction);
		}


		/**
		 *   List functions: get list query data
		 *   @access public
		 *   @param BASE_DATAOBEJCT_PARAM $paramObject Parameters object reference
		 *   @return void
		 */

		public function getListQuery( &$paramObject )
		{
			$paramObject->addColumns(oneof($this->fieldColumn,$this->param['fieldcolumn'],$this->tag));
		}


		/**
		 *   List functions: get total calculation query
		 *   @access public
		 *   @param BASE_DATAOBEJCT_PARAM $paramObject Parameters object reference
		 *   @return void
		 */

		public function getListTotalQuery( &$paramObject )
		{
			$paramObject->addColumns('SUM('.$this->fieldColumn.') as '.$this->fieldColumn);
		}


		/**
		 *  List parameters: link
		 *  @access public
		 *  @param string $listLink Link
		 *  @param string $listLinkTarget
		 *  @param string $listLinkEnabledIf
		 *  @return void
		 */

		public function setListLink( $listLink, $listLinkTarget=null, $listLinkEnabledIf=null )
		{
			$this->param['list_link']=$listLink;
			$this->param['list_link_target']=$listLinkTarget;
			$this->param['list_link_enabledif']=$listLinkEnabledIf;
		}


		/**
		 *  List parameters: empty value
		 *  @access public
		 *  @param string $listEmptyValue Empty value
		 *  @return void
		 */

		public function setListEmptyValue( $listEmptyValue )
		{
			$this->param['list_emptyvalue']=$listEmptyValue;
		}


		/**
		 *   Sets list format
		 *   @param string $listFormat  List format (for date() function)
		 */

		public function setListFormat( $listFormat )
		{
			$this->param['list_format']=$listFormat;
		}


		/**
		 *  List parameters: JS action
		 *  @access public
		 *  @param string $listAction Action
		 *  @param string $event Event (default: onclick)
		 *  @return void
		 */

		public function setListAction( $listAction, $event='onclick' )
		{
			$this->param['list_action'][$event]=$listAction;
		}


		/**
		 *  List parameters: show total column
		 *  @access public
		 *  @param bool $showTotal Show total column
		 *  @return void
		 */

		public function setListShowTotal ( $showTotal )
		{
			$this->param['list_showtotal']=$showTotal;
		}


		/**
		 *   Form triggers: load
		 *   @access public
		 *   @return void
		 */

		public function triggerFormLoad()
		{
			// This should be overwritten in the subclass if necessary.
		}


		/**
		 *   Form triggers: save
		 *   @access public
		 *   @return void
		 */

		public function triggerFormSave()
		{
			// This should be overwritten in the subclass if necessary.
		}


		/**
		 *   Form triggers: pre-display
		 *   @access public
		 *   @return void
		 */

		public function triggerFormPreDisplay()
		{
			// This should be overwritten in the subclass if necessary.
		}


		/**
		 *   Get value
		 *   @access public
		 *   @return mixed value
		 */

		public function getValue()
		{
			// Fixed array
			if (is_array($this->param['value']))
			{
				return $this->param['value'];
			}

			// Fixed value
			if (mb_strlen($this->param['value']))
			{
				return $this->zeroHack($this->param['value'],'add');
			}

			// Submitting - get value from the POST array
			if (is_object($this->formObject) && $this->formObject->doSubmit)
			{
				return $_POST[$this->tag];
			}

			// Loaded data
			if (is_object($this->formObject) && $this->formObject->operation=='edit')
			{
				return $this->zeroHack($this->formObject->data->{$this->tag},'add');
			}

			// Default
			if (isset($this->param['default']))
			{
				return $this->zeroHack($this->param['default'],'add');
			}

			return NULL;
		}


		/**
		 *   Save value
		 *   @access public
		 *   @return mixed value
		 */

		public function saveValue()
		{
			return $this->zeroHack($this->getValue(),'remove');
		}


		/**
		 *   Display value
		 *   @access public
		 *   @return mixed value
		 */

		public function displayValue()
		{
			return htmlspecialchars($this->getValue());
		}


		/**
		 *  Parse variables in a link
		 *  @access private
		 *  @param string match
		 *  @param array variables as array
		 *  @return string parsed variable
		 */

		private function _parse_link_variable( $match )
		{
			return urlencode($this->_row[$match[1]]);
		}


		/**
		 *   List value
		 *   @access public
		 *   @param string $value Column value
		 *   @param array $row Row pointer
		 *   @return mixed value
		 */

		public function listValue( $value, &$row )
		{
			$retval='';
			$listLink=$this->param['list_link'];
			if (!empty($listLink) && !empty($this->param['list_link_enabledif']))
			{
				eval('$listLinkEnabled=('.$this->param['list_link_enabledif'].')?true:false;');
				if (!$listLinkEnabled) $listLink=false;
			}

			if (!empty($listLink) || (!empty($this->param['list_action']) && sizeof($this->param['list_action'])))
			{
				$retval.='<a';
				if (!empty($listLink))
				{
					$this->_row=&$row;
					$link=preg_replace_callback("/\\$([A-Za-z0-9\_]+)/",array($this,'_parse_link_variable'),$listLink);
					$retval.=' href="'.$link.'"'.(!empty($this->param['list_link_target'])?' target="'.$this->param['list_link_target'].'"':'');
				}
				if (!empty($this->param['list_action']) && sizeof($this->param['list_action']))
				{
					foreach ($this->param['list_action'] as $event => $action)
					{
						$this->_row=&$row;
						$action=preg_replace_callback("/\\$([A-Za-z0-9\_]+)/",array($this,'_parse_link_variable'),$action);
						$retval.=' '.$event.'="'.$action.'"';
					}
				}
				$retval.='>';
			}
			if (!empty($this->param['list_format']))
			{
				$value=sprintf($this->param['list_format'],$value);
				$retval.=htmlspecialchars($value);
			}
			else
			{
				if (mb_strlen($value))
				{
					$retval.=htmlspecialchars($value);
				}
				else
				{
					$retval.=htmlspecialchars($this->param['list_emptyvalue']);
				}
			}
			if (!empty($listLink) || (!empty($this->param['list_action']) && sizeof($this->param['list_action'])))
			{
				$retval.='</a>';
			}
			return $retval;
		}


		/**
		 *   List total value
		 *   @access public
		 *   @param string $value Column value
		 *   @param array $row Row pointer
		 *   @return mixed value
		 */

		public function listTotalValue( $value, &$row )
		{
			return htmlspecialchars($value);
		}


		/**
		 *   Zero hack
		 *   @access public
		 *   @param string value
		 *   @return string value
		 */

		public function zeroHack( $value, $type )
		{
			if (is_array($value) || is_object($value)) return $value;
			if (!empty($this->param['zerohack']))
			{
				if ($type=='add')
				{
					return 'val_'.$value;
				}
				else
				{
					return preg_replace("/^val_/","",$value);
				}
			}
			return $value;
		}


		/**
		 *   Is this field required?
		 *   @access public
		 *   @return mixed value
		 */

		public function required()
		{
			// Not required
			if (empty($this->param['required'])) return FALSE;

			// Always required
			if ($this->param['required']=='always')
			{
				return TRUE;
			}

			// Required on insert
			if ($this->param['required']=='add' && $this->formObject->operation=='add')
			{
				return TRUE;
			}

			// Required on edit
			if ($this->param['required']=='edit' && $this->formObject->operation=='edit')
			{
				return TRUE;
			}

			// Required_if
			if ($this->param['required']=='if')
			{
				$eval=$this->param['required_cond'];
				if (is_object($this->formObject))
				{
					$fieldSet=$this->formObject->field;
					$keys=array_map('strlen', array_keys($fieldSet));
					array_multisort($keys, SORT_DESC, $fieldSet);
					foreach ($fieldSet as $fieldTag => &$fieldObject)
					{
						$eval=str_replace('$'.$fieldTag,'$this->formObject->field["'.$fieldTag.'"]->getValue()',$eval);
					}
				}
				eval('$reqd=('.$eval.')?TRUE:FALSE;');
				return $reqd;
			}

			// If we got here, it isn't required
			return FALSE;
		}


		/**
		 *   No save?
		 *   @access public
		 *   @return boolean
		 */

		public function noSave()
		{
			return (!empty($this->param['nosave'])?TRUE:FALSE);
		}


		/**
		 *   No log?
		 *   @access public
		 *   @return boolean
		 */

		public function noLog()
		{
			return (!empty($this->param['nolog'])?TRUE:FALSE);
		}


		/**
		 *   Data validate
		 *   @access public
		 *   @return boolean
		 */

		public function validate( $value )
		{
			// Required, but not set
			if ($this->required() && !mb_strlen($value))
			{
				return '[BASE.FORM.Error.RequiredFieldEmpty]';
			}

			// No point in checking further if empty
			if (empty($value)) return;

			// Minimum length not met
			if (intval($this->param['minlength']) && mb_strlen($value)<intval($this->param['minlength']))
			{
				return '[BASE.FORM.Error.MinLength,'.intval($this->param['minlength']).']';
			}

			// Over maximum length
			if (intval($this->param['maxlength']) && mb_strlen($value)>intval($this->param['maxlength']))
			{
				return '[BASE.FORM.Error.MaxLength,'.intval($this->param['maxlength']).']';
			}

			// Filter
			if (mb_strlen($this->param['filter']) && !preg_match($this->param['filter'],$value))
			{
				return '[BASE.FORM.Error.Filter]'.(!empty($this->param['filter_description'])?' ('.$this->param['filter_description'].')':'');
			}

			// Unique
			if (mb_strlen($this->param['unique']))
			{
				$param=new BASE_DATAOBJECT_PARAM();
				if (!empty($this->param['unique_cond']))
				{
					$cond=$this->param['unique_cond'];
					if (is_object($this->formObject))
					{
						foreach ($this->formObject->field as $fieldTag => &$fieldObject)
						{
							$cond=str_replace('$'.$fieldTag,$fieldObject->saveValue(),$cond);
						}
					}
					$param->addWhere($cond);
				}
				if (is_object($this->formObject) && !$this->formObject->data->isUnique($this->tag,$value,$param)) return oneof($this->param['unique_message'],'[BASE.FORM.Error.Unique]');
			}

			// Otherwise validate
			return '';
		}


		/**
		 *   Get options
		 *   @access public
		 *   @return array options
		 */

		public function getOptions()
		{
			global $LAB;
			$option=array();
			switch($this->fieldSource)
			{
				case 'query':
					$dataset=$LAB->DB->querySelectSQL($this->param['source_query']);
					foreach ($dataset as $row)
					{
						$option[$row['value']]=$row['description'];
					}
					break;
				case 'dao':
				case 'dataobject':
					if ($this->fieldSourceDAO instanceof BASE_DATAOBJECT)
					{
						$DAO=$this->fieldSourceDAO;
					}
					else
					{
						$cName=$this->fieldSourceDAO;
						$DAO=new $cName();
					}
					if ($this->fieldSourceDAOloadParam instanceof BASE_DATAOBJECT_PARAM)
					{
						$loadListParam=$this->fieldSourceDAOloadParam;
					}
					else
					{
						$loadListParam=new BASE_DATAOBJECT_PARAM();
					}
					$loadListParam->addColumns(array($this->fieldSourceDAOkeyField.' as value'));
					$loadListParam->addColumns(array($this->fieldSourceDAOvalueField.' as description'));
					if ($DAO->_param['setord'])
					{
						$loadListParam->addOrderBy('ord');
					}
					else
					{
						$loadListParam->addOrderBy('value');
					}
					$dataset=$DAO->loadList($loadListParam);
					foreach ($dataset as $drow) $option[$drow['value']]=$drow['description'];
					break;
				case 'func':
					$option=call_user_func($this->param['source_func'],$this);
					break;
				default:
					$option=&$this->param['source_list'];
					break;
			}
			if (!empty($this->param['zerohack']))
			{
				$origoption=$option;
				$option=array();
				foreach ($origoption as $k => $v)
				{
					$option['val_'.$k]=$v;
				}
			}
			return $option;
		}


		/**
		 *   Get label width class
		 *   @access public
		 *   @return string label width class
		 */

		public function getLabelWidthClass()
		{
			$fieldLabelWidth=(is_object($this->formObject)?$this->formObject->fieldLabelWidth:'3');
			return 'col-md-'.$fieldLabelWidth;
		}


		/**
		 *   Get content width class
		 *   @access public
		 *   @return string label width class
		 */

		public function getContentWidthClass()
		{
			$fieldContentWidth=(is_object($this->formObject)?$this->formObject->fieldContentWidth:'9');
			return 'col-md-'.$fieldContentWidth;
		}


		/**
		 *   Log data
		 *   @access public
		 *   @param BASE_DATAOBJECT $dataObject Data object
		 *   @param string $operation Operation
		 *   @param array $logValuesArray Log values array
		 *   @return string
		 */

		public function getLogData( $dataObject, $operation, &$logValuesArray )
		{
			unset($logValuesArray[$this->tag]);

			if ($operation!='edit' && !mb_strlen($dataObject->{$this->tag})) return;
			if ($operation=='edit')
			{
				if ($dataObject->{$this->tag}==$dataObject->_in[$this->tag]) return;
				if ($dataObject->{$this->tag}=='' && $dataObject->_in[$this->tag]=='0') return;
				if ($dataObject->{$this->tag}=='0' && $dataObject->_in[$this->tag]=='') return;
			}

			$XML='<fld id="'.htmlspecialchars($this->tag).'" name="'.htmlspecialchars($this->param['title']).'">';
			if ($operation!='add' && !$this->param['forcevalue']) $XML.='<old>'.htmlspecialchars($dataObject->_in[$this->tag]).'</old>';
			$XML.='<new>'.htmlspecialchars($dataObject->{$this->tag}).'</new>';
			$XML.='</fld>';
			return $XML;
		}


		/**
		 *   Get log value
		 *   @access public
		 *   @param mixed $value Value
		 *   @return string
		 */

		public function getLogValue( $value )
		{
			return $value;
		}


		/**
		 *   Display beginning block
		 *   @access public
		 *   @param $value Field value
		 *   @param $row Row number (for multi-row fields)
		 *   @return string contents
		 */

		public function displayBeginningBlock( $value, $row=NULL )
		{
			$c='<div id="field_'.$this->tag.'" class="form-group'.(!empty($this->param['rowclass'])?' '.$this->param['rowclass']:'').'"'.(!empty($this->param['rowstyle'])?' style="'.$this->param['rowstyle'].'"':'').'>';
			return $c;
		}


		/**
		 *   Display the label
		 *   @access public
		 *   @return string contents
		 */

		public function displayLabel( $value, $row=NULL )
		{
			$c='<label for="'.$this->tag.'" class="'.$this->getLabelWidthClass().' control-label'.(!empty($this->param['titleclass'])?' '.$this->param['titleclass']:'').'">';
			if ($this->param['required']=='always')
			{
				$c.='<span class="text-danger icon-asterisk"></span> ';
			}
			elseif ($this->param['required']=='if')
			{
				$c.='<span class="text-muted icon-asterisk"></span> ';
			}
			if (mb_strlen($this->param['title'])) $c.=$this->param['title'].':';
			$c.='</label>';
			return $c;
		}


		/**
		 *   Display field comment
		 *   @access public
		 *   @return string
		 */

		public function displayComment()
		{
			if (!empty($this->param['comment']))
			{
				return '<p class="form-control-static" style="display: inline-block; margin-left: 10px">'.$this->param['comment'].'</p>';
			}
			return '';
		}


		/**
		 *   Display the field
		 *   @access public
		 *   @param $value Field value
		 *   @param $row Row number (for multi-row fields)
		 *   @return string contents
		 */

		public function displayField( $value, $row=NULL )
		{
			$c='<div class="'.$this->getContentWidthClass().'">Function <b>displayField()</b> not implemented for this class.</div>';
			return $c;
		}


		/**
		 *   Display beginning block
		 *   @access public
		 *   @param $value Field value
		 *   @param $row Row number (for multi-row fields)
		 *   @return string contents
		 */

		public function displayEndingBlock( $value, $row=NULL )
		{
			$c='</div>';
			return $c;
		}


		/**
		 *   Display function
		 *   @access public
		 *   @return string contents
		 */

		public function display()
		{
			$this->triggerFormPreDisplay();
			if ($this->multirow>1)
			{
				$c='';
				for ($i=1;$i<=$this->multirow;++$i)
				{
					$c.=$this->displayBeginningBlock($this->getValue(),$i);
					$c.=$this->displayLabel($this->getValue(),$i);
					$c.=$this->displayField($this->getValue(),$i);
					$c.=$this->displayEndingBlock($this->getValue(),$i);
				}
			}
			else
			{
				$c =$this->displayBeginningBlock($this->getValue());
				$c.=$this->displayLabel($this->getValue());
				$c.=$this->displayField($this->getValue());
				$c.=$this->displayEndingBlock($this->getValue());
			}
			return $c;
		}


	}


?>