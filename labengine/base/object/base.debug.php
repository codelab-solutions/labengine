<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   The debugger class
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_DEBUG
	{


		/**
		 *   Debugger enabled
		 *   @var boolean
		 *   @access public
		 */

		public $debugOn = FALSE;


		/**
		 *   Profiler enabled
		 *   @var boolean
		 *   @access public
		 */

		public $profilerOn = FALSE;


		/**
		 *   Output to file
		 *   @var boolean
		 *   @access public
		 */

		public $debugFile = FALSE;


		/**
		 *   Debug messages
		 *   @var array
		 *   @access public
		 */

		public $debugMessages = array();


		/**
		 *   Profiler data
		 *   @var array
		 *   @access public
		 */

		public $debugProfilerData = NULL;


		/**
		 *   Are we running in a dev environment?
		 *   @var bool
		 *   @access public
		 */

		public $devEnvironment = false;


		/**
		 *   Custom error display function: HTML
		 *   @var string
		 *   @access public
		 */

		public $outputErrorHTML = NULL;


		/**
		 *   Custom error display function: plain text
		 *   @var string
		 *   @access public
		 */

		public $outputErrorPlain = NULL;


		/**
		 *   Init debugger
		 *   @access public
		 *   @return void
		 */

		public function initDebug()
		{
			global $LAB;

			// Init parameters
			$this->debugOn=($LAB->CONFIG->get('debug.debug')?TRUE:FALSE);
			$this->profilerOn=($LAB->CONFIG->get('debug.profiler')?$LAB->CONFIG->get('debug.profiler'):FALSE);
			$this->debugFile=($LAB->CONFIG->get('debug.file')?$LAB->CONFIG->get('debug.file'):FALSE);
			$this->devEnvironment=($LAB->CONFIG->get('debug.dev')?TRUE:FALSE);

			// Init profiler array
			$this->debugProfilerData=array(
				'totalQueryCount' => 0,
				'totalQueryCountSelect' => 0,
				'totalQueryCountUpdate' => 0,
				'totalQueryTime'  => 0
			);
		}


		/**
		 *   Add a debug message
		 *   @access public
		 *   @param string $debugTitle Message title
		 *   @param string $debugMessage Message content
		 *   @param integer $debugMessageType Message type
		 *   @return void
		 */

		public function addMessage( $debugTitle, $debugMessage, $debugMessageType='1' )
		{
			global $LAB;

			// Outputting to file?
			if ($this->debugFile)
			{
				$FH=fopen($this->debugFile,'a+');
				if (!sizeof($this->debugMessages))
				{
					fwrite($FH,"\n\n\n");
					fwrite($FH,"---------------------------------------------------------------------------\n");
					fwrite($FH,'Time: '.date("c")."\n");
					fwrite($FH,'requestURI: '.$LAB->REQUEST->requestURI."\n");
					fwrite($FH,'requestScript: '.$LAB->REQUEST->requestScript."\n");
					fwrite($FH,'requestHandler: '.$LAB->REQUEST->requestHandler."\n");
					fwrite($FH,'requestLang: '.$LAB->REQUEST->requestLang."\n");
					fwrite($FH,"\n");
				}
				fwrite($FH,$debugTitle.":\n");
				fwrite($FH,str_replace('<br/>',"\n",str_replace('<br>',"\n",$debugMessage))."\n\n");
				fclose($FH);
			}

			// No debugging
			if (!$this->debugFile && !$this->debugOn) return;

			// Write to array
			$this->debugMessages[]=array(
				'title'   => $debugTitle,
				'message' => $debugMessage,
				'type'    => $debugMessageType
			);
		}


		/**
		 *   Log profiler info
		 *   @access public
		 *   @return void
		 */

		public function logProfilerInfo()
		{
			global $LAB;

			// Only when profiler is enabled
			if (!$this->profilerOn) return;

			// Log main page profiler info
			$pcols=array();
			$pcols['pageprofiler_tstamp']=date('Y-m-d H:i:s');
			$pcols['pageprofiler_request_id']=$LAB->requestID;
			$pcols['pageprofiler_user_oid']=$LAB->USER->getUserOID();
			$pcols['pageprofiler_request_method']=$_SERVER['REQUEST_METHOD'];
			$pcols['pageprofiler_request_uri']=$LAB->REQUEST->requestURI;
			$pcols['pageprofiler_requesttime']=$LAB->totalRequestTime;
			$pcols['pageprofiler_dbquerytime']=$this->debugProfilerData['totalQueryTime'];
			$pcols['pageprofiler_dbquerycnt']=intval($this->debugProfilerData['totalQueryCount']);
			$pcols['pageprofiler_dbquerycnt_select']=intval($this->debugProfilerData['totalQueryCountSelect']);
			$pcols['pageprofiler_dbquerycnt_update']=intval($this->debugProfilerData['totalQueryCountUpdate']);
			$pcols['pageprofiler_peakmemoryusage']=memory_get_peak_usage(true);
			$LAB->DB->queryInsert('base_pageprofiler',$pcols);

			// Log queries
			if (intval($this->profilerOn)===2 && is_array($this->debugProfilerData['queryInfo']) && sizeof($this->debugProfilerData['queryInfo']))
			{
				foreach ($this->debugProfilerData['queryInfo'] as $q => $query)
				{
					$qcols=array();
					$qcols['pageprofiler_query_tstamp']=$pcols['pageprofiler_tstamp'];
					$qcols['pageprofiler_query_request_id']=$LAB->requestID;
					$qcols['pageprofiler_query_no']=intval($q+1);
					$qcols['pageprofiler_query_sql']=trim($query['sql']);
					$qcols['pageprofiler_query_time']=$query['time'];
					$qcols['pageprofiler_query_affectedrows']=$query['time'];
					$qcols['pageprofiler_query_explain']=((is_array($query['explain']) && sizeof($query['explain']))?var_dump_str($query['explain']):'');
					$LAB->DB->queryInsert('base_pageprofiler_query',$qcols);
				}
			}
		}


		/**
		 *   Display debug output
		 *   @access public
		 *   @return void
		 */

		public function display()
		{
			global $LAB;

			// Only when debug is enabled
			if (!$this->debugOn) return;

			// Wrapper begins
			$c='<div id="lab-debuginfo" style="background: #333333; padding: 20px; color: #ffffff; font-family: Arial, Helvetica, sans-serif; font-size: 12px">';
			$c.='<style>';
			$c.=' #lab-debuginfo table.lab-debuginfo-table { width: 100%; border-collapse: collapse; } ';
			$c.=' #lab-debuginfo table.lab-debuginfo-table th { border: 1px #919191 solid; padding: 5px; 10px; background: #444444; font-weight; bold } ';
			$c.=' #lab-debuginfo table.lab-debuginfo-table td { border: 1px #919191 solid; padding: 5px; 10px; } ';
			$c.=' #lab-debuginfo table.lab-debuginfo-table td.info-name { width: 10%; white-space: nowrap; padding: 4px 20px; text-align: center; } ';
			$c.=' #lab-debuginfo table.lab-debuginfo-table td.info-content { width: 90%; text-align: left; } ';
			$c.='</style>';

				$c.='<div style="background: #919191; padding: 10px; text-align: center; font-weight: bold">QUERY STATS</div>';
				$c.='<table class="lab-debuginfo-table">';
					$c.='<tr><td class="info-name">Total query count</td><td class="info-content">'.intval($this->debugProfilerData['totalQueryCount']).'</td></tr>';
					$c.='<tr><td class="info-name">SELECT queries</td><td class="info-content">'.intval($this->debugProfilerData['totalQueryCountSelect']).'</td></tr>';
					$c.='<tr><td class="info-name">INSERT/UPDATE/DELETE queries</td><td class="info-content">'.intval($this->debugProfilerData['totalQueryCountUpdate']).'</td></tr>';
					$c.='<tr><td class="info-name">Total query time</td><td class="info-content">'.$LAB->REGIONAL->formatDecimalValue($this->debugProfilerData['totalQueryTime'],8,true).' sec</td></tr>';
				$c.='</table>';
				if (is_array($this->debugProfilerData['queryInfo']) && sizeof($this->debugProfilerData['queryInfo']))
				{
					foreach ($this->debugProfilerData['queryInfo'] as $q => $query)
					{
						$c.='<table class="lab-debuginfo-table">';
						$c.='<tr><td colspan="2" style="background: #555555; font-weight; bold">Query #'.($q+1).'</td></tr>';
						$c.='<tr><td class="info-name">Query</td><td class="info-content">'.htmlspecialchars($query['sql']).'</td></tr>';
						$c.='<tr><td class="info-name">Fetched/affected</td><td class="info-content">'.intval($query['rows']).' rows</td></tr>';
						$c.='<tr><td class="info-name">Time</td><td class="info-content">'.$LAB->REGIONAL->formatDecimalValue($query['time'],8,true).' sec</td></tr>';
						$c.='</table>';
						if (is_array($query['explain']) && sizeof($query['explain']))
						{
							$c.='<table class="lab-debuginfo-table">';
							$c.='<tr><th><b>table</b></th><th><b>type</b></th><th><b>possible_keys</b></th><th><b>key</b></th><th><b>key_len</b></th><th><b>ref</b></th><th><b>rows</b></th><th><b>extra</b></th></tr>';
							foreach ($query['explain'] as $row)
							{
								$c.='<tr>';
								$c.='<td>'.$row['table'].'</td>';
								$c.='<td>'.$row['type'].'</td>';
								$c.='<td>'.$row['possible_keys'].'</td>';
								$c.='<td>'.$row['key'].'</td>';
								$c.='<td>'.$row['key_len'].'</td>';
								$c.='<td>'.$row['ref'].'</td>';
								$c.='<td>'.$row['rows'].'</td>';
								$c.='<td>'.$row['Extra'].'</td>';
								$c.='</tr>';
							}
							$c.='</table>';
						}
					}
				}



			// Wrapper ends
			$c.='</div>';
			return $c;
		}

	}


	?>