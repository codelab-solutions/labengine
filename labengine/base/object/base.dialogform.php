<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   The popup dialog form class
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_DIALOGFORM extends BASE_FORM
	{


		/**
		 *   Are we uploading a file (for IE hack)
		 *   @var boolean
		 *   @access public
		 */

		public $fileUpload = FALSE;


		/**
		 *   Init form
		 *   @access public
		 *   @return void
		 */

		public function initForm()
		{
			// Init
			$this->operation=mb_strlen($this->param['operation'])?$this->param['operation']:(!empty($_POST[$this->data->_param['idfield']])?'edit':'add');
			$this->XHR=TRUE;
		}


		/**
		 *   Set display trigger
		 *   @access public
		 *   @return string contents
		 */

		public function setDisplayTrigger( $displayTrigger )
		{
			$this->param['displaytrigger'].=$displayTrigger;
		}


		/**
		 *   Set redirect URL
		 *   @access public
		 *   @return string contents
		 */

		public function setRedirectURL( $redirectURL )
		{
			$this->param['url_redirect']=$redirectURL;
		}


		/**
		 *   Set page reload on save
		 *   @access public
		 *   @return string contents
		 */

		public function setReload( $reload )
		{
			$this->param['reload']=$reload;
		}


		/**
		 *   Set submit handler
		 *   @access public
		 *   @return string contents
		 */

		public function setSubmitHandler( $submitHandler )
		{
			$this->param['submithandler']=$submitHandler;
		}


		/**
		 *  	Set JS action to run after successful submit
		 *  	@access public
		 *  	@return string contents
		 */
		public function setSubmitSuccessAction ( $action )
		{
			$this->param['submitsuccessaction']=$action;
		}


		/**
		 *   Set response message
		 *   @access public
		 *   @return string contents
		 */

		public function setResponseMessage( $message )
		{
			$this->param['message']=$message;
		}


		/**
		 *  Display form begin
		 *  @access private
		 *  @return string contents
		 */

		public function displayFormBegin()
		{
			// The form begins
			$id=oneof($this->param['id'],$this->data->_param['table'].'-form','labengine-form');
			$form_begin='<form class="form-horizontal" role="form" id="form_'.$_POST['tag'].'" method="post" action="'.$this->buildURL($this->param['url'], FALSE).'" onsubmit="return false">';
			$form_begin.='<input type="hidden" id="ajaxsubmit" name="ajaxsubmit" value="1">';

			// ID field if it's not in the fieldset
			if (!empty($this->data->_param['idfield']) && ($this->operation=='edit' && !is_object($this->field[$this->data->_param['idfield']])))
			{
				$form_begin.='<input type="hidden" id="'.$this->data->_param['idfield'].'" name="'.$this->data->_param['idfield'].'" value="'.$_POST[$this->data->_param['idfield']].'">';
			}

			// Passthru fields if they're not separately defined
			foreach ($this->passthru as $ptVar => $ptObject)
			{
				if (isset($this->field[$ptVar])) continue;
				if ($ptVar==$this->data->_param['idfield']) continue;
				$form_begin.='<input type="hidden" id="'.$ptVar.'" name="'.$ptVar.'" value="'.$_POST[$ptVar].'">';
			}

			// Set template items
			$this->template->set('url_submit',$this->buildURL($this->param['url'], FALSE));
			$this->template->set('form_begin',$form_begin);
			$this->template->templateVar['form'].=$form_begin;
		}


		/**
		 *  Display form itself
		 *  @access private
		 *  @return string contents
		 */

		public function displayForm()
		{
			$response=array();
			$this->template=new BASE_TEMPLATE('form.dialogform');
			$this->template->set('form_id','form_'.trim(strip_tags($_POST['tag'])));
			$this->template->set('operation',$this->operation);

			$this->template->set('form','');
			$c=$this->displayTitle()
				.$this->displayFormBegin()
				.$this->displayFormContents()
				.$this->displayFormEnd()
				.$this->displayNotes();
			$this->template->set('contents',$c);
			$response['status']='1';
			if (!empty($this->param['title'])) $response['title']=$this->param['title'];
			if ($this->operation=='add' && !empty($this->param['title_add'])) $response['title']=$this->param['title_add'];
			if ($this->operation=='edit' && !empty($this->param['title_edit'])) $response['title']=$this->param['title_edit'];
			if (!empty($this->param['submitbuttontitle'])) $response['submit']=$this->param['submitbuttontitle'];
			if (!empty($this->param['progressmessage'])) $response['progressmessage']=$this->param['progressmessage'];
			if (!empty($this->param['displaytrigger'])) $response['displaytrigger']=$this->param['displaytrigger'];
			if (!empty($this->param['submithandler'])) $response['submithandler']=$this->param['submithandler'];
			$response['content']=$this->template->display();
			return $response;
		}



		/**
		 *  Display function
		 *  @access public
		 *  @return string contents
		 */

		public function display()
		{
			global $LAB;

			// Set defaults
			$this->setDefaults();

			// Cancel
			if (isset($_POST['submit_cancel']))
			{
				return;
			}

			// Init
			$LAB->RESPONSE->setType('json');
			$response=array();
			$response['status']='2';
			try
			{
				// Init form
				$this->initForm();

				// Load data
				if ($this->operation=='edit') $this->dataLoad();

				// Init fields
				$this->initFields();

				// Sanitize input
				$this->sanitizeInput();

				// Are we submitting?
				$this->doSubmit=$this->checkSubmit();

				// Handle submit
				if ($this->doSubmit)
				{
					// Init
					$this->errors=array();

					// Prevalidate
					$err=$this->dataPreValidate();
					if (is_array($err))
					{
						$this->errors=array_merge($this->errors, $err);
					}
					elseif (!empty($err))
					{
						$this->errors['PREGLOBAL']=$err;
					}

					if (!sizeof($this->errors))
					{
						// Validate fields
						$err=$this->validateFields();
						if (is_array($err) && sizeof($err))
						{
							$this->errors=array_merge($this->errors, $err);
						}

						// Do global error checking
						$err=$this->dataValidate();
						if (is_array($err))
						{
							$this->errors=array_merge($this->errors, $err);
						}
						else
						{
							if (strlen($err)) $this->errors['GLOBAL']=$err;
						}
					}

					// Save data
					if (!sizeof($this->errors))
					{
						$err=$this->dataSave();
						if (!empty($err)) $this->errors['GLOBAL']=$err;
					}

					// File upload hack
					if ($this->fileUpload)
					{
						$LAB->RESPONSE->responseParam['enable_iejsonhack']=TRUE;
					}

					// Return errors
					if (sizeof($this->errors))
					{
						$response['status']='2';
						$response['error']=$this->errors;
					}

					// Success
					else
					{
						$response['status']='1';
						if (!empty($this->param['url_redirect'])) $response['redirect']=$this->buildURL($this->param['url_redirect']);
						if (!empty($this->param['reload'])) $response['reload']='1';
						if (!empty($this->param['message'])) $response['message']=$this->param['message'];
						if (!empty($this->param['returndata']))
						{
							$response['data']=$this->param['returndata'];
						}
						if (!empty($this->param['submitsuccessaction'])) $response['submitsuccessaction']=$this->param['submitsuccessaction'];
					}
				}

				// Render form
				else
				{
					$displayResponse=$this->displayForm();
					if ($displayResponse['status']=='1')
					{
						foreach ($displayResponse as $k => $v)
						{
							$response[$k]=$v;
						}
					}
					else
					{
						$response['status']='2';
						$response['error']=oneof($displayResponse['error'],'[BASE.COMMON.ErrorDisplayingForm]');
					}
				}

				// Display
				return $response;
			}
			catch (Exception $e)
			{
				$response=array();
				$response['status']='2';
				$response['error']=$e->getMessage();
				return $response;
			}
		}


	}


?>