<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   The DB-based session handler class
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_SESSION_DB extends BASE_SESSION
	{


		/**
		 *   Session ID
		 *   @var string
		 */

		public $sessionID = null;


		/**
		 *   Load the session
		 *   @access public
		 *   @return void
		 */

		public function loadSession()
		{
			global $LAB;

			// Is there a session?
			$sessionCookieName=oneof($LAB->CONFIG->get('session.cookie_name'),'LABSID');

			// Get session ID
			$sessionID=$this->sessionID=oneof($_REQUEST['LAB_FORCE_SID'],$_COOKIE[$sessionCookieName],sha1(uniqid()));

			// Set session cookie
			if (intval($LAB->CONFIG->get('session.cookie_lifetime')))
			{
		    setcookie($sessionCookieName,$sessionID,time()+intval($LAB->CONFIG->get('session.cookie_lifetime')),"/");
			}
			else
			{
				setcookie($sessionCookieName,$sessionID,null,'/');
			}

			// Load session
			$sDataset=$LAB->DB->querySelectSQL("SELECT * FROM base_session WHERE session_id='".addslashes($sessionID)."'");
			foreach ($sDataset as $srow)
			{
				$this->sessionData=unserialize($srow['session_data']);
			}
			$this->sessionLoaded=true;
		}



		/**
		 *   Save the session
		 *   @access public
		 *   @return void
		 */

		public function saveSession()
		{
			global $LAB;

			// Update
			if ($this->sessionUpdated || $LAB->CONFIG->get('session.keep_tstamp'))
			{
				$cols=array();
				$cols['session_id']=$this->sessionID;
				$cols['session_tstamp']=date('Y-m-d H:i:s');
				$cols['session_locked']='0000-00-00 00:00:00';
				$cols['session_data']=serialize($this->sessionData);
				$LAB->DB->queryReplace('base_session',$cols);
			}
		}



		/**
		 *   Destroy the session
		 *   @access public
		 *   @param boolean Destroy the data as well (FALSE by default)
		 *   @return void
		 */

		public function destroySession( $destroyData=FALSE )
		{
			if ($destroyData)
			{
				$this->sessionData=array();
			}
			session_write_close();
			$this->sessionLoaded=FALSE;
		}



		/**
		 *   Set a session variable
		 *   @access public
		 *   @param string variable name
		 *   @param string mixed variable value
		 *   @return void
		 */

		public function set( $varName, $varValue )
		{
			traverse_set($varName,$this->sessionData,$varValue);
			$this->sessionUpdated=TRUE;
		}



		/**
		 *   Get a session variable
		 *   @access public
		 *   @param string variable name
		 *   @return void
		 */

		public function get( $varName )
		{
			return traverse_get($varName,$this->sessionData);
		}


	}


	?>