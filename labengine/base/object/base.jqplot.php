<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   jqPlot wrapper class
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_JQPLOT
	{


		/**
		 *   Init jqPlot
		 *   @public
		 *   @static
		 *   @return void
		 */

		public static function init()
		{
			global $LAB;

			$LAB->RESPONSE->JS->addJS('base/static/jqplot/jqplot.js','jqplot',6);
			$LAB->RESPONSE->CSS->addCSS('base/static/jqplot/jqplot.css','jqplot',6);

			$LAB->RESPONSE->JS->addJS('base/static/jqplot/plugins/jqplot.dateAxisRenderer.min.js','jqplot/plugins/dateAxisRenderer.min',6.5);
			$LAB->RESPONSE->JS->addJS('base/static/jqplot/plugins/jqplot.canvasAxisLabelRenderer.min.js','jqplot/plugins/canvasAxisLabelRenderer.min',6.5);
			$LAB->RESPONSE->JS->addJS('base/static/jqplot/plugins/jqplot.canvasTextRenderer.min.js','jqplot/plugins/canvasTextRenderer.min',6.5);
			$LAB->RESPONSE->JS->addJS('base/static/jqplot/plugins/jqplot.pieRenderer.min.js','jqplot/plugins/pieRenderer.min',6.5);
			$LAB->RESPONSE->JS->addJS('base/static/jqplot/plugins/jqplot.categoryAxisRenderer.min.js','jqplot/plugins/categoryAxisRenderer.min',6.5);
			$LAB->RESPONSE->JS->addJS('base/static/jqplot/plugins/jqplot.barRenderer.min.js','jqplot/plugins/barRenderer.min',6.5);
			$LAB->RESPONSE->JS->addJS('base/static/jqplot/plugins/jqplot.pointLabels.min.js','jqplot/plugins/pointLabels.min',6.5);
			$LAB->RESPONSE->JS->addJS('base/static/jqplot/plugins/jqplot.enhancedLegendRenderer.min.js','jqplot/plugins/jqplot.enhancedLegendRenderer.min',6.5);
			$LAB->RESPONSE->JS->addJS('base/static/jqplot/plugins/jqplot.canvasAxisTickRenderer.min.js','jqplot/plugins/jqplot.canvasAxisTickRenderer.min',6.5);
			$LAB->RESPONSE->JS->addJS('base/static/jqplot/plugins/jqplot.highlighter.min.js','jqplot/plugins/jqplot.highlighter.min',6.5);
		}


		public static function lineChart($divname,$series,$options)
		{
			/*
			$options.=',
						seriesDefaults: {
							pointLabels: {
								show: true,
								edgeTolerance: -15
							}
						},
						highlighter: {
					        show: true,
					        sizeAdjust: 10
		      			},';
			$ypadding=0;
			$options.='series:[';
			for ($i=0; $i<sizeof($series); $i++) {
				$options.='{pointLabels: {ypadding: '.$ypadding.'}}';
				if ($i<sizeof($series)-1)
				{
					$options.=',';
					$ypadding+=10;
				}
			}
			$options.='],';
*/
			return BASE_JQPLOT::chart($divname,$series,$options);
		}

		public static function pieChart($divname,$series,$options)
		{
			$options.='
						seriesDefaults:{
				            renderer:$.jqplot.PieRenderer,
				            rendererOptions: {
					          showDataLabels: true,
					          sliceMargin: 8,
		                	  startAngle: -90
					        }
				        },
				        legend: {
				        	show: true,
				        	rendererOptions: {
			                    border: "none"
			                },
				        	location: \'e\',
				        	textColor: "#999999",
				        },';

			return BASE_JQPLOT::chart($divname,$series,$options);
		}

		public static function barChartHorizontal($divname,$series,$options)
		{
			$options.=',
						seriesDefaults:{
				            renderer:$.jqplot.BarRenderer,
				            rendererOptions: {
				                barDirection: \'horizontal\'
				            },
				            pointLabels: {
				            	show: true,
				            	location: \'e\',
				            	edgeTolerance: -15
				            }
				        },';

			return BASE_JQPLOT::chart($divname,$series,$options);
		}

		public static function barChartVertical($divname,$series,$options)
		{
			$options.=',
						seriesDefaults:{
				            renderer:$.jqplot.BarRenderer,
				            pointLabels: {
				            	show: true,
				            	location: \'e\'
				            }
				        },';

			return BASE_JQPLOT::chart($divname,$series,$options);
		}

		private static function chart($divname, $series, $options, $divStyle='width: 100%; height: 300px' )
		{
			$chart='<div id="'.$divname.'" style="'.$divStyle.'"></div>';
			$chart.='<script type="text/javascript">
				$(document).ready(function(){
					var plot = $.jqplot(\''.$divname.'\','.json_encode($series).',{'.$options.'});
				});
			</script>';
			return $chart;
		}

		private static function generateColors($num)
		{
			$hexArray = array();
			$baseHue = 130;
			$step = 200 / $num;
			for ($i = 1; $i < $num; ++$i)
			{
				$hexArray[] = self::getHEX(($baseHue + $step * $i) % 200, 70, 80);
			}
			return $hexArray;
		}

		private static function getHEX($iH, $iS, $iV) {

			if($iH < 0)   $iH = 0;   // Hue:
			if($iH > 360) $iH = 360; //   0-360
			if($iS < 0)   $iS = 0;   // Saturation:
			if($iS > 100) $iS = 100; //   0-100
			if($iV < 0)   $iV = 0;   // Lightness:
			if($iV > 100) $iV = 100; //   0-100

			$dS = $iS/100.0; // Saturation: 0.0-1.0
			$dV = $iV/100.0; // Lightness:  0.0-1.0
			$dC = $dV*$dS;   // Chroma:     0.0-1.0
			$dH = $iH/60.0;  // H-Prime:    0.0-6.0
			$dT = $dH;       // Temp variable

			while($dT >= 2.0) $dT -= 2.0; // php modulus does not work with float
			$dX = $dC*(1-abs($dT-1));     // as used in the Wikipedia link

			switch($dH) {
				case($dH >= 0.0 && $dH < 1.0):
					$dR = $dC; $dG = $dX; $dB = 0.0; break;
				case($dH >= 1.0 && $dH < 2.0):
					$dR = $dX; $dG = $dC; $dB = 0.0; break;
				case($dH >= 2.0 && $dH < 3.0):
					$dR = 0.0; $dG = $dC; $dB = $dX; break;
				case($dH >= 3.0 && $dH < 4.0):
					$dR = 0.0; $dG = $dX; $dB = $dC; break;
				case($dH >= 4.0 && $dH < 5.0):
					$dR = $dX; $dG = 0.0; $dB = $dC; break;
				case($dH >= 5.0 && $dH < 6.0):
					$dR = $dC; $dG = 0.0; $dB = $dX; break;
				default:
					$dR = 0.0; $dG = 0.0; $dB = 0.0; break;
			}

			$dM  = $dV - $dC;
			$dR += $dM; $dG += $dM; $dB += $dM;
			$dR *= 255; $dG *= 255; $dB *= 255;

			$color = dechex( ($dR << 16) + ($dG << 8) + $dB );
			return '#'.str_repeat('0', 6 - strlen($color)) . $color;
		}


	}


?>