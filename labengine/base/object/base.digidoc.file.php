<?php


	/**
	 *
	 *   LabEngine™ 7
     *   DigiDoc file class
	 *
	 *   @author   Codelab Solutions OÜ <codelab@codelab.ee>
	 *   @license  https://www.flaskphp.com/LICENSE MIT
	 *
	 */

	class BASE_DIGIDOC_FILE
	{


		/**
		 *   File ID
		 *   @var string
		 *   @access public
		 */

		public $id = null;


		/**
		 *   File name
		 *   @var string
		 *   @access public
		 */

		public $filename = null;


		/**
		 *   MIME content-type
		 *   @var string
		 *   @access public
		 */

		public $contentType = null;


		/**
		 *   File size
		 *   @var int
		 *   @access public
		 */

		public $size = null;


		/**
		 *   Is readable?
		 *   @var bool
		 *   @access public
		 */

		public $readable = null;


		/**
		 *   Content filename on disk
		 *   @var string
		 *   @access public
		 */

		public $contentFilename = null;


		/**
		 *   Content
		 *   @var string
		 *   @access public
		 */

		public $content = null;


	}


?>