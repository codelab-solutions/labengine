<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   The display object class
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_DISPLAYOBJECT
	{


		/**
		 *   Parameters
		 *   @var array
		 *   @access public
		 */
		public $param = NULL;


		/**
		 *   The data object
		 *   @var BASE_DATAOBJECT
		 *   @access public
		 */
		public $data = NULL;


		/**
		 *   Inherit parameters from the parent
		 *   @access public
		 *   @param array $inheritedParam Inherited parameters
		 *   @param BASE_DATAOBJECT Inherited data object
		 *   @return void
		 */

		public function inherit( $inheritedParam, $inheritedData=FALSE )
		{
			// Parameters
			foreach ($inheritedParam as $pKey => $pValue)
			{
				if (isset($this->param[$pKey])) continue;
				$this->param[$pKey]=$pValue;
			}

			// Data object
			if (is_object($inheritedData) && !is_object($this->data))
			{
				$this->data=$inheritedData;
			}
		}


		/**
		 *   Set unset parameters to default values
		 *   @access public
		 *   @return void
		 */

		public function setDefaults()
		{
			global $LAB;

			// Template
			if (!isset($this->param['template'])) $this->param['template']='default';

			// URL
			if (!isset($this->param['baseurl'])) $this->param['baseurl']=$LAB->REQUEST->requestScript;
		}


		/**
		 *  Build URL
		 *  @access public
		 *  @return string URL
		 */

		public function buildURL( $url=NULL, $addParam=TRUE )
		{
			// Base URL
			if ($url[0]=='/' || !strncasecmp($url,'http:',5) || !strncasecmp($url,'https:',6))
			{
				$buildURL=$url;
			}
			elseif (!empty($url))
			{
				$buildURL=$this->param['baseurl'].'/'.$url;
			}
			else
			{
				$buildURL=oneof($this->param['url'],$this->param['baseurl']);
			}

			// Split anchor
			$anchor='';
			if (strpos($buildURL,'#')!==FALSE)
			{
				list($buildURL,$anchor)=preg_split('/#/',$buildURL,2);
				$anchor='#'.$anchor;
			}

			// Param
			if ($addParam && is_array($this->passthru) && sizeof($this->passthru))
			{
				foreach ($this->passthru as $ptVar => $ptObject)
				{
					if (!empty($_POST[$ptVar]))
					{
						$buildURL.='/'.oneof($ptObject->prettyName,$ptVar).'='.urlencode($_POST[$ptVar]);
					}
				}
			}

			// Return URL
			return $buildURL.$anchor;
		}


		/**
		 *   Set title
		 *   @access public
		 *   @param string $title Title
		 *   @param boolean $append Append to current title
		 *   @return void
		 */

		public function setTitle ( $title, $append=FALSE )
		{
			if ($append && !empty($this->param['title']))
			{
				$this->param['title'].=($append!==TRUE?' '.$append.' ':'').$title;
			}
			else
			{
				$this->param['title']=$title;
			}
		}


		/**
		 *   Set base URL
		 *   @access public
		 *   @param string $baseURL Base URL
		 *   @return void
		 */

		public function setBaseURL ( $baseURL )
		{
			$this->param['baseurl']=$baseURL;
		}


		/**
		 *  Set template
		 *  @access public
		 *  @param string $template Template tag
		 *  @return void
		 */

		public function setTemplate ( $template )
		{
			$this->param['template']=$template;
		}


		/**
		 *   Set log message
		 *   @access public
		 *   @param string $logMessage Log message
		 *   @return void
		 */

		public function setLogMessage ( $logMessage )
		{
			$this->param['logmessage']=$logMessage;
		}


		/**
		 *  Set log ref OID
		 *  @access public
		 *  @param integer $logRefOID Log ref OID
		 *  @return void
		 */

		public function setLogRefOID ( $logRefOID )
		{
			$this->param['logrefoid']=$logRefOID;
		}


		/**
		 *   Run the displayobject and return contents
		 *   @throws Exception
		 *   @return string
		 */

		public function display()
		{
			throw new Exception('Function display() not implemented for the display object.');
		}


	}


?>