<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   The base DB abstraction layer class
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_DB
	{


		/**
		 *   Init datasource connection
		 *   @access public
		 *   @param array connection parameters
		 *   @return boolean TRUE if succeeded, FALSE if failed
		 */

		public function connect( $param=array() )
		{
			// This can be empty, in case the data source does not need an implicit connect.
		}


		/**
		 *   Disconnect from the datasource
		 *   @access public
		 *   @return void
		 */

		public function disconnect()
		{
			// This can be empty, in case the data source does not need an implicit disconnect.
		}


	}


?>