<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   The controller object class
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_CONTROLLEROBJECT
	{


		/**
		 *  Parameters
		 *  @var array
		 *  @access public
		 */
		public $param = array();

		/**
		 *  Actions
		 *  @var array
		 *  @access public
		 */
		public $actionData = array();

		/**
		 *  Login required?
		 *  @var string
		 *  @access public
		 */
		public $controllerLoginRequired = TRUE;

		/**
		 *  Login URL
		 *  @var string
		 *  @access public
		 */
		public $controllerLoginURL = '/login';

		/**
		 *  Authorized roles
		 *  @var string
		 *  @access public
		 */
		public $controllerRoleRequired = FALSE;


		/**
		 *  Add an action
		 *  @access public
		 *  @param string $actionTag Action tag
		 *  @param BASE_CONTROLLERACTION $actionObject Action object
		 *  @throws Exception
		 *  @return BASE_CONTROLLERACTION
		 */

		public function addAction( $actionTag, $actionObject=NULL )
		{
			// Check
			if (isset($this->actionData[$actionTag])) throw new Exception('Action '.$actionTag.' already exists.');

			// Action passed
			if ($actionObject instanceof BASE_CONTROLLERACTION)
			{
				$this->actionData[$actionTag]=$actionObject;
			}

			// Create blank action
			else
			{
				$this->actionData[$actionTag]=new BASE_CONTROLLERACTION();
			}

			// Create backreference
			$this->actionData[$actionTag]->controllerObject=&$this;

			// Return reference to action
			$retVal=&$this->actionData[$actionTag];
			return $retVal;
		}


		/**
		 *  Set whether login is required
		 *  @access public
		 *  @param boolean $loginRequired Login required
		 *  @return void
		 */

		public function setLoginRequired ( $loginRequired )
		{
			$this->controllerLoginRequired=$loginRequired;
		}


		/**
		 *  Set login URL
		 *  @access public
		 *  @param boolean $loginURL Login URL
		 *  @return void
		 */

		public function setLoginURL( $loginURL )
		{
			$this->controllerLoginURL=$loginURL;
		}


		/**
		 *  Set required privileges
		 *  @access public
		 *  @param string|array $roleRequired Required role(s)
		 *  @return void
		 */

		public function setRoleRequired ( $roleRequired )
		{
			$this->controllerRoleRequired=$roleRequired ;
		}


		/**
		 *   Set a controller parameter
		 *   @access public
		 *   @param string $param Parameter name
		 *   @param mixed $value Value
		 *   @return void
		 */

		public function setParam ( $param, $value )
		{
			$this->param[$param]=$value;
		}


		/**
		 *   Get controller parameter
		 *   @access public
		 *   @param string $param Parameter name
		 *   @return void
		 */

		public function getParam ( $param )
		{
			return $this->param[$param];
		}


		/**
		 *   Run controller
		 *   @access public
		 *   @param string|boolean $forceAction Force action
		 *   @throws \Exception
		 *   @return void
		 */

		public function runController ( $forceAction=FALSE )
		{
			global $LAB;

			// Check
			if (!sizeof($this->actionData)) throw new Exception('No actions defined.');
			if (!empty($this->controllerRoleRequired) && !$LAB->USER->checkRole($this->controllerRoleRequired)) throw new Exception('Access denied.');

			// Get action
			$actionTag=$urlActionTag=oneof($forceAction,$LAB->REQUEST->requestVar['var_1']['name']);

			// No action? Will use default
			if (empty($actionTag))
			{
				reset($this->actionData);
				$actionUse=key($this->actionData);
			}

			// See if the action exists
			else
			{
				$actionFound=FALSE;
				foreach ($this->actionData as $aTag => $aObject)
				{
					if ($actionTag==$aTag)
					{
						$actionUse=$aTag;
						$actionFound=TRUE;
						break;
					}
				}

				// Still not found? Let's revert to default
				if (!$actionFound)
				{
					reset($this->actionData);
					$urlActionTag='';
					$actionUse=key($this->actionData);
				}
			}

			// Get action object
			$actionObject=&$this->actionData[$actionUse];

			// Check if implicit login requirement on action
			if ($actionObject->actionLoginRequired!=='inherit')
			{
				if ($actionObject->actionLoginRequired===true && !$LAB->USER->isLoggedIn())
				{
					$LAB->SESSION->set('login.redirect',$_SERVER['SCRIPT_URL']);
					$LAB->RESPONSE->setRedirect(oneof($actionObject->actionLoginURL,$this->controllerLoginURL,'/login'));
					return;
				}
			}

			// Check if we have privileges for this action
			if (!empty($actionObject->actionRoleRequired) && !$LAB->USER->checkRole($actionObject->actionRoleRequired)) throw new Exception('Access denied.');

			// Include
			if (!empty($actionObject->actionInclude))
			{
				$actionInclude=str_array($actionObject->actionInclude);
				foreach ($actionInclude as $inc)
				{
					if (!include($inc)) throw new Exception('Action include not found: '.$inc);
				}
			}

			// Action class
			if (!empty($actionObject->actionClass))
			{
				$cName=$actionObject->actionClass;
				$actionClass=new $cName();
			}

			// Action handler
			elseif (!empty($actionObject->actionHandler))
			{
				$actionClass=&${$actionObject->actionHandler};
				if (!is_object($actionClass)) throw new Exception('Action handler not found: '.$actionObject->actionHandler);
			}

			// See if we have a DisplayObject instance
			else
			{
				$foundDisplayObjects=0;
				foreach (array_keys(get_defined_vars()) as $varName)
				{
					if ($$varName instanceof BASE_DISPLAYOBJECT)
					{
						$foundDisplayObjects++;
						$actionClass=new $varName();
					}
				}
				if (!$foundDisplayObjects)
				{
					if (!is_object($actionClass)) throw new Exception('No action class or handler specified and no BASE_DISPLAYOBJECT instance found.');
				}
				if ($foundDisplayObjects>1)
				{
					throw new Exception('Found more than one BASE_DISPLAYOBJECT class. Please specify the class or handler in the controller.');
				}
			}

			// Set backreference
			$actionClass->controllerObject=&$this;

			// Set baseURL
			$actionClass->setBaseURL('/'.$LAB->REQUEST->requestController.(!empty($urlActionTag)?'/'.$urlActionTag:''));

			// Pass the inherited parameters
			$actionClass->inherit($this->param, $this->data);

			// Run the action and return contents
			return $actionClass->display();
		}


	}


?>