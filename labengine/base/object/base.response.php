<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   The response handler class
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_RESPONSE
	{


		/*
		 *   HTTP status codes
		 *   @var array
		 *   @static
		 *   @access public
		 */

		public static $SET_http_status = array(
			'100' => 'Continue',
			'101' => 'Switching Protocols',
			'102' => 'Processing',
			'200' => 'OK',
			'201' => 'Created',
			'202' => 'Accepted',
			'203' => 'Non-Authoritative Information',
			'204' => 'No Content',
			'205' => 'Reset Content',
			'206' => 'Partial Content',
			'207' => 'Multi-Status',
			'208' => 'Already Reported',
			'226' => 'IM Used',
			'300' => 'Multiple Choices',
			'301' => 'Moved Permanently',
			'302' => 'Found',
			'303' => 'See Other',
			'304' => 'Not Modified',
			'305' => 'Use Proxy',
			'307' => 'Temporary Redirect',
			'308' => 'Permanent Redirect',
			'400' => 'Bad Request',
			'401' => 'Unauthorized',
			'402' => 'Payment Required',
			'403' => 'Forbidden',
			'404' => 'Not Found',
			'405' => 'Method Not Allowed',
			'406' => 'Not Acceptable',
			'407' => 'Proxy Authentication Required',
			'408' => 'Request Timeout',
			'409' => 'Conflict',
			'410' => 'Gone',
			'411' => 'Length Required',
			'412' => 'Precondition Failed',
			'413' => 'Payload Too Large',
			'414' => 'URI Too Long',
			'415' => 'Unsupported Media Type',
			'416' => 'Range Not Satisfiable',
			'417' => 'Expectation Failed',
			'421' => 'Misdirected Request',
			'422' => 'Unprocessable Entity',
			'423' => 'Locked',
			'424' => 'Failed Dependency',
			'426' => 'Upgrade Required',
			'428' => 'Precondition Required',
			'429' => 'Too Many Requests',
			'431' => 'Request Header Fields Too Large',
			'500' => 'Internal Server Error',
			'501' => 'Not Implemented',
			'502' => 'Bad Gateway',
			'503' => 'Service Unavailable',
			'504' => 'Gateway Timeout',
			'505' => 'HTTP Version Not Supported',
			'506' => 'Variant Also Negotiates',
			'507' => 'Insufficient Storage',
			'508' => 'Loop Detected',
			'510' => 'Not Extended',
			'511' => 'Network Authentication Required'
		);


		/**
		 *   Response type
		 *   @var string
		 *   @access public
		 */

		public $responseType = 'html';


		/**
		 *   Response HTTP status
		 *   @var int
		 *   @access public
		 */

		public $responseStatus = 200;


		/**
		 *   Response content-type (for raw)
		 *   @var string
		 *   @access public
		 */

		public $responseContentType = FALSE;


		/**
		 *   Response template
		 *   @var string
		 *   @access public
		 */

		public $responseTemplate = 'default';


		/**
		 *   Response redirect
		 *   @var string
		 *   @access public
		 */

		public $responseRedirect = FALSE;


		/**
		 *   Response content
		 *   @var string|array
		 *   @access public
		 */

		public $responseContent = '';


		/**
		 *   Response content file
		 *   @var string
		 *   @access public
		 */

		public $responseContentFile = '';


		/**
		 *   Response variables
		 *   @var array
		 *   @access public
		 */

		public $responseVar = array();


		/**
		 *   Response expiration
		 *   @var int
		 *   @access public
		 */

		public $responseExpires = FALSE;


		/**
		 *   Response last-modified
		 *   @var int
		 *   @access public
		 */

		public $responseLastModified = FALSE;


		/**
		 *   Filename
		 *   @var string
		 *   @access public
		 */

		public $responseFileName = FALSE;


		/**
		 *   Content disposition (inline|attachment)
		 *   @var string
		 *   @access public
		 */

		public $responseContentDisposition = 'attachment';


		/**
		 *   Page title
		 *   @var string
		 *   @access public
		 */

		public $responseTitle = FALSE;


		/**
		 *   Other response parameters
		 *   @var array
		 *   @access public
		 */

		public $responseParam = array();


		/**
		 *   Other response headers
		 *   @var array
		 *   @access public
		 */

		public $responseHeader = array();


		/**
		 *   Meta tags
		 *   @var array
		 *   @access public
		 */

		public $responseMeta = array();


		/**
		 *   Additional head tags
		 *   @var array
		 *   @access public
		 */

		public $responseHeadTags = array();


		/**
		 *   CSS
		 *   @var array
		 *   @access public
		 */

		public $CSS = FALSE;


		/**
		 *  JS
		 *  @var array
		 *  @access public
		 */

		public $JS = FALSE;


		/**
		 *   Constructor
		 *   @access public
		 *   @param string response type
		 *   @param string response content-type
		 *   @return void
		 */

		public function __construct()
		{
			$this->JS=new BASE_RESPONSE_JS();
			$this->CSS=new BASE_RESPONSE_CSS();
		}


		/**
		 *   Set type
		 *   @access public
		 *   @param string response type
		 *   @param string response content-type
		 *   @return void
		 */

		public function setType( $responseType, $responseContentType=FALSE )
		{
			$this->responseType=$responseType;
			if (!empty($responseContentType)) $this->responseContentType=$responseContentType;
		}


		/**
		 *   Set response HTTP status
		 *   @access public
		 *   @param int $responseStatus Status
		 *   @return void
		 *   @throws Exception
		 */

		public function setStatus( $responseStatus )
		{
			if (empty($responseStatus)) throw new Exception('Empty status.');
			if (!array_key_exists($responseStatus,BASE_RESPONSE::$SET_http_status)) throw new Exception('Unknown status.');
			$this->responseStatus=$responseStatus;
		}


		/**
		 *   Set redirect
		 *   @access public
		 *   @param string $responseRedirect Redirect URL
		 *   @param int $redirectStatus Redirect status
		 *   @return void
		 */

		public function setRedirect( $responseRedirect, $redirectStatus=null )
		{
			$this->responseRedirect=$responseRedirect;
			if (!empty($redirectStatus)) $this->setStatus($redirectStatus);
		}


		/**
		 *   Set template
		 *   @access public
		 *   @param string response template
		 *   @return void
		 */

		public function setTemplate( $responseTemplate )
		{
			$this->responseTemplate=$responseTemplate;
		}


		/**
		 *   Set expiration
		 *   @access public
		 *   @param int expiration as timestamp
		 *   @return void
		 */

		public function setExpires( $responseExpires )
		{
			$this->responseExpires=$responseExpires;
		}


		/**
		 *   Set last modidief
		 *   @access public
		 *   @param int last-modified as timestamp
		 *   @return void
		 */

		public function setLastModified( $responseLastModified )
		{
			$this->responseLastModified=$responseLastModified;
		}


		/**
		 *   Set response filename
		 *   @access public
		 *   @param string filename
		 *   @return void
		 */

		public function setFileName( $responseFileName )
		{
			$this->responseFileName=$responseFileName;
		}


		/**
		 *   Set content disposition
		 *   @access public
		 *   @param string content disposition
		 *   @return void
		 */

		public function setContentDisposition( $responseContentDisposition )
		{
			$this->responseContentDisposition=$responseContentDisposition;
		}


		/**
		 *  Set page title
		 *  @access public
		 *  @param string title
		 *  @param boolean append
		 *  @param string title separator
		 *  @return void
		 */

		function setTitle( $title, $append=TRUE, $titleSeparator='»' )
		{
			if ($append)
			{
				if (intval($append)==2)
				{
					$this->responseTitle=$title.(!empty($this->responseTitle)?' '.$titleSeparator.' '.$this->responseTitle:'');
				}
				else
				{
					$this->responseTitle.=(!empty($this->responseTitle)?' '.$titleSeparator.' ':'').$title;
				}
			}
			else
			{
				$this->responseTitle=$title;
			}
		}


		/**
		 *   Set other response parameter
		 *   @access public
		 *   @param string parameter name
		 *   @param string parameter value
		 *   @return void
		 */

		function setParam( $responseParamName, $responseParamValue )
		{
			$this->responseParam[$responseParamName]=$responseParamValue;
		}


		/**
		 *   Set response header
		 *   @access public
		 *   @param string $headerName Header name
		 *   @param string $headerValue Header value
		 *   @return void
		 */

		function setHeader( $headerName, $headerValue )
		{
			$this->responseHeader[$headerName]=$headerValue;
		}


		/**
		 *   Set HTTP meta header
		 *   @access public
		 *   @param string|array parameter name (or array of names)
		 *   @param string parameter value
		 *   @return void
		 */

		function setMeta( $paramName, $paramValue )
		{
			if (is_array($paramName))
			{
				foreach ($paramName as $p)
				{
					$this->responseMeta[$p]=$paramValue;
				}
			}
			else
			{
				$this->responseMeta[$paramName]=$paramValue;
			}
		}


		/**
		 *   Add a custom <head> tag
		 *   @access public
		 *   @param string $tag Tag
		 *   @return void
		 */

		function addHeadTag( $tag )
		{
			$this->responseHeadTags[]=$tag;
		}


		/**
		 *   Add a CSS file
		 *   @access public
		 *   @param string $cssFilename Filename
		 *   @param string $cssID ID
		 *   @param int $cssPriority Priority (1-9)
		 *   @param string $cssMedia Media
		 *   @return void
		 */

		public function addCSS ( $cssFilename, $cssID='', $cssPriority=5 )
		{
			$this->CSS->addCSS($cssFilename,$cssID,$cssPriority);
		}


		/**
		 *   Output css
		 *   @access public
		 *   @return void
		 */

		public function outputCSS()
		{
			// Bundle
			if (sizeof($this->responseCSSbundle))
			{
				$CSSBUNDLE=new BASE_BUNDLE_CSS();
				$bundleSet=$CSSBUNDLE->buildBundle($this->responseCSSbundle);
				foreach ($bundleSet as $bundleID => $bundleProperties)
				{
					echo '<link rel="stylesheet" href="/css/bundle/'.$bundleID.'"'.(!empty($bundleProperties['media'])?' media="'.$bundleProperties['media'].'"':'').'>';
				}
			}

			// Standard CSS
			if (sizeof($this->responseCSS))
			{
				$css=sortdataset($this->responseCSS,'priority');
				foreach ($css as $cssID => $cssProperties)
				{
					echo '<link rel="stylesheet" href="'.(strncmp($cssProperties['filename'],'/',1)?'/static/css/':'').$cssProperties['filename'].'"'.(!empty($cssProperties['media'])?' media="'.$cssProperties['media'].'"':'').'>';
				}
			}
		}


		/**
		 *   Add a JS file
		 *   @access public
		 *   @param string $jsFilename Filename
		 *   @param string $jsID ID
		 *   @param int $jsPriority Priority (1-9)
		 *   @param array $jsExtraParam Extra parameters
		 *   @return void
		 */

		public function addJS ( $jsFilename, $jsID='', $jsPriority=5, $jsExtraParam=NULL )
		{
			$this->JS->addJS($jsFilename,$jsID,$jsPriority,$jsExtraParam);
		}


		/**
		 *   Add a JS file to bundle
		 *   @access public
		 *   @param string $jsFilename Filename
		 *   @param string $jsID ID
		 *   @param int $jsPriority Priority (1-9)
		 *   @return void
		 */

		public function addJSbundle ( $jsFilename, $jsID='', $jsPriority=5 )
		{
			if (!strlen($jsID)) $jsID=strval(sizeof($this->responseJSbundle)+1);
			if ($jsPriority===TRUE) $jsPriority=1;
			$this->responseJSbundle[$jsID]=array(
				'filename' => $jsFilename,
				'priority' => $jsPriority
			);
		}


		/**
		 * Output js
		 * @access public
		 * @return void
		 */

		public function outputJS()
		{
			// Bundle
			if (sizeof($this->responseJSbundle))
			{
				$JSBUNDLE=new BASE_BUNDLE_JS();
				$bundleSet=$JSBUNDLE->buildBundle($this->responseJSbundle);
				foreach ($bundleSet as $bundleID => $bundleProperties)
				{
					echo '<script src="/js/bundle/'.$bundleID.'"></script>';
				}
			}

			// Regular JS
			if (sizeof($this->responseJS))
			{
				$js=sortdataset($this->responseJS,'priority');
				foreach ($js as $jsID => $jsProperties)
				{
					echo '<script src="'.(strncmp($jsProperties['filename'],'/',1)?'/static/js/':'').$jsProperties['filename'].'"';
					if ($jsProperties['extraparam']!=FALSE && is_array($jsProperties['extraparam']))
					{
						foreach ($jsProperties['extraparam'] as $key => $val) echo ' '.$key.'="'.$val.'"';
					}
					echo '></script>';
				}
			}
		}


		/**
		 *   Handle response
		 *   @access public
		 *   @return void
		 */

		public function handleResponse()
		{
			global $LAB;

			//  Stop the timer
			$timeEnd=microtime_float();
			$LAB->totalRequestTime=$timeEnd-$LAB->requestStartTime;

			//  Redirect
			if (strlen($this->responseRedirect))
			{
				// Send redirect headers
				if ($this->responseStatus>300 && $this->responseStatus<399)
				{
					header("HTTP/1.1 ".$this->responseStatus." ".BASE_RESPONSE::$SET_http_status[$this->responseStatus]);
				}
				if (strpos($this->responseRedirect,"://")===false)
				{
					$this->responseRedirect=$LAB->CONFIG->get('site.url').$this->responseRedirect;
				}
				header("Request-URI: ".$this->responseRedirect);
				header("Content-Location: ".$this->responseRedirect);
				header("Location: ".$this->responseRedirect);
				header('Expires: '.date("D, j M Y G:i:s T"));
				header('Pragma: no-cache');
				header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
				return;
			}

			// HTTP status
			if ($this->responseStatus!=200)
			{
				header("HTTP/1.1 ".$this->responseStatus." ".BASE_RESPONSE::$SET_http_status[$this->responseStatus]);
			}

			// Send expires header if set
			if (!empty($this->responseExpires))
			{
				header('Expires: '.date("D, j M Y G:i:s T",$this->responseExpires));
				if ($this->responseExpires>time())
				{
					$age=($this->responseExpires-time());
					header('Pragma: cache');
					header('Cache-Control: max-age='.$age);
					header('User-Cache-Control: max-age='.$age);
				}
				else
				{
					header('Pragma: no-cache');
					header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
				}
			}
			else
			{
				header('Expires: '.date("D, j M Y G:i:s T"));
				header('Pragma: no-cache');
				header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
			}

			// Send last-modified header if set
			if (!empty($this->responseLastModified))
			{
				header('Last-Modified: '.date("D, j M Y G:i:s T", $this->responseLastModified));
			}

			// Set other headers
			foreach ($this->responseHeader as $headerName => $headerValue)
			{
				header($headerName.': '.(is_array($headerValue)?join('; ',$headerValue):$headerValue));
			}

			// Switch depending on response type
			switch ($this->responseType)
			{
				// Raw
				case 'raw':
					$this->outputRawContent();
					break;

				// JSON
				case 'json':
					$this->outputJsonContent();
					break;

				// XML
				case 'xml':
					$this->outputXmlContent();
					break;

				// Default: HTML
				default:
					$this->outputHtmlContent();
					break;
			}
		}


		/**
		 *   Output content: raw data
		 *   @access public
		 *   @return void
		 */

		public function outputRawContent()
		{
			header('Content-type: '.$this->responseContentType.((!strncmp($this->responseContentType,'text/',5) && strpos($this->responseContentType,'charset=')===FALSE)?'; charset=UTF-8':''));
			if (!array_key_exists('Content-length',$this->responseHeader))
			{
				if (!empty($this->responseContentFile))
				{
					header('Content-length: '.intval(filesize($this->responseContentFile)));
				}
				else
				{
					header('Content-length: '.intval(strlen($this->responseContent)));
				}
			}
			if (!empty($this->responseFileName))
			{
				header('Content-disposition: '.$this->responseContentDisposition.'; filename='.urlencode($this->responseFileName));
			}
			if (!empty($this->responseContentFile))
			{
				readfile($this->responseContentFile);
				@unlink($this->responseContentFile);
			}
			else
			{
				echo $this->responseContent;
			}
		}


		/**
		 *   Output content: JSON
		 *   @access public
		 *   @return void
		 */

		public function outputJsonContent()
		{
			global $LAB;
			// TODO: finish
			if (!empty($this->responseParam['enable_iejsonhack']) && BASE::isIE() && !BASE::isXHR())
			{
				header('Content-type: text/plain; charset=UTF-8');
			}
			else
			{
				header('Content-type: application/json; charset=UTF-8');
			}

			if (is_array($this->responseContent) || is_object($this->responseContent))
			{
				echo json_encode(BASE_TEMPLATE::parseContent($this->responseContent),JSON_FORCE_OBJECT|JSON_HEX_QUOT|JSON_UNESCAPED_UNICODE);
			}
			else
			{
				echo BASE_TEMPLATE::parseContent($this->responseContent);
			}
		}


		/**
		 *   Output content: XML
		 *   @access public
		 *   @return void
		 */

		public function outputXmlContent()
		{
			header('Content-type: text/xml; charset=UTF-8');
			if (strncmp($this->responseContent,'<?xml',5))
			{
				echo '<?xml version="1.0" encoding="UTF-8"?>'."\n";
			}
			echo $this->responseContent;
		}


		/**
		 *   Output content: HTML
		 *   @access public
		 *   @return void
		 */

		public function outputHtmlContent()
		{
			global $LAB;

			// Parse template
			$responseTemplate=new BASE_TEMPLATE((strpos($this->responseTemplate,'/')===FALSE?'response.':'').$this->responseTemplate);
			$responseTemplate->templateVar=$this->responseVar;
			$responseTemplate->templateVar['content']=$this->responseContent;
			$responseContent=$responseTemplate->display();

			// Display HTML
			echo '<!DOCTYPE html>';
			echo '<html lang="'.$LAB->REQUEST->requestLang.'">';
			echo '<head>';

			// Title
			echo '<title>'.$LAB->CONFIG->get('SYSTEM.title').(!empty($this->responseTitle)?' '.$this->responseTitleSeparator.' '.BASE_TEMPLATE::parseContent($this->responseTitle):'').'</title>';
			echo '<meta charset="utf-8">';

			// For our dear friends at Microsoft
			echo '<meta name="MSSmartTagsPreventParsing" content="true">';
			echo '<meta http-equiv="X-UA-Compatible" content="IE=edge">';
			echo '<meta name="msapplication-config" content="/webconfig.xml" />';

			// Viewport
			if (empty($this->responseMeta['viewport']))
			{
				echo '<meta name="viewport" content="width=device-width, initial-scale=1">';
			}

			// Open Graph
			foreach ($this->responseMeta as $param => $content)
			{
				if (preg_match("/^og:/",$param) || preg_match("/^article:/",$param))
				{
					$paramType='property';
				}
				elseif (in_array($param,array('content-language')))
				{
					$paramType='http-equiv';
				}
				else
				{
					$paramType='name';
				}
				echo '<meta '.$paramType.'="'.htmlspecialchars($param).'" content="'.htmlspecialchars($content).'">';
			}

			// Icons
			if (file_exists($LAB->CONFIG->get('site.path').'/site/static/gfx/apple-icon-57x57.png')) echo '<link rel="apple-touch-icon" sizes="57x57" href="/site/static/gfx/apple-icon-57x57.png">';
			if (file_exists($LAB->CONFIG->get('site.path').'/site/static/gfx/apple-icon-60x60.png')) echo '<link rel="apple-touch-icon" sizes="60x60" href="/site/static/gfx/apple-icon-60x60.png">';
			if (file_exists($LAB->CONFIG->get('site.path').'/site/static/gfx/apple-icon-72x72.png')) echo '<link rel="apple-touch-icon" sizes="72x72" href="/site/static/gfx/apple-icon-72x72.png">';
			if (file_exists($LAB->CONFIG->get('site.path').'/site/static/gfx/apple-icon-76x76.png')) echo '<link rel="apple-touch-icon" sizes="76x76" href="/site/static/gfx/apple-icon-76x76.png">';
			if (file_exists($LAB->CONFIG->get('site.path').'/site/static/gfx/apple-icon-76x76.png')) echo '<link rel="apple-touch-icon" sizes="114x114" href="/site/static/gfx/apple-icon-114x114.png">';
			if (file_exists($LAB->CONFIG->get('site.path').'/site/static/gfx/apple-icon-120x120.png')) echo '<link rel="apple-touch-icon" sizes="120x120" href="/site/static/gfx/apple-icon-120x120.png">';
			if (file_exists($LAB->CONFIG->get('site.path').'/site/static/gfx/apple-icon-144x144.png')) echo '<link rel="apple-touch-icon" sizes="144x144" href="/site/static/gfx/apple-icon-144x144.png">';
			if (file_exists($LAB->CONFIG->get('site.path').'/site/static/gfx/apple-icon-152x152.png')) echo '<link rel="apple-touch-icon" sizes="152x152" href="/site/static/gfx/apple-icon-152x152.png">';
			if (file_exists($LAB->CONFIG->get('site.path').'/site/static/gfx/apple-icon-180x180.png')) echo '<link rel="apple-touch-icon" sizes="180x180" href="/site/static/gfx/apple-icon-180x180.png">';
			if (file_exists($LAB->CONFIG->get('site.path').'/site/static/gfx/android-icon-192x192.png')) echo '<link rel="icon" type="image/png" sizes="192x192"  href="/site/static/gfx/android-icon-192x192.png">';
			if (file_exists($LAB->CONFIG->get('site.path').'/site/static/gfx/favicon-32x32.png')) echo '<link rel="icon" type="image/png" sizes="32x32" href="/site/static/gfx/favicon-32x32.png">';
			if (file_exists($LAB->CONFIG->get('site.path').'/site/static/gfx/favicon-96x96.png')) echo '<link rel="icon" type="image/png" sizes="96x96" href="/site/static/gfx/favicon-96x96.png">';
			if (file_exists($LAB->CONFIG->get('site.path').'/site/static/gfx/favicon-16x16.png')) echo '<link rel="icon" type="image/png" sizes="16x16" href="/site/static/gfx/favicon-16x16.png">';
			if (file_exists($LAB->CONFIG->get('site.path').'/site/static/gfx/ms-icon-144x144.png'))
			{
				echo '<meta name="msapplication-TileColor" content="#ffffff">';
				echo '<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">';
			}
			if (file_exists($LAB->CONFIG->get('site.path').'/favicon.ico'))
			{
				echo '<link rel="shortcut icon" href="/favicon.ico" />';
			}

			// Additional head tags
			foreach ($this->responseHeadTags as $tag)
			{
				echo $tag;
			}

			// CSS
			echo $this->CSS->outputCSS();

			// JavaScript
			echo $this->JS->outputJS();

			// Other
			if (!empty($this->responseParam['header_js']))
			{
				echo $this->responseParam['header_js'];
			}

			echo '</head>';
			echo '<body'.(!empty($this->responseParam['bodyclass'])?' class="'.htmlspecialchars($this->responseParam['bodyclass']).'"':'').'>';

			// Display template
			echo $responseContent;

			// Profiler info and errors
			echo $LAB->DEBUG->display();

			echo '</body>';
			echo '</html>';
		}


	}


	?>