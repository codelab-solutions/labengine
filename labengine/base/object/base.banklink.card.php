<?php


/**
 *
 *   LabEngine™ 7
 *   BankLink integration: Credit Cards
 *
 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
 *
 */


class BASE_BANKLINK_CARD extends BASE_BANKLINK_ECOMMERCE
{


    /**
     *   Bank ID
     *   @var string
     *   @static
     */

    public static $BANK_id = 'card';


    /**
     *   Bank destination URL
     *   @var string
     */

    public $BANK_destination_url = 'https://pos.estcard.ee/ecom/iPayServlet';


}


?>