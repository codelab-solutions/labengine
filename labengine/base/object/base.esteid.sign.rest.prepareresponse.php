<?php


	/**
	 *
	 *   FlaskPHP-Identity-EstEID
	 *   ------------------------
	 *   Digital signing provider for ID card
	 *
	 *   @author   Codelab Solutions OÜ <codelab@codelab.ee>
	 *   @license  https://www.flaskphp.com/LICENSE MIT
	 *
	 */


	class BASE_ESTEID_SIGN_REST_PREPARERESPONSE
	{
		/**
		 * Digest Hex
		 * @var string
		 */

		public $hex = null;

		/**
		 * @return string
		 */
		public function getHex(): string
		{
			return $this->hex;
		}

		/**
		 * @param string $hex
		 * @return PrepareResponse
		 */
		public function setHex(string $hex): BASE_ESTEID_SIGN_REST_PREPARERESPONSE
		{
			$this->hex = $hex;
			return $this;
		}


	}

?>