<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   The LAB SuperObject
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_LABSUPEROBJECT
	{


		/**
		 *   Request ID
		 *   @var string
		 *   @access public
		 */

		public $requestID=NULL;


		/**
		 *   Request start time (for profiling)
		 *   @var integer
		 *   @access public
		 */

		public $requestStartTime=NULL;


		/**
		 *   Request time (for profiling)
		 *   @var integer
		 *   @access public
		 */

		public $totalRequestTime=NULL;


		/**
		 *   Configuration
		 *   @var BASE_CONFIG
		 *   @access public
		 */

		public $CONFIG=NULL;


		/**
		 *   Locale
		 *   @var BASE_LOCALE
		 *   @access public
		 */

		public $LOCALE=NULL;


		/**
		 *   Database connection
		 *   @var BASE_DB_MYSQL
		 *   @access public
		 */

		public $DB=NULL;


		/**
		 *   Session
		 *   @var BASE_SESSION
		 *   @access public
		 */

		public $SESSION=NULL;


		/**
		 *   User
		 *   @var BASE_USER
		 *   @access public
		 */

		public $USER=NULL;


		/**
		 *   Request
		 *   @var BASE_REQUEST
		 *   @access public
		 */

		public $REQUEST=NULL;


		/**
		 *   Response
		 *   @var BASE_RESPONSE
		 *   @access public
		 */

		public $RESPONSE=NULL;


		/**
		 *   Date/time functions
		 *   @var BASE_DATETIME
		 *   @access public
		 */

		public $DATETIME=NULL;


		/**
		 *   Regional settings and formatting
		 *   @var BASE_REGIONAL
		 *   @access public
		 */

		public $REGIONAL=NULL;


		/**
		 *   Cache
		 *   @var BASE_CACHE
		 *   @access public
		 */

		public $CACHE=NULL;


		/**
		 *   Debug & profiler info
		 *   @var BASE_DEBUG
		 *   @access public
		 */

		public $DEBUG=NULL;


		/**
		 *   CMS
		 *   @var CMS_CMSSUPEROBJECT
		 *   @access public
		 */

		public $CMS=NULL;


	}


	?>