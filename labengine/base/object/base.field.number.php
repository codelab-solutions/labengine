<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   Field class: number field
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_FIELD_NUMBER extends BASE_FIELD_TEXT
	{


		/**
		 *  Set display format
		 *  @access public
		 *  @param string title
		 *  @return void
		 */

		public function setDisplayFormat ( $displayFormat )
		{
			$this->param['displayformat']=$displayFormat;
		}


		/**
		 *  Set decimal precision
		 *  @access public
		 *  @param string title
		 *  @return void
		 */

		public function setPrecision ( $precision )
		{
			$this->param['precision']=$precision;
		}


		/**
		 *  Set negative allowed
		 *  @access public
		 *  @param string title
		 *  @return void
		 */

		public function setAllowNegative ( $allowNegative )
		{
			$this->param['allownegative']=$allowNegative;
		}


		/**
		 *  Set minimum value
		 *  @access public
		 *  @param string title
		 *  @return void
		 */

		public function setMinimumValue ( $minimumValue )
		{
			$this->param['minimumvalue']=$minimumValue;
		}


		/**
		 *  Set maximum value
		 *  @access public
		 *  @param string title
		 *  @return void
		 */

		public function setMaximumValue ( $maximumValue )
		{
			$this->param['maximumvalue']=$maximumValue;
		}


		/**
		 *  Show zero value
		 *  @access public
		 *  @param boolean show
		 *  @return void
		 */

		public function showZeroValue ( $showZeroValue )
		{
			$this->param['showzerovalue']=$showZeroValue;
		}


		/**
		 *  Data validate
		 *  @access public
		 *  @return boolean
		 */

		public function validate( $value )
		{
			// Parandame
			$value=trim(preg_replace("/\s+/",'',str_replace(',','.',$value)));

			// Parent
			$e=parent::validate($value);
			if (!empty($e)) return $e;

			// Required, but not set
			if ($this->required() && !floatval($value))
			{
				return '[BASE.FORM.Error.RequiredFieldEmpty]';
			}

			// No point in checking further
			if (strval($value)=='') return '';

			// Checkime
			if ($this->required() && preg_match('/^[0]+(\.[0]*){0,1}$/',$value))
			{
				if ($this->param['minimumvalue']!=$value) return '[BASE.FORM.Error.RequiredFieldEmpty]';
			}
			if ($this->param['allownegative'] && $this->param['precision']>0)
			{
				$filter='/^([\-]){0,1}[0-9]+(\.([0-9]){1,'.intval($this->param['precision']).'}){0,1}$/';
			}
			elseif ($this->param['allownegative'])
			{
				$filter='/^([\-]){0,1}[0-9]+$/';
			}
			elseif ($this->param['precision']>0)
			{
				$filter='/^[0-9]+(\.([0-9]){1,'.intval($this->param['precision']).'}){0,1}$/';
			}
			else
			{
				$filter='/^[0-9]+$/';
			}
			if (!preg_match($filter,$value)) return '[BASE.COMMON.Error.NumberFld.Format]';

			// Minimumvalue
			if (isset($this->param['minimumvalue']) && round($value,$this->param['precision'])<round($this->param['minimumvalue'],$this->param['precision'])) return '[BASE.COMMON.Error.NumberFld.MinValue,'.round($this->param['minimumvalue'],$this->param['precision']).']';

			// Maximumvalue
			if (isset($this->param['maximumvalue']) && round($value,$this->param['precision'])>round($this->param['maximumvalue'],$this->param['precision'])) return '[BASE.COMMON.Error.NumberFld.MaxValue,'.round($this->param['maximumvalue'],$this->param['precision']).']';

			// All goode!
			return '';
		}


		/**
		 *  Save value
		 *  @access public
		 *  @return mixed value
		 */

		public function saveValue()
		{
			$value=$this->getValue();
			$value=trim(preg_replace("/\s+/",'',str_replace(',','.',$value)));
			return $value;
		}


		/**
		 *   Display value
		 *   @access public
		 *   @return mixed value
		 */

		public function displayValue()
		{
			$value=$this->getValue();

			// Value
			if (!empty($this->param['displayformat']))
			{
				$value=sprintf($this->param['displayformat'],$value);
			}
			else
			{
				if (!floatval($value) && empty($this->param['showzerovalue'])) return '';
				$value=sprintf("%.0".$this->param['precision']."f",$value);
			}

			// Comment
			if (!empty($this->param['comment']))
			{
				$value.=' '.$this->param['comment'];
			}

			return $value;
		}


		/**
		 *  Display the field
		 *  @access public
		 *  @return string contents
		 */

		public function displayField( $value, $row=NULL )
		{
			// Style
			$this->param['fieldstyle']=(!empty($this->param['fieldclass'])?' ':'').'width: 110px !important; text-align: center !important';

			// Add field class
			$this->param['fieldclass']=(!empty($this->param['fieldclass'])?' ':'').'numberfield';

			// Format value
			if (!empty($this->param['displayformat']))
			{
				$value=sprintf($this->param['displayformat'],$value);
			}
			else
			{
				if ($value!='' || $this->param['showzerovalue']!==false)
				{
					// TODO: write local function: $value=BASE::formatDecimalValue($value,TRUE,FALSE);
					$value=sprintf("%.0".$this->param['precision']."f",$value);
				}
			}
			if (!floatval($value) && $this->param['showzerovalue']==false)
			{
				$value='';
			}

			// Empty value format
			if ($this->param['precision']>0)
			{
				$this->param['emptyformat']=sprintf("%.0".$this->param['precision']."f",0);
			}
			else
			{
				$this->param['emptyformat']='0';
			}

			// Call parent
			return parent::displayField($value,$row);
		}


		/**
		 *   List value
		 *   @access public
		 *   @param string $value Column value
		 *   @param array $row Row pointer
		 *   @return mixed value
		 */

		public function listValue( $value, &$row )
		{
			if (!floatval($value) && $this->param['showzerovalue']===false) return '';
			return parent::listValue($value,$row);
		}


	}


?>