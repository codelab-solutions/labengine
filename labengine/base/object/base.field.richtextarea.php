<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   Field class: rich text area
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_FIELD_RICHTEXTAREA extends BASE_FIELD
	{


		/**
		 *   Allow HTML
		 *   @var bool
		 *   @access public
		 */

		public $allowHTML=true;


		/**
		 *  Set height
		 *  @access public
		 *  @param string height
		 *  @return void
		 */

		public function setHeight ( $height )
		{
			$this->param['height']=$height;
		}


		/**
		 *   Display value
		 *   @access public
		 *   @return mixed value
		 */

		public function displayValue()
		{
			return $this->getValue();
		}


		/**
		 *  Display the field
		 *  @access public
		 *  @return string contents
		 */

		public function displayField( $value, $row=NULL )
		{
			global $LAB;

			// Style
			$style=array();
			if (!empty($this->param['fieldstyle'])) $style[]=$this->param['fieldstyle'];

			// Class
			$class=array();
			$class[]='form-control';
			if (!empty($this->param['fieldclass'])) $class[]=$this->param['fieldclass'];

			// Render
			$c='<div class="'.$this->getContentWidthClass().'">';

				$value = str_replace('{','&#123;',$value);
				$value = str_replace('}','&#125;',$value);
				$value = str_replace('[','&#091;',$value);
				$value = str_replace(']','&#093;',$value);

				$c.='<textarea';
				$c.=' id="'.$this->tag.'"';
				$c.=' name="'.$this->tag.'"';
				$c.=' data-originalvalue="'.htmlspecialchars($value).'"';
				$c.=' autocomplete="off"';
				$c.=' wrap="'.oneof($this->param['wrap'],'soft').'"';
				$c.=' class="'.join(' ',$class).'"';
				if (!empty($style)) $c.=' style="'.join('; ',$style).'"';
				if (!empty($this->param['readonly'])) $c.=' readonly="readonly"';
				if (!empty($this->param['disabled'])) $c.=' disabled="disabled"';
				if (!empty($this->param['keephiddenvalue'])) $c.=' data-keephiddenvalue="1"';
				if (isset($this->param['event']) && is_array($this->param['event']))
				{
					foreach ($this->param['event'] as $eventType => $eventContent) $c.=' '.$eventType.'="'.$eventContent.'"';
				}
				if (isset($this->param['data']) && is_array($this->param['data']))
				{
					foreach ($this->param['data'] as $dataKey => $dataValue) $c.=' data-'.$dataKey.'="'.htmlspecialchars($dataValue).'"';
				}
				$c.='>';
				$c.=$value;
				$c.='</textarea>';

				// Comment
				$c.=$this->displayComment();

			$c.='</div>';

			// JavaScript
			if (!empty($this->formObject))
			{
				$initJS='
					$("#'.$this->tag.'").summernote({
						popover: {
            	image: [
              	[\'custom\', [\'imageAttributes\']],
                [\'imagesize\', [\'imageSize100\', \'imageSize50\', \'imageSize25\']],
                [\'float\', [\'floatLeft\', \'floatRight\', \'floatNone\']],
                [\'custom\', [\'imageShapes\']],
                [\'remove\', [\'removeMedia\']]
              ],
            },
              lang: \'en-US\', // Change to your chosen language
            	imageAttributes:{
              	icon:\'<i class="note-icon-pencil"/>\',
              	removeEmpty:false, // true = remove attributes | false = leave empty if present
              	disableUpload: true // true = don\'t display Upload Options | Display Upload Options
              },
            popatmouse:false,
						height: "'.oneof($this->param['height'],'200px').'",
						onkeyup: function(e){
               $("#'.$this->tag.'").val($(".note-editable").code());
              },
               onblur: function(e){
               $("#'.$this->tag.'").val($(".note-editable").code());
              }
					});
				';

				if ($this->formObject instanceof BASE_DIALOGFORM)
				{
					$this->formObject->setDisplayTrigger($initJS.'BASE.centerModal();');
				}
				else
				{
					$this->formObject->addJS($initJS);
				}
			}

			// Return
			return $c;
		}


	}


?>