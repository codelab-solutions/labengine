<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   The dataobject parameters class
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_DATAOBJECT_PARAM
	{


		/**
		 * Query type
		 * @var string
		 * @access public
		 */
		public $paramType = NULL;

		/**
		 * Table(s)
		 * @var string
		 * @access public
		 */
		public $paramTable = NULL;
		public $paramTableJoinCondition = NULL;
		public $paramTableJoinType = NULL;

		/**
		 * Columns
		 * @var array
		 * @access public
		 */
		public $paramColumns = NULL;

		/**
		 * Skip columns
		 * @var array
		 * @access public
		 */
		public $paramSkipColumns = NULL;

		/**
		 * WHERE clauses
		 * @var array
		 * @access public
		 */
		public $paramWhere = NULL;

		/**
		 * WHERE type (or/and)
		 * @var array
		 * @access public
		 */
		public $paramWhereType = 'and';

		/**
		 * GROUP BY clauses
		 * @var array
		 * @access public
		 */
		public $paramGroupBy = NULL;

		/**
		 * HAVING clauses
		 * @var array
		 * @access public
		 */
		public $paramHaving = NULL;

		/**
		 * ORDER BY
		 * @var array
		 * @access public
		 */
		public $paramOrderBy = NULL;

		/**
		 * LIMIT
		 * @var int
		 * @access public
		 */
		public $paramLimit = NULL;

		/**
		 * LIMIT offset
		 * @var int
		 * @access public
		 */
		public $paramLimitOffset = NULL;

		/**
		 * Calculate found rows
		 * @var boolean
		 * @access public
		 */
		public $calcFoundRows = FALSE;


		/**
		 *  Add table(s)
		 *  @access public
		 *  @param string table
		 *  @param string JOIN condition
		 *  @param string JOIN type
		 *  @return void
		 */

		public function addTable ( $table, $joinCondition=FALSE, $joinType='LEFT JOIN' )
		{
			if (!is_array($this->paramTable)) $this->paramTable=array();
			$this->paramTable[$table]=$table;
			$this->paramTableJoinCondition[$table]=$joinCondition;
			$this->paramTableJoinType[$table]=$joinType;
		}


		/**
		 *  Add columns
		 *  @access public
		 *  @param string|array columns
		 *  @return void
		 */

		public function addColumns ( $columns )
		{
			if (!is_array($this->paramColumns)) $this->paramColumns=array();
			$columns=str_array($columns);
			foreach ($columns as $col)
			{
				$this->paramColumns[$col]=$col;
			}
		}


		/**
		 *  Add skip columns
		 *  @access public
		 *  @param string|array columns
		 *  @return void
		 */

		public function addSkipColumns ( $columns )
		{
			if (!is_array($this->paramSkipColumns)) $this->paramSkipColumns=array();
			$columns=str_array($columns);
			foreach ($columns as $col)
			{
				$this->paramSkipColumns[$col]=$col;
			}
		}


		/**
		 *  Add WHERE clause(s)
		 *  @access public
		 *  @param string|array WHERE clause(s)
		 *  @return void
		 */

		public function addWhere ( $where )
		{
			if (!is_array($this->paramWhere)) $this->paramWhere=array();
			$this->paramWhere[$where]=$where;
		}


		/**
		 *  Set WHERE type
		 *  @access public
		 *  @param string WHERE type (and|or)
		 *  @return void
		 */

		public function setWhereType ( $whereType )
		{
			$whereType=mb_strtolower($whereType);
			switch($whereType)
			{
				case 'and':
				case 'or':
					$this->paramWhereType=$whereType;
					return;
				default:
					throw new Exception('Invalid where type.');
					return;
			}
		}


		/**
		 *  Add GROUP BY clause(s)
		 *  @access public
		 *  @param string|array GROUP BY clause(s)
		 *  @return void
		 */

		public function addGroupBy ( $groupBy )
		{
			if (!is_array($this->paramGroupBy)) $this->paramGroupBy=array();
			$groupBy=str_array($groupBy);
			foreach ($groupBy as $gb)
			{
				$this->paramGroupBy[$gb]=$gb;
			}
		}


		/**
		 *  Add HAVING clause(s)
		 *  @access public
		 *  @param string|array HAVING clause(s)
		 *  @return void
		 */

		public function addHaving ( $having )
		{
			if (!is_array($this->paramHaving)) $this->paramHaving=array();
			$having=str_array($having);
			foreach ($having as $h)
			{
				$this->paramHaving[$h]=$h;
			}
		}


		/**
		 *  Add ORDER BY clause(s)
		 *  @access public
		 *  @param string|array ORDER BY clause(s)
		 *  @return void
		 */

		public function addOrderBy ( $orderBy )
		{
			if (!is_array($this->paramOrderBy)) $this->paramOrderBy=array();
			$orderBy=str_array($orderBy);
			foreach ($orderBy as $ob)
			{
				$this->paramOrderBy[$ob]=$ob;
			}
		}


		/**
		 *  Add LIMIT
		 *  @access public
		 *  @param int limit
		 *  @param int offset
		 *  @return void
		 */

		public function addLimit ( $limit, $offset=NULL )
		{
			$this->paramLimit=$limit;
			$this->paramLimitOffset=$offset;
		}


		/**
		 *  Get dataobject with specified columns (useful for $DAO->save())
		 *  @access public
		 *  @param string columns
		 *  @return BASE_DATAOBJECT_PARAM
		 */

		public static function columns ( $columns )
		{
			$DAO=new BASE_DATAOBJECT_PARAM();
			$DAO->addColumns($columns);
			return $DAO;
		}


	}


?>