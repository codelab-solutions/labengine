<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   DigiDoc helper functions: DDOC
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_DIGIDOC_DDOC extends BASE_DIGIDOC
	{


		/**
		 *   Parse file
		 *   @access public
		 *   @var string $filename
		 *   @throws Exception
		 *   @return void
		 */

		public function parse( $filename )
		{
			global $LAB;

			// Read file contents
			$docSource=file_get_contents($filename);
			if (!mb_strlen($docSource)) throw new Exception('Empty file.');

			// Parse XML
			try
			{
				$XML=new SimpleXMLElement($docSource);
			}
			catch (Exception $e)
			{
				if ($LAB->DEBUG->devEnvironment) throw new Exception('Error parsing ddoc XML: '.$e->getMessage());
				throw new Exception('Error reading .ddoc format.');
			}

			// Check document attributes
			$docAttributes=$XML->attributes();
			if (strval($docAttributes->format)!='DIGIDOC-XML')
			{
				if ($LAB->DEBUG->devEnvironment) throw new Exception('Error parsing ddoc: format not DIGIDOC-XML');
				throw new Exception('Error reading .ddoc format.');
			}
			if (!mb_strlen($docAttributes->version))
			{
				if ($LAB->DEBUG->devEnvironment) throw new Exception('Error parsing ddoc: version not specified');
				throw new Exception('Error reading .ddoc format.');
			}
			$this->info['version']=strval($docAttributes->version);

            $fileIdx=1;

            // Read files
			foreach ($XML->DataFile as $FILE)
			{
				$fileAttributes=$FILE->attributes();

                $fileData = new BASE_DIGIDOC_FILE();
                $fileData->id=intval($fileIdx);
                $fileData->filename=strval($fileAttributes->Filename);
				//$fileData['encoding']=strval($fileAttributes->ContentType);
				if (strval($fileAttributes->MimeType)=='application/octet-stream')
				{
                    $fileData->contenttype=BASE::getMimeType($fileData->filename);
				}
				else
				{
                    $fileData->contenttype=strval($fileAttributes->MimeType);
				}
                $fileData->size=intval($fileAttributes->Size);
                $fileData->readable=true;
                //$fileData->contentFilename = $this->path . '/' . $fileFullPath;

				$this->files[]=$fileData;
                $fileIdx++;
            }

			// Read signatures
			foreach ($XML->Signature as $SIGNATURE)
			{
				$signatureAttributes=$SIGNATURE->attributes();

                $Signature=new BASE_BDOC_SIGNATURE();
                //$Signature['id']=strval($signatureAttributes->Id);

				// Parse signature
				foreach ($SIGNATURE->KeyInfo->X509Data->X509Certificate as $CERT)
				{
					$certificateData=openssl_x509_parse('-----BEGIN CERTIFICATE-----'."\n".trim($CERT)."\n".'-----END CERTIFICATE-----');
					if ($certificateData!==false && is_array($certificateData))
					{
                        $Signature->certificate=$certificateData['name'];
                        $Signature->country=$certificateData['subject']['C'];
                        $Signature->name_last=$certificateData['subject']['SN'];
                        $Signature->name_first=$certificateData['subject']['GN'];
                        $Signature->idcode=$certificateData['subject']['serialNumber'];
					}
				}

				// Signed time
				if (!empty($SIGNATURE->Object->QualifyingProperties->SignedProperties->SignedSignatureProperties->SigningTime))
				{
                    $Signature->signed_time=strtotime(strval($SIGNATURE->Object->QualifyingProperties->SignedProperties->SignedSignatureProperties->SigningTime));
				}

				// Role
				if (!empty($SIGNATURE->Object->QualifyingProperties->SignedProperties->SignedSignatureProperties->SignerRole->ClaimedRoles->ClaimedRole))
				{
					foreach ($SIGNATURE->Object->QualifyingProperties->SignedProperties->SignedSignatureProperties->SignerRole->ClaimedRoles->ClaimedRole as $ROLE)
					{
                        $Signature->role=strval($SIGNATURE->Object->QualifyingProperties->SignedProperties->SignedSignatureProperties->SignerRole->ClaimedRoles->ClaimedRole);
						break;
					}
				}

				$this->signatures[]=$Signature;
			}

		}


        /**
         *
         *   Create a new container
         *   @access public
         *   @static
         *   @throws Exception
         *   @return BASE_DIGIDOC_DDOC
         *
         */

        public static function createContainer()
        {
            // Ddoc is deprated.
            throw new Exception('The ddoc format is deprecated. Please use bdoc instead.');
        }


        /**
         *
         *   Add a file to container
         *   @access public
         *   @param string $sourceFile Source file
         *   @param string $fileName File name (if not the same as source file)
         *   @throws Exception
         *   @return void
         *
         */

        public function addFile( $sourceFile, $fileName=null )
        {
            // Ddoc is deprated.
            throw new Exception('The ddoc format is deprecated and cannot be modified. Please use bdoc instead.');
        }


        /**
         *
         *   Add a file to container from string
         *   @access public
         *   @param string $fileContent File content
         *   @param string $fileName File name
         *   @throws Exception
         *   @return void
         *
         */

        public function addFileFromString( $fileContent, $fileName )
        {
            // Ddoc is deprated.
            throw new Exception('The ddoc format is deprecated and cannot be modified. Please use bdoc instead.');
        }


        /**
         *
         *   Get DigiDoc container file
         *   @access public
         *   @param bool $getHashcoded Get hashcoded version?
         *   @throws Exception
         *   @return string
         *
         */

        public function getDigiDoc( $getHashcoded=false )
        {
            // Ddoc is deprated.
            throw new Exception('The ddoc format is deprecated. Please use bdoc instead.');
        }


        /**
         *
         *   Close the container
         *   @access public
         *   @throws Exception
         *   @return void
         *
         */

        public function closeContainer()
        {
            // Nothing needs to be done here.
        }



    }


?>