<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   BankLink integration: Luminor
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_BANKLINK_LUMINOR extends BASE_BANKLINK_IPIZZA
	{


		/**
		 *   Bank ID
		 *   @var string
		 *   @static
		 */

		public static $BANK_id = 'luminor';


		/**
		 *   Bank destination URL
		 *   @var string
		 */

		public $BANK_destination_url = 'soslastekyla.banklink.nordea.php';


	}


?>