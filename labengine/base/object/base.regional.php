<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   The regional settings i18n class
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_REGIONAL
	{


		/**
		 *   Decimal separators
		 */

		public static $SET_regional_decimalseparator = array(
			'.' => '.',
			',' => ','
		);


		/**
		 *   Current/default decimal separator
		 */

		public $REGIONAL_decimalseparator = '.';


		/**
		 *   Thousand separators
		 */

		public static $SET_regional_thousandseparator = array(
			''  => 'none',
			' ' => 'space',
			'.' => '.',
			',' => ',',
			"'" => "'"
		);


		/**
		 *   Current/default decimal separator
		 */

		public $REGIONAL_thousandseparator = ' ';


		/**
		 *   Currency placement
		 */

		public static $SET_regional_currencyplacement = array(
			'afterspace' => 'after value, separated with a space',
			'after' => 'after value',
			'beforespace' => 'before value, separated with a space',
			'before' => 'before',
		);


		/**
		 *   Current/default decimal separator
		 */

		public $REGIONAL_currencyplacement = 'afterspace';


		/**
		 *   Set decimal separator
		 *   @access public
		 *   @param string $decimalSeparator Decimal separator
		 *   @return void
		 *   @throws Exception
		 */

		public function setDecimalSeparator( $decimalSeparator )
		{
			if (!array_key_exists($decimalSeparator,BASE_REGIONAL::$SET_regional_decimalseparator)) throw new Exception('Unknown decimal separator.');
			$this->REGIONAL_decimalseparator=$decimalSeparator;
		}


		/**
		 *   Set thousands separator
		 *   @access public
		 *   @param string $thousandSeparator Thousands separator
		 *   @return void
		 *   @throws Exception
		 */

		public function setThousandSeparator( $thousandSeparator )
		{
			if (!array_key_exists($thousandSeparator,BASE_REGIONAL::$SET_regional_thousandseparator)) throw new Exception('Unknown thousands separator.');
			$this->REGIONAL_thousandseparator=$thousandSeparator;
		}


		/**
		 *   Set currency placement
		 *   @access public
		 *   @param string $currencyPlacement Currency placement
		 *   @return void
		 *   @throws Exception
		 */

		public function setCurrencyPlacement( $currencyPlacement )
		{
			if (!array_key_exists($currencyPlacement,BASE_REGIONAL::$SET_regional_currencyplacement)) throw new Exception('Unknown currency placement.');
			$this->REGIONAL_currencyplacement=$currencyPlacement;
		}


		/**
		 *   Format decimal value
		 *   @access public
		 *   @param float $value Value
		 *   @param int $precision Max precision
		 *   @param bool $trim Trim decimal value if there is none
		 *   @return string
		 *   @throws Exception
		 */

		public function formatDecimalValue( $value, $precision=2, $trim=false )
		{
			$formattedValue=number_format($value,$precision,$this->REGIONAL_decimalseparator,$this->REGIONAL_thousandseparator);
			if ($precision && $trim)
			{
				$formattedValue=rtrim($formattedValue,'0');
				$formattedValue=preg_replace("/\\".$this->REGIONAL_decimalseparator."$/","",$formattedValue);
			}
			return $formattedValue;
		}


		/**
		 *   Format currency value
		 *   @access public
		 *   @param float $value Value
		 *   @param string $currency Currency
		 *   @param bool $useCurrencySymbol Use currency symbol if exists
		 *   @return string
		 *   @throws Exception
		 */

		public function formatCurrency( $value, $currency, $useCurrencySymbol=false )
		{
			$precision=intval(BASE_CURRENCYDATA::$SET_standard_currency[$currency]['decimals']);
			$formattedValue=$this->formatDecimalValue($value,$precision,($precision?false:true));
			if ($useCurrencySymbol && !empty(BASE_CURRENCYDATA::$SET_standard_currency[$currency]['symbol'])) $currency=BASE_CURRENCYDATA::$SET_standard_currency[$currency]['symbol'];
			switch ($this->REGIONAL_currencyplacement)
			{
				case 'afterspace':
					$formattedValue=$formattedValue.' '.$currency;
					break;
				case 'after':
					$formattedValue=$formattedValue.$currency;
					break;
				case 'beforespace':
					$formattedValue=$currency.' '.$formattedValue;
					break;
				case 'before':
					$formattedValue=$currency.$formattedValue;
					break;
				default:
					throw new Exception('Unknown currency placement setting.');
			}
			return $formattedValue;
		}


	}


?>