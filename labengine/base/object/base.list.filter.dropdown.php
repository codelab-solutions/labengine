<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   The text list filter class
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_LIST_FILTER_DROPDOWN extends BASE_LIST_FILTER
	{

		var $filterGrouping = FALSE;
		var $filterZeroHack = FALSE;
		var $filterShowAll  = TRUE;

		function __construct()
		{
			$this->filterExact=TRUE;
		}

		public function setGrouping ( $grouping )
		{
			$this->filterGrouping=$grouping;
		}

		public function setZeroHack ( $zeroHack )
		{
			$this->filterZeroHack=$zeroHack;
		}

		public function setShowAll ( $showAll )
		{
			$this->filterShowAll=$showAll;
		}

		public function getCriteria ( $value, &$paramObject )
		{
			if (!mb_strlen($value)) return;

			if ($this->filterZeroHack) $value=preg_replace("/^val_/","",$value);
			if ($this->filterExact)
			{
				$cValue="='".addslashes($value)."'";
			}
			else
			{
				$cValue=" like '%".addslashes($value)."%'";
			}

			$this->applyCriteria($cValue, $paramObject);
		}

		public function displayField( $value, $row=NULL )
		{
			$options=$this->getOptions();

			$c='<select';
				$c.=' class="form-control"';
				$c.=' id="filter_'.$this->tag.'"';
				$c.=' name="filter_'.$this->tag.'"';
				if (sizeof($this->filterEvent))
				{
					foreach ($this->filterEvent as $eventType => $eventContent) $c.=' '.$eventType.'="'.$eventContent.'"';
				}
			$c.='>';

			if ($this->filterShowAll)
			{
				$c.='<option value="">--- [BASE.COMMON.All] ---</option>';
			}

			if ($this->filterGrouping)
			{
				foreach ($options as $optgroup_name => $optgroup_contents)
				{
					$c.='<optgroup label="'.htmlspecialchars($optgroup_name).'">';
					foreach ($optgroup_contents as $key => $val)
					{
						$c.='<option value="'.$key.'"'.($key==$this->getValue()?' selected="selected"':'').'>'.$val.'</option>';
					}
					$c.='</optgroup>';
				}
			}
			else
			{
				foreach ($options as $key => $val)
				{
					if ($this->filterZeroHack) $key='val_'.$key;
					$c.='<option value="'.$key.'"'.($key==$this->getValue()?' selected="selected"':'').'>'.$val.'</option>';
				}
			}

			$c.='</select>';
			return $c;
		}

	}


?>