<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   Field class: subtitle field
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_FIELD_SUBTITLE extends BASE_FIELD
	{


		/**
		 *  No value.
		 *  @access public
		 *  @return boolean
		 */

		public function getValue( $lang=NULL )
		{
			return FALSE;
		}


		/**
		 *  No save.
		 *  @access public
		 *  @return boolean
		 */

		public function noSave()
		{
			return TRUE;
		}


		/**
		 *  Data validate
		 *  @access public
		 *  @return boolean
		 */

		public function validate( $value )
		{
			return '';
		}


		/**
		 *  Log data
		 *  @access public
		 *  @return boolean
		 */

		public function getLogData( $dataObject, $operation, &$logValuesArray )
		{
			unset($logValuesArray[$this->tag]);
			return '';
		}


		/**
		 *  Display the label
		 *  @access public
		 *  @return string contents
		 */

		public function displayLabel( $value, $row=NULL )
		{
			$c='<h4 class="col-lg-12'.(!empty($this->param['titleclass'])?' '.$this->param['titleclass']:'').' field-subtitle" style="text-align: center">';
			$c.=$this->param['title'];
			$c.='</h4>';
			return $c;
		}


		/**
		 *  Display the field
		 *  @access public
		 *  @return string contents
		 */

		public function displayField( $value, $row=NULL )
		{
			return '';
		}


	}


?>