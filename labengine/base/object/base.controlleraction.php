<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   The controller action class
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_CONTROLLERACTION
	{


		/**
		 *  Backreference to controller object
		 *  @var BASE_CONTROLLEROBJECT
		 *  @access public
		 */
		public $controllerObject = NULL;

		/**
		 *  Login required?
		 *  @var string
		 *  @access public
		 */
		public $actionLoginRequired = 'inherit';

		/**
		 *  Login URL
		 *  @var string
		 *  @access public
		 */
		public $actionLoginURL = NULL;

		/**
		 *  Authorized roles
		 *  @var string
		 *  @access public
		 */
		public $actionRoleRequired = FALSE;

		/**
		 *  Handler instance variable name
		 *  @var string
		 *  @access public
		 */
		public $actionHandler = NULL;

		/**
		 *  Handler class
		 *  @var string
		 *  @access public
		 */
		public $actionClass = NULL;

		/**
		 *  Include
		 *  @var string|array
		 *  @access public
		 */
		public $actionInclude = NULL;


		/**
		 *  Set handler
		 *  @access public
		 *  @param string $actionHandler Action handler
		 *  @return void
		 */

		public function setHandler ( $actionHandler )
		{
			$this->actionHandler=$actionHandler;
		}


		/**
		 *  Set class
		 *  @access public
		 *  @param string $actionClass Class name
		 *  @return void
		 */

		public function setClass ( $actionClass )
		{
			$this->actionClass=$actionClass;
		}


		/**
		 *  Set include(s)
		 *  @access public
		 *  @param string|array $actionInclude Include(s)
		 *  @return void
		 */

		public function setInclude ( $actionInclude )
		{
			$this->actionInclude=$actionInclude;
		}


		/**
		 *  Set required privileges
		 *  @access public
		 *  @param string $roleRequired Required role(s)
		 *  @return void
		 */

		public function setRoleRequired ( $roleRequired )
		{
			$this->actionRoleRequired=$roleRequired;
		}


		/**
		 *  Set login required
		 *  @access public
		 *  @param string $loginRequired Required role(s)
		 *  @return void
		 */

		public function setLoginRequired ( $loginRequired )
		{
			$this->actionLoginRequired=$loginRequired;
		}


		/**
		 *  Set login URL
		 *  @access public
		 *  @param string $roleRequired Required role(s)
		 *  @return void
		 */

		public function setLoginURL ( $loginURL )
		{
			$this->actionLoginURL=$loginURL;
		}


	}


?>