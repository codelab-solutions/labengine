<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   Field class: chooser field
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_FIELD_CHOOSER extends BASE_FIELD
	{


		/**
		 *   Allow multiple items to be selected
		 *   @param bool $allowMultiple Allow multiple
		 */

		public function setAllowMultiple( $allowMultiple )
		{
			$this->param["multiple"]=$allowMultiple;
		}


		/**
		 *   Set chooser URL
		 *   @param string $chooser_url Chooser URL
		 */

		public function setChooserURL( $chooser_url )
		{
			$this->param["chooser_url"]=$chooser_url;
		}


		/**
		 *   Set chooser title
		 *   @param string $chooser_title Chooser URL
		 */

		public function setChooserTitle( $chooser_title )
		{
			$this->param["chooser_title"]=$chooser_title;
		}


		/**
		 *   Set chooser placeholder
		 *   @param string $chooser_placeholder Chooser placeholder
		 */

		public function setChooserPlaceholder( $chooser_placeholder )
		{
			$this->param["chooser_placeholder"]=$chooser_placeholder;
		}


		/**
		 *   Set empty value
		 *   @param string $chooser_emptyvalue Empty value title
		 */

		public function setChooserEmptyValue( $chooser_emptyvalue )
		{
			$this->param["chooser_emptyvalue"]=$chooser_emptyvalue;
		}


		/**
		 *   Set allow clearing of value
		 *   @param string $chooser_clear Allow clear value
		 */

		public function setChooserClear( $chooser_clear )
		{
			$this->param["chooser_clear"]=$chooser_clear;
		}


		/**
		 *   Set choose button title
		 *   @param string $chooser_choosebuttontitle Button title
		 */

		public function setChooseButtonTitle( $chooser_choosebuttontitle )
		{
			$this->param["chooser_choosebuttontitle"]=$chooser_choosebuttontitle;
		}


		/**
		 *   Set chooser data
		 *   @param array $chooser_data Chooser data (associative array)
		 */

		public function setChooserData( $chooser_data )
		{
			$this->param["chooser_data"]=$chooser_data;
		}


		/**
		 *   Set add form
		 *   @param string $chooser_addform_button Add button title
		 *   @param string $chooser_addform_url Add form URL
		 */

		public function setChooserAddForm( $chooser_addform_button, $chooser_addform_url )
		{
			$this->param["chooser_addform_button"]=$chooser_addform_button;
			$this->param["chooser_addform_url"]=$chooser_addform_url;
		}


		/**
		 *   Display form value
		 *   @param string $value Value
		 *   @return string
		 */

		public function displayFormValue( $value )
		{
			global $LAB;

			switch ($this->fieldSource)
			{
				case 'list':
					return $this->param['source_list'][$value];
				case 'query':
					$dataset=$LAB->DB->querySelectSQL($this->param['source_query'],'value');
					return $dataset[$value]['description'];
				case 'dao':
				case 'dataobject':
					if ($this->fieldSourceDAO instanceof BASE_DATAOBJECT)
					{
						$DAO=$this->fieldSourceDAO;
					}
					else
					{
						$cName=$this->fieldSourceDAO;
						$DAO=new $cName();
					}
					$DAO->load($value);
					return $DAO->{$this->fieldSourceDAOvalueField};
				default:
					return $value;
			}
		}


		/**
		 *  Display the field
		 *  @access public
		 *  @return string contents
		 */

		public function displayField( $value, $row=NULL )
		{
			global $LAB;

			// Style
			$style=array();
			if (!empty($this->param['fieldstyle'])) $style[]=$this->param['fieldstyle'];

			// Class
			$class=array();
			$class[]='form-control';
			if (!empty($this->param['fieldclass'])) $class[]=$this->param['fieldclass'];

			// Render multiple chooser
			if (!empty($this->param["multiple"]))
			{
				$value=str_array($value,"\t");

				$c='<div class="field-chooser field-chooser-multiple '.$this->getContentWidthClass().'" style="overflow: hidden">';

					$c.='<input type="hidden"';
						$c.=' id="'.$this->tag.'_trigger"';
						$c.='name="'.$this->tag.'_trigger"';
						$c.=' value=""';
						if (isset($this->param['event']) && is_array($this->param['event']))
						{
							foreach ($this->param['event'] as $eventType => $eventContent) $c.=' '.$eventType.'="'.$eventContent.'"';
						}
					$c.='>';

					$c.='<div id="'.$this->tag.'_value" class="field-chooser-multiple-valuecontainer">';
						$c.='<p id="'.$this->tag.'_none" class="form-control-static field-chooser-value-none" style="width: 100%'.(empty($value)?'':'; display: none').'"><span class="lightgray">'.oneof($this->param["chooser_emptyvalue"],'[BASE.COMMON.None]').'</span></p>';
						foreach ($value as $v)
						{
							$c.='<p id="'.$this->tag.'_val_'.htmlspecialchars($v).'" class="form-control-static field-chooser-value" style="overflow: hidden; width: 100%">';
							$c.='<input type="hidden" id="'.$this->tag.'_'.htmlspecialchars($v).'" name="'.$this->tag.'[]" value="'.htmlspecialchars($v).'">';
							$c.='<span class="field-chooser-display">'.$this->displayFormValue($v).'</span>';
							$c.='<span class="field-chooser-choose"><a onclick="CHOOSER.removeItem(\''.$this->tag.'\',\''.htmlspecialchars($v).'\')"><span class="icon-cancel"></span></a></span>';
							$c.='</p>';
						}
					$c.='</div>';

					$c.='<div class="field-chooser-multple-choose">';
						$c.='<a onclick="CHOOSER.openChooser(';
							$c.="'".$this->tag."'";
							$c.=",'".$this->param["chooser_url"]."'";
							$c.=','.(!empty($this->param['chooser_title'])?"'".jsencode($this->param['chooser_title'])."'":'null');
							$c.=','.(!empty($this->param['chooser_placeholder'])?"'".jsencode($this->param['chooser_placeholder'])."'":'null');
							$c.=',{multiple:1}';
							if (!empty($this->param['chooser_addform_button']))
							{
								$c.=",'".jsencode($this->param['chooser_addform_button'])."'";
								$c.=",'".jsencode($this->param['chooser_addform_url'])."'";
							}
						$c.=')">'.(oneof($this->param['chooser_choosebuttontitle'],'<span class="icon-search"></span> [BASE.FORM.Chooser.choose]')).'</a>';
					$c.='</div>';

				$c.='</div>';
			}

			// Render
			else
			{
				$c='<div class="field-chooser '.$this->getContentWidthClass().'" style="overflow: hidden">';
				$c.='<p class="form-control-static" style="width: 100%">';

				$c.='<input';
					$c.=' type="hidden"';
					$c.=' id="'.$this->tag.'"';
					$c.=' name="'.$this->tag.'"';
					$c.=' value="'.htmlspecialchars($value).'"';
					if (isset($this->param['event']) && is_array($this->param['event']))
					{
						foreach ($this->param['event'] as $eventType => $eventContent) $c.=' '.$eventType.'="'.$eventContent.'"';
					}
				$c.='>';

				$c.='<span class="field-chooser-display" id="'.$this->tag.'_display">';

				if (!empty($value))
				{
					$c.=$this->displayFormValue($value);
				}
				else
				{
					$c.='<span class="lightgray">'.oneof($this->param["chooser_emptyvalue"],'[BASE.COMMON.NotSet]').'</span>';
				}

				$c.='</span>';

				$c.='<span class="field-chooser-choose">';
					$c.='<a onclick="CHOOSER.openChooser(';
						$c.="'".$this->tag."'";
						$c.=",'".$this->param["chooser_url"]."'";
						$c.=','.(!empty($this->param['chooser_title'])?"'".jsencode($this->param['chooser_title'])."'":'null');
						$c.=','.(!empty($this->param['chooser_placeholder'])?"'".jsencode($this->param['chooser_placeholder'])."'":'null');
						if (!empty($this->param['chooser_data']))
						{
							if (is_object($this->param['chooser_data']) || is_array($this->param['chooser_data']))
							{
								$c.=',{';
									$data=array();
									foreach ($this->param['chooser_data'] as $k => $v)
									{
										$data[]=$k.":'".jsencode($v)."'";
									}
									$c.=join(',',$data);
								$c.='}';
							}
							else
							{
								throw new Exception('Chooser data must be an object or an array.');
							}
						}
						else
						{
							$c.=',{}';
						}
						if (!empty($this->param['chooser_addform_button']))
						{
							$c.=",'".jsencode($this->param['chooser_addform_button'])."'";
							$c.=",'".jsencode($this->param['chooser_addform_url'])."'";
						}
					$c.=')">'.(oneof($this->param['chooser_choosebuttontitle'],'<span class="icon-search"></span> [BASE.FORM.Chooser.choose]')).'</a>';
					if (!empty($this->param['chooser_clear']))
					{
						$c.=' &nbsp; <a onclick="CHOOSER.select(\'\',\''.jsencode('<span class=lightgray>'.oneof($this->param["chooser_emptyvalue"],'[BASE.COMMON.NotSet]').'</span>').'\')"><span class="icon-cancel"></span></a>';
					}
				$c.='</span>';

				$c.='</p>';
				$c.='</div>';
			}

			// Return
			return $c;
		}


	}


?>