<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   The graphics utilities class
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_GFX
	{


		var $imageTypes=array
		(
			'jpeg' => 'image/jpeg',
			'jpg' => 'image/jpeg',
			'gif' => 'image/gif',
			'bmp' => 'image/bmp',
			'png' => 'image/png'
		);


		//
		//  createCanvas()
		//  Create canvas
		//
		//  Args:
		//    $canvasWidth - Width
		//    $canvasHeight - Height
		//    $outputFormat - Output format
		//
		//  Returns:
		//    Image object
		//

		public static function createCanvas( $canvasWidth, $canvasHeight, $outputFormat='jpg' )
		{
			$img=imagecreatetruecolor($canvasWidth,$canvasHeight);
			if ($outputFormat=='png')
			{
				imagecolortransparent($img,imagecolorallocatealpha($img, 0, 0, 0, 127));
				imagealphablending($img,false);
				imagesavealpha($img,true);
			}
			else
			{
				$white = imagecolorallocate($img, 255, 255, 255);
				imagefill($img, 0, 0, $white);
			}
			return $img;
		}


		//
		//  resizeImage()
		//  Resize an image
		//
		//  Args:
		//    $imgdata - original image data
		//    $size - maximum length of the longer side
		//    $watermark - watermark image
		//    $set_width - set width to
		//    $set_height - set height to
		//
		//  Returns:
		//    resized image as a binary string
		//

		public static function resizeImage ( $imgdata, $size=0, $outputFormat='jpg', $watermark='', $set_width=0, $set_height=0, $canvasWidth=0, $canvasHeight=0 )
		{
			// Another dirty hack
			if (!function_exists("imagecreatefromstring"))
			{
				return $imgdata;
			}

			// Lubame output formatiks content-type
			$outputFormat=mb_strtolower(str_replace('image/','',$outputFormat));

			// Polegi vaja midagi teha?
			if (!$size && !$set_width && !$set_height && !$canvasWidth && !$canvasHeight && !strlen($watermark))
			{
				return $imgdata;
			}

			$src = imagecreatefromstring($imgdata);
            if ($src===false) return $imgdata;
			$width = imagesx($src);
			$height = imagesy($src);
			$aspect_ratio = ($height>$width)?($width/$height):($height/$width);

			if ($canvasWidth && $canvasHeight)
			{
				// Create canvas
				$img=static::createCanvas($canvasWidth,$canvasHeight,$outputFormat);

				if ($width<=$canvasWidth && $height<=$canvasHeight)
				{
					$posX=floor(($canvasWidth-$width)/2);
					$posY=floor(($canvasHeight-$height)/2);
					imagecopyresampled($img,$src,$posX,$posY,0,0,$width,$height,$width,$height);
				}
				else
				{
					$rW=($canvasWidth/$width);
					$rH=($canvasHeight/$height);
					if ($rW<$rH)
					{
						$new_w = $canvasWidth;
						if ($width > $height)
						{
							$new_h = abs($new_w * $aspect_ratio);
						}
						else
						{
							$new_h = abs($new_w / $aspect_ratio);
						}
					}
					else
					{
						$new_h = $canvasHeight;
						if ($width > $height)
						{
							$new_w = abs($new_h / $aspect_ratio);
						}
						else
						{
							$new_w = abs($new_h * $aspect_ratio);
						}
					}
					$posX=floor(($canvasWidth-$new_w)/2);
					$posY=floor(($canvasHeight-$new_h)/2);
					imagecopyresampled($img,$src,$posX,$posY,0,0,$new_w,$new_h,$width,$height);
				}
			}
			elseif ($set_width && $set_height)
			{
				$img=static::createCanvas($set_width,$set_height,$outputFormat);
				imagecopyresampled($img,$src,0,0,0,0,$set_width,$set_height,$width,$height);
			}
			elseif ($set_width)
			{
				$new_w = $set_width;
				if ($width > $height)
				{
					$new_h = abs($new_w * $aspect_ratio);
				}
				else
				{
					$new_h = abs($new_w / $aspect_ratio);
				}
				$img=static::createCanvas($new_w,$new_h,$outputFormat);
				imagecopyresampled($img,$src,0,0,0,0,$new_w,$new_h,$width,$height);
			}
			elseif ($set_height)
			{
				$new_h = $set_height;
				if ($width > $height)
				{
					$new_w = abs($new_h / $aspect_ratio);
				}
				else
				{
					$new_w = abs($new_h * $aspect_ratio);
				}
				$img=static::createCanvas($new_w,$new_h,$outputFormat);
				imagecopyresampled($img,$src,0,0,0,0,$new_w,$new_h,$width,$height);
			}
			elseif ($size)
			{
				if ($width <= $size && $height <= $size)
				{
					$new_w = $width;
					$new_h = $height;
					$img=$src;
				}
				else
				{
					if ($height > $width)
					{
						$new_h = $size;
						$new_w = abs($new_h * $aspect_ratio);
					}
					else
					{
						$new_w = $size;
						$new_h = abs($new_w * $aspect_ratio);
					}
					$img=static::createCanvas($new_w,$new_h,$outputFormat);
					imagecopyresampled($img,$src,0,0,0,0,$new_w,$new_h,$width,$height);
				}
			}
			else
			{
				$img=$src;
				$new_w = $width;
				$new_h = $height;
			}

			// Watermark
			if (strlen($watermark))
			{
				imagealphablending($img, true);
				$wmimg=imagecreatefrompng($watermark);
				$wm_w=imagesx($wmimg);
				$wm_h=imagesy($wmimg);
				$wmpos_x=$new_w-$wm_w;
				$wmpos_y=$new_h-$wm_h;
				imagecopy($img,$wmimg,$wmpos_x,$wmpos_y,0,0,$wm_w,$wm_h);
			}

			// Return image
			ob_start();
			imageinterlace($img,1);
			switch($outputFormat)
			{
				case 'png':
					if (imagetypes() & IMG_PNG)
					{
						imagepng($img,NULL,5);
					}
					else
					{
						imagejpeg($img,NULL,95);
					}
					break;
				default:
					imagejpeg($img,NULL,95);
					break;
			}

			$imgsrc=ob_get_contents();
			ob_end_clean();
			imagedestroy($img);
			return $imgsrc;
		}


		//
		//  cropImage()
		//  Resize an image
		//
		//  Args:
		//    $imgdata - original image data
		//    $x1 - X1
		//    $x2 - X2
		//    $y1 - Y1
		//    $y2 - Y2
		//
		//  Returns:
		//    cropped image as a binary string
		//

		public static function cropImage ( $imgdata, $x1, $y1, $x2, $y2 )
		{
			// Another dirty hack
			if (!function_exists("imagecreatefromstring"))
			{
				return $imgdata;
			}

			// Init
			$src = imagecreatefromstring($imgdata);
			$width = ($x2-$x1);
			$height = ($y2-$y1);

			// Crop
			$img=imagecreatetruecolor($width,$height);
			imagecopyresampled($img,$src,0,0,$x1,$y1,$width,$height,$width,$height);

			// Return image
			ob_start();
			imageinterlace($img,1);
			imagejpeg($img,NULL,95);
			$imgsrc=ob_get_contents();
			ob_end_clean();
			imagedestroy($img);
			return $imgsrc;
		}


		//
		//  cropSquare()
		//  Resize an image
		//
		//  Args:
		//    $imgdata - original image data
		//
		//  Returns:
		//    cropped image as a binary string
		//

		public static function cropSquare ( $imgdata )
		{
			// Another dirty hack
			if (!function_exists("imagecreatefromstring"))
			{
				return $imgdata;
			}

			$src = imagecreatefromstring($imgdata);
			$width = imagesx($src);
			$height = imagesy($src);

			// calculating the part of the image to use for thumbnail
			if ($width > $height)
			{
				$thumbSize=$height;
				$x1=round($width/2-($thumbSize/2));
				$x2=$x1+$thumbSize;
				$y1=0;
				$y2=$height;
			}
			else
			{
				$thumbSize=$width;
				$x1=0;
				$x2=$width;
				$y1=round($height/2-($thumbSize/2));
				$y2=$y1+$thumbSize;
			}

			// Crop
			$img=imagecreatetruecolor($thumbSize,$thumbSize);
			imagecopyresampled($img,$src,0,0,$x1,$y1,$thumbSize,$thumbSize,$thumbSize,$thumbSize);

			// Return image
			ob_start();
			imageinterlace($img,1);
			imagejpeg($img,NULL,95);
			$imgsrc=ob_get_contents();
			ob_end_clean();
			imagedestroy($img);
			return $imgsrc;
		}



		//
		//  imageSize()
		//  Get image dimensions
		//
		//  Args:
		//    $imgdata - original image data
		//
		//  Returns:
		//    image dimensions as array(x,y)
		//

		public static function imageSize ( &$imgdata )
		{
			// Another dirty hack
			if (!function_exists("imagecreatefromstring"))
			{
				return array(0,0);
			}

			$src = imagecreatefromstring($imgdata);
			$width = imagesx($src);
			$height = imagesy($src);
			imagedestroy($src);
			return array($width,$height);
		}


		//
		//  displayImage()
		//  Display image, handle caching, etc
		//
		//  Args:
		//    $image_oid - image OID value
		//    $table - table
		//    $idfield - ID field name
		//    $imagefield - image field name
		//    $query_where - additional WHERE conditions if needed
		//    $cachetag - cache tag (if not set, table name is used)
		//
		//  Returns:
		//    (none)
		//

		public static function displayObjectImage ( &$OBJECT, $baseField, $type=null, $param=[] )
		{
			global $LAB;

			// Check
			if (!is_object($OBJECT) || !$OBJECT->_loaded || !intval($OBJECT->{$baseField.'_fsize'}))
			{
				if ($LAB->DEBUG->devEnvironment)
				{
					$LAB->RESPONSE->setType('raw','text/plain');
					$LAB->RESPONSE->setExpires(time());
					if (!is_object($OBJECT))
					{
						return '$OBJECT is not an object.';
					}
					elseif (!$OBJECT->_loaded)
					{
						return '$OBJECT not loaded.';
					}
					else
					{
						return $baseField.'_fsize is 0';
					}
				}
				else
				{
					$LAB->RESPONSE->setType('raw','image/png');
					$LAB->RESPONSE->setExpires(time());
					return file_get_contents(stream_resolve_include_path('base/static/gfx/icon-noimage.png'));
				}
			}

			// Cannot be resized?
			if (!in_array($OBJECT->{$baseField.'_ctype'},array('image/jpeg','image/png','image/gif')))
			{
				if (BASE_FILE::dbStorage())
				{
					$img=$OBJECT->{$baseField};
				}
				else
				{
					$img=BASE_FILE::getFile($OBJECT->_oid,oneof($type,mb_strtolower(get_class($OBJECT))));
				}

				$LAB->RESPONSE->setType('raw',$OBJECT->{$baseField.'_ctype'});
				$LAB->RESPONSE->setContentDisposition('inline');
				$LAB->RESPONSE->setExpires(time()+(array_key_exists('expires',$param)?$param['expires']:(365*86400)));
				$LAB->RESPONSE->setLastModified(time());
				return $img;
			}

			// Init vars
			$watermark='';
			$size=oneof($param['size'],intval($LAB->REQUEST->requestVar['s']));
			$width=oneof($param['width'],intval($LAB->REQUEST->requestVar['w']));
			$height=oneof($param['height'],intval($LAB->REQUEST->requestVar['h']));
			$canvasWidth=oneof($param['canvasWidth'],intval($LAB->REQUEST->requestVar['cw']));
			$canvasHeight=oneof($param['canvasHeight'],intval($LAB->REQUEST->requestVar['ch']));
			$nocache=oneof($param['nocache'],intval($LAB->REQUEST->requestVar['nc']))?TRUE:FALSE;

			// Cache
			if (!$nocache)
			{
				if (BASE_FILE::dbStorage() && mb_strlen($LAB->CONFIG->get('site.cachepath')))
				{
					$cacheFileName=BASE_FILE::getCacheFileName($OBJECT->_oid,oneof($type,mb_strtolower(get_class($OBJECT))));
					if (!empty($size)) $cacheFileName.='.s'.intval($size);
					if (!empty($width)) $cacheFileName.='.w'.intval($width);
					if (!empty($height)) $cacheFileName.='.h'.intval($height);
					if (!empty($canvasWidth)) $cacheFileName.='.cw'.intval($canvasWidth);
					if (!empty($canvasHeight)) $cacheFileName.='.ch'.intval($canvasHeight);
				}
				elseif (!BASE_FILE::dbStorage())
				{
					$cacheFileName=BASE_FILE::getFileName($OBJECT->_oid,oneof($type,mb_strtolower(get_class($OBJECT))));
					if (!empty($size)) $cacheFileName.='.s'.intval($size);
					if (!empty($width)) $cacheFileName.='.w'.intval($width);
					if (!empty($height)) $cacheFileName.='.h'.intval($height);
					if (!empty($canvasWidth)) $cacheFileName.='.cw'.intval($canvasWidth);
					if (!empty($canvasHeight)) $cacheFileName.='.ch'.intval($canvasHeight);
				}
				if (mb_strlen($cacheFileName) && file_exists($cacheFileName))
				{
					$LAB->RESPONSE->setType('raw',oneof($param['force_ctype'],$OBJECT->{$baseField.'_ctype'}));
					$LAB->RESPONSE->setContentDisposition('inline');
					$LAB->RESPONSE->setExpires(time()+(array_key_exists('expires',$param)?$param['expires']:(365*86400)));
					$LAB->RESPONSE->setLastModified(filemtime($cacheFileName));
					return file_get_contents($cacheFileName);
				}
			}

			// Get image
			if (BASE_FILE::dbStorage())
			{
				$img=$OBJECT->{$baseField};
			}
			else
			{
				$img=BASE_FILE::getFile($OBJECT->_oid,oneof($type,mb_strtolower(get_class($OBJECT))));
			}

			// Resize if needed
			switch ($OBJECT->{$baseField.'_ctype'})
			{
				case 'image/png':
					$outputFormat='png';
					break;
				default:
					$outputFormat='jpg';
					break;
			}
			$img=BASE_GFX::resizeImage($img,$size,$outputFormat,'',$width,$height,$canvasWidth,$canvasHeight);

			// Cache: write to cache
			if (!$nocache && mb_strlen($cacheFileName))
			{
				file_put_contents($cacheFileName,$img);
			}

			// Output
			$LAB->RESPONSE->setType('raw',oneof($param['force_ctype'],$OBJECT->{$baseField.'_ctype'}));
			$LAB->RESPONSE->setContentDisposition('inline');
			$LAB->RESPONSE->setExpires(time()+(array_key_exists('expires',$param)?$param['expires']:(365*86400)));
			$LAB->RESPONSE->setLastModified(time());
			return $img;
		}


	}


?>