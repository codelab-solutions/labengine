<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   The date list filter class
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_LIST_FILTER_DATE extends BASE_LIST_FILTER
	{


		/**
		 *   Filterable column is a dDatetime field (default: date)
		 */

		public $fieldDateTime = false;


		/**
		 *  Set field type
		 *  @access public
		 *  @return void
		 */

		public function setFieldDateTime ( $fieldDateTime )
		{
			$this->fieldDateTime=$fieldDateTime;
		}


		/**
		 *  Apply criteria
		 *  @access public
		 *  @param string value
		 *  @param array row
		 *  @return void
		 */

		public function getCriteria ( $value, &$paramObject )
		{
			global $LAB;

			// No value?
			if (!mb_strlen($value)) return;

			// Validate
			try
			{
				$date=$LAB->DATETIME->toTimestamp($value);
			}
			catch (Exception $e)
			{
				$this->error=sprintf($LAB->LOCALE->get('BASE.COMMON.Error.Date.Invalid'),BASE_TEMPLATE::parseContent($LAB->DATETIME->getDateFormat('disp_user')));
				return;
			}

			// Apply
			if ($this->fieldDateTime)
			{
				$cValue=" between '".date('Y-m-d 0000:00:00',$date)."' and '".date('Y-m-d 23:59:59',$date)."'";
			}
			else
			{
				$cValue="='".date('Y-m-d',$date)."'";
			}

			$this->applyCriteria($cValue, $paramObject);
		}


		/**
		 *  Display field
		 *  @access public
		 *  @return string contents
		 */

		public function displayField( $value, $row=NULL )
		{
			global $LAB;

			// Value
			$value=$this->getValue();

			// Render
			$c='<div id="filterwrapper_'.$this->tag.'"><table style="width: 100%; border: none !important;"><tr>';

				$c.='<td style="border: none !important; padding: 0 !important; width: 99%; white-space: nowrap">';
				$c.='<input type="text" class="form-control datefield" style="text-align: center" autocomplete="off" id="filter_'.$this->tag.'" name="filter_'.$this->tag.'" value="'.$value.'" maxlength="10" data-date-format="'.$LAB->DATETIME->getDateFormat('datepicker').'" data-mask="'.$LAB->DATETIME->getDateFormat('mask').'">';
				$c.='</td>';
				$c.='<td style="border: none !important; padding: 0px 0px 0px 5px !important; width: 1%; white-space: nowrap">';
				$c.='<span style="display: inline-block; cursor: pointer" class="icon-calendar" onclick="FORM.openDatepicker(\'#filter_'.$this->tag.'\')"></span>';
				$c.='</td>';

			$c.='</tr></table></div>';
			$c.='<script language="JavaScript"> $(function(){ FORM.initElements(\'#filterwrapper_'.$this->tag.'\'); }); </script>';
			return $c;
		}

	}


?>