<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   Standard Bootstrap info table
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_INFOTABLE
	{


		/**
		 *   Table rows
		 *   @var array
		 */

		public $tableRow=array();


		/**
		 *   Parameters
		 *   @var array
		 */

		public $param=array();


		/**
		 *   Set table class
		 *   @access public
		 *   @param string $tableClass Table class
		 *   @return void
		 */

		public function setTableClass( $tableClass )
		{
			$this->param['tableclass']=$tableClass;
		}


		/**
		 *   Add table row
		 *   @access public
		 *   @param string $label Label
		 *   @param string $content Content
		 *   @param string $emptyValue Empty value
		 *   @param bool $encodeContent Encode HTML content
		 *   @return void
		 */

		public function addRow( $label, $content, $emptyValue=null, $encodeContent=true )
		{
			$c='';
			$c.='<tr>';
			$c.='<th>'.$label.':</th>';
			$c.='<td>';
			// $c.='<div class="list-mobile-label show-on-mobile">'.$label.':</div>';
			if (mb_strlen($content))
			{
				if ($encodeContent)
				{
					$c.=nl2br(htmlspecialchars($content));
				}
				else
				{
					$c.=$content;
				}
			}
			elseif ($emptyValue!==null && $emptyValue!==false)
			{
				$c.='<span class="disabled">';
				if ($encodeContent)
				{
					$c.=nl2br(htmlspecialchars($emptyValue));
				}
				else
				{
					$c.=$emptyValue;
				}
				$c.='</span>';
			}
			$c.='</td>';
			$c.='</tr>';
			$this->tableRow[]=$c;
		}


		/**
		 *   Add data field as table row
		 *   @access public
		 *   @param BASE_DATAOBJECT $dataObject
		 *   @param string|BASE_FIELD $field Field (name or data object)
		 *   @param string $label Label
		 *   @param string $emptyValue Empty value 
		 *   @throws Exception
		 *   @return void
		 */

		public function addDataField( $dataObject, $field, $label=null, $emptyValue=null )
		{
			// Check
			if (is_object($field))
			{
				if (!($field instanceof BASE_FIELD)) throw new Exception('$field must be an instance of BASE_FIELD');
			}
			else
			{
				if (empty($dataObject->_field[$field])) throw new Exception($field.': No such field defined in the data object.');
				$field=$dataObject->_field[$field];
			}
			
			// Field Hack (tm)
			$field->formObject=new BASE_FORM();
			$field->formObject->operation='edit';
			$field->formObject->data=$dataObject;

			// Label
			if ($label===null)
			{
				$label=$field->getTitle();
			}
			
			// Value
			$value=$field->displayValue();
			
			// Add to table
			$this->addRow($label,$value,$emptyValue,false);
		}


		/**
		 *   Add subheader
		 *   @access public
		 *   @param string $label Label
		 *   @return string
		 */

		public function addHeader( $label )
		{
			$this->tableRow[]='<tr><th colspan="2" class="info-table-header">'.htmlspecialchars($label).'</td></tr>';
		}


		/**
		 *   Add divider
		 *   @access public
		 *   @return string
		 */

		public function addDivider()
		{
			$this->tableRow[]='<tr><td colspan="2" class="info-table-divider"></td></tr>';
		}


		/**
		 *   Display table
		 *   @access public
		 *   @return string
		 *   @throws Exception
		 */

		public function displayTable()
		{
			// Check
			if (!sizeof($this->tableRow)) return '';

			// Table begins
			$c='<table class="info-table'.(!empty($this->param['tableclass'])?' '.(is_array($this->param['tableclass'])?join(' ',$this->param['tableclass']):$this->param['tableclass']):'').'">';

				// Content
				$c.=join('',$this->tableRow);

			// Table ends
			$c.='</table>';
			return $c;
		}


	}


?>