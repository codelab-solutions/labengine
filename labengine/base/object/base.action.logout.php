<?


	/**
	 *
	 *   LabEngine™ 7
	 *   Standard actions: logout
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_ACTION_LOGOUT extends BASE_DISPLAYOBJECT
	{


		/**
		 *   Parameters
		 *   @var array
		 *   @access public
		 */
		public $param = array();


		/**
		 *   Set reload
		 *   @access public
		 *   @param bool $reload Reload on success
		 *   @return void
		 */

		public function setReload ( $reload )
		{
			$this->param['reload']=$reload;
		}


		/**
		 *   Set redirect URL
		 *   @access public
		 *   @param string $redirectURL Redirect URL
		 *   @return void
		 */

		public function setRedirectURL ( $redirectURL )
		{
			$this->param['url_redirect']=$redirectURL;
		}


		/**
		 *   Clear session on logout
		 *   @access public
		 *   @param bool $clearSession Clear session on logout
		 *   @return void
		 */

		public function setClearSession ( $clearSession )
		{
			$this->param['clearsession']=$clearSession;
		}


		/**
		 *   Init logout action
		 *   @access public
		 *   @return void
		 *   @throws Exception
		 */

		public function initLogout()
		{
			// This can be implemented in the subclass.
		}


		/**
		 *   Do login
		 *   @access public
		 *   @return void
		 *   @throws Exception
		 */

		public function doLogout()
		{
			global $LAB;
			$LAB->USER->doLogout();
		}


		/**
		 *   Logout trigger
		 *   @access public
		 *   @return void
		 *   @throws Exception
		 */

		public function logoutTrigger()
		{
			// This can be implemented in the subclass.
		}


		/**
		 *   Clear session
		 *   @access public
		 *   @return void
		 *   @throws Exception
		 */

		public function clearSession()
		{
			global $LAB;
			if (!empty($this->param['clearsession']))
			{
				$LAB->SESSION->sessionData=array();
			}
		}


		/**
		 *  Handle action
		 *  @access public
		 *  @return string|array
		 */

		public function display()
		{
			global $LAB;

			// AJAX?
			if (BASE::isXHR() || $LAB->REQUEST->requestMethod=='POST')
			{
				// Init
				$LAB->RESPONSE->setType('json');
				$this->initLogout();

				try
				{
					// Do logout
					$this->doLogout();
					$this->logoutTrigger();
					$this->clearSession();

					// Return
					$response=array();
					$response['status']='1';
					if (!empty($this->param['reload'])) $response['reload']='1';
					if (!empty($this->param['url_redirect'])) $response['redirect']=$this->param['url_redirect'];
					return $response;
				}
				catch (Exception $e)
				{
					$response=array();
					$response['status']='2';
					$response['error']=$e->getMessage();
					return $response;
				}
			}

			// Get
			else
			{
				// Init
				$this->initLogout();

				// Do logout
				$this->doLogout();
				$this->logoutTrigger();
				$this->clearSession();

				// Return login form
				$LAB->RESPONSE->setRedirect(oneof($this->param['url_redirect'],'/'));
			}
		}


	}


?>