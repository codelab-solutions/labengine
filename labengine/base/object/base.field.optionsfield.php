<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   Field class: field with options (dropdown, checklist, optionlist)
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_FIELD_OPTIONSFIELD extends BASE_FIELD
	{


		/**
		 *   List value
		 *   @access public
		 *   @return mixed value
		 */

		public function listValue( $value, &$row )
		{
			$options=$this->getOptions();
			return parent::listValue($options[$value],$row);
		}


		/**
		 *   Display value
		 *   @access public
		 *   @return mixed value
		 */

		public function displayValue()
		{
			$value=$this->getValue();
			$options=$this->getOptions();
			return $options[$value];
		}


		/**
		 *   Log data
		 *   @access public
		 *   @param BASE_DATAOBJECT $dataObject Data object
		 *   @param string $operation Operation
		 *   @param array $logValuesArray Log values array
		 *   @return string
		 */

		public function getLogData( $dataObject, $operation, &$logValuesArray )
		{
			unset($logValuesArray[$this->tag]);
			$options=$this->getOptions();

			if ($operation!='edit' && !mb_strlen($dataObject->{$this->tag})) return;
			if ($operation=='edit')
			{
				if ($dataObject->{$this->tag}==$dataObject->_in[$this->tag]) return;
				if ($dataObject->{$this->tag}=='' && $dataObject->_in[$this->tag]=='0') return;
				if ($dataObject->{$this->tag}=='0' && $dataObject->_in[$this->tag]=='') return;
			}

			$XML='<fld id="'.htmlspecialchars($this->tag).'" name="'.htmlspecialchars($this->param['title']).'">';
			if ($operation!='add' && !$this->param['forcevalue']) $XML.='<old value="'.htmlspecialchars($dataObject->_in[$this->tag]).'">'.htmlspecialchars(oneof($options[$dataObject->_in[$this->tag]],$dataObject->_in[$this->tag])).'</old>';
			$XML.='<new value="'.htmlspecialchars($dataObject->{$this->tag}).'">'.htmlspecialchars(oneof($options[$dataObject->{$this->tag}],$dataObject->{$this->tag})).'</new>';
			$XML.='</fld>';
			return $XML;
		}


		/**
		 *   Get log value
		 *   @access public
		 *   @param mixed $value Value
		 *   @return string
		 */

		public function getLogValue( $value )
		{
			$options=$this->getOptions();
			return oneof($options[$value],$value);
		}


	}


?>