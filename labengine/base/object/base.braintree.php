<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   Braintree library wrapper class
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_BRAINTREE
	{


		/**
		 *   Auth: merchant ID
		 *   @var string
		 *   @public
		 */

		public static $BRAINTREE_environment = 'production';


		/**
		 *   Auth: merchant ID
		 *   @var string
		 *   @public
		 */

		public static $BRAINTREE_merchant_id = '';


		/**
		 *   Auth: public key
		 *   @var string
		 *   @public
		 */

		public static $BRAINTREE_public_key = '';


		/**
		 *   Auth: private key
		 *   @var string
		 *   @public
		 */

		public static $BRAINTREE_private_key = '';



		/**
		 *   Init Braintree
		 *   @public
		 *   @static
		 *   @return void
		 */

		public static function init( $merchant_id=false, $public_key=false, $private_key=false, $environment=false )
		{
			global $LAB;

			// Init
			include 'base/contrib/Braintree/Braintree.php';

			// Set auth info
			Braintree_Configuration::environment($environment!==false?$environment:static::$BRAINTREE_environment);
			Braintree_Configuration::merchantId($merchant_id!==false?$merchant_id:static::$BRAINTREE_merchant_id);
			Braintree_Configuration::publicKey($public_key!==false?$public_key:static::$BRAINTREE_public_key);
			Braintree_Configuration::privateKey($private_key!==false?$private_key:static::$BRAINTREE_private_key);
		}


	}


?>