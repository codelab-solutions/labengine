<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   BankLink integration: base Solo class
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_BANKLINK_SOLO extends BASE_BANKLINK
	{


		/**
		 *   Language codes mapping
		 *   @var array
		 */
		public static $MAP_lang = array(
			'et' => '4',
			'en' => '3',
			'fi' => '1',
			'lv' => '6',
			'lt' => '7'
		);


		/**
		 *   Bank MAC key
		 *   @var string
		 */

		public $BANK_mac_key = null;


		/**
		 *   Bank MAC key version
		 *   @var string
		 */

		public $BANK_mac_key_version = null;


		/**
		 *   Bank destination URL
		 *   @var string
		 */

		public $BANK_destination_url = null;


		/**
		 *   Merchant rcv_id
		 *   @var string
		 */

		public $MERCHANT_rcv_id = null;


		/**
		 *   Merchant account
		 *   @var string
		 */

		public $MERCHANT_rcv_account = null;


		/**
		 *   Merchant return/notify URL
		 *   @var string
		 */

		public $MERCHANT_return_url = null;


		/*
		 *   Init
		 *   @return void
		 */

		public function init()
		{
			// This should be implemented in the subclass
		}


		/*
		 *   Set bank MAC key
		 *   @param string $bankMacKey Path to bank public key
		 *   @param string $bankMacKeyVersion Public key type/version
		 *   @throws Exception
		 *   @return void
		 */

		public function setBankMacKey( $bankMacKey, $bankMacKeyVersion )
		{
			$this->BANK_mac_key=$bankMacKey;
			switch ($bankMacKeyVersion)
			{
				case 'md5':
					$this->BANK_mac_key_version='0001';
					break;
				case 'sha1':
					$this->BANK_mac_key_version='0002';
					break;
				default:
					throw new Exception('Unknown value for $bankMacKeyVersion');
			}
		}


		/*
		 *   Set bank destination URL
		 *   @param string $bankDestinationURL Bank destination URL
		 *   @return void
		 */

		public function setBankDestinationURL( $bankDestinationURL )
		{
			$this->BANK_destination_url=$bankDestinationURL;
		}


		/*
		 *   Set merchant rcv id
		 *   @param string $merchantRcvID Merchant rcv_id
		 *   @return void
		 */

		public function setMerchantRcvID( $merchantRcvID )
		{
			$this->MERCHANT_rcv_id=$merchantRcvID;
		}


		/*
		 *   Set merchant receive account
		 *   @param string $merchantRcvAccount Merchant receive account
		 *   @return void
		 */

		public function setMerchantRcvAccount( $merchantRcvAccount )
		{
			$this->MERCHANT_rcv_account=$merchantRcvAccount;
		}


		/*
		 *   Set merchant snd id
		 *   @param string $merchantSndID Merchant snd_id
		 *   @return void
		 */

		public function setReturnURL( $returnURL )
		{
			$this->MERCHANT_return_url=$returnURL;
		}


		/**
	   *   Generates MAC string as needed according to the service number
		 *   @access public
	   *   @param  array $macFields MAC fields
		 *   @throws Exception
	   *   @return string MAC string
	   */

		public function generateMacString( $macFields )
		{
			// Check
			if (empty($this->BANK_mac_key)) throw new Exception('Bank MAC key not set.');
			if (empty($this->BANK_mac_key_version)) throw new Exception('Bank MAC key version not set.');

			// Variable order
			$variableOrder=array( 'SOLOPMT_VERSION', 'SOLOPMT_STAMP', 'SOLOPMT_RCV_ID', 'SOLOPMT_AMOUNT', 'SOLOPMT_REF', 'SOLOPMT_DATE', 'SOLOPMT_CUR' );

			// Init
			$data='';

			// Append data as needed
			foreach ($variableOrder as $key)
			{
				if (isset($macFields[$key]))
				{
					$data.=$macFields[$key].'&';
				}
				else
				{
					$data.='&';
				}
			}

			// Add MAC string
			$keyVersion=$this->BANK_mac_key_version;
			$mac=$this->BANK_mac_key;
			$data.=$mac.'&';

			// Encrypt data
			$data=($keyVersion=='0001')?md5($data):sha1($data);

			// Return data
			return strtoupper($data);
		}


		/**
		 *   Validate bank response
		 *   @param  array $requestData Fields received from the bank
		 *   @throws Exception
		 *   @return string Result (completed/cancelled/failed/not-implemented)
		 */

		public function validateResponse( $requestData, $throwException=true )
		{
			// Check
			if (empty($this->BANK_mac_key)) throw new Exception('Bank MAC key not set.');
			if (empty($this->BANK_mac_key_version)) throw new Exception('Bank MAC key version not set.');

			// Set some variables
			$variableOrder=array( 'SOLOPMT_RETURN_VERSION', 'SOLOPMT_RETURN_STAMP', 'SOLOPMT_RETURN_REF', 'SOLOPMT_RETURN_PAID' );

			// Data
			$data='';
			foreach ( $variableOrder as $var )
			{
				if (isset($requestData[$var]))
				{
					$data.=$requestData[$var].'&';
				}
				else
				{
					$data.='&';
				}
			}

			// Key/version
			$keyVersion=$this->BANK_mac_key_version;
			$mac=$this->BANK_mac_key;
			$data.=$mac.'&';
	
			// Encrypt data
			$data=($keyVersion=='0001')?md5($data):sha1($data);
	
			// Check the MACs
			if (isset($requestData['SOLOPMT_RETURN_MAC']) && strtoupper($data)==$requestData['SOLOPMT_RETURN_MAC'])
			{
				// Correct signature
				if (isset($requestData['SOLOPMT_RETURN_PAID']))
				{
					return 'completed';
				}
				else
				{
					return 'cancelled';
				}
			}
	
			if ($throwException) throw new Exception('Validation failed.');
			return 'failed';
		}


		/**
		 *   Get payment data from response
		 *   @param  array $requestData Fields received from the bank
		 *   @throws Exception
		 *   @return array Result
		 */

		public function getPaymentInfo( $requestData, $throwException=true )
		{
			// Init
			$paymentInfo=array();

				// Makse ID
				if (!empty($requestData['SOLOPMT_RETURN_STAMP'])) $paymentInfo['payment_id']=$requestData['SOLOPMT_RETURN_STAMP'];

				// Viitenumber
				if (!empty($requestData['SOLOPMT_RETURN_REF'])) $paymentInfo['payment_refno']=$requestData['SOLOPMT_RETURN_REF'];

			// Return
			return $paymentInfo;
		}


		/**
		 *   Generate payment form
		 *   @public
		 *   @param string $paymentID Order ID
		 *   @param float $paymentSum Order sum
		 *   @param string $paymentDescription Order description
		 *   @param int $paymentReferenceNo Order reference no
		 *   @return string Payment form
		 *   @throws Exception
		 */

		function generatePaymentForm( $paymentID, $paymentSum, $paymentDescription=null, $paymentReferenceNo=null )
		{
			global $LAB;

			// Check & init
			if (empty($paymentID)) throw new Exception('Order ID not set.');
			if (!floatval($paymentSum)) throw new Exception('Order sum not set.');
			if (empty($paymentDescription)) $paymentDescription=$paymentID;
			if (empty($paymentReferenceNo)) $paymentReferenceNo=static::generateRefNo($paymentID);
			if (empty($this->MERCHANT_rcv_id)) throw new Exception('Merchant ID not set.');
			if (empty($this->MERCHANT_rcv_account)) throw new Exception('Merchant account not set.');
			if (empty($this->MERCHANT_return_url)) throw new Exception('Return URL not set.');
			if (empty($this->BANK_destination_url)) throw new Exception('Bank destination URL not set.');

			// Current time
			$datetime=new DateTime('NOW');

			// Language
			if (!empty(static::$MAP_lang[$LAB->REQUEST->requestLang]))
			{
				$soloPmtLanguage=static::$MAP_lang[$LAB->REQUEST->requestLang];
			}
			else
			{
				$soloPmtLanguage=static::$MAP_lang['et'];
			}

			// Set MAC fields
			$macFields=array(
				'SOLOPMT_VERSION'     => '0003',
				'SOLOPMT_STAMP'       => $paymentID,
				'SOLOPMT_RCV_ID'      => $this->MERCHANT_rcv_id,
				'SOLOPMT_RCV_ACCOUNT' => $this->MERCHANT_rcv_account,
				'SOLOPMT_LANGUAGE'    => $soloPmtLanguage,
				'SOLOPMT_AMOUNT'      => round($paymentSum,2),
				'SOLOPMT_REF'         => $paymentReferenceNo,
				'SOLOPMT_DATE'        => 'EXPRESS',
				'SOLOPMT_MSG'         => $paymentDescription,
				'SOLOPMT_RETURN'      => $this->MERCHANT_return_url,
				'SOLOPMT_CANCEL'      => $this->MERCHANT_return_url,
				'SOLOPMT_REJECT'      => $this->MERCHANT_return_url,
				'SOLOPMT_CONFIRM'     => 'YES',
				'SOLOPMT_KEYVERS'     => $this->BANK_mac_key_version,
				'SOLOPMT_CUR'         => 'EUR'
			);

			// Generate MAC string from the private key
			$macFields['SOLOPMT_MAC']=$this->generateMacString($macFields);

			// Language
			if (!empty(static::$MAP_lang[$LAB->REQUEST->requestLang]))
			{
				$macFields['VK_LANG']=static::$MAP_lang[$LAB->REQUEST->requestLang];
			}
			else
			{
				$macFields['VK_LANG']='EST';
			}

			// Form begins
			$form='<form action="'.htmlspecialchars($this->BANK_destination_url).'" method="post" id="banklink_'.htmlspecialchars(static::$BANK_id).'_payment_form">';

				// Add fields
				foreach($macFields as $field => $value )
				{
					$form.='<input type="hidden" name="'.htmlspecialchars($field).'" value="'.htmlspecialchars($value) .'" />';
				}

			// Form ends
			$form.='</form>';
			return $form;
		}


	}


?>