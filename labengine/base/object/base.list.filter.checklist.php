<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   The check-list filter class
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_LIST_FILTER_CHECKLIST extends BASE_LIST_FILTER
	{

		var $filterGrouping = FALSE;
		var $filterZeroHack = FALSE;

		function __construct()
		{
			$this->filterExact=TRUE;
		}

		public function setGrouping ( $grouping )
		{
			$this->filterGrouping=$grouping;
		}

		public function setZeroHack ( $zeroHack )
		{
			$this->filterZeroHack=$zeroHack;
		}

		public function getCriteria ( $value, &$paramObject )
		{
			if (!mb_strlen($value)) return;
			if ($this->filterZeroHack) $value=preg_replace("/^val_/","",$value);
			$valArray=str_array($value);
			$cValue=" in('".join("','",$valArray)."')";
			$this->applyCriteria($cValue, $paramObject);
		}

		public function displayField( $value, $row=NULL )
		{
			$options=$this->getOptions();

			// TODO: grouping

			// Compose value
			$selectedItems=array();
			foreach (str_array($value) as $v) $selectedItems[$v]=$options[$v];

			// Value
			$c='<input type="hidden" id="filter_'.str_replace('.','_',$this->tag).'" name="filter_'.str_replace('.','_',$this->tag).'" value="'.join(',',array_keys($selectedItems)).'" />';

			// Display
			$c.='<input type="text"';
				$c.=' class="form-control checklistfilter'.(!empty($this->error)?' error':'').'"';
				$c.=' autocomplete="off"';
				$c.=' readonly="readonly"';
				$c.=' id="filter_'.$this->tag.'_disp"';
				$c.=' name="filter_'.$this->tag.'_disp"';
				$c.=' value="'.(sizeof($selectedItems)?join(', ',$selectedItems):'--- [BASE.COMMON.All] ---').'"';
				$c.=' onclick="LIST.FILTER.CHECKLIST.openFilter(\''.str_replace('.','_',$this->tag).'\',\''.htmlspecialchars($this->getTitle()).'\','.str_replace('"',"'",json_encode($options,JSON_FORCE_OBJECT | JSON_HEX_QUOT)).')"';
			$c.='>';

			return $c;
		}

	}


?>