<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   BankLink integration: Danske Bank
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_BANKLINK_NORDEA extends BASE_BANKLINK_SOLO
	{


		/**
		 *   Bank ID
		 *   @var string
		 *   @static
		 */

		public static $BANK_id = 'nordea';


		/**
		 *   Bank destination URL
		 *   @var string
		 */

		public $BANK_destination_url = 'https://netbank.nordea.com/pnbepay/epayn.jsp';


	}


?>