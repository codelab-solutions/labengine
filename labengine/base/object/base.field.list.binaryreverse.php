<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   List field class: reverse binary
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_FIELD_LIST_BINARYREVERSE extends BASE_FIELD
	{


		/**
		 *   List value
		 *   @access public
		 *   @return mixed value
		 */

		public function listValue( $value, &$row )
		{
			return (!empty($value)?'':'<span class="icon-ok"></span>');
		}


	}


?>