
import org.digidoc4j.*;

import java.security.cert.X509Certificate;
import java.io.*;

import org.digidoc4j.impl.asic.asice.bdoc.BDocContainerBuilder;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.FileUtils;
import eu.europa.esig.dss.spi.DSSUtils;
import eu.europa.esig.dss.enumerations.DigestAlgorithm;

import java.util.Base64;
import javax.xml.bind.DatatypeConverter;

import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;

public class GenerateHash {

    static String SigningCertInHex = "";
    static String ContainerFile = "";
    static String LogFile = "";
    static String DataToSignFile = "";
    static String DigestHashFile = "";
    static String Environment = "";

    private FileSigner signer;

    public static String getDataToSignFile() {
        return DataToSignFile;
    }

    public FileSigner getSigner() {
        return signer;
    }

    public void setSigner(FileSigner signer) {
        this.signer = signer;
    }

    public static void main(String[] args) throws FileNotFoundException, IOException {

        // Args 1 - SigningCertInHex
        // Args 2 - ContainerFile
        // Args 3 - LogFile
        // Args 4 - DataToSignFile
        // Args 5 - DigestHashFile
        // Args 6 - Environment

        for (int i = 0; i < args.length; i++) {
            switch (i) {
                case 0:
                    setSigningCertInHex(args[i]);
                    break;
                case 1:
                    setContainerFile(args[i]);
                    break;
                case 2:
                    setLogFile(args[i]);
                    break;
                case 3:
                    setDataToSignFile(args[i]);
                    break;
                case 4:
                    setDigestHashFile(args[i]);
                    break;
                case 5:
                    setEnvironment(args[i]);
                    break;
            }
        }


        try {
            PrintStream out = new PrintStream(new FileOutputStream(getLogFile()));
            System.setOut(out);
            System.setErr(out);
        } catch (FileNotFoundException ex) {
            System.out.println("[error] " + ex.getMessage());
        }

        Container container;

        if (getEnvironment().equals("TEST")) {
            Configuration configuration = new Configuration(Configuration.Mode.TEST);
            configuration.setTslLocation("https://open-eid.github.io/test-TL/tl-mp-test-EE.xml");

            container = ContainerBuilder.
                    aContainer("BDOC").
                    withConfiguration(configuration).
                    fromExistingFile(getContainerFile()).
                    build();
        } else {

            container = ContainerBuilder.
                    aContainer("BDOC").
                    fromExistingFile(getContainerFile()).
                    build();
        }

        Digest digest = new Digest();
        FileSigner signer = new FileSigner();


        try {
            DataToSign dataToSign = signer.getDataToSign(container, getSigningCertInHex());

            FileOutputStream fileOut = new FileOutputStream(getDataToSignFile());
            ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);
            objectOut.writeObject(dataToSign);
            objectOut.close();

            String dataToSignInHex =
                    DatatypeConverter.printHexBinary(DSSUtils.digest(DigestAlgorithm.SHA256, dataToSign.getDataToSign()));
            digest.setHex(dataToSignInHex);
            digest.setResult("ok");
            System.out.println("[dataToSignInHex] " + dataToSignInHex);

        } catch (Exception e) {
            System.out.println("[error] " + e.getMessage());
            digest.setResult("error_generating_hash");
        }

        System.out.println(digest.getHex());

        try (FileOutputStream fos = new FileOutputStream(getDigestHashFile())) {
            String text = digest.getHex();
            byte[] mybytes = text.getBytes();
            fos.write(mybytes);
        } catch (Exception e) {
            System.out.println("[error] " + e.getMessage());
        }

    }


    public static void setContainerFile(String containerFile) {
        ContainerFile = containerFile;
    }

    public static String getContainerFile() {
        return ContainerFile;
    }

    public static void setLogFile(String logfile) {
        LogFile = logfile;
    }

    public static String getLogFile() {
        return LogFile;
    }

    public static void setSigningCertInHex(String signingCertInHex) {
        SigningCertInHex = signingCertInHex;
    }

    public static String getSigningCertInHex() {
        return SigningCertInHex;
    }

    public static String getDigestHashFile() {
        return DigestHashFile;
    }

    public static void setDigestHashFile(String digestHashFile) {
        DigestHashFile = digestHashFile;
    }

    public static void setDataToSignFile(String dataToSignFile) {
        DataToSignFile = dataToSignFile;
    }

    public static void setEnvironment(String environment) {
        Environment = environment;
    }

    public static String getEnvironment() {
        return Environment;
    }
}
