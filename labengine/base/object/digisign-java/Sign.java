
import org.digidoc4j.*;

import java.security.cert.X509Certificate;
import java.io.*;

import org.apache.commons.lang3.StringUtils;

import org.digidoc4j.impl.asic.asice.bdoc.BDocContainerBuilder;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.FileUtils;
import eu.europa.esig.dss.spi.DSSUtils;
import eu.europa.esig.dss.enumerations.DigestAlgorithm;

import java.util.Base64;
import javax.xml.bind.DatatypeConverter;

import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class Sign {

    static String SigningCertInHex = "";
    static String SignatureInHex = "";
    static String ContainerFile = "";
    static String LogFile = "";
    static String DataToSignFile = "";
    static String DigestHashFile = "";
    static String Environment = "";

    private FileSigner signer;


    public static void main(String[] args) throws FileNotFoundException, IOException {

        // Args 1 - SigningCertInHex
        // Args 2 - SignatureInHex
        // Args 3 - ContainerFile
        // Args 4 - LogFile
        // Args 5 - DataToSignFile
        // Args 6 - DigestHashFile
        // Args 7 - Environment

        for (int i = 0; i < args.length; i++) {
            switch (i) {
                case 0:
                    setSigningCertInHex(args[i]);
                    break;
                case 1:
                    setSignatureInHex(args[i]);
                    break;
                case 2:
                    setContainerFile(args[i]);
                    break;
                case 3:
                    setLogFile(args[i]);
                    break;
                case 4:
                    setDataToSignFile(args[i]);
                    break;
                case 5:
                    setDigestHashFile(args[i]);
                    break;
                case 6:
                    setEnvironment(args[i]);
                    break;
            }
        }


        try {
            PrintStream out = new PrintStream(new FileOutputStream(getLogFile()));
            System.setOut(out);
            System.setErr(out);
        } catch (FileNotFoundException ex) {
            System.out.println("[error] " + ex.getMessage());
        }

        DataToSign dataToSign = null;

        // Deserialization
        try {

            FileInputStream file = new FileInputStream(getDataToSignFile());
            ObjectInputStream in = new ObjectInputStream(file);

            dataToSign = (DataToSign) in.readObject();
            in.close();
            file.close();
        } catch (IOException ex) {
            System.out.println("[error] " + ex.getMessage());
            System.out.println("IOException is caught");
        } catch (ClassNotFoundException ex) {
            System.out.println("[error] " + ex.getMessage());
            System.out.println("ClassNotFoundException is caught");
        }

        Container container;

        if (getEnvironment().equals("TEST")) {
            Configuration configuration = new Configuration(Configuration.Mode.TEST);
            configuration.setTslLocation("https://open-eid.github.io/test-TL/tl-mp-test-EE.xml");

            container = ContainerBuilder.
                    aContainer("BDOC").
                    withConfiguration(configuration).
                    fromExistingFile(getContainerFile()).
                    build();
        } else {

            container = ContainerBuilder.
                    aContainer("BDOC").
                    fromExistingFile(getContainerFile()).
                    build();
        }

        Digest digest = new Digest();

        FileSigner signer = new FileSigner();

        System.out.println("[Starting signing process] ");
        try {
            signer.signContainer(container, dataToSign, getSignatureInHex());
            container.saveAsFile(getContainerFile());
            System.out.println("[containerFile] " + getContainerFile());
        } catch (Exception e) {
            System.out.println("[error] " + e.getMessage());
            digest.setResult("error_generating_hash");
        }


    }

    public static void setContainerFile(String containerFile) {
        ContainerFile = containerFile;
    }

    public static String getContainerFile() {
        return ContainerFile;
    }

    public static void setLogFile(String logfile) {
        LogFile = logfile;
    }

    public static String getLogFile() {
        return LogFile;
    }

    public static void setSignatureInHex(String signatureInHex) {
        SignatureInHex = signatureInHex;
    }

    public static String getSignatureInHex() {
        return SignatureInHex;
    }

    public static String getDigestHashFile() {
        return DigestHashFile;
    }

    public static void setDigestHashFile(String digestHashFile) {
        DigestHashFile = digestHashFile;
    }

    public static void setSigningCertInHex(String signingCertInHex) {
        SigningCertInHex = signingCertInHex;
    }

    public static String getSigningCertInHex() {
        return SigningCertInHex;
    }

    public static void setDataToSignFile(String dataToSignFile) {
        DataToSignFile = dataToSignFile;
    }

    public static String getDataToSignFile() {
        return DataToSignFile;
    }

    public static void setEnvironment(String environment) {
        Environment = environment;
    }

    public static String getEnvironment() {
        return Environment;
    }
}
