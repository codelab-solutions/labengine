<?


	/**
	 *
	 *   LabEngine™ 7
	 *   Standard actions: login
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_ACTION_LOGIN extends BASE_DISPLAYOBJECT
	{


		/**
		 *   Parameters
		 *   @var array
		 *   @access public
		 */
		public $param = array();


		/**
		 *   Enable language selection
		 *   @access public
		 *   @param bool $languageSelect Language select
		 *   @return void
		 */

		public function enableLanguageSelect ( $languageSelect )
		{
			$this->param['langsel']=$languageSelect;
		}


		/**
		 *   Set language selection source list
		 *   @access public
		 *   @param array $languageSourceList Language source list
		 *   @return void
		 */

		public function setLanguageSourceList ( $languageSourceList )
		{
			$this->param['langsel_sourcelist']=$languageSourceList;
		}


		/**
		 *   Set reload
		 *   @access public
		 *   @param bool $reload Reload on success
		 *   @return void
		 */

		public function setReload ( $reload )
		{
			$this->param['reload']=$reload;
		}


		/**
		 *   Set redirect URL
		 *   @access public
		 *   @param string $redirectURL Redirect URL
		 *   @return void
		 */

		public function setRedirectURL ( $redirectURL )
		{
			$this->param['url_redirect']=$redirectURL;
		}


		/**
		 *   Set template
		 *   @access public
		 *   @param string $template Template
		 *   @return void
		 */

		public function setTemplate ( $template )
		{
			global $LAB;
			$this->param['template']=$template;
		}


		/**
		 *   Set login banner
		 *   @access public
		 *   @param string $banner Login banner
		 *   @return void
		 */

		public function setBanner ( $banner )
		{
			global $LAB;
			$this->param['banner']=$banner;
		}


		/**
		 *   Set e-mail label
		 *   @access public
		 *   @param string $labelEmail E-mail label
		 *   @return void
		 */

		public function setEmailLabel ( $labelEmail )
		{
			global $LAB;
			$this->param['label_email']=$labelEmail;
		}


		/**
		 *   Set password label
		 *   @access public
		 *   @param string $labelPassword Password label
		 *   @return void
		 */

		public function setPasswordLabel ( $labelPassword )
		{
			global $LAB;
			$this->param['label_password']=$labelPassword;
		}


		/**
		 *   Set language selection label
		 *   @access public
		 *   @param string $labelLangSel Password label
		 *   @return void
		 */

		public function setLangSelLabel ( $labelLangSel )
		{
			global $LAB;
			$this->param['label_langsel']=$labelLangSel;
		}


		/**
		 *   Set submit button label
		 *   @access public
		 *   @param string $labelSubmit Submit button label
		 *   @return void
		 */

		public function setSubmitLabel ( $labelSubmit )
		{
			global $LAB;
			$this->param['label_submit']=$labelSubmit;
		}


		/**
		 *   Init login action
		 *   @access public
		 *   @return string|array
		 *   @throws Exception
		 */

		public function initLogin()
		{
			// This can be implemented in the subclass.
		}


		/**
		 *   Pre-validate login submit
		 *   @access public
		 *   @return void
		 *   @throws Exception
		 */

		public function loginPreValidate()
		{
			// This can be implemented in the subclass.
		}


		/**
		 *   Do login
		 *   @access public
		 *   @return void
		 *   @throws Exception
		 */

		public function doLogin()
		{
			global $LAB;
			$LAB->USER->doLogin($_POST['login_email'],$_POST['login_password']);
		}


		/**
		 *   Login trigger / after-validate
		 *   @access public
		 *   @return void
		 *   @throws Exception
		 */

		public function loginTrigger()
		{
			// This can be implemented in the subclass.
		}


		/**
		 *   Select language
		 *   @access public
		 *   @return void
		 *   @throws Exception
		 */

		public function selectLanguage()
		{
			global $LAB;
			if (empty($this->param['langsel'])) return;
			if (empty($_POST['login_lang'])) return;
			if (!empty($this->param['langsel_sourcelist']))
			{
				if (empty($this->param['langsel_sourcelist'][$_POST['login_lang']])) throw new Exception('[BASE.USER.LOGIN.Error.UnknownLanguage]');
			}
			else
			{
				if (!$LAB->LOCALE->localeExists($_POST['login_lang'])) throw new Exception('[BASE.USER.LOGIN.Error.UnknownLanguage]');
			}
			$LAB->SESSION->set('LANG',$_POST['login_lang']);
		}


		/**
		 *  Display login form
		 *  @access public
		 *  @return string
		 */

		public function displayLoginForm()
		{
			global $LAB;

			$c='<div class="login-wrapper">';
			if (!empty($this->param['banner'])) $c.='<div class="login-banner">'.$this->param['banner'].'</div>';
			$c.='<div id="login_message" class="login-message" style="display: none"></div>';
			$c.='<div class="login-form-wrapper">';
			$c.='<form role="form" id="login_form" class="login-form form-horizontal" method="post" action="'.$LAB->REQUEST->requestURI.'" onsubmit="return false">';
			$c.='<fieldset>';

				$c.='
					<div class="form-group has-feedback">
						<label for="login_email" class="col-lg-3 control-label">'.oneof($this->param['label_email'],'[BASE.COMMON.Email]').':</label>
						<div class="col-lg-9">
							<input type="text" class="form-control" id="login_email" name="login_email" onkeydown="LOGIN.usernameKeypress(event)">
							<span id="feedback_login_email" class="form-control-feedback"></span>
						</div>
					</div>
				';
				$c.='
					<div class="form-group has-feedback">
						<label for="login_password" class="col-lg-3 control-label">'.oneof($this->param['label_password'],'[BASE.COMMON.Password]').':</label>
						<div class="col-lg-9">
							<input type="password" class="form-control" id="login_password" name="login_password" onkeydown="LOGIN.passwordKeypress(event)">
							<span id="feedback_login_password" class="form-control-feedback"></span>
						</div>
					</div>
				';
				if (!empty($this->param['langsel']))
				{
					if (!empty($this->param['langsel_sourcelist']))
					{
						$languageList=$this->param['langsel_sourcelist'];
					}
					else
					{
						$languageList=array();
						foreach ($LAB->LOCALE->localeLanguageSet as $lang)
						{
							$languageList[$lang]=mb_strtolower(BASE_LOCALE::getName($lang));
						}
					}
					$c.='
						<div class="form-group has-feedback">
							<label for="login_password" class="col-lg-3 control-label">'.oneof($this->param['label_langsel'],'[BASE.COMMON.Language]').':</label>
							<div class="col-lg-9">
								<select class="form-control" id="login_lang" name="login_lang">'.BASE::arrayToSelectOptions($languageList,$LAB->REQUEST->requestLang).'</select>
								<span id="feedback_login_password" class="form-control-feedback"></span>
							</div>
						</div>
					';
				}
				$c.='
					<div class="form-group">
						<div class="col-lg-9 col-lg-offset-3">
							<button type="button" id="login_submit" class="btn btn-primary" data-title="'.oneof($this->param['label_password'],'[BASE.COMMON.Login]').'" data-title-progress="<span class=spinner></span>" onclick="LOGIN.doLogin()">'.oneof($this->param['label_password'],'[BASE.COMMON.Login]').'</button>
						</div>
					</div>
				';

			$c.='</fieldset>';
			$c.='</form>';
			$c.='</div>';
			$c.='</div>';
			$c.='<script language="JavaScript"> $(function(){ $("#login_email").focus(); }); </script>';
			return $c;
		}


		/**
		 *  Handle action
		 *  @access public
		 *  @return string|array
		 */

		public function display()
		{
			global $LAB;

			// AJAX?
			if (BASE::isXHR() || $LAB->REQUEST->requestMethod=='POST')
			{
				// Init
				$LAB->RESPONSE->setType('json');
				$this->initLogin();
				BASE::sanitizeInput();

				try
				{
					// Do login
					if (!empty($_POST['login']) || !empty($_POST['ajaxsubmit']) || (mb_strlen($_POST['login_email']) && mb_strlen($_POST['login_password'])))
					{
						// Pre-validate
						$this->loginPreValidate();
						
						// Do login
						$this->doLogin();
						
						// Login trigger/validate
						$this->loginTrigger();

						// Select language
						$this->selectLanguage();
						
						// Response
						$response=array();
						$response['status']='1';
						if (!empty($this->param['reload']))
						{
							$response['reload']='1';
						}
						elseif (mb_strlen($LAB->SESSION->get('login.redirect')))
						{
							$response['redirect']=$LAB->SESSION->get('login.redirect');
							$LAB->SESSION->set('login.redirect','');
						}
						elseif (!empty($this->param['url_redirect']))
						{
							$response['redirect']=$this->param['url_redirect'];
						}
						return $response;
					}

					// Return login form
					else
					{
						$response=array();
						$response['status']='1';
						$response['content']=$this->displayLoginForm();
						return $response;
					}
				}
				catch (Exception $e)
				{
					// Log out user if we logged it in
					if ($LAB->USER->isLoggedIn())
					{
						$LAB->USER->doLogout();
					}
					
					// Return error
					$response=array();
					$response['status']='2';
					$response['error']=$e->getMessage();
					return $response;
				}
			}

			// Get
			else
			{
				// Init
				$this->initLogin();

				// Set template
				$LAB->RESPONSE->setTemplate(oneof($this->param['template'],'login'));

				// Return login form
				return $this->displayLoginForm();
			}
		}


	}


?>