<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   DOMPDF wrapper class
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_DOMPDF
	{

		/**
		 *   DOMPDF instance
		 *   @var DOMPDF
		 *   @public
		 */

		public $DOMPDF = NULL;


		/**
		 *   Init DOMPDF
		 *   @public
		 *   @static
		 *   @return BASE_DOMPDF
		 */

		public static function init()
		{
			global $LAB;

			// Init
			require 'base/contrib/DOMPDF/autoload.inc.php';

			// Ease limits
			ini_set("max_execution_time","1200");
			ini_set('memory_limit', '4096M');

			// Create class instance
			$retClass=new BASE_DOMPDF();

			// Create and attach PHPExcel main class instance
			$retClass->DOMPDF=new \Dompdf\Dompdf();

			// Return
			return $retClass;
		}


		/**
		 *   Render PDF
		 *   @public
		 *   @return string
		 */

		public function generatePDF( $html, $basePath=FALSE, $data=FALSE, $paper='A4', $orientation='portrait' )
		{
			global $_dompdf_warnings;

            // Set basepath
            if (!empty($basePath))
            {
                $this->DOMPDF->set_base_path($basePath);
                $this->DOMPDF->set_option('chroot',$basePath);
            }

            // Load HTML
			if (substr($html,0,1)=='/' || preg_match("/\.html$/",$html))
			{
				$this->DOMPDF->load_html_file($html);
			}
			else
			{
				$this->DOMPDF->load_html($html);
			}

			// TODO: data

			// Set paper size
			$this->DOMPDF->set_paper($paper,$orientation);

			// Render
			$this->DOMPDF->render();

			// Errors / warnings?
			$msg=array();
			foreach ($_dompdf_warnings as $warning) $msg[]=$warning;
			if (!empty($this->DOMPDF->get_canvas()->get_cpdf()->messages)) $msg[]=$this->DOMPDF->get_canvas()->get_cpdf()->messages;
			//if (sizeof($msg)) { echo ('<pre>Errors generating PDF: '.join(' / ',$msg)); exit; }

			// Return
			return $this->DOMPDF->output(array("compress"=>0));
		}


	}


?>