<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   BankLink integration: base iPizza class
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_BANKLINK_IPIZZA extends BASE_BANKLINK
	{


		/**
		 *   Variable order for iPizza requests
		 *   @var array
		 */

		public static $VAR_order = array(
			1011 => array( 'VK_SERVICE', 'VK_VERSION', 'VK_SND_ID', 'VK_STAMP', 'VK_AMOUNT', 'VK_CURR', 'VK_ACC', 'VK_NAME', 'VK_REF', 'VK_MSG', 'VK_RETURN', 'VK_CANCEL', 'VK_DATETIME' ),
			1012 => array( 'VK_SERVICE', 'VK_VERSION', 'VK_SND_ID', 'VK_STAMP', 'VK_AMOUNT', 'VK_CURR', 'VK_REF', 'VK_MSG', 'VK_RETURN', 'VK_CANCEL', 'VK_DATETIME' ),
			1111 => array( 'VK_SERVICE', 'VK_VERSION', 'VK_SND_ID', 'VK_REC_ID', 'VK_STAMP', 'VK_T_NO', 'VK_AMOUNT', 'VK_CURR', 'VK_REC_ACC', 'VK_REC_NAME', 'VK_SND_ACC', 'VK_SND_NAME', 'VK_REF', 'VK_MSG', 'VK_T_DATETIME' ),
			1911 => array( 'VK_SERVICE', 'VK_VERSION', 'VK_SND_ID', 'VK_REC_ID', 'VK_STAMP', 'VK_REF', 'VK_MSG' )
		);


		/**
		 *   Language codes mapping
		 *   @var array
		 */
		public static $MAP_lang = array(
			'et' => 'EST',
			'en' => 'ENG',
			'ru' => 'RUS'
		);


		/**
		 *   Bank public key
		 *   @var string
		 */

		public $BANK_public_key = null;


		/**
		 *   Bank destination URL
		 *   @var string
		 */

		public $BANK_destination_url = null;


		/**
		 *   Merchant snd_id
		 *   @var string
		 */

		public $MERCHANT_snd_id = null;


		/**
		 *   Merchant private key
		 *   @var string
		 */

		public $MERCHANT_private_key = null;


		/**
		 *   Merchant private key passphrase
		 *   @var string
		 */

		public $MERCHANT_private_key_pass = '';


		/**
		 *   Merchant return/notify URL
		 *   @var string
		 */

		public $MERCHANT_return_url = null;


		/*
		 *   Init
		 *   @return void
		 */

		public function init()
		{
			// This should be implemented in the subclass
		}


		/*
		 *   Set bank public key
		 *   @param string $bankPublicKey Path to bank public key
		 *   @return void
		 */

		public function setBankPublicKey( $bankPublicKey )
		{
			$this->BANK_public_key=$bankPublicKey;
		}


		/*
		 *   Set bank destination URL
		 *   @param string $bankDestinationURL Bank destination URL
		 *   @return void
		 */

		public function setBankDestinationURL( $bankDestinationURL )
		{
			$this->BANK_destination_url=$bankDestinationURL;
		}


		/*
		 *   Set merchant snd id
		 *   @param string $merchantSndID Merchant snd_id
		 *   @return void
		 */

		public function setMerchantSndID( $merchantSndID )
		{
			$this->MERCHANT_snd_id=$merchantSndID;
		}


		/*
		 *   Set merchant private key and passphrase
		 *   @param string $merchantPrivateKey Path to private key
		 *   @param string $merchantPrivateKeyPass Private key passphrase
		 *   @return void
		 */

		public function setMerchantPrivateKey( $merchantPrivateKey, $merchantPrivateKeyPass='' )
		{
			$this->MERCHANT_private_key=$merchantPrivateKey;
			$this->MERCHANT_private_key_pass=$merchantPrivateKeyPass;
		}


		/*
		 *   Set merchant snd id
		 *   @param string $merchantSndID Merchant snd_id
		 *   @return void
		 */

		public function setReturnURL( $returnURL )
		{
			$this->MERCHANT_return_url=$returnURL;
		}


		/**
	   *   Generates MAC string as needed according to the service number
		 *   @access public
	   *   @param  array $macFields MAC fields
	   *   @return string MAC string
	   */

		public function generateMacString( $macFields )
		{
			// Get service number
			$serviceNumber=$macFields['VK_SERVICE'];

			// Init
			$data='';

			// Append data as needed
			foreach (static::$VAR_order[$serviceNumber] as $key)
			{
				$value=$macFields[$key];
				$data.=str_pad(mb_strlen($value),3,'0',STR_PAD_LEFT).$value;
			}

			// Return data
			return $data;
		}


		/**
		 *   Validate bank response
		 *   @param  array $requestData Fields received from the bank
		 *   @throws Exception
		 *   @return string Result (completed/cancelled/failed/not-implemented)
		 */

		public function validateResponse( $requestData, $throwException=true )
		{
			// Check
			if (!mb_strlen($this->BANK_public_key)) throw new Exception('Bank public key not set.');
			$BANK_public_key=stream_resolve_include_path($this->BANK_public_key);
			if (empty($BANK_public_key)) throw new Exception('Bank public key not found.');

			// Init
			$result=array('payment' => 'failed');
			$vk_service=$requestData['VK_SERVICE'];
			$macFields=array();

			// Generate MAC fields
			foreach ($requestData as $k => $v)
			{
				if (substr($k,0,3)=='VK_')
				{
					$macFields[$k] = $v;
				}
			}

			// Get public key
			$key=openssl_pkey_get_public(file_get_contents($BANK_public_key));
			$macString=$this->generateMacString($macFields);
			$verify_mac=openssl_verify($macString,base64_decode($macFields['VK_MAC']),$key,OPENSSL_ALGO_SHA1);

			// Check result
			if ($verify_mac===1)
			{
				// Correct signature
				if ( $vk_service == '1111' )
				{
					return 'completed';
				}
				else
				{
					return 'cancelled';
				}
			}

			if ($throwException) throw new Exception('Validation failed.');
			return 'failed';
		}


		/**
		 *   Get payment data from response
		 *   @param  array $requestData Fields received from the bank
		 *   @throws Exception
		 *   @return array Result
		 */

		public function getPaymentInfo( $requestData, $throwException=true )
		{
			// Init
			$paymentInfo=array();

				// Makse ID
				if (!empty($requestData['VK_STAMP'])) $paymentInfo['payment_id']=$requestData['VK_STAMP'];

				// Viitenumber
				if (!empty($requestData['VK_REF'])) $paymentInfo['payment_refno']=$requestData['VK_REF'];

				// Kirjeldus
				if (!empty($requestData['VK_MSG'])) $paymentInfo['payment_description']=$requestData['VK_MSG'];

				// Summa
				if (!empty($requestData['VK_AMOUNT'])) $paymentInfo['payment_sum']=round($requestData['VK_AMOUNT'],2);
				if (!empty($requestData['VK_CURR'])) $paymentInfo['payment_currency']=$requestData['VK_CURR'];

				// Saaja info
				if (!empty($requestData['VK_REC_ACC'])) $paymentInfo['payment_receiver_account']=$requestData['VK_REC_ACC'];
				if (!empty($requestData['VK_REC_NAME'])) $paymentInfo['payment_receiver_name']=$requestData['VK_REC_NAME'];

				// Maksja info
				if (!empty($requestData['VK_SND_ACC'])) $paymentInfo['payment_payer_account']=$requestData['VK_SND_ACC'];
				if (!empty($requestData['VK_SND_NAME'])) $paymentInfo['payment_payer_name']=$requestData['VK_SND_NAME'];

				// Makse aeg
				if (!empty($requestData['VK_T_DATETIME'])) $paymentInfo['payment_tstamp']=date('Y-m-d H:i:s',strtotime($requestData['VK_T_DATETIME']));

			// Return
			return $paymentInfo;
		}


		/**
		 *   Generate payment form
		 *   @public
		 *   @param string $paymentID Order ID
		 *   @param float $paymentSum Order sum
		 *   @param string $paymentDescription Order description
		 *   @param int $paymentReferenceNo Order reference no
		 *   @return string Payment form
		 *   @throws Exception
		 */

		function generatePaymentForm( $paymentID, $paymentSum, $paymentDescription=null, $paymentReferenceNo=null )
		{
			global $LAB;

			// Check & init
			if (empty($paymentID)) throw new Exception('Order ID not set.');
			if (!floatval($paymentSum)) throw new Exception('Order sum not set.');
			if (empty($paymentDescription)) $paymentDescription=$paymentID;
			if (empty($paymentReferenceNo)) $paymentReferenceNo=static::generateRefNo($paymentID);
			if (empty($this->MERCHANT_snd_id)) throw new Exception('Merchant ID not set.');
			if (empty($this->MERCHANT_private_key)) throw new Exception('Private key not set.');
			if (empty($this->MERCHANT_return_url)) throw new Exception('Return URL not set.');
			if (empty($this->BANK_destination_url)) throw new Exception('Bank destination URL not set.');
			$MERCHANT_private_key=stream_resolve_include_path($this->MERCHANT_private_key);
			if (empty($MERCHANT_private_key)) throw new Exception('Private key not found.');

			// Current time
			$datetime=new DateTime('NOW');

			// Set MAC fields
			$macFields  = array(
				'VK_SERVICE'  => '1012',
				'VK_VERSION'  => '008',
				'VK_SND_ID'   => $this->MERCHANT_snd_id,
				'VK_STAMP'    => $paymentID,
				'VK_AMOUNT'   => round($paymentSum,2),
				'VK_CURR'     => 'EUR',
				'VK_REF'      => $paymentReferenceNo,
				'VK_MSG'      => $paymentDescription,
				'VK_RETURN'   => $this->MERCHANT_return_url,
				'VK_CANCEL'   => $this->MERCHANT_return_url,
				'VK_DATETIME' => $datetime->format( DateTime::ISO8601 )
			);

			// Generate MAC string from the private key
			$key=openssl_pkey_get_private(file_get_contents($MERCHANT_private_key),$this->MERCHANT_private_key_pass);
			$signature='';
			$macString=$this->generateMacString($macFields);

			// Try to sign the MAC string
			if (!openssl_sign($macString,$signature,$key,OPENSSL_ALGO_SHA1)) throw new Exception('Unable to generate signature.');

			// Encode signature
			$macFields['VK_MAC']=base64_encode($signature);

			// Language
			if (!empty(static::$MAP_lang[$LAB->REQUEST->requestLang]))
			{
				$macFields['VK_LANG']=static::$MAP_lang[$LAB->REQUEST->requestLang];
			}
			else
			{
				$macFields['VK_LANG']='EST';
			}

			// Form begins
			$form='<form action="'.htmlspecialchars($this->BANK_destination_url).'" method="post" id="banklink_'.htmlspecialchars(static::$BANK_id).'_payment_form">';

				// Add fields
				foreach($macFields as $field => $value )
				{
					$form.='<input type="hidden" name="'.htmlspecialchars($field).'" value="'.htmlspecialchars($value) .'" />';
				}

				// Set encoding, just in case
				$form.='<input type="hidden" name="VK_ENCODING" value="UTF-8" />';

			// Form ends
			$form.='</form>';
			return $form;
		}


	}


?>