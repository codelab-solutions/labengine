<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   Standard Bootstrap info block
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_INFOBLOCK
	{


		/**
		 *   Display responsible info block
		 *   @access public
		 *   @static
		 *   @param string $title Title
		 *   @param string|BASE_INFOTABLE $content Content
		 *   @param array $actions Actions
		 *   @param string $blockClass Block class
		 *   @param array $blockAttr Block attributes
		 *   @throws Exception
		 *   @return string
		 */

		public static function displayBlock( $title, $content, $actions=null, $blockClass=null, $blockAttr=null )
		{
			$c='<div';
				$c.=' class="panel panel-default'.(!empty($blockClass)?' '.(is_array($blockClass)?join(' ',$blockClass):$blockClass):'').'"';
				if (!empty($blockAttr))
				{
					foreach ($blockAttr as $k => $v) $c.=' '.$k.='="'.$v.'"';
				}
			$c.='>';

				// Header
				if (!empty($title) || (is_array($actions) && sizeof($actions)))
				{
					$c.='<div class="panel-heading">';
						if (!empty($title))
						{
							$c.='<h3 class="panel-title">'.$title.'</h3>';
						}
						if (is_array($actions) && sizeof($actions))
						{
							$c.='<div class="panel-actions">';
								$c.='<div class="action">'.join('</div><div class="action">',$actions).'</div>';
							$c.='</div>';
						}
					$c.='</div>';
				}

				// Content
				$c.='<div class="panel-body">';

					if (is_object($content) && $content instanceof BASE_INFOTABLE)
					{
						$c.=$content->displayTable();
					}
					else
					{
						$c.=$content;
					}

				$c.='</div>';

			$c.='</div>';
			return $c;
		}


	}


?>