<?php


	/**
	 *
	 *   FlaskPHP-Identity-EstEID
	 *   ------------------------
	 *   Digital signing provider for Mobile ID
	 *
	 *   @author   Codelab Solutions OÜ <codelab@codelab.ee>
	 *   @license  https://www.flaskphp.com/LICENSE MIT
	 *
	 */


	class BASE_ESTEID_SIGN
	{


		/**
		 *   Dev mode?
		 *   @var bool
		 *   @access public
		 */

		public $devMode = false;


		/**
		 *   Service language
		 *   @var string
		 *   @access public
		 */

		public $serviceLanguage = 'EST';


		/**
		 *
		 *   Constructor
		 *   -----------
		 *   @access public
		 *   @param bool $devMode
		 *   @throws Exception
		 *   @return BASE_ESTEID_SIGN
		 *
		 */

		public function __construct( $devMode=null )
		{
            global $LAB;

            // Add locale
			$LAB->LOCALE->loadLocale(__DIR__.'/../locale');

			// Init
			$this->initEstEID($devMode);
		}


		/**
		 *
		 *   Init the provider
		 *   -----------------
		 *   @access public
		 *   @param bool $devMode Force dev environment
		 *   @throws \Exception
		 *   @return void
		 *
		 */

		public function initEstEID( $devMode=null )
		{
            global $LAB;

            // Dev mode
			if ($devMode!==null)
			{
				$this->devMode=$devMode;
			}
			else
			{
				$this->devMode=$LAB->DEBUG->devEnvironment;
			}

			// Language
			if ($LAB->CONFIG->get('identity.esteid.language'))
			{
				$this->serviceLanguage=$LAB->CONFIG->get('identity.esteid.language');
			}
		}


		/**
		 *
		 *   Init SOAP client
		 *   ----------------
		 *   @access private
		 *   @return \SoapClient
		 *
		 */

		private function initSoapClient()
		{
            global $LAB;

            // SOAP options
			$streamOptions=array(
				'http' => array(
					'user_agent' => 'PHPSoapClient'
				)
			);
			$streamContext=stream_context_create($streamOptions);
			$soapOptions = array(
				'cache_wsdl' => WSDL_CACHE_MEMORY,
				'stream_context' => $streamContext,
				'trace' => true,
				'encoding' => 'utf-8'
			);

			// Init SOAP client
			if ($this->devMode)
			{
				$WSDL='https://tsp.demo.sk.ee/dds.wsdl';
				$this->serviceName='Testimine';
			}
			else
			{
				$WSDL='https://digidocservice.sk.ee/?wsdl';
			}

			$soapClient=new \SoapClient($WSDL, $soapOptions);
			return $soapClient;
		}


		/**
		 *
		 *   Start signing session
		 *   ---------------------
		 *   @access public
		 *   @param string $digiDocFile Digidoc file (hashcoded)
		 *   @throws Exception
		 *   @return BASE_ESTEID_SIGNRESPONSE
		 *
		 */

		public function startSession( $digiDocFile )
		{
            global $LAB;

            try
			{
				// Check && base64-encode file
				if (!mb_strlen($digiDocFile)) throw new Exception('No file.');
				$digiDocFile=base64_encode($digiDocFile);

				// Init SOAP client
				$soapClient=$this->initSoapClient();

				// Make request
				$soapResponse=$soapClient->StartSession(
					'',
					$digiDocFile,
					true
				);



				// Debug
				if ($LAB->DEBUG->debugOn)
				{
					file_put_contents($LAB->CONFIG->get('site.tmppath').'/labengine-esteid-sign.debug',date('Y-m-d H:i:s')." StartSession: \n\n",FILE_APPEND);
					file_put_contents($LAB->CONFIG->get('site.tmppath').'/labengine-esteid-sign.debug',$soapClient->__getLastRequest()."\n\n",FILE_APPEND);
					file_put_contents($LAB->CONFIG->get('site.tmppath').'/labengine-esteid-sign.debug',$soapClient->__getLastResponse()."\n\n",FILE_APPEND);
				}

				// Success
				if ($soapResponse['Status']=='OK' && intval($soapResponse['Sesscode']))
				{
					// Save Mobiil-ID data to session
					$LAB->SESSION->set('identity.esteid.sign.sesscode',intval($soapResponse['Sesscode']));

					// Return response
					$response=new BASE_ESTEID_SIGNRESPONSE();
					$response->status='OK';
					$response->sessCode=intval($soapResponse['Sesscode']);
					$response->signedDocInfo=$soapResponse['SignedDocInfo'];
					return $response;
				}

				// Fail
				else
				{
					if ($LAB->DEBUG->debugOn)
					{
						file_put_contents($LAB->CONFIG->get('site.tmppath').'/labengine-esteid-sign.debug.bdoc',base64_decode($digiDocFile));
					}
					throw new Exception('Error talking to the EID service'.($LAB->DEBUG->devEnvironment?': '.var_dump_str($soapResponse):''));
				}
			}
			catch (\SoapFault $soapFault)
			{
				if ($LAB->DEBUG->debugOn)
				{
					file_put_contents($LAB->CONFIG->get('site.tmppath').'/labengine-esteid-sign.debug',date('Y-m-d H:i:s')." StartSession SoapFault: \n\n",FILE_APPEND);
					file_put_contents($LAB->CONFIG->get('site.tmppath').'/labengine-esteid-sign.debug',var_dump_str($soapFault)."\n\n",FILE_APPEND);
					file_put_contents($LAB->CONFIG->get('site.tmppath').'/labengine-esteid-sign.debug',$soapClient->__getLastRequest()."\n\n",FILE_APPEND);
					file_put_contents($LAB->CONFIG->get('site.tmppath').'/labengine-esteid-sign.debug',$soapClient->__getLastResponse()."\n\n",FILE_APPEND);
					file_put_contents($LAB->CONFIG->get('site.tmppath').'/flaskphp-identity-esteid-sign.bdoc',base64_decode($digiDocFile));
				}
				if (!empty($soapFault->detail->message))
				{
					throw new Exception('[[ FLASK.COMMON.Error ]]: '.strval($soapFault->detail->message));
				}
				else
				{
					throw new Exception('Error talking to the EID service'.($LAB->DEBUG->devEnvironment?': '.var_dump_str($soapFault):''));
				}
			}
		}


		/**
		 *
		 *   Prepare signature
		 *   -----------------
		 *   @access public
		 *   @param string $signerCertificate Signer's certificate
		 *   @param string $signerToken Signer's certificate token
		 *   @param array $signerInfo Additional signature info
		 *   @throws Exception
		 *   @return BASE_ESTEID_SIGNRESPONSE
		 *
		 */

		public function prepareSignature( $signerCertificate, $signerToken, array $signerInfo=null )
		{
            global $LAB;

            try
			{
				// Check session
				$sessCode=$LAB->SESSION->get('identity.esteid.sign.sesscode');
				if (!intval($sessCode)) throw new Exception('SK session code not found. Session not initialized?');

				// Init SOAP client
				$soapClient=$this->initSoapClient();

				// Additional signer info
				$signerInfoRole=(($signerInfo!==null && array_key_exists('role',$signerInfo))?$signerInfo['role']:'');
				$signerInfoCity=(($signerInfo!==null && array_key_exists('city',$signerInfo))?$signerInfo['city']:'');
				$signerInfoState=(($signerInfo!==null && array_key_exists('state',$signerInfo))?$signerInfo['state']:'');
				$signerInfoPostalCode=(($signerInfo!==null && array_key_exists('postalcode',$signerInfo))?$signerInfo['postalcode']:'');
				$signerInfoCountry=(($signerInfo!==null && array_key_exists('country',$signerInfo))?$signerInfo['country']:'');

				// Make request
				$soapResponse=$soapClient->PrepareSignature(
					$sessCode,
					$signerCertificate,
					$signerToken,
					$signerInfoRole,
					$signerInfoCity,
					$signerInfoState,
					$signerInfoPostalCode,
					$signerInfoCountry,
					'LT_TM'
				);

				// Debug
				if ($LAB->DEBUG->debugOn)
				{
					file_put_contents($LAB->CONFIG->get('site.tmppath').'/labengine-esteid-sign.debug',date('Y-m-d H:i:s')." PrepareSignature: \n\n",FILE_APPEND);
					file_put_contents($LAB->CONFIG->get('site.tmppath').'/labengine-esteid-sign.debug',$soapClient->__getLastRequest()."\n\n",FILE_APPEND);
					file_put_contents($LAB->CONFIG->get('site.tmppath').'/labengine-esteid-sign.debug',$soapClient->__getLastResponse()."\n\n",FILE_APPEND);
				}

				// Success
				if ($soapResponse['Status']=='OK')
				{
					// Return response
					$response=new BASE_ESTEID_SIGNRESPONSE();
					$response->status='OK';
					$response->signatureID=$soapResponse['SignatureId'];
					$response->signedInfoDigest=$soapResponse['SignedInfoDigest'];
					$response->hashType='SHA-256';
					return $response;
				}

				// Fail
				else
				{
					if (!empty($soapResponse['Status']))
					{
						throw new Exception('[[ FLASK.COMMON.Error ]]: '.$soapResponse['Status']);
					}
					else
					{
						throw new Exception('Error talking to the EID service');
					}
				}
			}
			catch (\SoapFault $soapFault)
			{
				if ($LAB->DEBUG->debugOn)
				{
					file_put_contents($LAB->CONFIG->get('site.tmppath').'/labengine-esteid-sign.debug',date('Y-m-d H:i:s')." MobileSign SoapFault: \n\n",FILE_APPEND);
					file_put_contents($LAB->CONFIG->get('site.tmppath').'/labengine-esteid-sign.debug',var_dump_str($soapFault)."\n\n",FILE_APPEND);
					file_put_contents($LAB->CONFIG->get('site.tmppath').'/labengine-esteid-sign.debug',$soapClient->__getLastRequest()."\n\n",FILE_APPEND);
					file_put_contents($LAB->CONFIG->get('site.tmppath').'/labengine-esteid-sign.debug',$soapClient->__getLastResponse()."\n\n",FILE_APPEND);
				}
				if (!empty($soapFault->detail->message))
				{
					throw new Exception('[[ FLASK.COMMON.Error ]]: '.strval($soapFault->detail->message));
				}
				else
				{
					throw new Exception('Error talking to the EID service'.($LAB->DEBUG->devEnvironment?': '.var_dump_str($soapFault):''));
				}
			}
		}


		/**
		 *
		 *   Finalize signature
		 *   ------------------
		 *   @access public
		 *   @param string $signerSignatureID Signer's signature ID
		 *   @param string $signerSignature Signer's signature
		 *   @throws Exception
		 *   @return BASE_ESTEID_SIGNRESPONSE
		 *
		 */

		public function finalizeSignature( $signerSignatureID, $signerSignature )
		{
            global $LAB;

            try
			{
				// Check session
				$sessCode=$LAB->SESSION->get('identity.esteid.sign.sesscode');
				if (!intval($sessCode)) throw new Exception('SK session code not found. Session not initialized?');

				// Init SOAP client
				$soapClient=$this->initSoapClient();

				// Make request
				$soapResponse=$soapClient->FinalizeSignature(
					$sessCode,
					$signerSignatureID,
					$signerSignature
				);

				// Debug
				if ($LAB->DEBUG->debugOn)
				{
					file_put_contents($LAB->CONFIG->get('site.tmppath').'/labengine-esteid-sign.debug',date('Y-m-d H:i:s')." FinalizeSignature: \n\n",FILE_APPEND);
					file_put_contents($LAB->CONFIG->get('site.tmppath').'/labengine-esteid-sign.debug',$soapClient->__getLastRequest()."\n\n",FILE_APPEND);
					file_put_contents($LAB->CONFIG->get('site.tmppath').'/labengine-esteid-sign.debug',$soapClient->__getLastResponse()."\n\n",FILE_APPEND);
				}

				// Success
				if ($soapResponse['Status']=='OK')
				{
					// Return response
					$response=new BASE_ESTEID_SIGNRESPONSE();
					$response->status='OK';
					$response->signedDocInfo=$soapResponse['SignedDocInfo'];
					return $response;
				}

				// Fail
				else
				{
					if (!empty($soapResponse['Status']))
					{
						throw new Exception('[[ FLASK.COMMON.Error ]]: '.$soapResponse['Status']);
					}
					else
					{
						throw new Exception('Error talking to the EID service');
					}
				}
			}
			catch (\SoapFault $soapFault)
			{
				if ($LAB->DEBUG->debugOn)
				{
					file_put_contents($LAB->CONFIG->get('site.tmppath').'/labengine-esteid-sign.debug',date('Y-m-d H:i:s')." MobileSign SoapFault: \n\n",FILE_APPEND);
					file_put_contents($LAB->CONFIG->get('site.tmppath').'/labengine-esteid-sign.debug',var_dump_str($soapFault)."\n\n",FILE_APPEND);
					file_put_contents($LAB->CONFIG->get('site.tmppath').'/labengine-esteid-sign.debug',$soapClient->__getLastRequest()."\n\n",FILE_APPEND);
					file_put_contents($LAB->CONFIG->get('site.tmppath').'/labengine-esteid-sign.debug',$soapClient->__getLastResponse()."\n\n",FILE_APPEND);
				}
				if (!empty($soapFault->detail->message))
				{
					throw new Exception('[[ FLASK.COMMON.Error ]]: '.strval($soapFault->detail->message));
				}
				else
				{
					throw new Exception('Error talking to the EID service'.($LAB->DEBUG->devEnvironment?': '.var_dump_str($soapFault):''));
				}
			}
		}


		/**
		 *
		 *   Get signed document
		 *   -------------------
		 *   @access public
		 *   @throws Exception
		 *   @return string
		 *
		 */

		public function getSignedDoc()
		{
            global $LAB;

            try
			{
				// Check session
				$sessCode=$LAB->SESSION->get('identity.esteid.sign.sesscode');
				if (!intval($sessCode)) throw new Exception('SK session code not found. Session not initialized or already closed?');

				// Init SOAP client
				$soapClient=$this->initSoapClient();

				// Make request
				$soapResponse=$soapClient->GetSignedDoc(
					$sessCode
				);

				// Debug
				if ($LAB->DEBUG->debugOn)
				{
					file_put_contents($LAB->CONFIG->get('site.tmppath').'/labengine-esteid-sign.debug',date('Y-m-d H:i:s')." GetSignedDoc: \n\n",FILE_APPEND);
					file_put_contents($LAB->CONFIG->get('site.tmppath').'/labengine-esteid-sign.debug',$soapClient->__getLastRequest()."\n\n",FILE_APPEND);
					file_put_contents($LAB->CONFIG->get('site.tmppath').'/labengine-esteid-sign.debug',$soapClient->__getLastResponse()."\n\n",FILE_APPEND);
				}

				// Success
				if ($soapResponse['Status']=='OK')
				{
					return base64_decode($soapResponse['SignedDocData']);
				}

				// Fail
				else
				{
					throw new Exception('Error talking to the EID service'.($LAB->DEBUG->devEnvironment?': '.var_dump_str($soapResponse):''));
				}
			}
			catch (\SoapFault $soapFault)
			{
				if ($LAB->DEBUG->debugOn)
				{
					file_put_contents($LAB->CONFIG->get('site.tmppath').'/labengine-esteid-sign.debug',date('Y-m-d H:i:s')." GetSignedDoc SoapFault: \n\n",FILE_APPEND);
					file_put_contents($LAB->CONFIG->get('site.tmppath').'/labengine-esteid-sign.debug',var_dump_str($soapFault)."\n\n",FILE_APPEND);
					file_put_contents($LAB->CONFIG->get('site.tmppath').'/labengine-esteid-sign.debug',$soapClient->__getLastRequest()."\n\n",FILE_APPEND);
					file_put_contents($LAB->CONFIG->get('site.tmppath').'/labengine-esteid-sign.debug',$soapClient->__getLastResponse()."\n\n",FILE_APPEND);
				}
				if (!empty($soapFault->detail->message))
				{
					throw new Exception('[[ FLASK.COMMON.Error ]]: '.strval($soapFault->detail->message));
				}
				else
				{
					throw new Exception('Error talking to the EID service'.($LAB->DEBUG->devEnvironment?': '.var_dump_str($soapFault):''));
				}
			}
		}


		/**
		 *
		 *   Close signing session
		 *   @access public
		 *   @throws Exception
		 *   @return BASE_ESTEID_SIGNRESPONSE
		 *
		 */

		public function closeSession()
		{
            global $LAB;

            try
			{
				// Check session
				$sessCode=$LAB->SESSION->get('identity.esteid.sign.sesscode');
				if (!intval($sessCode)) throw new Exception('SK session code not found. Session not initialized or already closed?');

				// Init SOAP client
				$soapClient=$this->initSoapClient();

				// Make request
				$soapResponse=$soapClient->CloseSession(
					$sessCode
				);

				// Debug
				if ($LAB->DEBUG->debugOn)
				{
					file_put_contents($LAB->CONFIG->get('site.tmppath').'/labengine-esteid-sign.debug',date('Y-m-d H:i:s')." CloseSession: \n\n",FILE_APPEND);
					file_put_contents($LAB->CONFIG->get('site.tmppath').'/labengine-esteid-sign.debug',$soapClient->__getLastRequest()."\n\n",FILE_APPEND);
					file_put_contents($LAB->CONFIG->get('site.tmppath').'/labengine-esteid-sign.debug',$soapClient->__getLastResponse()."\n\n",FILE_APPEND);
				}

				// Success
				if ($soapResponse=='OK')
				{
					// Save Mobiil-ID data to session
					$LAB->SESSION->set('identity.esteid.sign.sesscode',null);

					// Return response
					$response=new BASE_ESTEID_SIGNRESPONSE();
					$response->status='OK';
					return $response;
				}

				// Fail
				else
				{
					throw new Exception('Error talking to the EID service'.($LAB->DEBUG->devEnvironment?': '.var_dump_str($soapResponse):''));
				}
			}
			catch (\SoapFault $soapFault)
			{
				if ($LAB->DEBUG->debugOn)
				{
					file_put_contents($LAB->CONFIG->get('site.tmppath').'/labengine-esteid-sign.debug',date('Y-m-d H:i:s')." CloseSession SoapFault: \n\n",FILE_APPEND);
					file_put_contents($LAB->CONFIG->get('site.tmppath').'/labengine-esteid-sign.debug',var_dump_str($soapFault)."\n\n",FILE_APPEND);
					file_put_contents($LAB->CONFIG->get('site.tmppath').'/labengine-esteid-sign.debug',$soapClient->__getLastRequest()."\n\n",FILE_APPEND);
					file_put_contents($LAB->CONFIG->get('site.tmppath').'/labengine-esteid-sign.debug',$soapClient->__getLastResponse()."\n\n",FILE_APPEND);
				}
				if (!empty($soapFault->detail->message))
				{
					throw new Exception('[[ FLASK.COMMON.Error ]]: '.strval($soapFault->detail->message));
				}
				else
				{
					throw new Exception('Error talking to the EID service'.($LAB->DEBUG->devEnvironment?': '.var_dump_str($soapFault):''));
				}
			}
		}


	}


?>