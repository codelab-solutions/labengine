<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   The dataobject class
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_DATAOBJECT
	{


		/**
		 *   Fields/columns
		 *   @var array
		 *   @access public
		 */
		public $_field = array();

		/**
		 *   Parameters
		 *   @var array
		 *   @access public
		 */
		public $_param = array();

		/**
		 *   Foreign relations
		 *   @var array
		 *   @access public
		 */
		public $_rel = array();

		/**
		 *   Loaded data
		 *   @var array
		 *   @access public
		 */
		public $_in = array();

		/**
		 *   Loaded?
		 *   @var array
		 *   @access public
		 */
		public $_loaded = FALSE;

		/**
		 *   OID
		 *   @var int
		 *   @access public
		 */
		public $_oid = NULL;

		/**
		 *   Parent OID
		 *   @var int
		 *   @access public
		 */
		public $_parent = NULL;

		/**
		 *   Load list found rows
		 *   @var string
		 *   @access public
		 */
		public $_foundRows = NULL;

		/**
		 *   Access: is readable?
		 *   @var int
		 *   @access public
		 */
		public $_readable = NULL;

		/**
		 *   Access: is writable?
		 *   @var int
		 *   @access public
		 */
		public $_writable = NULL;

		/**
		 *    Field names for log
		 *    @var array
		 *    @access public
		 *    @static
		 */

		public static $LOG_fieldnames = array();

		/**
		 *    Field value mappings for log
		 *    @var array
		 *    @access public
		 *    @static
		 */

		public static $LOG_fieldvaluemappings = array();

		/**
		 *    Skip fields for log
		 *    @var array
		 *    @access public
		 *    @static
		 */

		public static $LOG_nolog = array();


		/**
		 *   Constructor - init
		 *   @access public
		 *   @return BASE_DATAOBJECT
		 */

		public function __construct()
		{
			$this->initDataObject();
			$this->defineFields();
			$this->setDefaults();
		}


		/**
		 *   Init data object
		 *   @access public
		 *   @throws Exception
		 *   @return void
		 */

		public function initDataObject()
		{
			// This should be defined in the subclass.
			throw new Exception('The initDataObject() function is not implemented in the '.mb_strtoupper(get_called_class()).' subclass.');
		}


		/**
		 *   Define fields and relations for data object
		 *   @access public
		 *   @throws Exception
		 *   @return void
		 */

		public function defineFields()
		{
			// This should be defined in the subclass if necessary.
		}


		/**
		 *   Set defaults
		 *   @access public
		 *   @return void
		 */

		public function setDefaults()
		{
			// Set property table values to defaults
			if ($this->_param['prop'])
			{
				$this->_param['prop_table']=$this->_param['table'].'_prop';
				$this->_param['prop_referencefield']=$this->_param['idfield'];
				$prop_table_arr=preg_split('/_/',$this->_param['prop_table']);
				if (sizeof($prop_table_arr)==1 || (sizeof($prop_table_arr)==2 && $prop_table_arr[1]=='prop'))
				{
					$fld_base=$prop_table_arr[0];
				}
				else
				{
					$fld_base=preg_replace("/^".$prop_table_arr[0]."_/","",$this->_param['prop_table']);
					if ($prop_table_arr[sizeof($prop_table_arr)-1]!='prop') $fld_base.='Prop';
				}
				$this->_param['prop_namefield']=$fld_base.'_name';
				$this->_param['prop_valuefield']=$fld_base.'_value';
			}
		}



		/**
		 *   Clear data object
		 *   @access public
		 *   @return void
		 */

		public function clear()
		{
			// Clear variables
			foreach ($this as $k=>$v)
			{
				if (!strncmp($k,'_',1)) continue;
				unset($this->$k);
			}

			// Set system variables to defaults
			$this->_in=array();
			$this->_oid=NULL;
			$this->_loaded=FALSE;
			$this->_readable=NULL;
			$this->_writable=NULL;
		}


		/**
		 *   Load data object by OID
		 *   @static
		 *   @access public
		 *   @param mixed ID value
		 *   @param BASE_DATAOBJECT_PARAM parameters
		 *   @param boolean force object reload
		 *   @return BASE_DATAOBJECT loaded object
		 *   @throws Exception
		 */

		public static function getObject( $oid, $param=NULL, $forceReload=FALSE )
		{
			global $LAB;

			// See if we have it in cache
			$className=get_called_class();
			if (!$forceReload && is_object($LAB->objectCache[mb_strtolower($className)][$oid]))
			{
				$retVal=&$LAB->objectCache[mb_strtolower($className)][$oid];
				return $retVal;
			}

			// Create and return
			$obj=new $className();
			$obj->load($oid,$param);
			return $obj;
		}


		/**
		 *   Get data object by field
		 *   @static
		 *   @access public
		 *   @param string Field name
		 *   @param mixed ID value
		 *   @param boolean Force object reload
		 *   @param boolean Throw exception when not found?
		 *   @return BASE_DATAOBJECT loaded object
		 *   @throws Exception
		 */

		public static function getByField( $field, $value, $forceReload=FALSE, $throwException=TRUE )
		{
			global $LAB;

			// Get class
			$className=get_called_class();
			$obj=new $className();

			// Find OID
			$oid=$LAB->DB->selectFieldSQL($obj->_param['idfield'],"
				SELECT
					".$obj->_param['idfield']."
				FROM
					".$obj->_param['table']."
				WHERE
					".$field."='".addslashes($value)."'
				ORDER BY
					".$obj->_param['idfield']."
				LIMIT 1
			");
			if (empty($oid))
			{
				if ($throwException)
				{
					throw new Exception($className.' item by given parameter not found.');
				}
				else
				{
					return false;
				}
			}

			// See if we have it in cache
			if (!$forceReload && is_object($LAB->objectCache[mb_strtolower($className)][$oid]))
			{
				$retVal=&$LAB->objectCache[mb_strtolower($className)][$oid];
				return $retVal;
			}

			// Create and return
			$obj->load($oid);
			return $obj;
		}


		/**
		 *   Load data object
		 *   @access public
		 *   @param mixed $oid OID value
		 *   @param BASE_DATAOBJECT_PARAM $param parameters
		 *   @return void
		 *   @throws Exception
		 */

		public function load( $oid, $param=NULL )
		{
			global $LAB;

			// Already loaded?
			if ($this->_loaded) return TRUE;

			// Set defaults
			$this->setDefaults();

			// Clear
			$this->clear();

			// Nothing to load?
			if (empty($oid)) throw new Exception('Cannot load '.get_called_class().': missing OID');

			// Init query object
			$query=new BASE_QUERY('SELECT');

			// Main table
			$query->addTable($this->_param['table']);

			// Main where condition
			$query->addWhere($this->_param['idfield']."='".addslashes($oid)."'");

			// Other parameters
			if ($param!=NULL)
			{
				// Check
				if (!($param instanceof BASE_DATAOBJECT_PARAM))
				{
					throw new Exception('Invalid parameters, must be passed as an BASE_DATAOBJECT_PARAM object.');
					return;
				}

				// Columns
				if (is_array($param->paramColumns)) foreach ($param->paramColumns as $col)
				{
					if ($this->_param['prop'] && !strncasecmp($col,'prop_',5)) continue;
					$query->addColumns($col);
				}

				// Tables
				if (is_array($param->paramTable)) foreach ($param->paramTable as $table)
				{
					$query->addTable($table, $param->paramTableJoinCondition[$table], $param->paramTableJoinType[$table]);
				}

				// Where
				if (is_array($param->paramWhere)) foreach ($param->paramWhere as $where)
				{
					$query->addWhere($where);
				}

				// Where type
				$query->setWhereType($param->paramWhereType);

				// Group by
				if (is_array($param->paramGroupBy)) foreach ($param->paramGroupBy as $groupBy)
				{
					$query->addGroupBy($groupBy);
				}

				// Having
				if (is_array($param->paramHaving)) foreach ($param->paramHaving as $having)
				{
					$query->addHaving($having);
				}
			}

			// Record
			$row=$LAB->DB->selectOne($query);
			if ($row[$this->_param['idfield']]!=$oid) throw new Exception('Cannot load '.get_called_class().': OID '.intval($oid).' not found');
			foreach ($row as $k=>$v)
			{
				$this->_in[$k]=$v;
				$this->$k=$v;
			}

			// OID
			$this->_oid=$this->{$this->_param['idfield']};

			// Props
			if ($this->_param['prop'])
			{
				$proplist=array();
				if ($param!=NULL && is_array($param->paramColumns))
				{
					foreach ($param->paramColumns as $col)
					{
						if (strncasecmp($col,'prop_',5)) continue;
						$proplist[]=substr($col,5);
					}
				}
				if ($param==NULL || empty($param->paramColumns) || sizeof($proplist))
				{
					$sql="
						SELECT
						 *
						FROM
							".$this->_param['prop_table']."
						WHERE
							".$this->_param['prop_referencefield']."='".addslashes($oid)."'
							".(sizeof($proplist)?" and ".$this->_param['prop_namefield']." in ('".join("','",$proplist)."')":"")."
					";
					$propset=$LAB->DB->querySelectSQL($sql);
					foreach ($propset as $row)
					{
						$this->{'prop_'.$row[$this->_param['prop_namefield']]}=$row[$this->_param['prop_valuefield']];
						$this->_in['prop_'.$row[$this->_param['prop_namefield']]]=$row[$this->_param['prop_valuefield']];
					}
				}
			}

			// Put into object cache
			$LAB->objectCache[mb_strtolower(get_called_class())][$this->_oid]=&$this;

			// Relations
			if (sizeof($this->_rel))
			{
				foreach ($this->_rel as $relID => $relParam)
				{
					if (empty($this->{$relParam['field']})) continue;
					if (intval($this->{$relParam['field']})<1000000) continue;
					$cName=$relParam['remotedao'];
					$this->{$relID}=$cName::getObject($this->{$relParam['field']});
				}
			}

			// Loaded
			$this->_oid=$oid;
			$this->_loaded=TRUE;
			$this->_uniqID=uniqid();
		}


		/**
		 *   Save data object
		 *   @access public
		 *   @param BASE_DATAOBJECT_PARAM $param parameters
		 *   @param string|boolean $logMessage Log message (NULL for default, FALSE for no log, string for message)
		 *   @param string $logData Log data
		 *   @param int $refOID Log reference OID
		 *   @param string $logOp Log operation
		 *   @param string $formObject Form object for logging helper info
		 *   @return void
		 *   @throws Exception
		 */

		public function save( $param=NULL, $logMessage=NULL, $logData=NULL, $refOID=NULL, $logOp=NULL, &$formObject=NULL )
		{
			global $LAB, $op;

			// Set defaults
			$this->setDefaults();

			// Init
			$updated=FALSE;

			// Try
			try
			{
				// Start transaction
				$LAB->DB->startTransaction();

				// Save
				if ($this->_loaded)
				{
					// Op
					$op='edit';

					// Check for OID
					$oid=$this->_oid;

					// Save main record
					$cols=array();
					foreach ($this as $col => $val)
					{
						if (is_object($val)) continue;
						if ($col[0]=='_') continue;
						if (mb_strtolower($col)!=$col) continue;
						if ($this->_param['prop'] && !strncasecmp($col,'prop_',5)) continue;
						if (!empty($param) && !empty($param->paramColumns) && !in_array($col,$param->paramColumns)) continue;
						if (!empty($param) && !empty($param->paramSkipColumns) && in_array($col,$param->paramSkipColumns)) continue;
						if ((empty($param) || empty($param->paramColumns)) && $val==$this->_in[$col]) continue;
						$cols[$col]=$val;
						if ($val!=$this->_in[$col]) $updated=TRUE;
					}

					// Mod fields
					if (sizeof($cols) && !empty($this->_param['modfields']))
					{
						$cols['mod_tstamp']=date('Y-m-d H:i:s');
						if (!empty($LAB->USER->user_oid))
						{
							$cols['mod_user_oid']=intval($LAB->USER->user_oid);
						}
						else
						{
							$cols['mod_user_oid']='0';
						}
					}

					// Query
					if (sizeof($cols))
					{
						$where = array($this->_param['idfield']."='".addslashes($oid)."'");
						if (!empty($param) && !empty($param->paramWhere))
						{
							$where = array_merge($where, $param->paramWhere);
						}
						$LAB->DB->queryUpdate($this->_param['table'],$cols,$where);
					}
				}

				// Add
				else
				{
					// Op
					$op='add';
					$updated=TRUE;

					// Get OID
					$oid=$this->getOID();
					$_POST[$this->_param['idfield']]=$this->_oid=$this->{$this->_param['idfield']}=$oid;

					// Init cols
					$cols=array();
					$cols[$this->_param['idfield']]=$oid;
					if (isset($this->_param['setord']) && $this->_param['setord']) $cols['ord']=$oid;

					// Number hack
					if ($this->_param['no'])
					{
						// Parse format
						$this->_param['no_format']=(!empty($this->_param['no_format'])?$this->_param['no_format']:'N');
						$fmt=array();
						foreach (str_split($this->_param['no_format']) as $f)
						{
							if (empty($f)) continue;
							$fmt[$f]=$fmt[$f].$f;
						}

						// Fields
						$icols=array();
						$icols[$this->_param['no_field'].'_year']=(isset($fmt['Y'])?date('Y'):'0');
						$icols[$this->_param['no_field'].'_month']=(isset($fmt['M'])?date('m'):'0');
						$icols[$this->_param['no_field'].'_day']=(isset($fmt['D'])?date('d'):'0');
						$sql="
							INSERT INTO ".$this->_param['table']." (".$this->_param['idfield'].",".$this->_param['no_field']."_year,".$this->_param['no_field']."_month,".$this->_param['no_field']."_day,".$this->_param['no_field']."_seq)
							SELECT '".intval($oid)."','".intval($icols[$this->_param['no_field'].'_year'])."','".intval($icols[$this->_param['no_field'].'_month'])."','".intval($icols[$this->_param['no_field'].'_day'])."',coalesce(max(".$this->_param['no_field']."_seq),0)+1
							FROM ".$this->_param['table']."
							WHERE
							".$this->_param['no_field']."_year='".intval($icols[$this->_param['no_field'].'_year'])."'
							and ".$this->_param['no_field']."_month='".intval($icols[$this->_param['no_field'].'_month'])."'
							and ".$this->_param['no_field']."_day='".intval($icols[$this->_param['no_field'].'_day'])."'
						";
						$LAB->DB->queryInsertSQL($sql);

						// Load back
						$insertedRow=$LAB->DB->selectOneSQL("SELECT * FROM ".$this->_param['table']." WHERE ".$this->_param['idfield']."=".intval($oid));
						if ($insertedRow[$this->_param['idfield']]!=$oid) throw new Exception('Could not load back inserted row.');

						if (empty($this->{$this->_param['no_field'].'_no'}))
						{
							// Make display number
							$displayNo=$this->_param['no_format'];
							if (isset($fmt['Y']))
							{
								$str=$insertedRow[$this->_param['no_field'].'_year'];
								if (strlen($fmt['Y'])<strlen($str))
								{
									$str=substr($str,0-strlen($fmt['Y']));
								}
								elseif (strlen($fmt['Y'])>strlen($str))
								{
									$str=str_pad($str, strlen($fmt['Y']), "0", STR_PAD_LEFT);
								}
								$displayNo=str_replace($fmt['Y'],$str,$displayNo);
							}
							if (isset($fmt['M']))
							{
								$str=$insertedRow[$this->_param['no_field'].'_month'];
								if (strlen($fmt['M'])<strlen($str))
								{
									$str=substr($str,0-strlen($fmt['M']));
								}
								elseif (strlen($fmt['M'])>strlen($str))
								{
									$str=str_pad($str, strlen($fmt['M']), "0", STR_PAD_LEFT);
								}
								$displayNo=str_replace($fmt['M'],$str,$displayNo);
							}
							if (isset($fmt['D']))
							{
								$str=$insertedRow[$this->_param['no_field'].'_day'];
								if (strlen($fmt['D'])<strlen($str))
								{
									$str=substr($str,0-strlen($fmt['D']));
								}
								elseif (strlen($fmt['D'])>strlen($str))
								{
									$str=str_pad($str, strlen($fmt['D']), "0", STR_PAD_LEFT);
								}
								$displayNo=str_replace($fmt['D'],$str,$displayNo);
							}
							if (isset($fmt['N']))
							{
								if ($fmt['N']=='N')
								{
									$displayNo=str_replace('N',$insertedRow[$this->_param['no_field'].'_seq'],$displayNo);
								}
								else
								{
									$str=$insertedRow[$this->_param['no_field'].'_seq'];
									if (strlen($fmt['N'])<strlen($str))
									{
										$str=substr($str,0-strlen($fmt['N']));
									}
									elseif (strlen($fmt['N'])>strlen($str))
									{
										$str=str_pad($str, strlen($fmt['N']), "0", STR_PAD_LEFT);
									}
									$displayNo=str_replace($fmt['N'],$str,$displayNo);
								}
							}
							else
							{
								$displayNo.=$insertedRow[$this->_param['no_field'].'_seq'];
							}

							$this->{$this->_param['no_field'].'_no'}=$this->_param['no_prefix'].$displayNo;
							$cols[$this->_param['no_field'].'_no']=$this->{$this->_param['no_field'].'_no'};
						}
					}

					// Add main record
					foreach ($this as $col => $val)
					{
						if (is_object($val)) continue;
						if ($col[0]=='_') continue;
						if (mb_strtolower($col)!=$col) continue;
						if ($this->_param['prop'] && !strncasecmp($col,'prop_',5)) continue;
						if (!empty($param) && !empty($param->paramColumns) && !in_array($col,$param->paramColumns)) continue;
						if (!empty($param) && !empty($param->paramSkipColumns) && in_array($col,$param->paramSkipColumns)) continue;
						$cols[$col]=$val;
					}

					// Mod fields
					if (!empty($this->_param['modfields']))
					{
						if (!empty($this->_add_tstamp))
						{
							$cols['add_tstamp']=$this->_add_tstamp;
						}
						else
						{
							$cols['add_tstamp']=date('Y-m-d H:i:s');
						}
						if (!empty($this->_add_user_oid))
						{
							$cols['add_user_oid']=$this->_add_user_oid;
						}
						else
						{
							if (!empty($LAB->USER->user_oid))
							{
								$cols['add_user_oid']=intval($LAB->USER->user_oid);
							}
							else
							{
								$cols['add_user_oid']='0';
							}
						}
					}

					// Query
					if ($this->_param['no'])
					{
						$where=array($this->_param['idfield']."='".addslashes($oid)."'");
						$LAB->DB->queryUpdate($this->_param['table'],$cols,$where);
					}
					else
					{
						$LAB->DB->queryInsert($this->_param['table'],$cols);
					}

					// We're loaded now
					$this->_loaded=TRUE;
					$this->_uniqID=uniqid();

					// Link to objectCache
					$LAB->objectCache[mb_strtolower(get_called_class())][$oid]=&$this;
				}

				// Save properties
				if ($this->_param['prop'])
				{
					$queryCols=array();

					foreach ($this as $col => $val)
					{
						if (strncasecmp($col,'prop_',5)) continue;
						if (!empty($param) && !empty($param->paramColumns) && !in_array($col,$param->paramColumns)) continue;
						if (!empty($param) && !empty($param->paramSkipColumns) && in_array($col,$param->paramSkipColumns)) continue;
						if ((empty($param) || empty($param->paramColumns)) && $val==$this->_in[$col]) continue;
						$queryCols[]=array(
							$this->_param['prop_referencefield'] => $oid,
							$this->_param['prop_namefield']      => preg_replace('/^prop_/','',$col),
							$this->_param['prop_valuefield']     => $val
						);
						if ($val!=$this->_in[$col]) $updated=TRUE;
					}

					if (sizeof($queryCols))
					{
						$LAB->DB->queryReplace($this->_param['prop_table'],$queryCols);
					}
				}

				// Relations
				if (sizeof($this->_rel))
				{
					foreach ($this->_rel as $relID => $relParam)
					{
						if (empty($this->{$relParam['field']})) continue;
						if (intval($this->{$relParam['field']})<1000000) continue;
						$cName=$relParam['remotedao'];
						$this->{$relID}=$cName::getObject($this->{$relParam['field']});
					}
				}

				// Log
				if (($updated || $LAB->DEBUG->devEnvironment) && !empty($this->_param['log']) && $logMessage!==FALSE)
				{
					$logMessage=oneof($logMessage,$this->_param['objectname'].' [BASE.LOG.LogEntry.Action.'.$op.']');
					if ($refOID===NULL)
					{
						if (!empty($this->_param['log_refoid']))
						{
							$refOID=str_array($this->_param['log_refoid'],',');
							foreach ($refOID as $k => $v)
							{
								// Numeric: konkreetne väärtus
								if (is_numeric($v))
								{
									$refOID[$k]=$v;
								}
								// Otherwise: antud välja väärtus
								else
								{
									$refOID[$k]=$this->{$v};
								}
							}
							$refOID=join(',',$refOID);
						}
						else
						{
							$refOID=intval($this->{$this->_param['idfield']});
						}
					}
					$logData=($this->_param['log']!=='simple'?oneof($logData,$this->getLogData(oneof($logOp,$op),$formObject,$refOID)):'');
					$affectedOID=($refOID!=$this->{$this->_param['idfield']}?$this->{$this->_param['idfield']}:'0');
					BASE_LOG::logEntry($refOID,$logMessage,$logData,$affectedOID,($this->_add_user_oid?$this->_add_user_oid:false),($this->_add_tstamp?$this->_add_tstamp:false));
				}

				// Set $_in to current values
				$this->_in=array();
				foreach ($this as $col => $val)
				{
					if (is_object($val)) continue;
					if ($col[0]=='_') continue;
					if (mb_strtolower($col)!=$col) continue;
					$this->_in[$col]=$val;
				}

				// Commit
				$LAB->DB->doCommit();
				return TRUE;
			}

				// Catch exception: rollback and re-throw
			catch (Exception $e)
			{
				$LAB->DB->doRollback();
				throw $e;
			}
		}


		/**
		 *   Delete data object
		 *   @param string|boolean $logMessage Log message (NULL for default, FALSE for no log, string for message)
		 *   @param string $logData Log data
		 *   @param int $refOID Log reference OID
		 *   @param string $logOp Log operation
		 *   @return void
		 *   @throws Exception
		 */

		public function delete ( $logMessage=NULL, $logData=NULL, $refOID=NULL, $logOp=NULL )
		{
			global $LAB;

			// Check
			if (!$this->_loaded) throw new Exception(get_called_class().'::delete() failed: object not loaded.');

			// Set defaults
			$this->setDefaults();

			// Try
			try
			{
				// Start transaction
				$LAB->DB->startTransaction();

				// Delete main entry
				$query=new BASE_QUERY('DELETE');
				$query->addTable($this->_param['table']);
				$query->addWhere($this->_param['idfield']."='".addslashes($this->_oid)."'");
				$LAB->DB->queryDelete($query);

				// Delete props
				if ($this->_param['prop'])
				{
					$query=new BASE_QUERY('DELETE');
					$query->addTable($this->_param['prop_table']);
					$query->addWhere($this->_param['prop_referencefield']."='".addslashes($this->_oid)."'");
					$LAB->DB->queryDelete($query);
				}

				// Log
				if (!empty($this->_param) && $logMessage!==FALSE)
				{
					$logMessage=oneof($logMessage,$this->_param['objectname'].' [BASE.LOG.LogEntry.Action.delete]');
					if ($refOID===NULL)
					{
						if (!empty($this->_param['log_refoid']))
						{
							$refOID=str_array($this->_param['log_refoid'],',');
							foreach ($refOID as $k => $v)
							{
								// Numeric: konkreetne väärtus
								if (is_numeric($v))
								{
									$refOID[$k]=$v;
								}
								// Otherwise: antud välja väärtus
								else
								{
									$refOID[$k]=$this->{$v};
								}
							}
							$refOID=join(',',$refOID);
						}
						else
						{
							$refOID=intval($this->{$this->_param['idfield']});
						}
					}
					$logData=($this->_param['log']!=='simple'?oneof($logData,$this->getLogData(oneof($logOp,'delete'),$formObject,$refOID)):'');
					$affectedOID=($refOID!=$this->{$this->_param['idfield']}?$this->{$this->_param['idfield']}:false);
					BASE_LOG::logEntry($refOID,$logMessage,$logData,$affectedOID);
				}

				// Commit
				$LAB->DB->doCommit();

				// Reset self
				$this->_oid=NULL;
				$this->_loaded=FALSE;
				$this->_in=array();
				foreach ($this->_rel as $relID => $relParam) unset($this->$relID);
			}

				// Catch exception: rollback and re-throw
			catch (Exception $e)
			{
				$LAB->DB->doRollback();
				throw $e;
			}
		}


		/**
		 *   Swap two items
		 *   @access public
		 *   @param int OID 1
		 *   @param int OID 2
		 *   @return void
		 *   @throws Exception
		 */

		public function swap ( $oid1, $oid2 )
		{
			global $LAB;

			// Set defaults
			$this->setDefaults();

			// Check
			if (!$this->_param['setord']) return;

			// Try
			try
			{
				// Start transaction
				$LAB->DB->startTransaction();

				// Load
				$sql="SELECT ".$this->_param['idfield'].", ord FROM ".$this->_param['table']." WHERE ".$this->_param['idfield']." in('".addslashes($oid1)."','".addslashes($oid2)."')";
				$dataset=$LAB->DB->querySelectSQL($sql,$this->_param['idfield']);
				if (sizeof($dataset)!=2) throw new Exception('[BASE.COMMON.ErrorReadingData]');

				// Swap 1
				$cols=array();
				$cols['ord']=$dataset[$oid2]['ord'];
				$LAB->DB->queryUpdate($this->_param['table'],$cols,$this->_param['idfield']."='".addslashes($oid1)."'");

				// Swap 2
				$cols=array();
				$cols['ord']=$dataset[$oid1]['ord'];
				$LAB->DB->queryUpdate($this->_param['table'],$cols,$this->_param['idfield']."='".addslashes($oid2)."'");

				// Commit
				$LAB->DB->doCommit();
			}

				// Catch exception: rollback and re-throw
			catch (Exception $e)
			{
				$LAB->DB->doRollback();
				throw $e;
			}
		}


		/**
		 *   Load list
		 *   @access public
		 *   @param BASE_DATAOBJECT_PARAM $param Additional load parameters
		 *   @return array dataset
		 *   @throws Exception
		 */

		public function loadList( $param=NULL )
		{
			global $LAB;

			// Set default
			$this->setDefaults();

			// Init query object
			$query=new BASE_QUERY('SELECT');
			$joinedTables=array();

			// Main table
			$query->addTable($this->_param['table']);

			// Other parameters
			if ($param!=NULL)
			{
				// Check
				if (!($param instanceof BASE_DATAOBJECT_PARAM))
				{
					throw new Exception('Invalid parameters, must be passed as an BASE_DATAOBJECT_PARAM object.');
					return;
				}

				// Columns
				if (is_array($param->paramColumns)) foreach ($param->paramColumns as $col)
				{
					if ($this->_param['prop'] && !strncasecmp($col,'prop_',5))
					{
						$query->addColumns(array($col.'.'.$this->_param['prop_valuefield'].' as '.$col));
						$query->addTable($this->_param['prop_table'].' as '.$col,$this->_param['table'].".".$this->_param['idfield']."=".$col.".".$this->_param['prop_referencefield']." and ".$col.".".$this->_param['prop_namefield']."='".addslashes(substr($col,5))."'");
					}
					else
					{
						if (strpos($col,'->')!==FALSE)
						{
							$relationArr=preg_split('/->/',$col);
							$fld=array_pop($relationArr);

							$RELBASE=&$this;
							for ($i=0;$i<sizeof($relationArr);++$i)
							{
								$tbl=mb_strtoupper($relationArr[$i]);
								if (!isset($RELBASE->_rel[$relationArr[$i]]))
								{
									throw new Exception('Unknown relation: '.mb_strtoupper(get_class($RELBASE)).'->'.$tbl);
								}
								$currentRel=$RELBASE->_rel[$tbl];
								$cName=$currentRel['remotedao'];
								$REL=new $cName();
								$REL->setDefaults();
								if (empty($joinedTables[$tbl]))
								{
									$query->addTable($REL->_param['table'].' as '.$tbl,$tbl.".".$REL->_param['idfield']."=".(get_class($RELBASE)==get_class($this)?$this->_param['table']:mb_strtoupper($relationArr[$i-1])).'.'.$currentRel['field']);
									$joinedTables[$tbl]=$tbl;
								}
								$RELBASE=&$REL;
							}
							if ($REL->_param['prop'] && !strncasecmp($fld,'prop_',5))
							{
								$query->addColumns($tbl."_".$fld.'.'.$REL->_param['prop_valuefield'].' as '.str_replace('->','_',$col));
								$query->addTable($REL->_param['prop_table'].' as '.$tbl."_".$fld,$tbl.".".$REL->_param['idfield']."=".$tbl."_".$fld.".".$REL->_param['prop_referencefield']." and ".$tbl.'_'.$fld.".".$REL->_param['prop_namefield']."='".addslashes(substr($fld,5))."'");
							}
							else
							{
								$query->addColumns($tbl.'.'.$fld.' as '.str_replace('.','_',$tbl).'_'.$fld);
							}
						}
						else
						{
							if (strpos($col,' as ')!==FALSE || strpos($col,'.')!==FALSE || strpos($col,'*')!==FALSE)
							{
								$query->addColumns(array($col));
							}
							else
							{
								$query->addColumns(array($this->_param['table'].'.'.$col));
							}
						}
					}
				}

				// Tables
				if (is_array($param->paramTable)) foreach ($param->paramTable as $table)
				{
					$query->addTable($table, $param->paramTableJoinCondition[$table], $param->paramTableJoinType[$table]);
				}

				// Where
				if (is_array($param->paramWhere)) foreach ($param->paramWhere as $where)
				{
					$query->addWhere($where);
				}

				// Group by
				if (is_array($param->paramGroupBy)) foreach ($param->paramGroupBy as $groupBy)
				{
					$query->addGroupBy($groupBy);
				}

				// Having
				if (is_array($param->paramHaving)) foreach ($param->paramHaving as $having)
				{
					$query->addHaving($having);
				}

				// Order by
				if (is_array($param->paramOrderBy)) foreach ($param->paramOrderBy as $orderBy)
				{
					$query->addOrderBy($orderBy);
				}

				// Limit
				$query->addLimit($param->paramLimit, $param->paramLimitOffset);

				// Calc found rows
				if ($param->calcFoundRows) $query->calcFoundRows=TRUE;
			}

			// Run query, return results
			$dataset=$LAB->DB->querySelect($query);
			if ($param->calcFoundRows) $this->_foundRows=$LAB->DB->foundRows();
			return $dataset;
		}


		/**
		 *   Get list - static wrapper for loadList
		 *   @access public
		 *   @static
		 *   @param BASE_DATAOBJECT_PARAM $param Additional load parameters
		 *   @return array dataset
		 *   @throws Exception
		 */

		public static function getList( $param=NULL )
		{
			$className=get_called_class();
			$CLASS=new $className();
			return $CLASS->loadList($param);
		}


		/**
		 *   Get a new OID number
		 *   @access public
		 *   @return int OID
		 */

		public function getOID ()
		{
			global $LAB;

			// Init
			$sequenceTable=oneof($this->_param['sequence'],'base_sequence');
			$objectType=mb_strtolower(oneof($this->_param['table'],get_class($this)));

			// Insert an entry into the object table
			$cols=array();
			$cols['sequence_objecttype']=$objectType;
			$oid=$LAB->DB->queryInsert($sequenceTable,$cols);

			// Return the ID we got
			return $oid;
		}


		/**
		 *   Get log data
		 *   @access public
		 *  @param string $op Operation
		 * @param BASE_FORM $formObject Form object if submitting a form
		 * @param int $refOID Reference OID
		 *   @return string
		 */

		public function getLogData( $op='edit', $formObject=NULL, $refOID=NULL )
		{
			global $LAB;

			// Init
			$updated=FALSE;

			// On delete, log key relations and values
			if ($op=='delete')
			{
				// Init
				$updated=TRUE;
				$XML='';

				// ID field
				if (!empty($this->_param['idfield']))
				{
					$XML.='<fld id="'.htmlspecialchars($this->_param['idfield']).'" name="'.htmlspecialchars($this->_param['objectname']).' OID">';
					$XML.='<value>'.htmlspecialchars($this->{$this->_param['idfield']}).'</value>';
					$XML.='</fld>';
				}

				// Key relations
				if (sizeof($this->_rel))
				{
					reset($this->_rel);
					foreach ($this->_rel as $relID => $relParam)
					{
						if (!$relParam['keyrelation']) continue;
						if ($relParam['type']!='onetomany') continue;
						if (empty($this->{$relParam['field']})) continue;
						if (!is_object($this->$relID) || !$this->{$relID}->_loaded) continue;
						$XML.='<fld';
						$XML.=' id="'.htmlspecialchars($this->{$relParam['field']}).'"';
						if (!empty(static::$LOG_fieldnames[$this->{$relParam['field']}]))
						{
							$XML.=' name="'.htmlspecialchars(static::$LOG_fieldnames[$this->{$relParam['field']}]).'">';
						}
						elseif (is_object($this->_field[$this->{$relParam['field']}]))
						{
							$XML.=' name="'.htmlspecialchars($this->_field[$this->{$relParam['field']}]->getTitle()).'">';
						}
						elseif (!empty($this->{$relID}->_param['objectname']))
						{
							$XML.=' name="'.htmlspecialchars($this->{$relID}->_param['objectname']).'"';
						}
						$XML.='>';
						$desc='';
						if (!empty($this->{$relID}->_param['descriptionfield']))
						{
							$descFldArr=str_array($this->{$relID}->_param['descriptionfield']);
							$descArr=array();
							foreach ($descFldArr as $descFld)
							{
								if (strlen($this->{$relID}->{$descFld})) $descArr[]=$this->{$relID}->{$descFld};
							}
							if (sizeof($descArr)) $desc=join('; ',$descArr);
						}
						if (strlen($desc))
						{
							$XML.='<value value="'.htmlspecialchars($this->{$relParam['field']}).'">'.htmlspecialchars($desc).'</value>';
						}
						else
						{
							$XML.='<value>'.htmlspecialchars($this->{$relParam['field']}).'</value>';
						}
						$XML.='</fld>';
					}
				}

				// Key fields
				if (!empty($this->_param['descriptionfield']))
				{
					$descFldArr=str_array($this->_param['descriptionfield']);
					foreach ($descFldArr as $descFld)
					{
						if (!strlen($this->{$descFld})) continue;
						$XML.='<fld';
						$XML.=' id="'.htmlspecialchars($descFld).'"';
						if (!empty(static::$LOG_fieldnames[$descFld]))
						{
							$XML.=' name="'.htmlspecialchars(static::$LOG_fieldnames[$descFld]).'">';
						}
						elseif (is_object($this->_field[$descFld]))
						{
							$XML.=' name="'.htmlspecialchars($this->_field[$descFld]->getTitle()).'">';
						}
						$XML.='>';
						if (!empty(static::$LOG_fieldvaluemappings[$descFld]) && isset(static::$LOG_fieldvaluemappings[$descFld][$this->{$descFld}]))
						{
							$XML.='<value value="'.htmlspecialchars($this->{$descFld}).'">'.htmlspecialchars(static::$LOG_fieldvaluemappings[$descFld][$this->{$descFld}]).'</value>';
						}
						elseif (is_object($this->_field[$descFld]))
						{
							$XML.='<value value="'.htmlspecialchars($this->{$descFld}).'">'.htmlspecialchars($this->_field[$descFld]->getLogValue($this->{$descFld})).'</value>';
						}
						else
						{
							$XML.='<value>'.htmlspecialchars($this->{$descFld}).'</value>';
						}
						$XML.='</fld>';
					}
				}

				// Return
				return '<logdata>'.$XML.'</logdata>';
			}

			// Muutunud väärtused
			$logValues=array();
			foreach ($this as $col => $val)
			{
				// Need on süsteemsed
				if ($col[0]=='_') continue;
				if ($col!=mb_strtolower($col)) continue;
				if ($col==$this->_param['idfield']) continue;

				// Neid ei näita
				if (!empty(static::$LOG_nolog) && in_array($col,str_array(static::$LOG_nolog))) continue;
				if (is_object($this->_field[$col]) && $this->_field[$col]->noLog()) continue;

				// Mitte-editi korral tühja väärtust ei näita
				if ($op!='edit' && !mb_strlen($val)) continue;

				// Editi korral skipime mittemuutunud asjad
				if ($op=='edit')
				{
					if ($val==$this->_in[$col]) continue;
					if ($val=='' && $this->_in[$col]=='0') continue;
					if ($val=='0' && $this->_in[$col]=='') continue;
				}

				// Otherwise, logime
				$logValues[$col]=$col;
			}

			// Init
			$XML='';

			// ID field
			if ($refOID!=$this->{$this->_param['idfield']})
			{
				$XML.='<fld id="'.htmlspecialchars($this->_param['idfield']).'" name="'.htmlspecialchars($this->_param['objectname']).' OID">';
				$XML.='<value>'.htmlspecialchars($this->{$this->_param['idfield']}).'</value>';
				$XML.='</fld>';
			}

			// formObjecti korral käime esmalt selle läbi
			if (!empty($formObject) && is_object($formObject))
			{
				foreach ($formObject->field as $fieldTag => $fieldObject)
				{
					$logVal=$fieldObject->getLogData($this,$op,$logValues);
					$XML.=$logVal;
					if (mb_strlen($logVal)) $updated=TRUE;
				}
			}

			// Key relations
			if (sizeof($this->_rel))
			{
				reset($this->_rel);
				foreach ($this->_rel as $relID => $relParam)
				{
					if (!$relParam['keyrelation']) continue;
					if ($relParam['type']!='onetomany') continue;
					if (empty($this->{$relParam['field']})) continue;
					if (!is_object($this->$relID) || !$this->{$relID}->_loaded) continue;
					unset($logValues[$relParam['field']]);
					$XML.='<fld';
					$XML.=' id="'.htmlspecialchars($this->{$relParam['field']}).'"';
					if (!empty(static::$LOG_fieldnames[$this->{$relParam['field']}]))
					{
						$XML.=' name="'.htmlspecialchars(static::$LOG_fieldnames[$this->{$relParam['field']}]).'">';
					}
					elseif (is_object($this->_field[$this->{$relParam['field']}]))
					{
						$XML.=' name="'.htmlspecialchars($this->_field[$this->{$relParam['field']}]->getTitle()).'"';
					}
					elseif (!empty($this->{$relID}->_param['objectname']))
					{
						$XML.=' name="'.htmlspecialchars($this->{$relID}->_param['objectname']).'"';
					}
					$XML.='>';
					if ($op=='edit' && $this->_in[$relParam['field']]!=$this->{$relParam['field']})
					{
						$updated=TRUE;
						$XML.='<old>'.htmlspecialchars($this->_in[$relParam['field']]).'</old>';
						$fld='new';
					}
					else
					{
						$fld=(in_array($op,array('add'))?'new':'value');
					}
					$desc='';
					if (!empty($this->{$relID}->_param['descriptionfield']))
					{
						$descFldArr=str_array($this->{$relID}->_param['descriptionfield']);
						$descArr=array();
						foreach ($descFldArr as $descFld)
						{
							if (strlen($this->{$relID}->{$descFld})) $descArr[]=$this->{$relID}->{$descFld};
						}
						if (sizeof($descArr)) $desc=join('; ',$descArr);
					}
					if (strlen($desc))
					{
						$XML.='<'.$fld.' value="'.htmlspecialchars($this->{$relParam['field']}).'">'.htmlspecialchars($desc).'</'.$fld.'>';
					}
					else
					{
						$XML.='<'.$fld.'>'.htmlspecialchars($this->{$relParam['field']}).'</'.$fld.'>';
					}
					$XML.='</fld>';
				}
			}

			// Ülejäänud väärtused
			foreach ($logValues as $k)
			{
				// Neid ei näita
				if ($k=='ord') continue;
				if (in_array($k,str_array(static::$LOG_nolog))) continue;
				if (is_object($this->_field[$k]) && $this->_field[$k]->noLog()) continue;

				// Teeme välja
				$fld='<fld';
					$fld.=' id="'.htmlspecialchars($k).'"';
					if (!empty(static::$LOG_fieldnames[$k]))
					{
						$fld.=' name="'.htmlspecialchars(static::$LOG_fieldnames[$k]).'"';
					}
					elseif (is_object($this->_field[$k]))
					{
						$fld.=' name="'.htmlspecialchars($this->_field[$k]->getTitle()).'"';
					}
				$fld.='>';

				if ($op=='edit')
				{
					$valSet=array(
						'old' => $this->_in[$k],
						'new' => $this->{$k}
					);
					if ($this->_in[$k]!=$this->{$k}) $updated=TRUE;
				}
				elseif ($op=='add')
				{
					$valSet=array(
						'new' => $this->{$k}
					);
					$updated=TRUE;
				}
				else
				{
					$valSet=array(
						'value' => $this->{$k}
					);
					$updated=TRUE;
				}

				foreach ($valSet as $fldTag => $val)
				{
					if (!empty(static::$LOG_fieldvaluemappings[$k]) && isset(static::$LOG_fieldvaluemappings[$k][$val]))
					{
						$fld.='<'.$fldTag;
						$fld.=' value="'.htmlspecialchars($val).'"';
						$fld.='>';
						$fld.=htmlspecialchars(static::$LOG_fieldvaluemappings[$k][$val]);
						$fld.='</'.$fldTag.'>';
					}
					elseif (is_object($this->_field[$k]))
					{
						$fld.='<'.$fldTag;
						$fld.=' value="'.htmlspecialchars($val).'"';
						$fld.='>';
						$fld.=htmlspecialchars($this->_field[$k]->getLogValue($val));
						$fld.='</'.$fldTag.'>';
					}
					else
					{
						// Is it a relation?
						if (sizeof($this->_rel))
						{
							reset($this->_rel);
							foreach ($this->_rel as $relID => $relParam)
							{
								if ($relParam['field']!=$k) continue;
								if ($relParam['keyrelation']) continue;
								if ($relParam['type']!='onetomany') continue;
								if (!is_object($this->$relID) || !$this->{$relID}->_loaded) continue;
								if ($this->$relID->{oneof($relParam['remotefield'],$relParam['field'])}==$val)
								{
									$relObject=&$this->{$relID};
								}
								else
								{
									try
									{
										$relObject=new $relParam['remotedao']();
										$relObject->load($val);
										if (!$relObject->_loaded) continue;
									}
									catch (Exception $e)
									{
										continue;
									}
								}
								if (!empty($relObject->_param['descriptionfield']))
								{
									$descFldArr=str_array($relObject->_param['descriptionfield']);
									$descArr=array();
									foreach ($descFldArr as $descFld)
									{
										if (strlen($relObject->{$descFld})) $descArr[]=$relObject->{$descFld};
									}
									if (sizeof($descArr)) $val=join('; ',$descArr).' ['.$val.']';
								}
							}
						}

						$fld.='<'.$fldTag;
						$fld.='>';
						$fld.=htmlspecialchars($val);
						$fld.='</'.$fldTag.'>';
					}
				}

				// Väli lõpeb
				$fld.='</fld>';
				$XML.=$fld;
			}

			// Return
			if ($op=='edit' && (!$LAB->DEBUG->devEnvironment && !$updated)) return '';
			return (strlen($XML)?'<logdata op="'.htmlspecialchars($op).'">'.$XML.'</logdata>':'');
		}


		/**
		 *   Check if the field value is unique
		 *   @access public
		 *   @param string $fieldName Field name
		 *   @param string $fieldValue Field value
		 *   @param BASE_DATAOBJECT_PARAM $param Additional parameters
		 *   @return bool
		 *   @throws Exception
		 */

		public function isUnique( $fieldName, $fieldValue, $param=NULL )
		{
			global $LAB;

			// Set default
			$this->setDefaults();

			// Init query object
			$query=new BASE_QUERY('SELECT');

			// Main table
			$query->addTable($this->_param['table']);

			// Other parameters
			if ($param!=NULL)
			{
				// Check
				if (!($param instanceof BASE_DATAOBJECT_PARAM))
				{
					throw new Exception('Invalid parameters, must be passed as an BASE_DATAOBJECT_PARAM object.');
					return;
				}

				// Columns
				if (is_array($param->paramColumns)) foreach ($param->paramColumns as $col)
				{
					if ($this->_param['prop'] && !strncasecmp($col,'prop_',5))
					{
						$query->addColumns($col.'.'.$this->_param['prop_valuefield'].' as '.$col);
						$query->addTable($this->_param['prop_table'].' as '.$col,$this->_param['table'].".".$this->_param['idfield']."=".$col.".".$this->_param['prop_referencefield'].(!empty($this->_param['localized'])?" and ".$col.".".$this->_param['prop_langfield']." in ('".addslashes($LAB->REQUEST->requestLang)."','')":"")." and ".$col.".".$this->_param['prop_namefield']."='".addslashes(substr($col,5))."'");
					}
					else
					{
						if (strpos($col,'.')===FALSE)
						{
							$query->addColumns($this->_param['table'].'.'.$col);
						}
						else
						{
							$query->addColumns($col);
						}
					}
				}

				// Tables
				if (is_array($param->paramTable)) foreach ($param->paramTable as $table)
				{
					$query->addTable($table, $param->paramTableJoinCondition[$table], $param->paramTableJoinType[$table]);
				}

				// Where
				if (is_array($param->paramWhere)) foreach ($param->paramWhere as $where)
				{
					$query->addWhere($where);
				}

				// Group by
				if (is_array($param->paramGroupBy)) foreach ($param->paramGroupBy as $groupBy)
				{
					$query->addGroupBy($groupBy);
				}

				// Having
				if (is_array($param->paramHaving)) foreach ($param->paramHaving as $having)
				{
					$query->addHaving($having);
				}

				// Order by
				if (is_array($param->paramOrderBy)) foreach ($param->paramOrderBy as $orderBy)
				{
					$query->addOrderBy($orderBy);
				}
			}

			// Filter
			$query->addWhere($fieldName."='".addslashes($fieldValue)."'");
			if ($this->_loaded)
			{
				$query->addWhere($this->_param['idfield']."!='".addslashes($this->_oid)."'");
			}

			// Limit
			$query->addLimit('1');

			// Run query, return result
			$dataset=$LAB->DB->querySelect($query);
			return (sizeof($dataset) ? FALSE : TRUE);
		}



		/**
		 *   Add one-to-many relation
		 *   @access public
		 *   @param string $id Relation IS
		 *   @param string $field Field/column name
		 *   @param string $remoteDAO Remote data object class name
		 *   @param string $remoteField Remote field name (if empty, same as in this table)
		 *   @param bool $keyRelation Is a key relation
		 *   @return void
		 */

		public function addRelation( $id, $field, $remoteDAO, $remoteField=NULL, $keyRelation=FALSE )
		{
			$this->_rel[$id]=array(
				'type' => 'onetomany',
				'field' => $field,
				'remotedao' => $remoteDAO,
				'remotefield' => $remoteField,
				'keyrelation' => $keyRelation
			);
		}


		/**
		 *   Add a column/field
		 *   @access public
		 *   @return BASE_FIELD field object reference
		 *   @throws Exception
		 */

		public function addField( $fieldTag, $fieldData=NULL )
		{
			// Check if the action already exists
			if (isset($this->_field[$fieldTag]))
			{
				throw new Exception('Field '.$fieldTag.' already exists.');
				return NULL;
			}

			// Field object passed
			if ($fieldData instanceof BASE_FIELD)
			{
				$this->_field[$fieldTag]=$fieldData;
			}

			// Create blank field
			elseif (strlen($fieldData))
			{
				$fld=new $fieldData();
				$this->_field[$fieldTag]=$fld;
			}

			// Set tag/column
			$this->_field[$fieldTag]->tag=$fieldTag;
			$this->_field[$fieldTag]->fieldColumn=$fieldTag;

			// Set back-reference
			$this->_field[$fieldTag]->dataObject=&$this;

			// Return reference to action
			$retval=&$this->_field[$fieldTag];
			return $retval;
		}


	}


?>