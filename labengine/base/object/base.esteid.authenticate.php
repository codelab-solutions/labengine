<?php


	/**
	 *
	 *   Authentication provider for Estonian E-ID
	 *
	 *   @author   Codelab Solutions OÜ <codelab@codelab.ee>
	 *   @license  https://www.flaskphp.com/LICENSE MIT
	 *
	 */


	class BASE_ESTEID_AUTHENTICATE
	{


		/**
		 *
		 *   Constructor
		 *   -----------
		 *   @access public
		 *   @param bool $forceDev Force dev environment
		 *   @throws \Exception
		 *   @return BASE_ESTEID_AUTHENTICATE
		 *
		 */

		public function __construct( $forceDev=false )
		{
            global $LAB;

            // Add locale
			$LAB->LOCALE->loadLocale(__DIR__.'/../locale');

			// Init
			$this->initEstEID($forceDev);
		}


		/**
		 *
		 *   Init the provider
		 *   -----------------
		 *   @access public
		 *   @param bool $forceDev Force dev environment
		 *   @throws \Exception
		 *   @return void
		 *
		 */

		public function initEstEID( $forceDev=false )
		{
			// This can be extended in the subclass if necessary.
		}


		/**
		 *
		 *   Get certificate data
		 *   --------------------
		 *   @access public
		 *   @param bool $throwException Throw exception on failure (false = return null)
		 *   @throws Exception
		 *   @return BASE_ESTEID_AUTHENTICATERESPONSE
		 *
		 */

		public function getCertificateData( $throwException=false )
		{
			try
			{
				// Check
				$CN=oneof($_SERVER['SSL_CLIENT_S_DN_CN'],$_SERVER['REDIRECT_SSL_CLIENT_S_DN_CN'],$_SERVER['HTTP_SSL_CLIENT_S_DN_CN']);
				if (!mb_strlen($CN)) throw new Exception(BASE_TEMPLATE::parseContent('[BASE.IDENTITY.EstEID.Error.EIDNotDetected]'));

				// Init object
				$response=new BASE_ESTEID_AUTHENTICATERESPONSE();

				// CN
				$response->CN=$CN;

				// Parse CN
				list($lastName,$firstName,$idCode)=str_array($CN);
				if (empty($lastName) || empty($firstName) || empty($idCode)) throw new Exception(BASE_TEMPLATE::parseContent('[BASE.IDENTITY.EstEID.Error.ErrorParsingCN]'));

				// Set parsed data
				$response->firstName=$firstName;
				$response->lastName=$lastName;
				$response->idCode=preg_replace("/^PNOEE-/","",$idCode);

				// Return
				return $response;
			}
			catch (Exception $e)
			{
				if ($throwException) throw $e;
				return null;
			}
		}


		/**
		 *
		 *   Get certificate data value
		 *   --------------------------
		 *   @access public
		 *   @param string $key Data key
		 *   @param bool $throwException Throw exception on failure (false = return null)
		 *   @throws Exception
		 *   @return mixed
		 *
		 */

		public function getCertificateDataValue( $key, $throwException=false )
		{
			$certificateData=$this->getCertificateData($throwException);
			return $certificateData->{$key};
		}


	}


?>