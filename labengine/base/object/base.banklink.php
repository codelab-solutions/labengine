<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   BankLink integration: base class
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_BANKLINK
	{


		/**
		 *   Bank ID (should be set in the implementation class)
		 *   @var string
		 *   @static
		 */

		public static $BANK_id = null;


		/*
		 *   Init
		 *   @return void
		 */

		public function init()
		{
			// This should be implemented in the subclass
		}


		/**
		 *   Generate EE ref no
		 *   @access public
		 *   @static
		 *   @throws Exception
		 *   @return string
		 */

		public static function generateRefNo( $base )
		{
			global $LAB;

			// Init
			$number=$base;

			// The 731 method
			$multiplier=7;
			$checksum=0;
			do
			{
				$checksum=$checksum+(($number%10)*$multiplier);
				if ($multiplier==7) $multiplier=3;
				elseif ($multiplier==3) $multiplier=1;
				elseif ($multiplier==1) $multiplier=7;
			}
			while((int)$number/=10);
			$checksum=10-($checksum%10);
			if ($checksum==10) $checksum=0;

			// Valmis
			return strval($base).strval($checksum);
		}


		/**
		 *   Validate bank response
		 *   @param  array $requestData Fields received from the bank
		 *   @throws Exception
		 *   @return string Result (completed/cancelled/failed/not-implemented)
		 */

		public function validateResponse( $requestData, $throwException=true )
		{
			// Should be implemented in the subclass if applicable.
			return 'not-implemented';
		}


		/**
		 *   Get payment data from response
		 *   @param  array $requestData Fields received from the bank
		 *   @throws Exception
		 *   @return array Result
		 */

		public function getPaymentInfo( $requestData, $throwException=true )
		{
			// Should be implemented in the subclass if applicable.
			return array();
		}


		/**
		 *   Generate payment form
		 *   @public
		 *   @param string $paymentID Order ID
		 *   @param float $paymentSum Order sum
		 *   @param string $paymentDescription Order description
		 *   @param int $paymentReferenceNo Order reference no
		 *   @return string Payment form
		 *   @throws Exception
		 */

		function generatePaymentForm( $paymentID, $paymentSum, $paymentDescription=null, $paymentReferenceNo=null )
		{
			// Should be implemented in the subclass.
			throw new Exception('Function generatePaymentForm() not implemented.');
		}


	}


?>