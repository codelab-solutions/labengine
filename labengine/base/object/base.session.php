<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   The default session handler class
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_SESSION
	{


		/**
		 *   Session data
		 *   @var array
		 *   @access public
		 */

		public $sessionData = array();


		/**
		 *   Is session loaded
		 *   @var boolean
		 *   @access public
		 */

		public $sessionLoaded = FALSE;


		/**
		 *   Is session updated
		 *   @var boolean
		 *   @access public
		 */

		public $sessionUpdated  = FALSE;



		/**
		 *   Load the session
		 *   @access public
		 *   @return void
		 */

		public function loadSession()
		{
			global $LAB;

			// Use forced session SID
			if (!empty($_REQUEST['LAB_FORCE_SID']) && strlen($_REQUEST['LAB_FORCE_SID']))
			{
				session_id($_REQUEST['LAB_FORCE_SID']);
			}

			// Hack: make sure gc_maxlifetime is not smaller than cookie lifetime
			if (intval($LAB->CONFIG->get('session.gc_maxlifetime')))
			{
				if (ini_get('session.gc_maxlifetime')<intval($LAB->CONFIG->get('session.cookie_lifetime')))
				{
					ini_set('session.gc_maxlifetime',intval($LAB->CONFIG->get('session.cookie_lifetime')));
				}
			}

			// Start session
			session_start();
			$this->sessionData=&$_SESSION;
			$this->sessionLoaded=TRUE;

			// Set cookie lifetime
			if (intval($LAB->CONFIG->get('session.cookie_lifetime')))
			{
		    setcookie(session_name(),session_id(),time()+intval($LAB->CONFIG->get('session.cookie_lifetime')),"/");
			}
		}



		/**
		 *   Save the session
		 *   @access public
		 *   @return void
		 */

		public function saveSession()
		{
		}



		/**
		 *   Destroy the session
		 *   @access public
		 *   @param boolean Destroy the data as well (FALSE by default)
		 *   @return void
		 */

		public function destroySession( $destroyData=FALSE )
		{
			if ($destroyData)
			{
				$this->sessionData=array();
			}
			session_write_close();
			$this->sessionLoaded=FALSE;
		}



		/**
		 *   Set a session variable
		 *   @access public
		 *   @param string variable name
		 *   @param string mixed variable value
		 *   @return void
		 */

		public function set( $varName, $varValue )
		{
			traverse_set($varName,$this->sessionData,$varValue);
			$this->sessionUpdated=TRUE;
		}



		/**
		 *   Get a session variable
		 *   @access public
		 *   @param string variable name
		 *   @return void
		 */

		public function get( $varName )
		{
			return traverse_get($varName,$this->sessionData);
		}


	}


	?>