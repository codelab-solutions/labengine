<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   BankLink integration: Krediidipank
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_BANKLINK_KREDIIDIPANK extends BASE_BANKLINK_IPIZZA
	{


		/**
		 *   Bank ID
		 *   @var string
		 *   @static
		 */

		public static $BANK_id = 'krediidipank';


		/**
		 *   Bank destination URL
		 *   @var string
		 */

		public $BANK_destination_url = 'https://i-pank.krediidipank.ee/pay';


	}


?>