<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   Field class: file upload field
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_FIELD_FILE extends BASE_FIELD
	{


		/**
		 *  Set file type
		 *  @access public
		 *  @param string $fileType File type
		 *  @return void
		 */

		public function setFileType ( $fileType )
		{
			$this->param['filetype']=$fileType;
		}


		/**
		 *   Get file type
		 *   @access public
		 *   @return string
		 *   @throws Exception
		 */

		public function getFileType()
		{
			if (!empty($this->param['filetype']))
			{
				return $this->param['filetype'];
			}
			else
			{
				if (!empty($this->dataObject))
				{
					$dataObject=&$this->dataObject;
				}
				elseif (!empty($this->formObject) && !empty($this->formObject->data))
				{
					$dataObject=$this->formObject->data;
				}
				elseif (!empty($this->listObject) && !empty($this->listObject->data))
				{
					$dataObject=$this->listObject->data;
				}
				if (!($dataObject instanceof BASE_DATAOBJECT)) throw new Exception('Could not identify data object.');
				return mb_strtolower(get_class($dataObject));
			}
		}


		/**
		 *  Set display image
		 *  @access public
		 *  @param string $imageLink URL to show image
		 *  @return void
		 */

		public function setDisplayImage ( $imageLink )
		{
			$this->param['displayimage']=$imageLink;
		}


		/**
		 *  Set multiple
		 *  @access public
		 *  @param bool Allow multiple-file upload
		 *  @return void
		 */

		public function setMultiple ( $multiple )
		{
			$this->param['multiple']=$multiple;
		}


		/**
		 *  Set allow edit
		 *  @access public
		 *  @param boolean Allow edit
		 *  @return void
		 */

		public function setAllowEdit ( $allowEdit )
		{
			$this->param['allowedit']=$allowEdit;
		}


		/**
		 *  Set allow delete
		 *  @access public
		 *  @param boolean Allow delete
		 *  @return void
		 */

		public function setAllowDelete ( $allowDelete )
		{
			$this->param['allowdelete']=$allowDelete;
		}


		/**
		 *  Data validate
		 *  @access public
		 *  @return boolean
		 */

		public function validate( $value )
		{
			// Required and empty?
			if ($this->required())
			{
				if ($this->param['multiple'])
				{
					if (!intval($_FILES[$this->tag]['size'][0])) return '[BASE.FORM.Error.RequiredFieldEmpty]';
				}
				else
				{
					if (!intval($_FILES[$this->tag]['size'])) return '[BASE.FORM.Error.RequiredFieldEmpty]';
				}
				if (!empty($_POST[$this->tag.'_delete'])) return '[BASE.FORM.Error.RequiredFieldEmpty]';
			}
			if ($this->formObject->operation=='edit' && $this->param['required']=='add' && !empty($_POST[$this->tag.'_delete']))
			{
				return '[BASE.FORM.Error.RequiredFieldEmpty]';
			}

			// No point in checking further if empty
			if (empty($_FILES[$this->tag]['size'])) return;

			// Filter
			if (!empty($this->param['filter']))
			{
				$allowedFileTypes=str_array($this->param['filter']);
				if ($this->param['multiple'])
				{
					foreach ($_FILES[$this->tag]['type'] as $f => $ftype)
					{
						$allowed=FALSE;
						foreach ($allowedFileTypes as $aft)
						{
							if (preg_match('/^'.str_replace('/','\/',$aft).'$/',$ftype)) $allowed=TRUE;
						}
						if (!$allowed) return $_FILES[$this->tag]['name'][$f].': [BASE.FORM.ERROR.FileTypeNotAllowed]';
					}
				}
				else
				{
					$allowed=FALSE;
					foreach ($allowedFileTypes as $aft)
					{
						if (preg_match('/^'.str_replace('/','\/',$aft).'$/',$_FILES[$this->tag]['type'])) $allowed=TRUE;
					}
					if (!$allowed) return '[BASE.FORM.ERROR.FileTypeNotAllowed]';
				}
			}

			// We are uploading a file
			if (is_object($this->formObject))
			{
				$this->formObject->fileUpload=TRUE;
			}

			// Validates
			return '';
		}


		/**
		 *  Save trigger
		 *  @access public
		 *  @return mixed value
		 */

		public function triggerFormSave()
		{
			global $LAB;

			// Delete
			if (!empty($_POST[$this->tag.'_delete']))
			{
				BASE_FILE::deleteFile($this->formObject->data->_oid,$this->getFileType());
				return;
			}

			// Checks
			if ($this->param['multiple']) return;
			if (BASE_FILE::dbStorage()) return;
			if (empty($_FILES[$this->tag]['tmp_name'])) return;

			// Delete previous files
			if (is_object($this->formObject) && $this->formObject->operation=='edit')
			{
				BASE_FILE::deleteFile($this->formObject->data->_oid,$this->getFileType());
			}

			// Save file
			BASE_FILE::moveUploadedFile($_FILES[$this->tag]['tmp_name'],$this->formObject->data->_oid,$this->getFileType());
		}


		/**
		 *  Save value
		 *  @access public
		 *  @return mixed value
		 */

		public function saveValue()
		{
			// File upload hack for IE
			if (is_object($this->formObject))
			{
				$this->formObject->fileUpload=TRUE;
			}

			// Init
			$retval=array();

			// Delete?
			if (!empty($_POST[$this->tag.'_delete']))
			{
				if (BASE_FILE::dbStorage())
				{
					$retval[$this->tag]='';
				}
				$retval[$this->tag.'_ctype']='';
				$retval[$this->tag.'_fname']='';
				$retval[$this->tag.'_fsize']='';
				return $retval;
			}

			// No file
			if (!intval($_FILES[$this->tag]['size'])) return FALSE;

			// Save value
			if (BASE_FILE::dbStorage())
			{
				$retval[$this->tag]=file_get_contents($_FILES[$this->tag]['tmp_name']);
			}
			$retval[$this->tag.'_ctype']=$_FILES[$this->tag]['type'];
			$retval[$this->tag.'_fname']=$_FILES[$this->tag]['name'];
			$retval[$this->tag.'_fsize']=$_FILES[$this->tag]['size'];

			// Return
			return $retval;
		}


		/**
		 *  Log data
		 *  @access public
		 *  @return boolean
		 */

		public function getLogData( $dataObject, $operation, &$logValuesArray )
		{
			unset($logValuesArray[$this->tag]);
			unset($logValuesArray[$this->tag.'_ctype']);
			unset($logValuesArray[$this->tag.'_fname']);
			unset($logValuesArray[$this->tag.'_fsize']);

			if ($operation!='edit' && !intval($dataObject->{$this->tag.'_fsize'})) return;
			if ($operation=='edit')
			{
				if ($dataObject->{$this->tag}==$dataObject->_in[$this->tag]) return;
				if ($dataObject->{$this->tag}=='' && $dataObject->_in[$this->tag]=='0') return;
				if ($dataObject->{$this->tag}=='0' && $dataObject->_in[$this->tag]=='') return;
			}

			// Log
			$XML='<fld id="'.htmlspecialchars($this->tag).'" name="'.htmlspecialchars($this->param['title']).'">';
			if ($operation!='add' && !$this->param['forcevalue']) $XML.='<old>'.htmlspecialchars($dataObject->_in[$this->tag.'_fname']).' ('.BASE_FILE::niceFileSize($dataObject->_in[$this->tag.'_fsize']).')'.'</old>';
			$XML.='<new>'.htmlspecialchars($dataObject->{$this->tag.'_fname'}).' ('.BASE_FILE::niceFileSize($dataObject->{$this->tag.'_fsize'}).')'.'</new>';
			$XML.='</fld>';
			return $XML;
		}


		/**
		 *   Display the field
		 *   @access public
		 *   @param $value Field value
		 *   @param $row Row number (for multi-row fields)
		 *   @return string contents
		 */

		public function displayField( $value, $row=NULL )
		{
			// File upload hack for IE
			if (is_object($this->formObject))
			{
				$this->formObject->fileUpload=TRUE;
			}

			// Style
			$style=array();
			if (!empty($this->param['fieldstyle'])) $style[]=$this->param['fieldstyle'];
			if (!empty($this->param['comment'])) $style[]='width: 70%; display: inline-block';

			// Class
			$class=array();
			if (!empty($this->param['fieldclass'])) $class[]=$this->param['fieldclass'];

			// Render
			$c='<div class="'.$this->getContentWidthClass().'">';

			// Current file
			if ($this->formObject->operation=='edit' && !empty($this->formObject->data->{$this->tag.'_fname'}))
			{
				// Image
				if (!empty($this->param['displayimage']))
				{
					$c.='<div id="'.$this->tag.'_displayimage" style="border-bottom: 1px #e0e0e0 solid; margin-bottom: 6px; padding-bottom: 6px">';
					$url=str_replace('$id',$_POST[$this->formObject->data->_param['idfield']],$this->param['displayimage']);
					$c.='<img src="'.$url.'" border="1">';
					$c.='</div>';
				}

				// File info + delete
				if (!empty($this->param['allowdelete']))
				{
					$c.='<input type="hidden" id="'.$this->tag.'_delete" name="'.$this->tag.'_delete" value="">';
				}
				$c.='<div id="'.$this->tag.'_fileinfo" style="position: relative;'.($this->param['allowedit']?'border-bottom: 1px #e8e8e8 solid; margin-bottom: 3px; padding-bottom: 6px;':'').'">';
					$c.='<table style="width: 100%">';
					$c.='<tr>';

					// File
					$c.='<td style="vertical-align: top; width: 99%">';
					$c.=$this->formObject->data->{$this->tag.'_fname'}.' ('.BASE_FILE::niceFileSize($this->formObject->data->{$this->tag.'_fsize'}).')';
					$c.='</td>';

					// Delete
					if (!empty($this->param['allowdelete']))
					{
						$c.='<td style="width: 1%: padding-left: 10px">';
							$c.='<a onclick="FORM.deleteFile(\''.$this->tag.'\')"><span class="icon-delete"></span></a>';
						$c.='</td>';
					}

					$c.='</tr>';
					$c.='</table>';
				$c.='</div>';
			}

			// Field
			if ($this->formObject->operation=='add' || $this->param['allowedit'])
			{
				$c.='<p class="form-control-static">';
				$c.='<input type="file"';
				$c.=' id="'.$this->tag.'"';
				$c.=' name="'.$this->tag.($this->param['multiple']?'[]':'').'"';
				if ($this->param['multiple']) $c.=' multiple="multiple"';
				if (!empty($class)) $c.=' class="'.join(' ',$class).'"';
				if (!empty($style)) $c.=' style="'.join('; ',$style).'"';
				if (!empty($this->param['readonly'])) $c.=' readonly="readonly"';
				if (!empty($this->param['disabled'])) $c.=' disabled="disabled"';
				if (!empty($this->param['keephiddenvalue'])) $c.=' data-keephiddenvalue="1"';
				if (isset($this->param['event']) && is_array($this->param['event']))
				{
					foreach ($this->param['event'] as $eventType => $eventContent) $c.=' '.$eventType.'="'.$eventContent.'"';
				}
				if (isset($this->param['data']) && is_array($this->param['data']))
				{
					foreach ($this->param['data'] as $dataKey => $dataValue) $c.=' data-'.$dataKey.'="'.htmlspecialchars($dataValue).'"';
				}
				$c.='>';
				$c.=$this->displayComment();
				$c.='</p>';
			}

			// Field ends
			$c.='</div>';

			// Return
			return $c;
		}





	}


?>