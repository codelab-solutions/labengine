<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   Field class: e-mail field
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_FIELD_EMAIL extends BASE_FIELD_TEXT
	{


		/**
		 *   Allow multiple e-mails?
		 *   @access public
		 *   @param bool $allowMultiple Allow multiple e-mails
		 *   @return void
		 */

		public function setAllowMultiple ( $allowMultiple )
		{
			$this->param['allowmultiple']=$allowMultiple;
		}


		/**
		 *   Set list values as clickable links?
		 *   @access public
		 *   @param bool $listClickable List values are clickable
		 *   @return void
		 */

		public function setListClickable( $listClickable )
		{
			$this->param['list_clickable']=$listClickable;
		}


		/**
		 *   Display value
		 *   @access public
		 *   @return mixed value
		 */

		public function displayValue()
		{
			global $LAB;
			$value=str_array($this->getValue());
			if (!empty($this->param['list_clickable']))
			{
				foreach ($value as $k => $v)
				{
					$value[$k]='<a href="mailto:'.$v.'">'.$v.'</a>';
				}
			}

			return join(', ',$value);
		}


		/**
		 *   Data validate
		 *   @access public
		 *   @return boolean
		 */

		public function validate( $value )
		{
			$value=trim($value);

			// Validate parent
			$err=parent::validate($value);
			if (!empty($err)) return $err;

			// No point in checking further if empty
			if (!mb_strlen($value)) return '';

			// Validate e-mail
			if (!empty($this->param['allowmultiple']))
			{
				$emailArr=str_array($value);
				foreach ($emailArr as $email)
				{
					if (!preg_match('/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/',$email))
					{
						return '[BASE.FORM.ERROR.InvalidEmail]: '.$email;
					}
				}
			}
			else
			{
				if (!preg_match('/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/',$value))
				{
					return '[BASE.FORM.ERROR.InvalidEmail]';
				}
			}
		}


	}


?>