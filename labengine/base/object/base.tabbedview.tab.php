<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   The tabbed view tab class
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_TABBEDVIEW_TAB
	{

		public $tag = NULL;
		public $viewObject = NULL;

		public $tabTitle = '';
		public $tabIcon = '';
		public $tabHandlerFunction = NULL;
		public $tabHandlerFunctionParam = NULL;
		public $tabSelectTrigger = NULL;

		public function setTitle ( $title )
		{
			$this->tabTitle=$title;
		}

		public function setIcon ( $icon )
		{
			$this->tabIcon=$icon;
		}

		public function setHandlerFunction ( $handlerFunction, $functionParam=false )
		{
			$this->tabHandlerFunction=$handlerFunction;
			if ($functionParam!==false)
			{
				$this->tabHandlerFunctionParam=$functionParam;
			}
		}

		public function setSelectTrigger ( $trigger )
		{
			$this->tabSelectTrigger=$trigger;
		}

	}


?>