<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   Field class: dropdown field
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_FIELD_DROPDOWN extends BASE_FIELD_OPTIONSFIELD
	{


		/**
		 *  Set select
		 *  @access public
		 *  @param string $select select value
		 *  @return void
		 */

		public function setSelect ( $select )
		{
			$this->param['select']=$select;
		}


		/**
		 *  Set grouping
		 *  @access public
		 *  @param bool $grouping Option grouping on
		 *  @return void
		 */

		public function setGrouping ( $grouping )
		{
			$this->param['optgroup']=$grouping;
		}


		/**
		 *  Set additional option data
		 *  @access public
		 *  @param array $optionData option data
		 *  @return void
		 */

		public function setOptionData ( $optionData )
		{
			$this->param['optiondata']=$optionData;
		}


		/**
		 *  Display the field
		 *  @access public
		 *  @return string contents
		 */

		public function displayField( $value, $row=NULL )
		{
			// Get data
			$option=$this->getOptions();

			// Style
			$style=array();
			if (!empty($this->param['fieldstyle'])) $style[]=$this->param['fieldstyle'];
			if (!empty($this->param['comment'])) $style[]='width: 70%; display: inline-block';

			// Class
			$class=array();
			$class[]='form-control';
			if (!empty($this->param['fieldclass'])) $class[]=$this->param['fieldclass'];

			// Autocomplete
			if (!empty($this->param['autocomplete']))
			{
				$this->param['fieldclass']=(!empty($this->param['fieldclass'])?$this->param['fieldclass'].' ':'').'autocomplete';
			}

			// Field begins
			$c='<div class="'.$this->getContentWidthClass().'">';

				$c.='<select';
				$c.=' id="'.$this->tag.'"';
				$c.=' name="'.$this->tag.'"';
				$c.=' class="'.join(' ',$class).'"';
				$c.=' data-originalvalue="'.htmlspecialchars($value).'"';
				if (!empty($style)) $c.=' style="'.join('; ',$style).'"';
				if (!empty($this->param['disabled'])) $c.=' disabled="disabled"';
				if (!empty($this->param['keephiddenvalue'])) $c.=' data-keephiddenvalue="1"';
				if (isset($this->param['event']) && is_array($this->param['event']))
				{
					foreach ($this->param['event'] as $eventType => $eventContent) $c.=' '.$eventType.'="'.$eventContent.'"';
				}
				if (isset($this->param['data']) && is_array($this->param['data']))
				{
					foreach ($this->param['data'] as $dataKey => $dataValue) $c.=' data-'.$dataKey.'="'.htmlspecialchars($dataValue).'"';
				}
				$c.='>';

				// Default
				if (!empty($this->param['select'])) $c.='<option value="">--- '.$this->param['select'].' ---</option>';

				// Display options
				if (is_array($option))
				{
					if (!empty($this->param['optgroup']))
					{
						foreach ($option as $optgroup_name => $optgroup_contents)
						{
							$c.='<optgroup label="'.htmlspecialchars($optgroup_name).'">';
							foreach ($optgroup_contents as $o_val => $o_desc)
							{
								$c.='<option';
								$c.=' value="'.$o_val.'"';
								$c.=($o_val==$value?' selected="selected"':'');
								if (isset($this->param['optiondata'][$o_val]))
								{
									foreach ($this->param['optiondata'][$o_val] as $k => $v)
									{
										$c.=' data-'.$k.'="'.htmlspecialchars($v).'"';
									}
								}
								$c.='>';
								$c.=htmlspecialchars($o_desc);
								$c.='</option>';
							}
							$c.='</optgroup>';
						}
					}
					else
					{
						foreach ($option as $o_val => $o_desc)
						{
							$c.='<option';
							$c.=' value="'.$o_val.'"';
							$c.=($o_val==$value?' selected="selected"':'');
							if (isset($this->param['optiondata'][$o_val]))
							{
								foreach ($this->param['optiondata'][$o_val] as $k => $v)
								{
									$c.=' data-'.$k.'="'.htmlspecialchars($v).'"';
								}
							}
							$c.='>';
							$c.=htmlspecialchars($o_desc);
							$c.='</option>';
						}
					}
				}

				// Field ends
				$c.='</select>';

				// Comment
				$c.=$this->displayComment();

			$c.='</div>';

			// Return
			return $c;
		}


	}


?>