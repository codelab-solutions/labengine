<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   The daterange list filter class
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_LIST_FILTER_DATERANGE extends BASE_LIST_FILTER
	{


		/**
		 *   Filterable column is a dDatetime field (default: date)
		 */

		public $fieldDateTime = false;


		/**
		 *  Set field type
		 *  @access public
		 *  @return void
		 */

		public function setFieldDateTime ( $fieldDateTime )
		{
			$this->fieldDateTime=$fieldDateTime;
		}


		/**
		 *  Apply criteria
		 *  @access public
		 *  @param string value
		 *  @param array row
		 *  @return void
		 */

		public function getCriteria ( $value, &$paramObject )
		{
			global $LAB;

			// No value?
			if (!is_array($value) && !mb_strlen($value)) return;
			if (is_array($value) && !sizeof($value)) return;
			if (is_array($value) && !mb_strlen($value['from']) && !mb_strlen($value['to'])) return;

			// Parse
			if (mb_strlen($value['from']) && mb_strlen($value['to']))
			{
				try
				{
					$dateStart=$LAB->DATETIME->toTimestamp($value['from']);
				}
				catch (Exception $e)
				{
					$this->error='[BASE.COMMON.Error.Date.StartInvalid,'.BASE_TEMPLATE::parseContent($LAB->DATETIME->getDateFormat('disp_user')).']';
					return;
				}
				try
				{
					$dateEnd=$LAB->DATETIME->toTimestamp($value['to']);
				}
				catch (Exception $e)
				{
					$this->error='[BASE.COMMON.Error.Date.EndInvalid,'.BASE_TEMPLATE::parseContent($LAB->DATETIME->getDateFormat('disp_user')).']';
					return;
				}
				if ($dateEnd<$dateStart)
				{
					$this->error='[BASE.COMMON.Error.Date.EndEarlierThanStart]';
					return;
				}
				if ($dateStart==$dateEnd)
				{
					if ($this->fieldDateTime)
					{
						$cValue=" between '".date('Y-m-d 0000:00:00',$dateStart)."' and '".date('Y-m-d 23:59:59',$dateStart)."'";
					}
					else
					{
						$cValue="='".date('Y-m-d',($this->fieldDateTime?strtotime('+1day',$dateEnd):$dateStart))."'";
					}
				}
				else
				{
					$cValue=" between '".date('Y-m-d',$dateStart)."' and '".date('Y-m-d',($this->fieldDateTime?strtotime('+1day',$dateEnd):$dateEnd))."'";
				}
			}
			elseif (mb_strlen($value['from']))
			{
				try
				{
					$dateStart=$LAB->DATETIME->toTimestamp($value['from']);
				}
				catch (Exception $e)
				{
					$this->error='[BASE.COMMON.Error.Date.StartInvalid,'.BASE_TEMPLATE::parseContent($LAB->DATETIME->getDateFormat('disp_user')).']';
					return;
				}
				$cValue=">='".date(($this->fieldDateTime?'Y-m-d 00:00:00':'Y-m-d'),$dateStart)."'";
			}
			elseif (mb_strlen($value['to']))
			{
				try
				{
					$dateEnd=$LAB->DATETIME->toTimestamp($value['to']);
				}
				catch (Exception $e)
				{
					$this->error='[BASE.COMMON.Error.Date.EndInvalid,'.BASE_TEMPLATE::parseContent($LAB->DATETIME->getDateFormat('disp_user')).']';
					return;
				}
				$cValue="<='".date(($this->fieldDateTime?'Y-m-d 23:59:59':'Y-m-d'),$dateEnd)."'";
			}
			else
			{
				$this->error='[BASE.COMMON.Error.Date.Invalid.Range,'.BASE_TEMPLATE::parseContent($LAB->DATETIME->getDateFormat('disp_user')).']';
				return;
			}

			$this->applyCriteria($cValue, $paramObject);
		}


		/**
		 *  Display field
		 *  @access public
		 *  @return string contents
		 */

		public function displayField( $value, $row=NULL )
		{
			global $LAB;

			// Value
			$value=$this->getValue();

			// Render
			$c='<div id="filter_'.$this->tag.'"><table style="width: 100%; border: none !important;"><tr>';

				$c.='<td style="border: none !important; padding: 0 !important; width: 49%; white-space: nowrap">';
				$c.='<input type="text" class="form-control datefield" style="text-align: center" autocomplete="off" id="filter_'.$this->tag.'_from" name="filter_'.$this->tag.'[from]" value="'.$value['from'].'" maxlength="10" data-date-format="'.$LAB->DATETIME->getDateFormat('datepicker').'" data-mask="'.$LAB->DATETIME->getDateFormat('mask').'">';
				$c.='</td>';
				$c.='<td style="border: none !important; padding: 0px 0px 0px 5px !important; width: 1%; white-space: nowrap">';
				$c.='<span style="display: inline-block; cursor: pointer" class="icon-calendar" onclick="FORM.openDatepicker(\'#filter_'.$this->tag.'_from\')"></span>';
				$c.='</td>';

				$c.='<td style="border: none !important; padding: 0px 10px !important; width: 2%; white-space: nowrap">--</td>';

				$c.='<td style="border: none !important; padding: 0 !important; width: 49%; white-space: nowrap">';
				$c.='<input type="text" class="form-control datefield" style="text-align: center" autocomplete="off" id="filter_'.$this->tag.'_to" name="filter_'.$this->tag.'[to]" value="'.$value['to'].'" maxlength="10" data-date-format="'.$LAB->DATETIME->getDateFormat('datepicker').'" data-mask="'.$LAB->DATETIME->getDateFormat('mask').'">';
				$c.='</td>';
				$c.='<td style="border: none !important; padding: 0px 0px 0px 5px !important; width: 1%; white-space: nowrap">';
				$c.='<span style="display: inline-block; cursor: pointer" class="icon-calendar" onclick="FORM.openDatepicker(\'#filter_'.$this->tag.'_to\')"></span>';
				$c.='</td>';

			$c.='</tr></table></div>';
			$c.='<script language="JavaScript"> $(function(){ FORM.initElements(\'#filter_'.$this->tag.'\'); }); </script>';
			return $c;
		}

	}


?>