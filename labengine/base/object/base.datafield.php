<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   The base data field class
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_DATAFIELD
	{


		/**
		 *   Field name
		 *   @var string
		 *   @access public
		 */
		public $fieldName = NULL;

		/**
		 *   Parameters
		 *   @var array
		 *   @access public
		 */
		public $param = NULL;

		/**
		 *   Backreference to form object
		 *   @var BASE_FORM
		 *   @access public
		 */
		public $formObject = NULL;

		/**
		 *   Backreference to list object
		 *   @var BASE_LIST
		 *   @access public
		 */
		public $listObject = NULL;


	}


?>