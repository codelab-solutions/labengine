<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   Field class: text field
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_FIELD_DISPLAY_LOOKUP extends BASE_FIELD
	{


		/**
		 *   List value
		 *   @access public
		 *   @return mixed value
		 */

		public function listValue( $value, &$row )
		{
			$options=$this->getOptions();
			return htmlspecialchars($options[$value]);
		}


		/**
		 *  Display the field
		 *  @access public
		 *  @return string contents
		 */

		public function displayField( $value, $row=NULL )
		{
			// Get data
			$options=$this->getOptions();

			// Render
			$c='<div class="'.$this->getContentWidthClass().'">';
			$c.='<p class="form-control-static">';

				$c.='<input type="hidden"';
				$c.=' id="'.$this->tag.'"';
				$c.=' name="'.$this->tag.'"';
				$c.=' value="'.htmlspecialchars($value).'"';
				$c.='>';

				$c.=$options[$value];

			$c.='</p>';
			$c.='</div>';

			// Return
			return $c;
		}


	}


?>