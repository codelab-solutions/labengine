<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   Timer class
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_TIMER
	{


		/**
		 *   Start timer
		 *   @access public
		 *   @static
		 *   @return string Timer ID
		 */

		public static function startTimer()
		{
			global $LAB;
			$timerID=uniqid();
			$LAB->CACHE->timer[$timerID]=microtime_float();
			return $timerID;
		}


		/**
		 *   Stop timer
		 *   @access public
		 *   @static
		 *   @param string $timerID Timer ID
		 *   @throws Exception
		 *   @return float Time in seconds
		 */

		public static function stopTimer( $timerID )
		{
			global $LAB;
			if (empty($LAB->CACHE->timer[$timerID])) throw new Exception('Unknown timer: '.$timerID);
			$timeEnd=microtime_float();
			$time=$timeEnd-$LAB->CACHE->timer[$timerID];
			unset ($LAB->CACHE->timer[$timerID]);
			return $time;
		}


	}


?>