<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   The log class
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_LOG extends BASE_DATAOBJECT
	{


		/**
		 *   Init data object
		 *   @access public
		 *   @throws Exception
		 *   @return void
		 */

		public function initDataObject()
		{
			// Main fields
			$this->_param['table']            = 'base_log';
			$this->_param['objectname']       = 'Log entry';
			$this->_param['idfield']          = NULL;
			$this->_param['descriptionfield'] = NULL;
			$this->_param['setord']           = FALSE;
			$this->_param['prop']             = FALSE;
			$this->_param['modfields']        = FALSE;
		}


		/**
		 *   Load data object by OID
		 *   @static
		 *   @access public
		 *   @param int $refOID Reference OID
		 *   @param string $logEntry Log entry
		 *   @param string $logData Detailed log data
		 *   @param bool|int $affectedOID Affected element OID (false=none)
		 *   @param bool|int $userOID User OID (false=current user)
		 *   @return bool|void
		 *   @throws Exception
		 */

		public static function logEntry( $refOID, $logEntry, $logData='', $affectedOID=false, $userOID=false, $timestamp=false )
		{
			global $LAB;
			try
			{
				$refOID=str_array($refOID,',');
				foreach ($refOID as $r)
				{
					$cols=array();
					$cols['ref_oid']=$r;
					$cols['affected_oid']=($affectedOID!==false?$affectedOID:'0');
					$cols['user_oid']=($userOID!==false?$userOID:($LAB->USER->isLoggedIn()?$LAB->USER->{$LAB->USER->_param['idfield']}:'0'));
					$cols['log_tstamp']=($timestamp!==false?$timestamp:date('Y-m-d H:i:s'));
					$cols['log_entry']=$logEntry;
					$cols['log_data']=(is_array($logData)?join("\n",$logData):$logData);
					$LAB->DB->queryInsert('base_log',$cols);
				}
			}
			catch (Exception $e)
			{
				return false;
			}
		}


		/**
		 *   Get log data as HTML
		 *   @static
		 *   @access public
		 *   @param string $logData Detailed log data
		 *   @return string
		 */

		public static function getLogDataHtml( $logData )
		{
			global $LAB;
			try
			{
				if (substr($logData,0,8)=='<logdata')
				{
					$c='';
					$XML=new SimpleXMLElement($logData);
					if ($LAB->DEBUG->devEnvironment && strlen($XML->attributes()->op)) $c.='<div style="border-bottom: 1px #b0b0b0 solid; padding-bottom: 4px; margin-bottom: 4px"><b>Operation:</b> '.strval($XML->attributes()->op).'</div>';
					foreach ($XML->fld as $FLD)
					{
						$c.='<b>'.htmlspecialchars(strlen($FLD->attributes()->name)?$FLD->attributes()->name:$FLD->attributes()->id).':</b> ';
						if (isset($FLD->new))
						{
							if (isset($FLD->old))
							{
								if (strlen($FLD->old))
								{
									$c.=BASE_LOG::getLogDataHtmlFieldValue($FLD->old);
								}
								else
								{
									$c.='<i style="color: #808080">[BASE.COMMON.Empty]</i>';
								}
								$c.=' &raquo; ';
							}
							if (strlen($FLD->new))
							{
								$c.=BASE_LOG::getLogDataHtmlFieldValue($FLD->new);
							}
							else
							{
								$c.='<i style="color: #808080">[BASE.COMMON.Empty]</i>';
							}
						}
						elseif (isset($FLD->value) && strlen($FLD->value))
						{
							$c.=BASE_LOG::getLogDataHtmlFieldValue($FLD->value);
						}
						else
						{
							$c.='<i style="color: #808080">[BASE.COMMON.Empty]</i>';
						}
						$c.='<br/>';
					}
					return BASE_TEMPLATE::parseContent($c);
				}
				else
				{
					return nl2br(htmlspecialchars(BASE_TEMPLATE::parseContent($logData)));
				}
			}
			catch (Exception $e)
			{
				return '<div style="color: #920000">'.htmlspecialchars($e->getMessage()).'</div>';
			}
		}


		/**
		 *   Helper func: display log value
		 *   @static
		 *   @access public
		 *   @param SimpleXMLElement $FLD XML field object
		 *   @return string
		 */

		public static function getLogDataHtmlFieldValue( &$FLD )
		{
			if (strlen($FLD->attributes()->value) && strval($FLD->attributes()->value)!=strval($FLD))
			{
				return strval($FLD).' ['.strval($FLD->attributes()->value).']';
			}
			else
			{
				return strval($FLD);
			}
		}


	}


?>