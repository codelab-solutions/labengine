<?php


	/**
	 *
	 *   FlaskPHP-Identity-EstEID
	 *   ------------------------
	 *   Mobile ID signing response
	 *
	 *   @author   Codelab Solutions OÜ <codelab@codelab.ee>
	 *   @license  https://www.flaskphp.com/LICENSE MIT
	 *
	 */


	class BASE_ESTEID_SIGNRESPONSE
	{


		/**
		 *   Response status
		 *   @var string
		 *     pending  -  request pending
		 *     success  -  successfully authenticated
		 *     error    -  error
		 */

		public $status = null;


		/**
		 *   Response session code
		 *   @var int
		 */

		public $sessCode = null;


		/**
		 *   Prepared signature ID
		 *   @var string
		 */

		public $signatureID = null;


		/**
		 *   Prepared signature digest
		 *   @var string
		 */

		public $signedInfoDigest = null;


		/**
		 *   Prepared signature hash type
		 *   @var string
		 */

		public $hashType = null;


		/**
		 *   Signed document info
		 *   @var object
		 */

		public $signedDocInfo = null;


		/**
		 *   Error message
		 *   @var string
		 *   @access public
		 */

		public $error = null;

		/**
		 * @var string
		 */

		public $filename;


		/**
		 * @var string
		 */

		public $bdocfilename;


		/**
		 *
		 *   Get status
		 *   ----------
		 *   @access public
		 *   return string
		 *
		 */

		public function getStatus()
		{
			return $this->status;
		}


	}


?>