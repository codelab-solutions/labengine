<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   DigiDoc helper functions: CDOC
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_DIGIDOC_CDOC extends BASE_DIGIDOC
	{


		/**
		 *   Parse file
		 *   @access public
		 *   @var string $filename
		 *   @throws Exception
		 *   @return void
		 */

		public function parse( $filename )
		{
			global $LAB;

			// Read file contents
			$docSource=file_get_contents($filename);
			if (!mb_strlen($docSource)) throw new Exception('Empty file.');

			// Parse XML
			try
			{
				$CDOC=new DOMDocument();
				$CDOC->load($filename);
				$XPATH=new DOMXPath($CDOC);
				$XPATH->registerNamespace('denc','http://www.w3.org/2001/04/xmlenc#');
				$XPATH->registerNamespace('ds','http://www.w3.org/2000/09/xmldsig#"');
			}
			catch (Exception $e)
			{
				if ($LAB->DEBUG->devEnvironment) throw new Exception('Error parsing cdoc XML: '.$e->getMessage());
				throw new Exception('Error reading .cdoc format.');
			}

			// Get document format
			$documentFormatList=$XPATH->query('/denc:EncryptedData/denc:EncryptionProperties/denc:EncryptionProperty[@Name="DocumentFormat"]');
			foreach ($documentFormatList as $documentFormat)
			{
				$this->info['format']=strval($documentFormat->nodeValue);
			}

			// Get document format
			$documentFormatList=$XPATH->query('/denc:EncryptedData/denc:EncryptionProperties/denc:EncryptionProperty[@Name="LibraryVersion"]');
			foreach ($documentFormatList as $documentFormat)
			{
				$this->info['library_version']=strval($documentFormat->nodeValue);
			}

			// Get files
			$fileList=$XPATH->query('/denc:EncryptedData/denc:EncryptionProperties/denc:EncryptionProperty[@Name="orig_file"]');
			foreach ($fileList as $file)
			{
				$xmlFileData=str_array(strval($file->nodeValue),'|');
				$fileData=new BASE_DIGIDOC_FILE();
				$fileData->id=$xmlFileData['3'];
				$fileData->filename=$xmlFileData['0'];
				$fileDatasize=intval($xmlFileData['1']);
				if ($xmlFileData['2']=='application/octet-stream')
				{
					$fileData->contenttype=BASE::getMimeType($fileData->filename);
				}
				else
				{
					$fileData->contenttype=$xmlFileData['2'];
				}
				$fileData->readable=false;
				$this->files[]=$fileData;
			}

			// Get keys encrypted for
			$x509KeyList=$XPATH->query('/denc:EncryptedData/*/denc:EncryptedKey/*/*/*');
			foreach ($x509KeyList as $x509Key)
			{
				if (preg_match("/X509Certificate$/",$x509Key->nodeName))
				{
					$certificateData=openssl_x509_parse('-----BEGIN CERTIFICATE-----'."\n".trim(strval($x509Key->nodeValue))."\n".'-----END CERTIFICATE-----');
					if ($certificateData!==false && is_array($certificateData))
					{
						$keyData=array();
						$keyData['certificate']=$certificateData['name'];
						$keyData['country']=$certificateData['subject']['C'];
						$keyData['name_last']=$certificateData['subject']['SN'];
						$keyData['name_first']=$certificateData['subject']['GN'];
						$keyData['idcode']=$certificateData['subject']['serialNumber'];
						$this->keys[]=$keyData;
					}
				}
			}
		}


	}


?>