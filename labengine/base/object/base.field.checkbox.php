<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   Field class: checkbox field
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_FIELD_CHECKBOX extends BASE_FIELD
	{


		/**
		 *  Set checkbox title
		 *  @access public
		 *  @param string comment
		 *  @return void
		 */

		public function setCheckboxTitle ( $title )
		{
			$this->param['checkbox_title']=$title;
		}


		/**
		 *  Set checkbox empty title (for display)
		 *  @access public
		 *  @param string value
		 *  @return void
		 */

		public function setCheckboxEmptyTitle ( $title )
		{
			$this->param['checkbox_emptytitle']=$title;
		}


		/**
		 *  Set checkbox value
		 *  @access public
		 *  @param string value
		 *  @return void
		 */

		public function setCheckboxValue ( $value )
		{
			$this->param['checkbox_value']=$value;
		}


		/**
		 *  Set checkbox checked
		 *  @access public
		 *  @param string comment
		 *  @return void
		 */

		public function setChecked ( $value )
		{
			$this->param['checked']=$value;
		}


		/**
		 *  Log data
		 *  @access public
		 *  @return boolean
		 */

		public function getLogData( $dataObject, $operation, &$logValuesArray )
		{
			unset($logValuesArray[$this->tag]);

			if ($operation!='edit' && !mb_strlen($dataObject->{$this->tag})) return;
			if ($operation=='edit')
			{
				if ($dataObject->{$this->tag}==$dataObject->_in[$this->tag]) return;
				if ($dataObject->{$this->tag}=='' && $dataObject->_in[$this->tag]=='0') return;
				if ($dataObject->{$this->tag}=='0' && $dataObject->_in[$this->tag]=='') return;
			}

			// Params
			$checkboxValue=oneof($this->param['checkbox_value'],'1');
			$checkboxTitle=oneof($this->param['checkbox_title'],'[BASE.COMMON.Yes|tolower]');

			// Log
			$XML='<fld id="'.htmlspecialchars($this->tag).'" name="'.htmlspecialchars($this->param['title']).'">';
			if ($operation!='add' && !$this->param['forcevalue']) $XML.='<old value="'.htmlspecialchars($dataObject->_in[$this->tag]).'">'.htmlspecialchars($dataObject->_in[$this->tag]==$checkboxValue?$checkboxTitle:'--').'</old>';
			$XML.='<new value="'.htmlspecialchars($dataObject->{$this->tag}).'">'.htmlspecialchars($dataObject->{$this->tag}==$checkboxValue?$checkboxTitle:'--').'</new>';
			$XML.='</fld>';
			return $XML;
		}


		/**
		 *  Display the field
		 *  @access public
		 *  @return string contents
		 */

		public function displayField( $value, $row=NULL )
		{
			// Params
			$checkboxValue=oneof($this->param['checkbox_value'],'1');
			$checkboxTitle=oneof($this->param['checkbox_title'],'[BASE.COMMON.Yes|tolower]');
			$isChecked=oneof($this->param['checked'],false);

			// Style
			$style=array();
			if (!empty($this->param['fieldstyle'])) $style[]=$this->param['fieldstyle'];

			// Class
			$class=array();
			$class[]='form-control';
			if (!empty($this->param['fieldclass'])) $class[]=$this->param['fieldclass'];

			// Render
			$c='<div class="'.$this->getContentWidthClass().'">';
			$c.='<div class="checkbox">';

				// The checkbox itself
				$c.='<label for="'.$this->tag.'"><input type="checkbox"';
				$c.=' id="'.$this->tag.'"';
				$c.=' name="'.$this->tag.'"';
				$c.=' value="'.$checkboxValue.'"';
				if ($value==$checkboxValue||$isChecked) $c.=' checked="checked"';
				if (!empty($style)) $c.=' style="'.join('; ',$style).'"';
				if (!empty($this->param['fieldclass'])) $c.=' class="'.$this->param['fieldclass'].'"';
				if (!empty($this->param['readonly'])) $c.=' readonly="readonly"';
				if (!empty($this->param['disabled'])) $c.=' disabled="disabled"';
				if (!empty($this->param['keephiddenvalue'])) $c.=' data-keephiddenvalue="1"';
				if (isset($this->param['event']) && is_array($this->param['event']))
				{
					foreach ($this->param['event'] as $eventType => $eventContent) $c.=' '.$eventType.'="'.$eventContent.'"';
				}
				if (isset($this->param['data']) && is_array($this->param['data']))
				{
					foreach ($this->param['data'] as $dataKey => $dataValue) $c.=' data-'.$dataKey.'="'.htmlspecialchars($dataValue).'"';
				}
				$c.='>';
				$c.=' '.htmlspecialchars($checkboxTitle);
				$c.='</label>';

				// Comment
				$c.=$this->displayComment();

			$c.='</div>';
			$c.='</div>';

			// Return
			return $c;
		}


		/**
		 *   Display value
		 *   @access public
		 *   @return mixed value
		 */

		public function displayValue()
		{
			$value=$this->getValue();
			$checkboxTitle=oneof($this->param['checkbox_title'],'[BASE.COMMON.Yes|tolower]');
			$checkboxEmptyTitle=oneof($this->param['checkbox_emptytitle'],'[BASE.COMMON.No|tolower]');
			return (!empty($value)?$checkboxTitle:$checkboxEmptyTitle);
		}


	}


?>