<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   BankLink integration: Swedbank
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_BANKLINK_SWED extends BASE_BANKLINK_IPIZZA
	{


		/**
		 *   Bank ID
		 *   @var string
		 *   @static
		 */

		public static $BANK_id = 'swed';


		/**
		 *   Bank destination URL
		 *   @var string
		 */

		public $BANK_destination_url = 'https://www.swedbank.ee/banklink';


	}


?>