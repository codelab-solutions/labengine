<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   The date/time i18n class
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_DATETIME
	{


		/**
		 *   Define date formats
		 */

		public static $SET_date_format = array(

			'dd.mm.yyyy' => array(
				'disp' => 'd.m.Y',
				'disp_time' => 'd.m.Y H:i',
				'disp_time_seconds' => 'd.m.Y H:i:s',
				'disp_time12' => 'd.m.Y h:iA',
				'disp_time12_seconds' => 'd.m.Y h:i:sA',
				'disp_user' => '[BASE.COMMON.DayAbbr][BASE.COMMON.DayAbbr].[BASE.COMMON.MonthAbbr][BASE.COMMON.MonthAbbr].[BASE.COMMON.YearAbbr][BASE.COMMON.YearAbbr][BASE.COMMON.YearAbbr][BASE.COMMON.YearAbbr]',
				'datepicker' => 'dd.mm.yyyy',
				'phpexcel' => 'dd.mm.yyyy',
				'mask' => '99.99.9999',
				'check' => '\d\d\.\d\d\.\d\d\d\d',
				'check_time' => '\d\d\.\d\d\.\d\d\d\d \d\d:\d\d',
				'check_time_seconds' => '\d\d\.\d\d\.\d\d\d\d \d\d:\d\d:\d\d',
				'unpack' => 'a2day/a1sep1/a2month/a1sep2/a4year',
				'unpack_time' => 'a2day/a1sep1/a2month/a1sep2/a4year/a1space/a2hour/a1sep3/a2min',
				'unpack_time_seconds' => 'a2day/a1sep1/a2month/a1sep2/a4year/a1space/a2hour/a1sep3/a2min/a1sep4/a2sec',
			),

			'dd/mm/yyyy' => array(
				'disp' => 'd/m/Y',
				'disp_time' => 'd/m/Y H:i',
				'disp_time_seconds' => 'd/m/Y H:i:s',
				'disp_time12' => 'd/m/Y h:iA',
				'disp_time12_seconds' => 'd/m/Y h:i:sA',
				'disp_user' => '[BASE.COMMON.DayAbbr][BASE.COMMON.DayAbbr]/[BASE.COMMON.MonthAbbr][BASE.COMMON.MonthAbbr]/[BASE.COMMON.YearAbbr][BASE.COMMON.YearAbbr][BASE.COMMON.YearAbbr][BASE.COMMON.YearAbbr]',
				'datepicker' => 'dd/mm/yyyy',
				'phpexcel' => 'dd/mm/yyyy',
				'mask' => '99/99/9999',
				'check' => '\d\d\/\d\d\/\d\d\d\d',
				'check_time' => '\d\d\/\d\d\/\d\d\d\d \d\d:\d\d',
				'check_time_seconds' => '\d\d\/\d\d\/\d\d\d\d \d\d:\d\d:\d\d',
				'unpack' => 'a2day/a1sep1/a2month/a1sep2/a4year',
				'unpack_time' => 'a2day/a1sep1/a2month/a1sep2/a4year/a1space/a2hour/a1sep3/a2min',
				'unpack_time_seconds' => 'a2day/a1sep1/a2month/a1sep2/a4year/a1space/a2hour/a1sep3/a2min/a1sep4/a2sec',
			),

			'mm/dd/yyyy' => array(
				'disp' => 'm/d/Y',
				'disp_time' => 'm/d/Y H:i',
				'disp_time_seconds' => 'm/d/Y H:i:s',
				'disp_time12' => 'm/d/Y h:iA',
				'disp_time12_seconds' => 'm/d/Y h:i:sA',
				'disp_user' => '[BASE.COMMON.MonthAbbr][BASE.COMMON.MonthAbbr]/[BASE.COMMON.DayAbbr][BASE.COMMON.DayAbbr]/[BASE.COMMON.YearAbbr][BASE.COMMON.YearAbbr][BASE.COMMON.YearAbbr][BASE.COMMON.YearAbbr]',
				'datepicker' => 'mm/dd/yyyy',
				'phpexcel' => 'mm/dd/yyyy',
				'mask' => '99/99/9999',
				'check' => '\d\d\/\d\d\/\d\d\d\d',
				'check_time' => '\d\d\/\d\d\/\d\d\d\d \d\d:\d\d',
				'check_time_seconds' => '\d\d\/\d\d\/\d\d\d\d \d\d:\d\d:\d\d',
				'unpack' => 'a2month/a1sep/a2day/a1sep2/a4year',
				'unpack_time' => 'a2month/a1sep1/a2day/a1sep2/a4year/a1space/a2hour/a1sep3/a2min',
				'unpack_time_seconds' => 'a2month/a1sep1/a2day/a1sep2/a4year/a1space/a2hour/a1sep3/a2min/a1sep4/a2sec',
			),

			'yyyy.mm.dd' => array(
				'disp' => 'Y.m.d',
				'disp_time' => 'Y.m.d H:i',
				'disp_time_seconds' => 'Y.m.d H:i:s',
				'disp_time12' => 'Y.m.d h:iA',
				'disp_time12_seconds' => 'Y.m.d h:i:sA',
				'disp_user' => '[BASE.COMMON.YearAbbr][BASE.COMMON.YearAbbr][BASE.COMMON.YearAbbr][BASE.COMMON.YearAbbr].[BASE.COMMON.MonthAbbr][BASE.COMMON.MonthAbbr].[BASE.COMMON.DayAbbr][BASE.COMMON.DayAbbr]',
				'datepicker' => 'yyyy.mm.dd',
				'phpexcel' => 'yyyy.mm.dd',
				'mask' => '9999.99.99',
				'check' => '\d\d\d\d\.\d\d\.\d\d',
				'check_time' => '\d\d\d\d\.\d\d\.\d\d \d\d:\d\d',
				'check_time_seconds' => '\d\d\d\d\.\d\d\.\d\d \d\d:\d\d:\d\d',
				'unpack' => 'a4year/a1sep1/a2month/a1sep2/a2day',
				'unpack_time' => 'a4year/a1sep1/a2month/a1sep2/a2day/a1space/a2hour/a1sep3/a2min',
				'unpack_time_seconds' => 'a4year/a1sep1/a2month/a1sep2/a2day/a1space/a2hour/a1sep3/a2min/a1sep4/a2sec',
			)

		);


		/**
		 *   Current timezone
		 */

		public $timeZone = FALSE;


		/**
		 *   Current format
		 */

		public $dateFormat = FALSE;


		/**
		 *   Define time formats
		 */

		public static $SET_time_format = array(

			'24' => '24h',
			'12' => '12h'

		);


		/**
		 *   Current time format
		 */

		public $timeFormat = FALSE;


		/**
		 *   Constructor: init date/time class
		 *   @access public
		 *   @return void
		 */

		public function __construct( $timeZone=FALSE, $dateFormat=FALSE )
		{
			// Set timezone
			if (!empty($timeZone))
			{
				$this->timeZone=$timeZone;
			}
			else
			{
				$this->timeZone='UTC';
			}
			date_default_timezone_set($this->timeZone);

			// Set date format
			if (!empty($dateFormat))
			{
				$this->dateFormat=$dateFormat;
			}
			else
			{
				$this->dateFormat='dd.mm.yyyy';
			}
		}



		/**
		 *  Set date format
		 *  @access public
		 *  @return void
		 */

		public function setDateFormat( $dateFormat )
		{
			$this->dateFormat=$dateFormat;
		}


		/**
		 *  Get date format
		 *  @access public
		 *  @return void
		 */

		public function getDateFormat( $formatType )
		{
			return BASE_DATETIME::$SET_date_format[$this->dateFormat][$formatType];
		}


		/**
		 *  Format date
		 *  @access public
		 *  @static
		 *  @param string|int date value
		 *  @param boolean show time (hh:mm)
		 *  @param boolean show seconds as well
		 *  @return boolean
		 */

		public function formatDate( $dateValue, $showTime=FALSE, $showTimeSeconds=FALSE, $showEmptyDate=FALSE )
		{
			global $LAB;

			if (!$showEmptyDate)
			{
				if (!mb_strlen($dateValue)) return '';
				if (is_numeric($dateValue) && empty($dateValue)) return '';
				if (!is_numeric($dateValue) && ($dateValue=='0000-00-00' || $dateValue=='0000-00-00 00:00:00')) return '';
			}

			$dateFormat=$this->getDateFormat('disp'.($showTime?'_time'.($showTimeSeconds?'_seconds':''):''));
			$formattedDate=date($dateFormat,(is_numeric($dateValue)?$dateValue:strtotime($dateValue)));
			return $formattedDate;
		}


		/**
		 *  Convert display format to YYYY-MM-DD (HH:II:SS)
		 *  @access public
		 *  @static
		 *  @param string date value in display format
		 *  @return int
		 */

		public function toYMD( $dateString )
		{
			global $LAB;

			// Parse
			$time=$seconds=FALSE;
			$dateString=trim($dateString);
			if (preg_match("/^(".$LAB->DATETIME->getDateFormat('check_time_seconds').")$/",$dateString,$match))
			{
				$time=$seconds=TRUE;
				$dateArr=unpack($LAB->DATETIME->getDateFormat('unpack_time_seconds'),$dateString);
			}
			elseif (preg_match("/^(".$LAB->DATETIME->getDateFormat('check_time').")$/",$dateString,$match))
			{
				$time=TRUE;
				$dateArr=unpack($LAB->DATETIME->getDateFormat('unpack_time'),$dateString);
			}
			elseif (preg_match("/^(".$LAB->DATETIME->getDateFormat('check').")$/",$dateString,$match))
			{
				$dateArr=unpack($LAB->DATETIME->getDateFormat('unpack'),$dateString);
			}
			else
			{
				throw new Exception('[BASE.COMMON.Error.Date.Invalid,'.BASE_TEMPLATE::parseContent($LAB->DATETIME->getDateFormat('disp_user')).']'.' -- '.$dateString);
			}

			// Check
			try
			{
				if (intval($dateArr['month'])<1 || intval($dateArr['month'])>12) throw new Exception('Invalid month');
				if (intval($dateArr['day'])<1) throw new Exception('Invalid day');
				if (intval($dateArr['month'])==2 && intval($dateArr['day'])>29) throw new Exception('Invalid day');
				if (in_array(intval($dateArr['month']),array(4,6,9,11)) && intval($dateArr['day'])>30) throw new Exception('Invalid day');
				if ($time)
				{
					if (intval($dateArr['hour'])<0 || intval($dateArr['hour'])>23) throw new Exception('Invalid hour');
					if (intval($dateArr['min'])<0 || intval($dateArr['min'])>59) throw new Exception('Invalid minute');
					if ($seconds)
					{
						if (intval($dateArr['sec'])<0 || intval($dateArr['sec'])>59) throw new Exception('Invalid second');
					}
				}
			}
			catch (Exception $e)
			{
				throw new Exception('[BASE.COMMON.Error.Date.Invalid,'.BASE_TEMPLATE::parseContent($LAB->DATETIME->getDateFormat('disp_user')).']');
			}

			// Compile
			$dateYMD=sprintf("%04d-%02d-%02d",$dateArr['year'],$dateArr['month'],$dateArr['day']);
			if ($time) $dateYMD.=' '.sprintf("%02d:%02d:%02d",$dateArr['hour'],$dateArr['min'],($seconds?$dateArr['sec']:0));

			// Return
			return $dateYMD;
		}


		/**
		 *  Convert display format to timestamp
		 *  @access public
		 *  @static
		 *  @param string date value in display format
		 *  @return int
		 */

		public function toTimestamp( $dateString )
		{
			return strtotime($this->toYMD($dateString));
		}


		/**
		 *  Validate date input
		 *  @access public
		 *  @static
		 *  @param string date value in display format
		 *  @return int
		 */

		public function validateDateInput( $dateString )
		{
			try
			{
				$dateYMD=$this->toYMD($dateString);
				return TRUE;
			}
			catch (Exception $e)
			{
				return FALSE;
			}
		}


	}


	?>