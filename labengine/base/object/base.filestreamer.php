<?


	/**
	 *
	 *   LabEngine™ 7
	 *   Standard actions: File server with streaming capabilities
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */

	class BASE_FILESTREAMER extends BASE_DISPLAYOBJECT
	{


		/*
		 *   The stream
		 */

		private $streamHandle = null;


		/*
		 *   The content
		 */

		private $streamContent = null;


		/*
		 *   The buffer size
		 */

		private $bufferSize = 102400;


		/*
		 *   Start point
		 */

		private $streamStart = -1;


		/*
		 *   End point
		 */

		private $streamEnd = -1;


		/*
		 *   Size
		 */

		private $streamSize = 0;


		/**
		 *   Set base path
		 *   @access public
		 *   @param string $basePath Base path
		 *   @return void
		 *   @throws Exception
		 */

		function setBasePath( $basePath )
		{
			$this->param['source']='file';
			$this->param['source_basepath']=$basePath;
		}


		/**
		 *   Set source DataObject
		 *   @access public
		 *   @param string|BASE_DATAOBJECT $dataObject Data object (name or reference)
		 *   @param string $fileField File field
		 *   @param string $fileField File type
		 *   @return void
		 *   @throws Exception
		 */

		function setSourceDAO( $dataObject, $fileField, $fileType=null )
		{
			$this->param['source']='dao';
			$this->param['source_dao']=$dataObject;
			$this->param['source_dao_field']=$fileField;
			if (!empty($fileType)) $this->param['source_dao_filetype']=$fileType;
		}


		/**
		 *   Init the streamer
		 *   @access public
		 *   @return string output
		 */

		function initStreamer()
		{
			// This should be implemented in the subclass.
		}


		/**
		 *   Run the display object
		 *   @access public
		 *   @return string output
		 */

		function display()
		{
			global $LAB;
			try
			{
				// Init self
				$this->initStreamer();

				// Check source
				if (empty($this->param['source'])) throw new Exception('Source not specified.');

				// DAO: get OID from request
				if ($this->param['source']=='dao')
				{
					// Check URL
					$oid=intval($LAB->REQUEST->requestVar['var_2']['name']);
					if (empty($oid)) throw new Exception('Invalid request: missing OID.');

					// Init & load data object
					$this->data=(is_object($this->param['source_dao'])?$this->param['source_dao']:new $this->param['source_dao']());
					$this->data->load($oid);
				}

				// File
				elseif ($this->param['source']=='file')
				{
					$filename=$LAB->REQUEST->requestVar['var_2']['name'];
					if (!mb_strlen($filename)) throw new Exception('Invalid request: missing filename.');
					$filename=$this->param['source_basepath'].'/'.$filename;
					if (!is_readable($filename)) throw new Exception('File does not exist or not readable.');
				}

				// Init stream from DB
				if ($this->param['source']=='dao' && BASE_FILE::dbStorage())
				{
					$this->streamContent=$this->data->{$this->param['source_dao_field']};
					$this->streamSize=strlen($this->streamContent);
					$LAB->RESPONSE->setExpires(time()+864000);
					$LAB->RESPONSE->setHeader('Last-modified',date('r',strtotime(($this->data->mod_stamp!='0000-00-00 00:00:00'?$this->data->mod_tstamp:$this->data->add_tstamp))));
				}

				// Init stream from file
				else
				{
					// DAO filename
					if ($this->param['source']=='dao')
					{
						$filename=BASE_FILE::getFileName($oid,oneof($this->param['source_dao_filetype'],mb_strtolower(get_class($this->data))));
					}

					// Open stream
					if (!($this->streamHandle=fopen($filename,'rb'))) throw new Exception('Could not open file stream for reading.');
					$this->streamSize=filesize($filename);
					$LAB->RESPONSE->setHeader('Last-Modified',date('r',@filemtime($this->path)));
				}

				// Set accept range
				$this->streamEnd=intval($this->streamSize-1);
				$LAB->RESPONSE->setHeader('Accept-Ranges','0-'.$this->streamEnd);

				// Range
				if (isset($_SERVER['HTTP_RANGE']))
				{
					$c_start = $this->streamStart;
					$c_end = $this->streamEnd;

					list(,$range)=explode('=',$_SERVER['HTTP_RANGE'],2);
					if (strpos($range,',')!==false)
					{
						$LAB->RESPONSE->setStatus(416);
						$LAB->RESPONSE->setHeader('Content-range','bytes '.$this->streamStart.'-'.$this->streamEnd.'/'.$this->streamSize);
						throw new Exception('416 Requested Range Not Satisfiable');
					}

					if ($range == '-')
					{
						$c_start = $this->streamSize - substr($range, 1);
					}
					else
					{
						$range = explode('-', $range);
						$c_start = $range[0];
						$c_end = (isset($range[1]) && is_numeric($range[1])) ? $range[1] : $c_end;
					}
					$c_end = ($c_end > $this->streamEnd) ? $this->streamEnd : $c_end;
					if ($c_start > $c_end || $c_start > $this->streamSize - 1 || $c_end >= $this->streamSize)
					{
						$LAB->RESPONSE->setStatus(416);
						$LAB->RESPONSE->setHeader('Content-range','bytes '.$this->streamStart.'-'.$this->streamEnd.'/'.$this->streamSize);
						throw new Exception('416 Requested Range Not Satisfiable');
					}
					$this->streamStart = $c_start;
					$this->streamEnd = $c_end;
					$length = $this->streamEnd - $this->streamStart + 1;
					fseek($this->streamHandle, $this->streamStart);

					$LAB->RESPONSE->setStatus(206);
					$LAB->RESPONSE->setHeader('Content-length',$length);
					$LAB->RESPONSE->setHeader('Content-range','bytes '.$this->streamStart.'-'.$this->streamEnd.'/'.$this->streamSize);
				}

				// All of it
				else
				{
					$LAB->RESPONSE->setHeader('Content-length',$this->streamSize);
				}

				// Content-type header
				if ($this->param['source']=='dao')
				{
					$mimeType=$this->data->{$this->param['source_dao_field'].'_ctype'};
					$LAB->RESPONSE->setType('raw',$mimeType);
					$LAB->RESPONSE->setFileName($this->data->{$this->param['source_dao_field'].'_fname'});
				}
				else
				{
					$mimeType='TODO: finish';
					$LAB->RESPONSE->setType('raw',$mimeType);
					$LAB->RESPONSE->setFileName($filename);
				}
				if (preg_match('/^image\//',$mimeType) || preg_match('/^video\//',$mimeType) || preg_match('/^audio\//',$mimeType) || $mimeType=='application/pdf')
				{
					$LAB->RESPONSE->setContentDisposition('inline');
				}

				// Stream from string
				if (!empty($this->streamContent))
				{
					// TODO: finish
				}

				// Stream from file
				{
					$c='';
					$i=$this->streamStart;
					set_time_limit(0);
					while(!feof($this->streamHandle) && $i<=$this->streamEnd)
					{
						$bytesToRead=$this->bufferSize;
						if(($i+$bytesToRead)>$this->streamEnd)
						{
							$bytesToRead=$this->streamEnd - $i + 1;
						}
						$c.=fread($this->streamHandle, $bytesToRead);
						$i+=$bytesToRead;
					}
				}

				return $c;
			}

			// Catch exception
			catch (Exception $e)
			{
				// Close file handle if open
				if (!empty($this->streamHandle))
				{
					fclose($this->streamHandle);
				}

				// Return error
				$LAB->RESPONSE->setType('raw');
				return 'ERROR: '.$e->getMessage();
			}
		}


	}


?>