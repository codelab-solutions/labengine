<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   Field class: text field
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_FIELD_TEXT extends BASE_FIELD
	{


		/**
		 *   Sets edit mask
		 *   @param string $mask Edit mask
		 */

		public function setEditMask( $mask )
		{
			$this->param["editmask"]=$mask;
		}


		/**
		 *   Sets autocomplete
		 *   @param bool $autocomplete Autocomplete
		 */

		public function setAutocomplete( $autocomplete )
		{
			$this->param["autocomplete"]=$autocomplete;
		}


		/**
		 *   Sets autocomplete minimum length
		 *   @param int $autocompleteMinLength Minimum length
		 */

		public function setAutocompleteMinLength( $autocompleteMinLength )
		{
			$this->param["autocomplete_minlength"]=$autocompleteMinLength;
		}


		/**
		 *   Sets autocomplete source URL
		 *   @param string $autoCompleteSourceURL Autocomplete source URL
		 */

		public function setAutocompleteSourceURL( $autoCompleteSourceURL )
		{
			$this->param["autocomplete_sourceurl"]=$autoCompleteSourceURL;
		}


		/**
		 *   Sets autocomplete source list
		 *   @param string $autoCompleteSourceList Autocomplete source list variable
		 */

		public function setAutocompleteSourceList( $autoCompleteSourceList )
		{
			$this->param["autocomplete_sourcelist"]=$autoCompleteSourceList;
		}


		/**
		 *   Sets autocomplete key/value mode
		 *   @param bool $autoCompleteKeyValue Key/value mode
		 */

		public function setAutocompleteKeyValue( $autoCompleteKeyValue )
		{
			$this->param["autocomplete_keyvalue"]=$autoCompleteKeyValue;
		}


		/**
		 *  Display the field
		 *  @access public
		 *  @return string contents
		 */

		public function displayField( $value, $row=NULL )
		{
			// Style
			$style=array();
			if (!empty($this->param['fieldstyle'])) $style[]=$this->param['fieldstyle'];
			if (!empty($this->param['comment'])) $style[]='width: 70%; display: inline-block';

			// Class
			$class=array();
			$class[]='form-control';
			if (!empty($this->param['autocomplete'])) $class[]='autocomplete';
			if (!empty($this->param['fieldclass'])) $class[]=$this->param['fieldclass'];

			// Render
			$c='<div class="'.$this->getContentWidthClass().'">';

				$value = htmlspecialchars($value);
				$value = str_replace('{','&#123;',$value);
				$value = str_replace('}','&#125;',$value);
				$value = str_replace('[','&#091;',$value);
				$value = str_replace(']','&#093;',$value);
				$c.='<input type="text"';
				$c.=' id="'.$this->tag.'"';
				$c.=' name="'.$this->tag.'"';
				$c.=' value="'.$value.'"';
				$c.=' data-originalvalue="'.$value.'"';
				$c.=' autocomplete="off"';
				$c.=' class="'.join(' ',$class).'"';
				if (!empty($style)) $c.=' style="'.join('; ',$style).'"';
				if (!empty($this->param['maxlength'])) $c.=' maxlength="'.intval($this->param['maxlength']).'"';
				if (!empty($this->param['readonly'])) $c.=' readonly="readonly"';
				if (!empty($this->param['disabled'])) $c.=' disabled="disabled"';
				if (!empty($this->param['placeholder'])) $c.=' placeholder="'.htmlspecialchars($this->param['placeholder']).'"';
				if (!empty($this->param['emptyformat'])) $c.=' data-emptyformat="'.htmlspecialchars($this->param['emptyformat']).'"';
				if (!empty($this->param['keephiddenvalue'])) $c.=' data-keephiddenvalue="1"';
				if (!empty($this->param['editmask'])) $c.=' data-mask="'.$this->param['editmask'].'"';
				if (!empty($this->param['autocomplete']))
				{
					$c.='data-autocomplete-minlength="'.intval($this->param["autocomplete_minlength"]).'"';
					$c.='data-autocomplete-sourceurl="'.htmlspecialchars($this->param["autocomplete_sourceurl"]).'"';
					$c.='data-autocomplete-sourcelist="'.htmlspecialchars($this->param["autocomplete_sourcelist"]).'"';
					$c.='data-autocomplete-keyvalue="'.($this->param["autocomplete_keyvalue"]?'1':'0').'"';
				}
				if (isset($this->param['event']) && is_array($this->param['event']))
				{
					foreach ($this->param['event'] as $eventType => $eventContent) $c.=' '.$eventType.'="'.$eventContent.'"';
				}
				if (isset($this->param['data']) && is_array($this->param['data']))
				{
					foreach ($this->param['data'] as $dataKey => $dataValue) $c.=' data-'.$dataKey.'="'.htmlspecialchars($dataValue).'"';
				}
				$c.='>';

				// Comment
				$c.=$this->displayComment();

			$c.='</div>';

			// Return
			return $c;
		}


	}


?>