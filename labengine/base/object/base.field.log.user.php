<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   Field class: log user
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_FIELD_LOG_USER extends BASE_FIELD_DROPDOWN
	{


		/**
		 *   List value
		 *   @access public
		 *   @return mixed value
		 */

		public function listValue( $value, &$row )
		{
			if (!empty($value))
			{
				return parent::listValue($value,$row);
			}
			else
			{
				return '<i style="color: #919191">[BASE.LOG.User.System]</i>';
			}
		}


	}


?>