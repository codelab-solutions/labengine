<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   The base functions class
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE
	{


		/**
		 *  Dirty PHP Hack ™ to pass NULL to function parameters
		 */

		public static $NULL = NULL;


		/**
		 *  Is user using Internet Explorer?
		 *  @access public
		 *  @static
		 *  @return boolean
		 */

		public static function isIE()
		{
			$userAgent=$_SERVER['HTTP_USER_AGENT'];
			if (strpos($userAgent,'; MSIE ')!==FALSE) return TRUE;
			if (strpos($userAgent,'Trident/')!==FALSE && strpos($userAgent,'rv:')!==FALSE) return TRUE;
			return FALSE;
		}


		/**
		 *  Are we handling an XHR?
		 *  @static
		 *  @access public
		 *  @return boolean
		 */

		public static function isXHR()
		{
			return ((!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH']=='XMLHttpRequest')?TRUE:FALSE);
		}


		/**
		 *  Are we in a dev environment?
		 *  @static
		 *  @access public
		 *  @return boolean
		 */

		public static function isDevEnvironment()
		{
			global $LAB;
			if ($LAB->CONFIG->get('debug.dev')) return TRUE;
			return FALSE;
		}


		/**
		 *  Get user's IP
		 *  @static
		 *  @access public
		 *  @return string IP
		 */

		public static function remoteIP()
		{
			if (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
			{
				return $_SERVER['HTTP_X_FORWARDED_FOR'];
			}
			elseif (isset($_SERVER['REMOTE_ADDR']))
			{
				return $_SERVER['REMOTE_ADDR'];
			}
			else
			{
				return '';
			}
		}


		/**
		 *  Get user's hostname
		 *  @static
		 *  @access public
		 *  @return string hostname/IP
		 */

		public static function remoteHost()
		{
			if (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
			{
				return @gethostbyaddr($_SERVER['HTTP_X_FORWARDED_FOR']);
			}
			elseif (isset($_SERVER['REMOTE_ADDR']))
			{
				return @gethostbyaddr($_SERVER['REMOTE_ADDR']);
			}
			else
			{
				return '';
			}
		}


		/**
		 *  Sanitize input - either passed value or $_POST
		 *  @access public
		 *  @static
		 *  @param mixed $value Value
		 *  @return string Sanitized value
		 */

		public static function sanitizeInput( $value=FALSE )
		{
			if ($value!==FALSE)
			{
				if (is_array($value))
				{
					foreach ($value as $k => $v)
					{
						$value[$k]=BASE::sanitizeInput($v);
					}
					return $value;
				}
				return trim(strip_tags($value));
			}
			else
			{
				foreach ($_POST as $k => $v)
				{
					$_POST[$k]=BASE::sanitizeInput($v);
				}
			}
		}


		/**
		 *   Is this a valid e-mail address
		 *   @access public
		 *   @static
		 *   @param string $email E-mail address
		 *   @param bool $allowMultiple Allow multiple e-mail addresses?
		 *   @return bool
		 */

		public static function isValidEmail( $email, $allowMultiple=FALSE )
		{
			if ($allowMultiple)
			{
				$emailArr=str_array($email);
				foreach ($emailArr as $e)
				{
					if (!preg_match('/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/',$e))
					{
						return false;
					}
				}
			}
			else
			{
				if (!preg_match('/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/',$email))
				{
					return false;
				}
			}
			return true;
		}


		/**
		 *  Get temporary directory path
		 *  @access public
		 *  @return string Path
		 */

		public static function getTmpDir()
		{
			global $LAB;
			if (strlen($LAB->CONFIG->get('site.tmppath')))
			{

				return $LAB->CONFIG->get('site.tmppath');
			}
			else
			{
				return '/tmp';
			}
		}


		/**
		 *  Convert string to a URL-compatible format
		 *  @access public
		 *  @static
		 *  @param string text
		 *  @param int max length
		 *  @return string
		 */

		public static function stringToURL( $str, $maxlength=100 )
		{
			// Transliterate cyrillic
			$chrCyr=array(
				'а','б','в','г','д','е','ё','ж','з','и','й','к','л','м','н','о','п','р','с','т','у','ф','х','ц','ч','ш','щ','ъ','ы','ь','э','ю','я',
				'А','Б','В','Г','Д','Е','Ё','Ж','З','И','Й','К','Л','М','Н','О','П','Р','С','Т','У','Ф','Х','Ц','Ч','Ш','Щ','Ъ','Ы','Ь','Э','Ю','Я'
			);
			$chrLat=array(
				'a','b','v','g','d','e','e','zh','z','i','y','k','l','m','n','o','p','r','s','t','u','f','h','ts','ch','sh','sht','i','y','y','e','yu','ya',
				'A','B','V','G','D','E','E','Zh','Z','I','Y','K','L','M','N','O','P','R','S','T','U','F','H','Ts','Ch','Sh','Sht','I','Y','Y','E','Yu','Ya'
			);
			$str=str_replace($chrCyr,$chrLat,$str);

			// Transliterate known umlauts
			$chrUmlaut=array(
				'õ','ä','ö','ü','š','ž','ā','č','ē','ģ','ī','ķ','ļ','ņ','ū','ę','ų','ą','č','å',
				'Õ','Ä','Ö','Ü','Š','Ž','Ā','Č','Ē','Ģ','Ī','Ķ','Ļ','Ņ','Ū','Ę','Ų','Ą','Č','Å'
			);
			$chrLat=array(
				'o','a','o','u','s','z','a','c','e','g','i','k','l','n','u','e','u','a','c','a',
				'O','A','O','U','S','Z','A','C','E','G','I','K','L','N','U','E','U','A','C','A'
			);
			$str=str_replace($chrUmlaut,$chrLat,$str);

			// Trip
			$str=substr(trim(mb_strtolower($str)),0,$maxlength);

			// Täpid jms
			$str=iconv('UTF-8','ASCII//TRANSLIT//IGNORE',$str);

			// Muu
			$str=preg_replace("/[^a-z0-9\ ]/","",$str);
			$str=str_replace(" ","-",$str);
			return $str;
		}


		/**
		 *   Convert array to <select> options
		 *   @access public
		 *   @static
		 *   @param array $options Options array
		 *   @param string $selected Selected option value
		 *   @param boolean $optGrouping Option grouping ($options is a two-level array)
		 *   @return string
		 */

		public static function arrayToSelectOptions ( $options, $selected=NULL, $optGrouping=FALSE )
		{
			$c='';
			if (empty($options)) return;
			if ($optGrouping)
			{
				foreach ($options as $optGroupLabel => $optGroup)
				{
					$c.='<optgroup label="'.$optGroupLabel.'">';
					foreach ($optGroup as $k => $v)
					{
						$c.='<option value="'.$k.'"'.($k==$selected?' selected="selected"':'').'>'.$v.'</option>';
					}
					$c.='</optgroup>';
				}
			}
			else
			{
				foreach ($options as $k => $v)
				{
					$c.='<option value="'.$k.'"'.($k==$selected?' selected="selected"':'').'>'.$v.'</option>';
				}
			}
			return $c;
		}


		/**
		 *   Convert array to SQL escaped values
		 *   @access public
		 *   @static
		 *   @param array|object $options Options list
		 *   @return string
		 */

		public static function inValues( $options )
		{
			$retval=array();
			foreach ($options as $option)
			{
				$retval[]="'".addslashes($option)."'";
			}
			return join(',',$retval);
		}


		/**
		 *   Get array/object value
		 *   @access public
		 *   @static
		 *   @param array|object $input Input element
		 *   @param string $key Key
		 *   @return mixed
		 */

		public static function getValue( $input, $key )
		{
			if (is_object($input))
			{
				return $input->{$key};
			}
			elseif (is_array($input))
			{
				return $input[$key];
			}
			else
			{
				return BASE::$NULL;
			}
		}


		/**
		 *   Get MIME type for filename
		 *   @access public
		 *   @static
		 *   @param string $filename Filename
		 *   @throws Exception
		 *   @return string|bool
		 */

		public static function getMimeType( $filename )
		{
			global $LAB;

			// Get file extension
			$fileExtension=pathinfo($filename,PATHINFO_EXTENSION);

			// Read file
			$mimeTypesFile=stream_resolve_include_path('base/data/mime/mime.types');
			if (empty($mimeTypesFile)) throw new Exception('Cannot locate mime.types file.');
			$mimeTypes=file($mimeTypesFile,FILE_IGNORE_NEW_LINES);
			if (empty($mimeTypes) || !is_array($mimeTypes)) throw new Exception('Error reading mime.types file.');

			// Find MIME type
			foreach ($mimeTypes as $line)
			{
				if (empty($line) || $line[0]=='#') continue;
				$lineArr=preg_split("/\s+/",$line);
				if (empty($lineArr[1])) continue;
				for ($i=1;$i<sizeof($lineArr);++$i)
				{
					if ($lineArr[$i]==$fileExtension) return $lineArr[0];
				}
			}

			// Nothing?
			return false;
		}



	}


?>