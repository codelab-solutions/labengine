<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   The query class
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_QUERY
	{


		/**
		 * Query type
		 * @var string
		 * @access public
		 */
		public $queryType = NULL;

		/**
		 * Table(s)
		 * @var string
		 * @access public
		 */
		public $queryTable = NULL;

		/**
		 * Columns
		 * @var array
		 * @access public
		 */
		public $queryColumns = NULL;

		/**
		 * WHERE clauses
		 * @var array
		 * @access public
		 */
		public $queryWhere = NULL;

		/**
		 * WHERE type
		 * @var array
		 * @access public
		 */
		public $queryWhereType = 'and';

		/**
		 * GROUP BY clauses
		 * @var array
		 * @access public
		 */
		public $queryGroupBy = NULL;

		/**
		 * HAVING clauses
		 * @var array
		 * @access public
		 */
		public $queryHaving = NULL;

		/**
		 * ORDER BY
		 * @var array
		 * @access public
		 */
		public $queryOrderBy = NULL;

		/**
		 * LIMIT
		 * @var int
		 * @access public
		 */
		public $queryLimit = NULL;

		/**
		 * LIMIT offset
		 * @var int
		 * @access public
		 */
		public $queryLimitOffset = NULL;

		/**
		 * Calculate found rows
		 * @var boolean
		 * @access public
		 */
		public $calcFoundRows = FALSE;


		/**
		 *  The constructor
		 *  @access public
		 *  @param string query type
		 *  @return void
		 */

		public function __construct ( $queryType )
		{
			$queryType=strtoupper($queryType);
			switch($queryType)
			{
				case 'SELECT':
				case 'UPDATE':
				case 'REPLACE':
				case 'DELETE':
					$this->queryType=$queryType;
					return;
				case '':
					throw new Exception('Missing query type.');
					return;
				default:
					throw new Exception('Unknown query type.');
					return;
			}
		}


		/**
		 *   Add table(s)
		 *   @access public
		 *   @param string table
		 *   @param bool|string JOIN condition
		 *   @param string JOIN type
		 *   @return void
		 *   @throws Exception
		 */

		public function addTable ( $table, $joinCondition=FALSE, $joinType='LEFT JOIN' )
		{
			if (!is_array($this->queryTable)) $this->queryTable=array();
			if (sizeof($this->queryTable))
			{
				if (empty($joinCondition))
				{
					throw new Exception('Empty join condition<hr/>'.var_dump_str($this).'<hr>'.$table.'<hr>'.$joinCondition);
					return;
				}
				$joinCondition=str_array($joinCondition);
				$this->queryTable[$table]=$joinType.' '.$table.' ON ('.join(' and ',$joinCondition).')';
			}
			else
			{
				$this->queryTable[$table]=$table;
			}
		}


		/**
		 *   Add columns
		 *   @access public
		 *   @param string|array columns
		 *   @return void
		 */

		public function addColumns ( $columns )
		{
			if (!is_array($this->queryColumns)) $this->queryColumns=array();
			$columns=str_array($columns);
			foreach ($columns as $col)
			{
				$this->queryColumns[$col]=$col;
			}
		}


		/**
		 *   Add WHERE clause(s)
		 *   @access public
		 *   @param string|array WHERE clause(s)
		 *   @return void
		 */

		public function addWhere ( $where )
		{
			if (!is_array($this->queryWhere)) $this->queryWhere=array();
			$this->queryWhere[$where]=$where;
		}


		/**
		 *   Set WHERE type
		 *   @access public
		 *   @param string WHERE type (and|or)
		 *   @return void
		 *   @throws Exception
		 */

		public function setWhereType ( $whereType )
		{
			$whereType=mb_strtolower($whereType);
			switch($whereType)
			{
				case 'and':
				case 'or':
					$this->paramWhereType=$whereType;
					return;
				default:
					throw new Exception('Invalid WHERE type.');
					return;
			}
		}


		/**
		 *   Add GROUP BY clause(s)
		 *   @access public
		 *   @param string|array GROUP BY clause(s)
		 *   @return void
		 */

		public function addGroupBy ( $groupBy )
		{
			if (!is_array($this->queryGroupBy)) $this->queryGroupBy=array();
			$groupBy=str_array($groupBy);
			foreach ($groupBy as $gb)
			{
				$this->queryGroupBy[$gb]=$gb;
			}
		}


		/**
		 *   Add HAVING clause(s)
		 *   @access public
		 *   @param string|array HAVING clause(s)
		 *   @return void
		 */

		public function addHaving ( $having )
		{
			if (!is_array($this->queryHaving)) $this->queryHaving=array();
			$having=str_array($having);
			foreach ($having as $h)
			{
				$this->queryHaving[$h]=$h;
			}
		}


		/**
		 *   Add ORDER BY clause(s)
		 *   @access public
		 *   @param string|array ORDER BY clause(s)
		 *   @return void
		 */

		public function addOrderBy ( $orderBy )
		{
			if (!is_array($this->queryOrderBy)) $this->queryOrderBy=array();
			$orderBy=str_array($orderBy);
			foreach ($orderBy as $ob)
			{
				$this->queryOrderBy[$ob]=$ob;
			}
		}


		/**
		 *   Add LIMIT
		 *   @access public
		 *   @param int limit
		 *   @param int offset
		 *   @return void
		 */

		public function addLimit ( $limit, $offset=NULL )
		{
			$this->queryLimit=$limit;
			$this->queryLimitOffset=$offset;
		}


		/**
		 *   Create and populate the BASE_QUERY class from old FWK-style array
		 *   @access public
		 *   @static
		 *   @param string query type
		 *   @param array parameters
		 *       table      - (1) table name, (2) array of table names as key and JOIN condition as value
		 *       columns    - column names (string/array)
		 *       where      - WHERE clauses (string/array)
		 *       groupby    - GROUP BY clauses (string/array)
		 *       having     - HAVING clauses (string/array)
		 *       orderby    - ORDER BY clauses (string/array)
		 *       limit      - LIMIT clause (string/array)
		 *       foundrows  - calculate found rows (TRUE/FALSE)
		 *   @return BASE_QUERY
		 */

		public static function createObject ( $queryType, $queryParam )
		{
			// Init class
			$queryObject=new BASE_QUERY($queryType);

			// Columns
			if (isset($queryParam['columns'])) $queryObject->addColumns($queryParam['columns']);

			// Tables
			if (isset($queryParam['table']))
			{
				if (is_array($queryParam['table']))
				{
					foreach ($queryParam['table'] as $t_table => $t_cond)
					{
						$queryObject->addTable($t_table,$t_cond);
					}
				}
				else
				{
					$queryObject->addTable($queryParam['table']);
				}
			}

			// WHERE
			if (isset($queryParam['where'])) $queryObject->addWhere($queryParam['where']);

			// GROUP BY
			if (isset($queryParam['groupby'])) $queryObject->addGroupBy($queryParam['groupby']);

			// HAVING
			if (isset($queryParam['having'])) $queryObject->addHaving($queryParam['having']);

			// ORDER BY
			if (isset($queryParam['orderby'])) $queryObject->addOrderBy($queryParam['orderby']);

			// LIMIT
			if (isset($queryParam['limit']))
			{
				if (is_array($queryParam['limit']))
				{
					$queryObject->addLimit($queryParam['limit'][0],$queryParam['limit'][1]);
				}
				else
				{
					$queryObject->addLimit($queryParam['limit']);
				}
			}

			// Found rows
			if (isset($queryParam['foundrows']) && $queryParam['foundrows']==TRUE)
			{
				$queryObject->calcFoundRows=TRUE;
			}

			// Return object
			return $queryObject;
		}


		/**
		 *   Create IN() statement from array
		 *   @static
		 *   @access public
		 *   @param array options
		 *   @param boolean integer values?
		 *   @return string
		 */

		public static function inValues( $options, $integerValues=FALSE )
		{
			$optionSet=array();
			foreach ($options as $o)
			{
				$optionSet[]=($integerValues?intval($o):"'".addslashes($o)."'");
			}
			return join(',',$optionSet);
		}


	}


?>