<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   Field class: monetary value field
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_FIELD_MONETARYVALUE extends BASE_FIELD_NUMBER
	{


		/**
		 *   Construct
		 */

		public function __construct ( $tag=NULL )
		{
			if (!empty($tag)) $this->tag=$tag;
			$this->param['precision']='2';
			$this->param['showzerovalue']=true;
		}


		/**
		 *   Set currency field
		 *   @access public
		 *   @param string Currency field
		 *   @return void
		 */

		public function setCurrencyField ( $currencyField )
		{
			$this->param['currencyfield']=$currencyField;
		}


		/**
		 *   Set currency list
		 *   @access public
		 *   @param array Currency list
		 *   @return void
		 */

		public function setCurrencyList ( $currencyList )
		{
			$this->param['currencylist']=$currencyList;
		}


		/**
		 *   Set currency default
		 *   @access public
		 *   @param array Currency list
		 *   @return void
		 */

		public function setCurrencyDefault ( $currencyDefault )
		{
			$this->param['currencydefault']=$currencyDefault;
		}


		/**
		 *   Add a currency field event
		 *   @access public
		 *   @param string Event type
		 *   @param string Action
		 *   @return void
		 */

		public function addCurrencyEvent ( $eventType, $eventAction )
		{
			if (!empty($this->param['currencyevent'][$eventType]))
			{
				$this->param['currencyevent'][$eventType].=$eventAction;
			}
			else
			{
				$this->param['currencyevent'][$eventType]=$eventAction;
			}
		}


		/**
		 *  Save value
		 *  @access public
		 *  @return mixed value
		 */

		public function saveValue()
		{
			$value=$this->getValue();
			$value=trim(preg_replace("/\s+/",'',str_replace(',','.',$value)));
			$currencyValue=$_POST[oneof($this->param['currencyfield'],$this->tag.'_currency')];

			if (!empty($this->param['currencyfield']))
			{
				$retval=array();
				$retval[$this->tag]=$value;
				$retval[$this->param['currencyfield']]=$currencyValue;
			}
			else
			{
				$retval=sprintf("%.02f",$value).' '.$currencyValue;
			}

			return $retval;
		}


		/**
		 *  Log data
		 *  @access public
		 *  @return boolean
		 */

		public function getLogData( $dataObject, $operation, &$logValuesArray )
		{
			// Currency fld
			$currencyFld=(array_key_exists('currencyfield',$this->param)?$this->param['currencyfield']:$this->tag.'_currency');

			unset($logValuesArray[$this->tag]);
			unset($logValuesArray[$currencyFld]);

			if ($operation!='edit' && !mb_strlen($dataObject->{$this->tag}) && !mb_strlen($dataObject->{$currencyFld})) return;
			if ($operation=='edit')
			{
				if ($dataObject->{$this->tag}==$dataObject->_in[$this->tag] && $dataObject->{$currencyFld}==$dataObject->_in[$currencyFld]) return;
				if ($dataObject->{$this->tag}=='' && $dataObject->_in[$this->tag]=='0' && $dataObject->{$currencyFld}==$dataObject->_in[$currencyFld]) return;
				if ($dataObject->{$this->tag}=='0' && $dataObject->_in[$this->tag]=='' && $dataObject->{$currencyFld}==$dataObject->_in[$currencyFld]) return;
			}

			// Log
			$XML='<fld id="'.htmlspecialchars($this->tag).'" name="'.htmlspecialchars($this->param['title']).'">';
			if ($operation!='add' && !$this->param['forcevalue']) $XML.='<old>'.sprintf("%.02f",$dataObject->_in[$this->tag]).' '.htmlspecialchars($dataObject->_in[$currencyFld]).'</old>';
			$XML.='<new>'.sprintf("%.02f",$dataObject->{$this->tag}).' '.htmlspecialchars($dataObject->{$currencyFld}).'</new>';
			$XML.='</fld>';
			return $XML;
		}


		/**
		 *  Display the field
		 *  @access public
		 *  @return string contents
		 */

		public function displayField( $value, $row=NULL )
		{
			global $LAB;

			// Render
			$c='<div class="'.$this->getContentWidthClass().'">';

			// Init
			$comment=$this->param['comment'];
			$this->param['comment']=false;

			// Hack
			if (!empty($this->param['currencyfield']))
			{
				$currencyValue=$this->formObject->data->{$this->param['currencyfield']};
			}
			else
			{
				list($value,$currencyValue)=preg_split("/\s+/",$value,2);
			}
			if (empty($currencyValue) && is_object($this->formObject) && $this->formObject->operation=='add')
			{
				$currencyValue=$this->param['currencydefault'];
			}

			// Format value
			if (!empty($this->param['displayformat']))
			{
				$value=sprintf($this->param['displayformat'],$value);
			}
			else
			{
				if ($value!='' || $this->param['showzerovalue']!==false)
				{
					// TODO: write local function: $value=BASE::formatDecimalValue($value,TRUE,FALSE);
					$value=sprintf("%.0".$this->param['precision']."f",$value);
				}
			}
			if (!floatval($value) && $this->param['showzerovalue']==false)
			{
				$value='';
			}

			// Empty value format
			if ($this->param['precision']>0)
			{
				$this->param['emptyformat']=sprintf("%.0".$this->param['precision']."f",0);
			}
			else
			{
				$this->param['emptyformat']='0';
			}


			//
			//  Number field
			//

			$style=array();
			if (!empty($this->param['fieldstyle'])) $style[]=$this->param['fieldstyle'];
			$style[]='width: 110px !important; text-align: center !important; display: inline-block';

			$class=array();
			$class[]='form-control';
			$ckass[]='numberfield';
			if (!empty($this->param['fieldclass'])) $class[]=$this->param['fieldclass'];

			$value = htmlspecialchars($value);
			$value = str_replace('{','&#123;',$value);
			$value = str_replace('}','&#125;',$value);
			$value = str_replace('[','&#091;',$value);
			$value = str_replace(']','&#093;',$value);

			$valueDisplay='<input type="text"';
			$valueDisplay.=' id="'.$this->tag.'"';
			$valueDisplay.=' name="'.$this->tag.'"';
			$valueDisplay.=' value="'.$value.'"';
			$valueDisplay.=' data-originalvalue="'.$value.'"';
			$valueDisplay.=' autocomplete="off"';
			$valueDisplay.=' class="'.join(' ',$class).'"';
			if (!empty($style)) $valueDisplay.=' style="'.join('; ',$style).'"';
			if (!empty($this->param['maxlength'])) $valueDisplay.=' maxlength="'.intval($this->param['maxlength']).'"';
			if (!empty($this->param['readonly'])) $valueDisplay.=' readonly="readonly"';
			if (!empty($this->param['disabled'])) $valueDisplay.=' disabled="disabled"';
			if (!empty($this->param['placeholder'])) $valueDisplay.=' placeholder="'.htmlspecialchars($this->param['placeholder']).'"';
			if (!empty($this->param['emptyformat'])) $valueDisplay.=' data-emptyformat="'.htmlspecialchars($this->param['emptyformat']).'"';
			if (!empty($this->param['keephiddenvalue'])) $valueDisplay.=' data-keephiddenvalue="1"';
			if (isset($this->param['event']) && is_array($this->param['event']))
			{
				foreach ($this->param['event'] as $eventType => $eventContent) $valueDisplay.=' '.$eventType.'="'.$eventContent.'"';
			}
			$valueDisplay.='>';

			//
			//  Currency field
			//

			$currencyDisplay='<select';
			$currencyDisplay.=' id="'.oneof($this->param['currencyfield'],$this->tag.'_currency').'"';
			$currencyDisplay.=' name="'.oneof($this->param['currencyfield'],$this->tag.'_currency').'"';
			$currencyDisplay.=' class="form-control '.$this->param['currencyfieldclass'].'"';
			$currencyDisplay.=' data-originalvalue="'.htmlspecialchars($currencyValue).'"';
			if (!empty($style)) $currencyDisplay.=' style="display: inline-block; width: 110px !important'.(!empty($this->param['currencyfieldstyle'])?';'.$this->param['currencyfieldstyle']:'').'"';
			if (!empty($this->param['disabled'])) $currencyDisplay.=' disabled="disabled"';
			if (!empty($this->param['keephiddenvalue'])) $currencyDisplay.=' data-keephiddenvalue="1"';
			if (isset($this->param['currencyevent']) && is_array($this->param['currencyevent']))
			{
				foreach ($this->param['currencyevent'] as $eventType => $eventContent) $currencyDisplay.=' '.$eventType.'="'.$eventContent.'"';
			}
			$currencyDisplay.='>';
			$currencyDisplay.=BASE::arrayToSelectOptions($this->param['currencylist'],$currencyValue);
			$currencyDisplay.='</select>';

			// Display
			switch ($LAB->REGIONAL->REGIONAL_currencyplacement)
			{
				case 'beforespace':
				case 'before':
					$c.=$currencyDisplay.' &nbsp; '.$valueDisplay;
					break;
				default:
					$c.=$valueDisplay.' &nbsp; '.$currencyDisplay;
					break;
			}

			// Comment
			$this->param['comment']=$comment;
			$c.=$this->displayComment();

			$c.='</div>';
			return $c;
		}


	}


?>