<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   BankLink integration: Danske Bank
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_BANKLINK_DANSKE extends BASE_BANKLINK_IPIZZA
	{


		/**
		 *   Bank ID
		 *   @var string
		 *   @static
		 */

		public static $BANK_id = 'danske';


		/**
		 *   Bank destination URL
		 *   @var string
		 */

		public $BANK_destination_url = 'https://www2.danskebank.ee/ibank/pizza/pizza';


	}


?>