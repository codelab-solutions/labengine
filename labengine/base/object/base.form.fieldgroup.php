<?


	/**
	 *
	 *   LabEngine™ 7
	 *   The field group class
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_FORM_FIELDGROUP
	{


		public $tag        = FALSE;
		public $formObject = NULL;
		public $title      = '';
		public $info       = '';
		public $class      = '';
		public $checkable  = FALSE;
		public $checkField = NULL;
		public $indented   = FALSE;
		public $event      = array();
		public $actions    = array();


		public function saveValue()
		{
			if (is_object($this->formObject) && $this->formObject->doSubmit)
			{
				return $_POST[$this->checkField];
			}

			// Loaded data
			if (is_object($this->formObject) && $this->formObject->operation=='edit')
			{
				if (empty($this->formObject->data->{$this->checkField}))
				{
					return $this->param['default'];
				}
				return $this->formObject->data->{$this->checkField};
			}

			return FALSE;
		}


		public function setTitle ( $title )
		{
			$this->title=$title;
		}


		public function setInfo ( $info )
		{
			$this->info=$info;
		}

		public function setClass ( $class )
		{
			$this->class=$class;
		}


		public function setCheckable ( $checkable )
		{
			$this->checkable=$checkable;
		}


		public function setCheckField ( $checkField )
		{
			$this->checkField=$checkField;
		}


		public function setIndented ( $indented )
		{
			$this->indented=$indented;
		}


		public function addAction ( $action )
		{
			$this->actions[]=$action;
		}


		public function addEvent ( $eventType, $eventAction )
		{
			if (!empty($this->event[$eventType]))
			{
				$this->event[$eventType].=$eventAction;
			}
			else
			{
				$this->event[$eventType]=$eventAction;
			}
		}


	}



?>