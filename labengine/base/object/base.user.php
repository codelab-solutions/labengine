<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   The base user class
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_USER extends BASE_DATAOBJECT
	{


		/**
		 *   Roles
		 *   @var array
		 *   @access public
		 */

		public $_roles=array();


		/**
		 *   Init data object
		 *   @access public
		 *   @throws Exception
		 *   @return void
		 */

		public function initDataObject()
		{
			global $LAB;

			// Main fields
			$this->_param['table']            = 'base_user';
			$this->_param['objectname']       = 'User';
			$this->_param['idfield']          = 'user_oid';
			$this->_param['descriptionfield'] = 'user_email,user_name';
			$this->_param['setord']           = false;
			$this->_param['prop']             = true;
			$this->_param['modfields']        = true;

			// Password mechanism
			$this->_param['password']         = oneof($LAB->CONFIG->get('user.passwordmechanism'),'md5');

			// Authenticate against Voog?
			$this->_param['auth_voog']             = false;
			$this->_param['auth_voog_site']        = null;
			$this->_param['auth_voog_createuser']  = false;

			// User specific fields
			$this->_param['loginemailfield']       = 'user_email';
			$this->_param['loginpasswordfield']    = 'user_password';
			$this->_param['loginstatusfield']      = 'user_status';
			$this->_param['usernamefield']         = 'user_name';
			$this->_param['lastlogintstampfield']  = 'user_lastlogin';
			$this->_param['lastloginhostfield']    = 'user_lastlogin_host';

			// Log logins?
			$this->_param['loginlog']        = FALSE;
			$this->_param['loginlog_table']  = 'base_loginlog';
		}


		/**
		 *   Define fields and relations for data object
		 *   @access public
		 *   @throws Exception
		 *   @return void
		 */

		public function defineFields()
		{
			// E-mail
			$f=$this->addField('user_email',new BASE_FIELD_EMAIL());
			$f->setTitle('[BASE.COMMON.Email]');
			$f->setMaximumLength(255);
			$f->setUnique(TRUE);
			$f->setRequired('always');

			// Name
			$f=$this->addField('user_name',new BASE_FIELD_TEXT());
			$f->setTitle('[BASE.COMMON.Name]');
			$f->setMaximumLength(255);
			$f->setRequired('always');

			// Password
			$f=$this->addField('user_password',new BASE_FIELD_PASSWORD());
			$f->setTitle('[BASE.COMMON.Password]');
			$f->setTitleRepeat('[BASE.COMMON.Password.Repeat]');
			$f->setMinimumLength(4);
			$f->setRequired('add');
			$f->setSuggest(true);

			// Status
			$f=$this->addField('user_status',new BASE_FIELD_DROPDOWN());
			$f->setTitle('[BASE.COMMON.Status]');
			$f->setSourceList(array(
				'0' => '[BASE.USER.Status.0]',
				'1' => '[BASE.USER.Status.1]',
				'9' => '[BASE.USER.Status.9]'
			));
		}


		/**
		 *   Load user
		 *   @access public
		 *   @return void
		 */

		public function loadUser( $isLogin=FALSE )
		{
			global $LAB;

			// Sanity check: make sure we are logged in
			$userOID=$LAB->SESSION->get('LOGIN.user_oid');
			if (empty($userOID)) return;

			// Load user
			$this->load($userOID);

			// Load roles
			$this->_roles=$this->getRoles();

			// Set login info
			if ($isLogin && (!empty($this->_param['lastlogintstampfield']) || !empty($this->_param['lastloginhostfield'])))
			{
				$saveParam=new BASE_DATAOBJECT_PARAM();
				if (!empty($this->_param['lastlogintstampfield']))
				{
					$this->{$this->_param['lastlogintstampfield']}=date('Y-m-d H:i:s');
					$saveParam->addColumns($this->_param['lastlogintstampfield']);
				}
				if (!empty($this->_param['lastloginhostfield']))
				{
					$this->{$this->_param['lastloginhostfield']}=BASE::remoteHost();
					$saveParam->addColumns($this->_param['lastloginhostfield']);
				}
				$this->save($saveParam,false);
			}
		}


		/**
		 *   Is user logged in?
		 *   @access public
		 *   @return boolean
		 */

		public function isLoggedIn()
		{
			global $LAB;

			// Get user OID from session
			$userOID=$LAB->USER->{$LAB->USER->_param['idfield']};
			return (intval($userOID)?TRUE:FALSE);
		}


		/**
		 *   Get logged in user OID
		 *   @access public
		 *   @return boolean|int
		 */

		public function getUserOID()
		{
			global $LAB;

			// Get user OID from session
			$userOID=$LAB->USER->{$LAB->USER->_param['idfield']};
			return (intval($userOID)?$userOID:FALSE);
		}


		/**
		 *   Check if user has the given role(s)
		 *   @access public
		 *   @param string|array $role Role(s)
		 *   @return boolean
		 */

		public function checkRole( $role )
		{
			// Not logged in?
			if (!$this->isLoggedIn()) return FALSE;

			// Superpowwa
			if (in_array('supervisor',$this->_roles)) return true;

			// Parse: we support checking for multiple possible roles
			$roleList=str_array($role);

			// See if we have any
			foreach ($roleList as $r)
			{
				if (in_array($r,$this->_roles)) return TRUE;
			}

			// If we ended up, this means "no"
			return FALSE;
		}


		/**
		 *   Do login
		 *   @access public
		 *   @param string $email E-mail address
		 *   @param string $password Password
		 *   @param boolean $throwExceptionOnError Throw exception on error
		 *   @throws Exception
		 *   @return boolean Success/failure
		 */

		public function doLogin( $email, $password, $throwExceptionOnError=TRUE )
		{
			global $LAB;
			try
			{
				// Check input
				if (!mb_strlen($email)) throw new Exception($LAB->LOCALE->get('BASE.USER.LOGIN.Error.EmailEmpty'));
				if (!mb_strlen($password)) throw new Exception($LAB->LOCALE->get('BASE.USER.LOGIN.Error.PasswordEmpty'));

				// Validate against Voog?
				if (!empty($this->_param['auth_voog']))
				{
					$voogLogin=$this->authVoog($email,$password,false);
				}

				// Select
				$user=$LAB->DB->selectOneSQL("SELECT * FROM ".$this->_param['table']." WHERE ".$this->_param['loginemailfield']."='".addslashes($email)."'");

				// User not found
				if (empty($user[$this->_param['idfield']]))
				{
					// Create new user from Voog?
					if (!empty($this->_param['auth_voog']) && $voogLogin==true && !empty($this->_param['auth_voog_createuser']))
					{
						$userClass=get_called_class();
						$NEWUSER=new $userClass();
						$NEWUSER->{$this->_param['loginemailfield']}=$email;
						$NEWUSER->{$this->_param['loginpasswordfield']}=$this->getPasswordHash($password);
						$NEWUSER->{$this->_param['loginstatusfield']}='0';
						$NEWUSER->{$this->_param['usernamefield']}='Voog / '.$email;
						$NEWUSER->save(BASE::$NULL,'User created from Voog auth');
						$user=$LAB->DB->selectOneSQL("SELECT * FROM ".$this->_param['table']." WHERE ".$this->_param['idfield']."='".intval($NEWUSER->{$this->_param['idfield']})."'");
					}
					else
					{
						throw new Exception($LAB->LOCALE->get('BASE.USER.LOGIN.Error.LoginFailed'));
					}
				}

				// Check password
				if (empty($this->_param['auth_voog']) || !$voogLogin)
				{
					$passwordAlgorithm=oneof($this->_param['password'],'md5');
					switch($passwordAlgorithm)
					{
						// PHP password_hash
						case 'password_hash':
							if (!password_verify($password,$user[$this->_param['loginpasswordfield']])) throw new Exception($LAB->LOCALE->get('BASE.USER.LOGIN.Error.LoginFailed'));
							break;

						// MD5
						case 'md5':
							if (strpos($user[$this->_param['loginpasswordfield']],':')!==false)
							{
								list($hash,$salt)=preg_split('/:/',$user[$this->_param['loginpasswordfield']],2);
								if (md5($salt.$password)!=$hash) throw new Exception($LAB->LOCALE->get('BASE.USER.LOGIN.Error.LoginFailed'));
							}
							else
							{
								if (md5($password)!=$user[$this->_param['loginpasswordfield']]) throw new Exception($LAB->LOCALE->get('BASE.USER.LOGIN.Error.LoginFailed'));
							}
							break;

						// Default: meh?
						default:
							throw new Exception('Invalid/unknown/missing password encryption algorithm.');
					}
				}

				// Check status
				if ($user[$this->_param['loginstatusfield']]==1) throw new Exception($LAB->LOCALE->get('BASE.USER.LOGIN.Error.UserPending'));
				if ($user[$this->_param['loginstatusfield']]>1) throw new Exception($LAB->LOCALE->get('BASE.USER.LOGIN.Error.UserDisabled'));

				// Set session
				$LAB->SESSION->set('LOGIN.user_oid',$user[$this->_param['idfield']]);

				// Load user
				$this->loadUser(true);
				$this->loginTrigger();

				// Log success
				if (!empty($this->_param['loginlog']))
				{
					$cols=array(
						'user_oid' => $this->{$this->_param['idfield']},
						'loginlog_tstamp' => date('Y-m-d H:i:s'),
						'loginlog_type' => '1',
						'loginlog_ip' => BASE::remoteIP(),
						'loginlog_hostname' => BASE::remoteHost(),
						'loginlog_email' => $this->{$this->_param['loginemailfield']},
						'loginlog_entry' => '[BASE.USER.LOGIN.Success]'
					);
					if (!empty($this->_param['loginlog_data']))
					{
						foreach ($this->_param['loginlog_data'] as $k => $v)
						{
							$cols[$k]=$v;
						}
					}
					$LAB->DB->queryInsert(oneof($this->_param['loginlog_table'],'base_loginlog'),$cols);
				}

				// Success
				return TRUE;
			}
			catch (Exception $e)
			{
				// Log failure
				if (!empty($this->_param['loginlog']))
				{
					$cols=array(
						'user_oid' => $this->{$this->_param['idfield']},
						'loginlog_tstamp' => date('Y-m-d H:i:s'),
						'loginlog_type' => '2',
						'loginlog_ip' => BASE::remoteIP(),
						'loginlog_hostname' => BASE::remoteHost(),
						'loginlog_email' => $email,
						'loginlog_entry' => $e->getMessage()
					);
					if (!empty($this->_param['loginlog_data']))
					{
						foreach ($this->_param['loginlog_data'] as $k => $v)
						{
							$cols[$k]=$v;
						}
					}
					$LAB->DB->queryInsert(oneof($this->_param['loginlog_table'],'base_loginlog'),$cols);
				}

				// Handle error
				if ($throwExceptionOnError) throw $e;
				return FALSE;
			}
		}


		/**
		 *   Do logout
		 *   @access public
		 *   @return void
		 */

		public function doLogout()
		{
			global $LAB;
			$LAB->SESSION->set('LOGIN.user_oid',FALSE);
		}


		/**
		 *   Login trigger
		 *   @access public
		 *   @return void
		 */

		public function loginTrigger()
		{
			// This should be implemented in the subclass if needed.
		}


		/**
		 *   Authenticate against Voog
		 *   @access public
		 *   @param string $email E-mail address
		 *   @param string $password Password
		 *   @param boolean $throwExceptionOnError Throw exception on error
		 *   @throws Exception
		 *   @return boolean Success/failure
		 */

		public function authVoog( $email, $password, $throwExceptionOnError=true )
		{
			global $LAB;
			try
			{
				// Check input
				if (!mb_strlen($email)) throw new Exception($LAB->LOCALE->get('BASE.USER.LOGIN.Error.EmailEmpty'));
				if (!mb_strlen($password)) throw new Exception($LAB->LOCALE->get('BASE.USER.LOGIN.Error.PasswordEmpty'));
				if (!mb_strlen($this->_param['auth_voog_site'])) throw new Exception('auth_voog_site not specified.');

				// Make request
				$HTTP=new BASE_HTTPREQUEST();
				$HTTP->setURL($this->_param['auth_voog_site'].'/admin/login');
				$HTTP->setRequestMethod('POST');
				$HTTP->setOption('newcookiesession',true);
				$HTTP->setOption('followlocation',false);
				$HTTP->setRequestHeaders(array(
					'User-Agent' => 'LabEngine by Codelab, Voog user auth agent (curlHTTPRequest)',
					'Accept' => '',
					'Accept-Encoding' => ''
				));
				$HTTP->setPostFields(array(
					'utf8' => '✓',
					'language' => 'en',
					'email' => $email,
					'password' => $password
				));
				$voogAuthResponse=$HTTP->send();

				// 302 - login OK
				if ($HTTP->getResponseCode()==302)
				{
					return true;
				}

				// Otherwise, fail
				else
				{
					throw new Exception('Login failed, HTTP response: '.$HTTP->getResponseCode());
				}
			}
			catch (Exception $e)
			{
				if ($throwExceptionOnError) throw $e;
				return false;
			}
		}


		/**
		 *   Get roles list
		 *   @access public
		 *   @static
		 *   @return array()
		 */

		public static function getRoleList()
		{
			global $LAB;

			// CMS
			if ($LAB->CMS)
			{
				return $LAB->CMS->getRoleList();
			}

			// This should be implemented on the site level.
			return FALSE;
		}


		/**
		 *   Get roles
		 *   @access public
		 *   @return array()
		 *   @throws Exception
		 */

		public function getRoles()
		{
			global $LAB;

			// Check
			if (!$this->_loaded) throw new Exception('User not loaded.');

			// Load
			$query=new BASE_QUERY('SELECT');
			$query->addTable('base_user_role');
			$query->addColumns('*');
			$query->addWhere('user_oid='.intval($this->{$this->_param['idfield']}));
			$dataset=$LAB->DB->querySelect($query);

			// Return
			$roleList=array();
			foreach ($dataset as $row) $roleList[$row['user_role']]=$row['user_role'];
			return $roleList;
		}


		/**
		 *   Set roles
		 *   @access public
		 *   @param string|array $roleList Role list
		 *   @return void
		 *   @throws Exception
		 */

		public function setRoles( $roleList )
		{
			global $LAB;

			// Check
			if (!$this->_loaded) throw new Exception('User not loaded.');

			// Save
			try
			{
				// Start TX
				$LAB->DB->startTransaction();

				// Delete existing roles
				$query=new BASE_QUERY('DELETE');
				$query->addTable('base_user_role');
				$query->addWhere('user_oid='.intval($this->{$this->_param['idfield']}));
				$LAB->DB->queryDelete($query);

				// Add new ones
				if (!is_array($roleList))
				{
					if (strpos($roleList,"\t")!==false)
					{
						$roleList=str_array($roleList,"\t");
					}
					else
					{
						$roleList=str_array($roleList);
					}
				}
				if (is_array($roleList))
				{
					foreach ($roleList as $role)
					{
						$cols=array();
						$cols['user_oid']=$this->{$this->_param['idfield']};
						$cols['user_role']=$role;
						$LAB->DB->queryInsert('base_user_role',$cols);
					}
				}

				// Commit
				$LAB->DB->doCommit();
			}
			catch (Exception $e)
			{
				$LAB->DB->doRollback();
				throw $e;
			}
		}


		/**
		 *   Get password hash
		 *   @access public
		 *   @param string $password Password
		 *   @return string Password hash
		 *   @throws Exception
		 */

		public function getPasswordHash( $password )
		{
			global $LAB;

			// Encrypt
			$passwordAlgorithm=oneof($this->_param['password'],'md5');
			switch($passwordAlgorithm)
			{
				// PHP password_hash
				case 'password_hash':
					$passwordCryptStrength=oneof($LAB->USER->_param['password_cost'],'12');
					$hash=password_hash($password,PASSWORD_DEFAULT,array(
						'cost' => $passwordCryptStrength
					));
					break;

				// MD5
				case 'md5':
					$hash=md5($password);
					break;

				// Default: meh?
				default:
					throw new Exception('Invalid/unknown/missing password encryption algorithm.');
			}

			// Return
			return $hash;
		}


		/**
		 *   Verify password
		 *   @access public
		 *   @param string $password Password
		 *   @throws Exception
		 *   @return bool
		 */

		public function verifyPassword( $password )
		{
			// Encrypt
			$passwordAlgorithm=oneof($this->_param['password'],'md5');
			switch($passwordAlgorithm)
			{
				// PHP password_hash
				case 'password_hash':
					if (!password_verify($password,$this->{$this->_param['loginpasswordfield']})) return false;
					break;

				// MD5
				case 'md5':
					if (strpos($this->{$this->_param['loginpasswordfield']},':')!==false)
					{
						list($hash,$salt)=preg_split('/:/',$this->{$this->_param['loginpasswordfield']},2);
						if (md5($salt.$password)!=$hash) return false;
					}
					else
					{
						if (md5($password)!=$this->{$this->_param['loginpasswordfield']}) return false;
					}
					break;

				// Default: meh?
				default:
					throw new Exception('Invalid/unknown/missing password encryption algorithm.');
			}

			// Return
			return true;
		}


	}


?>