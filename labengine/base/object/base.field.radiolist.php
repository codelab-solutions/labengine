<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   Field class: radio list field
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_FIELD_RADIOLIST extends BASE_FIELD_OPTIONSFIELD
	{


		/**
		 *  Set items per row
		 *  @access public
		 *  @param integer $itemsPerRow Items per row
		 *  @return void
		 */

		public function setItemsPerRow ( $itemsPerRow )
		{
			$this->param['itemsperrow']=$itemsPerRow;
		}


		/**
		 *  Set additional option data
		 *  @access public
		 *  @param array $optionData option data
		 *  @return void
		 */

		public function setOptionData ( $optionData )
		{
			$this->param['optiondata']=$optionData;
		}


		/**
		 *  Set list separator
		 *  @access public
		 *  @param string $separator List separator
		 *  @return void
		 */

		public function setListSeparator ( $separator )
		{
			$this->param['list_separator']=$separator;
		}


		/**
		 *  Display the field
		 *  @access public
		 *  @return string contents
		 */

		public function displayField( $value, $row=NULL )
		{
			// Get data
			$option=$this->getOptions();

			// Style
			$style=array();
			if (!empty($this->param['fieldstyle'])) $style[]=$this->param['fieldstyle'];
//			if (!empty($this->param['comment'])) $style[]='width: 40% !important';

			// Class
			$class=array();
			$class[]='form-control';
			if (!empty($this->param['fieldclass'])) $class[]=$this->param['fieldclass'];

			// Items per row
			$radioStyle='';
			if (!empty($this->param['itemsperrow']))
			{
				if ($this->param['itemsperrow']==999)
				{
					$radioStyle='float: left; margin-right: 10px;';
				}
				else
				{
					$radioStyle='float: left; width: '.floor(100/$this->param['itemsperrow']).'%';
				}
			}

			// Field begins
			$c='<div class="'.$this->getContentWidthClass().'">';
			foreach ($option as $optionVal => $optionDesc)
			{
				$c.='<div class="radio" style="'.$radioStyle.'">';
				$c.='<label>';
				$c.='<input';
				$c.=' type="radio"';
				$c.=' id="'.$this->tag.'_'.$optionVal.'"';
				$c.=' name="'.$this->tag.'"';
				$c.=' value="'.htmlspecialchars($optionVal).'"';
				if ($optionVal==$value) $c.=' checked="checked"';
				if (!empty($style)) $c.=' style="'.join('; ',$style).'"';
				if (!empty($this->param['disabled'])) $c.=' disabled="disabled"';
				if (!empty($this->param['keephiddenvalue'])) $c.=' data-keephiddenvalue="1"';
				if (isset($this->param['event']) && is_array($this->param['event']))
				{
					foreach ($this->param['event'] as $eventType => $eventContent) $c.=' '.$eventType.'="'.$eventContent.'"';
				}
				$c.='>'.htmlspecialchars($optionDesc);
				$c.='</label>';
				$c.='</div>';
			}

			// Comment
			$c.=$this->displayComment();

			$c.='</div>';

			// Return
			return $c;
		}


	}


?>