<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   The base list filter class
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_LIST_FILTER
	{

		var $tag = '';

		var $column = '';

		var $listObject = NULL;

		var $filterLevel = 1;

		var $filterColumn = NULL;

		var $filterTitle = '';

		var $filterActions = '';

		var $filterExact = FALSE;

		var $fieldSource = 'list';

		var $fieldSourceList = FALSE;

		var $fieldSourceDAO = FALSE;

		var $fieldSourceDAOkeyField = FALSE;

		var $fieldSourceDAOvalueField = FALSE;

		var $fieldSourceDAOloadParam = FALSE;

		var $filterEvent = array();

		var $filterWidthClass = NULL;

		var $error = FALSE;


		public static $MAP_col_layout = array(
			'1' => '12',
			'2' => '6',
			'3' => '4',
			'4' => '3',
			'6' => '2'
		);


		public function setFilterLevel ( $filterLevel )
		{
			$this->filterLevel=$filterLevel;
		}
		public function getFilterLevel ()
		{
			return $this->filterLevel;
		}

		public function setColumn ( $column )
		{
			$this->filterColumn=$column;
		}

		public function getColumn()
		{
			return $this->filterColumn;
		}

		public function setTitle ( $title )
		{
			$this->filterTitle=$title;
		}

		public function getTitle()
		{
			return $this->filterTitle;
		}

		public function setActions ( $actions )
		{
			$this->filterActions=$actions;
		}

		public function setExact ( $exact )
		{
			$this->filterExact=$exact;
		}

		public function getExact()
		{
			return $this->filterExact;
		}

		public function setFilterWidthClass ( $filterWidthClass )
		{
			$this->filterWidthClass=$filterWidthClass;
		}

		public function setError ( $error )
		{
			$this->error=$error;
		}


		/**
		 *  Set source list
		 *  @access public
		 *  @param string title
		 *  @return void
		 */

		public function setSourceList ( $sourceList )
		{
			$this->fieldSourceList=$sourceList;
		}


		/**
		 *  Set source dataobject
		 *  @access public
		 *  @param string dataobject class
		 *  @param string key field
		 *  @param string value field
		 *  @return void
		 */

		public function setSourceDAO ( $sourceDAO, $keyField, $valueField )
		{
			$this->fieldSource='dao';
			$this->fieldSourceDAO=$sourceDAO;
			$this->fieldSourceDAOkeyField=$keyField;
			$this->fieldSourceDAOvalueField=$valueField;
		}


		/**
		 *  Get options
		 *  @access public
		 *  @return array options
		 */

		public function getOptions()
		{
			global $LAB;
			$option=array();
			switch($this->fieldSource)
			{
				case 'query':
					$dataset=$LAB->DB->querySelectSQL($this->param['source_query']);
					foreach ($dataset as $row)
					{
						$option[$row['value']]=$row['description'];
					}
					break;
				case 'dao':
				case 'dataobject':
					if ($this->fieldSourceDAO instanceof BASE_DATAOBJECT)
					{
						$DAO=$this->fieldSourceDAO;
					}
					else
					{
						$cName=$this->fieldSourceDAO;
						$DAO=new $cName();
					}
					if ($this->fieldSourceDAOloadParam instanceof BASE_DATAOBJECT_PARAM)
					{
						$loadListParam=$this->fieldSourceDAOloadParam;
					}
					else
					{
						$loadListParam=new BASE_DATAOBJECT_PARAM();
					}
					$loadListParam->addColumns($this->fieldSourceDAOkeyField);
					$loadListParam->addColumns($this->fieldSourceDAOvalueField);
					if ($DAO->_param['setord'])
					{
						$loadListParam->addOrderBy('ord');
					}
					else
					{
						$loadListParam->addOrderBy($this->fieldSourceDAOvalueField);
					}
					$dataset=$DAO->loadList($loadListParam);
					$data=array();
					foreach ($dataset as $drow) $option[$drow[$this->fieldSourceDAOkeyField]]=$drow[$this->fieldSourceDAOvalueField];
					break;
				case 'func':
					$option=call_user_func($this->param['source_func'],$this);
					break;
				default:
					$option=&$this->fieldSourceList;
					break;
			}
			return $option;
		}


		/**
		 *   Add an event
		 *   @access public
		 *   @param string $eventType event type
		 *   @param string $eventAction event action
		 *   @return void
		 */

		public function addEvent ( $eventType, $eventAction )
		{
			if (!empty($this->filterEvent[$eventType]))
			{
				$this->filterEvent[$eventType].=$eventAction;
			}
			else
			{
				$this->filterEvent[$eventType]=$eventAction;
			}
		}


		/**
		 *   Get filter value
		 *   @access public
		 *   @return string
		 */

		public function getValue()
		{
			global $LAB;

			// Check
			if (!is_object($this->listObject)) return NULL;

			// Value
			$value=$LAB->SESSION->get('list_'.$this->listObject->param['id'].'_filter_'.$this->tag);
			return $value;
		}


		/**
		 *  Apply criteria
		 *  @access public
		 *  @param string value
		 *  @param array row
		 *  @return void
		 */

		public function applyCriteria ( $cValue, &$paramObject )
		{
			// Init
			if ($this->filterColumn!==NULL)
			{
				$filterColumnArray=str_array($this->filterColumn);
			}
			else
			{
				$filterColumnArray=array($this->tag);
			}

			// Build criteria
			$where=array();
			$having=array();
			foreach ($filterColumnArray as $filterColumn)
			{
				if (strpos($filterColumn,'->')!==FALSE)
				{
					$paramObject->addColumns($filterColumn);
					$partsArr=preg_split('/->/',$filterColumn);
					if (!strncasecmp($partsArr[sizeof($partsArr)-1],'prop_',5))
					{
						$having[]=($partsArr[sizeof($partsArr)-2].'_'.$partsArr[sizeof($partsArr)-1].$cValue);
					}
					else
					{
						$where[]=($partsArr[sizeof($partsArr)-2].'.'.$partsArr[sizeof($partsArr)-1].$cValue);
					}
				}
				elseif (strpos($filterColumn,'.')!==FALSE)
				{
					list($tbl,$col)=preg_split('/\./',$filterColumn,2);
					if (!strncmp($col,'prop_',5))
					{
						$paramObject->addColumns($tbl.'_'.$col);
						$having[]=($tbl.'_'.$col.$cValue);
					}
					else
					{
						$where[]=($tbl.'.'.$col.$cValue);
					}
				}
				elseif (strpos($this->tag,'prop_')!==FALSE)
				{
					$paramObject->addColumns($filterColumn);
					$having[]=($filterColumn.$cValue);
				}
				elseif($filterColumn[0]!=strtolower($filterColumn[0]))
				{
					$paramObject->addColumns($filterColumn);
					$having[]=($filterColumn.$cValue);
				}
				else
				{
					$where[]=($this->listObject->data->_param['table'].'.'.$filterColumn.$cValue);
				}
			}

			// Check
			if (sizeof($where) && sizeof($having))
			{
				throw new Exception('Cannot have columns that mix WHERE and HAVING criteria.');
			}

			// Build
			if (sizeof($where)>1)
			{
				$paramObject->addWhere('(('.join(') or (',$where).'))');
			}
			elseif (sizeof($where)==1)
			{
				$paramObject->addWhere($where[0]);
			}
			if (sizeof($having)>1)
			{
				$paramObject->addHaving('(('.join(') or (',$having).'))');
			}
			elseif (sizeof($having)==1)
			{
				$paramObject->addHaving($having[0]);
			}
		}



		/**
		 *  Apply criteria
		 *  @access public
		 *  @param string value
		 *  @param array row
		 *  @return void
		 */

		public function getCriteria ( $value, &$paramObject )
		{
			if (!mb_strlen($value)) return;

			if ($this->filterExact)
			{
				$cValue="='".addslashes($value)."'";
			}
			else
			{
				$cValue=" like '%".addslashes($value)."%'";
			}

			$this->applyCriteria($cValue, $paramObject);
		}


		/**
		 *  Display beginning block
		 *  @access public
		 *  @return string contents
		 */

		public function displayBeginningBlock( $value, $row=NULL )
		{
			$c='<div class="form-group '.oneof($this->filterWidthClass,'col-md-'.static::$MAP_col_layout[$this->listObject->listFiltersPerRow]).'">';
			return $c;
		}


		/**
		 *  Display the label
		 *  @access public
		 *  @return string contents
		 */

		public function displayLabel( $value, $row=NULL )
		{
			$c='<label for="filter_'.$this->tag.'">';
			$c.=$this->getTitle().':';
			if (!empty($this->filterActions)) $c.='<span class="filter-actions">'.$this->filterActions.'</span>';
			if (!empty($this->error)) $c.=' <span data-toggle="tooltip" data-placement="top" title="'.htmlspecialchars($this->error).'" style="color: #920000; cursor: help"><span class="icon-attention"></span></span>';
			$c.='</label>';
			return $c;
		}


		/**
		 *  Display the field
		 *  @access public
		 *  @return string contents
		 */

		public function displayField( $value, $row=NULL )
		{
			$c='<div class="alert alert-danger">Function <b>displayField()</b> not implemented for this class.</div>';
			return $c;
		}


		/**
		 *  Display beginning block
		 *  @access public
		 *  @return string contents
		 */

		public function displayEndingBlock( $value, $row=NULL )
		{
			$c='</div>';
			return $c;
		}


		/**
		 *  Display filter
		 *  @access public
		 *  @return contents
		 */

		public function display ()
		{
			$c=$this->displayBeginningBlock($this->getValue());
			$c.=$this->displayLabel($this->getValue());
			$c.=$this->displayField($this->getValue());
			$c.=$this->displayEndingBlock($this->getValue());
			return $c;
		}


	}


?>