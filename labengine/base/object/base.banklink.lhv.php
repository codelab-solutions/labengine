<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   BankLink integration: LHV
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_BANKLINK_LHV extends BASE_BANKLINK_IPIZZA
	{


		/**
		 *   Bank ID
		 *   @var string
		 *   @static
		 */

		public static $BANK_id = 'lhv';


		/**
		 *   Bank destination URL
		 *   @var string
		 */

		public $BANK_destination_url = 'https://www.lhv.ee/banklink';


	}


?>