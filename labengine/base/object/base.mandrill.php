<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   Mandrill API wrapper class
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_MANDRILL
	{

		/**
		 *   Mandrill instance
		 *   @var PHPExcel
		 *   @public
		 */

		public $MANDRILL = NULL;


		/**
		 *   Init Mandrill API
		 *   @public
		 *   @static
		 *   @param string $apiKey API key
		 *   @return BASE_PHPEXCEL
		 */

		public static function init( $apiKey=null )
		{
			global $LAB;

			// Init
			require_once 'base/contrib/Mandrill/Mandrill.php';

			// Key
			if (empty($apiKey)) $apiKey=$LAB->CONFIG->get('mandrill.apikey');
			if (!mb_strlen($apiKey)) throw new Exception('API key not provided nor in the config.');

			// Create class instance
			$retClass=new BASE_MANDRILL();

			// Create and attach PHPExcel main class instance
			$retClass->MANDRILL=new Mandrill($apiKey);

			// Return
			return $retClass;
		}


	}


?>