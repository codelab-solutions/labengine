<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   Field class: hidden field
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_FIELD_HIDDEN extends BASE_FIELD
	{


		/**
		 *  Data validate
		 *  @access public
		 *  @return boolean
		 */

		public function validate( $value )
		{
			return '';
		}


		/**
		 *  Log data
		 *  @access public
		 *  @return boolean
		 */

		public function getLogData( $dataObject, $operation, &$logValuesArray )
		{
			if (empty($this->param['title']) && preg_match("/_oid$/",$this->tag)) return;
			return parent::getLogData($dataObject,$operation,$logValuesArray);
		}


		/**
		 *  Display beginning block
		 *  @access public
		 *  @return string contents
		 */

		public function displayBeginningBlock( $value, $row=NULL )
		{
			return '';
		}


		/**
		 *  Display ending block
		 *  @access public
		 *  @return string contents
		 */

		public function displayEndingBlock( $value, $row=NULL )
		{
			return '';
		}


		/**
		 *  Display the label
		 *  @access public
		 *  @return string contents
		 */

		public function displayLabel( $value, $row=NULL )
		{
			return '';
		}


		/**
		 *  Display the field
		 *  @access public
		 *  @return string contents
		 */

		public function displayField( $value, $row=NULL )
		{
			// Class
			$class=array();
			if (!empty($this->param['rowclass'])) $class[]=$this->param['rowclass'];
			if (!empty($this->param['fieldclass'])) $class[]=$this->param['fieldclass'];

			$value = htmlspecialchars($value);
			$value = str_replace('{','&#123;',$value);
			$value = str_replace('}','&#125;',$value);
			$value = str_replace('[','&#091;',$value);
			$value = str_replace(']','&#093;',$value);
			$c ='<input type="hidden"';
			$c.=' id="'.$this->tag.'"';
			$c.=' name="'.$this->tag.'"';
			$c.=' value="'.$value.'"';
			if (sizeof($class))	$c.=' class="'.join(' ',$class).'"';
			$c.='>';
			return $c;
		}


	}


?>