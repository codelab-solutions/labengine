<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   Field class: date field
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_FIELD_DATE extends BASE_FIELD_TEXT
	{


		/**
		 *   Sets min date to jquery datepicker.
		 *   @param string $minDate  Set the beginning date as a string of periods and units ('+1Months +10Days').
		 */

		public function setMinDate($minDate)
		{
			$this->param["mindate"]=$minDate;
		}


		/**
		 *   Sets max date to jquery datepicker.
		 *   @param string $maxDate  Set the beginning date as a string of periods and units ('+1Months +10Days').
		 */

		public function setMaxDate($maxDate)
		{
			$this->param["maxdate"]=$maxDate;
		}


		/**
		 *   Show time in list?
		 *   @param bool $showTime Show time in list
		 */

		public function setListShowTime( $showTime )
		{
			$this->param['list_showtime']=$showTime;
		}


		/**
		 *   Show seconds in list?
		 *   @param bool $showSeconds Show seconds in list
		 */

		public function setListShowTimeSeconds( $showSeconds )
		{
			$this->param['list_showtimesec']=$showSeconds;
		}


		/**
		 *   Display value
		 *   @access public
		 *   @return mixed value
		 */

		public function displayValue()
		{
			global $LAB;
			$value=$this->getValue();
			return $LAB->DATETIME->formatDate($value,$this->param['list_showtime'],$this->param['list_showtimesec']);
		}


		/**
		 *  Data validate
		 *  @access public
		 *  @return boolean
		 */

		public function validate( $value )
		{
			global $LAB;

			// Run general validation
			$err=parent::validate($value);
			if (mb_strlen($err)) return $err;

			// Empty?
			if (empty($value)) return '';

			if (!empty($this->param['mindate']) && ($LAB->DATETIME->toTimestamp($value)<strtotime($this->param['mindate']." midnight"))) return sprintf($LAB->LOCALE->get('BASE.COMMON.Error.NumberFld.MinValue'),$LAB->DATETIME->formatDate($this->param['mindate']));
			if (!empty($this->param['maxdate']) && ($LAB->DATETIME->toTimestamp($value)>strtotime($this->param['maxdate']." midnight"))) return sprintf($LAB->LOCALE->get('BASE.COMMON.Error.NumberFld.MaxValue'),$LAB->DATETIME->formatDate($this->param['maxdate']));

			if (!$LAB->DATETIME->validateDateInput($value))
			{
				return '[BASE.FORM.Error.InvalidDate]';
			}

			// Validates
			return '';
		}


		/**
		 *  Save value
		 *  @access public
		 *  @return mixed value
		 */

		public function saveValue()
		{
			global $LAB;
			$value=$this->getValue();
			if (empty($value) || $value=='0000-00-00') return '0000-00-00';
			if ($value=='0000-00-00 00:00:00') return '0000-00-00 00:00:00';
			if (!preg_match("/^(\d\d\d\d-\d\d-\d\d)$/",$value))
			{
				$value=$LAB->DATETIME->toYMD($value);
			}
			return $value;
		}


		/**
		 *   List value
		 *   @access public
		 *   @return mixed value
		 */

		public function listValue( $value, &$row )
		{
			global $LAB;
			if ($value=='0000-00-00' || $value=='0000-00-00 00:00:00') return '';
			if (!empty($this->param['list_format']))
			{
				return date($this->param['list_format'],strtotime($value));
			}
			else
			{
				return parent::listValue($LAB->DATETIME->formatDate($value,$this->param['list_showtime'],$this->param['list_showtimesec']),$row);
			}
		}


		/**
		 *  Display the field
		 *  @access public
		 *  @return string contents
		 */

		public function displayField( $value, $row=NULL )
		{
			global $LAB;

			// Hack
			if ($value=='0000-00-00' || $value=='0000-00-00 00:00:00') $value='';
			if (!empty($value)) $value=$LAB->DATETIME->formatDate($value);

			// Style
			$style=array();
			if (!empty($this->param['fieldstyle'])) $style[]=$this->param['fieldstyle'];
			$style[]='width: 110px !important';
			$style[]='text-align: center !important';
			$style[]='display: inline-block';

			// Class
			$class=array();
			$class[]='form-control';
			$class[]='datefield';
			if (!empty($this->param['fieldclass'])) $class[]=$this->param['fieldclass'];

			// Render
			$c='<div class="'.$this->getContentWidthClass().'">';

			$value = htmlspecialchars($value);
			$value = str_replace('{','&#123;',$value);
			$value = str_replace('}','&#125;',$value);
			$value = str_replace('[','&#091;',$value);
			$value = str_replace(']','&#093;',$value);
			$c.='<input type="text"';
			$c.=' id="'.$this->tag.'"';
			$c.=' name="'.$this->tag.'"';
			$c.=' value="'.$value.'"';
			$c.=' data-originalvalue="'.$value.'"';
			$c.=' data-mask="'.$LAB->DATETIME->getDateFormat('mask').'"';
			$c.=' data-date-format="'.$LAB->DATETIME->getDateFormat('datepicker').'"';
			$c.=' autocomplete="off"';
			$c.=' class="'.join(' ',$class).'"';
			$c.=' maxlength="10"';
			if (!empty($style)) $c.=' style="'.join('; ',$style).'"';
			if (!empty($this->param['readonly'])) $c.=' readonly="readonly"';
			if (!empty($this->param['disabled'])) $c.=' disabled="disabled"';
			if (!empty($this->param['emptyformat'])) $c.=' data-emptyformat="'.htmlspecialchars($this->param['emptyformat']).'"';
			if (!empty($this->param['keephiddenvalue'])) $c.=' data-keephiddenvalue="1"';
			if (isset($this->param['event']) && is_array($this->param['event']))
			{
				foreach ($this->param['event'] as $eventType => $eventContent) $c.=' '.$eventType.'="'.$eventContent.'"';
			}
			if (isset($this->param['data']) && is_array($this->param['data']))
			{
				foreach ($this->param['data'] as $dataKey => $dataValue) $c.=' data-'.$dataKey.'="'.htmlspecialchars($dataValue).'"';
			}
			$c.='>';
			$c.=' <span style="display: inline-block" class="icon-calendar" onclick="FORM.openDatepicker(\'#'.$this->tag.'\')"></span>';

			// Comment
			$c.=$this->displayComment();

			$c.='</div>';

			// Return
			return $c;
		}


	}


?>