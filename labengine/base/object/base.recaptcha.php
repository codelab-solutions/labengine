<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   Google ReCaptcha library wrapper class
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_RECAPTCHA
	{


		/**
		 *   Site key
		 *   @var string
		 *   @public
		 */

		public static $RECAPTCHA_sitekey = '';


		/**
		 *   Secret key
		 *   @var string
		 *   @public
		 */

		public static $RECAPTCHA_secret = '';


		/**
		 *   Recaptcha object
		 *   @var \ReCaptcha\ReCaptcha
		 *   @public
		 */

		public $RECAPTCHA = null;



		/**
		 *   Init Braintree
		 *   @public
		 *   @static
		 *   @return BASE_RECAPTCHA
		 */

		public static function init( $siteKey=false, $secret=false )
		{
			global $LAB;

			if (!is_object($LAB->CACHE->reCaptcha))
			{
				$LAB->CACHE->reCaptcha=new stdClass();
				$LAB->CACHE->reCaptcha->sitekey=( $siteKey!==false ? $siteKey : static::$RECAPTCHA_sitekey );
				$LAB->CACHE->reCaptcha->secret=( $secret!==false ? $secret : static::$RECAPTCHA_secret );

				spl_autoload_register(function ($class) {
					global $LAB;
					$basePath=stream_resolve_include_path('base/contrib');

					if (substr($class, 0, 10) !== 'ReCaptcha\\') {
						/* If the class does not lie under the "ReCaptcha" namespace,
						 * then we can exit immediately.
						 */
						return;
					}

					/* All of the classes have names like "ReCaptcha\Foo", so we need
					 * to replace the backslashes with frontslashes if we want the
					 * name to map directly to a location in the filesystem.
					 */
					$class = str_replace('\\', '/', $class);

					/* First, check under the current directory. It is important that
					 * we look here first, so that we don't waste time searching for
					 * test classes in the common case.
					 */
					$path = $basePath.'/'.$class.'.php';
					if (is_readable($path)) {
							require_once $path;
					}
				});
			}

			$className=get_called_class();
			$RECAPTCHA=new $className();
			$RECAPTCHA->RECAPTCHA=new \ReCaptcha\ReCaptcha($LAB->CACHE->reCaptcha->secret);
			return $RECAPTCHA;
		}


		/**
		 *   Get reCaptcha string
		 *   @public
		 *   @param string $additionalClasses Additional classes
		 *   @return string
		 *   @throws Exception
		 */

		public function display( $additionalClasses='' )
		{
			global $LAB;

			// Add JS
			$LAB->RESPONSE->JS->addExternalJS('https://www.google.com/recaptcha/api.js?hl='.$LAB->REQUEST->requestLang,'recaptcha_'.$LAB->REQUEST->requestLang);

			// Return
			return '<div class="g-recaptcha'.($additionalClasses!=''?' '.$additionalClasses:'').'" data-sitekey="'.htmlspecialchars($LAB->CACHE->reCaptcha->sitekey).'"></div>';
		}


		/**
		 *   Verify
		 *   @public
		 *   @param string $recaptchaResponse Response
		 *   @return \ReCaptcha\Response
		 *
		 */

		public function verify( $recaptchaResponse=false )
		{
			$recaptchaResponse=($recaptchaResponse===false?$_POST['g-recaptcha-response']:$recaptchaResponse);
			return $this->RECAPTCHA->verify($recaptchaResponse,BASE::remoteIP());
		}


	}


?>