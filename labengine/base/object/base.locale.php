<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   The locale class
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_LOCALE
	{


		/**
		 *   Current language
		 *   @var string
		 *   @access public
		 */

		public $localeLanguage = '';


		/**
		 *   Languages available
		 *   @var array
		 *   @access public
		 */

		public $localeLanguageSet = array();

		/**
		 *   Default language
		 *   @var string
		 *   @access public
		 */

		public $localeLanguageDefault = FALSE;


		/**
		 *   Loaded modules
		 *   @var array
		 *   @access public
		 */

		public $localeLoaded = array();


		/**
		 *   Locale data
		 *   @var array
		 *   @access public
		 */

		public $localeData = FALSE;


		/**
		 *   Available locales
		 *   @var array
		 *   @static
		 *   @access public
		 */

		public static $localeInfo=array(

			'en' => array(
				'tag'               => 'EN',
				'name'              => 'English',
				'name_short'        => 'English',
				'name_sel'          => 'In English',
				'name_eng'          => 'English',
				'phplocale'         => 'en_US',
				'dateformat_disp'   => 'd.m.Y',
				'dateformat_check'  => '(\d\d)\.(\d\d)\.(\d\d\d\d)'
			),

			'et' => array(
				'tag'               => 'ET',
				'name'              => 'Eesti keel',
				'name_short'        => 'Eesti',
				'name_sel'          => 'Eesti keeles',
				'name_eng'          => 'Estonian',
				'phplocale'         => 'et_EE',
				'dateformat_disp'   => 'd.m.Y',
				'dateformat_check'  => '(\d\d)\.(\d\d)\.(\d\d\d\d)'
			),

			'lv' => array(
				'tag'               => 'LV',
				'name'              => 'Latviešu',
				'name_short'        => 'Latviešu',
				'name_sel'          => 'Latviešu',
				'name_eng'          => 'Latvian',
				'phplocale'         => 'lv_LV',
				'dateformat_disp'   => 'd.m.Y',
				'dateformat_check'  => '(\d\d)\.(\d\d)\.(\d\d\d\d)'
			),

			'lt' => array(
				'tag'               => 'LT',
				'name'              => 'Lietuvių',
				'name_short'        => 'Lietuvių',
				'name_sel'          => 'Lietuvių',
				'name_eng'          => 'Lithuanian',
				'phplocale'         => 'lt_LT',
				'dateformat_disp'   => 'Y.m.d',
				'dateformat_check'  => '(\d\d\d\d).(\d\d).(\d\d)'
			),

			'ru' => array(
				'tag'               => 'RU',
				'name'              => 'Русский язык',
				'name_short'        => 'Русский',
				'name_sel'          => 'По-русски',
				'name_eng'          => 'Russian',
				'phplocale'         => 'ru_RU',
				'dateformat_disp'   => 'd.m.Y',
				'dateformat_check'  => '(\d\d)\.(\d\d)\.(\d\d\d\d)'
			),

			'de' => array(
				'tag'               => 'DE',
				'name'              => 'Deutsch',
				'name_short'        => 'Deutsch',
				'name_sel'          => 'Deutsch',
				'name_eng'          => 'German',
				'phplocale'         => 'de_DE',
				'dateformat_disp'   => 'd.m.Y',
				'dateformat_check'  => '(\d\d)\.(\d\d)\.(\d\d\d\d)'
			),

			'es' => array(
				'tag'               => 'ES',
				'name'              => 'Español',
				'name_short'        => 'Español',
				'name_sel'          => 'En Español',
				'name_eng'          => 'Spanish',
				'phplocale'         => 'es_ES',
				'dateformat_disp'   => 'd/m/Y',
				'dateformat_check'  => '(\d\d)\/(\d\d)\/(\d\d\d\d)'
			),

			'fi' => array
			 (
				'tag'        => 'FI',
				'name'       => 'Suomen kieli',
				'name_short' => 'Suomi',
				'name_sel'   => 'Suomeksi',
				'name_eng'   => 'Finnish',
				'phplocale'  => 'fi_FI',
				'dateformat_disp'   => 'd.m.Y',
				'dateformat_check'  => '(\d\d)\.(\d\d)\.(\d\d\d\d)'
			 ),

			'sv' => array
			 (
				'tag'        => 'SV',
				'name'       => 'Svenska',
				'name_short' => 'Svenska',
				'name_sel'   => 'På svenska',
				'name_eng'   => 'Swedish',
				'phplocale'  => 'sv_SE',
				'dateformat_disp'   => 'd.m.Y',
				'dateformat_check'  => '(\d\d)\.(\d\d)\.(\d\d\d\d)'
			 ),

			'fr' => array
			(
				'tag'        => 'FR',
				'name'       => 'Français',
				'name_short' => 'Français',
				'name_sel'   => 'En français',
				'name_eng'   => 'French',
				'phplocale'  => 'fr_FR',
				'dateformat_disp'   => 'd.m.Y',
				'dateformat_check'  => '(\d\d)\.(\d\d)\.(\d\d\d\d)'
			),

			'it' => array(
				'tag'        => 'IT',
				'name'       => 'Italiano',
				'name_short' => 'Italiano',
				'name_sel'   => 'In italiano',
				'name_eng'   => 'Italian',
				'phplocale'  => 'it_IT',
				'dateformat_disp'   => 'd.m.Y',
				'dateformat_check'  => '(\d\d)\.(\d\d)\.(\d\d\d\d)'
			),

		);


		/**
		 *   The constructor
		 *   @access public
		 *   @return void
		 */

		public function __construct()
		{
			setlocale(LC_ALL,'en_US.UTF-8');
		}


		/**
		 *   Load the locale file
		 *   @access private
		 *   @param string $localeFile File name
		 *   @param string $localeModule Module tag
		 *   @return void
		 *   @throws Exception
		 */

		private function _loadLocaleFile( $localeFile, $localeModule )
		{
			// Get file contents
			$fileFullPath=stream_resolve_include_path($localeFile);
			if ($fileFullPath===FALSE) return;
			$fileContents=file($fileFullPath,FILE_IGNORE_NEW_LINES);

			// Parse
			foreach ($fileContents as $line)
			{
				$line=trim($line);
				if ($line[0]=='/' || $line[0]=='#') continue;
				list($key,$val)=preg_split("/\s+/", $line,2);
				if (strlen($key) && strlen($val)) $this->set(trim($key),trim($val));
			}

			// Set module as loaded
			if ($localeModule!==FALSE)
			{
				$this->localeLoaded[$localeModule]=true;
			}
		}


		/**
		 *   Add a language
		 *   @access public
		 *   @param string 2-letter language tag
		 *   @return void
		 */

		public function addLanguage ( $lang )
		{
			// Check if it's a known locale
			if (!$this->localeExists($lang))
			{
				throw new Exception('Unknown locale: '.$lang);
				return;
			}

			// Set default if we don't have one yet
			if (!sizeof($this->localeLanguageSet))
			{
				$this->localeLanguageDefault = $lang;
			}

			// Add to available locales
			$this->localeLanguageSet[] = $lang;
		}


		/**
		 *   Get default language
		 *   @access public
		 *   @return string 2-letter locale tag
		 */

		public function getDefaultLanguage()
		{
			return $this->localeLanguageDefault;
		}


		/**
		 *   Check is the locale exists
		 *   @access public
		 *   @param string Locale tag
		 *   @return boolean TRUE if exists, FALSE if not
		 */

		public function localeExists ( $localeTag )
		{
			return array_key_exists($localeTag,static::$localeInfo);
		}


		/**
		 *   Get locale name
		 *   @access public
		 *   @static
		 *   @param string Locale tag
		 *   @param string $name Name Type
		 *   @throws Exception
		 *   @return string
		 */

		public static function getName ( $localeTag, $name=null )
		{
			global $LAB;

			if (empty(static::$localeInfo[mb_strtolower($localeTag)])) throw new Exception('Unknown locale: '.$localeTag);
			if (!empty($name))
			{
				return static::$localeInfo[mb_strtolower($localeTag)]['name_'.$name];
			}
			else
			{
				return static::$localeInfo[mb_strtolower($localeTag)]['name'];
			}
		}


		/**
		 *   Load the locale
		 *   @access public
		 *   @param string $localeTag Locale tag
		 *   @return void
		 *   @throws Exception
		 */

		public function loadLocale( $localeTag )
		{
			global $LAB;

			// Already loaded?
			if ($this->localeLanguage==$localeTag) return;

			// Init
			$this->localeData=array();
			$this->localeLoaded=array();

			// Load base locale
			if ($localeTag!='en') $this->_loadLocaleFile('base/locale/en.locale','base');
			$this->_loadLocaleFile('base/locale/'.$localeTag.'.locale','base');

			// CMS: load CMS locale
			if (is_object($LAB->CMS))
			{
				if ($localeTag!='en') $this->_loadLocaleFile('cms/locale/en.locale','cms');
				$this->_loadLocaleFile('cms/locale/'.$localeTag.'.locale','cms');
			}

			// Load site locale
			if ($localeTag!='en') $this->_loadLocaleFile('site/locale/en.locale','site');
			$this->_loadLocaleFile('site/locale/'.$localeTag.'.locale','site');

			// Set loaded language
			$this->localeLanguage=$localeTag;
		}



		/**
		 *   Set a locale string
		 *   @access public
		 *   @param string variable
		 *   @param string value
		 *   @return void
		 */

		public function set ( $varName, $varValue )
		{
			$varName=mb_strtolower($varName);
			$this->localeData[$varName]=$varValue;
		}



		/**
		 *   Get a localized string
		 *   @access public
		 *   @param string variable
		 *   @return string localized string
		 *   @throws Exception
		 */

		public function get ( $varName )
		{
			// Check that we are loaded
			if (empty($this->localeLanguage))
			{
				throw new Exception('Locale not loaded.');
			}

			// Init
			$varFunction='';
			$varParam=array();

			// Split into var and function
			if (strpos($varName,'|')!==FALSE)
			{
				list($varName,$varFunction)=preg_split('/\|/',$varName,2);
			}

			// Split parameters
			if (strpos($varName,',')!==FALSE)
			{
				$varParam=preg_split('/,/',$varName);
				$varName=array_shift($varParam);
			}

			// Make var/function names case insensitive
			$varName=mb_strtolower($varName);
			$varFunction=mb_strtolower($varFunction);

			// Locale item does not exist
			if (!isset($this->localeData[$varName])) return FALSE;

			// Base return value
			$returnVal=$this->localeData[$varName];

			// Parameters
			if (sizeof($varParam))
			{
				$returnVal=vsprintf($this->localeData[$varName],$varParam);
			}

			// Function
			if (strlen($varFunction))
			{
				switch($varFunction)
				{
					case 'tolower':
						$returnVal=mb_strtolower($returnVal,'UTF-8');
						break;
					case 'toupper':
						$returnVal=mb_strtoupper($returnVal,'UTF-8');
						break;
					default:
						return '[Unknown modifier function: '.$varFunction.']';
				}
			}

			// Return
			return $returnVal;
		}


	}


	?>