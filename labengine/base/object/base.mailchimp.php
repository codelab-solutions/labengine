<?


	/**
	 *
	 *   LabEngine™ 7
	 *   MailChimp integration class
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_MAILCHIMP
	{


		/**
		 *   API URL
		 *   @var string
		 */

		public $MAILCHIMP_api_url = null;


		/**
		 *   API key
		 *   @var string
		 */

		public $MAILCHIMP_api_key = null;


		/**
		 *   Constructor.
		 *   @param null $apiURL
		 *   @param null $apiKey
		 *   return BASE_MAILCHIMP
		 */

		public function __construct ( $apiURL=null, $apiKey=null )
		{
			// Set parameters if passed
			if (!empty($apiURL))
			{
				$this->MAILCHIMP_api_url=$apiURL;
			}
			if (!empty($apiKey))
			{
				$this->MAILCHIMP_api_key=$apiKey;
			}
		}


		/**
		 *   Set API URL
		 *   @access public
		 *   @param string $apiURL API URL
		 *   @return void
		 */

		public function setApiURL( $apiURL )
		{
			$this->MAILCHIMP_api_url=$apiURL;
		}


		/**
		 *   Set API key
		 *   @access public
		 *   @param string $apiURL API URL
		 *   @return void
		 */

		public function setApiKey( $apiKey )
		{
			$this->MAILCHIMP_api_key=$apiKey;
		}


		/**
		 *   Subscribe user to list
		 *   @access public
		 *   @param string $list_id List ID
		 *   @param string $email E-mail address
		 *   @param array $additionalData Additional data
		 *   @param bool $confirm
		 *   @param bool $throwException
		 *   @return bool
		 *   @throws Exception
		 */

		public function subscribe( $list_id, $email, $additionalData=array(), $confirm=true, $throwException=true )
		{
			global $LAB;

			// Sanity checks
			if (empty($this->MAILCHIMP_api_url)) throw new Exception('API URL not set.');
			if (empty($this->MAILCHIMP_api_key)) throw new Exception('API key not set.');
			if (empty($list_id)) throw new Exception('List ID not set.');
			if (empty($email)) throw new Exception('E-mail address empty.');

			// Subscribe
			try
			{
				$request=array();
				$request['apikey']=$this->MAILCHIMP_api_key;
				$request['id']=$list_id;
				$request['email']=array();
				$request['email']['email']=$email;
				if (sizeof($additionalData))
				{
					$request['merge_vars']=$additionalData;
				}
				$request['update_existing']=true;
				$request['replace_interests']=false;
				$request['double_optin']=($confirm?true:false);
				$request=json_encode($request,JSON_FORCE_OBJECT|JSON_HEX_QUOT);

				$HTTP=new BASE_HTTPREQUEST();
				$HTTP->setURL($this->MAILCHIMP_api_url.'/lists/subscribe');
				$HTTP->setRequestMethod('POST');
				$HTTP->setRawPostData($request);
				$mailChimpResponse=$HTTP->send();
				$mailChimpResponse=json_decode($mailChimpResponse,true);

				if ($mailChimpResponse['status']=='error')
				{
					throw new Exception($mailChimpResponse['error']);
				}

				return true;
			}
			catch (Exception $e)
			{
				if ($throwException) throw $e;
				return false;
			}
		}


		/**
		 *   Unsubscribe user from list
		 *   @param string $email E-mail address
		 *   @param bool $throwException Throw exception on failure
		 *   @return bool
		 *   @throws Exception
		 */

		public function unsubscribe( $email, $throwException=true )
		{
			global $LAB;

			// Sanity checks
			if (empty($this->MAILCHIMP_api_url)) throw new Exception('API URL not set.');
			if (empty($this->MAILCHIMP_api_key)) throw new Exception('API key not set.');
			if (empty($email)) throw new Exception('E-mail address empty.');

			// Unsubscribe
			try
			{
				$request=array();
				$request['apikey']=$this->MAILCHIMP_api_key;
				$request['email']=array();
				$request['email']['email']=$email;
				$request['delete_member']=false;
				$request['send_goodbye']=false;
				$request=json_encode($request,JSON_FORCE_OBJECT|JSON_HEX_QUOT);

				$HTTP=new BASE_HTTPREQUEST();
				$HTTP->setURL($this->MAILCHIMP_api_key.'/lists/unsubscribe');
				$HTTP->setRequestMethod('POST');
				$HTTP->setRawPostData($request);
				$mailChimpResponse=$HTTP->send();
				$mailChimpResponse=json_decode($mailChimpResponse,true);

				if ($mailChimpResponse['status']=='error')
				{
					throw new Exception($mailChimpResponse['error']);
				}

				return true;
			}
			catch (Exception $e)
			{
				if ($throwException) throw $e;
				return false;
			}
		}


	}


?>