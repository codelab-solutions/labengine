<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   The list class
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_LIST extends BASE_DISPLAYOBJECT
	{

		/**
		 *  Global actions
		 *  @var array
		 *  @access public
		 */

		public $listGlobalAction = array();


		/**
		 *  Row actions
		 *  @var array
		 *  @access public
		 */

		public $listAction = array();


		/**
		 *  List columns/fields
		 *  @var array
		 *  @access public
		 */

		public $listField = array();


		/**
		 *  Filters
		 *  @var array
		 *  @access public
		 */

		public $listFilter = array();


		/**
		 *  Filters per row
		 *  @var int
		 *  @access public
		 */

		public $listFiltersPerRow = 4;


		/**
		 *  Sorting enabled?
		 *  @var boolean
		 *  @access public
		 */

		public $listSortEnabled = FALSE;


		/**
		 *  Sort field
		 *  @var array
		 *  @access public
		 */

		public $listSortField = NULL;


		/**
		 *  Sort order
		 *  @var array
		 *  @access public
		 */

		public $listSortOrder = NULL;


		/**
		 *  Paging
		 *  @var array
		 *  @access public
		 */

		public $listPagingEnabled = FALSE;


		/**
		 *  Page size
		 *  @var array
		 *  @access public
		 */

		public $listPagingPageSize = '50';


		/**
		 *  Current page
		 *  @var array
		 *  @access public
		 */

		public $listPagingCurrentPage = 1;


		/**
		 * 	List class
		 * 	@var string
		 * 	@access  public
		 */

		public $listClass = '';


		/**
		 *  Total pages
		 *  @var array
		 *  @access public
		 */

		public $listPagingTotalPages = FALSE;


		/**
		 *  Total rows
		 *  @var array
		 *  @access public
		 */

		public $listPagingFoundRows = FALSE;


		/**
		 *  Show totals?
		 *  @var bool
		 *  @access public
		 */

		public $listShowTotal = FALSE;


		/**
		 *  Show row numbers?
		 *  @var bool
		 *  @access public
		 */

		public $listShowRowNum = FALSE;


		/**
		 *  Row number column title
		 *  @var bool
		 *  @access public
		 */

		public $listRowNumTitle = '';



		/**
		 *   Init the list
		 *   @throws Exception
		 *   @return void
		 */

		public function initList()
		{
		}


		/**
		 *   Define filters
		 *   @throws Exception
		 *   @return void
		 */

		public function initFilters()
		{
		}


		/**
		 *   Define fields/columns
		 *   @throws Exception
		 *   @return void
		 */

		public function initColumns()
		{
		}


		/**
		 *   Define global and row actions
		 *   @throws Exception
		 *   @return void
		 */

		public function initActions()
		{
		}


		/**
		 *  Set list class
		 *  @access public
		 *  @param string list class
		 *  @return void
		 */

		public function setListClass( $listClass )
		{
			$this->listClass=$listClass;
		}


		/**
		 *  Set paging enabled
		 *  @access public
		 *  @param boolean paging enabled
		 *  @return void
		 */

		public function setPagingEnabled ( $pagingEnabled )
		{
			$this->listPagingEnabled=$pagingEnabled;
		}


		/**
		 *  Get paging enabled
		 *  @access public
		 *  @return boolean paging enabled
		 */

		public function getPagingEnabled()
		{
			return $this->listPagingEnabled;
		}


		/**
		 *  Set page size
		 *  @access public
		 *  @param boolean paging enabled
		 *  @return void
		 */

		public function setPagingPageSize ( $pageSize )
		{
			$this->listPagingPageSize=$pageSize;
		}


		/**
		 *  Get page size
		 *  @access public
		 *  @return boolean paging enabled
		 */

		public function getPagingPageSize()
		{
			return $this->listPagingPageSize;
		}


		/**
		 *  Set show row numbers
		 *  @access public
		 *  @param boolean $showRowNum Show row numbers
		 *  @param string $rowNumTitle Row number column title
		 *  @return void
		 */

		public function setShowRowNum ( $showRowNum, $rowNumTitle='' )
		{
			$this->listShowRowNum=$showRowNum;
			$this->listRowNumTitle=$rowNumTitle;
		}


		/**
		 *  Set nested
		 *  @access public
		 *  @param boolean paging enabled
		 *  @return void
		 */

		public function setNested ( $nested )
		{
			$this->param['nested']=$nested;
		}


		/**
		 *  Get nested
		 *  @access public
		 *  @return boolean paging enabled
		 */

		public function getNested()
		{
			return $this->param['nested'];
		}


		/**
		 *  Set nested max depth
		 *  @access public
		 *  @param boolean paging enabled
		 *  @return void
		 */

		public function setNestedMaxDepth ( $maxDepth )
		{
			$this->param['nested_maxdepth']=$maxDepth;
		}


		/**
		 *  Get nested max depth
		 *  @access public
		 *  @return boolean paging enabled
		 */

		public function getNestedMaxDepth()
		{
			return $this->param['nested_maxdepth'];
		}


		/**
		 *  Set return link
		 *  @access public
		 *  @param boolean paging enabled
		 *  @return void
		 */

		public function setReturnLink ( $returnLink )
		{
			$this->param['returnlink']=$returnLink;
		}


		/**
		 *  Get return link
		 *  @access public
		 *  @return boolean paging enabled
		 */

		public function getReturnLink()
		{
			return $this->param['returnlink'];
		}


		/**
		 *  Set load parameters
		 *  @access public
		 *  @param BASE_DATAOBJECT_PARAM load parameters
		 *  @return void
		 */

		public function setLoadParam ( &$loadParam )
		{
			if (!($loadParam instanceof BASE_DATAOBJECT_PARAM)) throw new Exception('setLoadParam(): $loadParam must be an instance of BASE_DATAOBJECT_PARAM.');
			$this->param['loadparam']=&$loadParam;
		}


		/**
		 *  Get paging enabled
		 *  @access public
		 *  @return BASE_DATAOBJECT_PARAM load parameters
		 */

		public function getLoadParam()
		{
			return $this->param['loadparam'];
		}


		/**
		 *  Set filter required
		 *  @access public
		 *  @param boolean filter required
		 *  @return void
		 */

		public function setFilterRequired ( $filterRequired )
		{
			$this->param['filterrequired']=$filterRequired;
		}


		/**
		 *  Get filter required
		 *  @access public
		 *  @return boolean
		 */

		public function getFilterRequired()
		{
			return $this->param['filterrequired'];
		}


		/**
		 *  Set info/help text
		 *  @access public
		 *  @param string info/help text
		 *  @return void
		 */

		public function setInfo ( $info )
		{
			$this->param['info']=$info;
		}


		/**
		 *  Set no filter message
		 *  @access public
		 *  @param string no filter message
		 *  @return void
		 */

		public function setNoFilterMessage ( $noFilterMessage )
		{
			$this->param['nofiltermsg']=$noFilterMessage;
		}


		/**
		 *  Get filter required
		 *  @access public
		 *  @return string
		 */

		public function getNoFilterMessage()
		{
			return $this->param['nofiltermsg'];
		}


		/**
		 *  Set no results message
		 *  @access public
		 *  @param boolean paging enabled
		 *  @return void
		 */

		public function setNoResultsMessage ( $noResultsMessage )
		{
			$this->param['noresults']=$noResultsMessage;
		}


		/**
		 *  Set show results count
		 *  @access public
		 *  @param boolean show results count
		 *  @return void
		 */

		public function setShowResultCount ( $showResultCount )
		{
			$this->param['noresultcount']=($showResultCount?false:true);
		}


		/**
		 *  Set list filters per row
		 *  @access public
		 *  @param boolean paging enabled
		 *  @return void
		 */

		public function setListFiltersPerRow ( $listFiltersPerRow )
		{
			$this->listFiltersPerRow=$listFiltersPerRow;
		}


		/**
		 *  Add a row action
		 *  @access public
		 *  @return void
		 */

		public function addAction( $actionTag, $actionObject=NULL )
		{
			// Check if the action already exists
			if (isset($this->listAction[$actionTag]))
			{
				throw new Exception ('Action '.$actionTag.' already exists.');
				return NULL;
			}

			// Action passed
			if ($actionObject instanceof BASE_LIST_ACTION)
			{
				$this->listAction[$actionTag]=$actionObject;
			}

			// Create blank action
			else
			{
				$this->listAction[$actionTag]=new BASE_LIST_ACTION($actionTag);
			}

			// Return reference to action
			$retval=&$this->listAction[$actionTag];
			return $retval;
		}


		/**
		 *  Add a global action
		 *  @access public
		 *  @return void
		 */

		public function addGlobalAction( $actionTag, $actionObject=NULL )
		{
			// Check if the action already exists
			if (isset($this->listGlobalAction[$actionTag]))
			{
				throw new Exception ('Global action '.$actionTag.' already exists.');
				return NULL;
			}

			// Action passed
			if ($actionObject instanceof BASE_LIST_ACTION)
			{
				$this->listGlobalAction[$actionTag]=$actionObject;
			}

			// Create blank action
			else
			{
				$this->listGlobalAction[$actionTag]=new BASE_LIST_ACTION($actionTag);
			}

			// Return reference to action
			$retval=&$this->listGlobalAction[$actionTag];
			return $retval;
		}


		/**
		 *  Add a column
		 *  @access public
		 *  @return BASE_FIELD field object reference
		 */

		public function addField( $fieldTag, $fieldData=NULL )
		{
			// Check if the action already exists
			if (isset($this->listField[$fieldTag]))
			{
				throw new Exception('Field '.$fieldTag.' already exists.');
				return NULL;
			}

			// Field object passed
			if ($fieldData instanceof BASE_FIELD)
			{
				$this->listField[$fieldTag]=$fieldData;
			}

			// Create blank field
			elseif (strlen($fieldData))
			{
				$fld=new $fieldData();
				$this->listField[$fieldTag]=$fld;
			}

			// Otherwise - data objekti olemasolev field
			else
			{
				// Do we have a data object?
				if (!is_object($this->data)) throw new Exception('No data source defined.');
				if (!($this->data instanceof BASE_DATAOBJECT)) throw new Exception('Data source not a BASE_DATAOBJECT instance.');

				// Do we have this field?
				if (!is_object($this->data->_field[$fieldTag])) throw new Exception($fieldTag.': no such field defined in data object.');

				// Link
				$this->listField[$fieldTag]=&$this->data->_field[$fieldTag];
			}

			// Set tag/column
			if (empty($this->listField[$fieldTag]->tag)) $this->listField[$fieldTag]->tag=$fieldTag;
			if (empty($this->listField[$fieldTag]->fieldColumn)) $this->listField[$fieldTag]->fieldColumn=$fieldTag;

			// Create backreference to the field
			$this->listField[$fieldTag]->listObject=&$this;

			// Return reference to action
			$retval=&$this->listField[$fieldTag];
			return $retval;
		}


		/**
		 *  Add a filter
		 *  @access public
		 *  @return BASE_LIST_FILTER filter object reference
		 */

		public function addFilter( $filterTag, $filterData=NULL )
		{
			// Check if the action already exists
			if (isset($this->listFilter[$filterTag]))
			{
				throw new Exception('Filter '.$filterTag.' already exists.');
				return NULL;
			}

			// Filter object passed
			if ($filterData instanceof BASE_LIST_FILTER)
			{
				$fld=$this->listFilter[$filterTag]=&$filterData;
			}

			// Create blank filter
			elseif (strlen($filterData))
			{
				$fld=new $filterData();
				$this->listFilter[$filterTag]=$fld;
			}

			// Set tag
			$fld->tag=str_replace('.','_',str_replace('->','_',$filterTag));
			if ($fld->tag!=$filterTag && empty($filterData->filterColumn))
			{
				$filterData->filterColumn=$filterTag;
			}

			// Create backreference to the filter
			$fld->listObject=&$this;

			// Return reference to action
			$retval=&$this->listFilter[$filterTag];
			return $retval;
		}


		/**
		 *  Set unset parameters to default values
		 *  @access public
		 *  @return void
		 */

		public function setDefaults()
		{
			// Parent method
			parent::setDefaults();

			// List-specific defaults
			if (!isset($this->param['id']))
			{
				$this->param['id']=oneof(str_replace('.','_',$this->data->_param['table']),uniqid());
			}
			if ($this->param['nested'] && !isset($this->param['nested_parentfield'])) $this->param['nested_parentfield']='parent_oid';
		}


		/**
		 *   Apply filters & paging criteria
		 *   @throws Exception
		 *   @return bool Reload needed
		 */

		public function setCriteria()
		{
			global $LAB;

			// Apply filters
			if (!empty($_POST['filter_clear']))
			{
				foreach ($this->listFilter as $filterTag => &$filterObject)
				{
					$LAB->SESSION->set('list_'.$this->param['id'].'_filter_'.$filterObject->tag, "");
				}
				return TRUE;
			}
			if (!empty($_POST['filter_submit']))
			{
				foreach ($this->listFilter as $filterTag => &$filterObject)
				{
					$LAB->SESSION->set('list_'.$this->param['id'].'_filter_'.$filterObject->tag, $_POST['filter_'.$filterObject->tag]);
				}
				return TRUE;
			}

			// Apply paging
			if (!empty($_POST['page']))
			{
				if (intval($_POST['page'])>0) $LAB->SESSION->set('list_'.$this->param['id'].'_page',intval($_POST['page']));
				return TRUE;
			}

			// Apply sorting
			if (!empty($_POST['sort']))
			{
				if (is_object($this->listField[$_POST['sort']]))
				{
					$LAB->SESSION->set('list_'.$this->param['id'].'_sortfield',$_POST['sort']);
					if (!empty($_POST['dir']) && in_array($_POST['dir'],array('asc','desc')))
					{
						$LAB->SESSION->set('list_'.$this->param['id'].'_sortorder',$_POST['dir']);
					}
					else
					{
						$LAB->SESSION->set('list_'.$this->param['id'].'_sortorder',$this->listField[$_POST['sort']]->fieldDefaultSortOrder);
					}
				}
				if ($this->listPagingEnabled)
				{
					$LAB->SESSION->set('list_'.$this->param['id'].'_page',1);
				}
				return TRUE;
			}

			// Get paging
			if (intval($LAB->SESSION->get('list_'.$this->param['id'].'_page')))
			{
				$this->listPagingCurrentPage=intval($LAB->SESSION->get('list_'.$this->param['id'].'_page'));
			}

			// Sorting
			foreach ($this->listField as &$fieldObject)
			{
				if ($fieldObject->param['list_sortable'])
				{
					$this->listSortEnabled=TRUE;
					if (!$this->listSortField || $fieldObject->param['list_sortdefault'])
					{
						$this->listSortField=$fieldObject->tag;
						$this->listSortOrder=$fieldObject->param['list_defaultsortorder'];
					}
				}
			}
			$sortFld=$LAB->SESSION->get('list_'.$this->param['id'].'_sortfield');
			if ($sortFld)
			{
				if (isset($this->listField[$sortFld]))
				{
					$this->listSortField=$sortFld;
					$sortOrder=$LAB->SESSION->get('list_'.$this->param['id'].'_sortorder');
					$this->listSortOrder=oneof($sortOrder,$this->listField[$this->listSortField]->param['list_defaultsortorder']);
				}
				else
				{
					$LAB->SESSION->set('list_'.$this->param['id'].'_sortfield',$this->listSortField);
					$LAB->SESSION->set('list_'.$this->param['id'].'_sortorder',$this->listSortOrder);
					return TRUE;
				}
			}

			// Show totals
			foreach ($this->listField as &$fieldObject)
			{
				if (!empty($fieldObject->param['list_showtotal']))
				{
					$this->listShowTotal=TRUE;
				}
			}

			return FALSE;
		}


		/**
		 *   Check paging after data load
		 *   @throws Exception
		 *   @return void
		 */

		public function checkPaging()
		{
			global $LAB;

			// Calculate total number of pages
			$this->listPagingTotalPages=ceil($this->listPagingFoundRows/$this->listPagingPageSize);
			if (!$this->listPagingTotalPages) $this->listPagingTotalPages=1;
			if ($this->listPagingCurrentPage > $this->listPagingTotalPages)
			{
				$LAB->SESSION->set('list_'.$this->param['id'].'_page',$this->listPagingTotalPages);
				return TRUE;
			}
			return FALSE;
		}


		/**
		 *   Load data
		 *   @throws Exception
		 *   @return void
		 */

		public function dataLoad()
		{
			global $LAB;

			// Build parameters
			if (!empty($this->param['loadparam'])) $loadListParam=$this->param['loadparam'];
			else $loadListParam=new BASE_DATAOBJECT_PARAM();

			// Columns
			$loadListParam->addColumns($this->data->_param['idfield']);
			if ($this->param['nested']) $loadListParam->addColumns($this->param['nested_parentfield']);
			foreach ($this->listField as $fieldTag => &$fieldObject)
			{
				$fieldObject->getListQuery($loadListParam);
			}

			// Filters
			$this->gotFilters=FALSE;
			foreach ($this->listFilter as $filterTag => &$filterObject)
			{
				$setting=$LAB->SESSION->get('list_'.$this->param['id'].'_filter_'.$filterObject->tag);
				if ($setting!='') $this->gotFilters=TRUE;
				$filterObject->getCriteria($setting,$loadListParam);
			}

			// Sort
			if ($this->data->_param['setord'])
			{
				$loadListParam->addOrderBy($this->data->_param['table'].'.ord');
			}
			else
			{
				reset($this->listField);
				if ($this->listSortEnabled)
				{
					$this->listField[$this->listSortField]->getListSortCriteria($this->listSortOrder,$loadListParam);
				}
				elseif (!is_array($loadListParam->paramOrderBy) || !sizeof($loadListParam->paramOrderBy))
				{
					$this->listField[key($this->listField)]->getListSortCriteria($this->listField[key($this->listField)]->fieldDefaultSortOrder,$loadListParam);
				}
			}

			// Paging
			$loadListParam->calcFoundRows=TRUE;
			if ($this->listPagingEnabled)
			{
				$loadListParam->addLimit($this->listPagingPageSize,(($this->listPagingCurrentPage-1)*$this->listPagingPageSize));
			}

			// Load data
			if (!empty($this->gotFilters) || empty($this->param['filterrequired']))
			{
				$this->dataSet=$this->data->loadList($loadListParam);
				$this->listPagingFoundRows=$this->data->_foundRows;
			}

			// Get totals
			if ($this->listShowTotal)
			{
				$totalParam=$loadListParam;
				$totalParam->paramColumns=NULL;
				$totalParam->paramOrderBy=NULL;
				$totalParam->paramGroupBy=NULL;
				$totalParam->paramLimit=NULL;
				$totalParam->paramLimitOffset=NULL;
				foreach ($this->listField as $fieldTag => &$fieldObject)
				{
					if (!empty($fieldObject->param['list_showtotal']))
					{
						$fieldObject->getListTotalQuery($totalParam);
					}
				}
				$totalData=$this->data->loadList($totalParam);
				$this->dataTotal=$totalData[0];
			}
		}


		/**
		 *  Output HTML
		 *  @access public
		 *  @return string contents
		 */

		public function outputHTML()
		{
			// Init template
			$this->template=new BASE_TEMPLATE('list.'.$this->param['template']);
			$this->template->set('id',$this->param['id']);

			// Set title
			if (!empty($this->param['title']) && empty($this->param['notitle'])) $this->template->set('title',$this->param['title']);
			if (isset($this->param['blocktitle'])) $this->template->set('blocktitle',$this->param['blocktitle']);

			// Main parts
			$this->outputHTMLFilters();
			$this->outputHTMLList();
			$this->outputHTMLPaging();
			$this->outputHTMLGlobalActions();

			// Help
			if (!empty($this->param['info'])) $this->template->set('info',$this->param['info']);

			// Extra header/footer
			if (isset($this->param['extra_header'])) $this->template->set('extra_header',$this->param['extra_header']);
			if (isset($this->param['extra_footer'])) $this->template->set('extra_footer',$this->param['extra_footer']);

			// Return link
			if (isset($this->param['returnlink']))
			{
				$returnLink='<div class="returnlink"><a href="'.$this->param['returnlink'].'"><span class="icon-back"></span> '.oneof($this->param['returnlink_title'],'[BASE.LIST.Return]').'</a></div>';
				$this->template->set('returnlink',$returnLink);
			}

			// JavaScript
			if (!empty($this->js)) $this->template->set('js',$this->js);

			// Return list contents
			return $this->template->display();
		}

		/**
		 *  Output HTML
		 *  @access public
		 *  @return string contents
		 */

		public function outputHTMLFilters()
		{
			if (!sizeof($this->listFilter)) return;

			// Do we have second-level filters
			$extendedFilters='0';
			foreach ($this->listFilter as $filterTag => &$filterObject)
			{
				if ($filterObject->filterLevel==2) $extendedFilters='1';
			}
			$c='<div class="well bs-component list-filter">';
			$c.='<form role="form" method="post" action="'.$this->buildURL().'">';
			$c.='<div class="row">';
			$col=0;
			foreach ($this->listFilter as $filterTag => &$filterObject)
			{
				$col++;
				if ($col>$this->listFiltersPerRow)
				{
					$c.='</div><div class="row">';
					$col=1;
				}
				$c.=$filterObject->display();
			}
			$c.='</div>';
			$c.='<div class="buttonbar pull-right">';
			$c.='<button type="submit" class="btn btn-primary" id="filter_submit" name="filter_submit" value="filter_submit">[BASE.LIST.Filter.Submit]</button>';
			$c.='<button type="submit" class="btn btn-default" id="filter_clear" name="filter_clear" value="filter_clear">[BASE.LIST.Filter.Clear]</button>';
			$c.='</div>';
			$c.='</form>';
			$c.='</div>';
			$this->template->set('filter',$c);
		}


		/**
		 *  Output HTML: list
		 *  @access public
		 *  @return string contents
		 */

		public function outputHTMLList()
		{
			global $LAB;
			$c='';

			// No fields
			if (!sizeof($this->listField)) throw new Exception('No list fields defined.');

			// Filter required
			if (!empty($this->param['filterrequired']) && empty($this->gotFilters))
			{
				$c.='<div class="data"><div class="filter-required">'.oneof($this->param['nofiltermsg'],'[BASE.LIST.Filter.Required]').'</div></div>';
			}

			// Tabel
			else
			{
				// Table begins
				$c.='<div class="auto-scroll">';
				$c.='<table class="'.(empty($this->listClass)?'table table-bordered table-striped table-hovered':$this->listClass).'">';

				// Second OB begins
				$cc='';

				// Header
				$cc.='<thead>';
				$cc.='<tr>';
				if ($this->listShowRowNum) $cc.='<th class="rownum">'.htmlspecialchars($this->listRowNumTitle).'</th>';
				foreach ($this->listField as $fieldTag => &$fieldObject)
				{
					$cc.='<th';
						$style=array();
						if (!empty($fieldObject->param['list_fieldstyle'])) $style[]=$fieldObject->param['list_fieldstyle'];
						if (!empty($fieldObject->param['list_fieldwidth'])) $style[]='width: '.$fieldObject->param['list_fieldwidth'];
						if (!empty($fieldObject->param['list_fieldalign'])) $style[]='text-align: '.$fieldObject->param['list_fieldalign'];
						if (!empty($style)) $cc.=' style="'.join('; ',$style).'"';
						$class=array();
						if (!empty($fieldObject->param['list_nowrap'])) $class[]='nowrap';
						if (!empty($fieldObject->param['list_fieldclass'])) $class[]=$fieldObject->param['list_fieldclass'];
						if ($this->listSortEnabled && $fieldObject->param['list_sortable']) $class[]='sortable';
						if (!empty($class)) $cc.=' class="'.join(' ',$class).'"';
					$cc.='>';

					if ($this->listSortEnabled && $fieldObject->param['list_sortable'])
					{
						$cc.='<a onclick="BASE.doPostSubmit(\''.$LAB->REQUEST->requestURI.'\',{sort:\''.$fieldTag.'\',dir:\''.($this->listSortField==$fieldTag?($this->listSortOrder=='desc'?'asc':'desc'):$fieldObject->fieldDefaultSortOrder).'\'})">'.$fieldObject->getTitle().'';
						if ($this->listSortField==$fieldTag)
						{
							$cc.=' <span class="'.($this->listSortOrder=='desc'?'icon-dir-dn':'icon-dir-up').'"></span>';
						}
						$cc.='</a>';
					}
					else
					{
						$cc.=$fieldObject->getTitle();
					}
					$cc.='</th>';
				}
				if (sizeof($this->listAction))
				{
					$cc.='<th></th>';
				}
				$cc.='</tr>';
				$cc.='</thead>';
				$this->template->set('listheader',$cc);

				// Data
				$cc.=$this->outputHTMLListData(0,0);

				// Total
				if ($this->listShowTotal)
				{
					$ct='<tr>';
					if ($this->listShowRowNum) $ct.='<td class="rownum"></td>';
					foreach ($this->listField as $fieldTag => &$fieldObject)
					{
						$ct.='<td';
						$style=array();
							if (!empty($fieldObject->param['list_fieldstyle'])) $style[]=$fieldObject->param['list_fieldstyle'];
							if (!empty($fieldObject->param['list_fieldwidth'])) $style[]='width: '.$fieldObject->param['list_fieldwidth'];
							if (!empty($fieldObject->param['list_fieldalign'])) $style[]='text-align: '.$fieldObject->param['list_fieldalign'];
							if (!empty($style)) $ct.=' style="'.join('; ',$style).'"';
							$class=array();
							if (!empty($fieldObject->param['list_nowrap'])) $class[]='nowrap';
							if (!empty($fieldObject->param['list_fieldclass'])) $class[]=$fieldObject->param['list_fieldclass'];
							if (!empty($class)) $ct.=' class="'.join(' ',$class).'"';
						$ct.='>';
						if (!empty($fieldObject->param['list_showtotal']))
						{
							$ct.=$fieldObject->listTotalValue($this->dataTotal[$fieldTag],$this->dataTotal);
						}
						$ct.='</td>';
					}
					if (sizeof($this->listAction))
					{
						$ct.='<td></td>';
					}
					$ct.='</tr>';
					$this->template->set('listtotal',$ct);
					$cc.=$ct;
				}

				// List contents
				$this->template->set('listcontents',$cc);
				$c.=$cc;

				// Table ends
				$c.='</table>';
				$c.='</div>';
			}

			// Catch contents, set, return
			$this->template->set('list',$c);
			return TRUE;
		}


		/**
		 *  Output HTML: list data (recursive)
		 *  @access private
		 *  @return void
		 */

		function outputHTMLListData( $parent_id, $depth )
		{
			$c='';

			// No data?
			if ($parent_id==0 && $depth==0 && !sizeof($this->dataSet) && !empty($this->param['noresults']))
			{
				$c.='<tr>';
				$c.='<td class="data nohover noresults" colspan="'.(sizeof($this->listField)+(sizeof($this->listAction)?1:0)+($this->listShowRowNum?1:0)).'">'.$this->param['noresults'].'</td>';
				$c.='</tr>';
				return $c;
			}

			// Gather data
			if ($this->param['nested'])
			{
				$workdataset=array();
				reset($this->dataSet);
				foreach ($this->dataSet as &$row)
				{
					if ($row[$this->param['nested_parentfield']]!=$parent_id) continue;
					$workdataset[]=&$row;
				}
			}
			else
			{
				$workdataset=&$this->dataSet;
			}

			// Check
			if (!is_array($workdataset) || !sizeof($workdataset)) return;

			// Row num base
			if ($this->param['nested'])
			{
				$rowNum=0;
			}
			else
			{
				if ($this->listPagingEnabled)
				{
					$rowNum=round(($this->listPagingCurrentPage-1)*$this->listPagingPageSize);
				}
				else
				{
					$rowNum=0;
				}
			}

			// Display
			for ($i=0;$i<sizeof($workdataset);++$i)
			{
				// For convenience
				$row=&$workdataset[$i];

				// Count children if nested
				if ($this->param['nested'])
				{
					$childcnt=0;
					foreach ($this->dataSet as &$crow)
					{
						if ($crow[$this->param['nested_parentfield']]==$row[$this->data->_param['idfield']]) $childcnt++;
					}
				}

				// Row begins
				$c.=$this->outputHTMLListRowBeginningBlock($row);

				// Row num
				if ($this->listShowRowNum)
				{
					$rowNum++;
					$c.=$this->outputHTMLListRowNum($rowNum,$row);
				}

				// Columns
				$fcnt=0;
				foreach ($this->listField as $fieldTag => &$fieldObject)
				{
					$c.='<td';
						$style=array();
						if (!empty($fieldObject->param['list_fieldstyle'])) $style[]=$fieldObject->param['list_fieldstyle'];
						if (!empty($fieldObject->param['list_fieldwidth'])) $style[]='width: '.$fieldObject->param['list_fieldwidth'];
						if (!empty($fieldObject->param['list_fieldalign'])) $style[]='text-align: '.$fieldObject->param['list_fieldalign'];
						if (!$fcnt && $this->param['nested'] && $depth>0) $style[]='padding-left: '.($depth*30).'px';
						if (!empty($style)) $c.=' style="'.join('; ',$style).'"';
						$class=array();
						if (!empty($fieldObject->param['list_nowrap'])) $class[]='nowrap';
						if (!empty($fieldObject->param['list_fieldclass'])) $class[]=$fieldObject->param['list_fieldclass'];
						if (!empty($class)) $c.=' class="'.join(' ',$class).'"';
					$c.='>';

					$fld=oneof($fieldObject->fieldColumn,$fieldTag);
					if (strpos($fld,'->')!==FALSE)
					{
						$fldArr=preg_split('/->/',$fld);
						$fld=$fldArr[sizeof($fldArr)-2].'_'.$fldArr[sizeof($fldArr)-1];
					}
					elseif (strpos($fld,' ')!==FALSE)
					{
						$fldArr=preg_split('/ /',$fld);
						$fld=$fldArr[sizeof($fldArr)-1];
					}
					elseif (strpos($fld,'.')!==FALSE)
					{
						$fldArr=preg_split('/\./',$fld);
						$fld=$fldArr[sizeof($fldArr)-1];
					}
					//$fld=str_replace('.','_',$fld);
					$c.=nl2br($fieldObject->listValue($row[$fld],$row));

					$c.='</td>';
					$fcnt++;
				}

				if (sizeof($this->listAction))
				{
					$c.='<td class="rowactions"><ul class="rowactions">';
					$aCnt=0;
					foreach ($this->listAction as $actionTag => &$action)
					{
						// if ($aCnt) $c.='<div class="action splitter">|</div>';
						$c.='<li class="rowaction">';
						if ($actionTag=='swap')
						{
							if ($i>0)
							{
								$actionURL=(!strncmp($action->getURL($row),'/',1)?'':$this->param['url'].'/').$action->getURL($row);
								$actionParam=array();
								$actionParam[]="swap1: '".jsencode($row[$this->data->_param['idfield']])."'";
								$actionParam[]="swap2: '".jsencode($workdataset[$i-1][$this->data->_param['idfield']])."'";
								if (is_array($this->passthru) && sizeof($this->passthru)) foreach ($this->passthru as $ptVar => &$ptObject) if (!empty($_POST[$ptVar])) $actionParam[]=$ptVar.":'".jsencode($_POST[$ptVar])."'";
								$c.='<a onclick="BASE.doAjaxAction(\''.$actionURL.'\',{'.join(',',$actionParam).'}'.(mb_strlen($action->getConfirm($row))?",'".jsencode($action->getConfirm($row))."'":',null').',1)"><span class="icon-up"></span></a>';
							}
							else
							{
								$c.='<span class="icon-up disabled"></span>';
							}
							$c.='</li><li class="rowaction">';
							if ($i<(sizeof($workdataset)-1))
							{
								$actionURL=(!strncmp($action->getURL($row),'/',1)?'':$this->param['url'].'/').$action->getURL($row);
								$actionParam=array();
								$actionParam[]="swap1: '".jsencode($row[$this->data->_param['idfield']])."'";
								$actionParam[]="swap2: '".jsencode($workdataset[$i+1][$this->data->_param['idfield']])."'";
								if (is_array($this->passthru) && sizeof($this->passthru)) foreach ($this->passthru as $ptVar => &$ptObject) if (!empty($_POST[$ptVar])) $actionParam[]=$ptVar.":'".jsencode($_POST[$ptVar])."'";
								$c.='<a onclick="BASE.doAjaxAction(\''.$actionURL.'\',{'.join(',',$actionParam).'}'.(mb_strlen($action->getConfirm($row))?",'".jsencode($action->getConfirm($row))."'":',null').',1)"><span class="icon-dn"></span></a>';
							}
							else
							{
								$c.='<span class="icon-dn disabled"></span>';
							}
						}
						else
						{
							$actionEnabled=$action->isEnabled($row);
							if ($this->param['nested'] && $actionTag=='addsub' && $this->param['nested_maxdepth'] && ($depth+1)>=$this->param['nested_maxdepth']) $actionEnabled=FALSE;
							if ($this->param['nested'] && $actionTag=='delete' && $childcnt>0) $actionEnabled=FALSE;
							if ($actionEnabled)
							{
								if (mb_strlen($action->getJSAction($row)))
								{
									$parsedAction=$action->getJSAction($row);
									$parsedAction=str_replace('$id',$row[$this->data->_param['idfield']],$parsedAction);
									foreach (array_keys($row) as $fld) $parsedAction=str_replace('$'.$fld,$row[$fld],$parsedAction);
									$c.='<a onclick="'.$parsedAction.'">'.$action->getTitle($row).'</a>';
								}
								else
								{
									$actionURL=(!strncmp($action->getURL($row),'/',1)?'':$this->param['url'].'/').$action->getURL($row);
									$actionParam=array();
									$actionParam[]=(($this->param['nested'] && $actionTag=='addsub')?$this->param['nested_parentfield']:$this->data->_param['idfield']).":'".jsencode($row[$this->data->_param['idfield']])."'";
									if (is_array($this->passthru) && sizeof($this->passthru)) foreach ($this->passthru as $ptVar => &$ptObject) if (!empty($_POST[$ptVar])) $actionParam[]=$ptVar.":'".jsencode($_POST[$ptVar])."'";
									$c.='<a onclick="BASE.doAjaxAction(\''.$actionURL.'\',{'.join(',',$actionParam).'}'.(mb_strlen($action->getConfirm($row))?",'".jsencode($action->getConfirm($row))."'":'').')">'.$action->getTitle($row).'</a>';
								}
							}
							else
							{
								$c.='<span style="color: #999999">'.oneof($action->getDisabledTitle($row),$action->getTitle($row)).'</span>';
							}
						}
						$c.='</li>';
						$aCnt++;
					}
					$c.='</ul></td>';
				}

				// Row ends
				$c.=$this->outputHTMLListRowEndingBlock($row);

				// Children
				if ($this->param['nested'])
				{
					$d=$depth+1;
					$c.=$this->outputHTMLListData($row[$this->data->_param['idfield']], $d);
				}
			}

			// Return
			return $c;
		}


		/**
		 *   Output HTML: list row beginning
		 *   @access public
		 *   @param array $row Row
		 *   @return string
		 */

		function outputHTMLListRowBeginningBlock( &$row )
		{
			return '<tr>';
		}


		/**
		 *   Output HTML: list row ending
		 *   @access public
		 *   @param array $row Row
		 *   @return string
		 */

		function outputHTMLListRowEndingBlock( &$row )
		{
			return '</tr>';
		}


		/**
		 *   Output HTML: list row number
		 *   @access public
		 *   @param int $rowNum Row number
		 *   @param array $row Row
		 *   @return string
		 */

		function outputHTMLListRowNum( $rowNum, &$row )
		{
			$c='<td class="rownum">';
				$c.=intval($rowNum);
			$c.='</td>';
			return $c;
		}


		/**
		 *  Output HTML: pager
		 *  @access public
		 *  @return string contents
		 */

		public function outputHTMLPaging()
		{
			global $LAB;

			$c='';
			if ($this->listPagingEnabled && $this->listPagingTotalPages>1)
			{
				// In case of a gazillion pages, let's do a trick
				$pageMin=0;
				$pageMax=$this->listPagingTotalPages;
				if ($this->listPagingTotalPages>20)
				{
					if ($this->listPagingCurrentPage==$this->listPagingTotalPages)
					{
						$pageMin=$this->listPagingCurrentPage-2;
					}
					elseif ($this->listPagingCurrentPage>3)
					{
						$pageMin=$this->listPagingCurrentPage-1;
					}
					if ($this->listPagingCurrentPage==1)
					{
						$pageMax=3;
					}
					elseif ($this->listPagingCurrentPage<($this->listPagingTotalPages-3))
					{
						$pageMax=$this->listPagingCurrentPage+1;
					}
				}

				// Build paging
				$skip1=FALSE;
				$skip2=FALSE;
				$c.='<ul class="pagination">';
				for($i=1;$i<=$this->listPagingTotalPages;++$i)
				{
					// First
					if ($i<($this->listPagingTotalPages-2) && $i>$pageMax)
					{
						if (!$skip1)
						{
							$c.='<li><a style="cursor: default">...</a></li>';
							$skip1=TRUE;
						}
						continue;
					}

					// Last
					if ($i>3 && $i<$pageMin)
					{
						if (!$skip2)
						{
							$c.='<li><a style="cursor: default">...</a></li>';
							$skip2=TRUE;
						}
						continue;
					}

					if ($i==$this->listPagingCurrentPage)
					{
						$c.='<li class="active"><a>'.intval($i).'</a></li>';
					}
					else
					{
						$c.='<li><a onclick="BASE.doPostSubmit(\''.$LAB->REQUEST->requestURI.'\',{page:'.intval($i).'})">'.intval($i).'</a></li>';
					}
				}
				$c.='</ul>';
			}

			// Total records
			if (empty($this->param['noresultcount']))
			{
				$c.='<div class="total-records pull-right">'.sprintf($LAB->LOCALE->get('BASE.LIST.TotalRecords'),$this->listPagingFoundRows).'</div>';
			}

			// Set to template
			$this->template->set('paging',$c);
		}


		/**
		 *  Output HTML: global action
		 *  @access public
		 *  @return string contents
		 */

		public function outputHTMLGlobalActions()
		{
			global $LAB;

			$c='<ul class="globalactions">';
			foreach ($this->listGlobalAction as $actionKey => &$action)
			{
				if (!empty($action->actionJSAction)) // TODO:
				{
					$c.='<li class="action globalaction">';
					if (!empty($action->actionIcon)) $c.='<span class="icon-'.$action->actionIcon.'">';
					$c.='<a onclick="'.$action->actionJSAction.'">';
					$c.=$action->actionTitle;
					$c.='</a>';
					if (!empty($action->actionIcon)) $c.='</span>';
					$c.='</li>';
				}
				else
				{
					$c.='<li class="action globalaction">';
					if (!empty($action->actionIcon)) $c.='<span class="icon-'.$action->actionIcon.'">';
					$c.='<a href="'.$this->buildURL($action->actionURL).'">';
					$c.=$action->actionTitle;
					$c.='</a>';
					$c.='</span>';
					$c.='</li>';
				}
			}
			$c.='</ul>';

			$this->template->set('globalaction',$c);
		}


		/**
		 *   Display the list
		 *   @throws Exception
		 *   @return string
		 */

		public function display()
		{
			global $LAB;
			try
			{
				// Init
				$this->initList();
				$this->initFilters();
				$this->initColumns();
				$this->initActions();
				$this->setDefaults();

				// Apply filters, paging, etc.
				$res=$this->setCriteria();
				if ($res)
				{
					$redirectURL=preg_replace('/get=([A-Za-z0-9]+)/',"#$1",$LAB->REQUEST->requestURI);
					$LAB->RESPONSE->setRedirect($redirectURL);
					return;
				}

				// Load data
				$this->dataLoad();

				// Check paging
				$res=$this->checkPaging();
				if ($res)
				{
					$redirectURL=preg_replace('/get=([A-Za-z0-9]+)/',"#$1",$LAB->REQUEST->requestURI);
					$LAB->RESPONSE->setRedirect($redirectURL);
					return;
				}

				// Output
				return $this->outputHTML();
			}
			catch (Exception $e)
			{
				$c='';
				if (!empty($this->param['title'])) $c.='<h1>'.htmlspecialchars($this->param['title']).'</h1>';
				$c.='<div class="alert alert-dismissable alert-danger">'.htmlspecialchars($e->getMessage()).'</div>';
				return $c;
			}

		}


	}


?>