<?php

	/**
	 *
	 *   LabEngine™ 7
	 *   The request handler class
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */

	class BASE_REQUEST
	{


		/**
		 *   Request type
		 *   @var string
		 *   @access public
		 */

		public $requestType= 'site';


		/**
		 *   Request URI
		 *   @var string
		 *   @access public
		 */

		public $requestURI = '';


		/**
		 *   Request controller
		 *   @var string
		 *   @access public
		 */

		public $requestController = '';


		/**
		 *   Controller instance
		 *   @var object|boolean
		 *   @access public
		 */

		public $controllerObject = FALSE;


		/**
		 *   Request headers
		 *   @var string
		 *   @access public
		 */

		public $requestHeader = array();


		/**
		 *   Request variables
		 *   @var array
		 *   @access public
		 */

		public $requestVar = array();

		/**
		 *   Request language
		 *   @var string|boolean
		 *   @access public
		 */

		public $requestLang = FALSE;

		/**
		 *   Request language is set from URI?
		 *   @var boolean
		 *   @access public
		 */

		public $requestLangFromURI = FALSE;

		/**
		 *   Request method
		 *   @var string
		 *   @access public
		 */

		public $requestMethod = NULL;


		/**
		 *   Handle request
		 *   @access public
		 *   @throws Exception
		 *   @return void
		 */

		public function handleRequest()
		{
			global $LAB;

			// Init
			if (strlen($_GET['SCRIPT_URL'])) $_SERVER['SCRIPT_URL']=$_GET['SCRIPT_URL'];
			$uriArray=(strlen($_SERVER['SCRIPT_URL'])>1?str_array(substr($_SERVER['SCRIPT_URL'],1),'\/'):array('index'));
			$uriMode=0;
			$uriPos=0;
			$uriVarCount=0;

			// Request method
			$this->requestMethod=$_SERVER['REQUEST_METHOD'];

			// Headers
			$this->requestHeader=getallheaders();

			// Parse URI
			foreach($uriArray as $uriElement)
			{
				//  Bump element count
				if (!strlen($uriElement)) continue;
				$uriPos++;

				// Check for controller
				if ($uriPos==1 || ($uriPos==2 && $this->requestLangFromURI) || ($uriPos==2 && $this->requestType!='site') || ($uriPos==3 && $this->requestLangFromURI && $this->requestType!='site'))
				{
					// Check for invalid characters
					if (!preg_match("/^[A-Za-z0-9\.\-]+$/",$uriElement))
					{
						header("Status: 500 Internal Server Error");
						throw New Exception('HTTP 500: Invalid request.');
					}

					// Language
					if ($uriPos==1 && mb_strlen($uriElement)==2 && in_array($uriElement,$LAB->LOCALE->localeLanguageSet) && !$LAB->CONFIG->get('locale.nolangfromurl'))
					{
						$this->requestLang=$uriElement;
						$LAB->SESSION->set('LANG',$uriElement);
						$this->requestLangFromURI=true;
						$this->requestURI.='/'.$uriElement;
						if (sizeof($uriArray)==1)
						{
							// Set controller to index
							$this->requestController='index';
							$this->requestURI.='/index';
							$this->initController();

							// Check login
							if ($this->controllerObject->controllerLoginRequired && $this->requestController!='login' && !$LAB->USER->isLoggedIn())
							{
								$LAB->SESSION->set('login.redirect',$_SERVER['SCRIPT_URL']);
								$LAB->RESPONSE->setRedirect(oneof($this->controllerObject->controllerLoginURL,'/login'));
								return;
							}
						}
						continue;
					}

					// Controller type switch?
					if ($uriPos==1 && in_array($uriElement,array('base','cms')))
					{
						$this->requestType=$uriElement;
						continue;
					}

					// Check if controller exists
					if (!stream_resolve_include_path($this->requestType.'/controller/'.$uriElement.'.php'))
					{
						// See if there's a 404 handler
						if (stream_resolve_include_path($this->requestType.'/controller/errorhandler-404.php'))
						{
							$this->requestController='errorhandler-404';
							$this->requestURI.='/'.$uriElement;
							$this->initController();
							continue;
						}

						// If not, show 404
						else
						{
							header("Status: 404 Not Found");
							throw New Exception('HTTP 404: The specified resource /'.$uriElement.' was not found.');
						}
					}

					// Set controller
					$this->requestController=$uriElement;
					$this->requestURI.='/'.$uriElement;
					$this->initController();

					// Check login
					if ($this->controllerObject->controllerLoginRequired && $this->requestController!='login' && !$LAB->USER->isLoggedIn())
					{
						$LAB->SESSION->set('login.redirect',$_SERVER['SCRIPT_URL']);
						$LAB->RESPONSE->setRedirect(oneof($this->controllerObject->controllerLoginURL,'/login'));
						return;
					}

					// Continue
					continue;
				}

				//  Check for variable
				else
				{
					// Split
					$varName=$varValue='';
					if (strpos($uriElement,'=')!==FALSE)
					{
						list($varName,$varValue)=preg_split('/=/',$uriElement,2);
					}
					else
					{
						$varName=$uriElement;
						$varValue=TRUE;
					}

					// Check name
					if ($varValue!==TRUE && !preg_match("/^[A-Za-z0-9\.\_\-]+$/",$varName))
					{
						$LAB->RESPONSE->setRedirect($this->requestURI);
						return;
					}

					// Add to URI
					$this->requestURI.='/'.$uriElement;

					// Set variable
					$uriVarCount++;
					if (is_string($varValue) && preg_match("/^[0-9]+$/",$varValue)) $varValue=intval($varValue);
					if (is_string($varValue) && preg_match("/^[0-9\.]+$/",$varValue)) $varValue=floatval($varValue);
					$this->requestVar[$varName]=$varValue;
					$this->requestVar['var_'.$uriVarCount]=array('name'=>$varName,'value'=>$varValue);
				}
			}

			// Init environment
			if (stream_resolve_include_path('site/code/init.request.php'))
			{
				include_once 'site/code/init.request.php';
			}
			if (!empty($LAB->RESPONSE->responseRedirect))
			{
				return;
			}

			// Call script
			$this->runController();
		}


		/**
		 *   Init controller
		 *   @access public
		 *   @throws Exception
		 *   @return void
		 */

		public function initController( $pageFileName=FALSE )
		{
			global $LAB;

			// Page filename
			$pageFileName=oneof($pageFileName,$this->requestType.'/controller/'.$this->requestController.'.php');
			require($pageFileName);

			// Traverse variables - find the controller
			foreach (array_keys(get_defined_vars()) as $varName)
			{
				if ($$varName instanceof BASE_CONTROLLEROBJECT)
				{
					$this->controllerObject=&$$varName;
					return;
				}
			}

			// If we ended up here, this means that the controller object was not found
			throw new Exception('Could not find '.$this->requestController.' controller instance.');
		}


		/**
		 *   Run the controller
		 *   @access public
		 *   @throws Exception
		 *   @return void
		 */

		public function runController()
		{
			global $LAB;

			// Check
			if (!is_object($this->controllerObject))
			{
				throw new Exception('Could not find '.$this->requestController.' controller instance.');
			}

			// Load locale
			$localeTag=oneof($LAB->REQUEST->requestLang,$LAB->SESSION->get('LANG'),$LAB->LOCALE->localeLanguageDefault,'en');
			$LAB->REQUEST->requestLang=$localeTag;
			$LAB->LOCALE->loadLocale($localeTag);

			// Run controller
			$LAB->RESPONSE->responseContent=$this->controllerObject->runController();
		}


	}


	?>