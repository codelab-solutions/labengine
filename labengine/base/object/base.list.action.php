<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   The list action class
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_LIST_ACTION
	{


		/**
		 *  Titles
		 *  @var string
		 *  @access public
		 */
		public $actionTitle = NULL;
		public $actionTitleDisabled = NULL;

		/**
		 *  Icons
		 *  @var string
		 *  @access public
		 */
		public $actionIcon = NULL;
		public $actionIconDisabled = NULL;

		/**
		 *  URL
		 *  @var string
		 *  @access public
		 */
		public $actionURL = NULL;

		/**
		 *  JS action
		 *  @var string
		 *  @access public
		 */
		public $actionJSAction = NULL;

		/**
		 *	group
		 *	@var string
		 *	@access public
		 */
		public $actionGroup = NULL;


		/**
		 *  Confirm text
		 *  @var string
		 *  @access public
		 */
		public $actionConfirm = NULL;

		/**
		 *  Additional parameters
		 *  @var string
		 *  @access public
		 */
		public $actionParam = array();

		/**
		 *  Enabled critera
		 *  @var array
		 *  @access public
		 */
		public $actionEnabledIf = NULL;


		/**
		 *  The constructor
		 *  @access public
		 *  @param string action tag
		 *  @return void
		 */

		public function __construct ( $actionTag=NULL )
		{
			// Init defaults on magic action tags
			switch($actionTag)
			{
				case 'add':
					$this->setTitle('[BASE.LIST.Action.Add]');
					$this->setURL('edit');
					break;
				case 'addsub':
					$this->setTitle('[BASE.LIST.Action.AddSub]');
					$this->setURL('edit');
					break;
				case 'edit':
					$this->setTitle('[BASE.COMMON.Edit]');
					$this->setURL('edit');
					break;
				case 'delete':
					$this->setTitle('[BASE.COMMON.Delete]');
					$this->setConfirm('[BASE.LIST.Confirm]');
					break;
				case 'swap':
					$this->setURL('swap');
					break;
				default:
					break;
			}
		}


		/**
		 *  Set title
		 *  @access public
		 *  @param string title
		 *  @return void
		 */

		public function setTitle( $title )
		{
			$this->actionTitle=$title;
		}


		/**
		 *   Get title
		 *   @access public
		 *   @param array $row Row (for row action)
		 *   @return string title
		 */

		public function getTitle( &$row=NULL )
		{
			return $this->actionTitle;
		}


		/**
		 *  Set title
		 *  @access public
		 *  @param string title
		 *  @return void
		 */

		public function setDisabledTitle( $title )
		{
			$this->actionTitleDisabled=$title;
		}


		/**
		 *  Get title
		 *  @access public
		 *  @return string title
		 */

		public function getDisabledTitle()
		{
			return $this->actionTitleDisabled;
		}


		/**
		 *  Set URL
		 *  @access public
		 *  @param string URL
		 *  @return void
		 */

		public function setURL( $url )
		{
			$this->actionURL=$url;
		}


		/**
		 *   Get URL
		 *   @access public
		 *   @param array $row Row (for row action)
		 *   @return string URL
		 */

		public function getURL( &$row=NULL )
		{
			return $this->actionURL;
		}


		/**
		 *  Set icon
		 *  @access public
		 *  @param string icon
		 *  @return void
		 */

		public function setIcon( $icon )
		{
			$this->actionIcon=$icon;
		}


		/**
		 *   Get icon
		 *   @access public
		 *   @param array $row Row (for row action)
		 *   @return string icon
		 */

		public function getIcon( &$row=NULL )
		{
			return $this->actionIcon;
		}


		/**
		 *  Set icon
		 *  @access public
		 *  @param string icon
		 *  @return void
		 */

		public function setDisabledIcon( $icon )
		{
			$this->actionIconDisabled=$icon;
		}


		/**
		 *   Get icon
		 *   @access public
		 *   @param array $row Row (for row action)
		 *   @return string icon
		 */

		public function getDisabledIcon( &$row=NULL )
		{
			return $this->actionIconDisabled;
		}


		/**
		 *  Set confirmation message
		 *  @access public
		 *  @param string confirmation message
		 *  @return void
		 */

		public function setConfirm( $url )
		{
			$this->actionConfirm=$url;
		}


		/**
		 *   Get confirmation message
		 *   @access public
		 *   @param array $row Row (for row action)
		 *   @return string confirmation message
		 */

		public function getConfirm( &$row=NULL )
		{
			return $this->actionConfirm;
		}


		/**
		 *  Set JS action
		 *  @access public
		 *  @param string confirmation message
		 *  @return void
		 */

		public function setJSAction( $action )
		{
			$this->actionJSAction=$action;
		}


		/**
		 *   Get JS action
		 *   @access public
		 *   @param array $row Row (for row action)
		 *   @return string confirmation message
		 */

		public function getJSAction( &$row=NULL )
		{
			return $this->actionJSAction;
		}


		/**
		 *  Set confirmation message
		 *  @access public
		 *  @param string confirmation message
		 *  @return void
		 */

		public function setEnabledIf( $condition )
		{
			$this->actionEnabledIf=$condition;
		}


		/**
		 *  Get confirmation message
		 *  @access public
		 *  @return string confirmation message
		 */

		public function getEnabledIf()
		{
			return $this->actionEnabledIf;
		}


		/**
		 *  Add an additional parameter
		 *  @access public
		 *  @param string confirmation message
		 *  @return void
		 */

		public function addParam( $param, $value )
		{
			$this->actionParam[$param]=$value;
		}


		/**
		 *  Set parameters from array
		 *  @access public
		 *  @param string confirmation message
		 *  @return void
		 */

		public function setParam( $paramArray )
		{
			$this->actionParam=$paramArray;
		}


		/**
		 *  Get confirmation message
		 *  @access public
		 *  @return string confirmation message
		 */

		public function getParam()
		{
			return $this->actionParam;
		}


		/**
		 *  Is action enabled
		 *  @access public
		 *  @param array $row Row (For list action)
		 *  @return void
		 */

		public function isEnabled ( &$row=NULL )
		{
			if (!empty($this->actionEnabledIf))
			{
				eval('$enabled=('.$this->actionEnabledIf.')?TRUE:FALSE;');
				return $enabled;
			}
			return TRUE;
		}


	}


?>