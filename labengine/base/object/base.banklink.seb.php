<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   BankLink integration: SEB
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_BANKLINK_SEB extends BASE_BANKLINK_IPIZZA
	{


		/**
		 *   Bank ID
		 *   @var string
		 *   @static
		 */

		public static $BANK_id = 'seb';


		/**
		 *   Bank destination URL
		 *   @var string
		 */

		public $BANK_destination_url = 'https://www.seb.ee/cgi-bin/unet3.sh/un3min.r';


	}


?>