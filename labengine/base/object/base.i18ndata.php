<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   I18n data
	 *   Source: Linux /usr/share/i18n/locales
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_I18NDATA
	{


		/**
		 *   Get default locale for country
		 *   @access public
		 *   @static
		 *   @param string $cCode Country code
		 *   @return string
		 *   @throws Exception
		 */

		public static function getDefaultLocaleForCountry( $cCode )
		{
			// Get country data
			$countryData=BASE_COUNTRYDATA::getCountryData($cCode);
			if (empty($countryData)) throw new Exception('Could not find country data for '.$cCode);

			// Find default language
			$defaultLanguage=$countryData->languageCodes[0];
			if (empty($defaultLanguage)) throw new Exception('Could not find country data for '.$cCode);

			// Return
			return $defaultLanguage.'_'.$cCode;
		}


		/**
		 *   Parse locale file
		 *   @access public
		 *   @static
		 *   @param string $locale Locale code
		 *   @param string $data Specific data item to return
		 *   @return string
		 *   @throws Exception
		 */

		public static function parseLocale( $locale )
		{
			$localeFile=stream_resolve_include_path('base/data/i18n/'.$locale);
			if (empty($localeFile)) throw new Exception('Could not load locale data file for '.$locale);
			$fileContents=file($localeFile,FILE_IGNORE_NEW_LINES|FILE_SKIP_EMPTY_LINES);
			if (!is_array($fileContents)) throw new Exception('Could not parse locale data file for '.$locale);

			// Init
			$escapeChar=$commentChar=NULL;
			$section=NULL;
			$localeData=array();

			// Traverse
			foreach ($fileContents as $line)
			{
				// Convert line
				$line=html_entity_decode(preg_replace("/<U([0-9A-F]{4})>/", "&#x\\1;", $line), ENT_NOQUOTES, 'UTF-8');

				// Skip comments
				if (!empty($commentChar) && mb_substr($line,0,1)==$commentChar) continue;

				// Detect comment / escape char
				list($key,$value)=preg_split("/\s+/",$line,2);
				if ($key=='escape_char')
				{
					$escapeChar=$value;
					continue;
				}
				if ($key=='comment_char')
				{
					$commentChar=$value;
					continue;
				}

				// Section start/end
				if (preg_match("/^LC_/",$key) && empty($value))
				{
					$section=$key;
					continue;
				}
				if ($key=='END')
				{
					$section=NULL;
					continue;
				}

				// Parse value
				$value=preg_replace("/^\"(.*?)\"$/","$1",$value);

				// Assign
				if (empty($section)) throw new Exception('Error parsing locale file: key outside of section: '.$key);
				if (!empty($localeData[$section][$key]))
				{
					if (!is_array($localeData[$section][$key]))
					{
						$localeData[$section][$key]=array($localeData[$section][$key]);
						$localeData[$section][$key][]=$value;
					}
				}
				else
				{
					$localeData[$section][$key]=$value;
				}
			}

			// Return
			return $localeData;
		}


		/**
		 *   Get i18n data
		 *   @access public
		 *   @static
		 *   @param string $locale Locale code
		 *   @param string $section Section to return
		 *   @param string $data Specific data item in section to return
		 *   @return mixed
		 *   @throws Exception
		 */

		public static function getI18nData( $locale, $section=null, $data=null )
		{
			try
			{
				$localeData=BASE_I18NDATA::parseLocale($locale);
				if (!empty($section))
				{
					if (!empty($data))
					{
						return $localeData[$section][$data];
					}
					else
					{
						return $localeData[$section];
					}
				}
				else
				{
					return $localeData;
				}
			}
			catch (Exception $e)
			{
				return FALSE;
			}
		}


	}


?>