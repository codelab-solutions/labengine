<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   The configuration class
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_CONFIG
	{


		/**
		 *   Configuration file name
		 *   @var string
		 *   @access public
		 */

		public $configFilename = 'config/config.ini';


		/**
		 *   Configuration data
		 *   @var array
		 *   @access public
		 */

		public $configData = array();


		/**
		 *   Load configuration
		 *   @access public
		 *   @return void
		 */

		public function loadConfig()
		{
			global $LAB;

			// Config file name overrides
			if (isset($_SERVER['LAB_CONFIG']))
			{
				$this->configFilename = ($_SERVER['LAB_CONFIG'][0]=='/'?'':'config/').$_SERVER['LAB_CONFIG'];
			}
			elseif (isset($_ENV['LAB_CONFIG']))
			{
				$this->configFilename = ($_ENV['LAB_CONFIG'][0]=='/'?'':'config/').$_ENV['LAB_CONFIG'];
			}

			// Load config file
			$this->configData=parse_ini_file($this->configFilename,TRUE);
			if ($this->configData===FALSE)
			{
				throw new Exception('Could not load configuration file '.$this->configFilename);
				return;
			}

			// Check
			if (!isset($this->configData['site']['url']) || empty($this->configData['site']['url']))
			{
				if (isset($_SERVER['LAB_BASEURL']))
				{
					$this->configData['site']['url']=$_SERVER['LAB_BASEURL'];
				}
				elseif (isset($_ENV['LAB_BASEURL']))
				{
					$this->configData['site']['url']=$_ENV['LAB_BASEURL'];
				}
				else
				{
					$this->configData['site']['url']=(!empty($_SERVER['HTTPS'])?'https':'http').'://'.$_SERVER['HTTP_HOST'];
				}
			}
			if (!isset($this->configData['site']['path']) || empty($this->configData['site']['path']))
			{
				$this->configData['site']['path']=getcwd();
			}

			// Set defined
			define('SITE_URL',$this->configData['site']['url']);
			define('SITE_PATH',$this->configData['site']['path']);

			// Locales
			if (!isset($this->configData['locale']['lang']) || empty($this->configData['locale']['lang']))
			{
				throw new Exception('No languages defined.');
			}
			$langSet=str_array($this->configData['locale']['lang']);
			foreach ($langSet as $lang)
			{
				$LAB->LOCALE->addLanguage($lang);
			}
		}


		/**
		 *  Set a configuration directive
		 *  @access public
		 *  @param string Configuration directive
		 *  @param mixed value
		 *  @return void
		 */

		public function set( $configDirective, $value )
		{
			traverse_set(mb_strtolower($configDirective),$this->configData,$value);
		}


		/**
		 *  Get a configuration directive value
		 *  @access public
		 *  @param string Configuration directive
		 *  @return mixed Value
		 */

		public function get( $configDirective )
		{
			return traverse_get(mb_strtolower($configDirective),$this->configData);
		}


	}


	?>