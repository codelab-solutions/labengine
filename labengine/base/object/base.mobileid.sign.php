<?php


/**
 *
 *   FlaskPHP-Identity-MobileID
 *   --------------------------
 *   Digital signing provider for Mobile ID
 *
 *   @author   Codelab Solutions OÜ <codelab@codelab.ee>
 *   @license  https://www.flaskphp.com/LICENSE MIT
 *
 */


class BASE_MOBILEID_SIGN
{


    /**
     *   Dev mode?
     *   @var bool
     *   @access public
     */

    public $devMode = false;


    /**
     *   Service name
     *   @var string
     *   @access public
     */

    public $serviceName = null;


    /**
     *   Service message
     *   @var string
     *   @access public
     */

    public $serviceMessage = '';


    /**
     *   Service language
     *   @var string
     *   @access public
     */

    public $serviceLanguage = 'EST';


    /**
     *   Service language mapping
     *   @var array
     *   @static
     *   @access public
     */

    public static $serviceLanguageMap = [
        'et' => 'EST',
        'en' => 'ENG',
        'ru' => 'RUS',
        'lv' => 'LAT',
        'lt' => 'LIT',
        'fi' => 'FIN'
    ];



    /**
     *
     *   Constructor
     *   -----------
     *   @access public
     *   @param bool $devMode
     *   @throws SignException
     *   @return Sign
     *
     */

    public function __construct( bool $devMode=null )
    {
        global $LAB;

        // Add locale
        $LAB->LOCALE->loadLocale(__DIR__.'/../locale');

        // Init
        $this->initMobileID($devMode);
    }


    /**
     *
     *   Init the provider
     *   -----------------
     *   @access public
     *   @param bool $devMode Force dev environment
     *   @throws \Exception
     *   @return void
     *
     */

    public function initMobileID( bool $devMode=null )
    {
        global $LAB;

        // Dev mode
        if ($devMode!==null)
        {
            $this->devMode=$devMode;
        }
        else
        {
            $this->devMode=$LAB->DEBUG->devEnvironment;
        }

        // Service name
        if ($LAB->CONFIG->get('mobileid.servicename'))
        {
            $this->serviceName=$LAB->CONFIG->get('mobileid.servicename');
        }
        elseif (!$this->devMode)
        {
            throw new Exception('Missing identity.mobileid.service configuration directive');
        }

        // Language
        if ($LAB->CONFIG->get('identity.mobileid.language'))
        {
            $this->serviceLanguage=$LAB->CONFIG->get('identity.mobileid.language');
        }
        else
        {
            $this->serviceLanguage=oneof(static::$serviceLanguageMap[$LAB->LOCALE->localeLanguage],'EST');
        }

        // Default service message
        if ($LAB->CONFIG->get('identity.mobileid.servicemessage'))
        {
            $this->serviceMessage=$LAB->CONFIG->get('identity.mobileid.servicemessage');
        }
    }


    /**
     *
     *   Init SOAP client
     *   ----------------
     *   @access private
     *   @return \SoapClient
     *
     */

    private function initSoapClient()
    {
        // SOAP options
        $streamOptions=[
            'http' => array(
                'user_agent' => 'PHPSoapClient'
            )
        ];

      /*  if ($LAB->CONFIG->get('identity.mobileid.bindto'))
        {

            $bindTo=$LAB->CONFIG->get('identity.mobileid.bindto');
            if (mb_strpos($bindTo,':')===false) $bindTo.=':0';
            $streamOptions['socket']['bindto']=$bindTo;
        }*/
        $streamContext=stream_context_create($streamOptions);
        $soapOptions = array(
            'cache_wsdl' => WSDL_CACHE_MEMORY,
            'stream_context' => $streamContext,
            'trace' => true,
            'encoding' => 'utf-8'
        );


        // Init SOAP client
        if ($this->devMode)
        {
            $WSDL='https://tsp.demo.sk.ee/dds.wsdl';
            $this->serviceName='Testimine';
        }
        else
        {
            $WSDL='https://digidocservice.sk.ee/?wsdl';
        }

        $soapClient=new \SoapClient($WSDL, $soapOptions);
        return $soapClient;
    }


    /**
     *
     *   Start signing session
     *   ---------------------
     *   @access public
     *   @param string $digiDocFile Digidoc file (hashcoded)
     *   @throws SignException
     *   @return SignResponse
     *
     */

    public function startSession( string $digiDocFile )
    {
        global $LAB;

        try
        {
            // Check && base64-encode file
            if (!mb_strlen($digiDocFile)) throw new Exception('No file.');
            $digiDocFile=base64_encode($digiDocFile);

            // Init SOAP client
            $soapClient=$this->initSoapClient();
            // Make request
            $soapResponse=$soapClient->StartSession(
                '',
                $digiDocFile,
                true
            );
            // Debug
            if ($LAB->DEBUG->debugOn)
            {
                file_put_contents($LAB->CONFIG->get('site.tmppath').'/labengine-mobileid-sign.debug',date('Y-m-d H:i:s')." StartSession: \n\n",FILE_APPEND);
                file_put_contents($LAB->CONFIG->get('site.tmppath').'/labengine-mobileid-sign.debug',$soapClient->__getLastRequest()."\n\n",FILE_APPEND);
                file_put_contents($LAB->CONFIG->get('site.tmppath').'/labengine-mobileid-sign.debug',$soapClient->__getLastResponse()."\n\n",FILE_APPEND);
            }

            // Success
            if ($soapResponse['Status']=='OK' && intval($soapResponse['Sesscode']))
            {
                // Save Mobiil-ID data to session
                $LAB->SESSION->set('identity.mobileid.sign.sesscode',intval($soapResponse['Sesscode']));


                // Return response
                $response=new BASE_MOBILEID_SIGNRESPONSE();
                $response->status='OK';
                $response->sessCode=intval($soapResponse['Sesscode']);
                $response->signedDocInfo=$soapResponse['SignedDocInfo'];
                return $response;
            }


            // Fail
            else
            {
                if ($LAB->DEBUG->debugOn)
                {
                    file_put_contents($LAB->CONFIG->get('site.tmppath').'/labengine-mobileid-sign.debug.bdoc',base64_decode($digiDocFile));
                }
                throw new Exception('Error talking to the Mobile ID service'.($LAB->DEBUG->devEnvironment?': '.var_dump_str($soapResponse):''));
            }
        }
        catch (SoapFault $soapFault)
        {
            if ($LAB->DEBUG->debugOn)
            {
                file_put_contents($LAB->CONFIG->get('site.tmppath').'/labengine-mobileid-sign.debug',date('Y-m-d H:i:s')." StartSession SoapFault: \n\n",FILE_APPEND);
                file_put_contents($LAB->CONFIG->get('site.tmppath').'/labengine-mobileid-sign.debug',var_dump_str($soapFault)."\n\n",FILE_APPEND);
                file_put_contents($LAB->CONFIG->get('site.tmppath').'/labengine-mobileid-sign.debug',$soapClient->__getLastRequest()."\n\n",FILE_APPEND);
                file_put_contents($LAB->CONFIG->get('site.tmppath').'/labengine-mobileid-sign.debug',$soapClient->__getLastResponse()."\n\n",FILE_APPEND);
                file_put_contents($LAB->CONFIG->get('site.tmppath').'/labengine-mobileid-sign.bdoc',base64_decode($digiDocFile));
            }
            if (!empty($soapFault->detail->message))
            {
                throw new Exception('[BASE.COMMON.Error]: '.strval($soapFault->detail->message));
            }
            else
            {
                throw new Exception('Error talking to the Mobile ID service'.($LAB->DEBUG->devEnvironment?': '.var_dump_str($soapFault):''));
            }
        }
    }


    /**
     *
     *   Start Mobile ID sign process
     *   ----------------------------
     *   @access public
     *   @param string $phoneNo Phone number (372xxxxxxx)
     *   @param string $signerIdCode Signer's ID code
     *   @param string $signerCountry Signer's country
     *   @param array $signerInfo Additional signer info
     *   @param string $serviceMessage Service message
     *   @throws SignException
     *   @return SignResponse
     *
     */

    public function startSigning( string $phoneNo='', string $signerIdCode='', string $signerCountry='', array $signerInfo=null, string $serviceMessage=null )
    {
        global $LAB;

        try
        {
            // Check session
            //throw new Exception(var_dump_str($LAB->SESSION->get('identity.mobileid.sign.sesscode')));
            $sessCode=$LAB->SESSION->get('identity.mobileid.sign.sesscode');
            if (!intval($sessCode)) throw new Exception('SK session code not found. Session not initialized?');

            // Check that either phone number or ID code exists
            if (!mb_strlen($phoneNo) && !mb_strlen($signerIdCode)) throw new Exception('Either phoneNo or signedIdCode is required.');
            if (mb_strlen($phoneNo) && !preg_match("/^\+\d+$/",$phoneNo)) throw new Exception('Invalid phoneNo format, must be +xxxxxxxx.');

            // Remove leading country code
            $phoneNo=preg_replace('/^\+/','',$phoneNo);

            // Set service message
            if ($serviceMessage!==null)
            {
                $this->serviceMessage=$serviceMessage;
            }

            // Additional signer info
            $signerInfoRole=(($signerInfo!==null && array_key_exists('role',$signerInfo))?$signerInfo['role']:'');
            $signerInfoCity=(($signerInfo!==null && array_key_exists('city',$signerInfo))?$signerInfo['city']:'');
            $signerInfoState=(($signerInfo!==null && array_key_exists('state',$signerInfo))?$signerInfo['state']:'');
            $signerInfoPostalCode=(($signerInfo!==null && array_key_exists('postalcode',$signerInfo))?$signerInfo['postalcode']:'');
            $signerInfoCountry=(($signerInfo!==null && array_key_exists('country',$signerInfo))?$signerInfo['country']:'');

            // Init SOAP client
            $soapClient=$this->initSoapClient();

            // Make request
            $soapResponse=$soapClient->MobileSign(
                $sessCode,
                $signerIdCode,
                $signerCountry,
                $phoneNo,
                $this->serviceName,
                $this->serviceMessage,
                $this->serviceLanguage,
                $signerInfoRole,
                $signerInfoCity,
                $signerInfoState,
                $signerInfoPostalCode,
                $signerInfoCountry,
                'LT_TM',
                'asynchClientServer',
                null,
                true,
                true
            );

            // Debug
            if ($LAB->DEBUG->debugOn)
            {
                file_put_contents($LAB->CONFIG->get('site.tmppath').'/labengine-mobileid-sign.debug',date('Y-m-d H:i:s')." MobileSign: \n\n",FILE_APPEND);
                file_put_contents($LAB->CONFIG->get('site.tmppath').'/labengine-mobileid-sign.debug',$soapClient->__getLastRequest()."\n\n",FILE_APPEND);
                file_put_contents($LAB->CONFIG->get('site.tmppath').'/labengine-mobileid-sign.debug',$soapClient->__getLastResponse()."\n\n",FILE_APPEND);
            }

            // Success
            if ($soapResponse['Status']=='OK')
            {
                // Return response
                $response=new BASE_MOBILEID_SIGNRESPONSE();
                $response->status='pending';
                $response->challengeResponse=$soapResponse['ChallengeID'];
                return $response;
            }

            // Fail
            else
            {
                if (!empty($soapResponse['Status']))
                {
                    throw new Exception('[BASE.COMMON.Error]: '.$soapResponse['Status']);
                }
                else
                {
                    throw new Exception('Error talking to the Mobile ID service');
                }
            }
        }
        catch (SoapFault $soapFault)
        {
            if ($LAB->DEBUG->debugOn)
            {
                file_put_contents($LAB->CONFIG->get('site.tmppath').'/labengine-mobileid-sign.debug',date('Y-m-d H:i:s')." MobileSign SoapFault: \n\n",FILE_APPEND);
                file_put_contents($LAB->CONFIG->get('site.tmppath').'/labengine-mobileid-sign.debug',var_dump_str($soapFault)."\n\n",FILE_APPEND);
                file_put_contents($LAB->CONFIG->get('site.tmppath').'/labengine-mobileid-sign.debug',$soapClient->__getLastRequest()."\n\n",FILE_APPEND);
                file_put_contents($LAB->CONFIG->get('site.tmppath').'/labengine-mobileid-sign.debug',$soapClient->__getLastResponse()."\n\n",FILE_APPEND);
            }
            if (!empty($soapFault->detail->message))
            {
                throw new Exception('[BASE.COMMON.Error]: '.strval($soapFault->detail->message));
            }
            else
            {
                throw new Exception('Error talking to the Mobile ID service'.($LAB->DEBUG->devEnvironment?': '.var_dump_str($soapFault):''));
            }
        }
    }


    /**
     *
     *   Check Mobile ID auth status
     *   ---------------------------
     *   @access public
     *   @throws SignException
     *   @return SignResponse
     *
     */

    public function checkSignStatus()
    {
        global $LAB;
        try
        {
            //throw new Exception(var_dump_str($LAB->SESSION->get('identity.mobileid.sign.sesscode')));

            // Check session
            $sessCode=$LAB->SESSION->get('identity.mobileid.sign.sesscode');
            if (!intval($sessCode)) throw new Exception('SK session code not found. Session not initialized?');

            // Init SOAP client
            $soapClient=$this->initSoapClient();

            // Make request
            $soapResponse=$soapClient->GetStatusInfo(
                $sessCode,
                true,
                false
            );

            // Debug
            if ($LAB->DEBUG->debugOn)
            {
                file_put_contents($LAB->CONFIG->get('site.tmppath').'/labengine-mobileid-sign.debug',date('Y-m-d H:i:s')." GetStatusInfo: \n\n",FILE_APPEND);
                file_put_contents($LAB->CONFIG->get('site.tmppath').'/labengine-mobileid-sign.debug',$soapClient->__getLastRequest()."\n\n",FILE_APPEND);
                file_put_contents($LAB->CONFIG->get('site.tmppath').'/labengine-mobileid-sign.debug',$soapClient->__getLastResponse()."\n\n",FILE_APPEND);
            }

            // Success
            $success=false;
            if (!empty($soapResponse['Status']))
            {
                switch (strval($soapResponse['Status']))
                {
                    // OK
                    case 'OK':
                        switch ($soapResponse['StatusCode'])
                        {
                            // Pending
                            case 'OUTSTANDING_TRANSACTION':
                                $response=new BASE_MOBILEID_SIGNRESPONSE();
                                $response->status='pending';
                                break;

                            // OK, signed
                            case 'SIGNATURE':
                                $response=new BASE_MOBILEID_SIGNRESPONSE();
                                $response->status='success';
                                $response->signedDocInfo=$soapResponse['SignedDocInfo'];
                                break;

                            // Default
                            default:
                                $response=new BASE_MOBILEID_SIGNRESPONSE();
                                $response->status='error';
                                switch ($soapResponse['StatusCode'])
                                {
                                    case 'REVOKED_CERTIFICATE':
                                        $response->error=('[BASE.IDENTITY.MobileID.Error.CertificateRevoked]');
                                        break;
                                    case 'MID_NOT_READY':
                                        $response->error=('[BASE.IDENTITY.MobileID.Error.NotReady]');
                                        break;
                                    case 'SENDING_ERROR':
                                        $response->error=('[BASE.IDENTITY.MobileID.Error.RequestFailed]');
                                        break;
                                    case 'USER_CANCEL':
                                        $response->error=('[BASE.IDENTITY.MobileID.Error.UserCancelledSign]');
                                        break;
                                    case 'INTERNAL_ERROR':
                                        $response->error=('[BASE.IDENTITY.MobileID.Error.TechnicalError]');
                                        break;
                                    case 'SIM_ERROR':
                                        $response->error=('[BASE.IDENTITY.MobileID.Error.SIMError]');
                                        break;
                                    case 'PHONE_ABSENT':
                                        $response->error=('[BASE.IDENTITY.MobileID.Error.PhoneAbsent]');
                                        break;
                                    default:
                                        $response->error=('[BASE.IDENTITY.MobileID.Error.UnknownError]');
                                        break;
                                }
                                break;
                        }
                        break;

                    // Error
                    default:
                        $response=new BASE_MOBILEID_SIGNRESPONSE();
                        $response->status='error';
                        $response->error='[BASE.IDENTITY.MobileID.Error.UnknownError]';
                        break;
                }
                return $response;
            }
            else
            {
                throw new Exception('Error talking to the Mobile ID service');
            }
        }
        catch (\SoapFault $soapFault)
        {
            if ($LAB->DEBUG->debugOn)
            {
                file_put_contents($LAB->CONFIG->get('site.tmppath').'/labengine-mobileid-sign.debug',date('Y-m-d H:i:s')." GetStatusInfo SoapFault: \n\n",FILE_APPEND);
                file_put_contents($LAB->CONFIG->get('site.tmppath').'/labengine-mobileid-sign.debug',var_dump_str($soapFault)."\n\n",FILE_APPEND);
                file_put_contents($LAB->CONFIG->get('site.tmppath').'/labengine-mobileid-sign.debug',$soapClient->__getLastRequest()."\n\n",FILE_APPEND);
                file_put_contents($LAB->CONFIG->get('site.tmppath').'/labengine-mobileid-sign.debug',$soapClient->__getLastResponse()."\n\n",FILE_APPEND);
            }
            if (!empty($soapFault->detail->message))
            {
                throw new Exception('[BASE.COMMON.Error]: '.strval($soapFault->detail->message));
            }
            else
            {
                throw new Exception('Error talking to the Mobile ID service'.($LAB->DEBUG->devEnvironment?': '.var_dump_str($soapFault):''));
            }
        }
    }


    /**
     *
     *   Get signed document
     *   -------------------
     *   @access public
     *   @throws SignException
     *   @return string
     *
     */

    public function getSignedDoc()
    {
        global $LAB;

        try
        {
            // Check session
            $sessCode=$LAB->SESSION->get('identity.mobileid.sign.sesscode');
            if (!intval($sessCode)) throw new Exception('SK session code not found. Session not initialized or already closed?');

            // Init SOAP client
            $soapClient=$this->initSoapClient();

            // Make request
            $soapResponse=$soapClient->GetSignedDoc(
                $sessCode
            );

            // Debug
            if ($LAB->DEBUG->debugOn)
            {
                file_put_contents($LAB->CONFIG->get('site.tmppath').'/labengine-mobileid-sign.debug',date('Y-m-d H:i:s')." GetSignedDoc: \n\n",FILE_APPEND);
                file_put_contents($LAB->CONFIG->get('site.tmppath').'/labengine-mobileid-sign.debug',$soapClient->__getLastRequest()."\n\n",FILE_APPEND);
                file_put_contents($LAB->CONFIG->get('site.tmppath').'/labengine-mobileid-sign.debug',$soapClient->__getLastResponse()."\n\n",FILE_APPEND);
            }

            // Success
            if ($soapResponse['Status']=='OK')
            {
                return base64_decode($soapResponse['SignedDocData']);
            }

            // Fail
            else
            {
                throw new Exception('Error talking to the Mobile ID service'.($LAB->DEBUG->devEnvironment?': '.var_dump_str($soapResponse):''));
            }
        }
        catch (\SoapFault $soapFault)
        {
            if ($LAB->DEBUG->debugOn)
            {
                file_put_contents($LAB->CONFIG->get('site.tmppath').'/labengine-mobileid-sign.debug',date('Y-m-d H:i:s')." GetSignedDoc SoapFault: \n\n",FILE_APPEND);
                file_put_contents($LAB->CONFIG->get('site.tmppath').'/labengine-mobileid-sign.debug',var_dump_str($soapFault)."\n\n",FILE_APPEND);
                file_put_contents($LAB->CONFIG->get('site.tmppath').'/labengine-mobileid-sign.debug',$soapClient->__getLastRequest()."\n\n",FILE_APPEND);
                file_put_contents($LAB->CONFIG->get('site.tmppath').'/labengine-mobileid-sign.debug',$soapClient->__getLastResponse()."\n\n",FILE_APPEND);
            }
            if (!empty($soapFault->detail->message))
            {
                throw new Exception('[BASE.COMMON.Error]: '.strval($soapFault->detail->message));
            }
            else
            {
                throw new Exception('Error talking to the Mobile ID service'.($LAB->DEBUG->devEnvironment?': '.var_dump_str($soapFault):''));
            }
        }
    }


    /**
     *
     *   Close signing session
     *   @access public
     *   @throws SignException
     *   @return SignResponse
     *
     */

    public function closeSession()
    {
        global $LAB;

        try
        {
            // Check session
            $sessCode=$LAB->SESSION->get('identity.mobileid.sign.sesscode');
            if (!intval($sessCode)) throw new Exception('SK session code not found. Session not initialized or already closed?');

            // Init SOAP client
            $soapClient=$this->initSoapClient();

            // Make request
            $soapResponse=$soapClient->CloseSession(
                $sessCode
            );

            // Debug
            if ($LAB->DEBUG->debugOn)
            {
                file_put_contents($LAB->CONFIG->get('site.tmppath').'/labengine-mobileid-sign.debug',date('Y-m-d H:i:s')." CloseSession: \n\n",FILE_APPEND);
                file_put_contents($LAB->CONFIG->get('site.tmppath').'/labengine-mobileid-sign.debug',$soapClient->__getLastRequest()."\n\n",FILE_APPEND);
                file_put_contents($LAB->CONFIG->get('site.tmppath').'/labengine-mobileid-sign.debug',$soapClient->__getLastResponse()."\n\n",FILE_APPEND);
            }

            // Success
            if ($soapResponse=='OK')
            {
                // Save Mobiil-ID data to session
                $LAB->SESSION->set('identity.mobileid.sign.sesscode',null);

                // Return response
                $response=new BASE_MOBILEID_SIGNRESPONSE();
                $response->status='OK';
                return $response;
            }

            // Fail
            else
            {
                throw new Exception('Error talking to the Mobile ID service'.($LAB->DEBUG->devEnvironment?': '.var_dump_str($soapResponse):''));
            }
        }
        catch (\SoapFault $soapFault)
        {
            if ($LAB->DEBUG->debugOn)
            {
                file_put_contents($LAB->CONFIG->get('site.tmppath').'/labengine-mobileid-sign.debug',date('Y-m-d H:i:s')." CloseSession SoapFault: \n\n",FILE_APPEND);
                file_put_contents($LAB->CONFIG->get('site.tmppath').'/labengine-mobileid-sign.debug',var_dump_str($soapFault)."\n\n",FILE_APPEND);
                file_put_contents($LAB->CONFIG->get('site.tmppath').'/labengine-mobileid-sign.debug',$soapClient->__getLastRequest()."\n\n",FILE_APPEND);
                file_put_contents($LAB->CONFIG->get('site.tmppath').'/labengine-mobileid-sign.debug',$soapClient->__getLastResponse()."\n\n",FILE_APPEND);
            }
            if (!empty($soapFault->detail->message))
            {
                throw new Exception('[[ BASE.COMMON.Error ]]: '.strval($soapFault->detail->message));
            }
            else
            {
                throw new Exception('Error talking to the Mobile ID service'.($LAB->DEBUG->devEnvironment?': '.var_dump_str($soapFault):''));
            }
        }
    }


}


?>