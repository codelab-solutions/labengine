<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   The form class
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_FORM extends BASE_DISPLAYOBJECT
	{


		/**
		 *  Operation
		 *  @var string
		 *  @access public
		 */
		public $operation = NULL;


		/**
		 *  Errors
		 *  @var array
		 *  @access public
		 */
		public $errors = array();


		/**
		 *  Fields
		 *  @var array
		 *  @access public
		 */
		public $field = array();


		/**
		 *  Field groups
		 *  @var array
		 *  @access public
		 */
		public $fieldGroup = array();


		/**
		 *  Field label width
		 *  @var array
		 *  @access public
		 */
		public $fieldLabelWidth = '3';


		/**
		 *  Field content width
		 *  @var array
		 *  @access public
		 */
		public $fieldContentWidth = '9';


		/**
		 *  Custom submit actions
		 *  @var array
		 *  @access public
		 */
		public $submit = array();


		/**
		 *  Are we submitting?
		 *  @var boolean
		 *  @access public
		 */
		public $doSubmit = FALSE;
		public $XHR = FALSE;


		/**
		 *  Do we have file upload?
		 *  @var boolean
		 *  @access public
		 */
		public $fileUpload = FALSE;


		/**
		 *  Notes
		 *  @var array
		 *  @access public
		 */
		public $notes = array();


		/**
		 *  JavaScript
		 *  @var string
		 *  @access public
		 */
		public $js = '';


		/**
		 *  Init form
		 *  @access public
		 *  @return void
		 */

		public function initForm()
		{
			// Set defaults
			$this->setDefaults();

			// Init template
			$this->template=new BASE_TEMPLATE('form.'.$this->param['template']);

			// Init
			$this->operation=mb_strlen($this->param['operation'])?$this->param['operation']:(!empty($_POST[$this->data->_param['idfield']])?'edit':'add');
			$this->template->set('operation',$this->operation);
		}


		/**
		 *  Init fields
		 *  @access public
		 *  @return void
		 */

		public function initFields()
		{
			// Standard functionality: get all fields from data object
			if (!is_object($this->data)) throw new Exception('No data source defined.');
			if (!($this->data instanceof BASE_DATAOBJECT)) throw new Exception('Data source not a BASE_DATAOBJECT instance.');
			foreach ($this->data->_field as $fieldTag => &$fieldObject)
			{
				if ($fieldObject->param['noedit']) continue;
				$this->addField($fieldTag,$fieldObject);
			}
		}


		/**
		 *  Load data
		 *  @access public
		 *  @return void
		 */

		public function dataLoad()
		{
			// Set defaults
			$this->setDefaults();

			// Look for loading parameters
			$loadParam=(isset($this->param['loadparam'])?$this->param['loadparam']:NULL);

			// Load main data row
			if ($this->param['getobject'])
			{
				$daoClass=get_class($this->data);
				$this->data=$daoClass::getObject($_POST[$this->data->_param['idfield']],$loadParam);
			}
			else
			{
				$this->data->load($_POST[$this->data->_param['idfield']],$loadParam);
			}

			// Fire field load triggers
			foreach ($this->field as $fieldTag => &$fieldObject)
			{
				$fieldObject->triggerFormLoad();
			}
		}


		/**
		 *   Save data
		 *   @access public
		 *   @return void
		 *   @throws Exception
		 */

		public function dataSave()
		{
			try
			{
				// Save parameters
				$param=(isset($this->param['saveparam'])?$this->param['saveparam']:new BASE_DATAOBJECT_PARAM());

				// Gather columns and save values
				foreach ($this->field as $fieldTag=>$fieldObject)
				{
					if ($fieldObject->param['nosave']) continue;
					if ($fieldObject->noSave()) continue;
					$saveValue=$fieldObject->saveValue();
					if (is_array($saveValue))
					{
						foreach ($saveValue as $svKey => $svVal)
						{
							$param->addColumns($svKey);
							$this->data->$svKey=$svVal;
						}
					}
					elseif ($saveValue!==FALSE)
					{
						$param->addColumns($fieldTag);
						$this->data->$fieldTag=$saveValue;
					}
				}
				foreach ($this->fieldGroup as $fieldGroupTag=>$fieldGroupObject)
				{
					if (!$fieldGroupObject->checkable) continue;
					if (empty($fieldGroupObject->checkField)) continue;
					$saveValue=$fieldGroupObject->saveValue();
					if ($saveValue!==FALSE)
					{
						$param->addColumns($fieldGroupObject->checkField);
						$this->data->{$fieldGroupObject->checkField}=$saveValue;
					}
				}

				// Save data
				$logMessage=(isset($this->param['logmessage'])?$this->param['logmessage']:NULL);
				$logData=NULL;
				$refOID=(isset($this->param['logrefoid'])?(preg_match("/^[0-9]+$/",$this->param['logrefoid'])?$this->param['logrefoid']:$this->field[$this->param['logrefoid']]->saveValue()):NULL);
				$this->data->save($param,$logMessage,$logData,$refOID,$this->operation,$this);

				// Fire save triggers
				foreach ($this->field as $field_tag => &$fieldObject)
				{
					if ($fieldObject->param['nosave']) continue;
					if ($fieldObject->noSave()) continue;
					$fieldObject->triggerFormSave();
				}

				// Return successfully
				return '';
			}
			catch (Exception $e)
			{
				// Get error
				$err=$e->getMessage();

				// Check for common errors
				if (strpos($err,'MySQL query error: Duplicate entry')!==FALSE)
				{
					$err='[BASE.FORM.ERROR.DuplicateKey]';
				}

				// Return error
				throw new Exception('[BASE.FORM.Error.CouldNotSave]'.(!empty($err)?': '.$err:''));
			}
		}


		/**
		 *  Are we submitting?
		 *  @access public
		 *  @return boolean
		 */

		public function checkSubmit()
		{
			// Inline submit
			if (sizeof($this->submit))
			{
				foreach ($this->submit as $submit_name => $submit_value)
				{
					if (isset($_POST['submit_'.$submit_name])) return TRUE;
				}
			}
			else
			{
				if (isset($_POST['submit_save'])) return TRUE;
			}

			// AJAX submit
			if (!empty($_POST['ajaxsubmit']))
			{
				$this->XHR=TRUE;
				return TRUE;
			}
		}


		/**
		 *   Triggers: data pre-validation
		 *   @access public
		 *   @return void
		 *   @throws Exception
		 */

		public function dataPreValidate()
		{
			// This should be overwritten in the subclass if necessary
			return '';
		}


		/**
		 *   Triggers: data validation
		 *   @access public
		 *   @return void
		 *   @throws Exception
		 */

		public function dataValidate()
		{
			// This should be overwritten in the subclass if necessary
			return '';
		}


		/**
		 *   Validate fields
		 *   @access public
		 *   @return array
		 *   @throws Exception
		 */

		public function validateFields()
		{
			// Init
			$errors=array();

			// Validate fields
			foreach ($this->field as $fieldTag => &$fieldObject)
			{
				$fieldObject->triggerFormPreDisplay();
				$err=$fieldObject->validate($_POST[$fieldTag]);
				if (is_array($err))
				{
					foreach ($err as $fld => $e)
					{
						$errors[$fld]='<b>'.$fieldObject->param['title'].':</b> '.$e;
					}
				}
				elseif (mb_strlen($err))
				{
					$errors[$fieldTag]='<b>'.$fieldObject->param['title'].':</b> '.$err;
				}
			}

			// Return
			return $errors;
		}


		/**
		 *  Set add title
		 *  @access public
		 *  @param string title
		 *  @return void
		 */

		public function setAddTitle ( $title )
		{
			$this->param['title_add']=$title;
		}


		/**
		 *  Get add title
		 *  @access public
		 *  @return string title
		 */

		public function getAddTitle()
		{
			return $this->param['title_add'];
		}


		/**
		 *  Set edit title
		 *  @access public
		 *  @param string title
		 *  @return void
		 */

		public function setEditTitle ( $title )
		{
			$this->param['title_edit']=$title;
		}


		/**
		 *  Get edit title
		 *  @access public
		 *  @return string title
		 */

		public function getEditTitle()
		{
			return $this->param['title_edit'];
		}


		/**
		 *  Set operation
		 *  @access public
		 *  @param string operation
		 *  @return void
		 */

		public function setOperation ( $operation )
		{
			$this->param['operation']=$operation;
		}


		/**
		 *  Get operation
		 *  @access public
		 *  @return string operation
		 */

		public function getOperation()
		{
			return $this->param['operation'];
		}


		/**
		 *  Set URL
		 *  @access public
		 *  @param string return URL
		 *  @return void
		 */

		public function setURL ( $url )
		{
			$this->param['url']=$url;
		}


		/**
		 *  Get URL
		 *  @access public
		 *  @return string return URL
		 */

		public function getURL()
		{
			return $this->param['url'];
		}


		/**
		 *  Set return URL
		 *  @access public
		 *  @param string return URL
		 *  @return void
		 */

		public function setReturnURL ( $returnURL )
		{
			$this->param['url_return']=$returnURL;
		}


		/**
		 *  Get return URL
		 *  @access public
		 *  @return string return URL
		 */

		public function getReturnURL()
		{
			return $this->param['url_return'];
		}


		/**
		 *  Set cancel URL
		 *  @access public
		 *  @param string cancel URL
		 *  @cancel void
		 */

		public function setCancelURL ( $cancelURL )
		{
			$this->param['url_cancel']=$cancelURL;
		}


		/**
		 *  Get cancel URL
		 *  @access public
		 *  @cancel string cancel URL
		 */

		public function getCancelURL()
		{
			return $this->param['url_cancel'];
		}


		/**
		 *  Set field grouping
		 *  @access public
		 *  @param string return URL
		 *  @return void
		 */

		public function setFieldGrouping ( $fieldGrouping )
		{
			$this->param['fieldgrouping']=$fieldGrouping;
		}


		/**
		 *   Add a field
		 *   @access public
		 *   @return BASE_FIELD field object reference
		 *   @throws Exception
		 */

		public function addField( $fieldTag, $fieldData=NULL )
		{
			// Check if the action already exists
			if (isset($this->field[$fieldTag]))
			{
				throw new Exception('Field '.$fieldTag.' already exists.');
				return NULL;
			}

			// Field object passed
			if ($fieldData instanceof BASE_FIELD)
			{
				$this->field[$fieldTag]=$fieldData;
			}

			// Create blank field
			elseif (strlen($fieldData))
			{
				$fld=new $fieldData();
				$this->field[$fieldTag]=$fld;
			}

			// Otherwise - data objekti olemasolev field
			else
			{
				// Do we have a data object?
				if (!is_object($this->data)) throw new Exception('No data source defined.');
				if (!($this->data instanceof BASE_DATAOBJECT)) throw new Exception('Data source not a BASE_DATAOBJECT instance.');

				// Do we have this field?
				if (!is_object($this->data->_field[$fieldTag])) throw new Exception($fieldTag.': no such field defined in data object.');

				// Link
				$this->field[$fieldTag]=&$this->data->_field[$fieldTag];
			}

			// Set tag/column
			if (empty($this->field[$fieldTag]->tag)) $this->field[$fieldTag]->tag=$fieldTag;
			if (empty($this->field[$fieldTag]->fieldColumn)) $this->field[$fieldTag]->fieldColumn=$fieldTag;

			// Create backreference to the field
			$this->field[$fieldTag]->formObject=&$this;

			// Return reference to action
			$retval=&$this->field[$fieldTag];
			return $retval;
		}


		/**
		 *  Remove a field
		 *  @access public
		 *  @param string field tag
		 *  @return void
		 */

		public function removeField( $fieldTag )
		{
			// Check if the field exists
			if (!isset($this->field[$fieldTag])) throw new Exception('Field '.$fieldTag.' does not exist.');

			// Remove
			unset($this->field[$fieldTag]);
		}


		/**
		 *  Get field
		 *  @access public
		 *  @param string field tag
		 *  @return BASE_FIELD field object reference
		 */

		public function getField( $fieldTag )
		{
			// Check if the action already exists
			if (!isset($this->field[$fieldTag])) throw new Exception('Field '.$fieldTag.' does not exist.');

			// Return reference to field
			$retval=&$this->field[$fieldTag];
			return $retval;
		}


		/**
		 *   Add a field group
		 *   @access public
		 *   @param string fieldgroup tag
		 *   @return BASE_FORM_FIELDGROUP field object reference
		 *   @throws Exception
		 */

		public function addFieldGroup( $fieldGroupTag )
		{
			// Check if the action already exists
			if (isset($this->fieldGroup[$fieldGroupTag])) throw new Exception('Field group '.$fieldGroupTag.' already exists.');

			// Create blank field
			$fldGroup=new BASE_FORM_FIELDGROUP();
			$this->fieldGroup[$fieldGroupTag]=$fldGroup;
			$this->fieldGroup[$fieldGroupTag]->tag=$fieldGroupTag;
			$this->fieldGroup[$fieldGroupTag]->formObject=&$this;

			// Return reference
			$retval=&$this->fieldGroup[$fieldGroupTag];
			return $retval;
		}


		/**
		 *   Set submit btn title
		 *   @access public
		 *   @return string contents
		 */

		public function setSubmitButtonTitle( $title )
		{
			$this->param['submitbuttontitle']=$title;
		}


		/**
		 *   Set progress message
		 *   @access public
		 *   @return string contents
		 */

		public function setProgressMessage( $message )
		{
			$this->param['progressmessage']=$message;
		}


		/**
		 *   Set field label width
		 *   @access public
		 *   @param int $labelWidth Label width
		 *   @return void
		 */

		public function setFieldLabelWidth( $labelWidth )
		{
			$this->fieldLabelWidth=$labelWidth;
			$this->fieldContentWidth=12-$labelWidth;
		}


		/**
		 *   Set field content width
		 *   @access public
		 *   @param int $contentWidth Content width
		 *   @return void
		 */

		public function setFieldContentWidth( $contentWidth )
		{
			$this->fieldContentWidth=$contentWidth;
			$this->fieldLabelWidth=12-$contentWidth;
		}


		/**
		 *   Add JS
		 *   @access public
		 *   @string JS
		 *   @return void
		 */

		public function addJS( $js )
		{
			$this->js=$js.$this->js;
		}


		/**
		 *  Sanitize input
		 *  @access public
		 *  @return void
		 */

		public function sanitizeInput()
		{
			foreach ($_POST as $k => $v)
			{
				if (isset($this->field[$k]) && $this->field[$k]->allowHTML) continue;
				$_POST[$k]=BASE::sanitizeInput($v);
			}
		}


		/**
		 *   Display the form
		 *   @access private
		 *   @return string contents
		 */

		public function displayForm()
		{
			// Display parts
			$this->template->set('form','');

			$c=$this->displayTitle();
			$c.=$this->displayFormBegin();
			$c.=$this->displayFormContents();
			$c.=$this->displayFormSubmit();
			$c.=$this->displayFormEnd();
			$c.=$this->displayNotes();
			$c.=$this->displayJS();
			$this->template->set('contents',$c);

			// Return
			return $this->template->display();
		}


		/**
		 *  Display title
		 *  @access private
		 *  @return string contents
		 */

		public function displayTitle()
		{
			// Title
			if ($this->operation=='add')
			{
				$title=oneof($this->param['title_add'],$this->param['title']);
			}
			else
			{
				$title=oneof($this->param['title_edit'],$this->param['title']);
			}
			if (mb_strlen($title))
			{
				$this->template->set('title',str_replace('$operation','[BASE.FORM.Title.'.$this->operation.']',$title));
			}

			// Alerts/warnings
			if (!empty($this->param['alert']))
			{
				$this->template->set('alert',$this->param['alert']);
			}

			// Info
			if (!empty($this->param['info']))
			{
				$this->template->set('info',$this->param['info']);
			}
		}


		/**
		 *  Display form begin
		 *  @access private
		 *  @return string contents
		 */

		public function displayFormBegin()
		{
			// The form begins
			$form_begin='<form class="form-horizontal" role="form" id="labengine-form" method="post" action="'.$this->buildURL($this->param['url'], FALSE).'" onsubmit="FORM.submit('.(!empty($this->param['progressmessage'])?"'save',null,null,'".$this->param['progressmessage']."'":"").')">';

			// ID field if it's not in the fieldset
			if (!empty($this->data->_param['idfield']) && ($this->operation=='edit' && !is_object($this->field[$this->data->_param['idfield']])))
			{
				$form_begin.='<input type="hidden" id="'.$this->data->_param['idfield'].'" name="'.$this->data->_param['idfield'].'" value="'.htmlspecialchars($_POST[$this->data->_param['idfield']]).'" />';
			}

			// Passthru fields if they're not separately defined
			foreach ($this->passthru as $ptVar => $ptObject)
			{
				if (isset($this->field[$ptVar])) continue;
				if ($ptVar==$this->data->_param['idfield']) continue;
				$form_begin.='<input type="hidden" id="'.$ptVar.'" name="'.$ptVar.'" value="'.htmlspecialchars($_POST[$ptVar]).'" />';
			}

			// Set template items
			$this->template->set('url_submit',$this->buildURL($this->param['url'], FALSE));
			$this->template->set('form_begin',$form_begin);
			$this->template->templateVar['form'].=$form_begin;

			// Submit button title
			if (!empty($this->param['submitbuttontitle']))
			{
				$this->template->set('submitbuttontitle',$this->param['submitbuttontitle']);
			}

			// Return
			return $form_begin;
		}


		/**
		 *   Display the form contents
		 *   @access private
		 *   @return string contents
		 *   @throws Exception
		 */

		public function displayFormContents()
		{
			// Check
			if (!sizeof($this->field)) throw new Exception('No fields defined.');

			// Display the form elements
			$form_field='';
			$form_fieldset=array();
			$form_fieldgroup=array();
			$form_fieldgroupset=array();
			if (!empty($this->param['fieldgrouping']))
			{
				$fldGroupSet=array();
				foreach ($this->fieldGroup as $fieldGroupTag => &$fieldGroup)
				{
					$f=array();
					$f['title']=$fieldGroup->title;
					$f['info']=$fieldGroup->info;
					$f['indented']=$fieldGroup->indented;
					$f['class']=$fieldGroup->class;
					$f['checkable']=$fieldGroup->checkable;
					$f['checkfield']=oneof($fieldGroup->checkField,$fieldGroupTag);
					if ($fieldGroup->checkable && !empty($fieldGroup->checkField))
					{
						$f['value']=$this->data->{$fieldGroup->checkField};
					}
					$f['actions']=join('',$fieldGroup->actions);
					if (!empty($fieldGroup->event))
					{
						$f['event']=$fieldGroup->event;
					}
					$fldGroupSet[$fieldGroupTag]=$f;
				}
				$this->template->set('form_fieldgrouping',TRUE);
				$this->template->set('form_fieldgroup',$fldGroupSet);
			}
			foreach ($this->field as $field_tag => &$field_object)
			{
				$form_fieldset[$field_tag]=$field_tag;
				$fld=$field_object->display();
				$form_field.=$fld;
				$this->template->set('field_'.$field_tag,$fld);
				if (mb_strlen($field_object->param['fieldgroup']))
				{
					$form_fieldgroupset[$field_object->param['fieldgroup']][$field_tag]=$field_tag;
					$form_fieldgroup[$field_object->param['fieldgroup']].=$fld;
				}
			}
			$this->template->set('form_field',$form_field);
			$this->template->set('form_fieldset',$form_fieldset);
			if (sizeof($form_fieldgroup))
			{
				$this->template->set('form_fieldgroupset',$form_fieldgroupset);
				foreach ($form_fieldgroup as $grp_id => $grp_contents)
				{
					$this->template->set('fieldgroup_'.$grp_id,$grp_contents);
				}
			}
			$this->template->templateVar['form'].=$form_field;

			// Return
			return $form_field;
		}


		/**
		 *   Display submit buttons
		 *   @access private
		 *   @return string contents
		 */

		public function displayFormSubmit()
		{
			$form_submitset=array();
			$form_submit='<div class="submit">';
			if (!$this->DISABLED)
			{
				if (sizeof($this->submit))
				{
					foreach ($this->submit as $submit_name => $submit_value)
					{
						$form_submitset[$submit_name]=$submit_name;
						$submit='<button type="submit" id="submit_'.$submit_name.'" class="btn btn-primary" name="submit_'.$submit_name.'">'.$submit_value.'</button>';
						$this->template->set('submit_'.$submit_name,$submit);
						$form_submit.=$submit;
					}
				}
				else
				{
					$form_submitset[$this->operation]=$this->operation;
					$submit='<button type="button" id="submit_save" name="submit_save" class="btn btn-primary" onclick="FORM.submit('.(!empty($this->param['progressmessage'])?"'save',null,null,'".$this->param['progressmessage']."'":"").')">'.(!empty($this->param['submitbuttontitle'])?$this->param['submitbuttontitle']:'[BASE.FORM.Btn.'.$this->operation.']').'</button>';
					$this->template->set('submit_'.$this->operation,$submit);
					$form_submit.=$submit;
				}
			}
			$form_submitset['cancel']='cancel';
			$submit='<button type="button" id="submit_cancel" name="submit_cancel" class="btn" onclick="FORM.cancel()">[BASE.FORM.Btn.Cancel]</button>';
			$this->template->set('submit_cancel',$submit);
			$form_submit.=$submit;
			$form_submit.='</div>';
			$this->template->set('form_submit',$form_submit);
			$this->template->templateVar['form'].=$form_submit;

			// Return
			return $form_submit;
		}


		/**
		 *  Display form end
		 *  @access private
		 *  @return string contents
		 */

		public function displayFormEnd()
		{
			$form_end='</form>';
			$this->template->set('form_end',$form_end);
			$this->template->templateVar['form'].=$form_end;
			return $form_end;
		}


		/**
		 *  Display notes
		 *  @access private
		 *  @return string contents
		 */

		public function displayNotes()
		{
			if (sizeof($this->notes))
			{
				$this->template->set('notes','<div class="note">'.join('</div><div class="note">',$this->notes).'</div>');
			}
			return $this->template->templateVar['notes'];
		}


		/**
		 *  Display JavaScript
		 *  @access private
		 *  @return string contents
		 */

		public function displayJS()
		{
			if (mb_strlen($this->js))
			{
				$this->template->set('js',$this->js);
			}
			return $this->template->templateVar['js'];
		}


		/**
		 *  Display form
		 *  @access public
		 *  @return string contents
		 */

		public function display()
		{
			global $LAB;
			try
			{
				// Set defaults
				$this->setDefaults();

				// Cancel
				if (isset($_POST['submit_cancel']))
				{
					$LAB->RESPONSE->responseRedirect=$this->buildURL(oneof($this->param['url_cancel'],$this->param['url_return']));
					return;
				}

				// Init form
				$this->initForm();

				// Load data
				if ($this->operation=='edit') $this->dataLoad();

				// Are we submitting?
				$this->doSubmit=$this->checkSubmit();

				// Init fields
				$this->initFields();

				// Sanitize input
				$this->sanitizeInput();

				// Handle submit
				if ($this->doSubmit)
				{
					// Init
					$this->errors=array();

					// Prevalidate
					$err=$this->dataPreValidate();
					if (is_array($err))
					{
						$this->errors=array_merge($this->errors, $err);
					}
					elseif (!empty($err))
					{
						$this->errors['PREGLOBAL']=$err;
					}

					if (!sizeof($this->errors))
					{
						// Validate fields
						$err=$this->validateFields();
						if (is_array($err) && sizeof($err))
						{
							$this->errors=array_merge($this->errors, $err);
						}

						// Do global error checking
						$err=$this->dataValidate();
						if (is_array($err))
						{
							$this->errors=array_merge($this->errors, $err);
						}
						else
						{
							if (strlen($err)) $this->errors['GLOBAL']=$err;
						}
					}

					// Save data
					if (!sizeof($this->errors))
					{
						$err=$this->dataSave();
						if (!empty($err)) $this->errors['GLOBAL']=$err;
					}

					// Return on errors and XHR
					if (sizeof($this->errors) && $this->XHR)
					{
						$LAB->RESPONSE->setType('json');
						$response=array();
						$response['status']='2';
						$response['error']=$this->errors;
						return $response;
					}

					// Success
					if (!sizeof($this->errors))
					{
						if ($this->XHR)
						{
							$LAB->RESPONSE->setType('json');
							$response=array();
							$response['status']='1';
							$response['redirect']=$this->buildURL($this->param['url_return']);
							return $response;
						}
						else
						{
							if (empty($LAB->RESPONSE->responseRedirect)) $LAB->RESPONSE->responseRedirect=$this->buildURL($this->param['url_return']);
							return;
						}
					}
				}

				// Display form
				return $this->displayForm();
			}
			catch (Exception $e)
			{
				if ($this->XHR)
				{
					$LAB->RESPONSE->setType('json');
					$response=array();
					$response['status']='2';
					$response['error']=$e->getMessage();
					return $response;
				}
				else
				{
					$c='';
					if ($this->operation=='add')
					{
						$c.= '<h1>'.oneof($this->param['title_add'],$this->param['title']).'</h1>';
					}
					else
					{
						$c.= '<h1>'.oneof($this->param['title_edit'],$this->param['title']).'</h1>';
					}
					$c.='<div class="alert alert-danger">'.$e->getMessage().'</div>';
					$c.='<div class="back-to-previous"><a onclick="FORM.cancel()"><span class="icon-back"></span>[BASE.LIST.Return]</a></div>';
					return $c;
				}
			}
		}


	}


?>