<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   Response bundle: CSS
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_RESPONSE_CSS
	{


		/**
		 *   Response CSS files
		 *   @var string
		 *   @access public
		 */

		public $responseCSS = array();


		/**
		 *   Response external CSS files
		 *   @var string
		 *   @access public
		 */

		public $responseExternalCSS = array();


		/**
		 *   Add a CSS file
		 *   @access public
		 *   @param string $cssFilename Filename
		 *   @param string $cssID ID
		 *   @param int $cssPriority Priority (1-9)
		 *   @param string $cssMedia Media
		 *   @return void
		 */

		public function addCSS ( $cssFilename, $cssID='', $cssPriority=5 )
		{
			if (!strlen($cssID)) $cssID=strval(sizeof($this->responseCSS)+1);
			$this->responseCSS[$cssID]=array
			(
				'type' => 'file',
				'filename' => $cssFilename,
				'priority' => $cssPriority
			);
		}


		/**
		 *   Add dynamic CSS
		 *   @access public
		 *   @param string $cssSource Source
     *   @param int $modifiedTimestamp Modified timestamp
		 *   @param string $cssID ID
		 *   @param int $cssPriority Priority (1-9)
		 *   @param string $cssMedia Media
		 *   @return void
		 */

		public function addDynamicCSS ( $cssSource, $modifiedTimestamp, $cssID, $cssPriority=5 )
		{
			if (!strlen($cssID)) throw new Exception('ID is required, but empty for dynamic CSS.');
			$this->responseCSS[$cssID]=array
			(
				'type' => 'string',
				'source' => $cssSource,
				'mtime' => $modifiedTimestamp,
				'priority' => $cssPriority
			);
		}


		/**
		 *   Add an external CSS file
		 *   @access public
		 *   @param string $cssURL URL
		 *   @param string $cssID ID
		 *   @param int $cssPriority Priority (1-9)
		 *   @param string $cssMedia Media
		 *   @return void
		 */

		public function addExternalCSS ( $cssURL, $cssID='', $cssPriority=5 )
		{
			if (!strlen($cssID)) $cssID=strval(sizeof($this->responseCSS)+1);
			$this->responseExternalCSS[$cssID]=array
			(
				'type' => 'file',
				'url' => $cssURL,
				'priority' => $cssPriority
			);
		}


		/**
		 *   Minify CSS
		 *   @access public
		 *   @return string
		 *   @throws Exception
		 */

		public function minifyCSS( $CSS )
		{
			// Remove comments
			$CSS=preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!','',$CSS);

			// Remove space after colons
			$CSS=str_replace(': ', ':',$CSS);

			// Remove whitespace
			$CSS=str_replace(array("\r\n","\r","\n","\t",'  ','   ','    ','     ','      '),'',$CSS);

			// Return
			return $CSS;
		}


		/**
		 *   Parse Less & minify
		 *   @access public
		 *   @return string
		 *   @throws Exception
		 */

		public function parseLess( $CSS )
		{
			global $LAB;
			try
			{
				// Init less.php
				include_once 'base/contrib/LessPHP/Less.php';
				$PARSER=new Less_Parser(array(
					'compress' => true,
					'strictMath' => true
				));

				// Do the magic
				$PARSER->parse($CSS);

				// Return
				return $PARSER->getCss();
			}
			catch (Exception $e)
			{
				throw new Exception('Error parsing CSS/LESS: '.$e->getMessage());
			}
		}


		/**
		 *   Output CSS
		 *   @access public
		 *   @return string
		 *   @throws Exception
		 */

		public function outputCSS()
		{
			global $LAB;
			$c='';

			$this->addCSS('base/static/summernote/summernote.css','summernote','9');

			// Build asset array
			$assetArray=array();
			$assetTimeStamp=0;
			$this->responseCSS=sortdataset($this->responseCSS,'priority');
			foreach ($this->responseCSS as $cssID => $cssProperties)
			{
				if ($cssProperties['type']=='string')
				{
					$assetArray[$cssID]=$cssID;
					$filemtime=$cssProperties['mtime'];
				}
				else
				{
					if (!stream_resolve_include_path($cssProperties['filename'])) throw new Exception('CSS file not readable: '.$cssProperties['filename']);
					$assetArray[$cssID]=$cssProperties['filename'];
					$filemtime=filemtime(stream_resolve_include_path($cssProperties['filename']));
				}
				if ($filemtime>$assetTimeStamp) $assetTimeStamp=$filemtime;
			}
			$assetHash=md5(join('|',array_keys($assetArray)));
			$assetFileName=BASE::getTmpDir().'/'.$LAB->CONFIG->get('site.id').'.asset.css.'.$assetHash.'.'.intval($assetTimeStamp).'.css';

			// If file does not exist, build it
			if (!file_exists($assetFileName))
			{
				$assetFileContents='';
				foreach ($this->responseCSS as $cssID => $cssProperties)
				{
					if ($cssProperties['type']=='string')
					{
						$assetFileContents.=$cssProperties['source'];
					}
					else
					{
						$CSS=file_get_contents(stream_resolve_include_path($cssProperties['filename']));
						$assetFileContents.=$CSS;
					}
				}
				$assetFileContents=$this->parseLess($assetFileContents);
				file_put_contents($assetFileName,$assetFileContents);
			}

			// CSS bundle link
			$c.='<link rel="stylesheet" href="/base/css/bundle/'.$assetHash.'.'.intval($assetTimeStamp).'.css">';

			// External CSS
			if (sizeof($this->responseExternalCSS))
			{
				$this->responseExternalCSS=sortdataset($this->responseExternalCSS,'priority');
				foreach ($this->responseExternalCSS as $cssID => $cssProperties)
				{
					$c.='<link rel="stylesheet" href="'.$cssProperties['url'].'">';
				}
			}

			// Return
			return $c;
		}


	}


?>