<?php


/**
 *
 *   LabEngine™ 7
 *   BankLink integration: base ECommerce class
 *
 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
 *
 */


class BASE_BANKLINK_ECOMMERCE extends BASE_BANKLINK
{


    /**
     *   Variable order for iPizza requests
     *   @var array
     */

    public static $VAR_order = array(
        // Payment request
        'request' => array(
            'lang', // ISO 639-1
            'action', // gaf
            'ver', // 004
            'id', // web service id (available payment service contract)
            'ecuno',
            'eamount', // Payment amount (cents)
            'cur', // ISO-4217
            'datetime', // YYYYMMDDhhmmss (ISO-8601)
            'charEncoding', // UTF-8 (default - iso-8859-15)
            'feedBackUrl',
            'delivery', // delivery symbol (S)
            'additionalinfo', // optional description
            'mac' // ver + id + ecuno + eamount + cur + datetime + feedBackUrl + delivery
        ),

        // Payment confirmation (response to feedBackUrl)
        // Check response MAC - ver + id + ecuno + receipt_no + eamount + cur + respcode + datetime + msgdata + actiontext
        'response' => array(
            'ver', // 004
            'id', // web service id (available payment service contract)
            'ecuno',
            'receipt_no', // receipt counter
            'eamount', // payment amount (cents)
            'cur', // ISO-4217
            'respcode', // respond code (000-ok)
            'datetime', // YYYYMMDDhhmmss (ISO-8601)
            'msgdata', // payment description
            'actiontext', // response code description
            'mac' // ver + id + ecuno + receipt_no + eamount + cur + respcode + datetime + msgdata + actiontext
        )
    );

    public static $VAR_mac_positions = array(
        // Payment request
        'request' => array(
            'ver',
            'id',
            'ecuno',
            'eamount',
            'cur',
            'datetime',
            'feedbackurl',
            'delivery',
            'additionalinfo'
        ),

        // Payment confirmation (response to feedBackUrl)
        // Check response MAC - ver + id + ecuno + receipt_no + eamount + cur + respcode + datetime + msgdata + actiontext
        'response' => array(
            'ver',
            'id',
            'ecuno',
            'receipt_no',
            'eamount',
            'cur',
            'respcode',
            'datetime',
            'msgdata',
            'actiontext'
        )
    );

    public static $VAR_order_options = array(
        'ver' => [
            'pad' => 3,
            'string' => "0",
            'type' => STR_PAD_LEFT
        ],
        'id' => [
            'pad' => 10,
            'string' => " ",
            'type' => STR_PAD_RIGHT
        ],
        'ecuno' => [
            'pad' => 12,
            'string' => "0",
            'type' => STR_PAD_LEFT
        ],
        'eamount' => [
            'pad' => 12,
            'string' => "0",
            'type' => STR_PAD_LEFT
        ],
        'cur' => [
            'pad' => 3,
            'string' => " ",
            'type' => STR_PAD_RIGHT
        ],
        'datetime' => [
            'pad' => 14,
            'string' => " ",
            'type' => STR_PAD_RIGHT
        ],
        'feedBackUrl' => [
            'pad' => 128,
            'string' => " ",
            'type' => STR_PAD_RIGHT
        ],
        'additionalinfo' => [
            'pad' => 128,
            'string' => " ",
            'type' => STR_PAD_RIGHT
        ],
        'receipt_no' => [
            'pad' => 6,
            'string' => "0",
            'type' => STR_PAD_LEFT
        ],
        'respcode' => [
            'pad' => 3,
            'string' => "0",
            'type' => STR_PAD_LEFT
        ],
        'msgdata' => [
            'pad' => 40,
            'string' => " ",
            'type' => STR_PAD_RIGHT
        ],
        'actiontext' => [
            'pad' => 40,
            'string' => " ",
            'type' => STR_PAD_RIGHT
        ]
    );


    /**
     *   Language codes mapping
     *   @var array
     */
    public static $MAP_lang = array(
        'et' => 'EST',
        'en' => 'ENG',
        'ru' => 'RUS'
    );


    /**
     *   Bank public key
     *   @var string
     */

    public $BANK_public_key = null;


    /**
     *   Bank destination URL
     *   @var string
     */

    public $BANK_destination_url = null;


    /**
     *   Merchant snd_id
     *   @var string
     */

    public $MERCHANT_snd_id = null;


    /**
     *   Merchant private key
     *   @var string
     */

    public $MERCHANT_private_key = null;


    /**
     *   Merchant private key passphrase
     *   @var string
     */

    public $MERCHANT_private_key_pass = '';


    /**
     *   Merchant return/notify URL
     *   @var string
     */

    public $MERCHANT_return_url = null;


    /*
     *   Init
     *   @return void
     */

    public function init()
    {
        // This should be implemented in the subclass
    }


    /*
     *   Set bank public key
     *   @param string $bankPublicKey Path to bank public key
     *   @return void
     */

    public function setBankPublicKey( $bankPublicKey )
    {
        $this->BANK_public_key=$bankPublicKey;
    }


    /*
     *   Set bank destination URL
     *   @param string $bankDestinationURL Bank destination URL
     *   @return void
     */

    public function setBankDestinationURL( $bankDestinationURL )
    {
        $this->BANK_destination_url=$bankDestinationURL;
    }


    /*
     *   Set merchant snd id
     *   @param string $merchantSndID Merchant snd_id
     *   @return void
     */

    public function setMerchantSndID( $merchantSndID )
    {
        $this->MERCHANT_snd_id=$merchantSndID;
    }


    /*
     *   Set merchant private key and passphrase
     *   @param string $merchantPrivateKey Path to private key
     *   @param string $merchantPrivateKeyPass Private key passphrase
     *   @return void
     */

    public function setMerchantPrivateKey( $merchantPrivateKey, $merchantPrivateKeyPass='' )
    {
        $this->MERCHANT_private_key=$merchantPrivateKey;
        $this->MERCHANT_private_key_pass=$merchantPrivateKeyPass;
    }


    /*
     *   Set merchant snd id
     *   @param string $merchantSndID Merchant snd_id
     *   @return void
     */

    public function setReturnURL( $returnURL )
    {
        $this->MERCHANT_return_url=$returnURL;
    }


    /**
     *   Generates MAC string as needed according to the service number
     *   @access public
     *   @param  array $macFields MAC fields
     *   @return string MAC string
     */

    public function generateMacString( $macFields )
    {
        // Get service number
        $serviceAction = 'request';
        if (isset($macFields['respcode'])) {
            $serviceAction = 'response';
        }
        $encoding = 'UTF-8';

        // Init
        $data='';

        // Append data as needed
        foreach (static::$VAR_mac_positions[$serviceAction] as $key)
        {
            $value=$macFields[$key];
            if (array_key_exists($key, static::$VAR_order_options)) {
                $padLength = static::$VAR_order_options[$key]['pad'];
                $padString = static::$VAR_order_options[$key]['string'];
                $padType = static::$VAR_order_options[$key]['type'];
                $data.=static::mbStrPad($value, $padLength, $padString, $padType, $encoding);
            }
        }

        // Return data
        return $data;
    }


    /**
     *   Validate bank response
     *   @param  array $requestData Fields received from the bank
     *   @throws Exception
     *   @return string Result (completed/cancelled/failed/not-implemented)
     */

    public function validateResponse( $requestData, $throwException=true )
    {
        // Check
        if (!mb_strlen($this->BANK_public_key)) throw new Exception('Bank public key not set.');
        $BANK_public_key=stream_resolve_include_path($this->BANK_public_key);
        if (empty($BANK_public_key)) throw new Exception('Bank public key not found.');

        // Init
        $responseCode = $requestData['respcode'];

        $macFields=array();

        // Generate MAC fields
        foreach ($requestData as $k => $v)
        {
            $macFields[$k] = $v;
        }

        // Get public key
        $key=openssl_pkey_get_public(file_get_contents($BANK_public_key));
        $macString=$this->generateMacString($macFields);
        $verify_mac=openssl_verify($macString, pack('H*', $requestData['mac']), $key,OPENSSL_ALGO_SHA1);

        // Check result
        if ($verify_mac===1)
        {
            // Correct signature
            if ( $responseCode == '000' )
            {
                return 'completed';
            }
            else
            {
                return 'cancelled';
            }
        }

        if ($throwException) throw new Exception('Validation failed.');
        return 'failed';
    }


    /**
     *   Get payment data from response
     *   @param  array $requestData Fields received from the bank
     *   @throws Exception
     *   @return array Result
     */

    public function getPaymentInfo( $requestData, $throwException=true )
    {
        // Init
        $paymentInfo=array();

        // Makse ID
        if (!empty($requestData['receipt_no'])) $paymentInfo['payment_id']=$requestData['receipt_no'];

        // Viitenumber
        if (!empty($requestData['ecuno'])) $paymentInfo['payment_refno']=$requestData['ecuno'];

        // Kirjeldus
        if (!empty($requestData['msgdata'])) $paymentInfo['payment_description']=$requestData['msgdata'];

        // Summa
        if (!empty($requestData['eamount'])) $paymentInfo['payment_sum']=round($requestData['eamount'] / 100,2);
        if (!empty($requestData['cur'])) $paymentInfo['payment_currency']=$requestData['cur'];

        // Makse aeg
        if (!empty($requestData['datetime'])) $paymentInfo['payment_tstamp']=date('Y-m-d H:i:s',strtotime($requestData['datetime']));

        // Return
        return $paymentInfo;
    }


    /**
     *   Generate payment form
     *   @public
     *   @param string $paymentID Order ID
     *   @param float $paymentSum Order sum
     *   @param string $paymentDescription Order description
     *   @param int $paymentReferenceNo Order reference no
     *   @return string Payment form
     *   @throws Exception
     */

    function generatePaymentForm( $paymentID, $paymentSum, $paymentDescription=null, $paymentReferenceNo=null )
    {
        global $LAB;

        // Check & init
        if (empty($paymentID)) throw new Exception('Order ID not set.');
        if (!floatval($paymentSum)) throw new Exception('Order sum not set.');
        if (empty($paymentDescription)) $paymentDescription=$paymentID;
        if (empty($paymentReferenceNo)) $paymentReferenceNo=static::generateRefNo($paymentID);
        if (empty($this->MERCHANT_snd_id)) throw new Exception('Merchant ID not set.');
        if (empty($this->MERCHANT_private_key)) throw new Exception('Private key not set.');
        if (empty($this->MERCHANT_return_url)) throw new Exception('Return URL not set.');
        if (empty($this->BANK_destination_url)) throw new Exception('Bank destination URL not set.');
        $MERCHANT_private_key=stream_resolve_include_path($this->MERCHANT_private_key);
        if (empty($MERCHANT_private_key)) throw new Exception('Private key not found.');

        // Current time
        $datetime=new DateTime('NOW', new \DateTimeZone('Europe/Tallinn'));

        // Set MAC fields
        $macFields  = array(
            'ver'  => '004',
            'id'  => $this->MERCHANT_snd_id,
            'ecuno'   => $paymentReferenceNo,
            'eamount'    => round($paymentSum * 100,0),
            'cur'   => 'EUR',
            'datetime'     => $datetime->format(DateTime::ISO8601),
            'feedBackUrl'      => $this->MERCHANT_return_url,
            'delivery'      => 'S',
            'additionalinfo' => 'paymentID:'.$paymentID.';'
        );

        $allFields = array(
            'action' => 'gaf',
            'ver'  => '004',
            'id'  => $this->MERCHANT_snd_id,
            'ecuno'   => $paymentReferenceNo,
            'eamount'    => round($paymentSum * 100,0),
            'cur'   => 'EUR',
            'datetime'     => $datetime->format(DateTime::ISO8601),
            'charEncoding' => 'UTF-8',
            'feedBackUrl'      => $this->MERCHANT_return_url,
            'delivery'      => 'S',
            'additionalinfo' => 'paymentID:'.$paymentID.';'
        );

        if (!empty($paymentDescription) && $paymentDescription != $paymentID) {
            $macFields['additionalinfo'] = $macFields['additionalinfo'].'description:'.$paymentDescription.';';
            $allFields['additionalinfo'] = $allFields['additionalinfo'].'description:'.$paymentDescription.';';
        }

        // Generate MAC string from the private key
        $key=openssl_pkey_get_private(file_get_contents($MERCHANT_private_key),$this->MERCHANT_private_key_pass);
        $macString=$this->generateMacString($macFields);
        $signature='';

        // Try to sign the MAC string
        if (!openssl_sign($macString,$signature, $key,OPENSSL_ALGO_SHA1)) throw new Exception('Unable to generate signature.');

        // Encode signature
        $allFields['mac']=bin2hex($signature);

        // Language
        if (!empty($LAB->REQUEST->requestLang) && strlen($LAB->REQUEST->requestLang) == 2)
        {
            $allFields['lang']=$LAB->REQUEST->requestLang;
        }
        else
        {
            $allFields['lang']='et';
        }

        // Form begins
        $form='<form action="'.htmlspecialchars($this->BANK_destination_url).'" method="post" id="banklink_'.htmlspecialchars(static::$BANK_id).'_payment_form">';

        // Add fields
        foreach($allFields as $field => $value )
        {
            $form.='<input type="hidden" name="'.htmlspecialchars($field).'" value="'.htmlspecialchars($value) .'" />';
        }
        // Form ends
        $form.='</form>';
        return $form;
    }

    public static function mbStrPad($input, $padLength, $padString = ' ', $padType = STR_PAD_RIGHT, $encoding = null)
    {
        if (is_null($input) || strlen($input) === 0) {
            $input = '';
        }
        $diff = strlen($input) - mb_strlen($input);
        if ($encoding) {
            $diff = strlen($input) - mb_strlen($input, $encoding);
        }
        return str_pad($input, $padLength + $diff, $padString, $padType);
    }

}


?>