<?


/**
 *
 *   LabEngine™ 7
 *   Mailbow integration class
 *
 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
 *
 */


class BASE_MAILBOW
{


	/**
	 *   API URL
	 *   @var string
	 */

	public $MAILBOW_api_url = null;

	/**
	 *   API username
	 *   @var string
	 */

	public $MAILBOW_api_username = null;

	/**
	 *   API password
	 *   @var string
	 */

	public $MAILBOW_api_password = null;


	/**
	 *   Constructor.
	 *   @param null $apiURL
	 *   @param null $apiKey
	 *   return BASE_MAILBOW
	 */

	public function __construct ( $apiURL=null, $apiUsername=null, $apiPassword=null )
	{
		// Set parameters if passed
		if (!empty($apiURL))
		{
			$this->MAILBOW_api_url=$apiURL;
		}
		if (!empty($apiUsername))
		{
			$this->MAILBOW_api_username=$apiUsername;
		}
		if (!empty($apiPassword))
		{
			$this->MAILBOW_api_password=$apiPassword;
		}
	}


	/**
	 *   Set API URL
	 *   @access public
	 *   @param string $apiURL API URL
	 *   @return void
	 */

	public function setApiURL( $apiURL )
	{
		$this->MAILBOW_api_url=$apiURL;
	}


	/**
	 *   Set API username
	 *   @access public
	 *   @param string $apiURL API URL
	 *   @return void
	 */

	public function setApiUsername( $apiUsername )
	{
		$this->MAILBOW_api_username=$apiUsername;
	}


	/**
	 *   Set API username
	 *   @access public
	 *   @param string $apiURL API URL
	 *   @return void
	 */

	public function setApiPassword( $apiPassword )
	{
		$this->MAILBOW_api_password=$apiPassword;
	}


	/**
	 *   Subscribe user to list
	 *   @access public
	 *   @param string $list_id List ID
	 *   @param string $email E-mail address
	 *   @param array $additionalData Additional data
	 *   @param bool $confirm
	 *   @param bool $throwException
	 *   @return bool
	 *   @throws Exception
	 */

	public function subscribe( $list_id, $email, $additionalData=array(), $confirm=true, $throwException=true )
	{
		global $LAB;

		// Sanity checks
		if (empty($this->MAILBOW_api_url)) throw new Exception('API URL not set.');
		if (empty($this->MAILBOW_api_username)) throw new Exception('API username not set.');
		if (empty($this->MAILBOW_api_password)) throw new Exception('API password not set.');
		if (empty($list_id)) throw new Exception('List ID not set.');
		if (empty($email)) throw new Exception('E-mail address empty.');

		// Subscribe
		try
		{
			$request=array(
				array(
					"email"=>$email,
					"optin"=>"yes",
					"addLists"=>array(
						array(
							"id"=>$list_id
						)
					)
				)
			);
			$request=json_encode($request);

			$HTTP=new BASE_HTTPREQUEST();
			$HTTP->setURL($this->MAILBOW_api_url);
			$HTTP->setRequestMethod('POST');
			$HTTP->setOptions(array(
				'username' => $this->MAILBOW_api_username,
				'password' => $this->MAILBOW_api_password
			));
			$HTTP->setRequestHeader('Content-Type', 'application/json');
			$HTTP->setRawPostData($request);
			$mailBowResponse=$HTTP->send();
			$mailBowResponse=json_decode($mailBowResponse,true);

			if ($mailBowResponse['status']=='error')
			{
				throw new Exception($mailBowResponse['error']);
			}

			return true;
		}
		catch (Exception $e)
		{
			if ($throwException) throw $e;
			return false;
		}
	}


	/**
	 *   Unsubscribe user from list
	 *   @param string $email E-mail address
	 *   @param bool $throwException Throw exception on failure
	 *   @return bool
	 *   @throws Exception
	 */

	public function unsubscribe( $list_id, $email, $throwException=true )
	{
		global $LAB;

		// Sanity checks
		if (empty($this->MAILBOW_api_url)) throw new Exception('API URL not set.');
		if (empty($this->MAILBOW_api_username)) throw new Exception('API username not set.');
		if (empty($this->MAILBOW_api_password)) throw new Exception('API password not set.');
		if (empty($list_id)) throw new Exception('List ID not set.');
		if (empty($email)) throw new Exception('E-mail address empty.');

		// Unsubscribe
		try
		{
			$request=array(
				array(
					"email"=>$email,
					"optin"=>"no",
					"addLists"=>array(
						array(
							"id"=>$list_id
						)
					)
				)
			);
			$request=json_encode($request);

			$HTTP=new BASE_HTTPREQUEST();
			$HTTP->setURL($this->MAILBOW_api_url);
			$HTTP->setRequestMethod('POST');
			$HTTP->setOptions(array(
				'username' => $this->MAILBOW_api_username,
				'password' => $this->MAILBOW_api_password
			));
			$HTTP->setRequestHeader('Content-Type', 'application/json');
			$HTTP->setRawPostData($request);
			$mailBowResponse=$HTTP->send();
			$mailBowResponse=json_decode($mailBowResponse,true);

			if ($mailBowResponse['status']=='error')
			{
				throw new Exception($mailBowResponse['error']);
			}

			return true;
		}
		catch (Exception $e)
		{
			if ($throwException) throw $e;
			return false;
		}
	}


}


?>