<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   DigiDoc helper functions: base class
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_DIGIDOC
	{


		/**
		 *   File type (bdoc, ddoc)
		 *   @var string
		 */

		public $type = null;


		/**
		 *   Files
		 *   @var array
		 */

		public $files = array();


		/**
		 *   Signatures
		 *   @var array
		 */

		public $signatures = array();


		/**
		 *   Keys
		 *   @var array
		 */

		public $keys = array();


		/**
		 *   File info
		 *   @var array
		 */

		public $info = array();


		/**
		 *   Open digidoc
		 *   @access public
		 *   @static
		 *   @var string $filename
		 *   @var string $fileType File type
		 *   @var string $originalFilename Original file name
		 *   @var bool $fileType Open file as an object
		 *   @throws Exception
		 *   @return BASE_DIGIDOC
		 */

		public static function openDigiDoc( $filename, $fileType=null, $originalFilename=null, $openFileAsObject = false )
		{
			global $LAB;

			// Check
			if (!is_readable($filename)) throw new Exception('File not readable.');

			// Detect type
			if (!empty($fileType))
			{
				$fileType=mb_strtoupper($fileType);
				if (!in_array($fileType,array('BDOC','CDOC','DDOC','ASICE'))) throw new Exception('Unknown file type - must be either BDOC or CDOC or DDOC');
				if ($fileType=='ASICE') $fileType='BDOC';
			}
			else
			{
				$pathInfo=pathinfo(oneof($originalFilename,$filename));
				switch (mb_strtolower($pathInfo['extension']))
				{
					case 'ddoc':
						$fileType='DDOC';
						break;
					case 'bdoc':
						$fileType='BDOC';
						break;
					case 'cdoc':
						$fileType='CDOC';
						break;
					case 'asice':
						$fileType='BDOC';
						break;
					default:
						throw new Exception('Unknown file extension - must be either .bdoc or .cdoc or .ddoc');
				}
			}

			// Init
			$className='BASE_DIGIDOC_'.$fileType;
			$DOC=new $className();
			//$DOC->type=$fileType;

			// Parse file
			$DOC->parse($filename, ($openFileAsObject ? 'object' : 'array'));

            //throw new Exception(var_dump($DOC));
			// Return instance
			return $DOC;
		}


		/**
		 *   Parse file
		 *   @access public
		 *   @var string $filename
		 *   @throws Exception
		 *   @return void
		 */

		public function parse( $filename )
		{
			// This should be implemented in the subclass.
		}


		/**
		 *   Close/cleanup
		 *   @access public
		 *   @var string $filename
		 *   @throws Exception
		 *   @return void
		 */

		public function close()
		{
			// This should be implemented in the subclass.
		}


        /**
         *
         *   The constructor
         *   ---------------
         *   @access public
         *   @return DigiDocContainerInterface
         *
         */

        public function __construct()
        {
        }


        /**
         *
         *   Create a new container
         *   ----------------------
         *   @access public
         *   @static
         *   @throws Exception
         *   @return BASE_DIGIDOC_BDOC
         *
         */

        public static function create()
        {
            // This needs to be implemented in the implementation class.
            throw new Exception('Function createContainer() not implemented.');
        }


        /**
         *
         *   Add a file to container
         *   -----------------------
         *   @access public
         *   @param string $sourceFile Source file
         *   @param string $fileName File name (if not the same as source file)
         *   @throws Exception
         *   @return void
         *
         */

        public function addFile( $sourceFile, $fileName=null )
        {
            // This needs to be implemented in the implementation class.
            throw new Exception('Function addFile() not implemented.');
        }


        /**
         *
         *   Add a file to container from string
         *   -----------------------------------
         *   @access public
         *   @param string $fileContent File content
         *   @param string $fileName File name
         *   @throws Exception
         *   @return void
         *
         */

        public function addFileFromString( $fileContent, $fileName )
        {
            // This needs to be implemented in the implementation class.
            throw new Exception('Function addFileFromString() not implemented.');
        }


        /**
         *
         *   Get DigiDoc container file
         *   --------------------------
         *   @access public
         *   @param bool $getHashcoded Get hashcoded version?
         *   @throws Exception
         *   @return string
         *
         */

        public function getDigiDoc( $getHashcoded=false )
        {
            // This needs to be implemented in the implementation class.
            throw new Exception('Function getDigiDoc() not implemented.');
        }

	}


?>