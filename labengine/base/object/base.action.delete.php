<?


	/**
	 *
	 *   LabEngine™ 7
	 *   Standard actions: delete item
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_ACTION_DELETE extends BASE_AJAXACTION
	{


		/**
		 *  Parameters
		 *  @var string
		 *  @access public
		 */
		public $param = array();


		/**
		 *  Set redirect URL
		 *  @access public
		 *  @param string title
		 *  @return void
		 */

		public function setRedirectURL ( $redirectURL )
		{
			$this->param['url_redirect']=$redirectURL;
		}


		/**
		 *  Set reload
		 *  @access public
		 *  @param boolean $reload Reload on success
		 *  @return void
		 */

		public function setReload ( $reload )
		{
			$this->param['reload']=$reload;
		}


		/**
		 *  Set success action
		 *  @access public
		 *  @param string $successAction Success action
		 *  @return void
		 */

		public function setSuccessAction ( $successAction )
		{
			$this->param['successaction']=$successAction;
		}


		/**
		 *   Init delete action
		 *   @access public
		 *   @return void
		 *   @throws Exception
		 */

		public function initDelete()
		{
			// This can be implemented in the subclass.
		}


		/**
		 *  Swap
		 *  @access public
		 *  @return array
		 */

		public function handleAction()
		{
			$response=array('status'=>'2');
			try
			{
				// Init self
				$this->initDelete();

				// Check
				if (empty($_POST[$this->data->_param['idfield']])) throw new Exception('[BASE.COMMON.ErrorCheckingData]');

				// Load
				$this->data->load($_POST[$this->data->_param['idfield']]);

				// Delete
				$logMessage=(isset($this->param['logmessage'])?$this->param['logmessage']:NULL);
				$logData=NULL;
				$refOID=(isset($this->param['logrefoid'])?(preg_match("/^[0-9]+$/",$this->param['logrefoid'])?$this->param['logrefoid']:$this->data->_in[$this->param['logrefoid']]):NULL);
				$this->data->delete($logMessage,$logData,$refOID);

				// Success
				$response['status']='1';
				if (!empty($this->param['url_redirect']))
				{
					$response['redirect']=$this->param['url_redirect'];
				}
				elseif ($this->param['reload']!==false)
				{
					$response['reload']='1';
				}
				if (!empty($this->param['successaction']))
				{
					$response['submitsuccessaction']=$this->param['successaction'];
				}
				return $response;
			}
			catch (Exception $e)
			{
				$response['error']=$e->getMessage();
				return $response;
			}
		}


	}


?>