<?php


/**
 *
 *   LabEngine™ 7
 *   BankLink integration: COOP
 *
 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
 *
 */


class BASE_BANKLINK_COOP extends BASE_BANKLINK_IPIZZA
{


    /**
     *   Bank ID
     *   @var string
     *   @static
     */

    public static $BANK_id = 'coop';


    /**
     *   Bank destination URL
     *   @var string
     */

    public $BANK_destination_url = 'https://i.cooppank.ee/pay';


}


?>