<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   Field class: display field
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_FIELD_DISPLAY extends BASE_FIELD
	{


		/**
		 *  Display the field
		 *  @access public
		 *  @return string contents
		 */

		public function displayField( $value, $row=NULL )
		{
			// Render
			$c='<div class="'.$this->getContentWidthClass().'">';
			$c.='<p class="form-control-static '.$this->param['fieldclass'].'" style="'.$this->param['fieldstyle'].'">';

				$c.='<input type="hidden"';
				$c.=' id="'.$this->tag.'"';
				$c.=' name="'.$this->tag.'"';
				$c.=' value="'.htmlspecialchars($value).'"';
				$c.='>';

				$c.=$value;

			$c.='</p>';
			$c.='</div>';

			// Return
			return $c;
		}


	}


?>