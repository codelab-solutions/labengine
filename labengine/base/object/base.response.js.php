<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   Response bundle: JS
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_RESPONSE_JS
	{


		/**
		 *   Response JavaScripts
		 *   @var string
		 *   @access public
		 */

		public $responseJS = array();


		/**
		 *   Response external JavaScripts
		 *   @var string
		 *   @access public
		 */

		public $responseExternalJS = array();


		/**
		 *   Response inline JavaScripts
		 *   @var string
		 *   @access public
		 */

		public $responseInlineJS = array();


		/**
		 *   Response locale
		 *   @var string
		 *   @access public
		 */

		public $responseLocale = FALSE;


		/**
		 *   Response locale is simple/short?
		 *   @var string
		 *   @access public
		 */

		public $responseLocaleShort = FALSE;


		/**
		 *   Add a JS file
		 *   @access public
		 *   @param string $jsFilename Filename
		 *   @param string $jsID ID
		 *   @param int $jsPriority Priority (1-9)
		 *   @param array $jsExtraParam Extra parameters
		 *   @return void
		 */

		public function addJS ( $jsFilename, $jsID='', $jsPriority=5, $jsExtraParam=NULL )
		{
			if (!strlen($jsID)) $jsID=strval(sizeof($this->responseJS)+1);
			if ($jsPriority===TRUE) $jsPriority=-99;
			$this->responseJS[$jsID]=array(
				'filename' => $jsFilename,
				'priority' => $jsPriority,
				'extraparam' => $jsExtraParam
			);
		}


		/**
		 *   Add an external JS source
		 *   @access public
		 *   @param string $jsURL URL
		 *   @param string $jsID ID
		 *   @param int $jsPriority Priority (1-9)
		 *   @param array $jsExtraParam Extra parameters
		 *   @return void
		 */

		public function addExternalJS ( $jsURL, $jsID='', $jsPriority=5 )
		{
			if (!strlen($jsID)) $jsID=strval(sizeof($this->responseExternalJS)+1);
			if ($jsPriority===TRUE) $jsPriority=-99;
			$this->responseExternalJS[$jsID]=array(
				'url' => $jsURL,
				'priority' => $jsPriority
			);
		}


		/**
		 *   Add an inline JS
		 *   @access public
		 *   @param string $jsURL URL
		 *   @param string $jsID ID
		 *   @param int $jsPriority Priority (1-9)
		 *   @param array $jsExtraParam Extra parameters
		 *   @return void
		 */

		public function addInlineJS ( $jsSource, $jsID='', $jsPriority=5 )
		{
			if (!strlen($jsID)) $jsID=strval(sizeof($this->responseInlineJS)+1);
			if ($jsPriority===TRUE) $jsPriority=-99;
			$this->responseInlineJS[$jsID]=array(
				'src' => $jsSource,
				'priority' => $jsPriority
			);
		}


		/**
		 *   Output locale?
		 *   @access public
		 *   @param boolean Output locale, too?
		 *   @return void
		 */

		public function addLocale( $lang, $shortLocale=false )
		{
			$this->responseLocale=$lang;
			$this->responseLocaleShort=$shortLocale;
		}


		/**
		 *   Minify JS
		 *   @access public
		 *   @return string
		 *   @throws Exception
		 */

		public function minifyJS( $JS )
		{
			global $LAB;
			try
			{
				// Init less.php
				include_once 'base/contrib/JShrink/Minifier.php';

				// Do the magic
				$JS=\JShrink\Minifier::minify($JS,array(
					'flaggedComments' => false
				));

				// Return
				return $JS;
			}
			catch (Exception $e)
			{
				throw new Exception('Error minifying JS: '.$e->getMessage());
			}
		}


		/**
		 *   Output JS
		 *   @access public
		 *   @return string
		 *   @throws Exception
		 */

		public function outputJS()
		{
			global $LAB;

			// Standard JS components
			$this->addJS('base/static/jquery/jquery.min.js','jquery','1');
			$this->addJS('base/static/bootstrap/js/bootstrap.min.js','bootstrap','2');
			$this->addJS('base/static/summernote/summernote.js','summernote','9');
			$this->addJS('base/static/summernote/summernote-image-attributes.js','summernote-image-attributes','10');
			$this->addJS('base/static/summernote/summernote-image-shapes.js','summernote-image-shapes','11');

			// Build asset array
			$assetArray=array();
			$assetTimeStamp=0;
			$this->responseJS=sortdataset($this->responseJS,'priority');
			foreach ($this->responseJS as $jsID => $jsProperties)
			{
				if (!stream_resolve_include_path($jsProperties['filename'])) throw new Exception('JS file not readable: '.$jsProperties['filename']);
				$assetArray[$jsID]=$jsProperties['filename'];
				$filemtime=filemtime(stream_resolve_include_path($jsProperties['filename']));
				if ($filemtime>$assetTimeStamp) $assetTimeStamp=$filemtime;
			}
			$assetHash=md5(join('|',array_keys($assetArray)));
			$assetFileName=BASE::getTmpDir().'/'.$LAB->CONFIG->get('site.id').'.asset.js.'.$assetHash.'.'.intval($assetTimeStamp).'.js';

			// If file does not exist, build it
			if (!file_exists($assetFileName))
			{
				$assetFileContents='';
				foreach ($this->responseJS as $jsID => $jsProperties)
				{
					$JS=file_get_contents(stream_resolve_include_path($jsProperties['filename']));
					if (!$LAB->DEBUG->devEnvironment && strpos($jsProperties['filename'],'.min.')===false)
					{
						$JS=$this->minifyJS($JS);
					}
					$assetFileContents.=$JS;
				}
				file_put_contents($assetFileName,$assetFileContents);
			}

			// Return link
			$c='<script src="/base/js/bundle/'.$assetHash.'.'.intval($assetTimeStamp).'.js"></script>';
			if (sizeof($this->responseExternalJS))
			{
				$this->responseExternalJS=sortdataset($this->responseExternalJS,'priority');
				foreach ($this->responseExternalJS as $jsID => $jsProperties)
				{
					$c.='<script src="'.$jsProperties['url'].'"></script>';
				}
			}
			if (sizeof($this->responseInlineJS))
			{
				$this->responseInlineJS=sortdataset($this->responseInlineJS,'priority');
				foreach ($this->responseInlineJS as $jsID => $jsProperties)
				{
					$c.='<script> '.$jsProperties['src'].' </script>';
				}
			}
			if ($this->responseLocale)
			{
				if ($this->responseLocaleShort)
				{
					$c.='<script src="/base/js/slocale/'.$this->responseLocale.'/locale.js"></script>';
				}
				else
				{
					$c.='<script src="/base/js/locale/'.$this->responseLocale.'/locale.js"></script>';
				}
			}
			return $c;
		}


	}


?>