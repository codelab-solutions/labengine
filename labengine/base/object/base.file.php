<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   The file handling class
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_FILE
	{


		/**
		 *   Do we store files in DB?
		 *   @return boolean
		 */

		public static function dbStorage()
		{
			global $LAB;
			return (mb_strlen($LAB->CONFIG->get('site.filepath'))?FALSE:TRUE);
		}


		/**
		 *   Do we store files in file system?
		 *   @return boolean
		 */

		public static function fileStorage()
		{
			global $LAB;
			return (mb_strlen($LAB->CONFIG->get('site.filepath'))?TRUE:FALSE);
		}


		/**
		 *   Get file data directory, make necessary paths in the process
		 *   @param string|boolean $OID Object OID
		 *   @return string directory
		 *   @throws Exception
		 */

		public static function getFileDataDir( $OID=FALSE )
		{
			global $LAB;

			// Check
			if (!mb_strlen($LAB->CONFIG->get('site.filepath'))) throw new Exception('File storage not enabled.');

			// Init
			umask(0);
			$fileDataDir=$LAB->CONFIG->get('site.filepath');
			if (!file_exists($fileDataDir)) throw new Exception('Filedatapath ('.$fileDataDir.') does not exist.');
			if (!is_writable($fileDataDir)) throw new Exception('Filedatapath ('.$fileDataDir.') not writable by web server.');

			// File dir
			if (!empty($OID) && is_numeric($OID))
			{
				$fileDataDir.='/'.sprintf("%03d",mb_substr($OID,-3));
				if (!file_exists($fileDataDir))
				{
					mkdir($fileDataDir,0755);
				}
			}

			// Return
			return $fileDataDir;
		}


		/**
		 *   Get file data directory, make necessary paths in the process
		 *   @param string|boolean $OID Object OID
		 *   @return string directory
		 *   @throws Exception
		 */

		public static function getFileCacheDir( $OID=FALSE )
		{
			global $LAB;

			// Check
			if (!mb_strlen($LAB->CONFIG->get('site.cachepath'))) throw new Exception('Cache path not defined.');

			// Init
			umask(0);
			$fileDataDir=$LAB->CONFIG->get('site.cachepath');
			if (!file_exists($fileDataDir)) throw new Exception('Cache path ('.$fileDataDir.') does not exist.');
			if (!is_writable($fileDataDir)) throw new Exception('Cache path ('.$fileDataDir.') not writable by web server.');

			// File dir
			if (!empty($OID) && is_numeric($OID))
			{
				$fileDataDir.='/'.sprintf("%03d",mb_substr($OID,-3));
				if (!file_exists($fileDataDir))
				{
					mkdir($fileDataDir,0755);
				}
			}

			// Return
			return $fileDataDir;
		}


		/**
		 *   Put file into filedata
		 *   @param string $OID Object OID
		 *   @param string $type File type
		 *   @param string $fileContents File contents
		 *   @param string|boolean $subType Subtype
		 *   @return void
		 */

		public static function putFile( $OID, $type, $fileContents, $subType=FALSE )
		{
			umask(0);

			// If file exists, delete old one
			if ($subType==false && file_exists(BASE_FILE::getFile($OID,$type)))
			{
				BASE_FILE::deleteFile($OID,$type);
			}

			// Put file
			file_put_contents(BASE_FILE::getFileDataDir($OID).'/'.$OID.'.'.$type.(!empty($subType)?'.'.$subType:''),$fileContents);
			chmod(BASE_FILE::getFileDataDir($OID).'/'.$OID.'.'.$type,0644);
		}


		/**
		 *   Move uploaded file into filedata
		 *   @param string $fileName Temp uploaded file name
		 *   @param string $OID Object OID
		 *   @param string $type File type
		 *   @return string directory
		 */

		public static function moveUploadedFile( $fileName, $OID, $type )
		{
			umask(0);

			// If file exists, delete old one
			if (file_exists(BASE_FILE::getFile($OID,$type)))
			{
				BASE_FILE::deleteFile($OID,$type);
			}

			// Move file
			move_uploaded_file($fileName,BASE_FILE::getFileDataDir($OID).'/'.$OID.'.'.$type);
			chmod(BASE_FILE::getFileDataDir($OID).'/'.$OID.'.'.$type,0644);
		}


		/**
		 *   Get file contents
		 *   @param string $OID Object OID
		 *   @param string $type File type
		 *   @param string|boolean $subType Subtype
		 *   @return string directory
		 */

		public static function getFile( $OID, $type, $subType=FALSE )
		{
			return file_get_contents(BASE_FILE::getFileDataDir($OID).'/'.$OID.'.'.$type.(!empty($subType)?'.'.$subType:''));
		}


		/**
		 *   Get file full path
		 *   @param string $OID Object OID
		 *   @param string $type File type
		 *   @param string|boolean $subType Subtype
		 *   @return string Full path to file
		 */

		public static function getFileName( $OID, $type, $subType=FALSE )
		{
			return BASE_FILE::getFileDataDir($OID).'/'.$OID.'.'.$type.(!empty($subType)?'.'.$subType:'');
		}


		/**
		 *   Get file cache path
		 *   @param string $OID Object OID
		 *   @param string $type File type
		 *   @param string|boolean $subType Subtype
		 *   @return string Full path to file
		 */

		public static function getCacheFileName( $OID, $type, $subType=FALSE )
		{
			return BASE_FILE::getFileCacheDir($OID).'/'.$OID.'.'.$type.(!empty($subType)?'.'.$subType:'');
		}


		/**
		 *   Delete file
		 *   @param string $OID Object OID
		 *   @param string $type File type
		 *   @param string|boolean $subType Subtype
		 *   @return void
		 */

		public static function deleteFile( $OID, $type, $subType=FALSE )
		{
			if (!empty($subType))
			{
				unlink(BASE_FILE::getFileDataDir($OID).'/'.$OID.'.'.$type.'.'.$subType);
			}
			else
			{
				$fileList=glob(BASE_FILE::getFileName($OID,$type).'*');
				foreach ($fileList as $file)
				{
					unlink($file);
				}
			}
		}


		/**
		 *  Return nice displayable file size
		 *  @static
		 *  @access public
		 *  @param int file size in bytes
		 *  @return string displayable size
		 */

		public static function niceFileSize ( $fsize )
		{
			if ($fsize >= 1024*1024*1000)
				$s=sprintf("%.2f", ($fsize / (1024*1024*1024) ))." GB";
			elseif ($fsize >= 1024*1000)
				$s=sprintf("%.1f", ($fsize / (1024*1024) ))." MB";
			elseif ($fsize >= 1000)
				$s=round($fsize/1024)." KB";
			else
				$s=$fsize." bytes";
			return $s;
		}


		/**
		 *   Detect mime type
		 *   @access public
		 *   @static
		 *   @param string $filename Filename
		 *   @return string
		 */

		public static function getMimeType( $filename )
		{
			$finfo = finfo_open(FILEINFO_MIME_TYPE);
			$mimeType=finfo_file($finfo,$filename);
			finfo_close($finfo);
			return $mimeType;
		}


	}


?>