<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   Field class: address
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_FIELD_ADDRESS extends BASE_FIELD
	{


		/**
		 *  States
		 */

		public static $SET_us_state=array(
			'AL' => 'Alabama',
			'AK' => 'Alaska',
			'AZ' => 'Arizona',
			'AR' => 'Arkansas',
			'CA' => 'California',
			'CO' => 'Colorado',
			'CT' => 'Connecticut',
			'DE' => 'Delaware',
			'FL' => 'Florida',
			'GA' => 'Georgia',
			'HI' => 'Hawaii',
			'ID' => 'Idaho',
			'IL' => 'Illinois',
			'IN' => 'Indiana',
			'IA' => 'Iowa',
			'KS' => 'Kansas',
			'KY' => 'Kentucky',
			'LA' => 'Louisiana',
			'ME' => 'Maine',
			'MD' => 'Maryland',
			'MA' => 'Massachusetts',
			'MI' => 'Michigan',
			'MN' => 'Minnesota',
			'MS' => 'Mississippi',
			'MO' => 'Missouri',
			'MT' => 'Montana',
			'NE' => 'Nebraska',
			'NV' => 'Nevada',
			'NH' => 'New Hampshire',
			'NJ' => 'New Jersey',
			'NM' => 'New Mexico',
			'NY' => 'New York',
			'NC' => 'North Carolina',
			'ND' => 'North Dakota',
			'OH' => 'Ohio',
			'OK' => 'Oklahoma',
			'OR' => 'Oregon',
			'PA' => 'Pennsylvania',
			'RI' => 'Rhode Island',
			'SC' => 'South Carolina',
			'SD' => 'South Dakota',
			'TN' => 'Tennessee',
			'TX' => 'Texas',
			'UT' => 'Utah',
			'VT' => 'Vermont',
			'VA' => 'Virginia',
			'WA' => 'Washington',
			'WV' => 'West Virginia',
			'WI' => 'Wisconsin',
			'WY' => 'Wyoming',
		);


		/**
		 *   Set country
		 *   @access public
		 *   @param string $country Country
		 *   @return void
		 */

		public function setCountry ( $country )
		{
			$this->param['country']=$country;
		}


		/**
		 *   Enable state field
		 *   @access public
		 *   @param string $state State
		 *   @return void
		 */

		public function setState ( $state )
		{
			$this->param['state']=$state;
		}


		/**
		 *   Set default city
		 *   @access public
		 *   @param string $defaultCity Default city
		 *   @return void
		 */

		public function setDefaultCity ( $defaultCity )
		{
			$this->param['default_city']=$defaultCity;
		}


		/**
		 *   Set default ZIP
		 *   @access public
		 *   @param string $defaultZIP Default ZIP
		 *   @return void
		 */

		public function setDefaultZIP ( $defaultZIP )
		{
			$this->param['default_zip']=$defaultZIP;
		}


		/**
		 *   Set default country
		 *   @access public
		 *   @param string $defaultCountry Default country
		 *   @return void
		 */

		public function setDefaultCountry ( $defaultCountry )
		{
			$this->param['default_country']=$defaultCountry;
		}


		/**
		 *   Save value
		 *   @access public
		 *   @return mixed value
		 */

		public function saveValue()
		{
			// Init
			$retval=array();

			// Väärtused
			$retval[$this->tag]=$_POST[$this->tag];
			$retval[$this->tag.'_city']=$_POST[$this->tag.'_city'];
			if (!empty($this->param['state'])) $retval[$this->tag.'_state']=$_POST[$this->tag.'_state'];
			$retval[$this->tag.'_zip']=$_POST[$this->tag.'_zip'];
			$retval[$this->tag.'_country']=$_POST[$this->tag.'_country'];

			// Return
			return $retval;
		}


		/**
		 *   List value
		 *   @access public
		 *   @param string $value Column value
		 *   @param array $row Row pointer
		 *   @return mixed value
		 */

		public function listValue( $value, &$row )
		{
			global $LAB;

			if (!mb_strlen($value)) return '';
			$baseField=str_replace('->','_',$this->tag);

			$value=htmlspecialchars($value);
			if (!empty($row[$baseField.'_city']) || !empty($row[$baseField.'_state']) || !empty($row[$baseField.'_zip']))
			{
				$value.="\n".htmlspecialchars(trim($row[$baseField.'_zip'].' '.$row[$baseField.'_city']));
			}
			if (!empty($row[$baseField.'_country']))
			{
				if (!mb_strlen($LAB->CONFIG->get('locale.country')) || $row[$baseField.'_country']!=$LAB->CONFIG->get('locale.country'))
				{
					$value.="\n".htmlspecialchars(BASE_COUNTRYDATA::getName($row[$baseField.'_country']));
				}
			}
			return parent::listValue($value,$row);
		}


		/**
		 *   Display value
		 *   @access public
		 *   @return mixed value
		 */

		public function displayValue()
		{
			global $LAB;
			
			if (!mb_strlen($this->getValue())) return '';

			$value=$this->getValue();
			if (!empty($this->formObject->data->{$this->tag.'_city'}) || !empty($this->formObject->data->{$this->tag.'_state'}) || !empty($this->formObject->data->{$this->tag.'_zip'}))
			{
				$value.="\n".htmlspecialchars(trim($this->formObject->data->{$this->tag.'_zip'}.' '.$this->formObject->data->{$this->tag.'_city'}));
			}
			if (!empty($this->formObject->data->{$this->tag.'_country'}))
			{
				if (!mb_strlen($LAB->CONFIG->get('locale.country')) || $this->formObject->data->{$this->tag.'_country'}!=$LAB->CONFIG->get('locale.country'))
				{
					$value.="\n".htmlspecialchars(BASE_COUNTRYDATA::getName($this->formObject->data->{$this->tag.'_country'}));
				}
			}
			return nl2br($value);
		}


		/**
		 *  Display the field
		 *  @access public
		 *  @return string contents
		 */

		public function displayField( $value, $row=NULL )
		{
			global $LAB;

			// Values
			if (is_object($this->formObject) && $this->formObject->doSubmit)
			{
				$addressValue=$_POST[$this->tag];
				$addressCityValue=$_POST[$this->tag.'_city'];
				if (!empty($this->param['state'])) $addressStateValue=$_POST[$this->tag.'_state'];
				$addressZipValue=$_POST[$this->tag.'_zip'];
				$addressCountryValue=$_POST[$this->tag.'_country'];
			}
			elseif (is_object($this->formObject) && $this->formObject->operation=='edit')
			{
				$addressValue=$value;
				$addressCityValue=$this->formObject->data->_in[$this->tag.'_city'];
				if (!empty($this->param['state'])) $addressStateValue=$this->formObject->data->_in[$this->tag.'_state'];
				$addressZipValue=$this->formObject->data->_in[$this->tag.'_zip'];
				$addressCountryValue=$this->formObject->data->_in[$this->tag.'_country'];
			}
			else
			{
				$addressValue=oneof($this->param['default'],'');
				$addressCityValue=oneof($this->param['default_city'],'');
				if (!empty($this->param['state'])) $addressStateValue=oneof($this->param['default_state'],'');
				$addressZipValue=oneof($this->param['default_zip'],'');
				$addressCountryValue=oneof($this->param['default_country'],$this->param['country'],'');
			}

			// Style
			$style=array();
			if (!empty($this->param['fieldstyle'])) $style[]=$this->param['fieldstyle'];
			if (!empty($this->param['comment'])) $style[]='width: 70%; display: inline-block';

			// Class
			$class=array();
			$class[]='form-control';
			if (!empty($this->param['fieldclass'])) $class[]=$this->param['fieldclass'];

			// Render
			$c='<div class="'.$this->getContentWidthClass().'">';

			// Address
			$c.='<div style="display: block">';
			$value=htmlspecialchars($addressValue);
			$value=str_replace('{','&#123;',$value);
			$value=str_replace('}','&#125;',$value);
			$value=str_replace('[','&#091;',$value);
			$value=str_replace(']','&#093;',$value);
			$c.='<input type="text"';
			$c.=' id="'.$this->tag.'"';
			$c.=' name="'.$this->tag.'"';
			$c.=' value="'.$value.'"';
			$c.=' placeholder="[BASE.COMMON.Address.Street]"';
			$c.=' data-originalvalue="'.$value.'"';
			$c.=' autocomplete="off"';
			$c.=' class="'.join(' ',$class).'"';
			if (!empty($style)) $c.=' style="'.join('; ',$style).'"';
			if (!empty($this->param['maxlength'])) $c.=' maxlength="'.intval($this->param['maxlength']).'"';
			if (!empty($this->param['readonly'])) $c.=' readonly="readonly"';
			if (!empty($this->param['disabled'])) $c.=' disabled="disabled"';
			if (!empty($this->param['emptyformat'])) $c.=' data-emptyformat="'.htmlspecialchars($this->param['emptyformat']).'"';
			if (!empty($this->param['keephiddenvalue'])) $c.=' data-keephiddenvalue="1"';
			if (isset($this->param['event']) && is_array($this->param['event']))
			{
				foreach ($this->param['event'] as $eventType => $eventContent) $c.=' '.$eventType.'="'.$eventContent.'"';
			}
			if (isset($this->param['data']) && is_array($this->param['data']))
			{
				foreach ($this->param['data'] as $dataKey => $dataValue) $c.=' data-'.$dataKey.'="'.htmlspecialchars($dataValue).'"';
			}
			$c.='>';
			$c.='</div>';

			// Second line
			$c.='<div style="display: block; margin-top: 6px">';

			// Local stuff: US
			if ($this->param['country']=='US' && !empty($this->param['state']))
			{
				// State field
				$value=$addressStateValue;
				$this->param['zipevent']['onchange']="ADDRESS.addressHelper('".$this->tag."','zip');";
				$stateField='<select';
				$stateField.=' id="'.$this->tag.'_state"';
				$stateField.=' name="'.$this->tag.'_state"';
				$stateField.=' class="'.join(' ',$class).'"';
				$stateField.=' data-originalvalue="'.htmlspecialchars($value).'"';
				$stateField.=' style="'.join('; ',$style).'"';
				if (!empty($this->param['disabled'])) $stateField.=' disabled="disabled"';
				if (!empty($this->param['keephiddenvalue'])) $stateField.=' data-keephiddenvalue="1"';
				if (isset($this->param['event']) && is_array($this->param['event']))
				{
					foreach ($this->param['event'] as $eventType => $eventContent) $stateField.=' '.$eventType.'="'.$eventContent.'"';
				}
				if (isset($this->param['stateevent']) && is_array($this->param['stateevent']))
				{
					foreach ($this->param['stateevent'] as $eventType => $eventContent) $stateField.=' '.$eventType.'="'.$eventContent.'"';
				}
				$stateField.='>';
				$stateField.='<option value="">---</option>';
				foreach(static::$SET_us_state as $state => $stateName)
				{
					$stateField.='<option value="'.htmlspecialchars($state).'"'.($state==$value?' selected="selected"':'').'>'.htmlspecialchars($state).'</option>';
				}
				$stateField.='</select>';

				// Country
				BASE_COUNTRYDATA::$SET_country['US']='USA';
			}

			// City
			$value=htmlspecialchars($addressCityValue);
			$value=str_replace('{','&#123;',$value);
			$value=str_replace('}','&#125;',$value);
			$value=str_replace('[','&#091;',$value);
			$value=str_replace(']','&#093;',$value);
			$cityField='<input type="text"';
			$cityField.=' id="'.$this->tag.'_city"';
			$cityField.=' name="'.$this->tag.'_city"';
			$cityField.=' value="'.$value.'"';
			$cityField.=' placeholder="[BASE.COMMON.Address.City]"';
			$cityField.=' data-originalvalue="'.$value.'"';
			$cityField.=' autocomplete="off"';
			$cityField.=' class="'.join(' ',$class).'"';
			if (!empty($style)) $cityField.=' style="'.join('; ',$style).'"';
			if (!empty($this->param['maxlength'])) $cityField.=' maxlength="'.intval($this->param['maxlength']).'"';
			if (!empty($this->param['readonly'])) $cityField.=' readonly="readonly"';
			if (!empty($this->param['disabled'])) $cityField.=' disabled="disabled"';
			if (!empty($this->param['emptyformat'])) $cityField.=' data-emptyformat="'.htmlspecialchars($this->param['emptyformat']).'"';
			if (!empty($this->param['keephiddenvalue'])) $cityField.=' data-keephiddenvalue="1"';
			if (isset($this->param['event']) && is_array($this->param['event']))
			{
				foreach ($this->param['event'] as $eventType => $eventContent) $cityField.=' '.$eventType.'="'.$eventContent.'"';
			}
			if (isset($this->param['cityevent']) && is_array($this->param['cityevent']))
			{
				foreach ($this->param['cityevent'] as $eventType => $eventContent) $cityField.=' '.$eventType.'="'.$eventContent.'"';
			}
			$cityField.='>';

			// ZIP
			$value=htmlspecialchars($addressZipValue);
			$value=str_replace('{','&#123;',$value);
			$value=str_replace('}','&#125;',$value);
			$value=str_replace('[','&#091;',$value);
			$value=str_replace(']','&#093;',$value);
			$zipField='<input type="text"';
			$zipField.=' id="'.$this->tag.'_zip"';
			$zipField.=' name="'.$this->tag.'_zip"';
			$zipField.=' value="'.$value.'"';
			$zipField.=' placeholder="[BASE.COMMON.Address.ZIP]"';
			$zipField.=' data-originalvalue="'.$value.'"';
			$zipField.=' autocomplete="off"';
			$zipField.=' class="'.join(' ',$class).'"';
			$zipField.=' style="'.join('; ',$style).'"';
			if (!empty($this->param['maxlength'])) $zipField.=' maxlength="'.intval($this->param['maxlength']).'"';
			if (!empty($this->param['readonly'])) $zipField.=' readonly="readonly"';
			if (!empty($this->param['disabled'])) $zipField.=' disabled="disabled"';
			if (!empty($this->param['emptyformat'])) $zipField.=' data-emptyformat="'.htmlspecialchars($this->param['emptyformat']).'"';
			if (!empty($this->param['keephiddenvalue'])) $zipField.=' data-keephiddenvalue="1"';
			if (isset($this->param['event']) && is_array($this->param['event']))
			{
				foreach ($this->param['event'] as $eventType => $eventContent) $c.=' '.$eventType.'="'.$eventContent.'"';
			}
			if (isset($this->param['zipevent']) && is_array($this->param['zipevent']))
			{
				foreach ($this->param['zipevent'] as $eventType => $eventContent) $zipField.=' '.$eventType.'="'.$eventContent.'"';
			}
			$zipField.='>';

			// Country field
			$value=$addressCountryValue;
			$countryField='<select';
			$countryField.=' id="'.$this->tag.'_country"';
			$countryField.=' name="'.$this->tag.'_country"';
			$countryField.=' class="'.join(' ',$class).'"';
			$countryField.=' data-originalvalue="'.htmlspecialchars($value).'"';
			$countryField.=' style="'.join('; ',$style).'"';
			if (!empty($this->param['disabled'])) $countryField.=' disabled="disabled"';
			if (!empty($this->param['keephiddenvalue'])) $countryField.=' data-keephiddenvalue="1"';
			if (isset($this->param['event']) && is_array($this->param['event']))
			{
				foreach ($this->param['event'] as $eventType => $eventContent) $countryField.=' '.$eventType.'="'.$eventContent.'"';
			}
			if (isset($this->param['countryevent']) && is_array($this->param['countryevent']))
			{
				foreach ($this->param['countryevent'] as $eventType => $eventContent) $countryField.=' '.$eventType.'="'.$eventContent.'"';
			}
			$countryField.='>';
			$countryField.='<option value="">---</option>';
			foreach(BASE_COUNTRYDATA::getCountryList() as $cCode => $cName)
			{
				$countryField.='<option value="'.htmlspecialchars($cCode).'"'.($cCode==$value?' selected="selected"':'').'>'.htmlspecialchars($cName).'</option>';
			}
			$countryField.='</select>';

			// Display field
			switch ($this->param['country'])
			{
				// US
				case 'US':
					$c.='<div style="display: inline-block; float: left; width: 15%">'.$zipField.'</div>';
					$c.='<div style="display: inline-block; float: left; width: 38%; margin-left: 10px">'.$cityField.'</div>';
					if (!empty($this->param['state'])) $c.='<div style="display: inline-block; float: left; width: 16%; margin-left: 10px">'.$stateField.'</div>';
					$c.='<div style="display: inline-block; float: left; width: 22%; margin-left: 10px">'.$countryField.'</div>';
					break;

				// Default
				default:
					$c.='<div style="display: inline-block; float: left; width: 48%">'.$cityField.'</div>';
					$c.='<div style="display: inline-block; float: left; width: 20%; margin-left: 10px">'.$zipField.'</div>';
					$c.='<div style="display: inline-block; float: left; width: 25%; margin-left: 10px">'.$countryField.'</div>';
					break;
			}


			$c.='</div>';

			// Comment
			$c.=$this->displayComment();

			$c.='</div>';

			// Return
			return $c;
		}


	}


?>