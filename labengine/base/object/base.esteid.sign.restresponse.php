<?php


	/**
	 *
	 *   FlaskPHP-Identity-EstEID
	 *   ------------------------
	 *   Digital signing provider for ID card
	 *
	 *   @author   Codelab Solutions OÜ <codelab@codelab.ee>
	 *   @license  https://www.flaskphp.com/LICENSE MIT
	 *
	 */


	class BASE_ESTEID_SIGN_RESTRESPONSE
	{

		/**
		 * @var string
		 */
		public $filename;
		/**
		 * @var string
		 */
		public $bdocfilename;
		/**
		 * @var string
		 */
		public $dataToSignFile;
		/**
		 * @var string
		 */
		public $digestHashFile;
		/**
		 * @var string
		 */
		public $signerCertificateFile;

		/**
		 * @return string
		 */
		public function getFilename(): string
		{
			return $this->filename;
		}

		/**
		 * @param string $filename
		 * @return SessionStartResponse
		 */
		public function setFilename(string $filename): BASE_ESTEID_SIGN_RESTRESPONSE
		{
			$this->filename = $filename;
			return $this;
		}

		/**
		 * @return string
		 */
		public function getBdocfilename(): string
		{
			return $this->bdocfilename;
		}

		/**
		 * @param string $bdocfilename
		 * @return SessionStartResponse
		 */
		public function setBdocfilename(string $bdocfilename): BASE_ESTEID_SIGN_RESTRESPONSE
		{
			$this->bdocfilename = $bdocfilename;
			return $this;
		}

		/**
		 * @return string
		 */
		public function getDataToSignFile(): string
		{
			return $this->dataToSignFile;
		}

		/**
		 * @param string $dataToSignFile
		 * @return SessionStartResponse
		 */
		public function setDataToSignFile(string $dataToSignFile): BASE_ESTEID_SIGN_RESTRESPONSE
		{
			$this->dataToSignFile = $dataToSignFile;
			return $this;
		}

		/**
		 * @return string
		 */
		public function getDigestHashFile(): string
		{
			return $this->digestHashFile;
		}

		/**
		 * @param string $digestHashFile
		 * @return SessionStartResponse
		 */
		public function setDigestHashFile(string $digestHashFile): BASE_ESTEID_SIGN_RESTRESPONSE
		{
			$this->digestHashFile = $digestHashFile;
			return $this;
		}

		/**
		 * @return string
		 */
		public function getSignerCertificateFile(): string
		{
			return $this->signerCertificateFile;
		}

		/**
		 * @param string $signerCertificateFile
		 * @return SessionStartResponse
		 */
		public function setSignerCertificateFile(string $signerCertificateFile): BASE_ESTEID_SIGN_RESTRESPONSE
		{
			$this->signerCertificateFile = $signerCertificateFile;
			return $this;
		}


	}

?>