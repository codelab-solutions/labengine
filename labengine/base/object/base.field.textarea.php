<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   Field class: text area
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_FIELD_TEXTAREA extends BASE_FIELD
	{


		/**
		 *  Set wrap
		 *  @access public
		 *  @param string wrap
		 *  @return void
		 */

		public function setWrap ( $wrap )
		{
			$this->param['wrap']=$wrap;
		}


		/**
		 *  Set rows
		 *  @access public
		 *  @param string rows
		 *  @return void
		 */

		public function setRows ( $rows )
		{
			$this->param['rows']=$rows;
		}


		/**
		 *  Display the field
		 *  @access public
		 *  @return string contents
		 */

		public function displayField( $value, $row=NULL )
		{
			// Style
			$style=array();
			if (!empty($this->param['fieldstyle'])) $style[]=$this->param['fieldstyle'];

			// Class
			$class=array();
			$class[]='form-control';
			if (!empty($this->param['fieldclass'])) $class[]=$this->param['fieldclass'];

			// Render
			$c='<div class="'.$this->getContentWidthClass().'">';

				$value = htmlspecialchars($value);
				$value = str_replace('{','&#123;',$value);
				$value = str_replace('}','&#125;',$value);
				$value = str_replace('[','&#091;',$value);
				$value = str_replace(']','&#093;',$value);

				$c.='<textarea';
				$c.=' id="'.$this->tag.'"';
				$c.=' name="'.$this->tag.'"';
				$c.=' data-originalvalue="'.$value.'"';
				$c.=' autocomplete="off"';
				if (!empty($this->param['placeholder'])) $c.=' placeholder="'.htmlspecialchars($this->param['placeholder']).'"';
				$c.=' wrap="'.oneof($this->param['wrap'],'soft').'"';
				$c.=' rows="'.oneof($this->param['rows'],'5').'"';
				$c.=' class="'.join(' ',$class).'"';
				if (!empty($style)) $c.=' style="'.join('; ',$style).'"';
				if (!empty($this->param['readonly'])) $c.=' readonly="readonly"';
				if (!empty($this->param['disabled'])) $c.=' disabled="disabled"';
				if (!empty($this->param['keephiddenvalue'])) $c.=' data-keephiddenvalue="1"';
				if (isset($this->param['event']) && is_array($this->param['event']))
				{
					foreach ($this->param['event'] as $eventType => $eventContent) $c.=' '.$eventType.'="'.$eventContent.'"';
				}
				if (isset($this->param['data']) && is_array($this->param['data']))
				{
					foreach ($this->param['data'] as $dataKey => $dataValue) $c.=' data-'.$dataKey.'="'.htmlspecialchars($dataValue).'"';
				}
				$c.='>';
				$c.=$value;
				$c.='</textarea>';

				// Comment
				$c.=$this->displayComment();

			$c.='</div>';

			// Return
			return $c;
		}


	}


?>