<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   Field class: checklist field
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_FIELD_CHECKLIST extends BASE_FIELD_OPTIONSFIELD
	{


		/**
		 *  Set grouping
		 *  @access public
		 *  @param bool $grouping Option grouping on
		 *  @return void
		 */

		public function setGrouping ( $grouping )
		{
			$this->param['optgroup']=$grouping;
		}


		/**
		 *  Set additional option data
		 *  @access public
		 *  @param array $optionData option data
		 *  @return void
		 */

		public function setOptionData ( $optionData )
		{
			$this->param['optiondata']=$optionData;
		}


		/**
		 *  Set items per row
		 *  @access public
		 *  @param integer $itemsPerRow Items per row
		 *  @return void
		 */

		public function setItemsPerRow ( $itemsPerRow )
		{
			$this->param['itemsperrow']=$itemsPerRow;
		}


		/**
		 *  Set separate items
		 *  @access public
		 *  @param bool $separateItems Items are separate items
		 *  @return void
		 */

		public function setSeparateItems ( $separateItems)
		{
			$this->param['separateitems']=$separateItems;
		}


		/**
		 *  Set list separator
		 *  @access public
		 *  @param string $separator List separator
		 *  @return void
		 */

		public function setListSeparator ( $separator )
		{
			$this->param['list_separator']=$separator;
		}


		/**
		 *   List value
		 *   @access public
		 *   @return mixed value
		 */

		public function listValue( $value, &$row )
		{
			$options=$this->getOptions();
			if (!empty($this->param['separateitems']))
			{
				$valueArray=array();
				foreach ($options as $k => $v)
				{
					if (!empty($row[$k])) $valueArray[]=htmlspecialchars($v);
				}
			}
			else
			{
				$valueArray=str_array($value, "\t");
				foreach ($valueArray as $k=>$v) $valueArray[$k]=htmlspecialchars(oneof($options[$v], $v));
			}
			return join(oneof($this->param['list_separator'], ', '), $valueArray);
		}


		/**
		 *   Display value
		 *   @access public
		 *   @return mixed value
		 */

		public function displayValue()
		{
			$options=$this->getOptions();
			if (!empty($this->param['separateitems']))
			{
				$retval=array();
				foreach ($options as $k => $v)
				{
					if (!empty($this->formObject->data->{$k}))
					{
						$retval[]=$v;
					}
				}
				return join('<br>',$retval);
			}
			else
			{
				$value=str_array($this->getValue(),"\t");
				$retval=array();
				foreach ($value as $v)
				{
					$retval[]=$options[$v];
				}
				return join('<br>',$retval);
			}
		}


		/**
		 *   Save value
		 *   @access public
		 *   @return mixed value
		 */

		public function saveValue()
		{
			if (!empty($this->param['separateitems']))
			{
				$retval=array();
				$option=$this->getOptions();
				foreach ($option as $optionVal => $optionDesc)
				{
					$retval[$optionVal]=$_POST[$optionVal];
				}
				return $retval;
			}
			else
			{
				return join("\t",$_POST[$this->tag]);
			}
		}


		/**
		 *   Data validate
		 *   @access public
		 *   @return boolean
		 */

		public function validate( $value )
		{
			// Not required
			if (!$this->required()) return '';

			// Separate items - at least one required
			if ($this->param['separateitems'])
			{
				$options=$this->getOptions();
				$haveAtLeastOne=false;
				foreach ($options as $k => $v)
				{
					if (!empty($_POST[$k]))
					{
						$haveAtLeastOne=true;
						break;
					}
				}
				if (!$haveAtLeastOne) return '[BASE.FORM.Error.RequiredFieldEmpty]';
			}

			// Otherwise
			else
			{
				if (!is_array($value) || !sizeof($value)) return '[BASE.FORM.Error.RequiredFieldEmpty]';
			}
		}


		/**
		 *  Log data
		 *  @access public
		 *  @return string
		 */

		public function getLogData( $dataObject, $operation, &$logValuesArray )
		{
			$options=$this->getOptions();
			if ($this->param['separateitems'])
			{
				$XML='';
				foreach (array_keys($options) as $item)
				{
					unset($logValuesArray[$item]);
					if ($operation!='edit' && !mb_strlen($dataObject->{$item})) continue;
					if ($operation=='edit')
					{
						if ($dataObject->{$item}==$dataObject->_in[$item]) continue;
						if ($dataObject->{$item}=='' && $dataObject->_in[$item]=='0') continue;
						if ($dataObject->{$item}=='0' && $dataObject->_in[$item]=='') continue;
					}
					$XML.='<fld id="'.htmlspecialchars($item).'" name="'.htmlspecialchars($options[$item]).'">';
					if ($operation!='add' && !$this->param['forcevalue']) $XML.='<old value="'.htmlspecialchars($dataObject->_in[$item]).'">'.htmlspecialchars(!empty($dataObject->_in[$item])?'[BASE.COMMON.Yes|tolower]':'--').'</old>';
					$XML.='<new value="'.htmlspecialchars($dataObject->{$item}).'">'.htmlspecialchars(!empty($dataObject->{$item})?'[BASE.COMMON.Yes|tolower]':'--').'</new>';
					$XML.='</fld>';
				}
			}
			else
			{
				unset($logValuesArray[$this->tag]);

				if ($operation!='edit' && !mb_strlen($dataObject->{$this->tag})) return;
				if ($operation=='edit')
				{
					if ($dataObject->{$this->tag}==$dataObject->_in[$this->tag]) return;
					if ($dataObject->{$this->tag}=='' && $dataObject->_in[$this->tag]=='0') return;
					if ($dataObject->{$this->tag}=='0' && $dataObject->_in[$this->tag]=='') return;
				}

				$XML='<fld id="'.htmlspecialchars($this->tag).'" name="'.htmlspecialchars($this->param['title']).'">';
				if ($operation!='add' && !$this->param['forcevalue']) $XML.='<old>'.htmlspecialchars(str_replace("\t","; ",$dataObject->_in[$this->tag])).'</old>';
				$XML.='<new>'.htmlspecialchars(str_replace("\t","; ",$dataObject->{$this->tag})).'</new>';
				$XML.='</fld>';
			}
			return $XML;
		}


		/**
		 *  Display the field
		 *  @access public
		 *  @return string contents
		 */

		public function displayField( $value, $row=NULL )
		{
			// Get data
			$option=$this->getOptions();

			// Style
			$style=array();
			if (!empty($this->param['fieldstyle'])) $style[]=$this->param['fieldstyle'];
//			if (!empty($this->param['comment'])) $style[]='width: 40% !important';

			// Class
			$class=array();
			$class[]='form-control';
			if (!empty($this->param['fieldclass'])) $class[]=$this->param['fieldclass'];

			// Value
			$value=str_array($value,"\t");

			// Items per row
			$checkboxStyle='';
			if (!empty($this->param['itemsperrow']))
			{
				$checkboxStyle='float: left; width: '.floor(100/$this->param['itemsperrow']).'%';
			}

			// Options
			if ($this->param['optgroup'])
			{
				$option=$option;
			}
			else
			{
				$option=array(''=>$option);
			}

			// Field begins
			$c='<div class="'.$this->getContentWidthClass().'">';

				$g=0;
				foreach ($option as $optionGroupName => $optionGroupData)
				{
					$g++;
					if (mb_strlen($optionGroupName))
					{
						$c.='<p class="form-control-static"'.($g>1?'style="margin-top: 10px"':'').'><b>'.htmlspecialchars($optionGroupName).'</b></p>';
					}

					if (sizeof($option)>1) $c.='<div style="overflow: hidden">';
					foreach ($optionGroupData as $optionVal => $optionDesc)
					{
						if ($this->param['separateitems'])
						{
							$checkboxID=$optionVal;
							$checkboxName=$optionVal;
							$checkboxValue='1';
							$checkboxChecked=(!empty($this->formObject->data->{$optionVal})?true:false);
						}
						else
						{
							$checkboxID=$this->tag.'_'.$optionVal;
							$checkboxName=$this->tag.'[]';
							$checkboxValue=$optionVal;
							$checkboxChecked=(in_array($optionVal,$value)?true:false);
						}

						$c.='<div class="checkbox" style="'.$checkboxStyle.'">';
						$c.='<label>';
						$c.='<input';
						$c.=' type="checkbox"';
						$c.=' id="'.$checkboxID.'"';
						$c.=' name="'.$checkboxName.'"';
						$c.=' value="'.htmlspecialchars($checkboxValue).'"';
						if ($checkboxChecked) $c.=' checked="checked"';
						if (!empty($style)) $c.=' style="'.join('; ',$style).'"';
						if (!empty($this->param['disabled'])) $c.=' disabled="disabled"';
						if (!empty($this->param['keephiddenvalue'])) $c.=' data-keephiddenvalue="1"';
						if (isset($this->param['event']) && is_array($this->param['event']))
						{
							foreach ($this->param['event'] as $eventType => $eventContent) $c.=' '.$eventType.'="'.$eventContent.'"';
						}
						$c.='>'.htmlspecialchars($optionDesc);
						$c.='</label>';
						$c.='</div>';
					}
					if (sizeof($option)>1) $c.='</div>';
				}

			// Comment
			$c.=$this->displayComment();

			$c.='</div>';

			// Return
			return $c;
		}


	}


?>