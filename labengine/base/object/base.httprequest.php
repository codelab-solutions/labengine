<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   The HTTP request class
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_HTTPREQUEST
	{


		/**
		 *   cURL handle
		 *   @var resource
		 *   @public
		 */

		public $CH = NULL;


		/**
		 *   URL
		 *   @var string
		 *   @public
		 */

		public $URL = '';


		/**
		 *   Effective URL
		 *   @var string
		 *   @public
		 */

		public $effectiveURL = '';


		/**
		 *   Method
		 *   @var string
		 *   @public
		 */

		public $requestMethod = 'GET';


		/**
		 *   Request headers
		 *   @var string
		 *   @public
		 */

		public $requestHeader = [];


		/**
		 *   Options
		 *   @var array
		 *   @public
		 */

		public $requestOptions = [];


		/**
		 *   Get fields
		 *   @var array
		 *   @public
		 */

		public $getFields = [];


		/**
		 *   Post fields
		 *   @var array
		 *   @public
		 */

		public $postFields = [];


		/**
		 *   File uploads
		 *   @var array
		 *   @public
		 */

		public $fileUploads = [];


		/**
		 *   Raw POST data
		 *   @var string|null
		 *   @public
		 */

		public $rawPostData = null;


		/**
		 *   Response info
		 *   @var array
		 *   @public
		 */

		public $responseInfo = [];


		/**
		 *   Response headers
		 *   @var string
		 *   @public
		 */

		public $responseHeader = [];


		/**
		 *   Response body
		 *   @var string
		 *   @public
		 */

		public $responseBody = FALSE;


		/**
		 *   Constructor
		 *   @public
		 *   @param string $URL URL
		 *   @param string $requestMethod Method
		 *   @param string $requestOptions Options
		 *   @return BASE_HTTPREQUEST
		 */

		public function __construct( $URL=null, $requestMethod=null, $requestOptions=null )
		{
			global $LAB;

			// Set parameters
			if (!empty($URL)) $this->setURL($URL);
			if (!empty($requestMethod)) $this->setRequestMethod($requestMethod);
			if (!empty($requestOptions)) $this->setOptions($requestOptions);
		}


		/**
		 *   Set URL
		 *   @public
		 *   @param string $URL URL
		 *   @return void
		 */

		public function setURL( $URL )
		{
			$this->URL=$URL;
		}


		/**
		 *   Set method
		 *   @public
		 *   @param string $requestMethod Request method (POST, GET, PUT, PATCH, DELETE)
		 *   @return void
		 */

		public function setRequestMethod( $requestMethod )
		{
			$this->requestMethod=mb_strtoupper($requestMethod);
		}


		/**
		 *   Set request option
		 *   @public
		 *   @param string $optionName Option name
		 *   @param string $optionValue Option value
		 *   @return void
		 */

		public function setOption( $optionName, $optionValue )
		{
			$this->requestOptions[$optionName]=$optionValue;
		}


		/**
		 *   Set request options
		 *   @public
		 *   @param array $requestOptions Options
		 *   @return void
		 */

		public function setOptions( $requestOptions )
		{
			foreach ($requestOptions as $k => $v)
			{
				$this->requestOptions[$k]=$v;
			}
		}


		/**
		 *   Set GET fields
		 *   @public
		 *   @param array $getFields GET fields
		 *   @return void
		 */

		public function setGetFields( $getFields )
		{
			foreach ($getFields as $k => $v)
			{
				$this->getFields[$k]=$v;
			}
		}


		/**
		 *   Set GET field
		 *   @public
		 *   @param string $field Field name
		 *   @param string $value Value
		 *   @return void
		 */

		public function setGetField( $field, $value )
		{
			$this->getFields[$field]=$value;
		}


		/**
		 *   Set POST fields
		 *   @public
		 *   @param array $postFields Post fields
		 *   @return void
		 */

		public function setPostFields( $postFields )
		{
			foreach ($postFields as $k => $v)
			{
				$this->postFields[$k]=$v;
			}
		}


		/**
		 *   Set POST field
		 *   @public
		 *   @param string $field Field name
		 *   @param string $value Value
		 *   @return void
		 */

		public function setPostField( $field, $value )
		{
			$this->postFields[$field]=$value;
		}


		/**
		 *   Set raw POST data
		 *   @public
		 *   @param string $rawPostData Raw POST data
		 *   @return void
		 */

		public function setRawPostData( $rawPostData )
		{
			$this->rawPostData=$rawPostData;
		}


		/**
		 *   Set up file upload
		 *   @public
		 *   @param string $sourceFilename Source filename
		 *   @param string $contentType Content-type
		 *   @param string $postFilename POST file name
		 *   @param string $postFileID Post file ID
		 *   @throws Exception
		 *   @return void
		 */

		public function setFileUpload( $sourceFilename, $contentType=null, $postFilename=null, $postFileID=null )
		{
			// Check & init
			if (empty($sourceFilename)) throw new Exception('Missing source file name.');
			if (!is_readable($sourceFilename)) throw new Exception('Source file not readable.');

			// Check/get filename
			if (empty($postFilename))
			{
				$postFilename=pathinfo($sourceFilename,PATHINFO_BASENAME);
			}

			// Check/get content-type
			if (empty($contentType))
			{
				$contentType=BASE::getMimeType($postFilename);
			}

			// File ID
			if (empty($postFileID))
			{
				$postFileID=uniqid();
			}

			// Create CURL file object
			$cfile=curl_file_create($sourceFilename,$contentType,$postFilename);

			// Set method, just-in-case
			$this->setRequestMethod('POST');

			// Add to file upload list
			$this->fileUploads[$postFileID]=$cfile;
		}


		/**
		 *   Set login
		 *   @public
		 *   @param string $username Username
		 *   @param string $password Password
		 *   @return void
		 */

		public function setLogin( $username, $password )
		{
			$this->requestOptions['httpauth']=$username.':'.$password;
		}


		/**
		 *   Set certificate-based login
		 *   @public
		 *   @param string $certificate Certificate file
		 *   @param string $key Key file
		 *   @param string $certificatePassword Certificate file password
		 *   @param string $keyPassword Key file password
		 *   @param string $certificateType Certificate type
		 *   @return void
		 */

		public function setCertificateLogin( $certificate, $key=null, $certificatePassword=null, $keyPassword=null, $certificateType=null )
		{
			$this->requestOptions['sslcert']=$certificate;
			if (!empty($key)) $this->requestOptions['sslkey']=$key;
			if (!empty($certificatePassword)) $this->requestOptions['sslcertpasswd']=$certificatePassword;
			if (!empty($keyPassword)) $this->requestOptions['sslkeypasswd']=$keyPassword;
			if (!empty($certificateType)) $this->requestOptions['sslkeytype']=$certificateType;
		}


		/**
		 *   Set user agent
		 *   @public
		 *   @param string $userAgent User agent string
		 *   @return void
		 */

		public function setUserAgent( $userAgent )
		{
			$this->requestHeader['User-Agent']=$userAgent;
		}


		/**
		 *   Set request header
		 *   @public
		 *   @param string $headerName Header name
		 *   @param string $headerValue Header value
		 *   @return void
		 */

		public function setRequestHeader( $headerName, $headerValue )
		{
			$this->requestHeader[$headerName]=$headerValue;
		}


		/**
		 *   Set request options
		 *   @public
		 *   @param array $requestHeaders Headers
		 *   @return void
		 */

		public function setRequestHeaders( $requestHeaders )
		{
			foreach ($requestHeaders as $k => $v)
			{
				$this->requestHeader[$k]=$v;
			}
		}


		/**
		 *   Set request content-type
		 *   @public
		 *   @param $requestContentType Request content-type
		 *   @return void
		 */

		public function setRequestContentType( $requestContentType )
		{
			$this->requestHeader['Content-Type']=$requestContentType;
		}


		/**
		 *   Build a HTTP GET query string from an array
		 *   @access public
		 *   @static
		 *   @param bool|array $fields Fields
		 *   @param bool $fixSpace Fix spaces
		 *   @param bool|string $encodeTo Encode result to given charset
		 *   @return string
		 */

		public static function buildQueryString( $fields=FALSE, $fixSpace=FALSE, $encodeTo=FALSE )
		{
			$QS=array();
			foreach ($fields as $k => $v)
			{
				if ($encodeTo) $v=iconv('UTF-8',$encodeTo,$v);
				$v=urlencode($v);
				if ($fixSpace) $v=str_replace('+','%20',$v);
				if ($fixSpace) $v=str_replace('%3B',';',$v);
				$QS[]=$k.'='.$v;
			}
			return join('&',$QS);
		}


		/**
		 *   Make request
		 *   @public
		 *   @return content
		 *   @throws Exception
		 */

		public function send()
		{
			global $LAB;
			try
			{
				// Check
				if (!mb_strlen($this->URL)) throw new Exception('URL not specified.');
				if (!in_array($this->requestMethod,array('GET','POST','PUT','PATCH','DELETE'))) throw new Exception('Invalid request method.');

				// Init
				$this->CH=curl_init();
				if (empty($this->CH)) throw new Exception('Error initializing curl.');

				// Set URL
				$this->effectiveURL=$this->URL;
				if (sizeof($this->getFields))
				{
					$this->effectiveURL.='?'.BASE_HTTPREQUEST::buildQueryString($this->getFields);
				}
				curl_setopt($this->CH,CURLOPT_URL,$this->effectiveURL);

				// Set method
				switch($this->requestMethod)
				{
					case 'GET':
						curl_setopt($this->CH,CURLOPT_HTTPGET,1);
						break;
					case 'POST':
						curl_setopt($this->CH,CURLOPT_POST,1);
						if (!empty($this->fileUploads))
						{
							curl_setopt($this->CH,CURLOPT_POSTFIELDS,$this->fileUploads);
						}
						elseif (mb_strlen($this->rawPostData))
						{
							if (empty($this->requestHeader['Content-Type'])) $this->requestHeader['Content-Type']='text/plain';
							curl_setopt($this->CH,CURLOPT_POSTFIELDS,$this->rawPostData);
							$this->setRequestHeader('Content-Length',strlen($this->rawPostData));
						}
						else
						{
							curl_setopt($this->CH,CURLOPT_POSTFIELDS,$this->postFields);
						}
						break;
					case 'PUT':
						curl_setopt($this->CH,CURLOPT_POST,1);
						curl_setopt($this->CH, CURLOPT_CUSTOMREQUEST, 'PUT');
						if (mb_strlen($this->rawPostData))
						{
							if (empty($this->requestHeader['Content-Type'])) $this->requestHeader['Content-Type']='text/plain';
							curl_setopt($this->CH,CURLOPT_POSTFIELDS,$this->rawPostData);
							$this->setRequestHeader('Content-Length',strlen($this->rawPostData));
						}
						else
						{
							curl_setopt($this->CH,CURLOPT_POSTFIELDS,$this->postFields);
						}
						break;
					case 'PATCH':
						curl_setopt($this->CH,CURLOPT_POST,1);
						curl_setopt($this->CH, CURLOPT_CUSTOMREQUEST, 'PATCH');
						if (mb_strlen($this->rawPostData))
						{
							if (empty($this->requestHeader['Content-Type'])) $this->requestHeader['Content-Type']='text/plain';
							curl_setopt($this->CH,CURLOPT_POSTFIELDS,$this->rawPostData);
							$this->setRequestHeader('Content-Length',strlen($this->rawPostData));
						}
						else
						{
							curl_setopt($this->CH,CURLOPT_POSTFIELDS,$this->postFields);
						}
						break;
					case 'DELETE':
						curl_setopt($this->CH,CURLOPT_POST,1);
						curl_setopt($this->CH, CURLOPT_CUSTOMREQUEST, 'DELETE');
						if (mb_strlen($this->rawPostData))
						{
							if (empty($this->requestHeader['Content-Type'])) $this->requestHeader['Content-Type']='text/plain';
							curl_setopt($this->CH,CURLOPT_POSTFIELDS,$this->rawPostData);
							$this->setRequestHeader('Content-Length',strlen($this->rawPostData));
						}
						else
						{
							curl_setopt($this->CH,CURLOPT_POSTFIELDS,$this->postFields);
						}
						break;
					default:
						throw new Exception('Unknown request method: '.$this->requestMethod);
				}

				// Apply options
				if (array_key_exists('httpauth',$this->requestOptions))
				{
					curl_setopt($this->CH,CURLOPT_HTTPAUTH,CURLAUTH_BASIC) ;
					curl_setopt($this->CH,CURLOPT_USERPWD,$this->requestOptions['httpauth']);
				}
				elseif (array_key_exists('username',$this->requestOptions))
				{
					curl_setopt($this->CH,CURLOPT_HTTPAUTH,CURLAUTH_BASIC) ;
					curl_setopt($this->CH,CURLOPT_USERPWD,$this->requestOptions['username'].":".$this->requestOptions['password']);
				}
				if (array_key_exists('sslcert',$this->requestOptions))
				{
					curl_setopt($this->CH,CURLOPT_SSLCERT,$this->requestOptions['sslcert']);
					if (array_key_exists('sslcertpasswd',$this->requestOptions))
					{
						curl_setopt($this->CH,CURLOPT_SSLCERTPASSWD,$this->requestOptions['sslcertpasswd']);
					}
					if (array_key_exists('sslkey',$this->requestOptions))
					{
						curl_setopt($this->CH,CURLOPT_SSLKEY,$this->requestOptions['sslkey']);
					}
					if (array_key_exists('sslkeypasswd',$this->requestOptions))
					{
						curl_setopt($this->CH,CURLOPT_SSLKEYPASSWD,$this->requestOptions['sslkeypasswd']);
					}
					if (array_key_exists('sslkeytype',$this->requestOptions))
					{
						curl_setopt($this->CH,CURLOPT_SSLCERTTYPE,$this->requestOptions['sslkeytype']);
						curl_setopt($this->CH,CURLOPT_SSLKEYTYPE,$this->requestOptions['sslkeytype']);
					}
				}
				if (array_key_exists('ssl_verifyhost',$this->requestOptions))
				{
					curl_setopt($this->CH,CURLOPT_SSL_VERIFYHOST,$this->requestOptions['ssl_verifyhost']);
				}
				else
				{
					curl_setopt($this->CH,CURLOPT_SSL_VERIFYHOST,false);
				}
				if (array_key_exists('ssl_verifypeer',$this->requestOptions))
				{
					curl_setopt($this->CH,CURLOPT_SSL_VERIFYPEER,$this->requestOptions['ssl_verifypeer']);
				}
				else
				{
					curl_setopt($this->CH,CURLOPT_SSL_VERIFYPEER,false);
				}
				if (array_key_exists('ssl_cainfo',$this->requestOptions))
				{
					curl_setopt($this->CH,CURLOPT_CAINFO,$this->requestOptions['ssl_cainfo']);
				}
				else
				{
					curl_setopt($this->CH, CURLOPT_CAINFO,stream_resolve_include_path('base/data/cacert/cacert.pem'));
				}
				if (array_key_exists('ssl_version',$this->requestOptions))
				{
					curl_setopt($this->CH,CURLOPT_SSLVERSION,$this->requestOptions['ssl_version']);
				}
				if (array_key_exists('followlocation',$this->requestOptions))
				{
					curl_setopt($this->CH,CURLOPT_FOLLOWLOCATION,$this->requestOptions['followlocation']);
				}
				else
				{
					curl_setopt($this->CH,CURLOPT_FOLLOWLOCATION,true);
				}
				if (array_key_exists('connecttimeout',$this->requestOptions))
				{
					curl_setopt($this->CH,CURLOPT_CONNECTTIMEOUT,$this->requestOptions['connecttimeout']);
				}
				else
				{
					curl_setopt($this->CH,CURLOPT_CONNECTTIMEOUT,'15');
				}
				if (array_key_exists('timeout',$this->requestOptions))
				{
					curl_setopt($this->CH,CURLOPT_TIMEOUT,$this->requestOptions['timeout']);
				}
				else
				{
					curl_setopt($this->CH,CURLOPT_TIMEOUT,'60');
				}
				if (array_key_exists('cookiejar',$this->requestOptions))
				{
					curl_setopt($this->CH,CURLOPT_COOKIEJAR,$this->requestOptions['cookiejar']);
					curl_setopt($this->CH,CURLOPT_COOKIEFILE,$this->requestOptions['cookiejar']);
				}
				elseif (!empty($this->requestOptions['newcookiesession']))
				{
					curl_setopt($this->CH,CURLOPT_COOKIESESSION,true);
				}
				if (array_key_exists('referer',$this->requestOptions))
				{
					curl_setopt($this->CH,CURLOPT_REFERER,$this->requestOptions['referer']);
				}
				if (array_key_exists('useragent',$this->requestOptions))
				{
					curl_setopt($this->CH,CURLOPT_USERAGENT,$this->requestOptions['useragent']);
				}

				// Apply headers
				if (sizeof($this->requestHeader))
				{
					$requestHeader=array();
					foreach ($this->requestHeader as $header => $value)
					{
						$requestHeader[]=$header.': '.$value;
					}
					curl_setopt($this->CH,CURLOPT_HTTPHEADER,$requestHeader);
				}

				// Those shall we always need
				curl_setopt($this->CH,CURLOPT_ENCODING,'');
				curl_setopt($this->CH,CURLOPT_HEADER,1);
				curl_setopt($this->CH,CURLOPT_RETURNTRANSFER,1);
				curl_setopt($this->CH,CURLINFO_HEADER_OUT,1);

				// Exec
				$response=curl_exec($this->CH);

				// Error?
				if ($response===false)
				{
					throw new Exception('Error making HTTP request: '.curl_errno($this->CH).' - '.curl_error($this->CH));
				}

				// Store request info
				$this->responseInfo=curl_getinfo($this->CH);

				// Split header and body
				$responseHeader=substr($response,0, $this->responseInfo['header_size']);
				$this->responseBody=substr($response,$this->responseInfo['header_size']);

				// Split headers
				$this->responseHeader=array_map(
					function($x){
						return array_map("trim", explode(":", $x, 2));
					},
					array_filter(array_map("trim", explode("\n", $responseHeader)))
				);

				// Close handle
				curl_close($this->CH);

				// Return
				return $this->responseBody;
			}
			catch (Exception $e)
			{
				if (!empty($this->CH)) curl_close($this->CH);
				throw $e;
			}
		}


		/**
		 *   Get response code
		 *   @public
		 *   @return boolean|int
		 */

		public function getResponseCode()
		{
			return (array_key_exists('http_code',$this->responseInfo)?$this->responseInfo['http_code']:false);
		}


		/**
		 *   Get response headers
		 *   @public
		 *   @return void
		 */

		public function getResponseHeaders()
		{
			return $this->responseHeader;
		}


		/**
		 *   Get response header
		 *   @public
		 *   @param string $headerName Header name
		 *   @param bool $caseSensitive Is match case sensitive?
		 *   @return void
		 */

		public function getResponseHeader( $headerName, $caseSensitive=false )
		{
			$responseHeader=array();
			foreach ($this->responseHeader as $header)
			{
				if ($header[0]==$headerName || (!$caseSensitive && mb_strtolower($header[0])==mb_strtolower($headerName)))
				{
					$responseHeader[]=$header[1];
				}
			}
			if (sizeof($responseHeader)>1)
			{
				return $responseHeader;
			}
			elseif (sizeof($responseHeader)==1)
			{
				return $responseHeader[0];
			}
			else
			{
				return null;
			}
		}


	}


?>