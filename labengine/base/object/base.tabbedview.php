<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   The tabbed view class
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_TABBEDVIEW extends BASE_DISPLAYOBJECT
	{

		/**
		 *   Tabs
		 *   @var array
		 */

		public $tab = array();


		/**
		 *   Init the view
		 *   @throws Exception
		 *   @return void
		 */

		public function initView()
		{
			// This should be implemented in the subclass.
		}


		/**
		 *   Init the tabs
		 *   @throws Exception
		 *   @return void
		 */

		public function initTabs()
		{
			// This should be implemented in the subclass.
		}


		/**
		 *   Add a tab
		 *   @access public
		 *   @return BASE_TABBEDVIEW_TAB
		 *   @throws Exception
		 */

		public function addTab( $tabTag )
		{
			// Check
			if (isset($this->tab[$tabTag])) throw new Exception('Tab <b>'.$tabTag.'</b> already exists.');

			// Create tab
			$this->tab[$tabTag]=new BASE_TABBEDVIEW_TAB();
			$this->tab[$tabTag]->tag=$tabTag;

			// Create backreference
			$this->tab[$tabTag]->viewObject=&$this;

			// Return reference to action
			$retval=&$this->tab[$tabTag];
			return $retval;
		}


		/**
		 *  Set return link
		 *  @access public
		 *  @param string $returnLink Return link
		 *  @param string $returnLinkTitle Return link title
		 *  @return void
		 */

		public function setReturnLink ( $returnLink, $returnLinkTitle=null )
		{
			$this->param['returnlink']=$returnLink;
			$this->param['returnlink_title']=$returnLinkTitle;
		}


		/**
		 *  Get return link
		 *  @access public
		 *  @return string return link
		 */

		public function getReturnLink()
		{
			return $this->param['returnlink'];
		}


		/**
		 *  Set return JS action
		 *  @access public
		 *  @param string $returnAction JS action
		 *  @param string $returnActionTitle Return link/action title
		 *  @return void
		 */

		public function setReturnAction ( $returnAction, $returnActionTitle=null )
		{
			$this->param['returnaction']=$returnAction;
			$this->param['returnlink_title']=$returnActionTitle;
		}


		/**
		 *  Get return JS action
		 *  @access public
		 *  @return string return JS action
		 */

		public function getReturnAction()
		{
			return $this->param['returnaction'];
		}


		/**
		 *  Display title
		 *  @access public
		 *  @return string title content
		 */

		public function displayTitle()
		{
			$c='';
			if (!empty($this->param['title']))
			{
				$c.='<h1>'.htmlspecialchars($this->param['title']).'</h1>';
			}
			return $c;
		}


		/**
		 *  Display tabs
		 *  @access public
		 *  @return string tabs
		 */

		public function displayTabs()
		{
			$c='<div class="tabbedview-tabs"><ul class="nav nav-tabs">';
			$t=0;
			foreach ($this->tab as $tabTag => $tabObject)
			{
				$c.='<li id="tab_'.$tabTag.'" role="presentation"'.($t==0?' class="active"':'').'><a onclick="TAB.select(\''.htmlspecialchars($tabTag).'\');">'.$tabObject->tabTitle.'</a></li>';
				$t++;
			}
			$c.='</ul></div>';
			return $c;
		}


		/**
		 *   Display the tabbed view
		 *   @throws Exception
		 *   @return string
		 */

		public function display()
		{
			global $LAB;

			// Return tab contents
			if (!empty($LAB->REQUEST->requestVar['get']))
			{
				// Init view
				$retval=$this->initView();
				if ($retval===false) return;

				// Init tabs
				$retval=$this->initTabs();
				if ($retval===false) return;
				if (!sizeof($this->tab)) throw new Exception('No tabs defined.');

				// Do
				$LAB->RESPONSE->setType('json');
				$tabTag=$LAB->REQUEST->requestVar['get'];
				try
				{
					if (isset($this->tab[$tabTag]))
					{
						if (!method_exists($this,$this->tab[$tabTag]->tabHandlerFunction)) throw new Exception('Tab handler method <b>'.$this->tab[$tabTag]->tabHandlerFunction.'()</b> not defined in the class.');
						$response=array();
						if (!empty($this->tab[$tabTag]->tabHandlerFunctionParam))
						{
							if (is_array($this->tab[$tabTag]->tabHandlerFunctionParam))
							{
								$response['content']=call_user_func_array(array($this,$this->tab[$tabTag]->tabHandlerFunction),$this->tab[$tabTag]->tabHandlerFunctionParam);
							}
							else
							{
								$response['content']=call_user_func(array($this,$this->tab[$tabTag]->tabHandlerFunction,$this->tab[$tabTag]->tabHandlerFunctionParam));
							}
						}
						else
						{
							$response['content']=call_user_func(array($this,$this->tab[$tabTag]->tabHandlerFunction));
						}
						$response['status']='1';
						return $response;
					}
					else
					{
						throw new Exception('Tab '.$tabTag.' not found.');
					}
				}
				catch (Exception $e)
				{
					return array(
						'status' => '2',
						'error'  => $e->getMessage()
					);
				}
			}

			// Main content
			try
			{
				// Init
				$c='';

				// Init view
				$retval=$this->initView();
				if ($retval===false) return;

				// Init tabs
				$retval=$this->initTabs();
				if ($retval===false) return;
				if (!sizeof($this->tab)) throw new Exception('No tabs defined.');

				// Title
				$c.=$this->displayTitle();

				// Tabs navigation
				$c.=$this->displayTabs();

				// Content
				$t=0; reset($this->tab);
				$c.='<div class="block tabbedview-content">';
				foreach ($this->tab as $tabTag => $tabObject)
				{
					if (!$t)
					{
						$c.='<div id="content_'.$tabTag.'" class="tabcontent">';
						if (method_exists($this,$tabObject->tabHandlerFunction))
						{
							$c.=call_user_func(array($this,$tabObject->tabHandlerFunction));
						}
						else
						{
							$c.='<p>The method <b>'.$tabObject->tabHandlerFunction.'()</b> does not exist in this class.</p>';
						}
						$c.='</div>';
					}
					else
					{
						$c.='<div id="content_'.$tabTag.'" class="tabcontent" rel="'.$this->buildURL().'/get='.$tabTag.'/" data-displaytrigger="'.(!empty($tabObject->tabSelectTrigger)?$tabObject->tabSelectTrigger:'').'"></div>';
					}
					$t++;
				}
				$c.='</div>';

				// Return link
				if (isset($this->param['returnaction']))
				{
					$c.='<div class="returnlink"><a onclick="'.$this->param['returnaction'].'"><span class="icon-back"></span> '.oneof($this->param['returnlink_title'],'[BASE.LIST.Return]').'</a></div>';
				}
				elseif (isset($this->param['returnlink']))
				{
					$c.='<div class="returnlink"><a href="'.$this->param['returnlink'].'"><span class="icon-back"></span> '.oneof($this->param['returnlink_title'],'[BASE.LIST.Return]').'</a></div>';
				}

				// Return
				return $c;
			}
			catch (Exception $e)
			{
				$c='';
				if (!empty($this->param['title']))
				{
					$c.='<h1>'.htmlspecialchars($this->param['title']).'</h1>';
				}
				$c.='<div class="alert alert-dismissable alert-danger">'.htmlspecialchars($e->getMessage()).'</div>';
				return $c;
			}
		}


		/**
		 *   Display log tab
		 *   @access public
		 *   @return string
		 *   @throws Exception
		 */

		function getLog()
		{
			global $LAB;

			// Init list
			$LIST=new BASE_LIST();
			$LIST->setListClass('table table-bordered table-hovered table-striped');
			$LIST->data=new BASE_LOG();
			$listParam=new BASE_DATAOBJECT_PARAM();
			$listParam->addWhere('ref_oid='.intval($this->data->_in[$this->data->_param['idfield']]));
			$listParam->addOrderBy('log_tstamp desc');
			$LIST->setLoadParam($listParam);
			$LIST->setNoResultsMessage('[BASE.LOG.List.NoResults]');

			// Fields
			$f=$LIST->addField('log_tstamp',new BASE_FIELD_DATE());
			$f->setTitle('[BASE.LOG.Fld.Tstamp]');
			$f->setListFieldWidth('15%');
			$f->setListShowTime(true);

			$f=$LIST->addField('user_oid',new BASE_FIELD_LOG_USER());
			$f->setTitle('[BASE.LOG.Fld.User]');
			$f->setSourceDAO(get_class($LAB->USER),$LAB->USER->_param['idfield'],$LAB->USER->_param['usernamefield']);
			$f->setListFieldWidth('25%');

			$f=$LIST->addField('log_entry',new BASE_FIELD_TEXT());
			$f->setTitle('[BASE.LOG.Fld.Entry]');
			$f->setListFieldWidth('60%');

			$f=$LIST->addField('log_data',new BASE_FIELD_LOG_DATA());
			$f->setTitle('<span class="icon-log"></span>');
			$f->setListFieldWidth('1%');
			$f->setListFieldClass('iconactions');

			// Return
			return $LIST->display();
		}


	}


?>