<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   The text list filter class
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class BASE_LIST_FILTER_TEXT extends BASE_LIST_FILTER
	{


		/**
		 *   Sets autocomplete
		 *   @param bool $autocomplete Autocomplete
		 */

		public function setAutocomplete( $autocomplete )
		{
			$this->param["autocomplete"]=$autocomplete;
		}


		/**
		 *   Sets autocomplete minimum length
		 *   @param int $autocompleteMinLength Minimum length
		 */

		public function setAutocompleteMinLength( $autocompleteMinLength )
		{
			$this->param["autocomplete_minlength"]=$autocompleteMinLength;
		}


		/**
		 *   Sets autocomplete source URL
		 *   @param string $autoCompleteSourceURL Autocomplete source URL
		 */

		public function setAutocompleteSourceURL( $autoCompleteSourceURL )
		{
			$this->param["autocomplete_sourceurl"]=$autoCompleteSourceURL;
		}


		/**
		 *   Sets autocomplete source list
		 *   @param string $autoCompleteSourceList Autocomplete source list variable
		 */

		public function setAutocompleteSourceList( $autoCompleteSourceList )
		{
			$this->param["autocomplete_sourcelist"]=$autoCompleteSourceList;
		}


		/**
		 *   Sets autocomplete key/value mode
		 *   @param bool $autoCompleteKeyValue Key/value mode
		 */

		public function setAutocompleteKeyValue( $autoCompleteKeyValue )
		{
			$this->param["autocomplete_keyvalue"]=$autoCompleteKeyValue;
		}


		public function displayField( $value, $row=NULL )
		{
			// Class
			$class=array();
			$class[]='form-control';
			if (strpos($this->tag,'date')!==FALSE) $class[]='hasDatepicker';
			if (!empty($this->error)) $class[]='error';
			if (!empty($this->param['autocomplete'])) $class[]='autocomplete';

			$c='<div id="filterwrapper_'.$this->tag.'" class="filterwrapper">';
			$c.='<input type="text"';
				$c.=' class="'.join(' ',$class).'"';
				$c.=' autocomplete="off"';
				$c.=' id="filter_'.$this->tag.'"';
				$c.=' name="filter_'.$this->tag.'"';
				$c.=' value="'.$this->getValue().'"';
				if (!empty($this->param['autocomplete']))
				{
					$c.='data-autocomplete-minlength="'.intval($this->param["autocomplete_minlength"]).'"';
					$c.='data-autocomplete-sourceurl="'.htmlspecialchars($this->param["autocomplete_sourceurl"]).'"';
					$c.='data-autocomplete-sourcelist="'.htmlspecialchars($this->param["autocomplete_sourcelist"]).'"';
					$c.='data-autocomplete-keyvalue="'.($this->param["autocomplete_keyvalue"]?'1':'0').'"';
				}
				if (sizeof($this->filterEvent))
				{
					foreach ($this->filterEvent as $eventType => $eventContent) $c.=' '.$eventType.'="'.$eventContent.'"';
				}
			$c.='></div>';
			$c.='<script language="JavaScript"> $(function(){ FORM.initElements(\'#filterwrapper_'.$this->tag.'\'); }); </script>';
			return $c;
		}

	}


?>