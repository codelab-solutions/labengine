<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   Error handling
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */



	/**
	 *   The error handler function
	 *   @param integer $errNo Error number
	 *   @param string $errStr Error string
	 *   @param string $errFile Error file
	 *   @param integer $errLine Error line
	 *   @return void
	 */

	function errorHandler( $errNo, $errStr, $errFile, $errLine )
	{
		global $LAB;
		$debugOn=(is_object($LAB) && is_object($LAB->DEBUG) && $LAB->DEBUG->debugOn)?TRUE:FALSE;
		$devMode=(is_object($LAB) && is_object($LAB->CONFIG) && $LAB->CONFIG->get('debug.dev'))?TRUE:FALSE;
		$XHR=((!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH']=='XMLHttpRequest')?TRUE:FALSE);
		$XML=((is_object($LAB) && is_object($LAB->RESPONSE) && $LAB->RESPONSE->responseType=='xml')?TRUE:FALSE);
		$stackTrace=array();
		switch($errNo)
		{
			case E_WARNING:
			case E_NOTICE:
			case E_USER_WARNING:
			case E_USER_NOTICE:
			case E_STRICT:
			case E_DEPRECATED:
			case E_RECOVERABLE_ERROR:
				if (!$debugOn) return;
				if (strpos($errStr,'deprecated')!==FALSE) return;
				if (!strncmp($errStr,'Undefined index:',16)) return;
				if (!strncmp($errStr,'Undefined offset:',17)) return;
				if (!strncmp($errStr,'Undefined property:',19)) return;
				$LAB->DEBUG->addMessage('PHP Error','<b>'.$errStr.'</b><br/>(in: '.$errFile.' line '.$errLine.')');
				return;
			default:
				header('Expires: '.date("D, j M Y G:i:s T"));
				header('Pragma: no-cache');
				header('Cache-Control: no-store, no-cache, must-revalidate');
				header('Cache-Control: post-check=0, pre-check=0', false);
				if ($debugOn)
				{
					$errStr=$errStr.' (in: '.$errFile.' line '.$errLine.')';
					foreach (debug_backtrace() as $lvl => $trace)
					{
						$t='';
						$t.='#'.$lvl.': '.$trace['file'].' line '.$trace['line'];
						if (!empty($trace['function']))
						{
							$t.=' -- ';
							if (!empty($trace['class'])) if ($debugOn) $t.= $trace['class'].$trace['type'];
							if ($debugOn) $t.= $trace['function'].'(';
							if (!empty($trace['args']) && sizeof($trace['args']))
							{
								$arg=array();
								foreach ($trace['args'] as $a)
								{
									if (is_object($a))
									{
										$t.='&'.get_class($a);
										if ($a instanceof BASE_DATAOBJECT)
										{
											$t.='('.intval($a->_oid).')';
										}
									}
									elseif ($a===NULL)
									{
										$arg[]='NULL';
									}
									elseif (is_array($a))
									{
										$arg[]='array('.sizeof($a).')';
									}
									elseif (is_numeric($a))
									{
										$arg[]=$a;
									}
									else
									{
										$arg[]='"'.$a.'"';
									}
								}
								if ($debugOn) $t.= join(', ',$arg);
							}
							if ($debugOn) $t.= ')';
						}
						$stackTrace[]=$t;
					}
				}
				if ($XHR)
				{
					outputErrorJSON($errStr,($debugOn?$stackTrace:FALSE));
				}
				elseif ($XML)
				{
					outputErrorXML($errStr,($debugOn?$stackTrace:FALSE));
				}
				elseif (!empty($_SERVER['HTTP_HOST']))
				{
					$errorFunc=oneof($LAB->DEBUG->outputErrorHTML,'outputErrorHTML');
					call_user_func($errorFunc,$errStr,($debugOn?$stackTrace:FALSE));
				}
				else
				{
					$errorFunc=oneof($LAB->DEBUG->outputErrorPlain,'outputErrorPlain');
					call_user_func($errorFunc,$errStr,($debugOn?$stackTrace:FALSE));
				}
				exit;
		}
	}



	/**
	 *   The exception handler function
	 *   @param Exception $e Exception
	 *   @return void
	 */

	function exceptionHandler ( $e )
	{
		global $LAB;
		$debugOn=(is_object($LAB) && is_object($LAB->DEBUG) && $LAB->DEBUG->debugOn)?TRUE:FALSE;
		$devMode=(is_object($LAB) && is_object($LAB->CONFIG) && $LAB->CONFIG->get('debug.dev'))?TRUE:FALSE;
		$XHR=((!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH']=='XMLHttpRequest')?TRUE:FALSE);
		$XML=((is_object($LAB) && is_object($LAB->RESPONSE) && $LAB->RESPONSE->responseType=='xml')?TRUE:FALSE);
		$errStr='ERROR: '.$e->getMessage();
		$stackTrace=array();
		if ($debugOn)
		{
			$errStr=$errStr.' (in: '.$e->getFile().' line '.$e->getLine().')';
			foreach ($e->getTrace() as $lvl => $trace)
			{
				$t='';
				$t.='#'.$lvl.': '.$trace['file'].' line '.$trace['line'];
				if (!empty($trace['function']))
				{
					$t.=' -- ';
					if (!empty($trace['class'])) if ($debugOn) $t.= $trace['class'].$trace['type'];
					if ($debugOn) $t.= $trace['function'].'(';
					if (!empty($trace['args']) && sizeof($trace['args']))
					{
						$arg=array();
						foreach ($trace['args'] as $a)
						{
							if (is_object($a))
							{
								$t.='&'.get_class($a);
								if ($a instanceof BASE_DATAOBJECT)
								{
									$t.='('.intval($a->_oid).')';
								}
							}
							elseif ($a===NULL)
							{
								$arg[]='NULL';
							}
							elseif (is_array($a))
							{
								$arg[]='array('.sizeof($a).')';
							}
							elseif (is_numeric($a))
							{
								$arg[]=$a;
							}
							else
							{
								$arg[]='"'.htmlspecialchars($a).'"';
							}
						}
						if ($debugOn) $t.= join(', ',$arg);
					}
					if ($debugOn) $t.= ')';
				}
				$stackTrace[]=$t;
			}
		}
		if ($XHR)
		{
			outputErrorJSON($errStr,($debugOn?$stackTrace:FALSE));
		}
		elseif ($XML)
		{
			outputErrorXML($errStr,($debugOn?$stackTrace:FALSE));
		}
		elseif (!empty($_SERVER['HTTP_HOST']))
		{
			$errorFunc=oneof($LAB->DEBUG->outputErrorHTML,'outputErrorHTML');
			call_user_func($errorFunc,$errStr,($debugOn?$stackTrace:FALSE));
		}
		else
		{
			$errorFunc=oneof($LAB->DEBUG->outputErrorPlain,'outputErrorPlain');
			call_user_func($errorFunc,$errStr,($debugOn?$stackTrace:FALSE));
		}
		exit;
	}



	/**
	 *   The shutdown handler function
	 *   @return void
	 */

	function shutdownHandler()
	{
		if(!is_null($e = error_get_last()))
		{
			errorHandler($e['type'],$e['message'],$e['file'],$e['line']);
		}
	}



	/**
	 *   Output error as JSON
	 *   @param string $errorMessage Error message
	 *   @param boolean|array $stackTrace Stack trace (or FALSE if none)
	 *   @return void
	 */

	function outputErrorJSON( $errorMessage, $stackTrace=FALSE )
	{
		$response=array();
		$response['status']='2';
		$response['error']='FATAL ERROR: '.$errorMessage;
		if ($stackTrace!==FALSE) $response['stacktrace']=$stackTrace;
		echo json_encode($response,JSON_FORCE_OBJECT|JSON_HEX_QUOT);
	}



	/**
	 *   Output error as XML
	 *   @param string $errorMessage Error message
	 *   @param boolean|array $stackTrace Stack trace (or FALSE if none)
	 *   @return void
	 */

	function outputErrorXML( $errorMessage, $stackTrace=FALSE )
	{
		header('Content-type: text/xml; charset=UTF-8');
		echo '<?xml version="1.0" encoding="UTF-8"?>';
		echo '<response>';
		echo '<status>2</status>';
		echo '<error>FATAL ERROR: '.htmlspecialchars($errorMessage).'</error>';
		if ($stackTrace!==FALSE)
		{
			echo '<stacktrace>';
			echo '<line>'.join('</line><line>',$stackTrace).'</line>';
			echo '</stacktrace>';
		}
		echo '</response>';
		echo "\n";
	}



	/**
	 *   Output error as HTML
	 *   @param string $errorMessage Error message
	 *   @param boolean|array $stackTrace Stack trace (or FALSE if none)
	 *   @return void
	 */

	function outputErrorHTML( $errorMessage, $stackTrace=FALSE )
	{
		header('Content-type: text/html; charset=UTF-8');
		echo '<html><head><title>FATAL ERROR</title></head><body>';
		echo '<h1>FATAL ERROR</h1>';
		echo '<style> body { padding: 20px; font-family: Arial, Helvetica; font-size: 15px; line-height: 18px; } h1 { font-size: 48px; font-weight: normal; padding: 15px 0px 15px 0px; border-bottom: 2px #D3C64B solid; } ul { list-style-type: square; } li { color: #666666; } </style>';
		echo '<p>'.htmlspecialchars($errorMessage).'</p>';
		if ($stackTrace!==FALSE)
		{
			echo '<ul><li>'.join('</li><li>',$stackTrace).'</li></ul>';
		}
		echo '</body></html>';
	}



	/**
	 *   Output error as plain text
	 *   @param string $errorMessage Error message
	 *   @param boolean|array $stackTrace Stack trace (or FALSE if none)
	 *   @return void
	 */

	function outputErrorPlain( $errorMessage, $stackTrace=FALSE )
	{
		echo "FATAL ERROR: ".$errorMessage."\n";
		if ($stackTrace!==FALSE)
		{
			echo "Stack trace:\n";
			echo join("\n",$stackTrace)."\n";
		}
		echo "\n";
	}


?>