<?


	/**
	 *
	 *   LabEngine™ 7
	 *   Base actions: JS / locale
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class JS_LOCALE extends BASE_DISPLAYOBJECT
	{

		function display()
		{
			global $LAB;

			// Set response type
			$LAB->RESPONSE->setType('raw','application/javascript; charset=UTF-8');

			// Load
			if (!empty($LAB->REQUEST->requestVar['var_2']['name']) && $LAB->LOCALE->localeExists($LAB->REQUEST->requestVar['var_2']['name']))
			{
				$lang=$LAB->REQUEST->requestVar['var_2']['name'];
			}
			elseif ($LAB->SESSION->get('LANG')!='')
			{
				$lang=$LAB->SESSION->get('LANG');
			}
			else
			{
				$lang=$LAB->REQUEST->requestLang;
			}
			$LAB->LOCALE->loadLocale($lang);

			// Create object
			$c  = "LOCALE = new Object();\n";
			$c .= "LOCALE.get = function(tag) { return LOCALE.data[tag.toLowerCase()]; }\n";
			$c .= "LOCALE.tag = new String('".$LAB->LOCALE->localeLanguage."');\n";
			$c .= "LOCALE.data = {";
			$i=0;
			foreach ($LAB->LOCALE->localeData as $key => $value)
			{
				$c .= ($i?",\n":"\n").'"'.$key.'":"'.str_replace('"','\"',$value).'"';
				$i++;
			}
			$c .= "\n}\n\n";

			// Datepicker
			// TODO: finish
			$c .= "LOCALE.datepicker = new String('".$LAB->DATETIME->getDateFormat('datepicker')."');\n";

			$c .= "LOCALE.decimalseparator = '".$LAB->REGIONAL->REGIONAL_decimalseparator."';\n";
			$c .= "LOCALE.currencyPlacement = '".$LAB->REGIONAL->REGIONAL_currencyplacement."';\n";
			//$c .= "LOCALE.currency = new String('".."')";
			
			$c .= "LOCALE.formatDecimalValue = function( value, precision, trim )
						 {
						 		var retval=parseFloat(value);
						 		if (precision!=null) {
						 			if (precision>0) {
						 				retval=sprintf('%.0'+precision+'f',retval);
						 			} else {
						 				retval=Math.floor(retval);
						 			}
						 		}
						 		retval=retval.toString().replace('.',LOCALE.decimalseparator);
						 		if (trim!=null && trim==true) {
						 			retval=retval.replace(/[0]+$/,'');
						 			if (retval.substring(retval.length-1)==LOCALE.decimalseparator) {
						 				retval=retval.substring(0,retval.length-1);
						 			}
						 		}
						 		return retval;
						 };";
			
			/*$c .= "LOCALE.formatCurrency = function( value, currency )
						{
							var formattedValue = LOCALE.formatDecimalValue();
						};";*/
			/*
			$c.='$(document).ready(function(){'."\n";
			$c.='  $.datepicker.setDefaults($.datepicker.regional["'.$LAB->REQUEST->requestLang.'"]);'."\n";
			$c.='  $.datepicker.setDefaults({dateFormat:"'.$LAB->DATE->getDateFormat('datepicker').'"});'."\n";
			$c.='});'."\n";
			*/

			// Return
			return $c;
		}

	}


	$JS_LOCALE=new JS_LOCALE();


?>