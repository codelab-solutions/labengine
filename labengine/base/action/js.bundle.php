<?


	/**
	 *
	 *   LabEngine™ 7
	 *   Base actions: JS / JS bundle
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class JS_BUNDLE extends BASE_DISPLAYOBJECT
	{

		function display()
		{
			global $LAB;
			try
			{
				// Check
				$fileName=$LAB->REQUEST->requestVar['var_2']['name'];
				if (!mb_strlen($fileName)) throw new Exception('Invalid parameters.');
				$fileName=BASE::getTmpDir().'/'.$LAB->CONFIG->get('site.id').'.asset.js.'.$fileName;
				if (!file_exists($fileName)) throw new Exception('Invalid parameters.');

				// Set type
				$LAB->RESPONSE->setType('raw','application/javascript; charset=UTF-8');
				$LAB->RESPONSE->setExpires(strtotime('+10year'));
				$LAB->RESPONSE->setLastModified(filemtime($fileName));
				return file_get_contents($fileName);
			}
			catch (Exception $e)
			{
				$LAB->RESPONSE->setType('raw','text/plain');
				return 'ERROR: '.$e->getMessage();
			}
		}

	}


	$JS_BUNDLE=new JS_BUNDLE();


?>