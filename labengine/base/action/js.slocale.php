<?


	/**
	 *
	 *   LabEngine™ 7
	 *   Base actions: JS / short/simple locale
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	class JS_SLOCALE extends BASE_DISPLAYOBJECT
	{


		public static $SET_short_locale=array(
			'BASE.COMMON',
			'BASE.LIST',
			'BASE.DIALOG',
			'BASE.FORM',
		);


		function display()
		{
			global $LAB;

			// Set response type
			$LAB->RESPONSE->setType('raw','application/javascript; charset=UTF-8');

			// Load
			if (!empty($LAB->REQUEST->requestVar['var_2']['name']) && $LAB->LOCALE->localeExists($LAB->REQUEST->requestVar['var_2']['name']))
			{
				$lang=$LAB->REQUEST->requestVar['var_2']['name'];
			}
			elseif ($LAB->SESSION->get('LANG')!='')
			{
				$lang=$LAB->SESSION->get('LANG');
			}
			else
			{
				$lang=$LAB->REQUEST->requestLang;
			}
			$LAB->LOCALE->loadLocale($lang);

			// Create object
			$c  = "LOCALE = new Object();\n";
			$c .= "LOCALE.get = function(tag) { return LOCALE.data[tag.toLowerCase()]; }\n";
			$c .= "LOCALE.tag = new String('".$LAB->LOCALE->localeLanguage."');\n";
			$c .= "LOCALE.data = {";
			$i=0;
			foreach ($LAB->LOCALE->localeData as $key => $value)
			{
				// Is in the short locale?
				$isInShortLocale=false;
				foreach (static::$SET_short_locale as $slTag)
				{
					if (!strncasecmp($slTag,$key,mb_strlen($slTag)))
					{
						$isInShortLocale=true;
						break;
					}
				}
				if (!$isInShortLocale) continue;

				$c .= ($i?",\n":"\n").'"'.$key.'":"'.str_replace('"','\"',$value).'"';
				$i++;
			}
			$c .= "\n}\n\n";

			// Datepicker
			// TODO: finish
			$c .= "LOCALE.datepicker = new String('dd.mm.yyyy');\n";
			/*
			$c .= "LOCALE.datepicker = new String('".$LAB->DATETIME->getDateFormat('datepicker')."');\n";
			$c.='$(document).ready(function(){'."\n";
			$c.='  $.datepicker.setDefaults($.datepicker.regional["'.$LAB->REQUEST->requestLang.'"]);'."\n";
			$c.='  $.datepicker.setDefaults({dateFormat:"'.$LAB->DATE->getDateFormat('datepicker').'"});'."\n";
			$c.='});'."\n";
			*/

			// Return
			return $c;
		}

	}


	$JS_SLOCALE=new JS_SLOCALE();


?>