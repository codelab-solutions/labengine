<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   Base controllers: CSS
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	// Init controller
	$CSS=new BASE_CONTROLLEROBJECT();
	$CSS->setLoginRequired(false);

	//
	//  Actions
	//

	$a=$CSS->addAction('bundle');
	$a->setInclude('base/action/css.bundle.php');
	$a->setHandler('CSS_BUNDLE');


?>