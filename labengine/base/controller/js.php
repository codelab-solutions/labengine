<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   Base controllers: JS
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	// Init controller
	$JS=new BASE_CONTROLLEROBJECT();
	$JS->setLoginRequired(false);

	//
	//  Actions
	//

	$a=$JS->addAction('bundle');
	$a->setInclude('base/action/js.bundle.php');
	$a->setHandler('JS_BUNDLE');

	$a=$JS->addAction('slocale');
	$a->setInclude('base/action/js.slocale.php');
	$a->setHandler('JS_SLOCALE');

	$a=$JS->addAction('locale');
	$a->setInclude('base/action/js.locale.php');
	$a->setHandler('JS_LOCALE');


?>