
//
//
//	LabEngine™ 7
//  2014 (c) Codelab Solutions OÜ <codelab@codelab.ee>
//
//

$(function(){

	// Set default jQuery ajax options
	$.ajaxSetup({
		type:     "post",
		dataType: "json"
	});

	// Init tooltips
	$('[data-toggle="tooltip"]').tooltip();

	// Tab switch
	var URL = document.location.toString();
	if (URL.match(/#/))
	{
		var anchor = URL.split('#')[1];
		if ($("#content_"+anchor).length)
		{
			TAB.select(anchor);
		}
	}

	// We can remove that
	$('noscript').remove();

	// Default focus
	if ($(".defaultfocus").length)
	{
		$(".defaultfocus")[0].focus();
	}

});


//
//  Helper funcs
//

function str_repeat(i, m)
{
	for (var o = []; m > 0; o[--m] = i);
	return o.join('');
}

function sprintf()
{
	var i = 0, a, f = arguments[i++], o = [], m, p, c, x, s = '';
	while (f) {
		if (m = /^[^\x25]+/.exec(f)) {
			o.push(m[0]);
		}
		else if (m = /^\x25{2}/.exec(f)) {
			o.push('%');
		}
		else if (m = /^\x25(?:(\d+)\$)?(\+)?(0|'[^$])?(-)?(\d+)?(?:\.(\d+))?([b-fosuxX])/.exec(f)) {
			if (((a = arguments[m[1] || i++]) == null) || (a == undefined)) {
				throw('Too few arguments.');
			}
			if (/[^s]/.test(m[7]) && (typeof(a) != 'number')) {
				throw('Expecting number but found ' + typeof(a));
			}
			switch (m[7]) {
				case 'b': a = a.toString(2); break;
				case 'c': a = String.fromCharCode(a); break;
				case 'd': a = parseInt(a); break;
				case 'e': a = m[6] ? a.toExponential(m[6]) : a.toExponential(); break;
				case 'f': a = m[6] ? parseFloat(a).toFixed(m[6]) : parseFloat(a); break;
				case 'o': a = a.toString(8); break;
				case 's': a = ((a = String(a)) && m[6] ? a.substring(0, m[6]) : a); break;
				case 'u': a = Math.abs(a); break;
				case 'x': a = a.toString(16); break;
				case 'X': a = a.toString(16).toUpperCase(); break;
			}
			a = (/[def]/.test(m[7]) && m[2] && a >= 0 ? '+'+ a : a);
			c = m[3] ? m[3] == '0' ? '0' : m[3].charAt(1) : ' ';
			x = m[5] - String(a).length - s.length;
			p = m[5] ? str_repeat(c, x) : '';
			o.push(s + (m[4] ? a + p : p + a));
		}
		else {
			throw('Huh ?!');
		}
		f = f.substring(m[0].length);
	}
	return o.join('');
}

function sortObject(object)
{
	var newArr = new Array();
	var last = "";
	for (n in object)
	{
		last = "";
		for (i in object)
		{
			if ((last=='' || object[i] < last) && !newArr[i])
			{
				last = i;
			}
		}
		newArr[last] = object[last];
	}
	return newArr;
}

function var_dump(obj)
{
	var out = typeof(obj) + ": \n";
	for (var i in obj) {
		out += i + ": " + obj[i] + "\n";
	}
	return out;
}

function oneof()
{
	for (var i = 0; i < arguments.length; i++)
	{
		if (arguments[i]!=null && arguments[i]!='' && arguments[i]!=0) return arguments[i];
	}
	return arguments[arguments.length];
}

function htmlspecialchars(text)
{
  return text
	.toString()
	.replace(/&/g, "&amp;")
  .replace(/</g, "&lt;")
  .replace(/>/g, "&gt;")
  .replace(/"/g, "&quot;")
  .replace(/'/g, "&#039;");
}

Number.prototype.formatNumber = function(c, d, t)
{
	var n = this,
	c = isNaN(c = Math.abs(c)) ? 2 : c,
	d = d == undefined ? "," : d,
	t = t == undefined ? " " : t,
	s = n < 0 ? "-" : "",
	i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
	j = (j = i.length) > 3 ? j % 3 : 0;
	return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};



//
//  JavaScript hacks & additions
//

Math.stdRound = Math.round;
Math.round = function(number, precision)
{
	if (isNaN(number)) return 0;
	if (precision!=null)
	{
		precision = Math.abs(parseInt(precision)) || 0;
		var coefficient = Math.pow(10, precision);
		return Math.stdRound(number*coefficient)/coefficient;
	}
	else
	{
		return Math.stdRound(number);
	}
};

Object.elementCount = function(obj)
{
	var size = 0, key;
	for (key in obj) {
		if (obj.hasOwnProperty(key)) size++;
	}
	return size;
};


//
//  Base64 encode/decode
//

var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9\+\/\=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/\r\n/g,"\n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}



//
//  Standard LabEngine functions
//

BASE = {};

BASE.target_data = {};

BASE.redirect = function( url )
{
	window.location.href=url;
};

BASE.reload = function()
{
	window.location.reload(true);
};

BASE.createDialog = function ( title, buttons, width, dialog_tag, dialog_class )
{
	// Create dialog tag
	if (dialog_tag==null)
	{
		var dialog_tag='dialog_' + Math.floor(Math.random()*100000+1);
	}

	// Width
	if (width!=null)
	{
		width=parseInt(width);
	}

	// No resize
	if (dialog_class!=null && dialog_class!='')
	{
		var dialog_class='modal '+dialog_class;
	}
	else
	{
		var dialog_class='modal';
	}

	// Create html
	var         dialog_html='<div id="'+dialog_tag+'" class="'+dialog_class+'" tabindex="-1">';
	dialog_html=dialog_html+'<div class="modal-dialog">';
	dialog_html=dialog_html+'<div class="modal-content">';
	if (!(title!=null && title==false))
	{
		dialog_html=dialog_html+'<div class="modal-header">';
		dialog_html=dialog_html+'<button type="button" id="'+dialog_tag+'_close" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">'+LOCALE.get('BASE.DIALOG.CloseWindow')+'</span></button>';
		dialog_html=dialog_html+'<h4 class="modal-title">'+title+'</h4>';
		dialog_html=dialog_html+'</div>';
	}
	dialog_html=dialog_html+'<div class="modal-body"><div class="modal-body-content"></div></div>';
	if (!(buttons!=null && buttons==false))
	{
		dialog_html=dialog_html+'<div class="modal-footer"></div>';
	}
	dialog_html=dialog_html+'</div>';
	dialog_html=dialog_html+'</div>';
	dialog_html=dialog_html+'</div>';
	$('body').append(dialog_html);

	// Set standard events
	dialog_tag=dialog_tag;
	$('#'+dialog_tag).on('show.bs.modal',function(){
		$('#'+dialog_tag).css('display','block');
		if (width!=null && width>0)
		{
			if ($(window).width()>(width+80))
			{
				$('#'+dialog_tag+' .modal-dialog').css('width',width+'px');
			}
			else if ($(window).width()>767)
			{
				$('#'+dialog_tag+' .modal-dialog').css('width','90%');
			}
		}
		BASE.initModal(dialog_tag);
	});
	$('#'+dialog_tag).on('hidden.bs.modal',function(){
		$('#'+dialog_tag).remove();
	});

	// Return dialog ID
	return dialog_tag;
};

BASE.closeDialog = function( context )
{
	if (context!=null)
	{
		$(context).closest('.modal').modal('hide');
	}
	else
	{
		$('.modal:visible').modal('hide');
	}
};

BASE.centerModal = function ( dialog_tag )
{
	if ($('#'+dialog_tag).hasClass('nocenter')) return;
	if ($(window).width()<768)
	{
		return;
	}
	if (dialog_tag!=null)
	{
		var dialog=$('#'+dialog_tag+' .modal-dialog');
		var offset=Math.round(($(window).height()-dialog.height())/2);
		dialog.css('margin-top',(offset>40?offset:''));
	}
	else
	{
		$('.modal-dialog').each(function(){
			var dialog=$(this);
			var offset=Math.round(($(window).height()-dialog.height())/2);
			dialog.css('margin-top',(offset>40?offset:''));
		});
	}
};

BASE.resizeContent = function ( dialog_tag )
{
	// No resize
	if ($('#'+dialog_tag).hasClass('noresize')) return;

	// Window height
	var window_height=$(window).height();

	// Reset
	$('#'+dialog_tag+' .modal-body-content').css('display','block');
	$('#'+dialog_tag+' .modal-body-content').css('height','');
	$('#'+dialog_tag+' .modal-body-content').css('padding-right','');
	$('#'+dialog_tag+' .modal-body-content').css('overflow-y','');

	// Calc content height
	var content_height=$('#'+dialog_tag+' .modal-body-content').height();
	var max_content_height=window_height-250;

	// Set scrolling if needed
	if (content_height>max_content_height)
	{
		$('#'+dialog_tag+' .modal-body-content').css('display','block');
		$('#'+dialog_tag+' .modal-body-content').css('height',max_content_height+'px');
		$('#'+dialog_tag+' .modal-body-content').css('padding-right','30px');
		$('#'+dialog_tag+' .modal-body-content').css('overflow-y','auto');
	}
};

BASE.initModal = function( dialog_tag )
{
	BASE.resizeContent(dialog_tag);
	BASE.centerModal(dialog_tag);
};

BASE.getDialogTag = function( context )
{
	if ($(context).closest('.modal').length)
	{
		return $(context).closest('.modal').attr('id');
	}
	else
	{
		return false;
	}
};

BASE.alert = function ( message, title, reloadonclose, redirectonclose, width )
{
	if (title==null || !title.length)
	{
		title=LOCALE.get('BASE.DIALOG.Title.Message');
	}
	var dialog_tag=BASE.createDialog(title,true,width);
	$('#'+dialog_tag+' .modal-body-content').html('<div style="margin: 0px">'+message+'</div>');
	$('#'+dialog_tag).on('hidden.bs.modal',function(){
		if (reloadonclose!=null && reloadonclose==true)
		{
			BASE.reload();
		}
		if (redirectonclose!=null && redirectonclose.length>0)
		{
			BASE.redirect(redirectonclose);
		}
	});
	$('#'+dialog_tag+' .modal-footer').append('<button type="button" class="btn btn-default btn-primary" data-dismiss="modal">'+LOCALE.get('BASE.COMMON.OK')+'</button>');
	$('#'+dialog_tag).modal({
		backdrop: 'static',
		keyboard: true,
		show: true
	});
};

BASE.confirmDialog = function ( message, title , buttons, width )
{
	if (title==null || !title.length)
	{
		title=LOCALE.get('BASE.DIALOG.Title.Choose');
	}
	var dialog_tag=BASE.createDialog(title,true,width);
	$('#'+dialog_tag+' .modal-body-content').html('<div style="margin: 0px">'+message+'</div>');
	for (t in buttons)
	{
		$('#'+dialog_tag+' .modal-footer').append('<button type="button" id="'+dialog_tag+'_btn_'+t+'" class="btn btn-default'+(buttons[t].class!=null?' '+buttons[t].class:'')+'">'+buttons[t].title+'</button>');
		$('#'+dialog_tag+'_btn_'+t).on('click',buttons[t].callback);
		if (buttons[t].cancel!=null && buttons[t].cancel==true)
		{
			$('#'+dialog_tag+'_close').on('click',buttons[t].callback);
		}
	}
	$('#'+dialog_tag).modal({
		backdrop: 'static',
		keyboard: true,
		show: true
	});
};

BASE.displayErrors = function ( message, title, errors )
{
	var fieldErrors=false;
	var firstField=null;
	if (typeof errors!=='undefined')
	{
		fieldErrors=true;
		for (var formElementID in errors)
		{
			if (firstField==null) firstField=formElementID;
			$('#'+formElementID).addClass('error');
		}
	}
	if (title==null || !title.length)
	{
		title=LOCALE.get('BASE.DIALOG.Title.Message');
	}
	BASE.alert(message,title);
};

BASE.openDialog = function ( content_url, content_data, dialog_width, buttons, close_button_label, dialog_class )
{
	if (dialog_width==null) dialog_width=600;
	if (buttons!=null && buttons==false)
	{
		var dialog_tag=BASE.createDialog('&nbsp;',false,dialog_width,null,dialog_class);
	}
	else
	{
		var dialog_tag=BASE.createDialog('&nbsp;',true,dialog_width,null,dialog_class);
	}
	if (content_data==null)
	{
		content_data={'tag':dialog_tag};
	}
	else
	{
		content_data.tag=dialog_tag;
	}
	$.ajax({
		url: content_url,
		data: content_data,
		success: function( data )
		{
			if (data!=null && data.status=='1')
			{
				$('#'+dialog_tag+' .modal-body-content').html(data.content);
				if (data.title!=null)
				{
					$('#'+dialog_tag+' .modal-title').text(data.title);
				}
				else
				{
					$('#'+dialog_tag+' .modal-title').text(LOCALE.get('BASE.DIALOG.Title.Message'));
				}

				if (buttons!=null)
				{
					if (buttons!=false)
					{
						for (t in buttons)
						{
							$('#'+dialog_tag+' .modal-footer').append('<button type="button" id="'+(buttons[t].id!=null?buttons[t].id:dialog_tag+'_btn_'+t)+'" class="btn btn-default'+(buttons[t].class!=null?' '+buttons[t].class:'')+'"'+((buttons[t].disabled!=null && buttons[t].disabled==1)?' disabled="disabled"':'')+'>'+buttons[t].title+'</button>');
							$('#'+(buttons[t].id!=null?buttons[t].id:dialog_tag+'_btn_'+t)).on('click',buttons[t].callback);
							if (buttons[t].cancel!=null && buttons[t].cancel==true)
							{
								$('#'+dialog_tag+'_close').on('click',buttons[t].callback);
							}
						}
						if (close_button_label==null || close_button_label!==false)
						{
							$('#'+dialog_tag+' .modal-footer').append('<button type="button" class="btn btn-default" data-dismiss="modal">'+oneof(close_button_label,LOCALE.get('BASE.FORM.Btn.Cancel'))+'</button>');
						}
					}
				}
				else
				{
					$('#'+dialog_tag+' .modal-footer').append('<button type="button" class="btn btn-default" data-dismiss="modal">'+oneof(close_button_label,LOCALE.get('BASE.COMMON.OK'))+'</button>');
				}

				$('#'+dialog_tag).modal({
					backdrop: 'static',
					keyboard: true,
					show: true
				});
				if (data.displaytrigger!=null && data.displaytrigger!="")
				{
					eval(data.displaytrigger);
				}
				if ($("#"+dialog_tag+" .modal-footer button.btn-primary").length==1)
				{
					$("#form_"+dialog_tag+" input, #form_"+dialog_tag+" select").not('.noautosubmit').keypress(function(e){
						if (e.which==13)
						{
							$("#"+dialog_tag+" .modal-footer button.btn-primary").trigger('click');
						}
					});
				}
				if ($("#"+dialog_tag+" .modal-body-content .defaultfocus").length)
				{
					$("#"+dialog_tag+" .modal-body-content .defaultfocus")[0].focus();
				}
				else
				{
					if ($("#"+dialog_tag+" .modal-body-content :input:visible").length)
					{
						$("#"+dialog_tag+" .modal-body-content :input:visible").first().focus();
					}
				}
				FORM.initElements("#form_"+dialog_tag);
			}
			else
			{
				if (data!=null && data.error!=null)
				{
					BASE.alert(data.error,LOCALE.get('BASE.COMMON.Error'));
				}
				else
				{
					BASE.alert(LOCALE.get('BASE.FORM.ERROR.ErrorOpeningDialog'),LOCALE.get('BASE.COMMON.Error'));
				}
				return;
			}
		},
		error: function()
		{
			BASE.alert(LOCALE.get('BASE.FORM.ERROR.ErrorOpeningDialog'),LOCALE.get('BASE.COMMON.Error'));
			return null;
		}
	});
	return dialog_tag;
};

BASE.openEditDialog = function ( content_url, content_data, dialog_width, extra_buttons, no_standard_save, dialog_class )
{
	if (dialog_width==null)
	{
		dialog_width=850;
	}
	var dialog_tag=BASE.createDialog('&nbsp;',true,dialog_width,null,dialog_class);

	if (content_data==null)
	{
		content_data={'tag':dialog_tag};
	}
	else
	{
		content_data.tag=dialog_tag;
	}

	$.ajax({
		url: content_url,
		data: content_data,
		success: function( data )
		{
			if (data!=null && data.status=='1')
			{
				$('#'+dialog_tag+' .modal-body-content').html(data.content);
				$('#'+dialog_tag+' .modal-title').text(data.title);

				var submitbuttontitle = LOCALE.get('BASE.FORM.Btn.Save');
				if (data.submit!=null && data.submit!="")
				{
					submitbuttontitle = data.submit;
				}
				var progressmessage = LOCALE.get('BASE.FORM.Msg.Saving');
				if (data.progressmessage!=null && data.progressmessage!="")
				{
					progressmessage = data.progressmessage;
				}
				var submithandler = null;
				if (data.submithandler!=null && data.submithandler!="")
				{
					submithandler = data.submithandler;
				}

				if (no_standard_save==null || no_standard_save==false)
				{
					$('#'+dialog_tag+' .modal-footer').append('<button type="button" id="'+dialog_tag+'_btn_submit" class="btn btn-primary">'+submitbuttontitle+'</button>');
					$('#'+dialog_tag+'_btn_submit').on('click',function(){
						if ($("#form_"+dialog_tag)[0].action==null || $("#form_"+dialog_tag)[0].action=="")
						{
							BASE.alert(LOCALE.get('BASE.FORM.Error.CouldNotFindForm'),LOCALE.get('BASE.COMMON.Error'));
							return;
						}
						$("#form_"+dialog_tag+" td.label.error").removeClass('error');

						// Custom submithandler?
						if (submithandler!=null)
						{
							eval(submithandler+'(\''+dialog_tag+'\')');
						}

						// Submit
						else
						{
							// Progress
							if (progressmessage!='')
							{
								BASE.progressDialog(progressmessage);
							}

							// Do submit
							$("#form_"+dialog_tag).ajaxSubmit({
								type:     "post",
								data: { ajaxsubmit: '1' },
								success: function( data )
								{
									if (data!=null && data.status=='1')
									{
										if (data.message!=null && data.message!='')
										{
											BASE.closeProgressDialog();
											BASE.closeDialog('#'+dialog_tag);
											if (data.redirect!=null && data.redirect!='')
											{
												BASE.alert(data.message,null,null,data.redirect);
											}
											else if (data.reload!=null && data.reload=='1')
											{
												BASE.alert(data.message,null,true);
											}
										}
										else if (data.redirect!=null && data.redirect!='')
										{
											BASE.closeDialog('#'+dialog_tag);
											BASE.redirect(data.redirect);
										}
										else if (data.reload!=null && data.reload=='1')
										{
											BASE.closeDialog('#'+dialog_tag);
											BASE.reload();
										}
										else
										{
											BASE.closeProgressDialog();
											if (data.submitsuccessaction)
											{
												eval(data.submitsuccessaction);
											}
											BASE.closeDialog('#'+dialog_tag);
										}
									}
									else
									{
										BASE.closeProgressDialog();
										var errors='<b>'+LOCALE.get('BASE.FORM.Error.Errors')+':</b>';
										if (data!=null && data.error!=null)
										{
											if (typeof(data.error)=='object')
											{
												for(var fld_tag in data.error)
												{
													if(data.error.hasOwnProperty(fld_tag))
													{
														errors=errors+'<br/> &#0149; '+data.error[fld_tag];
													}
												}
												BASE.displayErrors(errors,LOCALE.get('BASE.COMMON.Error'),data.error);
											}
											else
											{
												BASE.alert(data.error,LOCALE.get('BASE.COMMON.Error'));
											}
										}
										else
										{
											BASE.alert(LOCALE.get('BASE.COMMON.ErrorSavingData')+': 1',LOCALE.get('BASE.COMMON.Error'));
										}
										return;
									}
								},
								error: function(xhr, ajaxOptions, thrownError)
								{
									BASE.closeProgressDialog();
									BASE.alert(LOCALE.get('BASE.COMMON.ErrorSavingData')+': '+thrownError+' / '+var_dump(xhr),LOCALE.get('BASE.COMMON.Error'));
									return;
								}
							});
						}
					});
				}
				if (extra_buttons!=null)
				{
					for (var t in extra_buttons)
					{
						$('#'+dialog_tag+' .modal-footer').append('<button type="button" id="'+dialog_tag+'_btn_'+t+'" class="btn'+(buttons[t].class!=null?' '+buttons[t].class:'')+'">'+buttons[t].title+'</button>');
						$('#'+dialog_tag+'_btn_'+t).on('click',buttons[t].callback);
						if (buttons[t].cancel!=null && buttons[t].cancel==true)
						{
							$('#'+dialog_tag+'_close').on('click',buttons[t].callback);
						}
					}
				}
				$('#'+dialog_tag+' .modal-footer').append('<button type="button" class="btn btn-default" data-dismiss="modal">'+LOCALE.get('BASE.FORM.Btn.Cancel')+'</button>');

				$('#'+dialog_tag).modal({
					backdrop: 'static',
					keyboard: true,
					show: true
				});
				if (data.displaytrigger!=null && data.displaytrigger!="")
				{
					eval(data.displaytrigger);
				}
				if ($("#"+dialog_tag+" .modal-footer button.btn-primary").length==1)
				{
					$("#form_"+dialog_tag+" input, #form_"+dialog_tag+" select").not('.noautosubmit').keypress(function(e){
						if (e.which==13)
						{
							$("#"+dialog_tag+" .modal-footer button.btn-primary").trigger('click');
						}
					});
				}
				if ($("#form_"+dialog_tag+" .defaultfocus").length)
				{
					$("#form_"+dialog_tag+" .defaultfocus")[0].focus();
				}
				else
				{
					if ($("#form_"+dialog_tag+" :input:visible").length)
					{
						$("#form_"+dialog_tag+" :input:visible").first().focus();
					}
				}
				FORM.initElements("#form_"+dialog_tag);
			}
			else
			{
				if (data!=null && data.error!=null)
				{
					if (typeof(data.error)=='object')
					{
						BASE.displayErrors(data.error,LOCALE.get('BASE.COMMON.Error'),data.error);
					}
					else
					{
						BASE.alert(data.error,LOCALE.get('BASE.COMMON.Error'));
					}
				}
				else
				{
					BASE.alert(LOCALE.get('BASE.FORM.ERROR.ErrorOpeningDialog'),LOCALE.get('BASE.COMMON.Error'));
				}
				return;
			}
		},
		error: function()
		{
			BASE.alert(LOCALE.get('BASE.FORM.ERROR.ErrorOpeningDialog'),LOCALE.get('BASE.COMMON.Error'));
			return null;
		}
	});
	return dialog_tag;
};

BASE.openIntermediaryDialog = function ( content_url, content_data, target_url, target_data, dialog_width, dialog_class )
{
	if (dialog_width==null)
	{
		dialog_width=850;
	}
	var dialog_tag=BASE.createDialog('&nbsp;',true,dialog_width,null,dialog_class);
	var target_data=target_data;


	if (content_data==null)
	{
		content_data={'tag':dialog_tag};
	}
	else
	{
		content_data.tag=dialog_tag;
	}

	$.ajax({
		url: content_url,
		data: content_data,
		success: function( data )
		{
			if (data!=null && data.status=='1')
			{
				$('#'+dialog_tag+' .modal-body-content').html(data.content);
				$('#'+dialog_tag+' .modal-title').text(data.title);

				var submitbuttontitle = LOCALE.get('BASE.FORM.Btn.Save');
				if (data.submit!=null && data.submit!="")
				{
					submitbuttontitle = data.submit;
				}
				var progressmessage = LOCALE.get('BASE.FORM.Msg.Saving');
				if (data.progressmessage!=null && data.progressmessage!="")
				{
					progressmessage = data.progressmessage;
				}
				var submithandler = null;
				if (data.submithandler!=null && data.submithandler!="")
				{
					submithandler = data.submithandler;
				}

				$('#'+dialog_tag+' .modal-footer').append('<button type="button" id="'+dialog_tag+'_btn_submit" class="btn btn-primary">'+submitbuttontitle+'</button>');
				$('#'+dialog_tag+'_btn_submit').on('click',function(){
					if ($("#form_"+dialog_tag)[0].action==null || $("#form_"+dialog_tag)[0].action=="")
					{
						BASE.alert(LOCALE.get('BASE.FORM.Error.CouldNotFindForm'),LOCALE.get('BASE.COMMON.Error'));
						return;
					}
					$("#form_"+dialog_tag+" td.label.error").removeClass('error');

					// Custom submithandler?
					if (submithandler!=null)
					{
						eval(submithandler+'(\''+dialog_tag+'\')');
					}

					// Submit
					else
					{
						// Progress
						if (progressmessage!='')
						{
							BASE.progressDialog(progressmessage);
						}

						// Do submit
						$("#form_"+dialog_tag).ajaxSubmit({
							type:     "post",
							data: { ajaxsubmit: '1' },
							success: function( data )
							{
								if (data!=null 	&& data.status=='1')
								{
									$('input[name=ajaxsubmit]').remove();
									$.each($("#form_"+dialog_tag).serializeArray(),function(_,kv){
										target_data[kv.name]=kv.value;
									});
									BASE.closeProgressDialog();
									BASE.closeDialog('#'+dialog_tag);
									BASE.openEditDialog(target_url,target_data);
								}
								else
								{
									BASE.closeProgressDialog();
									var errors='<b>'+LOCALE.get('BASE.FORM.Error.Errors')+':</b>';
									if (data!=null && data.error!=null)
									{
										if (typeof(data.error)=='object')
										{
											for(var fld_tag in data.error)
											{
												if(data.error.hasOwnProperty(fld_tag))
												{
													errors=errors+'<br/> &#0149; '+data.error[fld_tag];
												}
											}
											BASE.displayErrors(errors,LOCALE.get('BASE.COMMON.Error'),data.error);
										}
										else
										{
											BASE.alert(data.error,LOCALE.get('BASE.COMMON.Error'));
										}
									}
									else
									{
										BASE.alert(LOCALE.get('BASE.COMMON.ErrorSavingData')+': 1',LOCALE.get('BASE.COMMON.Error'));
									}
									return;
								}
							},
							error: function(xhr, ajaxOptions, thrownError)
							{
								BASE.closeProgressDialog();
								BASE.alert(LOCALE.get('BASE.COMMON.ErrorSavingData')+': '+thrownError+' / '+var_dump(xhr),LOCALE.get('BASE.COMMON.Error'));
								return;
							}
						});
					}
				});
				$('#'+dialog_tag+' .modal-footer').append('<button type="button" class="btn btn-default" data-dismiss="modal">'+LOCALE.get('BASE.FORM.Btn.Cancel')+'</button>');

				$('#'+dialog_tag).modal({
					backdrop: 'static',
					keyboard: true,
					show: true
				});
				if (data.displaytrigger!=null && data.displaytrigger!="")
				{
					eval(data.displaytrigger);
				}
				if ($("#"+dialog_tag+" .modal-footer button.btn-primary").length==1)
				{
					$("#form_"+dialog_tag+" input, #form_"+dialog_tag+" select").not('.noautosubmit').keypress(function(e){
						if (e.which==13)
						{
							$("#"+dialog_tag+" .modal-footer button.btn-primary").trigger('click');
						}
					});
				}
				if ($("#form_"+dialog_tag+" .defaultfocus").length)
				{
					$("#form_"+dialog_tag+" .defaultfocus")[0].focus();
				}
				else
				{
					if ($("#form_"+dialog_tag+" :input:visible").length)
					{
						$("#form_"+dialog_tag+" :input:visible").first().focus();
					}
				}
				FORM.initElements("#form_"+dialog_tag);
			}
			else
			{
				if (data!=null && data.error!=null)
				{
					if (typeof(data.error)=='object')
					{
						BASE.displayErrors(data.error,LOCALE.get('BASE.COMMON.Error'),data.error);
					}
					else
					{
						BASE.alert(data.error,LOCALE.get('BASE.COMMON.Error'));
					}
				}
				else
				{
					BASE.alert(LOCALE.get('BASE.FORM.ERROR.ErrorOpeningDialog'),LOCALE.get('BASE.COMMON.Error'));
				}
				return;
			}
		},
		error: function()
		{
			BASE.alert(LOCALE.get('BASE.FORM.ERROR.ErrorOpeningDialog'),LOCALE.get('BASE.COMMON.Error'));
			return null;
		}
	});
	return dialog_tag;
};

BASE.progressDialog = function ( message, width )
{
	BASE.createDialog(false,false,width,'lab-progressdialog');
	$('#lab-progressdialog .modal-body-content').html('<div style="margin: 30px 10px; text-align: center">'+message+'</div>');
	$('#lab-progressdialog').modal({
		backdrop: 'static',
		keyboard: true,
		show: true
	});
};

BASE.closeProgressDialog = function ()
{
	if ($("#lab-progressdialog").length>0)
	{
		$("#lab-progressdialog").modal('hide');
	}
};

BASE.doPostSubmit = function ( form_url, form_data, confirm_message, new_window )
{
	if (confirm_message!=null)
	{
		if (!confirm(confirm_message)) return;
	}
	var form_tag=Math.floor(Math.random()*100000+1);
	if (new_window!=null && new_window=='1')
	{
		var target=' target="_blank"';
	}
	else
	{
		var target='';
	}
	$('body').append('<form id="form_'+form_tag+'" method="post" action="'+form_url+'"'+target+'></form>');
	for (var k in form_data)
	{
		$('#form_'+form_tag).append('<input type="hidden" name="'+k+'" value="'+htmlspecialchars(form_data[k])+'" />');
	}
	$('#form_'+form_tag)[0].submit();
};

BASE.doPostSubmitWithOptions = function ( form_url, form_data, title, message, option_key, options, enable_cancel )
{
	if (title==null || title=='')
	{
		title='Dialog';
	}
	if (form_data==null)
	{
		form_data = {};
	}
	var buttons = {};
	$.each(options,function(o,button_title){
		buttons[button_title]={
			buttonAction: function()
			{
				$("#labengine_alert").dialog('close');
				$("#labengine_alert").remove();
				form_data[option_key]=o;
				BASE.doPostSubmit(form_url,form_data);
			},
			buttonClass: "primary"
		}
	});
	if (enable_cancel!=null && enable_cancel==true)
	{
		buttons[LOCALE.get('BASE.FORM.BTN.Cancel')]={
			buttonACtion: function()
			{
				$("#labengine_alert").dialog('close');
				$("#labengine_alert").remove();
			}
		}
	}
	$("#labengine_alert").remove();
	$('body').append('<div id="labengine_alert" title="'+title+'" style="display:none">'+message+'</div>');
	$("#labengine_alert").dialog({
		width: '500px',
		resizable: false,
		bgiframe: true,
		modal: true,
		buttons: buttons
	});
};

BASE.doAjaxAction = function ( action_url, action_data, confirm_message, reload_on_success, progressmessage, reload_on_close )
{
	if (confirm_message!=null)
	{
		BASE.confirmDialog(confirm_message,LOCALE.get('BASE.DIALOG.Title.Confirm'),{
			ok: {
				title: LOCALE.get('BASE.FORM.BTN.OK'),
				class: "btn-primary",
				callback: function(){
					BASE.closeDialog(this);
					BASE.doAjaxAction(action_url,action_data,null,reload_on_success,progressmessage,reload_on_close);
				}
			},
			cancel: {
				title: LOCALE.get('BASE.FORM.BTN.Cancel'),
				cancel: true,
				callback: function(){
					BASE.closeDialog(this);
				}
			}
		});
		return;
	}
	if (progressmessage!=false)
	{
		if (progressmessage==null)
		{
			var progressmessage=LOCALE.get('BASE.FORM.MSG.Saving');
		}
		BASE.progressDialog(progressmessage);
	}
	$.ajax(
		{
			url: action_url,
			data: action_data,
			success: function( data )
			{
				if (data!=null && data.status=='1')
				{
					if (data.redirect!=null)
					{
						BASE.redirect(data.redirect);
					}
					else if ((data.reload!=null && data.reload=='1') || reload_on_success==null || reload_on_success==true)
					{
						BASE.reload();
					}
					else
					{
						if (progressmessage!=false)
						{
							BASE.closeProgressDialog();
						}
						if (data.submitsuccessaction!=null && data.submitsuccessaction.length>0)
						{
							eval(data.submitsuccessaction);
						}
					}
				}
				else
				{
					if (progressmessage!=false)
					{
						BASE.closeProgressDialog();
					}
					if (data!=null && data.error!=null)
					{
						if (reload_on_close==true)
						{
							BASE.alert(data.error,LOCALE.get('BASE.COMMON.Error'),true);
						}
						else
						{
							BASE.alert(data.error,LOCALE.get('BASE.COMMON.Error'));
						}
					}
					else
					{
						if (reload_on_close==true)
						{
							BASE.alert(LOCALE.get('BASE.COMMON.ErrorSavingData'),LOCALE.get('BASE.COMMON.Error'),true);
						}
						else
						{
							BASE.alert(LOCALE.get('BASE.COMMON.ErrorSavingData'),LOCALE.get('BASE.COMMON.Error'));
						}

					}
					return;
				}
			},
			error: function()
			{
				if (progressmessage!=false)
				{
					BASE.closeProgressDialog();
				}
				BASE.alert(LOCALE.get('BASE.COMMON.ErrorSavingData'),LOCALE.get('BASE.COMMON.Error'));
				return;
			}
		});
};

BASE.doAjaxActionWithOptions = function ( action_url, action_data, title, message, option_key, options, enable_cancel, reload_on_success, progressmessage )
{
	if (title==null || title=='')
	{
		title='Dialog';
	}
	if (action_data==null)
	{
		action_data = {};
	}
	var buttons = {};
	$.each(options,function(o,button_title){
		buttons[button_title]={
			buttonAction: function()
			{
				$("#labengine_alert").dialog('close');
				$("#labengine_alert").remove();
				action_data[option_key]=o;
				BASE.doAjaxAction(action_url,action_data,null,reload_on_success, progressmessage);
			},
			buttonClass: "primary"
		}
	});
	if (enable_cancel!=null && enable_cancel==true)
	{
		buttons[LOCALE.get('BASE.FORM.BTN.Cancel')]={
			buttonAction: function()
			{
				$("#labengine_alert").dialog('close');
				$("#labengine_alert").remove();
			}
		}
	}
	$("#labengine_alert").remove();
	$('body').append('<div id="labengine_alert" title="'+title+'" style="display:none">'+message+'</div>');
	$("#labengine_alert").dialog({
		width: '500px',
		resizable: false,
		bgiframe: true,
		modal: true,
		buttons: buttons
	});
};

BASE.stringToURL = function( str )
{
  str = str.replace(/^\s+|\s+$/g, ''); // trim
  str = str.toLowerCase();

  // remove accents, swap ñ for n, etc
  var from = "ãàáäâẽèéëêìíïîõòóöôùúüûñçабвгдеёжзийклмнопрстуфхцчшщъыьэюя·/_,:;";
  var to   = "aaaaaeeeeeiiiiooooouuuuncabvgdeezziyklmnoprstufhccssiyyeua------";
  for (var i=0, l=from.length ; i<l ; i++) {
    str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
  }

  str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
    .replace(/\s+/g, '-') // collapse whitespace and replace by -
    .replace(/-+/g, '-'); // collapse dashes

  return str;
};


//
//  Form functions
//

var FORM = {};


FORM.submit = function( form_action, form_id, success_callback, progressmessage )
{
	// Init
	if (form_action==null)
	{
		form_action='save';
	}
	if (form_id==null)
	{
		form_id='labengine-form';
	}

	// Check
	if ($("#"+form_id)[0].action==null || $("#"+form_id)[0].action=="")
	{
		BASE.alert(LOCALE.get('BASE.FORM.Error.CouldNotFindForm'),LOCALE.get('BASE.COMMON.Error'));
		return;
	}

	// Clear
	$("#"+form_id+" .errors").remove();
	$("#"+form_id+" label.error").removeClass('error');

	// Progress
	if (progressmessage==null)
	{
		progressmessage=LOCALE.get('BASE.FORM.MSG.Saving');
	}
	if (progressmessage!='')
	{
		BASE.progressDialog(progressmessage);
	}

	// Submit
	$("#"+form_id).ajaxSubmit({
		data: {
			ajaxsubmit: '1',
			action: form_action
		},
		success: function( data )
		{
			if (data!=null && data.status=='1')
			{
				if (success_callback!=null)
				{
					BASE.closeProgressDialog();
					success_callback(data);
				}
				else if (data.reload!=null && data.reload=='1')
				{
					BASE.reload();
				}
				else if (data.redirect!=null && data.redirect.length>0)
				{
					BASE.redirect(data.redirect);
				}
				else
				{
					BASE.closeProgressDialog();
				}
			}
			else
			{
				BASE.closeProgressDialog();
				var errors='<b>'+LOCALE.get('BASE.FORM.Error.Errors')+':</b>';
				if (data!=null && data.error!=null)
				{
					if (typeof(data.error)=='object')
					{
						for(var fld_tag in data.error)
						{
							if(data.error.hasOwnProperty(fld_tag))
							{
								errors=errors+'<br/> &#0149; '+data.error[fld_tag];
							}
						}
						BASE.displayErrors(errors,LOCALE.get('BASE.COMMON.Error'),data.error);
					}
					else
					{
						BASE.alert(data.error,LOCALE.get('BASE.COMMON.Error'));
					}
				}
				else
				{
					BASE.alert(LOCALE.get('BASE.COMMON.ErrorSavingData'),LOCALE.get('BASE.COMMON.Error'));
				}
				return;
			}
		},
		error: function(xhr, ajaxOptions, thrownError)
		{
			BASE.closeProgressDialog();
			BASE.alert(LOCALE.get('BASE.COMMON.ErrorSavingData')+' - '+thrownError+' - '+xhr.responseText,LOCALE.get('BASE.COMMON.Error'));
			return;
		}
	});
};

FORM.cancel = function( return_url )
{
	if (return_url!=null)
	{
		BASE.redirect(return_url);
	}
	else
	{
		window.history.go(-1);
	}
};

FORM.initElements = function( base_element )
{
	// Set datepicker
	FORM.setDatePicker(base_element);

	// Set mask
	FORM.setMask(base_element);

	// Set autocomplete
	FORM.setAutoComplete(base_element);
};

FORM.setDatePicker = function( base_element )
{
	$((base_element!=null?base_element+" ":"")+"input.datefield").each(function(){
		$(this).datepicker({
			container: 'body',
			format: $(this).attr('data-date-format'),
			weekStart: (typeof($(this).attr('data-weekstart'))!=='undefined'?parseInt($(this).attr('data-weekstart')):1),
			showOnFocus: false,
			todayHighlight: true,
			autoclose: true
		});
	});
};

FORM.openDatepicker = function( element )
{
	if ($(element+':disabled').length>0) return;
	if ($(element+'[readonly=readonly]').length>0) return;
	$(element).datepicker('show');
	$(element).datepicker().on('changeDate', function (ev) {
		$('.datepicker').hide();
	});
}

FORM.setMask = function ( base_element )
{
	$((base_element!=null?base_element+" ":"")+" input[data-mask!='']").each(function(){
		$(this).mask($(this).attr('data-mask'));
	});
}

FORM.setAutoComplete = function( base_element )
{
	$((base_element!=null?base_element+" ":"")+" input.autocomplete").each(function(){
		var options={};
		options.field_tag=$(this).attr('id');
		if ($(this).attr('data-autocomplete-minlength')!='')
		{
			options.minLength=$(this).attr('data-autocomplete-minlength');
		}
		if ($(this).attr('data-autocomplete-keyvalue')=='1')
		{
			options.key_value=true;
			options.datamap=new Array();
			options.updater=function(item){
				return options.datamap[item];
			};
		}
		else
		{
			options.key_value=false;
		}
		if ($(this).attr('data-autocomplete-sourceurl')!='')
		{
			options.datasource_url=$(this).attr('data-autocomplete-sourceurl');
			options.source=function(query,process){
				$.ajax({
					url: options.datasource_url,
					data: { search: query },
					success: function ( data ) {
						if (data.status!=null && data.status=='1')
						{
							var autocomplete=new Array();
							options.datamap=new Array();
							for (var a in data.autocomplete)
							{
								autocomplete.push(data.autocomplete[a]);
								if (options.key_value==true)
								{
									options.datamap[data.autocomplete[a]]=a;
								}
							}
							process(autocomplete);
						}
					},
					error: function ( data ) {}
				});
      };
		}
    $(this).typeahead(options);
	});
};

FORM.loadOptions = function( select_name, data, selected_option )
{
	var option='';
	for(var key in data)
	{
		var dkey=((key.indexOf("#_") > -1)?key.replace("#_",""):key);
		option+='<option value="'+dkey+'"'+(dkey==selected_option?' selected="selected"':'')+'>'+data[key]+'</option>';
	}
	$(select_name+' option').remove();
	$(select_name).html(option);
};

FORM.setValues = function ( values, base_field )
{
	for (var k in values)
	{
		if (base_field!=null && base_field!='')
		{
			if (base_field=='prop' && k.match(/^prop_/))
			{
				var fld=k;
			}
			else
			{
				var fld=base_field+"_"+k;
			}
		}
		else
		{
			var fld=k;
		}
		if ($("select[name="+fld+"]").length)
		{
			var val=$("select[name="+fld+"]").val();
			if (values[k]!=null && typeof(values[k])=='object')
			{
				FORM.loadOptions("select[name="+fld+"]",sortObject(values[k]),$("select[name="+fld+"]").val());
			}
			else
			{
				$("#"+fld).val(values[k]);
			}
			if (val!=values[k])
			{
				$("#"+fld).trigger('change');
			}
		}
		else if ($("input[name="+fld+"]").attr('type')=='radio')
		{
			//$("#"+fld+"_"+values[k]).attr('checked','checked');
			$("#"+fld+"_"+values[k]).trigger("click");
		}
		else if ($("input[name="+fld+"]").attr('type')=='checkbox')
		{
			if (values[k]=='1')
			{
				$("#"+fld).attr('checked','checked');
			}
			else
			{
				$("#"+fld).removeAttr('checked');
			}
		}
		else
		{
			$("#"+fld).val(values[k]);
		}
	}
};

FORM.toggleFieldGroup = function ( fieldgroup )
{
	if ($("#fieldgroup_"+fieldgroup).is(':visible'))
	{
		$("#fieldgroup_"+fieldgroup).hide();
		$("#header_"+fieldgroup).parent().find('.icon-fieldgroup').removeClass('icon-fieldgroup-open');
		$("#header_"+fieldgroup).parent().find('.icon-fieldgroup').addClass('icon-fieldgroup-closed');
		var hidden='1';
	}
	else
	{
		$("#fieldgroup_"+fieldgroup).show();
		$("#header_"+fieldgroup).parent().find('.icon-fieldgroup').removeClass('icon-fieldgroup-closed');
		$("#header_"+fieldgroup).parent().find('.icon-fieldgroup').addClass('icon-fieldgroup-open');
		var hidden='0';
	}
	$.ajax({
		url : "/ajax/dashboard/module",
		data : "module=fieldgroup_" + encodeURIComponent(fieldgroup) + "&hidden=" + encodeURIComponent(hidden),
		success : function ( data ) {},
		error : function ( data ) {}
	});
};

FORM.toggleShowAll = function()
{
	if ($("#broker_person_oid_showall").is(':checked'))
	{
		$("#broker_person_oid").html($("#broker_person_oid_fulllist").html());
	}
	else
	{
		$("#broker_person_oid").html($("#broker_person_oid_shortlist").html());
	}
};

FORM.toUpperCase = function ( field_tag )
{
	$(field_tag).val($(field_tag).val().toUpperCase());
};

FORM.toLowerCase = function ( field_tag )
{
	$(field_tag).val($(field_tag).val().toLowerCase());
};

FORM.toNameCase = function ( field_tag )
{
	var name=$(field_tag).val();

	// Convert "Name Name"
	var pieces = name.split(" ");
	for ( var i = 0; i < pieces.length; i++ )
	{
		var j = pieces[i].charAt(0).toUpperCase();
		pieces[i] = j + pieces[i].substr(1);
	}
	name=pieces.join(" ");

	// Convert "Name-Name"
	var pieces = name.split("-");
	for ( var i = 0; i < pieces.length; i++ )
	{
		var j = pieces[i].charAt(0).toUpperCase();
		pieces[i] = j + pieces[i].substr(1);
	}
	name=pieces.join("-");

	$(field_tag).val(name);
};

FORM.fixupTextFields = function( dom_base )
{
	if (dom_base==undefined) dom_base='';
	$(dom_base+":text").change(function(){
		// Trim
		this.value = jQuery.trim( this.value );
	});
};

FORM.checklistSelectAll = function ( field_tag )
{
	if ($('input[name="'+field_tag+'[]"]:checked').length<$('input[name="'+field_tag+'[]"]').length)
	{
		$('input[name="'+field_tag+'[]"]').attr('checked','checked').parent().find('span.icon-check-empty').addClass("icon-check");
	}
	else
	{
		$('input[name="'+field_tag+'[]"]').removeAttr('checked').parent().find('span.icon-check-empty').removeClass("icon-check");
	}
};

FORM.showFields = function ( hide_fields, show_fields, field, param_attr, init )
{
	if (field!=null && $(field).length==0) return;
	if (hide_fields!='')
	{
		$(hide_fields).hide();
	}
	if (field!=null)
	{
		if (param_attr!=null)
		{
			if ($(field).get(0).tagName=='SELECT')
			{
				show_fields=show_fields.replace('$val',$(field+' option:selected').attr(param_attr));
				show_fields=show_fields.replace('$empty',(($(field).val().length==0 || $(field).val()=='0')?'empty':'notempty'));
			}
			else
			{
				show_fields=show_fields.replace('$val',$(field).attr(param_attr));
				show_fields=show_fields.replace('$empty',(($(field).attr(param_attr).length==0)?'empty':'notempty'));
			}
		}
		else
		{
			if ($(field).attr('type')=='checkbox')
			{
				show_fields=show_fields.replace('$val',($(field).is(':checked')?'1':'0'));
				show_fields=show_fields.replace('$empty',($(field).is(':checked')?'notempty':'empty'));
			}
			else if ($(field).attr('type')=='radio')
			{
				show_fields=show_fields.replace('$val',$(field+':checked').val());
				show_fields=show_fields.replace('$empty',($(field+':checked').length>0?'notempty':'empty'));
			}
			else
			{
				show_fields=show_fields.replace('$val',$(field).val());
				show_fields=show_fields.replace('$empty',(($(field).val()==null || $(field).val().length==0 || ($(field).get(0).tagName=='SELECT' && $(field).val()=='0'))?'empty':'notempty'));
			}
		}
	}
	if (show_fields!='')
	{
		$(show_fields).show();
	}
	if (hide_fields!='')
	{
		$(hide_fields + " input[type=text]").each(function(){
			if ($(this).closest("div.form-group").css('display')=='none' && $(this).attr('data-keephiddenvalue')!='1')
			{
				$(this).val($(this).attr('data-emptyformat'));
			}
		});
		$(hide_fields + " textarea").each(function(){
			if ($(this).closest("div.form-group").css('display')=='none' && $(this).attr('data-keephiddenvalue')!='1')
			{
				$(this).val($(this).attr('data-emptyformat'));
			}
		});
		$(hide_fields + " select").each(function(){
			if ($(this).closest("div.form-group").css('display')=='none' && $(this).attr('data-keephiddenvalue')!='1')
			{
				if ($(this).attr('data-emptyformat')!=null && $(this).attr('data-emptyformat')!='')
				{
					$(this).val($(this).attr('data-emptyformat'));
				}
				else
				{
					$(this).val($('option:first',this).attr('value'));
				}
			}
		});
		$(hide_fields + " input[type=checkbox]").each(function(){
			if ($(this).closest("div.form-group").css('display')=='none' && $(this).attr('data-keephiddenvalue')!='1')
			{
				$(this).removeAttr('checked');
			}
		});
	}
	BASE.centerModal();
};

FORM.getOptions = function( field_name, fetch_url, data, set_fields )
{
	// Data
	if (data==null)
	{
		data = {};
	}
	if (field_name!=null)
	{
		data[field_name]=$("#"+field_name).val();
	}
	$.ajax({
		url: fetch_url,
		data: data,
		success: function( data )
		{
			if (data!=null && data.status=='1')
			{
				if (set_fields!=null)
				{
					for (var k in set_fields)
					{
						if (data[set_fields[k]]!=null)
						{
							FORM.loadOptions('#'+set_fields[k],data[set_fields[k]],$('#'+set_fields[k]).val());
						}
					}
				}
				else
				{
					for (var k in data)
					{
						if (k=='status') continue;
						if ($('#'+k).length==0) continue;
						FORM.loadOptions('#'+k,data[k],$('#'+k).val());
					}
				}
			}
			else
			{
				if (set_fields!=null)
				{
					for (var k in set_fields)
					{
						if ($("#"+set_fields[k]).get(0).tagName=='SELECT')
						{
							$("#"+set_fields[k]).html('<option value="">--- '+LOCALE.get('BASE.FORM.ERROR.ErrorFetchingData')+' ---</option>');
						}
						else
						{
							$("#"+set_fields[k]).val(LOCALE.get('BASE.FORM.ERROR.ErrorFetchingData'));
						}
					}
				}
			}
		},
		error: function()
		{
			if (set_fields!=null)
			{
				for (var k in set_fields)
				{
					if ($("#"+set_fields[k]).get(0).tagName=='SELECT')
					{
						$("#"+set_fields[k]).html('<option value="">--- '+LOCALE.get('BASE.FORM.ERROR.ErrorFetchingData')+' ---</option>');
					}
					else
					{
						$("#"+set_fields[k]).val(LOCALE.get('BASE.FORM.ERROR.ErrorFetchingData'));
					}
				}
			}
		}
	});
};

FORM.deleteFile = function( field_tag )
{
	BASE.confirmDialog(LOCALE.get('BASE.FORM.File.Delete.Confirm'),LOCALE.get('BASE.DIALOG.Title.Confirm'),{
		ok: {
			title: LOCALE.get('BASE.FORM.BTN.OK'),
			class: "btn-primary",
			callback: function(){
				BASE.closeDialog(this);
				$("#"+field_tag+"_delete").val('1');
				$("#"+field_tag+"_displayimage").remove();
				$("#"+field_tag+"_fileinfo").remove();
			}
		},
		cancel: {
			title: LOCALE.get('BASE.FORM.BTN.Cancel'),
			cancel: true,
			callback: function(){
				BASE.closeDialog(this);
			}
		}
	});
};


//
//  Chooser
//

/*
 *   Artist stuff
 */

CHOOSER = {};

CHOOSER.dialog_tag = '';
CHOOSER.field_name = '';
CHOOSER.search_url = '';
CHOOSER.data = {};
CHOOSER.addform_url = '';

CHOOSER.openChooser = function( field_name, search_url, chooser_title, search_placeholder, data, enable_add, addform_url, displaytrigger )
{
	var dialog_tag='dialog_' + Math.floor(Math.random()*100000+1);
	CHOOSER.dialog_tag=dialog_tag;

	CHOOSER.field_name=field_name;
	CHOOSER.search_url=search_url;
	if (chooser_title==null)
	{
		chooser_title=LOCALE.get('BASE.CHOOSER.DialogTitle');
	}
	if (search_placeholder==null)
	{
		search_placeholder=LOCALE.get('BASE.CHOOSER.SearchPlaceholder');
	}
	if (data!=null)
	{
		CHOOSER.data=data;
	}
	CHOOSER.data.tag=dialog_tag;
	if (addform_url!=null)
	{
		CHOOSER.addform_url=addform_url;
	}

	var chooser_dialog='<div id="chooser_searchform" style="margin-bottom: 20px"><table style="width: 100%">';
	chooser_dialog=chooser_dialog+'<tr>';
	chooser_dialog=chooser_dialog+'<td style="width: 99%"><input type="text" class="form-control" id="chooser_search" name="chooser_search" placeholder="'+search_placeholder+'" autocomplete="off" onkeyup="CHOOSER.searchKeypress(event)"></td>';
	chooser_dialog=chooser_dialog+'<td style="width: 1%; text-align: right" nowrap="nowrap">';
		chooser_dialog=chooser_dialog+'<button class="btn btn-default" style="margin-left: 10px" onclick="CHOOSER.doSearch()"><span class="icon-search"></span> '+LOCALE.get('BASE.CHOOSER.Action.Search')+'</button>';
		if (enable_add!=null && enable_add!=false) chooser_dialog=chooser_dialog+'<button class="btn btn-default" style="margin-left: 10px" onclick="CHOOSER.openAddForm()"><span class="icon-add"></span> '+enable_add+'</button>';
	chooser_dialog=chooser_dialog+'</td>';
	chooser_dialog=chooser_dialog+'</tr>';
	chooser_dialog=chooser_dialog+'</table></div>';
	chooser_dialog=chooser_dialog+'<div id="chooser_results" style="margin-top: 20px"><p style="color: #919191">'+LOCALE.get('BASE.CHOOSER.ResultsPlaceholder')+'</p></div>';
	if (enable_add!=null && enable_add!=false) chooser_dialog=chooser_dialog+'<div id="chooser_addform" style="display: none">add form be here</div>';

		BASE.createDialog(chooser_title,true,800,dialog_tag);
	$("#"+dialog_tag+" .modal-body-content").html(chooser_dialog);

	var chooser_buttons='';
	if (enable_add!=null && enable_add!=false) chooser_buttons=chooser_buttons+'<button id="chooser_addsubmit" class="btn btn-primary" style="display: none" onclick="">'+LOCALE.get('BASE.FORM.BTN.Add')+'</button>'
	chooser_buttons=chooser_buttons+'<button class="btn btn-default" onclick="BASE.closeDialog(\'#'+dialog_tag+'\')">'+LOCALE.get('BASE.DIALOG.CloseWindow')+'</button>';
	$("#"+dialog_tag+" .modal-footer").html(chooser_buttons);

	$('#'+dialog_tag).modal({
		backdrop: 'static',
		keyboard: true,
		show: true
	});

	if (displaytrigger!=null)
	{
		displaytrigger(dialog_tag);
	}

	$("#chooser_search").focus();
};

CHOOSER.searchKeypress = function( evt )
{
	if (evt.which==13)
	{
		CHOOSER.doSearch();
		evt.stopPropagation();
	}
};

CHOOSER.doSearch = function()
{
	var search=$("#chooser_search").val();
	if (search=='')
	{
		return;
	}
	else if (search.length<3)
	{
		$("#chooser_results").html('<p style="color: #919191">'+sprintf(LOCALE.get('BASE.CHOOSER.Error.MinimumLength'),'3')+'</p>');
		return;
	}
	var submitdata=CHOOSER.data;
	submitdata.search=search;
	submitdata.dialog_tag=CHOOSER.dialog_tag;
	$.ajax({
		url: CHOOSER.search_url,
		data: submitdata,
		success: function( data )
		{
			if (data!=null && data.status=='1')
			{
				$("#chooser_results").html(data.content);
				BASE.centerModal(CHOOSER.dialog_tag);
			}
			else
			{
				$("#chooser_results").html('<p style="color: #920000">'+oneof(data.error,LOCALE.get('BASE.CHOOSER.Error'))+'</p>');
				BASE.centerModal(CHOOSER.dialog_tag);
				return;
			}
		},
		error: function()
		{
			$("#chooser_results").html('<p style="color: #920000">'+LOCALE.get('BASE.CHOOSER.Error')+'</p>');
			BASE.centerModal(CHOOSER.dialog_tag);
		}
	});
};

CHOOSER.openAddForm = function()
{
	$.ajax({
		url: CHOOSER.addform_url,
		data: CHOOSER.data,
		success: function (data)
		{
			if (data!=null && data.status=='1')
			{
				$("#chooser_searchform").hide();
				$("#chooser_results").hide();
				$("#chooser_addform").show();
				$("#chooser_addsubmit").show();
				$("#chooser_addform").html(data.content);
				if (data.submit!=null)
				{
					$("#chooser_addsubmit").text(data.submit);
				}
				if ($("#"+CHOOSER.dialog_tag+" .modal-body-content .defaultfocus").length)
				{
					$("#"+CHOOSER.dialog_tag+" .modal-body-content .defaultfocus")[0].focus();
				}
				else
				{
					if ($("#"+CHOOSER.dialog_tag+" .modal-body-content :input:visible").length)
					{
						$("#"+CHOOSER.dialog_tag+" .modal-body-content :input:visible").first().focus();
					}
				}
				if ($("#"+CHOOSER.dialog_tag+" .modal-body-content input.setsearchresult").length)
				{
					$("#"+CHOOSER.dialog_tag+" .modal-body-content input.setsearchresult").val($("#chooser_search").val());
				}
				BASE.centerModal(CHOOSER.dialog_tag);
				FORM.initElements("#form_"+CHOOSER.dialog_tag);
				$('#chooser_addsubmit').on('click',function(){
					if ($("#form_"+CHOOSER.dialog_tag)[0].action==null || $("#form_"+CHOOSER.dialog_tag)[0].action=="")
					{
						BASE.alert(LOCALE.get('BASE.FORM.Error.CouldNotFindForm'),LOCALE.get('BASE.COMMON.Error'));
						return;
					}
					$("#form_"+CHOOSER.dialog_tag+" td.label.error").removeClass('error');
					BASE.progressDialog(LOCALE.get('BASE.FORM.MSG.Saving'));
					$("#form_"+CHOOSER.dialog_tag).ajaxSubmit({
						type: "post",
						data: { ajaxsubmit: '1' },
						success: function( data )
						{
							if (data!=null && data.status=='1')
							{
								if (data.message!=null && data.message!='')
								{
									BASE.closeProgressDialog();
									BASE.closeDialog('#'+dialog_tag);
									if (data.redirect!=null && data.redirect!='')
									{
										BASE.alert(data.message,null,null,data.redirect);
									}
									else if (data.reload!=null && data.reload=='1')
									{
										BASE.alert(data.message,null,true);
									}
								}
								else if (data.redirect!=null && data.redirect!='')
								{
									BASE.closeDialog('#'+CHOOSER.dialog_tag);
									BASE.redirect(data.redirect);
								}
								else if (data.reload!=null && data.reload=='1')
								{
									BASE.closeDialog('#'+CHOOSER.dialog_tag);
									BASE.reload();
								}
								else
								{
									BASE.closeProgressDialog();
									if (data.submitsuccessaction)
									{
										eval(data.submitsuccessaction);
									}
									BASE.closeDialog('#'+CHOOSER.dialog_tag);
								}
							}
							else
							{
								BASE.closeProgressDialog();
								var errors='<b>'+LOCALE.get('BASE.FORM.Error.Errors')+':</b>';
								if (data!=null && data.error!=null)
								{
									if (typeof(data.error)=='object')
									{
										for(var fld_tag in data.error)
										{
											if(data.error.hasOwnProperty(fld_tag))
											{
												errors=errors+'<br/> &#0149; '+data.error[fld_tag];
											}
										}
										BASE.displayErrors(errors,LOCALE.get('BASE.COMMON.Error'),data.error);
									}
									else
									{
										BASE.alert(data.error,LOCALE.get('BASE.COMMON.Error'));
									}
								}
								else
								{
									BASE.alert(LOCALE.get('BASE.COMMON.ErrorSavingData')+': 1',LOCALE.get('BASE.COMMON.Error'));
								}
								return;
							}
						},
						error: function(xhr, ajaxOptions, thrownError)
						{
							BASE.closeProgressDialog();
							BASE.alert(LOCALE.get('BASE.COMMON.ErrorSavingData')+': '+thrownError+' / '+var_dump(xhr),LOCALE.get('BASE.COMMON.Error'));
							return;
						}
					});
				});
			}
			else
			{
				if (data.error!=null)
				{
					$("#chooser_results").html('<p style="color: #920000">' + data.error + '</p>');
				}
				else
				{
					$("#chooser_results").html('<p style="color: #920000">' + LOCALE.get('BASE.COMMON.ErrorReadingData') + '</p>');
				}
			}
		},
		error: function()
		{
			$("#chooser_results").html('<p style="color: #920000">' + LOCALE.get('BASE.COMMON.ErrorReadingData') + '</p>');
			BASE.centerModal(CHOOSER.dialog_tag);
		}
	});
};

CHOOSER.select = function( value, description )
{
	$("#"+CHOOSER.field_name).val(value).trigger('change');
	$("#"+CHOOSER.field_name+"_display").html(description);
	CHOOSER.closeChooser();
};

CHOOSER.addItem = function( value, description )
{
	// Add
	if ($("#"+CHOOSER.field_name+"_val_"+value).length==0)
	{
		var html='<p id="'+CHOOSER.field_name+'_val_'+value+'" class="form-control-static field-chooser-value" style="width: 100%">';
		var html=html+'<input type="hidden" id="'+CHOOSER.field_name+'_'+value+'" name="'+CHOOSER.field_name+'[]" value="'+decodeURIComponent(value)+'">';
		var html=html+'<span class="field-chooser-display">'+description+'</span>';
		var html=html+'<span class="field-chooser-choose"><a onclick="CHOOSER.removeItem(\''+CHOOSER.field_name+'\',\''+value+'\')"><span class="icon-cancel"></span></a></span>';
		var html=html+'</p>';
		$("#"+CHOOSER.field_name+"_value").append(html);
		$("#"+CHOOSER.field_name+"_trigger").trigger('change');
	}

	// Close search
	$("#"+CHOOSER.field_name+"_none").hide();
	CHOOSER.closeChooser();
};

CHOOSER.removeItem = function( field_tag, value )
{
	$("#"+field_tag+"_val_"+value).remove();
	if ($("#"+field_tag+"_value .field-chooser-value").length==0)
	{
		$("#"+field_tag+"_none").show();
	}
	$("#"+field_tag+"_trigger").trigger('change');
};

CHOOSER.closeChooser = function()
{
	BASE.closeDialog('#'+CHOOSER.dialog_tag);
};


//
//  Inline chooser
//

INLINECHOOSER = {};

INLINECHOOSER.search_results = new Array();

INLINECHOOSER.keyPress = function( field_tag, min_search_length, search_url, multiple, evt )
{
	var search=$("#"+field_tag+"_search").val();
	if (search.length<min_search_length)
	{
		$("#"+field_tag+"_searchresults").hide();
		return;
	}
	if (evt.which==13)
	{
		evt.stopPropagation();
		if (INLINECHOOSER.search_results[field_tag]!=null && Object.elementCount(INLINECHOOSER.search_results[field_tag])==1)
		{
			for (var i in INLINECHOOSER.search_results[field_tag])
			{
				var key=((i.indexOf("#_")>-1)?i.replace("#_",""):i);
				if (multiple)
				{
					INLINECHOOSER.addItem(field_tag,key,INLINECHOOSER.search_results[field_tag][i]);
				}
				else
				{
					INLINECHOOSER.select(field_tag,key,INLINECHOOSER.search_results[field_tag][i]);
				}
			}
		}
		return;
	}
	$.ajax({
		url: search_url,
		data: {
			search: search
		},
		success: function( data )
		{
			if (data!=null && data.status=='1')
			{
				if (data.message!=null && data.message.length>0)
				{
					$("#"+field_tag+"_searchresults").show();
					$("#"+field_tag+"_searchresults").html('<div class="field-inlinechooser-search-message">'+data.message+'</div>');
				}
				else if (data.search!=null)
				{
					INLINECHOOSER.search_results[field_tag]=data.search;
					var res='';
					for (var i in data.search)
					{
						var key=((i.indexOf("#_")>-1)?i.replace("#_",""):i);
						if (multiple)
						{
							var action="INLINECHOOSER.addItem('"+field_tag+"','"+key+"','"+data.search[i]+"')";
						}
						else
						{
							var action="INLINECHOOSER.select('"+field_tag+"','"+key+"','"+encodeURIComponent(data.search[i])+"')";
						}
						res=res+'<div class="field-inlinechooser-search-resultitem" onclick="'+action+'">'+data.search[i]+'</a></div>';
					}
					$("#"+field_tag+"_searchresults").show();
					$("#"+field_tag+"_searchresults").html(res);
				}
				else
				{
					$("#"+field_tag+"_searchresults").show();
					$("#"+field_tag+"_searchresults").html('<div class="field-inlinechooser-search-message" style="color: #920000">'+LOCALE.get('BASE.CHOOSER.Error')+'</div>');
				}
			}
			else
			{
				$("#"+field_tag+"_searchresults").show();
				$("#"+field_tag+"_searchresults").html('<div class="field-inlinechooser-search-message" style="color: #920000">'+oneof(data.error,LOCALE.get('BASE.CHOOSER.Error'))+'</div>');
			}
		},
		error: function()
		{
			$("#"+field_tag+"_searchresults").show();
			$("#"+field_tag+"_searchresults").html('<div class="field-inlinechooser-search-message" style="color: #920000">'+LOCALE.get('BASE.CHOOSER.Error')+'</div>');
		}
	});
};

INLINECHOOSER.select = function( field_tag, value, description )
{
	$("#"+field_tag).val(decodeURIComponent(value)).trigger('change');
	$("#"+field_tag+"_display").html(decodeURIComponent(description));
	$("#"+field_tag+"_searchresults").hide();
	$("#"+field_tag+"_search").val('').focus();
};

INLINECHOOSER.addItem = function( field_tag, value, description )
{
	// Add
	if ($("#"+field_tag+"_val_"+value).length==0)
	{
		var html='<p id="'+field_tag+'_val_'+value+'" class="form-control-static field-inlinechooser-value" style="width: 100%">';
		var html=html+'<input type="hidden" id="'+field_tag+'_'+value+'" name="'+field_tag+'[]" value="'+decodeURIComponent(value)+'">';
		var html=html+'<span class="field-chooser-display">'+description+'</span>';
		var html=html+'<span class="field-chooser-choose"><a onclick="INLINECHOOSER.removeItem(\''+field_tag+'\',\''+value+'\')"><span class="icon-cancel"></span></a></span>';
		var html=html+'</p>';
		$("#"+field_tag+"_value").append(html);
		$("#"+field_tag+"_trigger").trigger('change');
	}

	// Close search
	$("#"+field_tag+"_none").hide();
	$("#"+field_tag+"_searchresults").hide();
	$("#"+field_tag+"_search").val('').focus();
};

INLINECHOOSER.removeItem = function( field_tag, value )
{
	$("#"+field_tag+"_val_"+value).remove();
	if ($("#"+field_tag+"_value .field-inlinechooser-value").length==0)
	{
		$("#"+field_tag+"_none").show();
	}
	$("#"+field_tag+"_trigger").trigger('change');
};


//
//  List stuff
//

LIST = {};

LIST.FILTER = {};

LIST.FILTER.CHECKLIST = {};

LIST.FILTER.CHECKLIST.openFilter = function ( field_tag, title, options )
{
	// Olemasolevad väärtused
	var selected=$("input[name=filter_"+field_tag+"]").val().split(',');

	// Teeme HTMLi
	var filtercontent = '<div class="checklistfilter-options container-fluid"><div class="row-fluid">';
	var c = 1;
	for (var o in options)
	{
		c++;
		if (c==2)
		{
			filtercontent = filtercontent + '</div>';
			filtercontent = filtercontent + '<div class="row-fluid">';
			c=0;
		}
		filtercontent = filtercontent + '<div class="col-xs-6"><label for="filteroption_'+o+'"><input type="checkbox" style="margin-right: 7px" id="filteroption_'+o+'" rel="filteroption_'+field_tag+'" value="' + o + '"' + ($.inArray(o,selected)!=-1?'checked="checked"':'')  + '>' + options[o] +  '</label></div>';
	}
	filtercontent = filtercontent + '</div></div>';

	// Dialog
	var dialog_tag=BASE.createDialog(title,true);
	$('#'+dialog_tag+' .modal-body-content').html(filtercontent);

	// Buttons
	$('#'+dialog_tag+' .modal-footer').append('<button type="button" id="'+dialog_tag+'_btn_apply" class="btn btn-primary">'+LOCALE.get('BASE.LIST.Filter.ApplySelection')+'</button>');
	$('#'+dialog_tag+'_btn_apply').on('click',function(){
		var filterval = '';
		var filtertext = '';
		if ($("input[rel=filteroption_"+field_tag+"]:checked").length<$("input[rel=filteroption_"+field_tag+"]").length)
		{
			$("input[rel=filteroption_"+field_tag+"]:checked").each(function(){
				if (filterval!='')
				{
					filterval=filterval+',';
					filtertext=filtertext+', ';
				}
				filterval=filterval+$(this).attr('value');
				filtertext=filtertext+options[$(this).attr('value')];
			});
		}
		if (filterval=='')
		{
			filtertext='--- '+LOCALE.get('BASE.COMMON.All')+' ---';
		}
		$("input[name=filter_"+field_tag+"]").val(filterval).change();
		$("input[name=filter_"+field_tag+"_disp]").val(filtertext);
		$("#"+dialog_tag).modal('hide');
	});
	$('#'+dialog_tag+' .modal-footer').append('<button type="button" class="btn btn-default" data-dismiss="modal">'+LOCALE.get('BASE.FORM.Btn.Cancel')+'</button>');

	// Show
	$('#'+dialog_tag).modal({
		backdrop: 'static',
		keyboard: true,
		show: true
	});
};



//
//  Tabbed content
//

var TAB = {};

TAB.loaded = new Array();

TAB.select = function ( tab_id, content_url )
{
	// Show tab
	$('.tabcontent').hide();
	$('#content_'+tab_id).show();
	$(".nav-tabs li").removeClass('active');
	$('#tab_'+tab_id).addClass('active');

	// URL
	var URL = document.location.toString();
	if (URL.match('#'))
	{
		var URLbase = URL.split('#')[0];
		document.location.href = URLbase + '#' + tab_id;
	}
	else
	{
		document.location.href = URL + '#' + tab_id;
	}

	// Load content
	if (content_url==null && $("#content_"+tab_id).attr('rel')!=undefined && $("#content_"+tab_id).attr('rel')!='')
	{
		content_url=$("#content_"+tab_id).attr('rel');
	}
	if (content_url!=null)
	{
		// Already loaded
		if (TAB.loaded[tab_id]!=null && TAB.loaded[tab_id]==1) {
			return;
		}

		// Loading
		$("#content_"+tab_id).html('<div class="tab-loader"><span class="spinner"></span>'+LOCALE.get('BASE.TAB.Loading')+'</div>');

		// Get content
		$.ajax({
			url: content_url,
			data: "",
			success: function( data )
			{
				if (data!=null && data.status=='1')
				{
					$("#content_"+tab_id).hide().html(data.content).fadeIn(200);
					TAB.loaded[tab_id]=1;

					// Init tooltips
					$("#content_"+tab_id+' [data-toggle="tooltip"]').tooltip();

					// Init tab
					TAB.initTab(tab_id);

					// Fire display trigger
					if ($("#content_"+tab_id).attr('data-displaytrigger').length)
					{
						eval($("#content_"+tab_id).attr('data-displaytrigger'));
					}
				}
				else
				{
					if (data.error!=null && data.error!='')
					{
						$("#content_"+tab_id).html('<div class="alert alert-danger">'+data.error+'</div>');
					}
					else
					{
						$("#content_"+tab_id).html('<div class="alert alert-danger">'+LOCALE.get('BASE.COMMON.ErrorReadingData')+'</div>');
					}
					return;
				}
			},
			error: function()
			{
				$("#content_"+tab_id).html('<div class="alert alert-danger">'+LOCALE.get('BASE.COMMON.ErrorReadingData')+'</div>');
				return;
			}
		});
	}
};

TAB.initTab = function( tab_id )
{
	// This can be overwritten in the application if needed
};


//
//	Log
//

var LOG = {};

LOG.toggleData = function( id )
{
	if (id!=null)
	{
		var $icon=$('#'+id).prev().find('span.icon-arrow-closed,span.icon-arrow-open');
		if ($icon.attr('class')=='icon-arrow-closed')
		{
			$icon.attr('class','icon-arrow-open');
		}
		else
		{
			$icon.attr('class','icon-arrow-closed');
		}
		$('#'+id).toggle();
	}
	else
	{
		$('.logrow').show();
		$('#content_log table span').attr('class','icon-arrow-open');
	}

};


//
//  Passwords
//

PASSWORD = {};

PASSWORD.suggestPassword = function( field_tag )
{
	// Generate password
	while(true)
	{
		var password='';
		var symbols = "ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz987654321";
		for(var b=0;b<4;b++)
		{
			if (b>0) password+='-';
			for(var i=0;i<3;i++)
			{
				var symbol = symbols[(Math.floor(Math.random() * symbols.length))];
				password+=symbol;
			}
		}
		if (password.match(/([987654321])+/)) break;
	}

	// Set
	$("#"+field_tag).val(password);
	$("#"+field_tag+"_repeat").val(password);

	// Show
	BASE.alert(LOCALE.get('BASE.FORM.Password.SuggestPassword.Result.1')+': <b>'+password+'</b><br/>'+LOCALE.get('BASE.FORM.Password.SuggestPassword.Result.2'),LOCALE.get('BASE.FORM.Password.SuggestPassword.Title'));
};


//
//  LOGIN
//

LOGIN = {};

LOGIN.usernameKeypress= function( evt )
{
	if ( evt.which == 13 )
	{
		$("#login_password")[0].focus();
		evt.stopPropagation();
		return;
	}
};

LOGIN.passwordKeypress= function( evt )
{
	if ( evt.which == 13 )
	{
		LOGIN.doLogin();
		evt.stopPropagation();
		return;
	}
};

LOGIN.doLogin = function()
{
	$("#login_message").hide();
	$("#feedback_login_email").attr('class','form-control-feedback');
	$("#feedback_login_password").attr('class','form-control-feedback');

	var username=jQuery.trim($("#login_email").val());
	if (username=='')
	{
		$("#feedback_login_email").attr('class','form-control-feedback icon-cancel text-danger');
		$("#login_email")[0].focus();
		return;
	}
	else if (!username.match(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/))
	{
		$("#feedback_login_email").attr('class','form-control-feedback icon-cancel text-danger');
		$("#login_email")[0].focus();
		return;
	}

	var password=jQuery.trim($("#login_password").val());
	if (username=='password')
	{
		$("#feedback_login_password").attr('class','form-control-feedback icon-cancel text-danger');
		$("#login_password")[0].focus();
		return;
	}

	$("#login_email").attr('readonly','readonly');
	$("#login_password").attr('readonly','readonly');
	$("#login_submit").attr('disabled','disabled');
	if ($("#login_submit").attr('data-title-progress')!=null && $("#login_submit").attr('data-title-progress')!='')
	{
		$("#login_submit").html($("#login_submit").attr('data-title-progress'));
	}

	$("#login_form").ajaxSubmit({
		type: 'post',
		data: { login: '1' },
		success: function( data )
		{
			$("#login_email").removeAttr('readonly');
			$("#login_password").removeAttr('readonly');
			$("#login_submit").removeAttr('disabled');
			if ($("#login_submit").attr('data-title')!=null && $("#login_submit").attr('data-title')!='')
			{
				$("#login_submit").html($("#login_submit").attr('data-title'));
			}
			if (data!=null && data.status=='1')
			{
				if (data.redirect!=null && data.redirect!='')
				{
					BASE.redirect(data.redirect);
				}
				else if (data.reload!=null && data.reload=='1')
				{
					BASE.reload();
				}
				else
				{
					if (data.submitsuccessaction)
					{
						eval(data.submitsuccessaction);
					}
				}
			}
			else
			{
				$("#login_message").text(oneof(data.error,'Login error')).show();
				return;
			}
		},
		error: function()
		{
			$("#login_email").removeAttr('disabled');
			$("#login_password").removeAttr('disabled');
			$("#login_submit").removeAttr('disabled');
			if ($("#login_submit").attr('data-title')!=null && $("#login_submit").attr('data-title')!='')
			{
				$("#login_submit").html($("#login_submit").attr('data-title'));
			}
			$("#login_message").text('Login error').show();
			return;
		}
	});
};
