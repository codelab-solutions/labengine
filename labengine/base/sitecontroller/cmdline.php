<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   The command-line script controller
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	//
	//  Set include path if it exists
	//

	if (!empty($_ENV['LAB_INCLUDEPATH']))
	{
		set_include_path($_ENV['LAB_INCLUDEPATH']);
	}


	//
	//  Include basic files
	//

	include_once 'base/code/errorhandling.php';
	include_once 'base/code/systemfunctions.php';
	include_once 'base/object/base.php';


	//
	//  Set error handling
	//

	error_reporting(E_ALL);
	ini_set('display_errors',0);
	set_error_handler("errorHandler");
	set_exception_handler("exceptionHandler");
	register_shutdown_function('shutdownHandler');


	//
	//  Do sanity check
	//

	if (ini_get('register_globals')=='On') trigger_error('Cowardly refusing to run with register_globals on.',E_USER_ERROR);
	date_default_timezone_set('UTC');


	//
	//  Initialize the superobject
	//

	$LAB=new BASE_LABSUPEROBJECT();


	//
	//  Start the request timer
	//

	$LAB->requestID=date('YmdHis').'-'.uniqid();
	$LAB->requestStartTime=microtime_float();


	//
	//  Init non-configuration-dependent classes
	//

	$LAB->LOCALE=new BASE_LOCALE();
	$LAB->CACHE=new BASE_CACHE();


	//
	//  Load configuration
	//

	$LAB->CONFIG=new BASE_CONFIG();
	$LAB->CONFIG->loadConfig();


	//
	//  Init system l10n/i18n settings
	//

	mb_internal_encoding('UTF-8');
	$timeZone=oneof($LAB->CONFIG->get('locale.timezone'),FALSE);
	$dateFormat=oneof($LAB->CONFIG->get('locale.dateformat'),FALSE);
	$LAB->DATETIME=new BASE_DATETIME($timeZone,$dateFormat);
	$LAB->REGIONAL=new BASE_REGIONAL();


	//
	//  Init debug class
	//

	$LAB->DEBUG=new BASE_DEBUG();
	$LAB->DEBUG->initDebug();


	//
	//  Init database class & connect
	//

	if ($LAB->CONFIG->get('db.handler'))
	{
		$className=$LAB->CONFIG->get('db.handler');
	}
	else
	{
		if (!$LAB->CONFIG->get('db.db')) throw new Exception('DB engine not set in config.');
		$className='BASE_DB_'.$LAB->CONFIG->get('db.db');
	}
	$LAB->DB=new $className();
	$LAB->DB->connect();


	//
	//  Init session
	//

	$className=oneof($LAB->CONFIG->get('session.handler'),'BASE_SESSION');
	$LAB->SESSION=new $className();


	//
	//  Init user
	//

	$className=oneof($LAB->CONFIG->get('user.handler'),'BASE_USER');
	$LAB->USER=new $className();


	//
	//  Run script
	//

	if (isset($runScript) && is_object($runScript))
	{
		if (!method_exists($runScript,'runScript'))
		{
			throw new Exception("Did not find the runScript() method in the class.\n");
		}
		$returnCode=$runScript->runScript($argv);
	}
	else
	{
		if (!function_exists('runScript'))
		{
			throw new Exception("Did not find the runScript() method.\n");
		}
		$returnCode=runScript();
	}


	//
	//  Close database connection
	//

	$LAB->DB->disconnect();


	//
	//  Return
	//

	exit(intval($returnCode));


?>