<?php


	/**
	 *
	 *   LabEngine™ 7
	 *   The main site controller
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	//
	//  Include basic files
	//

	include_once 'base/code/errorhandling.php';
	include_once 'base/code/systemfunctions.php';
	include_once 'base/object/base.php';


	//
	//  Set error handling
	//

	error_reporting(E_ALL);
	ini_set('display_errors',0);
	set_error_handler("errorHandler");
	set_exception_handler("exceptionHandler");
	register_shutdown_function('shutdownHandler');


	//
	//  Do sanity check
	//

	if (ini_get('register_globals')=='On') trigger_error('Cowardly refusing to run with register_globals on.',E_USER_ERROR);
	date_default_timezone_set('UTC');


	//
	//  Initialize the superobject
	//

	$LAB=new BASE_LABSUPEROBJECT();


	//
	//  Start the request timer
	//

	$LAB->requestID=date('YmdHis').'-'.uniqid();
	$LAB->requestStartTime=microtime_float();


	//
	//  Init non-configuration-dependent classes
	//

	$LAB->LOCALE=new BASE_LOCALE();
	$LAB->CACHE=new BASE_CACHE();


	//
	//  Load configuration
	//

	$LAB->CONFIG=new BASE_CONFIG();
	$LAB->CONFIG->loadConfig();


	//
	//  Init system l10n/i18n settings
	//

	mb_internal_encoding('UTF-8');
	$timeZone=oneof($LAB->CONFIG->get('locale.timezone'),FALSE);
	$dateFormat=oneof($LAB->CONFIG->get('locale.dateformat'),FALSE);
	$LAB->DATETIME=new BASE_DATETIME($timeZone,$dateFormat);
	$LAB->REGIONAL=new BASE_REGIONAL();


	//
	//  Init debug class
	//

	$LAB->DEBUG=new BASE_DEBUG();
	$LAB->DEBUG->initDebug();


	//
	//  Init database class & connect
	//

	if ($LAB->CONFIG->get('db.handler'))
	{
		$className=$LAB->CONFIG->get('db.handler');
	}
	else
	{
		if (!$LAB->CONFIG->get('db.db')) throw new Exception('DB engine not set in config.');
		$className='BASE_DB_'.$LAB->CONFIG->get('db.db');
	}
	$LAB->DB=new $className();
	$LAB->DB->connect();


	//
	//  Init environment
	//

	if (stream_resolve_include_path('site/code/init.environment.php'))
	{
		include_once 'site/code/init.environment.php';
	}


	//
	//  Init session
	//

	$className=oneof($LAB->CONFIG->get('session.handler'),'BASE_SESSION');
	$LAB->SESSION=new $className();
	$LAB->SESSION->loadSession();


	//
	//  Init user
	//

	$className=oneof($LAB->CONFIG->get('user.handler'),'BASE_USER');
	$LAB->USER=new $className();
	$LAB->USER->loadUser();


	//
	//  Init request/response handlers
	//

	$className=oneof($LAB->CONFIG->get('request.handler'),($LAB->CONFIG->get('cms.cms')?'CMS_REQUEST':FALSE),'BASE_REQUEST');
	$LAB->REQUEST=new $className();

	$className=oneof($LAB->CONFIG->get('response.handler'),'BASE_RESPONSE');
	$LAB->RESPONSE=new $className();


	//
	//  Handle request
	//

	$LAB->REQUEST->handleRequest();


	//
	//  Handle response
	//

	$LAB->RESPONSE->handleResponse();


	//
	//  Save session
	//

	$LAB->SESSION->saveSession();


	//
	//  Log profiler info
	//

	$LAB->DEBUG->logProfilerInfo();


	//
	//  Close database connection
	//

	$LAB->DB->disconnect();


?>