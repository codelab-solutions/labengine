#
#  System-wide common strings
#

BASE.COMMON.OK                                            OK
BASE.COMMON.Warnings                                      Warnungen
BASE.COMMON.NotOK                                         Nicht OK
BASE.COMMON.ShowAll                                       alle anzeigen
BASE.COMMON.Missing                                       fehlt
BASE.COMMON.NotSet                                        nicht gesendet
BASE.COMMON.NotEntered                                    nicht eingegeben
BASE.COMMON.NotConnected                                  nicht verbunden
BASE.COMMON.None                                          keine
BASE.COMMON.Empty                                         leer
BASE.COMMON.Unknown                                       unbekannt
BASE.COMMON.All                                           alle
BASE.COMMON.SelectAll                                     alle auswählen
BASE.COMMON.Selected                                      ausgewählt
BASE.COMMON.Other                                         andere
BASE.COMMON.OtherEnter                                    andere, weiter
BASE.COMMON.BackToList                                    zurück zur Liste

BASE.COMMON.Error                                         FEHLER
BASE.COMMON.ErrorPage                                     Leider ist die gewünschte Seite aufgrund eines Fehlers vorübergehend nicht erreichbar
BASE.COMMON.ErrorReadingData                              Fehler beim Lesen der Daten.
BASE.COMMON.ErrorSavingData                               Fehler beim Speichern der Daten.
BASE.COMMON.ErrorCheckingData                             Fehler beim Überprüfen der Daten.
BASE.COMMON.ErrorReadingFile                              Fehler beim Lesen der Datei.
BASE.COMMON.ErrorReadingCalcResults                       Fehler beim Lesen der Antwort.
BASE.COMMON.ErrorDisplayingForm                           Fehler beim Anzeigen des Formulars.
BASE.COMMON.ErrorCreatingPDF                              Fehler beim Erstellen des PDF.
BASE.COMMON.Error.TemplateNotFound                        Vorlage konnte nicht gefunden werden.
BASE.COMMON.Error.AccessDenied                            Zugang verweigert.
BASE.COMMON.Error.NotNumeric                              Kein numerischer Wert
BASE.COMMON.Error.NothingSelected                         Nichts ausgewählt
BASE.COMMON.Error.Date.Invalid                            Muss im %s Format vorliegen
BASE.COMMON.Error.Date.InvalidDate                        Ungültiges Datum
BASE.COMMON.Error.Date.Invalid.Range                      Muss als Datum oder Datumsbereich im %s Format vorliegen
BASE.COMMON.Error.Date.StartInvalid                       Startzeitraum nicht im gültigen %s Format
BASE.COMMON.Error.Date.EndInvalid                         Endzeitraum nicht im gültigen %s Format
BASE.COMMON.Error.Date.EndEarlierThanStart                Startdatum muss vor dem Enddatum liegen
BASE.COMMON.Error.NumberFld.Format                        Der angegebene Wert ist keine gültige Zahl
BASE.COMMON.Error.NumberFld.Format.Negative               Der angegebene Wert ist keine gültige positive Zahl
BASE.COMMON.Error.NumberFld.MinValue                      Das erlaubte Minimum ist %s
BASE.COMMON.Error.NumberFld.MaxValue                      Das erlaubte Maximum ist %s
BASE.COMMON.Error.Delete.RelatedDataExists                Löschen nicht möglich - Zugehörige Daten existieren.

BASE.COMMON.Yes                                           Ja
BASE.COMMON.No                                            Nein
BASE.COMMON.Enabled                                       Aktiviert
BASE.COMMON.Disabled                                      Deaktiviert

BASE.COMMON.Add                                           Hinzufügen
BASE.COMMON.Edit                                          Bearbeiten
BASE.COMMON.Delete                                        Löschen
BASE.COMMON.Choose                                        Auswählen
BASE.COMMON.NotEditable                                   Nicht editierbar

BASE.COMMON.Email                                         E-Mail Adresse
BASE.COMMON.Email.Short                                   E-Mail
BASE.COMMON.Name                                          Name
BASE.COMMON.Name.First                                    Vorname
BASE.COMMON.Name.Last                                     Nachname
BASE.COMMON.Name.Middle                                   Zweiter Vorname
BASE.COMMON.Address                                       Adresse
BASE.COMMON.Address.Street                                Strassenname
BASE.COMMON.Address.ZIP                                   Postleitzahl
BASE.COMMON.Address.City                                  Stadt
BASE.COMMON.Address.State                                 Bundesland
BASE.COMMON.Address.Country                               Land

BASE.COMMON.Language                                      Sprache
BASE.COMMON.Login                                         Login

BASE.COMMON.Password                                      Passwort
BASE.COMMON.Password.Repeat                               Passwort (wiederholen)


#
#  Date/time stuff
#

BASE.COMMON.Date                                          Datum
BASE.COMMON.Time                                          Zeit
BASE.COMMON.DateTime                                      Datum/Zeit

BASE.COMMON.Day                                           Tag
BASE.COMMON.Days                                          Tage
BASE.COMMON.DayAbbr                                       T
BASE.COMMON.Week                                          Woche
BASE.COMMON.Weeks                                         Wochen
BASE.COMMON.WeekAbbr                                      W
BASE.COMMON.Month                                         Monat
BASE.COMMON.Months                                        Monate
BASE.COMMON.MonthAbbr                                     M
BASE.COMMON.Year                                          Jahr
BASE.COMMON.Years                                         Jahre
BASE.COMMON.YearAbbr                                      J
BASE.COMMON.Hour                                          Stunde
BASE.COMMON.Hours                                         Stunden
BASE.COMMON.HourAbbr                                      h
BASE.COMMON.Min                                           Minute
BASE.COMMON.Mins                                          Minuten
BASE.COMMON.MinAbbr                                       min

BASE.COMMON.DAY.1                                         Montag
BASE.COMMON.DAY.2                                         Dienstag
BASE.COMMON.DAY.3                                         Mittwoch
BASE.COMMON.DAY.4                                         Donnerstag
BASE.COMMON.DAY.5                                         Freitag
BASE.COMMON.DAY.6                                         Samstag
BASE.COMMON.DAY.7                                         Sonntag

BASE.COMMON.DAY.Short.1                                   Mo
BASE.COMMON.DAY.Short.2                                   Di
BASE.COMMON.DAY.Short.3                                   Mi
BASE.COMMON.DAY.Short.4                                   Do
BASE.COMMON.DAY.Short.5                                   Fr
BASE.COMMON.DAY.Short.6                                   Sa
BASE.COMMON.DAY.Short.7                                   So

BASE.COMMON.MONTH.1                                       Jänner
BASE.COMMON.MONTH.2                                       Februar
BASE.COMMON.MONTH.3                                       März
BASE.COMMON.MONTH.4                                       April
BASE.COMMON.MONTH.5                                       Mai
BASE.COMMON.MONTH.6                                       Juni
BASE.COMMON.MONTH.7                                       Juli
BASE.COMMON.MONTH.8                                       August
BASE.COMMON.MONTH.9                                       September
BASE.COMMON.MONTH.10                                      Oktober
BASE.COMMON.MONTH.11                                      November
BASE.COMMON.MONTH.12                                      Dezember

BASE.COMMON.MONTH.Short.1                                 Jan
BASE.COMMON.MONTH.Short.2                                 Feb
BASE.COMMON.MONTH.Short.3                                 Mar
BASE.COMMON.MONTH.Short.4                                 Apr
BASE.COMMON.MONTH.Short.5                                 Mai
BASE.COMMON.MONTH.Short.6                                 Jun
BASE.COMMON.MONTH.Short.7                                 Jul
BASE.COMMON.MONTH.Short.8                                 Aug
BASE.COMMON.MONTH.Short.9                                 Sep
BASE.COMMON.MONTH.Short.10                                Okt
BASE.COMMON.MONTH.Short.11                                Nov
BASE.COMMON.MONTH.Short.12                                Dez


#
#  Tab strings
#

BASE.TAB.Loading                                          Ladet ...


#
#  List strings
#

BASE.LIST.Filter.Submit                                   Filter anwenden
BASE.LIST.Filter.Clear                                    Filter zurücksetzen
BASE.LIST.Filter.Required                                 Bitte zumindest einen Filter auswählen/eintragen.
BASE.LIST.Filter.ApplySelection                           Auswahl anwenden
BASE.LIST.Filter.ToggleAll                                Alle ausblenden
BASE.LIST.Filter.ShowAll                                  Alle anzeigen

BASE.LIST.Actions                                         Aktionen
BASE.LIST.Action.Add                                      Neuen Eintrag hinzufügen
BASE.LIST.Action.AddSub                                   Zusatzeintrag hinzufügen
BASE.LIST.Action.Edit                                     Eintrag bearbeiten
BASE.LIST.Action.Delete                                   Eintrag löschen
BASE.LIST.Action.Swap.Up                                  Eintrag nach oben verschieben
BASE.LIST.Action.Swap.Dn                                  Eintrag nach unten verschieben

BASE.LIST.Confirm                                         Ganz sicher? Diese Aktion kann nicht nicht rückgängig gemacht werden.
BASE.LIST.Confirm2                                        Bist du ganz sicher?

BASE.LIST.TotalRecords                                    Gesamtzahl <b>%d</b> Einträge

BASE.LIST.Return                                          zurück


#
#  Dialog strings
#

BASE.DIALOG.Title.Message                                 Nachricht
BASE.DIALOG.Title.Choose                                  Bitte wählen
BASE.DIALOG.Title.Confirm                                 Bitte bestätigen
BASE.DIALOG.CloseWindow                                   Dialog schließen


#
#  Form strings
#

BASE.FORM.Select                                          von hier auswählen

BASE.FORM.BTN.Save                                        Speichern
BASE.FORM.BTN.Add                                         Hinzufügen
BASE.FORM.BTN.Edit                                        Ändern
BASE.FORM.BTN.OK                                          OK
BASE.FORM.BTN.Cancel                                      Abbrechen

BASE.FORM.MSG.Querying                                    Suche Daten, einen Moment bitte ...
BASE.FORM.MSG.Saving                                      Sichere Daten, einen Moment bitte ...

BASE.FORM.Notes                                           Notizen
BASE.FORM.Notes.Password                                  Passwort nur eingeben, wenn du es ändern möchtest.

BASE.FORM.Chooser.choose                                  wählen
BASE.FORM.Chooser.clear                                   Zurücksetzen

BASE.FORM.File.Delete.Confirm                             Diese Datei löschen?

BASE.FORM.Password.SuggestPassword                        Sicheres Passwort vorschlagen
BASE.FORM.Password.SuggestPassword.Title                  Passwort Vorschlag
BASE.FORM.Password.SuggestPassword.Result.1               Dein Passwort lautet
BASE.FORM.Password.SuggestPassword.Result.2               Bitte notiere es und bewahre es an einem sicheren Ort auf.

BASE.FORM.ERROR.Errors                                    Die folgenden Fehler traten beim Validieren der Daten auf
BASE.FORM.ERROR.ErrorOpeningDialog                        Beim Öffnen des Dialogs ist ein Fehler aufgetreten.
BASE.FORM.ERROR.CouldNotFindForm                          Konnte Formular nicht finden
BASE.FORM.ERROR.CouldNotLoad                              Konnte Daten nicht laden
BASE.FORM.ERROR.CouldNotSave                              Konnte Daten nicht speichern
BASE.FORM.ERROR.RequiredFieldEmpty                        Pflichtfeld ist nicht ausgefüllt
BASE.FORM.ERROR.Filter                                    Feld enthält unzulässige Zeichen
BASE.FORM.ERROR.Unique                                    Wert ist nicht einzigartig
BASE.FORM.ERROR.MinLength                                 Wert ist zu kurz (es werden mindestens %d Zeichen benötigt)
BASE.FORM.ERROR.MaxLength                                 Wert zu lang (maximal %d Zeichen zulässig)
BASE.FORM.ERROR.MinValue                                  Wert unter dem zulässigen Minimum (%d)
BASE.FORM.ERROR.MaxValue                                  Wert oberhalb des erlaubten Maximums (%d)
BASE.FORM.ERROR.InvalidValue                              Ungültiger Wert
BASE.FORM.ERROR.InvalidDate                               Ungültiges Datum
BASE.FORM.ERROR.InvalidTime                               Ungültige Zeit
BASE.FORM.ERROR.IncompleteDateTime                        Unvollständiges Datum/Zeit
BASE.FORM.ERROR.PasswordWeak1                             Passwort zu schwach (muss sowohl aus Zeichen als auch Nummern bestehen)
BASE.FORM.ERROR.PasswordWeak2                             Passwort zu schwach (muss Klein- sowie Großbuchstaben, Zahlen und Satzzeichen enthalten)
BASE.FORM.ERROR.PasswordRepeatEmpty                       Bitte Passwort wiederholen
BASE.FORM.ERROR.PasswordRepeatMismatch                    Wiederholtes Passwort stimmt nicht überein
BASE.FORM.ERROR.FileTypeNotAllowed                        Dateien dieser Art sind nicht zulässig
BASE.FORM.ERROR.InvalidEmail                              E-Mail Adresse ungültig
BASE.FORM.ERROR.InvalidEmail2                             E-Mail Adresse (%s) ungültig
BASE.FORM.ERROR.DuplicateKey                              Ein Eintrag mit den angegebenen Daten existiert bereits. Ein Duplikat kann nicht erstellt werden.
BASE.FORM.ERROR.ErrorFetchingData                         Fehler beim Auslesen der Daten

BASE.FORM.Log.Password.Old                                Altes Passwort
BASE.FORM.Log.Password.New                                Neues Passwort

#
#  Chooser
#

BASE.CHOOSER.DialogTitle                                  Auswählen
BASE.CHOOSER.Action.Search                                Suchen
BASE.CHOOSER.Error                                        Ein Fehler ist aufgetreten.
BASE.CHOOSER.Error.Empty                                  Bitte Suchkriterien eintragen.
BASE.CHOOSER.Error.MinimumLength                          Bitte zumindest %s Buchstaben verwenden.
BASE.CHOOSER.SearchPlaceholder                            Suchbegriff eingeben
BASE.CHOOSER.ResultsPlaceholder                           Suchbegriff eingeben und Enter-Taste drücken
BASE.CHOOSER.Results.ResultCount                          %s Resultat(e) gefunden.
BASE.CHOOSER.Results.TooManyResults                       Mehr als %s Treffer gefunden, bitte Auswahl einschränken.


#
#  User
#

BASE.USER.Status.0                                        aktiv
BASE.USER.Status.1                                        warten auf Verifizierung
BASE.USER.Status.9                                        deaktiviert

BASE.USER.Roles                                           Funktionen

BASE.USER.LOGIN.Error.EmailEmpty                          E-Mail Adresse fehlt.
BASE.USER.LOGIN.Error.PasswordEmpty                       Passwort fehlt.
BASE.USER.LOGIN.Error.LoginFailed                         Ungültige Zugangsdaten.
BASE.USER.LOGIN.Error.UserPending                         Benutzerkonto in Arbeit (noch nicht verifiziert).
BASE.USER.LOGIN.Error.UserDisabled                        Benutzerkonto ist deaktiviert.

BASE.USER.LOGIN.Success                                   Anmeldung erfolgreich


#
#  Tabbed view: log
#

BASE.LOG.Title                                            Protokoll

BASE.LOG.LogEntry.Action.add                              hinzugefügt
BASE.LOG.LogEntry.Action.edit                             bearbeitet
BASE.LOG.LogEntry.Action.delete                           gelöscht

BASE.LOG.Fld.Tstamp                                       Datum/Zeit
BASE.LOG.Fld.User                                         Benutzer
BASE.LOG.Fld.Entry                                        Protokolleintrag
BASE.LOG.Fld.Data                                         Protokolldetails

BASE.LOG.User.System                                      System

BASE.LOG.List.NoResults                                   Keine Protokolleinträge.
