<? if (!empty($this->templateVar['title'])) { ?>
<h1>{title}</h1>
<? } ?>

<? if (!empty($this->templateVar['alert'])) { ?>
<div class="alert alert-warning">{alert}</div>
<? } ?>

<? if (!empty($this->templateVar['info'])) { ?>
<div class="info">{info}</div>
<? } ?>

<div class="block">

<? if (!empty($this->templateVar['form_begin'])) { ?>
{form_begin}

	<? if (!empty($this->templateVar['form_fieldgrouping'])) { ?>

	<? foreach ($this->templateVar['form_fieldgroup'] as $fieldGroupTag => $fieldGroupProperties) { $fg++; ?>

	<div class="panel panel-default panel-dark<?= !empty($fieldGroupProperties['class'])?' '.$fieldGroupProperties['class']:'' ?>" <?= $fieldGroupProperties['indented']?' style="padding-left: 20px"':'' ?>>
	<div class="panel-heading<?=($fieldGroupProperties['checkable']?' checkable':'')?>">
		<!-- <div class="blocktitle"> -->
		<?php
							if($fieldGroupProperties['checkable'])
							{
								/*echo '<label class="icon-check-empty"><input id="'.$fieldGroupProperties['checkfield'].'" type="checkbox" class="pull-left" id="'.$fieldGroupProperties['checkfield'].'" name="'.$fieldGroupProperties['checkfield'].'" value="1" '.(!empty($fieldGroupProperties['value'])?'checked="checked"':'').' onclick="$(\'#'.$fieldGroupProperties['checkfield'].':checked\').length?$(\'#fieldgroup_'.$fieldGroupTag.'\').show():$(\'#fieldgroup_'.$fieldGroupTag.'\').hide()">';
		echo '<h3 class="panel-title">'.$fieldGroupProperties['title'].'</h3>';
		echo '<span class="icon-check'.(!empty($fieldGroupProperties['value'])?'':' hidden').'"></span></label>';*/
		echo '<label for="'.$fieldGroupProperties['checkfield'].'">';
			echo '<input';
			echo ' id="'.$fieldGroupProperties['checkfield'].'"';
			echo ' type="checkbox"';
			echo ' class="pull-left"';
			echo ' id="'.$fieldGroupProperties['checkfield'].'"';
			echo ' name="'.$fieldGroupProperties['checkfield'].'"';
			echo ' value="1"';
			echo ' '.(!empty($fieldGroupProperties['value'])?'checked="checked"':'');
			echo ' onclick="$(\'#'.$fieldGroupProperties['checkfield'].':checked\').length?$(\'#fieldgroup_'.$fieldGroupTag.'\').show():$(\'#fieldgroup_'.$fieldGroupTag.'\').hide();'.(!empty($fieldGroupProperties['event']['onclick'])?$fieldGroupProperties['event']['onclick']:'').'"';
			if (!empty($fieldGroupProperties['event']))
			{
			foreach ($fieldGroupProperties['event'] as $event => $eventAction)
			{
			if ($event=='onclick') continue;
			echo ' '.$event.='="'.$eventAction.'"';
			}
			}
			echo' '.(!empty($this->templateVar['DISABLED'])?'disabled="disabled"':'');
			echo '>';
			echo '<span class="icon-check-empty'.(!empty($fieldGroupProperties['value'])?' icon-check':'').'"></span>';
			echo '<h3 class="panel-title fieldgroup-title-checkable">'.$fieldGroupProperties['title'].'</h3>';
			echo '</label>';
		}
		else
		{
		echo '<h3 class="panel-title">'.$fieldGroupProperties['title'].'</h3>';
		}
		?>

		<!-- </div> -->
		<? if (!empty($fieldGroupProperties['actions'])) { ?><div class="blockactions"><?= $fieldGroupProperties['actions'] ?></div><? } ?>
	</div>
	<div class="panel-body" id="fieldgroup_<?= $fieldGroupTag ?>">
		<table class="labengine-form">
			<tbody>
			<?= $this->templateVar['fieldgroup_'.$fieldGroupTag] ?>
			</tbody>
		</table>
	</div>
</div>
<? } ?>

<? } else { ?>

	<div class="well bs-component">
	<fieldset>
	{form_field}
	</fieldset>
	<? if (!empty($this->templateVar['notes'])) { ?>
	<div class="notes">{notes}</div>
	<? } ?>
	</div>

<? } ?>

<? if (strlen($this->templateVar['form_submit'])) { ?>
<div class="buttonbar pull-right">
	{form_submit}
</div>
<? } ?>

{form_end}
<? } ?>

</div>

<? if (!empty($this->templateVar['form_fieldgrouping']) || strlen($this->templateVar['js'])) { ?>
<script type="text/javascript">
	$(document).ready(function(){
		<?
		if (!empty($this->templateVar['form_fieldgrouping']))
		{
			foreach ($this->templateVar['form_fieldgroup'] as $fieldGroupTag => $fieldGroupProperties)
			{
				if (!$fieldGroupProperties['checkable']) continue;
				echo "$('#".$fieldGroupProperties['checkfield'].":checked').length?$('#fieldgroup_".$fieldGroupTag."').show():$('#fieldgroup_".$fieldGroupTag."').hide();\n";
			}
		}
		?>
		{js}
	});
</script>
<? } ?>
