<h1>{title}</h1>
<div class="contentwrapper struct-list">
<div class="list">
	{extra_header}
	{globalaction}
	<? if (strlen($this->templateVar['filter'])) { ?>
	<div id="filter-<?=$this->templateVar['id']?>" class="panel-filter">
		{filter}
	</div>
	<? } ?>
	{list}
	<div class="paging">{paging}</div>
	{extra_footer}
</div>
<? if (strlen($this->templateVar['js'])) { ?>
<script type="text/javascript">
	{js}
</script>
<? } ?>
{returnlink}
</div>