# --------------------------------------------------------------------------- #


#
#
#  LabEngine™ 7
#  The CMS database
#  (c) Codelab Solutions OÜ <codelab@codelab.ee>
#
#


# --------------------------------------------------------------------------- #


#
#  DB version
#

REPLACE INTO db_version VALUES('cms','1');


# --------------------------------------------------------------------------- #


#
#  Site structure
#

DROP TABLE IF EXISTS cms_struct;
CREATE TABLE cms_struct
(
  struct_oid                   BIGINT UNSIGNED NOT NULL,       #  Struct OID
  parent_oid                   BIGINT UNSIGNED NOT NULL,       #  Parent OID
  ord                          BIGINT UNSIGNED NOT NULL,       #  Order

  struct_url                   VARCHAR(255) NOT NULL,          #  URL tag
  struct_url_full              VARCHAR(255) NOT NULL,          #  Full URL to the page
  struct_description           VARCHAR(255) NOT NULL,          #  Page description

  add_tstamp                   DATETIME NOT NULL,              #  Add: timestamp
  add_user_oid                 BIGINT UNSIGNED NOT NULL,       #  Add: user OID
  mod_tstamp                   DATETIME NOT NULL,              #  Mod: timestamp
  mod_user_oid                 BIGINT UNSIGNED NOT NULL,       #  Mod: user OID

  PRIMARY KEY (struct_oid),
  INDEX (parent_oid,ord),
  UNIQUE (struct_url_full)
)
ENGINE=INNODB
DEFAULT CHARSET=utf8
DEFAULT COLLATE=utf8_estonian_ci;


# --------------------------------------------------------------------------- #


#
#  Pages
#

DROP TABLE IF EXISTS cms_page;
CREATE TABLE cms_page
(
  page_oid                     BIGINT UNSIGNED NOT NULL,       #  Page OID
  struct_oid                   BIGINT UNSIGNED NOT NULL,       #  Struct OID
  page_lang                    VARCHAR(2) NOT NULL,            #  Language

  page_title                   VARCHAR(255) NOT NULL,          #  Page title
  page_published               TINYINT NOT NULL,               #  Is published? (1 - yes, 0 - no)
  page_access                  TINYINT NOT NULL,               #  Access (0 - public, 1 - requires login)

  add_tstamp                   DATETIME NOT NULL,              #  Add: timestamp
  add_user_oid                 BIGINT UNSIGNED NOT NULL,       #  Add: user OID
  mod_tstamp                   DATETIME NOT NULL,              #  Mod: timestamp
  mod_user_oid                 BIGINT UNSIGNED NOT NULL,       #  Mod: user OID

  PRIMARY KEY (page_oid),
  INDEX (struct_oid,page_lang)
)
ENGINE=INNODB
DEFAULT CHARSET=utf8
DEFAULT COLLATE=utf8_estonian_ci;

DROP TABLE IF EXISTS cms_page_prop;
CREATE TABLE cms_page_prop
(
  page_oid                     BIGINT UNSIGNED NOT NULL,       #  User OID
  page_prop_name               VARCHAR(255) NOT NULL,          #  Prop name
  page_prop_value              MEDIUMTEXT NOT NULL,            #  Prop value

  PRIMARY KEY (page_oid,page_prop_name)
)
ENGINE=INNODB
DEFAULT CHARSET=utf8
DEFAULT COLLATE=utf8_estonian_ci;


# --------------------------------------------------------------------------- #


#
#  Languages
#

DROP TABLE IF EXISTS cms_lang;
CREATE TABLE cms_lang
(
  lang_oid                     BIGINT UNSIGNED NOT NULL,       #  Language OID
  ord                          BIGINT UNSIGNED NOT NULL,       #  Order

  lang_tag                     VARCHAR(255) NOT NULL,          #  Language tag
  lang_status                  TINYINT NOT NULL,               #  Language status (0 - active, 1 - disabled)

  add_tstamp                   DATETIME NOT NULL,              #  Add: timestamp
  add_user_oid                 BIGINT UNSIGNED NOT NULL,       #  Add: user OID
  mod_tstamp                   DATETIME NOT NULL,              #  Mod: timestamp
  mod_user_oid                 BIGINT UNSIGNED NOT NULL,       #  Mod: user OID

  PRIMARY KEY (lang_oid),
  UNIQUE (lang_tag),
  INDEX (ord)
)
ENGINE=INNODB
DEFAULT CHARSET=utf8
DEFAULT COLLATE=utf8_estonian_ci;


# --------------------------------------------------------------------------- #


#
#  Modules
#

DROP TABLE IF EXISTS cms_module;
CREATE TABLE cms_module
(
  module_oid                   BIGINT UNSIGNED NOT NULL,       #  Module OID

  module_type                  TINYINT NOT NULL,               #  Module type (0 - system, 1 - site)
  module_tag                   VARCHAR(255) NOT NULL,          #  Module tag
  module_name                  VARCHAR(255) NOT NULL,          #  Module name

  module_status                TINYINT NOT NULL,               #  Module status (0 - active, 1 - disabled)

  add_tstamp                   DATETIME NOT NULL,              #  Add: timestamp
  add_user_oid                 BIGINT UNSIGNED NOT NULL,       #  Add: user OID
  mod_tstamp                   DATETIME NOT NULL,              #  Mod: timestamp
  mod_user_oid                 BIGINT UNSIGNED NOT NULL,       #  Mod: user OID

  PRIMARY KEY (module_oid),
  UNIQUE (module_tag)
)
ENGINE=INNODB
DEFAULT CHARSET=utf8
DEFAULT COLLATE=utf8_estonian_ci;


# --------------------------------------------------------------------------- #


#
#  Insert initial data
#

INSERT INTO base_sequence VALUES('10001001','base_user');
INSERT INTO base_user(user_oid,user_email,user_name,user_password,user_status) VALUES('10001001','admin@codelab.ee','Default Administrator','21232f297a57a5a743894a0e4a801fc3');
INSERT INTO base_user_role VALUES('10001001','cms');
INSERT INTO base_user_role VALUES('10001001','cms_supervisor');

INSERT INTO base_sequence VALUES('10001002','cms_struct');
INSERT INTO cms_struct(struct_oid,parent_oid,ord,struct_url,struct_url_full,struct_description) VALUES('10001002','0','10001002','index','/index','Index page');

INSERT INTO base_sequence VALUES('10001003','cms_lang');
INSERT INTO cms_lang(lang_oid,ord,lang_tag,lang_status) VALUES('10001003','10001003','en','0');



# --------------------------------------------------------------------------- #
