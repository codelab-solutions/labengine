# --------------------------------------------------------------------------- #


#
#
#  LabEngine™ 7
#  The base database
#  v.003: Add DB-based session support
#  (c) Codelab Solutions OÜ <codelab@codelab.ee>
#
#


# --------------------------------------------------------------------------- #


#
#  DB-based sessions
#

DROP TABLE IF EXISTS base_session;
CREATE TABLE base_session
(
  session_id                   VARCHAR(255) NOT NULL,          #  Session ID
  session_tstamp               DATETIME NOT NULL,              #  Session last used timestamp
  session_locked               DATETIME NOT NULL,              #  Session lock timestamp
  session_data                 MEDIUMBLOB NOT NULL,            #  Session data
  PRIMARY KEY (session_id)
)
ENGINE=INNODB
DEFAULT CHARSET=utf8
DEFAULT COLLATE=utf8_estonian_ci;


# --------------------------------------------------------------------------- #


REPLACE INTO db_version VALUES('base','3');


# --------------------------------------------------------------------------- #
