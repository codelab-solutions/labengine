# --------------------------------------------------------------------------- #


#
#
#  LabEngine™ 7
#  The base database
#  v.002: Add profiler tables
#  (c) Codelab Solutions OÜ <codelab@codelab.ee>
#
#


# --------------------------------------------------------------------------- #


#
#  Page profiler: page request data
#

DROP TABLE IF EXISTS base_pageprofiler;
CREATE TABLE base_pageprofiler
(
  pageprofiler_tstamp               DATETIME NOT NULL,         #  Timestamp

  pageprofiler_request_id           VARCHAR(255) NOT NULL,     #  Request ID
  pageprofiler_user_oid             BIGINT UNSIGNED NOT NULL,  #  User OID

  pageprofiler_request_method       VARCHAR(255) NOT NULL,     #  Request method
  pageprofiler_request_uri          VARCHAR(255) NOT NULL,     #  Request URI

  pageprofiler_requesttime          DECIMAL(20,8) NOT NULL,    #  Request time (seconds)
  pageprofiler_dbquerytime          DECIMAL(20,8) NOT NULL,    #  DB query time (seconds)

  pageprofiler_dbquerycnt           INT UNSIGNED NOT NULL,     #  DB query count
  pageprofiler_dbquerycnt_select    INT UNSIGNED NOT NULL,     #  DB query count
  pageprofiler_dbquerycnt_update    INT UNSIGNED NOT NULL,     #  DB query count

  pageprofiler_peakmemoryusage      BIGINT UNSIGNED NOT NULL   #  Peak memory usage
)
ENGINE=INNODB
DEFAULT CHARSET=utf8
DEFAULT COLLATE=utf8_estonian_ci;


#
#  Page profiler: query data
#

DROP TABLE IF EXISTS base_pageprofiler_query;
CREATE TABLE base_pageprofiler_query
(
  pageprofiler_query_tstamp         DATETIME NOT NULL,         #  Timestamp
  pageprofiler_query_request_id     VARCHAR(255) NOT NULL,     #  Request ID
  pageprofiler_query_no             INT NOT NULL,              #  Query no during request

  pageprofiler_query_sql            TEXT NOT NULL,             #  SQL
  pageprofiler_query_time           DECIMAL(10,2) NOT NULL,    #  Query time
  pageprofiler_query_affectedrows   BIGINT NOT NULL,           #  Affected rows
  pageprofiler_query_explain        TEXT NOT NULL              #  EXPLAIN output
)
ENGINE=INNODB
DEFAULT CHARSET=utf8
DEFAULT COLLATE=utf8_estonian_ci;



# --------------------------------------------------------------------------- #


REPLACE INTO db_version VALUES('base','2');


# --------------------------------------------------------------------------- #
